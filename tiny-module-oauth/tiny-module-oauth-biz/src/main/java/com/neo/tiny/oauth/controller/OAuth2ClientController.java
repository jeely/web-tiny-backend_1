package com.neo.tiny.oauth.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.oauth.entity.SysOauth2ClientDO;
import com.neo.tiny.oauth.service.SysOauth2ClientService;
import com.neo.tiny.oauth.vo.client.SysOauth2ClientCreateReqVO;
import com.neo.tiny.oauth.vo.client.SysOauth2ClientPageReqVO;
import com.neo.tiny.oauth.vo.client.SysOauth2ClientResVO;
import com.neo.tiny.oauth.vo.client.SysOauth2ClientUpdateReqVO;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.util.MyBatisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/11/10 00:01
 */
@Api(tags = "oauth2实现")
@RestController
@AllArgsConstructor
@RequestMapping("/open/oauth2-client")
public class OAuth2ClientController {

    private final SysOauth2ClientService sysOauth2ClientService;

    /**
     * 分页查询
     *
     * @param sysOauth2ClientPageReqVO oauth
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<IPage<SysOauth2ClientResVO>> getSysOauth2ClientPage(@Valid SysOauth2ClientPageReqVO sysOauth2ClientPageReqVO) {


        Page<SysOauth2ClientDO> page = sysOauth2ClientService.page(MyBatisUtils.buildPage(sysOauth2ClientPageReqVO), new LambdaQueryWrapperBase<SysOauth2ClientDO>()
                .likeIfPresent(SysOauth2ClientDO::getClientName, sysOauth2ClientPageReqVO.getClientName())
                .eqIfPresent(SysOauth2ClientDO::getClientStatus, sysOauth2ClientPageReqVO.getClientStatus()));

        return ResResult.success(CommonPage.restPage(page, SysOauth2ClientResVO.class));
    }


    /**
     * 新增oauth
     *
     * @param req oauth
     * @return ResResult
     */
    @ApiOperation("新增oauth客户端")
    @PostMapping("/create")
    public ResResult<Long> save(@Valid @RequestBody SysOauth2ClientCreateReqVO req) {

        Long clientId = sysOauth2ClientService.createOauth2Client(req);

        return ResResult.success(clientId);
    }


    /**
     * 修改oauth
     *
     * @param sysOauth2ClientUpdateReqVO oauth
     * @return ResResult
     */
    @ApiOperation("修改oauth")
    @PutMapping("/update")
    public ResResult<Boolean> updateById(@Valid @RequestBody SysOauth2ClientUpdateReqVO sysOauth2ClientUpdateReqVO) {
        sysOauth2ClientService.updateOauth2Client(sysOauth2ClientUpdateReqVO);
        return ResResult.success(true);
    }


    /**
     * 通过id查询oauth
     *
     * @param id id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{id}")
    public ResResult<SysOauth2ClientResVO> getById(@PathVariable("id") Long id) {
        return ResResult.success(BeanUtil.copyProperties(sysOauth2ClientService.getById(id), SysOauth2ClientResVO.class));
    }

    /**
     * 通过id删除oauth
     *
     * @param id id
     * @return R
     */
    @ApiOperation("通过id删除oauth")
    @DeleteMapping("/{id}")
    public ResResult<Boolean> removeById(@PathVariable Long id) {
        sysOauth2ClientService.deleteOauth2Client(id);
        return ResResult.success(true);
    }
}
