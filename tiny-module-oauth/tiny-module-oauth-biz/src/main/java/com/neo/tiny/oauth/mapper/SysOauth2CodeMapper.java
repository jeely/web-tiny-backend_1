package com.neo.tiny.oauth.mapper;

import com.neo.tiny.oauth.entity.SysOauth2CodeDO;
import com.neo.tiny.query.BaseMapperX;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: OAuth2 授权码
 * @Author: yqz
 * @CreateDate: 2022-11-11 10:44:38
 */
@Mapper
public interface SysOauth2CodeMapper extends BaseMapperX<SysOauth2CodeDO> {

    /**
     * 根据code查询授权码实体
     *
     * @param code 授权code
     * @return 授权码实体
     */
    default SysOauth2CodeDO selectByCode(String code) {

        return selectOne(SysOauth2CodeDO::getCode, code);

    }

}
