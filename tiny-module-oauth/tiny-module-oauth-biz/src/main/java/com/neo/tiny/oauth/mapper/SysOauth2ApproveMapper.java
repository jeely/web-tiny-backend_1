package com.neo.tiny.oauth.mapper;

import com.neo.tiny.oauth.entity.SysOauth2ApproveDO;
import com.neo.tiny.query.BaseMapperX;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description: OAuth2 批准
 * @Author: yqz
 * @CreateDate: 2022-11-11 17:18:47
 */
@Mapper
public interface SysOauth2ApproveMapper extends BaseMapperX<SysOauth2ApproveDO> {

    default int update(SysOauth2ApproveDO updateObj) {
        return update(updateObj, new LambdaQueryWrapperBase<SysOauth2ApproveDO>()
                .eq(SysOauth2ApproveDO::getUserId, updateObj.getUserId())
                .eq(SysOauth2ApproveDO::getUserType, updateObj.getUserType())
                .eq(SysOauth2ApproveDO::getClientId, updateObj.getClientId())
                .eq(SysOauth2ApproveDO::getScope, updateObj.getScope()));
    }

    default List<SysOauth2ApproveDO> selectListByUserIdAndUserTypeAndClientId(Long userId, Integer userType, String clientId) {
        return selectList(new LambdaQueryWrapperBase<SysOauth2ApproveDO>()
                .eq(SysOauth2ApproveDO::getUserId, userId)
                .eq(SysOauth2ApproveDO::getUserType, userType)
                .eq(SysOauth2ApproveDO::getClientId, clientId));
    }

}
