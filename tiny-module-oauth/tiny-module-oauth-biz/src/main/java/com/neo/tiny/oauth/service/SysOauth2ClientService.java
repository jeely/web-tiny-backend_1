
package com.neo.tiny.oauth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.oauth.entity.SysOauth2ClientDO;
import com.neo.tiny.oauth.vo.client.SysOauth2ClientCreateReqVO;
import com.neo.tiny.oauth.vo.client.SysOauth2ClientUpdateReqVO;

import javax.validation.Valid;
import java.util.Collection;

/**
 * @Description: oauth
 * @Author: yqz
 * @CreateDate: 2022-11-10 11:08:16
 */
public interface SysOauth2ClientService extends IService<SysOauth2ClientDO> {


    /**
     * 创建oAuth客户端
     *
     * @param req 请求参数
     * @return 主键
     */
    public Long createOauth2Client(SysOauth2ClientCreateReqVO req);

    /**
     * 更新 OAuth2 客户端
     *
     * @param updateReqVO 更新信息
     */
    void updateOauth2Client(@Valid SysOauth2ClientUpdateReqVO updateReqVO);

    /**
     * 删除 OAuth2 客户端
     *
     * @param id 编号
     */
    void deleteOauth2Client(Long id);

    /**
     * 从缓存中，校验客户端是否合法
     * <p>
     * 非空时，进行校验
     *
     * @param clientId            客户端编号
     * @param clientSecret        客户端密钥
     * @param authorizedGrantType 授权方式
     * @param scopes              授权范围
     * @param redirectUri         重定向地址
     * @return 客户端
     */
    SysOauth2ClientDO validOauthClient(String clientId, String clientSecret,
                                       String authorizedGrantType, Collection<String> scopes, String redirectUri);

    /**
     * 从缓存中，校验客户端是否合法
     *
     * @return 客户端
     */
    default SysOauth2ClientDO validOauthClient(String clientId) {
        return validOauthClient(clientId, null, null, null, null);
    }

}
