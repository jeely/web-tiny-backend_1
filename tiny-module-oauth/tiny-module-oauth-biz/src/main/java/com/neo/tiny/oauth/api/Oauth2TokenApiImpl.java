package com.neo.tiny.oauth.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import com.neo.tiny.oauth.entity.SysOauth2AccessTokenDO;
import com.neo.tiny.oauth.enums.LogoutScope;
import com.neo.tiny.oauth.service.Oauth2TokenService;
import com.neo.tiny.token.store.AccessTokenInfo;
import com.neo.tiny.token.store.OAuth2AccessToken;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 供其他module调用使用的service实现
 * @Author: yqz
 * @CreateDate: 2022/12/10 23:11
 */
@Service
@AllArgsConstructor
public class Oauth2TokenApiImpl implements Oauth2TokenApi {

    private final Oauth2TokenService oauth2TokenService;

    @Override
    public AccessTokenInfo createAccessToken(Long userId, String userName, Integer userType,
                                             String clientId, List<String> scopes) {
        OAuth2AccessToken accessToken = oauth2TokenService.createAccessToken(userId, userName, userType, clientId, scopes);
        return BeanUtil.copyProperties(accessToken, AccessTokenInfo.class);
    }

    @Override
    public AccessTokenInfo removeAccessToken(String accessToken) {
        Assert.notBlank(accessToken, "accessToken不能为空");
        // 本平台退出时，清空所有单点认证过的client
        SysOauth2AccessTokenDO sysOauth2AccessTokenDO = oauth2TokenService.removeAccessToken(accessToken, LogoutScope.ALl_CLIENT.getCode());
        return BeanUtil.copyProperties(sysOauth2AccessTokenDO, AccessTokenInfo.class);
    }

    @Override
    public OAuth2AccessToken checkAccessToken(String accessToken) {
        Assert.notBlank(accessToken, "accessToken不能为空");
        return oauth2TokenService.checkAccessToken(accessToken);
    }
}
