package com.neo.tiny.oauth.mapper;

import com.neo.tiny.oauth.entity.SysOauth2ClientDO;
import com.neo.tiny.query.BaseMapperX;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: oauth
 * @Author: yqz
 * @CreateDate: 2022-11-10 11:08:16
 */
@Mapper
public interface SysOauth2ClientMapper extends BaseMapperX<SysOauth2ClientDO> {

    /**
     * 根据客户端id查询客户端
     *
     * @param clientId 客户端id
     * @return 客户端
     */
    default SysOauth2ClientDO selectByClientId(String clientId) {
        return selectOne(SysOauth2ClientDO::getClientId, clientId);
    }
}
