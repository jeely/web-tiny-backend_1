package com.neo.tiny.oauth.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.neo.tiny.admin.api.dept.DeptApi;
import com.neo.tiny.admin.dto.dept.SysDeptDTO;
import com.neo.tiny.admin.dto.user.SysUserDTO;
import com.neo.tiny.admin.vo.post.PostVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.common.util.CommonDoTransfer;
import com.neo.tiny.oauth.vo.token.user.Oauth2UserInfoRespVO;
import com.neo.tiny.secrity.model.AdminUserDetails;
import com.neo.tiny.secrity.util.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yqz
 * @Description 认证用户
 * @CreateDate 2022/11/11 9:06
 */
@Slf4j
@Api(tags = "OAuth2.0 用户")
@AllArgsConstructor
@RestController
@RequestMapping("/open/oauth2-user")
public class OAuth2UserController {

    private final DeptApi deptApi;

    @GetMapping("/get")
    @ApiOperation("获得用户基本信息")
    public ResResult<Oauth2UserInfoRespVO> getUserInfo() {
        // 获得用户基本信息
        AdminUserDetails userDetails = SecurityUtils.getUser();
        Oauth2UserInfoRespVO resp = new Oauth2UserInfoRespVO();
        // 获得部门信息
        if (userDetails != null) {
            SysUserDTO user = userDetails.getSysUser();
            if (user != null) {
                BeanUtil.copyProperties(user, resp);
                resp.setUserId(user.getId());
                SysDeptDTO dept = deptApi.getDept(user.getDeptId());
                Oauth2UserInfoRespVO.Dept deptInfo = BeanUtil.copyProperties(dept, Oauth2UserInfoRespVO.Dept.class);
                deptInfo.setDeptId(dept.getId());
                resp.setDept(deptInfo);
            }
            // 获得岗位信息
            List<PostVO> postList = userDetails.getPostList();
            if (CollUtil.isNotEmpty(postList)) {
                List<Oauth2UserInfoRespVO.Post> postInfoList = postList.stream().map(post -> {
                    Oauth2UserInfoRespVO.Post postInfo = CommonDoTransfer.transfer(post, Oauth2UserInfoRespVO.Post.class);
                    postInfo.setPostId(post.getId());
                    return postInfo;
                }).collect(Collectors.toList());
                resp.setPosts(postInfoList);
            }
        }
        return ResResult.success(resp);
    }
}
