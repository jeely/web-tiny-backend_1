
package com.neo.tiny.oauth.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.neo.tiny.data.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Description: oauth
 * @Author: yqz
 * @CreateDate: 2022-11-10 11:08:16
 */
@Data
@TableName(value = "sys_oauth2_client", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "oauth")
public class SysOauth2ClientDO extends BaseDO {

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("编号")
    private Long id;

    /**
     * 客户端编号
     */
    @ApiModelProperty("客户端编号")
    private String clientId;

    /**
     * 客户端密钥
     */
    @ApiModelProperty("客户端密钥")
    private String clientSecret;

    /**
     * 应用名
     */
    @ApiModelProperty("应用名")
    private String clientName;

    /**
     * 应用图标
     */
    @ApiModelProperty("应用图标")
    private String clientLogo;

    /**
     * 应用描述
     */
    @ApiModelProperty("应用描述")
    private String clientDescription;

    /**
     * 状态
     */
    @ApiModelProperty("状态(0-关闭，1-正常)")
    private Integer clientStatus;

    /**
     * 访问令牌的有效期
     */
    @ApiModelProperty("访问令牌的有效期")
    private Integer accessTokenValiditySeconds;

    /**
     * 刷新令牌的有效期
     */
    @ApiModelProperty("刷新令牌的有效期")
    private Integer refreshTokenValiditySeconds;

    /**
     * 可重定向的 URI 地址
     */
    @ApiModelProperty("可重定向的 URI 地址")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> redirectUris;

    /**
     * 授权类型
     * 枚举 {@link com.neo.tiny.oauth.enums.OAuth2GrantTypeEnum}
     */
    @ApiModelProperty("授权类型")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> authorizedGrantTypes;

    /**
     * 授权范围
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> scopes;

    /**
     * 自动授权的 Scope
     * <p>
     * code 授权时，如果 scope 在这个范围内，则自动通过
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> autoApproveScopes;
    /**
     * 权限
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> authorities;

    /**
     * 资源
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> resourceIds;

    /**
     * 附加信息 json
     */
    @ApiModelProperty("附加信息")
    private String additionalInformation;


}
