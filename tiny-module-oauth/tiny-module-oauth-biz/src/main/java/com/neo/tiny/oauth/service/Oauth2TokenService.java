package com.neo.tiny.oauth.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.token.store.OAuth2AccessToken;
import com.neo.tiny.oauth.entity.SysOauth2AccessTokenDO;
import com.neo.tiny.oauth.vo.token.Oauth2AccessTokenPageReqVO;

import java.util.List;

/**
 * @author yqz
 * @Description 授权相关业务
 * @CreateDate 2022/11/11 9:28
 */
public interface Oauth2TokenService {


    /**
     * 创建访问令牌
     * 注意：该流程中，会包含创建刷新令牌的创建
     * <p>
     * 参考 DefaultTokenServices 的 createAccessToken 方法
     *
     * @param userId   用户编号
     * @param userType 用户类型
     * @param clientId 客户端编号
     * @param scopes   授权范围
     * @return 访问令牌的信息
     */
    OAuth2AccessToken createAccessToken(Long userId, String userName, Integer userType, String clientId, List<String> scopes);


    /**
     * 刷新访问令牌
     * <p>
     * 参考 DefaultTokenServices 的 refreshAccessToken 方法
     *
     * @param refreshToken 刷新令牌
     * @param clientId     客户端编号
     * @return 访问令牌的信息
     */
    OAuth2AccessToken refreshAccessToken(String refreshToken, String clientId);

    /**
     * 获得访问令牌
     * <p>
     * 参考 DefaultTokenServices 的 getAccessToken 方法
     *
     * @param accessToken 访问令牌
     * @return 访问令牌的信息
     */
    OAuth2AccessToken getAccessToken(String accessToken);

    /**
     * 校验访问令牌
     *
     * @param accessToken 访问令牌
     * @return 访问令牌的信息
     */
    OAuth2AccessToken checkAccessToken(String accessToken);

    /**
     * 移除访问令牌
     * 注意：该流程中，会移除相关的刷新令牌
     * <p>
     * 参考 DefaultTokenServices 的 revokeToken 方法
     *
     * @param accessToken 刷新令牌
     * @param removeScope 移除范围：0-本client，1-全部 {@link com.neo.tiny.oauth.enums.LogoutScope}
     * @return 访问令牌的信息
     */
    SysOauth2AccessTokenDO removeAccessToken(String accessToken, Integer removeScope);

    /**
     * 获得访问令牌分页
     *
     * @param reqVO 请求
     * @return 访问令牌分页
     */
    IPage<SysOauth2AccessTokenDO> getAccessTokenPage(Oauth2AccessTokenPageReqVO reqVO);
}
