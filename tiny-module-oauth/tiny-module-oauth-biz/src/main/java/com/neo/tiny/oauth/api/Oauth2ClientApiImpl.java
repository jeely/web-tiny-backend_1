package com.neo.tiny.oauth.api;

import cn.hutool.core.lang.Assert;
import com.neo.tiny.common.util.CommonDoTransfer;
import com.neo.tiny.oauth.entity.SysOauth2ClientDO;
import com.neo.tiny.oauth.service.SysOauth2ClientService;
import com.neo.tiny.oauth.vo.client.SysOauth2ClientBaseVO;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @Description: 客户端信息实现
 * @Author: yqz
 * @CreateDate: 2022/12/13 20:05
 */
@Service
@AllArgsConstructor
public class Oauth2ClientApiImpl implements Oauth2ClientApi {

    private final SysOauth2ClientService clientService;

    @Override
    public SysOauth2ClientBaseVO loadClientDetailsByClientId(String clientId) {
        Assert.notBlank(clientId, "客户端id不能为空");
        SysOauth2ClientDO clientDO = clientService.getOne(new LambdaQueryWrapperBase<SysOauth2ClientDO>()
                .eq(SysOauth2ClientDO::getClientId, clientId));
        return CommonDoTransfer.transfer(clientDO, SysOauth2ClientBaseVO.class);
    }
}
