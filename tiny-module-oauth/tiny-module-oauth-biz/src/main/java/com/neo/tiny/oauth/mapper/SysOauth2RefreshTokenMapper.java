package com.neo.tiny.oauth.mapper;

import com.neo.tiny.oauth.entity.SysOauth2RefreshTokenDO;
import com.neo.tiny.query.BaseMapperX;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: oauth 刷新token
 * @Author: yqz
 * @CreateDate: 2022-11-11 11:36:08
 */
@Mapper
public interface SysOauth2RefreshTokenMapper extends BaseMapperX<SysOauth2RefreshTokenDO> {


    default int deleteByRefreshToken(String refreshToken) {
        return delete(new LambdaQueryWrapperBase<SysOauth2RefreshTokenDO>()
                .eq(SysOauth2RefreshTokenDO::getRefreshToken, refreshToken));
    }

    default SysOauth2RefreshTokenDO selectByRefreshTokenAndClientId(String refreshToken, String clientId) {
        return selectOne(new LambdaQueryWrapperBase<SysOauth2RefreshTokenDO>()
                .eq(SysOauth2RefreshTokenDO::getRefreshToken, refreshToken)
                .eqIfPresent(SysOauth2RefreshTokenDO::getClientId, clientId));
    }

}
