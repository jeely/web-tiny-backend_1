package com.neo.tiny.oauth.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.oauth.entity.SysOauth2AccessTokenDO;
import com.neo.tiny.oauth.vo.token.Oauth2AccessTokenPageReqVO;
import com.neo.tiny.query.BaseMapperX;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

/**
 * @Description: oAuth令牌
 * @Author: yqz
 * @CreateDate: 2022-11-11 09:46:54
 */
@Mapper
public interface SysOauth2AccessTokenMapper extends BaseMapperX<SysOauth2AccessTokenDO> {

    default SysOauth2AccessTokenDO selectByAccessToken(String accessToken) {
        return selectOne(SysOauth2AccessTokenDO::getAccessToken, accessToken);
    }

    default List<SysOauth2AccessTokenDO> selectListByRefreshTokenAndClientId(String refreshToken, String clientId) {
        return selectList(new LambdaQueryWrapperBase<SysOauth2AccessTokenDO>()
                .eq(SysOauth2AccessTokenDO::getRefreshToken, refreshToken)
                .eqIfPresent(SysOauth2AccessTokenDO::getClientId, clientId));
    }

    default IPage<SysOauth2AccessTokenDO> selectPage(Oauth2AccessTokenPageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperBase<SysOauth2AccessTokenDO>()
                .eqIfPresent(SysOauth2AccessTokenDO::getUserId, reqVO.getUserId())
                .eqIfPresent(SysOauth2AccessTokenDO::getUserType, reqVO.getUserType())
                .likeIfPresent(SysOauth2AccessTokenDO::getClientId, reqVO.getClientId())
                .gt(SysOauth2AccessTokenDO::getExpiresTime, new Date())
                .orderByDesc(SysOauth2AccessTokenDO::getId));
    }

    /**
     * 根据用户id，查询该用户的所有token
     *
     * @param userId      用户id
     * @param accessToken 凭证
     * @return token数据集
     */
    default List<SysOauth2AccessTokenDO> selectListByUserIdAndAccessToken(Long userId, String accessToken) {
        return selectList(new LambdaQueryWrapperBase<SysOauth2AccessTokenDO>()
                .eqIfPresent(SysOauth2AccessTokenDO::getAccessToken, accessToken)
                .eq(SysOauth2AccessTokenDO::getUserId, userId));
    }

}
