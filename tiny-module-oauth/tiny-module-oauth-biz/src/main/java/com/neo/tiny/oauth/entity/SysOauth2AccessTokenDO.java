
package com.neo.tiny.oauth.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description: oAuth令牌
 * @Author: yqz
 * @CreateDate: 2022-11-11 09:46:54
 */
@Data
@TableName(value = "sys_oauth2_access_token", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "oAuth令牌")
public class SysOauth2AccessTokenDO extends BaseDO {

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("编号")
    private Long id;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private Long userId;

    /**
     * 用户账户
     */
    @ApiModelProperty("用户账户")
    private String userName;

    /**
     * 用户类型
     */
    @ApiModelProperty("用户类型")
    private Integer userType;

    /**
     * 访问令牌
     */
    @ApiModelProperty("访问令牌")
    private String accessToken;

    /**
     * 刷新令牌
     */
    @ApiModelProperty("刷新令牌")
    private String refreshToken;

    /**
     * 客户端编号
     */
    @ApiModelProperty("客户端编号")
    private String clientId;

    /**
     * 授权范围
     */
    @ApiModelProperty("授权范围")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> scopes;

    /**
     * 过期时间
     */
    @ApiModelProperty("过期时间")
    private LocalDateTime expiresTime;


}
