
package com.neo.tiny.oauth.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.neo.tiny.data.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description: OAuth2 授权码
 * @Author: yqz
 * @CreateDate: 2022-11-11 10:44:38
 */
@Data
@TableName(value = "sys_oauth2_code", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "OAuth2 授权码")
public class SysOauth2CodeDO extends BaseDO {

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("编号")
    private Long id;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private Long userId;


    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String userName;

    /**
     * 用户类型
     */
    @ApiModelProperty("用户类型")
    private Integer userType;

    /**
     * 授权码
     */
    @ApiModelProperty("授权码")
    private String code;

    /**
     * 客户端编号
     */
    @ApiModelProperty("客户端编号")
    private String clientId;

    /**
     * 授权范围
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> scopes;

    /**
     * 过期时间
     */
    @ApiModelProperty("过期时间")
    private LocalDateTime expiresTime;

    /**
     * 可重定向的 URI 地址
     */
    @ApiModelProperty("可重定向的 URI 地址")
    private String redirectUri;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private String state;


}
