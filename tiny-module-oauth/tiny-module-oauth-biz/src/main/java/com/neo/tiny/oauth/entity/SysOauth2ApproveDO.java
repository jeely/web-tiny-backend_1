
package com.neo.tiny.oauth.entity;

import com.neo.tiny.data.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @Description: OAuth2 批准
 * @Author: yqz
 * @CreateDate: 2022-11-11 17:18:47
 */
@Data
@TableName(value = "sys_oauth2_approve", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "OAuth2 批准 ")
public class SysOauth2ApproveDO extends BaseDO {

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("编号")
    private Long id;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private Long userId;

    /**
     * 用户类型
     */
    @ApiModelProperty("用户类型")
    private Integer userType;

    /**
     * 客户端编号
     */
    @ApiModelProperty("客户端编号")
    private String clientId;

    /**
     * 授权范围
     */
    @ApiModelProperty("授权范围")
    private String scope;

    /**
     * 是否接受
     */
    @ApiModelProperty("是否接受")
    private Boolean approved;

    /**
     * 过期时间
     */
    @ApiModelProperty("过期时间")
    private LocalDateTime expiresTime;


}
