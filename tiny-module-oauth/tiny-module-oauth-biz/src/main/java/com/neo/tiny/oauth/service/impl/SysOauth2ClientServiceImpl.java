
package com.neo.tiny.oauth.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.annotations.VisibleForTesting;
import com.neo.tiny.common.constant.ErrorCodeConstants;
import com.neo.tiny.common.enums.CommonStatusEnum;
import com.neo.tiny.common.exception.WebApiException;
import com.neo.tiny.oauth.entity.SysOauth2ClientDO;
import com.neo.tiny.oauth.mapper.SysOauth2ClientMapper;
import com.neo.tiny.oauth.service.SysOauth2ClientService;
import com.neo.tiny.oauth.vo.client.SysOauth2ClientCreateReqVO;
import com.neo.tiny.oauth.vo.client.SysOauth2ClientUpdateReqVO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * @Description: oauth
 * @Author: yqz
 * @CreateDate: 2022-11-10 11:08:16
 */
@Service
@AllArgsConstructor
public class SysOauth2ClientServiceImpl extends ServiceImpl<SysOauth2ClientMapper, SysOauth2ClientDO> implements SysOauth2ClientService {

    private final SysOauth2ClientMapper oauth2ClientMapper;

    @VisibleForTesting
    void validateClientIdExists(Long id, String clientId) {
        SysOauth2ClientDO client = oauth2ClientMapper.selectByClientId(clientId);
        if (client == null) {
            return;
        }
        // 如果 id 为空，说明不用比较是否为相同 id 的客户端
        if (id == null) {
            throw new WebApiException(ErrorCodeConstants.OAUTH2_CLIENT_EXISTS);
        }
        if (!client.getId().equals(id)) {
            throw new WebApiException(ErrorCodeConstants.OAUTH2_CLIENT_EXISTS);
        }
    }

    private void validateOauth2ClientExists(Long id) {
        if (oauth2ClientMapper.selectById(id) == null) {
            throw new WebApiException(ErrorCodeConstants.OAUTH2_CLIENT_NOT_EXISTS);
        }
    }

    @Override
    public Long createOauth2Client(SysOauth2ClientCreateReqVO req) {
        validateClientIdExists(null, req.getClientId());
        SysOauth2ClientDO sysOauth2Client = BeanUtil.copyProperties(req, SysOauth2ClientDO.class);

        oauth2ClientMapper.insert(sysOauth2Client);
        // TODO 数据更新通知

        return sysOauth2Client.getId();
    }

    @Override
    public void updateOauth2Client(SysOauth2ClientUpdateReqVO updateReqVO) {
        // 校验存在
        validateOauth2ClientExists(updateReqVO.getId());
        // 校验 Client 未被占用
        validateClientIdExists(updateReqVO.getId(), updateReqVO.getClientId());

        SysOauth2ClientDO oauth2ClientDO = BeanUtil.copyProperties(updateReqVO, SysOauth2ClientDO.class);

        oauth2ClientMapper.updateById(oauth2ClientDO);
        // TODO 刷新 缓存

    }

    @Override
    public void deleteOauth2Client(Long id) {
        // 校验存在
        validateOauth2ClientExists(id);
        // 删除
        oauth2ClientMapper.deleteById(id);
        // TODO 刷新缓存
    }

    @Override
    public SysOauth2ClientDO validOauthClient(String clientId, String clientSecret, String authorizedGrantType,
                                              Collection<String> scopes, String redirectUri) {

        SysOauth2ClientDO client = oauth2ClientMapper.selectByClientId(clientId);
        if (client == null) {
            throw new WebApiException(ErrorCodeConstants.OAUTH2_CLIENT_NOT_EXISTS);
        }
        if (ObjectUtil.notEqual(client.getClientStatus().toString(), CommonStatusEnum.ENABLE.getStatus())) {
            throw new WebApiException(ErrorCodeConstants.OAUTH2_CLIENT_DISABLE);
        }

        // 校验客户端密钥
        if (StrUtil.isNotEmpty(clientSecret) && ObjectUtil.notEqual(client.getClientSecret(), clientSecret)) {
            throw new WebApiException(ErrorCodeConstants.OAUTH2_CLIENT_CLIENT_SECRET_ERROR);
        }
        // 校验授权方式
        if (StrUtil.isNotEmpty(authorizedGrantType) && !CollUtil.contains(client.getAuthorizedGrantTypes(), authorizedGrantType)) {
            throw new WebApiException(ErrorCodeConstants.OAUTH2_CLIENT_AUTHORIZED_GRANT_TYPE_NOT_EXISTS);
        }
        // 校验授权范围
        if (CollUtil.isNotEmpty(scopes) && !CollUtil.containsAll(client.getScopes(), scopes)) {
            throw new WebApiException(ErrorCodeConstants.OAUTH2_CLIENT_SCOPE_OVER);
        }
        // 校验回调地址
        List<String> redirectUris = client.getRedirectUris();
        String[] redirectUrisArr = redirectUris.toArray(new String[redirectUris.size()]);
        if (StrUtil.isNotEmpty(redirectUri) && !StrUtil.startWithAny(redirectUri, redirectUrisArr)) {
            throw new WebApiException(StrUtil.format(ErrorCodeConstants.OAUTH2_CLIENT_REDIRECT_URI_NOT_MATCH.getMsg(), redirectUri));
        }
        /**
         * 默认支持加密方式如下：
         * <p>
         * {noop}密码明文
         * {加密特征码}密码密文
         * <p>
         * PasswordEncoder 会自动根据特征码匹配对应的加密算法，所以上一步⑧ 查询用户对象组装成  UserDetails 需要特殊处理
         */
//        client.setClientSecret(String.format("{noop}%s", client.getClientSecret()));
        return client;
    }
}
