
package com.neo.tiny.oauth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.oauth.entity.SysOauth2CodeDO;

import java.util.List;

/**
 * @Description: OAuth2 授权码
 * @Author: yqz
 * @CreateDate: 2022-11-11 10:44:38
 */
public interface SysOauth2CodeService extends IService<SysOauth2CodeDO> {
    /**
     * 使用授权码
     *
     * @param code 授权码
     * @return code实体
     */

    SysOauth2CodeDO consumeAuthorizationCode(String code);

    /**
     * 创建授权码
     * <p>
     * 参考 JdbcAuthorizationCodeServices 的 createAuthorizationCode 方法
     *
     * @param userId      用户编号
     * @param userName    用户名
     * @param userType    用户类型
     * @param clientId    客户端编号
     * @param scopes      授权范围
     * @param redirectUri 重定向 URI
     * @param state       状态
     * @return 授权码的信息
     */
    SysOauth2CodeDO createAuthorizationCode(Long userId, String userName, Integer userType, String clientId,
                                            List<String> scopes, String redirectUri, String state);
}
