package com.neo.tiny.oauth.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.oauth.entity.SysOauth2AccessTokenDO;
import com.neo.tiny.oauth.enums.LogoutScope;
import com.neo.tiny.oauth.service.Oauth2TokenService;
import com.neo.tiny.oauth.vo.token.Oauth2AccessTokenPageReqVO;
import com.neo.tiny.oauth.vo.token.Oauth2AccessTokenRespVO;
import com.neo.tiny.resolver.CommonPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/11/12 21:44
 */
@Api(tags = "OAuth2.0 令牌")
@RestController
@AllArgsConstructor
@RequestMapping("/open/oauth2-token")
public class AuthTokenDataController {

    private final Oauth2TokenService oauth2TokenService;


    @GetMapping("/page")
    @ApiOperation(value = "获得访问令牌分页", notes = "只返回有效期内的")
    public ResResult<IPage<Oauth2AccessTokenRespVO>> page(@Valid Oauth2AccessTokenPageReqVO vo) {
        IPage<SysOauth2AccessTokenDO> accessTokenPage = oauth2TokenService.getAccessTokenPage(vo);

        return ResResult.success(CommonPage.restPage(accessTokenPage, Oauth2AccessTokenRespVO.class));
    }

    @DeleteMapping("/delete")
    @ApiOperation(value = "后台管理员删除访问令牌（强制退出）")
    @ApiImplicitParam(name = "accessToken", required = true, value = "访问令牌", example = "accessToken", dataTypeClass = String.class)
    public ResResult<Boolean> revokeToken(@RequestParam("accessToken") String accessToken) {
        // 删除访问令牌
        return ResResult.success(oauth2TokenService.removeAccessToken(accessToken, LogoutScope.ALl_CLIENT.getCode()) != null);
    }
}
