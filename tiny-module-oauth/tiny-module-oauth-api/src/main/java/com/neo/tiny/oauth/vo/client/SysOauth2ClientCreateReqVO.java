package com.neo.tiny.oauth.vo.client;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 *
 * @Description: 创建oauth
 * @author yqz
 * @date 2022-11-10 11:08:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "oauth")
public class SysOauth2ClientCreateReqVO extends SysOauth2ClientBaseVO implements Serializable {



}
