package com.neo.tiny.oauth.vo.client;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/**
 * @author yqz
 * @Description: oauth基础类
 * @date 2022-11-10 11:08:16
 */
@Data
@ApiModel(description = "oauth")
public class SysOauth2ClientBaseVO implements Serializable {

    @ApiModelProperty(value = "编号", required = true, example = "1024")
    private Long id;


    /**
     * 客户端编号
     */
    @ApiModelProperty("客户端编号")
    @NotEmpty(message = "客户端编号不能为空")
    private String clientId;

    /**
     * 客户端密钥
     */
    @ApiModelProperty("客户端密钥")
    @NotEmpty(message = "客户端密钥不能为空")
    private String clientSecret;

    /**
     * 应用名
     */
    @ApiModelProperty("应用名")
    @NotEmpty(message = "应用名不能为空")
    private String clientName;

    /**
     * 应用图标
     */
    @ApiModelProperty(value = "应用图标", example = "https://www.baidu.com")
    @NotEmpty(message = "应用图标不能为空")
    @URL(message = "应用图标的地址不正确")
    private String clientLogo;

    /**
     * 应用描述
     */
    @ApiModelProperty("应用描述")
    private String clientDescription;

    /**
     * 状态
     * {@link com.neo.tiny.common.enums.CommonStatusEnum}
     */
    @ApiModelProperty("状态")
    @NotNull(message = "状态不能为空")
    private Integer clientStatus;

    /**
     * 访问令牌的有效期
     */
    @ApiModelProperty("访问令牌的有效期")
    @NotNull(message = "访问令牌的有效期不能为空")
    private Integer accessTokenValiditySeconds;

    /**
     * 刷新令牌的有效期
     */
    @ApiModelProperty("刷新令牌的有效期")
    @NotNull(message = "刷新令牌的有效期不能为空")
    private Integer refreshTokenValiditySeconds;

    /**
     * 可重定向的 URI 地址
     */
    @ApiModelProperty("可重定向的 URI 地址")
    @NotNull(message = "可重定向的 URI 地址不能为空")
    private List<@NotEmpty(message = "重定向的 URI 不能为空") @URL(message = "重定向的 URI 格式不正确") String> redirectUris;

    /**
     * 授权类型
     * 枚举 {@link com.neo.tiny.oauth.enums.OAuth2GrantTypeEnum}
     */
    @ApiModelProperty(value = "授权类型", required = true, example = "password", notes = "参见 OAuth2GrantTypeEnum 枚举")
    @NotNull(message = "授权类型不能为空")
    private List<String> authorizedGrantTypes;

    /**
     * 授权范围
     */
    @ApiModelProperty(value = "授权范围", example = "user_info")
    private List<String> scopes;

    /**
     * 自动通过的授权范围
     */
    @ApiModelProperty(value = "自动通过的授权范围", example = "user_info")
    private List<String> autoApproveScopes;

    /**
     * 权限
     */
    @ApiModelProperty(value = "权限", example = "sys-user-query")
    private List<String> authorities;

    /**
     * 资源
     */
    @ApiModelProperty(value = "资源", example = "1024")
    private List<String> resourceIds;

    /**
     * 附加信息
     */
    @ApiModelProperty(value = "附加信息", example = "{\"name\": \"张三\"}")
    private String additionalInformation;


}
