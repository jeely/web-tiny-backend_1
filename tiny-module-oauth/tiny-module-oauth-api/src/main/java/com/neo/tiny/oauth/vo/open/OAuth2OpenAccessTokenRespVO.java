package com.neo.tiny.oauth.vo.open;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author yqz
 * @Description 管理后台 - 【开放接口】访问令牌 Response VO
 * @CreateDate 2022/11/11 9:06
 */
@ApiModel("管理后台 - 【开放接口】访问令牌 Response VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OAuth2OpenAccessTokenRespVO implements Serializable {

    @ApiModelProperty(value = "访问令牌", required = true, example = "tiny")
    private String accessToken;

    @ApiModelProperty(value = "刷新令牌", required = true, example = "nice")
    private String refreshToken;

    @ApiModelProperty(value = "令牌类型", required = true, example = "bearer")
    private String tokenType;

    @ApiModelProperty(value = "过期时间", required = true, example = "42430", notes = "单位：秒")
    private Long expiresIn;

    @ApiModelProperty(value = "授权范围", example = "user_info", notes = "如果多个授权范围，使用空格分隔")
    private String scope;

}
