package com.neo.tiny.oauth.vo.client;

import com.neo.tiny.common.common.PageParam;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @Description: oauth分页请求类
 * @author yqz
 * @date 2022-11-10 11:08:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "oauth")
public class SysOauth2ClientPageReqVO extends PageParam implements Serializable {


    /**
     * 应用名
     */
    @ApiModelProperty("应用名")
    private String clientName;


    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Integer clientStatus;

}
