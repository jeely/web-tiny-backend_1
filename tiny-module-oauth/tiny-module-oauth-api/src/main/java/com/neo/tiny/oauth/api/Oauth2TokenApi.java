package com.neo.tiny.oauth.api;

import com.neo.tiny.token.store.AccessTokenInfo;
import com.neo.tiny.token.store.OAuth2AccessToken;

import java.util.List;

/**
 * @Description: 供其他module调用使用的service实现
 * @Author: yqz
 * @CreateDate: 2022/12/10 23:06
 */
public interface Oauth2TokenApi {

    /**
     * 创建访问令牌
     * 注意：该流程中，会包含创建刷新令牌的创建
     * <p>
     * 参考 DefaultTokenServices 的 createAccessToken 方法
     *
     * @param userId   用户编号
     * @param userName 用户名
     * @param userType 用户类型
     * @param clientId 客户端编号
     * @param scopes   授权范围
     * @return 访问令牌的信息
     */
    AccessTokenInfo createAccessToken(Long userId, String userName, Integer userType, String clientId, List<String> scopes);

    /**
     * 移除访问令牌，供外部使用
     * 注意：该流程中，会移除相关的刷新令牌
     * <p>
     * 参考 DefaultTokenServices 的 revokeToken 方法
     *
     * @param accessToken 刷新令牌
     * @return 访问令牌的信息
     */
    AccessTokenInfo removeAccessToken(String accessToken);

    /**
     * 校验访问令牌
     *
     * @param accessToken 访问令牌
     * @return 访问令牌的信息
     */
    OAuth2AccessToken checkAccessToken(String accessToken);

}
