package com.neo.tiny.oauth.util;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.neo.tiny.common.exception.WebApiException;
import com.neo.tiny.common.util.HttpUtils;
import lombok.experimental.UtilityClass;

import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author yqz
 * @Description OAuth2 相关的工具类
 * @CreateDate 2022/11/11 9:15
 */
@UtilityClass
public class OAuth2Utils {
    public static final String AUTHORIZATION_BEARER = "Bearer";


    public List<String> buildScopes(String scope) {
        return StrUtil.split(scope, ' ');
    }

    public static String buildUnsuccessfulRedirect(String redirectUri, String responseType, String state,
                                                   String error, String description) {
        Map<String, String> query = new LinkedHashMap<String, String>();
        query.put("error", error);
        query.put("error_description", description);
        if (state != null) {
            query.put("state", state);
        }
        return HttpUtils.append(redirectUri, query, null, !responseType.contains("code"));
    }

    /**
     * 构建授权码模式下，重定向的 URI
     * <p>
     * copy from Spring Security OAuth2 的 AuthorizationEndpoint 类的 getSuccessfulRedirect 方法
     *
     * @param redirectUri       重定向 URI
     * @param authorizationCode 授权码
     * @param state             状态
     * @return 授权码模式下的重定向 URI
     */
    public static String buildAuthorizationCodeRedirectUri(String redirectUri, String authorizationCode, String state) {
        Map<String, String> query = new LinkedHashMap<>();
        query.put("code", authorizationCode);
        if (state != null) {
            query.put("state", state);
        }
        return HttpUtils.append(redirectUri, query, null);
    }

    /**
     * 构建简化模式下，重定向的 URI
     * <p>
     * copy from Spring Security OAuth2 的 AuthorizationEndpoint 类的 appendAccessToken 方法
     *
     * @param redirectUri           重定向 URI
     * @param accessToken           访问令牌
     * @param state                 状态
     * @param expireTime            过期时间
     * @param scopes                授权范围
     * @param additionalInformation 附加信息
     * @return 简化授权模式下的重定向 URI
     */
    public static String buildImplicitRedirectUri(String redirectUri, String accessToken, String state, LocalDateTime expireTime,
                                                  Collection<String> scopes, Map<String, Object> additionalInformation) {
        Map<String, Object> vars = new LinkedHashMap<>();
        Map<String, String> keys = new HashMap<>();
        vars.put("access_token", accessToken);
        vars.put("token_type", AUTHORIZATION_BEARER.toLowerCase());
        if (state != null) {
            vars.put("state", state);
        }
        if (expireTime != null) {
            vars.put("expires_in", getExpiresIn(expireTime));
        }
        if (CollUtil.isNotEmpty(scopes)) {
            vars.put("scope", buildScopeStr(scopes));
        }
        if (CollUtil.isNotEmpty(additionalInformation)) {
            for (String key : additionalInformation.keySet()) {
                Object value = additionalInformation.get(key);
                if (value != null) {
                    keys.put("extra_" + key, key);
                    vars.put("extra_" + key, value);
                }
            }
        }
        // Do not include the refresh token (even if there is one)
        return HttpUtils.append(redirectUri, vars, keys, true);
    }

    public long getExpiresIn(LocalDateTime expireTime) {
        LocalDateTime now = LocalDateTime.now();
        Duration between = Duration.between(now, expireTime);
        return between.toMillis() / 1000;
    }

    public String buildScopeStr(Collection<String> scopes) {
        return CollUtil.join(scopes, " ");
    }


    public String[] obtainBasicAuthorization(HttpServletRequest request) {
        String[] clientIdAndSecret = HttpUtils.obtainBasicAuthorization(request);
        if (ArrayUtil.isEmpty(clientIdAndSecret) || clientIdAndSecret.length != 2) {
            throw new WebApiException("client_id未正确传递");
        }
        return clientIdAndSecret;
    }
}
