package com.neo.tiny.oauth.vo.token;

import com.neo.tiny.common.common.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/11/11 16:54
 */
@Data

@EqualsAndHashCode(callSuper = true)
public class Oauth2AccessTokenPageReqVO extends PageParam {

    @ApiModelProperty(value = "用户编号", required = true)
    private Long userId;

    @ApiModelProperty(value = "用户类型", required = true, notes = "参见 UserTypeEnum 枚举")
    private Integer userType;

    @ApiModelProperty(value = "客户端编号", required = true)
    private String clientId;
}
