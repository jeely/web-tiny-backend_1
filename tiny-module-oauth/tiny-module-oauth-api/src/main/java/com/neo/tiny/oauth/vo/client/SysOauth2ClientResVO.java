package com.neo.tiny.oauth.vo.client;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author yqz
 * @Description: oauth响应类
 * @date 2022-11-10 11:08:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "oauth")
public class SysOauth2ClientResVO extends SysOauth2ClientBaseVO implements Serializable {

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;


}
