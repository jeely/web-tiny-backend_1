package com.neo.tiny.oauth.api;

import com.neo.tiny.oauth.vo.client.SysOauth2ClientBaseVO;

/**
 * @Description: 客户端接口
 * @Author: yqz
 * @CreateDate: 2022/12/13 20:01
 */
public interface Oauth2ClientApi {

    /**
     * 根据客户端id查询客户端信息
     *
     * @param clientId 客户端id
     * @return 客户端信息
     */
    SysOauth2ClientBaseVO loadClientDetailsByClientId(String clientId);
}
