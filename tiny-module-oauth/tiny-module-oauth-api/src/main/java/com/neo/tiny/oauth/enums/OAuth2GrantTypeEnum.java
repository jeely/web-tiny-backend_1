package com.neo.tiny.oauth.enums;

import cn.hutool.core.util.ArrayUtil;
import lombok.AllArgsConstructor;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/11/10 11:18
 */
@AllArgsConstructor
public enum OAuth2GrantTypeEnum {

    /**
     * 密码模式
     */
    PASSWORD("password"),
    /**
     * 授权码模式
     */
    AUTHORIZATION_CODE("authorization_code"),

    /**
     * 简化模式
     */
    IMPLICIT("implicit"),
    /**
     * 客户端模式
     */
    CLIENT_CREDENTIALS("client_credentials"),
    /**
     * 刷新模式
     */
    REFRESH_TOKEN("refresh_token"),
    ;

    private final String grantType;

    public String getGrantType() {
        return grantType;
    }

    public static OAuth2GrantTypeEnum getByGranType(String grantType) {
        return ArrayUtil.firstMatch(o -> o.getGrantType().equals(grantType), values());
    }

}
