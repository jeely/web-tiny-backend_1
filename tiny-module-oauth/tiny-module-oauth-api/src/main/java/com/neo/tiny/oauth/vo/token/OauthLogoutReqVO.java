package com.neo.tiny.oauth.vo.token;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 退出登录入参
 * @Author: yqz
 * @CreateDate: 2022/12/14 19:22
 */
@Data
public class OauthLogoutReqVO implements Serializable {

    @ApiModelProperty("退出token")
    private String accessToken;

    @ApiModelProperty("退出范围：0-本client，1-全部")
    private Integer logoutScope;
}
