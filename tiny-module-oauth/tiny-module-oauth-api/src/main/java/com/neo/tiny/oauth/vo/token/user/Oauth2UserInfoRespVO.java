package com.neo.tiny.oauth.vo.token.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/11/12 22:24
 */
@Data
public class Oauth2UserInfoRespVO {


    @ApiModelProperty(value = "用户编号", required = true)
    private Long userId;

    @ApiModelProperty(value = "用户账号", required = true)
    private String userName;

    @ApiModelProperty(value = "用户昵称", required = true)
    private String nickName;

    @ApiModelProperty(value = "用户邮箱")
    private String email;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "用户性别", example = "1")
    private Integer sex;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    /**
     * 所在部门
     */
    private Dept dept;

    /**
     * 所属岗位数组
     */
    private List<Post> posts;

    @ApiModel("部门")
    @Data
    public static class Dept {

        @ApiModelProperty(value = "部门编号", required = true, example = "1")
        private Long deptId;

        @ApiModelProperty(value = "部门名称", required = true, example = "研发部")
        private String deptName;

    }

    @ApiModel("岗位")
    @Data
    public static class Post {

        @ApiModelProperty(value = "岗位编号", required = true, example = "1")
        private Long postId;

        @ApiModelProperty(value = "岗位名称", required = true, example = "开发")
        private String postName;

    }

}
