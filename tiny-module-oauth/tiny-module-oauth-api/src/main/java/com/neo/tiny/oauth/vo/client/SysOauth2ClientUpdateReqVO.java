package com.neo.tiny.oauth.vo.client;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;


/**
 * @author yqzBpmFormUpdateReqVO.java
 * @Description: oauth更新请求类
 * @date 2022-11-10 11:08:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "oauth")
public class SysOauth2ClientUpdateReqVO extends SysOauth2ClientBaseVO implements Serializable {

    /**
     * 编号
     */
    @ApiModelProperty("编号")
    @NotNull(message = "编号不能为空")
    private Long id;

}
