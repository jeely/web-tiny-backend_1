package com.neo.tiny.oauth.enums;

import java.util.Arrays;

/**
 * @Description: 退出范围枚举
 * @Author: yqz
 * @CreateDate: 2022/12/14 21:50
 */
public enum LogoutScope {

    /**
     * 只退出当前client
     */
    SELF_CLIENT(0, "只退出当前client"),
    /**
     * 全平台退出
     */
    ALl_CLIENT(1, "全平台退出");

    LogoutScope(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 枚举值
     */
    private final Integer code;

    /**
     * 枚举释义
     */
    private final String desc;

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }


    public static LogoutScope matchByCode(Integer code) {

        return Arrays.stream(values()).filter(item -> item.getCode().equals(code))
                .findFirst().orElse(null);

    }

    /**
     * 是否为全平台退出
     *
     * @param code
     * @return
     */
    public static Boolean isAllClient(Integer code) {
        return ALl_CLIENT.getCode().equals(code);
    }
}
