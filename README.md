# 一、模块说明

```
└── web-tiny-backend
    ├── doc      --业务系统，目前集成排队取号，后续业务模块代码均集成到这里
    │   └── sql    --公共模块
    │   	├── bpm    --flowable工作流扩展表    
    │   	├── all    --全量sql
    │   	├── queue  --取叫号队列sql
    │   	├── report --报表sql
    │   	└── sys    --系统后台
    ├── tiny-business      --业务系统，目前集成排队取号，后续业务模块代码均集成到这里
    │   └── tiny-business-queue --排队取号
    ├── tiny-framework     --基础框架
    │   ├── tiny-common    --公共模块
    │   ├── tiny-mybatis   --orm配置
    │   ├── tiny-redis     --缓存配置
    │   ├── tiny-security  --安全配置
    │   ├── tiny-swagger   --在线文档配置
    │   └── tiny-work-flow --工作量流配置，目前仅实现flowable
    ├── tiny-module-bmp    --flowable工作流实现
    │   ├── pom.xml
    │   ├── tiny-module-bmp-api
    │   └── tiny-module-bmp-biz
    ├── tiny-module-codegen --代码生成
    ├── tiny-module-middleware --中间件
    │   ├── tiny-module-file    --文件服务
    │   ├── tiny-module-socket   --长连接服务
    │   ├── tiny-redis     --缓存配置
    │   ├── tiny-security  --安全配置
    │   ├── tiny-swagger   --在线文档配置
    │   └── tiny-work-flow --工作量流配置，目前仅实现flowable
    ├── tiny-module-oauth  --oauth实现
    ├── tiny-module-report --报表
    │   ├── tiny-module-report-jimu    --积木报表
    │   └── tiny-module-ureport --ureport报表
    ├── tiny-module-search         --elasticsearch实现高亮搜索
    │   ├── README.md
    │   ├── tiny-module-search-api
    │   └── tiny-module-search-biz
    ├── tiny-module-system -- 后台系统业务代码
    │   ├── tiny-module-system-api
    │   └── tiny-module-system-biz
    └── tiny-server -- 后台服务端

```
# 二、技术栈

## 2.1 后端

| 框架                                                         | 说明                  | 版本          | 学习指南                                                     |
| ------------------------------------------------------------ | --------------------- | ------------- | ------------------------------------------------------------ |
| [Spring Boot](https://spring.io/projects/spring-boot)        | 应用开发框架          | 2.3.4.RELEASE | [文档](https://github.com/YunaiV/SpringBoot-Labs)            |
| [MySQL](https://www.mysql.com/cn/)                           | 数据库服务器          | 5.7           |                                                              |
| [Druid](https://github.com/alibaba/druid)                    | JDBC 连接池、监控组件 | 1.2.9         | [文档](http://www.iocoder.cn/Spring-Boot/datasource-pool/?yudao) |
| [MyBatis Plus](https://mp.baomidou.com/)                     | MyBatis 增强工具包    | 3.5.2         | [文档](http://www.iocoder.cn/Spring-Boot/MyBatis/?yudao)     |
| [Redis](https://redis.io/)                                   | key-value 数据库      | 5.0           |                                                              |
| [Spring MVC](https://github.com/spring-projects/spring-framework/tree/master/spring-webmvc) | MVC 框架              | 5.2.9         | [文档](http://www.iocoder.cn/SpringMVC/MVC/?yudao)           |
| [Spring Security](https://github.com/spring-projects/spring-security) | Spring 安全框架       | 2.3.4.RELEASE | [文档](http://www.iocoder.cn/Spring-Boot/Spring-Security/?yudao) |
| [Flowable](https://github.com/flowable/flowable-engine)      | 工作流引擎            | 6.8.0         | [文档](https://doc.iocoder.cn/bpm/)                          |
| [Knife4j](https://gitee.com/xiaoym/knife4j)                  | Swagger 增强 UI 实现  | 2.0.4         | [文档](http://www.iocoder.cn/Spring-Boot/Swagger/?yudao)     |

## 2.2 前端

前端使用AVUE和elementUI组合，前端地址：https://gitee.com/coder-young-study/web-tiny-front.git



## mysql8修改密码

```sql
ALTER USER 'root'@'%' IDENTIFIED BY 'mysql1024.' PASSWORD EXPIRE NEVER;
ALTER USER 'root'@'localhost' IDENTIFIED BY 'mysql1024.';

修改mysql中自增长列的起始值
alter table table_name AUTO_INCREMENT = 0;
查看当前自增值
SELECT Auto_increment
FROM information_schema.`TABLES`
WHERE table_name = 'sys_menu'


```
