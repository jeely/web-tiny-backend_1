package com.neo.tiny.token.store;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/11/11 13:24
 */
public interface TokenStore {

    /**
     * 存储token
     *
     * @param token token
     */
    void storeAccessToken(OAuth2AccessToken token);

    /**
     * 删除token
     *
     * @param token token
     */
    void removeAccessToken(OAuth2AccessToken token);


    /**
     * 获取token
     *
     * @param accessToken accessToken
     * @return
     */
    OAuth2AccessToken getAccessToken(String accessToken);

    OAuth2AccessToken getAccessTokenWithUserInfo(String clientId, Integer userType, Long userId);
}
