package com.neo.tiny.token.store;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/11/11 13:25
 */
public interface OAuth2AccessToken {


    Long getUserId();

    String getUserName();

    Integer getUserType();

    String getAccessToken();

    String getRefreshToken();

    String getClientId();

    LocalDateTime getExpiresTime();

    List<String> getScopes();
}
