package com.neo.tiny.secrity.model;

import cn.hutool.core.util.ObjectUtil;
import com.neo.tiny.admin.dto.dept.SysDeptDTO;
import com.neo.tiny.admin.dto.user.SysUserDTO;
import com.neo.tiny.admin.vo.dept.SysDeptVO;
import com.neo.tiny.admin.vo.menu.SysMenuVO;
import com.neo.tiny.admin.vo.post.PostVO;
import com.neo.tiny.admin.vo.role.SysRoleVO;
import com.neo.tiny.common.enums.CommonStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/7 22:44
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminUserDetails implements UserDetails {

    /**
     * 系统用户信息
     */
    private SysUserDTO sysUser;

    /**
     * 菜单信息
     */
    private List<SysMenuVO> menuList;

    /**
     * 岗位信息
     */
    private List<PostVO> postList;


    /**
     * 角色列表
     */
    private List<SysRoleVO> roleList;

    /**
     * 部门
     */
    private SysDeptVO dept;


    /**
     * 权限信息
     */
    private Collection<GrantedAuthority> authorities;

    public AdminUserDetails(SysUserDTO sysUser, Collection<GrantedAuthority> authorities) {
        this.sysUser = sysUser;
        this.authorities = authorities;
    }

    public AdminUserDetails(SysUserDTO sysUser) {
        this.sysUser = sysUser;
    }

    public AdminUserDetails(SysUserDTO sysUser, List<SysMenuVO> menuList, List<PostVO> postList, Collection<GrantedAuthority> authorities) {
        this.sysUser = sysUser;
        this.menuList = menuList;
        this.postList = postList;
        this.authorities = authorities;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return sysUser.getPassword();
    }

    @Override
    public String getUsername() {
        return sysUser.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return ObjectUtil.equal(sysUser.getStatus(), CommonStatusEnum.ENABLE.getStatus());
    }
}
