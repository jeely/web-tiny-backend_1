package com.neo.tiny.secrity.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author yqz
 * @Description 认证白名单
 * @CreateDate 2023/6/13 14:24
 */
@Data
@Component
@ConfigurationProperties("security.ignore.api")
public class AdminSecurityIgnoreProperties {

    /**
     * 免认证列表
     */
    private List<String> ignoreUrls;

}
