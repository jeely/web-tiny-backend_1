package com.neo.tiny.secrity.service;

import com.neo.tiny.secrity.model.AdminUserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/11/11 13:09
 */
public interface AdminUserDetailsService extends UserDetailsService {

    /**
     * 查询用户详情
     *
     * @param username 用户名
     * @return 用户详情
     */
    AdminUserDetails getUserDetails(String username);
}
