package com.neo.tiny.secrity.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/12/13 19:59
 */
public interface ClientDetailsService extends UserDetailsService {
}
