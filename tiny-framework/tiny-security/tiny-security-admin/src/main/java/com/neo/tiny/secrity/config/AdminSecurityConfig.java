package com.neo.tiny.secrity.config;

import com.neo.tiny.config.BaseSecurityConfig;
import com.neo.tiny.secrity.filter.RedisAuthenticationTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.filter.OncePerRequestFilter;

import java.util.List;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/3 18:04
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AdminSecurityConfig extends BaseSecurityConfig {


    @Autowired
    private RedisAuthenticationTokenFilter authenticationTokenFilter;

    @Autowired
    private AdminSecurityIgnoreProperties properties;

    @Override
    public String[] getWhiteUri() {

        List<String> ignoreUrls = properties.getIgnoreUrls();
        return ignoreUrls.toArray(new String[0]);
    }

    @Override
    public OncePerRequestFilter getRequestFilter() {
        return authenticationTokenFilter;
    }
}
