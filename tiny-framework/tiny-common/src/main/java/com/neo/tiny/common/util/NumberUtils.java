package com.neo.tiny.common.util;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/10/12 22:05
 */
public class NumberUtils extends NumberUtil {

    /**
     * 数字的工具类，补全 {@link cn.hutool.core.util.NumberUtil} 的功能
     *
     * @param str
     * @return
     */
    public static Long parseUpperCaseLong(String str) {
        return StrUtil.isNotEmpty(str) ? Long.valueOf(str) : null;
    }
}
