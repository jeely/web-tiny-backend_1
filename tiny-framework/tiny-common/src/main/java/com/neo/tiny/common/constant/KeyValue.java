package com.neo.tiny.common.constant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yqz
 * @Description 键值对通用对象
 * @CreateDate 2022/11/14 12:25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyValue<K, V> {
    private K key;
    private V value;

}
