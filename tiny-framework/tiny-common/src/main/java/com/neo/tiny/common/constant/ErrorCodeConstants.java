package com.neo.tiny.common.constant;

import com.neo.tiny.common.common.ResErrorCodeConstant;

/**
 * @Description: 工作流 错误码枚举类
 * @Author: yqz
 * @CreateDate: 2022/9/21 23:26
 */
public interface ErrorCodeConstants {
    /**
     * ========== 岗位模块 1-002-005-000 ==========
     */
    ResErrorCodeConstant POST_NOT_FOUND = new ResErrorCodeConstant(1002005000, "当前岗位不存在");


    ResErrorCodeConstant USER_NOT_EXISTS = new ResErrorCodeConstant(1002003003, "用户不存在");


    /**
     * ========== 角色模块 1-002-002-000 ==========
     */
    ResErrorCodeConstant ROLE_NOT_EXISTS = new ResErrorCodeConstant(1002002000, "角色不存在");

    ResErrorCodeConstant DEPT_NOT_FOUND = new ResErrorCodeConstant(1002004002, "当前部门不存在");


    /**
     * ========== 字典数据 1-002-007-000 ==========
     */
    ResErrorCodeConstant DICT_DATA_NOT_EXISTS = new ResErrorCodeConstant(1002007001, "当前字典数据不存在");

    /**
     * ========== OAuth2 客户端 1-002-020-000 =========
     */
    ResErrorCodeConstant OAUTH2_CLIENT_NOT_EXISTS = new ResErrorCodeConstant(1002020000, "OAuth2 客户端不存在");
    ResErrorCodeConstant OAUTH2_CLIENT_EXISTS = new ResErrorCodeConstant(1002020001, "OAuth2 客户端编号已存在");
    ResErrorCodeConstant OAUTH2_CLIENT_DISABLE = new ResErrorCodeConstant(1002020002, "OAuth2 客户端已禁用");
    ResErrorCodeConstant OAUTH2_CLIENT_AUTHORIZED_GRANT_TYPE_NOT_EXISTS = new ResErrorCodeConstant(1002020003, "不支持该授权类型");
    ResErrorCodeConstant OAUTH2_CLIENT_SCOPE_OVER = new ResErrorCodeConstant(1002020004, "授权范围过大");
    ResErrorCodeConstant OAUTH2_CLIENT_REDIRECT_URI_NOT_MATCH = new ResErrorCodeConstant(1002020005, "无效 redirect_uri: {}");
    ResErrorCodeConstant OAUTH2_CLIENT_CLIENT_SECRET_ERROR = new ResErrorCodeConstant(1002020006, "无效 client_secret: {}");


    /**
     * ========== OAuth2 授权 1002021000 =========
     */
    ResErrorCodeConstant OAUTH2_GRANT_CLIENT_ID_MISMATCH = new ResErrorCodeConstant(1002021000, "client_id 不匹配");
    ResErrorCodeConstant OAUTH2_GRANT_REDIRECT_URI_MISMATCH = new ResErrorCodeConstant(1002021001, "redirect_uri 不匹配");
    ResErrorCodeConstant OAUTH2_GRANT_STATE_MISMATCH = new ResErrorCodeConstant(1002021002, "state 不匹配");
    ResErrorCodeConstant OAUTH2_GRANT_CODE_NOT_EXISTS = new ResErrorCodeConstant(1002021003, "code 不存在");

    /**
     * ========== OAuth2 授权 1002022000 =========
     */
    ResErrorCodeConstant OAUTH2_CODE_NOT_EXISTS = new ResErrorCodeConstant(1002022000, "code 不存在");
    ResErrorCodeConstant OAUTH2_CODE_EXPIRE = new ResErrorCodeConstant(1002022000, "code 已过期");

    /**
     * ========== OA 流程模块 1-009-001-000 ==========
     */
    ResErrorCodeConstant OA_LEAVE_NOT_EXISTS = new ResErrorCodeConstant(1009001001, "请假申请不存在");


    /**
     * ========== 流程模型 1-009-002-000 ==========
     */
    ResErrorCodeConstant MODEL_KEY_EXISTS = new ResErrorCodeConstant(1009002000, "已经存在流程标识为【{}】的流程");
    ResErrorCodeConstant MODEL_NOT_EXISTS = new ResErrorCodeConstant(1009002001, "流程模型不存在");
    ResErrorCodeConstant MODEL_KEY_VALID = new ResErrorCodeConstant(1009002002, "流程标识格式不正确，需要以字母或下划线开头，后接任意字母、数字、中划线、下划线、句点！");
    ResErrorCodeConstant MODEL_DEPLOY_FAIL_FORM_NOT_CONFIG = new ResErrorCodeConstant(1009002003, "部署流程失败，原因：流程表单未配置，请点击【修改流程】按钮进行配置");
    ResErrorCodeConstant MODEL_DEPLOY_FAIL_TASK_ASSIGN_RULE_NOT_CONFIG = new ResErrorCodeConstant(1009002004, "部署流程失败，" +
            "原因：用户任务({})未配置分配规则，请点击【修改流程】按钮进行配置");
    ResErrorCodeConstant MODEL_DEPLOY_FAIL_TASK_INFO_EQUALS = new ResErrorCodeConstant(1009003005, "流程定义部署失败，原因：信息未发生变化");


    /**
     * ========== 动态表单模块 1-009-010-000 ==========
     */
    ResErrorCodeConstant FORM_NOT_EXISTS = new ResErrorCodeConstant(1009010000, "动态表单不存在");

    ResErrorCodeConstant FORM_FIELD_REPEAT = new ResErrorCodeConstant(1009010001, "表单项({}) 和 ({}) 使用了相同的字段名({})");


    /**
     * ========== 用户组模块 1-009-011-000 ==========
     */

    ResErrorCodeConstant USER_GROUP_NOT_EXISTS = new ResErrorCodeConstant(1009011000, "用户组不存在");


    /**
     * ========== 流程定义 1-009-003-000 ==========
     */
    ResErrorCodeConstant PROCESS_DEFINITION_KEY_NOT_MATCH = new ResErrorCodeConstant(1009003000, "流程定义的标识期望是({})，当前是({})，请修改 BPMN 流程图");
    ResErrorCodeConstant PROCESS_DEFINITION_NAME_NOT_MATCH = new ResErrorCodeConstant(1009003001, "流程定义的名字期望是({})，当前是({})，请修改 BPMN 流程图");
    ResErrorCodeConstant PROCESS_DEFINITION_NOT_EXISTS = new ResErrorCodeConstant(1009003002, "流程定义不存在");
    ResErrorCodeConstant PROCESS_DEFINITION_IS_SUSPENDED = new ResErrorCodeConstant(1009003003, "流程定义处于挂起状态");


    /**
     * ========== 流程实例 1-009-004-000 ==========
     */
    ResErrorCodeConstant PROCESS_INSTANCE_NOT_EXISTS = new ResErrorCodeConstant(1009004000, "流程实例不存在");

    /**
     * ========== 流程任务 1-009-005-000 ==========
     */
    ResErrorCodeConstant TASK_COMPLETE_FAIL_NOT_EXISTS = new ResErrorCodeConstant(1009005000, "审批任务失败，原因：该任务不处于未审批");
    ResErrorCodeConstant TASK_COMPLETE_FAIL_ASSIGN_NOT_SELF = new ResErrorCodeConstant(1009005001, "审批任务失败，原因：该任务的审批人不是你");


    /**
     * ========== 流程任务分配规则 1-009-006-000 ==========
     */
    ResErrorCodeConstant TASK_ASSIGN_RULE_EXISTS = new ResErrorCodeConstant(1009006000, "流程({}) 的任务({}) 已经存在分配规则");

    ResErrorCodeConstant TASK_ASSIGN_RULE_NOT_EXISTS = new ResErrorCodeConstant(1009006001, "流程任务分配规则不存在");
    ResErrorCodeConstant TASK_UPDATE_FAIL_NOT_MODEL = new ResErrorCodeConstant(1009006002, "只有流程模型的任务分配规则，才允许被修改");
    ResErrorCodeConstant TASK_CREATE_FAIL_NO_CANDIDATE_USER = new ResErrorCodeConstant(1009006003, "操作失败，原因：找不到任务的审批人！");
    ResErrorCodeConstant TASK_ASSIGN_SCRIPT_NOT_EXISTS = new ResErrorCodeConstant(1009006004, "操作失败，原因：任务分配脚本({}) 不存在");


    /**
     * 验证码
     */
    ResErrorCodeConstant LOGIN_LOGIN_ERROR = new ResErrorCodeConstant(500, "验证码有误或已过期");

    /**
     * 用户手机号不存在
     */
    ResErrorCodeConstant USER_PHONE_NOT_EXIST = new ResErrorCodeConstant(500, "用户手机号不存在");
    ResErrorCodeConstant USER_PHONE_LOGIN_ERROR = new ResErrorCodeConstant(500, "验证码有误或已过期");


}
