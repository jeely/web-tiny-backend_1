package com.neo.tiny.common.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/9/15 22:38
 */
@ApiModel("es分页参数")
@Data
public class EsPageParam implements Serializable {

    private static final Integer PAGE_NO = 0;
    private static final Integer PAGE_SIZE = 10;

    @ApiModelProperty(value = "页码，从 0 开始，", required = true, example = "0")
    @NotNull(message = "页码不能为空")
    @Min(value = 0, message = "页码最小值为 1")
    private Integer current = PAGE_NO;

    @ApiModelProperty(value = "每页条数，最大值为 100", required = true, example = "10")
    @NotNull(message = "每页条数不能为空")
    @Min(value = 1, message = "每页条数最小值为 1")
    @Max(value = 100, message = "每页条数最大值为 100")
    private Integer size = PAGE_SIZE;
}
