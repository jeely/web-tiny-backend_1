package com.neo.tiny.common.constant;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 17:17
 */
public interface CommonConstants {

    /**
     * 菜单树根节点
     */
    Long MENU_TREE_ROOT_ID = -1L;

    /**
     * 取号数据自增key前缀
     */
    String TICKET_INCR = "ticket_incr";
    String QUESTION_MARK = "?";

}
