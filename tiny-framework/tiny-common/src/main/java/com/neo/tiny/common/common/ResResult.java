package com.neo.tiny.common.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/3 18:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResResult<T> implements Serializable {
    private long code;
    private String msg;
    private T data;


    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> ResResult<T> success(T data) {
        return new ResResult<T>(ResCode.SUCCESS.getCode(), ResCode.SUCCESS.getMsg(), data);
    }

    /**
     * 成功返回结果
     *
     * @param data    获取的数据
     * @param message 提示信息
     */
    public static <T> ResResult<T> success(T data, String message) {
        return new ResResult<T>(ResCode.SUCCESS.getCode(), message, data);
    }




    /**
     * 失败返回结果
     *
     * @param msg 提示信息
     */
    public static <T> ResResult<T> failed(String msg) {
        return new ResResult<T>(ResCode.FAILED.getCode(), msg, null);
    }

    /**
     * 失败返回结果
     *
     * @param msg 提示信息
     */
    public static <T> ResResult<T> failed(long code, String msg) {
        return new ResResult<T>(code, msg, null);
    }



    /**
     * 参数验证失败返回结果
     */
    public static <T> ResResult<T> validateFailed() {
        return failed(ResCode.VALIDATE_FAILED.getMsg());
    }

    /**
     * 参数验证失败返回结果
     *
     * @param msg 提示信息
     */
    public static <T> ResResult<T> validateFailed(String msg) {
        return new ResResult<T>(ResCode.VALIDATE_FAILED.getCode(), msg, null);
    }

    /**
     * 未登录返回结果
     */
    public static <T> ResResult<T> unauthorized(T data) {
        return new ResResult<T>(ResCode.UNAUTHORIZED.getCode(), ResCode.UNAUTHORIZED.getMsg(), data);
    }

    /**
     * 未授权返回结果
     */
    public static <T> ResResult<T> forbidden(T data) {
        return new ResResult<T>(ResCode.FORBIDDEN.getCode(), ResCode.FORBIDDEN.getMsg(), data);
    }
}
