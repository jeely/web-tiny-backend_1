package com.neo.tiny.common.util;

import cn.hutool.core.bean.BeanUtil;
import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/9/4 11:44
 */
@UtilityClass
public class CommonDoTransfer {


    /**
     * 实体类list集合转为vo
     *
     * @param dos     数据库实体类集合
     * @param voClass vo的class
     * @param <DO>    范型约束
     * @param <VO>    范型约束
     * @return 转换后的数据
     */
    public <DO, VO> List<VO> transfer(List<DO> dos, Class<VO> voClass) {

        if (null == dos) {
            return null;
        }

        return dos.stream()
//                .map(item -> BeanUtil.copyProperties(item, voClass))
                .map(item -> transfer(item, voClass))
                .collect(Collectors.toList());
    }


    /**
     * 数据库实体类与vo转换
     *
     * @param entityDo 数据库实体类
     * @param clazz    vo的class
     * @param <DO>     范型约束
     * @param <VO>     范型约束
     * @return VO
     */
    public <DO, VO> VO transfer(DO entityDo, Class<VO> clazz) {

        if (null == entityDo) {
            return null;
        }
        return BeanUtil.copyProperties(entityDo, clazz);
    }
}
