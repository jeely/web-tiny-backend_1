package com.neo.tiny.common.exception;

import com.neo.tiny.common.common.ResErrorCode;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/7 14:13
 */
public class WebApiException extends RuntimeException {
    private ResErrorCode errorCode;

    public WebApiException(ResErrorCode errorCode) {
        super(errorCode.getMsg());
        this.errorCode = errorCode;
    }

    public WebApiException(String msg) {
        super(msg);
    }

    public WebApiException(Throwable cause) {
        super(cause);
    }

    public WebApiException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public ResErrorCode getErrorCode() {
        return errorCode;
    }
}
