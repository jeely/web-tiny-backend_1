package com.neo.tiny.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author yqz
 * @Description 数据范围枚举类
 * <p>
 * 用于实现数据级别的权限
 * @CreateDate 2023/12/14 17:52
 */
@Getter
@AllArgsConstructor
public enum DataScopeEnum {

    ALL(1, "全部数据权限"),

    DEPT_CUSTOM(2, "指定部门数据权限"),
    DEPT_ONLY(3, "部门数据权限"),
    DEPT_AND_CHILD(4, "部门及以下数据权限"),

    SELF(5, "仅本人数据权限");

    /**
     * 范围
     */
    private final Integer scope;

    private final String desc;

}
