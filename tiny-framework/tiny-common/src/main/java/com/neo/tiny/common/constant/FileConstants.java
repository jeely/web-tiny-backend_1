package com.neo.tiny.common.constant;

/**
 * @Description: 文件存储相关配置
 * @Author: yqz
 * @CreateDate: 2022/11/14 22:48
 */
public interface FileConstants {

    String STORAGE_CONFIG = "STORAGE_CONFIG";

}
