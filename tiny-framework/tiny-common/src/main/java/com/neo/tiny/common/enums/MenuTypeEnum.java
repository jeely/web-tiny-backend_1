package com.neo.tiny.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 16:34
 */
@Getter
@RequiredArgsConstructor
public enum MenuTypeEnum {


    /**
     * 目录
     */
    DIR_MENU("M", "directory"),


    /**
     * 左侧菜单
     */
    LEFT_MENU("C", "left"),

    /**
     * 按钮
     */
    BUTTON("F", "button"),

    /**
     * 顶部菜单
     */
    TOP_MENU("T", "top");

    /**
     * 类型
     */
    private final String type;

    /**
     * 描述
     */
    private final String description;

}
