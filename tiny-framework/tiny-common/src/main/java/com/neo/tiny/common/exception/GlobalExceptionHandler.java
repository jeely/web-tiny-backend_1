package com.neo.tiny.common.exception;

import com.neo.tiny.common.common.ResResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/7 14:12
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @ResponseBody
    @ExceptionHandler(value = WebApiException.class)
    public ResResult handle(WebApiException e) {
        logger.error("WebApiException异常信息：{}", e.getMessage());
        if (e.getErrorCode() != null) {
            return ResResult.failed(e.getErrorCode().getCode(), e.getErrorCode().getMsg());
        }
        return ResResult.failed(e.getMessage());
    }


    @ResponseBody
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResResult handleValidException(MethodArgumentNotValidException e) {
        logger.error("MethodArgumentNotValidException异常信息：{}", e.getMessage());
        BindingResult bindingResult = e.getBindingResult();
        String message = null;
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null) {
                message = fieldError.getField() + fieldError.getDefaultMessage();
            }
        }
        return ResResult.failed(message);
    }

    @ResponseBody
    @ExceptionHandler(value = BindException.class)
    public ResResult handleValidException(BindException e) {
        logger.error("BindException异常信息：{}", e.getMessage());
        BindingResult bindingResult = e.getBindingResult();
        String message = null;
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null) {
                message = fieldError.getField() + fieldError.getDefaultMessage();
            }
        }
        return ResResult.validateFailed(message);
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ResResult Exception(Exception e) {
        logger.error("Exception异常信息：{}", e.getMessage());
        logger.error("Exception异常信息", e);
        e.printStackTrace();
        String message = e.getMessage();
        return ResResult.validateFailed(message);
    }
}
