package com.neo.tiny.common.util;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.StrUtil;
import com.neo.tiny.common.constant.CommonConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Map;

/**
 * @author yqz
 * @Description HTTP 工具类
 * @CreateDate 2022/11/11 9:26
 */
@Slf4j
public class HttpUtils {

    public static String[] obtainBasicAuthorization(HttpServletRequest request) {
        String clientId;
        String clientSecret;
        // 先从 Header 中获取
        String authorization = request.getHeader("Authorization");
        authorization = StrUtil.subAfter(authorization, "Basic ", true);
        if (StringUtils.hasText(authorization)) {
            authorization = Base64.decodeStr(authorization);
            clientId = StrUtil.subBefore(authorization, ":", false);
            clientSecret = StrUtil.subAfter(authorization, ":", false);
            // 再从 Param 中获取
        } else {
            clientId = request.getParameter("client_id");
            clientSecret = request.getParameter("client_secret");
        }

        // 如果两者非空，则返回
        if (StrUtil.isNotEmpty(clientId) && StrUtil.isNotEmpty(clientSecret)) {
            return new String[]{clientId, clientSecret};
        }
        return null;
    }

    /**
     * 拼接 URL
     * <p>
     * copy from Spring Security OAuth2 的 AuthorizationEndpoint 类的 append 方法
     *
     * @param base     基础 URL
     * @param query    查询参数
     * @param keys     query 的 key，对应的原本的 key 的映射。例如说 query 里有个 key 是 xx，实际它的 key 是 extra_xx，则通过 keys 里添加这个映射
     * @param fragment URL 的 fragment，即拼接到 # 中
     * @return 拼接后的 URL
     */
    public static String append(String base, Map<String, ?> query, Map<String, String> keys, boolean fragment) {
        try {
            base = URLEncoder.encode(base, "UTF-8");

            UriComponentsBuilder template = UriComponentsBuilder.newInstance();
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(base);
            URI redirectUri;
            try {
                // assume it's encoded to start with (if it came in over the wire)
                redirectUri = builder.build(true).toUri();
            } catch (Exception e) {
                // ... but allow client registrations to contain hard-coded non-encoded values
                redirectUri = builder.build().toUri();
                builder = UriComponentsBuilder.fromUri(redirectUri);
            }
            template.scheme(redirectUri.getScheme()).port(redirectUri.getPort()).host(redirectUri.getHost())
                    .userInfo(redirectUri.getUserInfo()).path(redirectUri.getPath());

            if (fragment) {
                StringBuilder values = new StringBuilder();
                if (redirectUri.getFragment() != null) {
                    String append = redirectUri.getFragment();
                    values.append(append);
                }
                for (String key : query.keySet()) {
                    if (values.length() > 0) {
                        values.append("&");
                    }
                    String name = key;
                    if (keys != null && keys.containsKey(key)) {
                        name = keys.get(key);
                    }
                    values.append(name).append("={").append(key).append("}");
                }
                if (values.length() > 0) {
                    template.fragment(values.toString());
                }
                UriComponents encoded = template.build().expand(query).encode();
                builder.fragment(encoded.getFragment());
            } else {
                for (String key : query.keySet()) {
                    String name = key;
                    if (keys != null && keys.containsKey(key)) {
                        name = keys.get(key);
                    }
                    template.queryParam(name, "{" + key + "}");
                }
                template.fragment(redirectUri.getFragment());
                UriComponents encoded = template.build().expand(query).encode();
                builder.query(encoded.getQuery());
            }
            return URLDecoder.decode(builder.build().toUriString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("URL编码失败");
            throw new RuntimeException(e);
        }
    }

    /**
     * 拼接 URL
     * <p>
     * copy from Spring Security OAuth2 的 AuthorizationEndpoint 类的 append 方法
     *
     * @param base  基础 URL
     * @param query 查询参数
     * @param keys  query 的 key，对应的原本的 key 的映射。例如说 query 里有个 key 是 xx，实际它的 key 是 extra_xx，则通过 keys 里添加这个映射
     * @return 拼接后的 URL
     */
    public static String append(String base, Map<String, ?> query, Map<String, String> keys) {
        UriComponentsBuilder template = UriComponentsBuilder.newInstance();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(base);
        URI redirectUri;
        try {
            // assume it's encoded to start with (if it came in over the wire)
            redirectUri = builder.build(true).toUri();
        } catch (Exception e) {
            // ... but allow client registrations to contain hard-coded non-encoded values
            redirectUri = builder.build().toUri();
            builder = UriComponentsBuilder.fromUri(redirectUri);
        }
        template.scheme(redirectUri.getScheme()).port(redirectUri.getPort()).host(redirectUri.getHost())
                .userInfo(redirectUri.getUserInfo()).path(redirectUri.getPath());

        // 判断路径中是否有 #有的话直接替换整个fragment，没有的话补充query参数
        String fragment = redirectUri.getFragment();
        if (StrUtil.isNotBlank(fragment)) {
            StringBuilder values = new StringBuilder();
            values.append(fragment);

            if (!fragment.contains(CommonConstants.QUESTION_MARK)) {
                values.append("?");
            }
            for (String key : query.keySet()) {
                if (fragment.indexOf(CommonConstants.QUESTION_MARK) > 0) {
                    values.append("&");
                }
                String name = key;
                if (keys != null && keys.containsKey(key)) {
                    name = keys.get(key);
                }
                values.append(name).append("={").append(key).append("}");
            }
            if (values.length() > 0) {
                template.fragment(values.toString());
            }
            UriComponents encoded = template.build().expand(query).encode();
            builder.fragment(encoded.getFragment());
        } else {
            for (String key : query.keySet()) {
                String name = key;
                if (keys != null && keys.containsKey(key)) {
                    name = keys.get(key);
                }
                template.queryParam(name, "{" + key + "}");
            }
            template.fragment(redirectUri.getFragment());
            UriComponents encoded = template.build().expand(query).encode();
            builder.query(encoded.getQuery());
        }
        return builder.build().toUriString();
    }
}
