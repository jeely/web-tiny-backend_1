package com.neo.tiny.common.enums;

/**
 * @Description: Spring Security Filter 默认为 -100，
 * 可见 org.springframework.boot.autoconfigure.security.SecurityProperties 配置属性类
 * @Author: yqz
 * @CreateDate: 2022/9/5 23:06
 */
public interface WebFilterOrderEnum {


    /**
     * 需要保证在 Spring Security 过滤后面
     */
    int FLOWABLE_FILTER = -98;
}
