package com.neo.tiny.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * @author yqz
 * @Description 全局用户类型枚举
 * @CreateDate 2022/11/11 13:20
 */
@AllArgsConstructor
@Getter
public enum UserTypeEnum {
    /**
     * 面向 c 端，普通用户
     */
    MEMBER(1, "会员"),
    /**
     * 面向 b 端，管理后台
     */
    ADMIN(2, "管理员"),

    /**
     * 客户端 {@link com.neo.tiny.oauth.enums.OAuth2GrantTypeEnum#CLIENT_CREDENTIALS}
     */
    CLIENT(3, "客户端");

    /**
     * 类型
     */
    private final Integer value;
    /**
     * 类型名
     */
    private final String name;

    public static UserTypeEnum match(Integer value) {
        return Arrays.stream(values())
                .filter(item -> item.getValue().equals(value))
                .findFirst()
                .orElse(null);
    }

    public static Boolean isClient(Integer value) {
        return CLIENT.getValue().equals(value);
    }

}
