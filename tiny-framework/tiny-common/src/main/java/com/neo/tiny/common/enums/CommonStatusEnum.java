package com.neo.tiny.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/10/4 19:32
 */
@Getter
@AllArgsConstructor
public enum CommonStatusEnum {

    /**
     * 开启
     */
    DISABLE("0", "关闭"),
    /**
     * 关闭
     */
    ENABLE("1", "开启");


    /**
     * 状态值
     */
    private final String status;
    /**
     * 状态名
     */
    private final String name;

    /**
     * 判断是否为开启
     *
     * @param status 状态值
     * @return
     */
    public static Boolean isEnable(Integer status) {
        return ENABLE.getStatus().equals(status);
    }
}
