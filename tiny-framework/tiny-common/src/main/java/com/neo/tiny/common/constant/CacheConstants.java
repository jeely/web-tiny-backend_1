package com.neo.tiny.common.constant;

/**
 * @Description: 缓存相关常量
 * @Author: yqz
 * @CreateDate: 2022/8/13 22:19
 */
public interface CacheConstants {
    /**
     * 菜单信息缓存
     */
    String MENU_DETAILS = "menu_details";

    /**
     * 用户信息缓存
     */
    String USER_DETAILS = "user_details";

    String CACHE_PATTERN_PREFIX = "*";

    /**
     * 账户密码登录验证码
     */
    String LOGIN_IMAGE_CODE = "login_image_code";

    /**
     * 短信验证码登录
     */
    String SMS_LOGIN_CODE = "sms_login";
}
