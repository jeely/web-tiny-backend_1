package com.neo.tiny.common.common;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/9/21 23:39
 */
public class ResErrorCodeConstant implements ResErrorCode{

    /**
     * 错误码
     */
    private final Integer code;
    /**
     * 错误提示
     */
    private final String msg;

    public ResErrorCodeConstant(Integer code, String message) {
        this.code = code;
        this.msg = message;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
