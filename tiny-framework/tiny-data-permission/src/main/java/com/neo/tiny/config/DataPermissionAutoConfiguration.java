package com.neo.tiny.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.neo.tiny.aop.DataPermissionAnnotationAdvisor;
import com.neo.tiny.interceptor.DataPermissionDatabaseInterceptor;
import com.neo.tiny.rule.DataPermissionRule;
import com.neo.tiny.rule.DataPermissionRuleFactory;
import com.neo.tiny.rule.DataPermissionRuleFactoryImpl;
import com.neo.tiny.util.MyBatisUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author yqz
 * @Description 数据权限的自动配置类
 * @CreateDate 2023/12/14 15:37
 */
@Configuration
public class DataPermissionAutoConfiguration {
    @Bean
    public DataPermissionRuleFactory dataPermissionRuleFactory(List<DataPermissionRule> rules) {
        return new DataPermissionRuleFactoryImpl(rules);
    }

    @Bean
    public DataPermissionDatabaseInterceptor dataPermissionDatabaseInterceptor(MybatisPlusInterceptor interceptor,
                                                                               DataPermissionRuleFactory ruleFactory) {
        // 创建 DataPermissionDatabaseInterceptor 拦截器
        DataPermissionDatabaseInterceptor inner = new DataPermissionDatabaseInterceptor(ruleFactory);
        // 添加到 interceptor 中
        // 需要加在首个，主要是为了在分页插件前面。这个是 MyBatis Plus 的规定
        MyBatisUtils.addInterceptor(interceptor, inner, 0);
        return inner;
    }

    @Bean
    public DataPermissionAnnotationAdvisor dataPermissionAnnotationAdvisor() {
        return new DataPermissionAnnotationAdvisor();
    }
}
