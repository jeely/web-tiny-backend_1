package com.neo.tiny.config;

import com.neo.tiny.admin.api.permission.PermissionApi;
import com.neo.tiny.rule.DeptDataPermissionRule;
import com.neo.tiny.rule.DeptDataPermissionRuleCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author yqz
 * @Description 基于部门的数据权限 AutoConfiguration
 * @CreateDate 2023/12/14 15:59
 */
@Configuration
public class DeptDataPermissionAutoConfiguration {

    @Bean
    public DeptDataPermissionRule deptDataPermissionRule(PermissionApi permissionApi,
                                                         List<DeptDataPermissionRuleCustomizer> customizers) {
        // 创建 DeptDataPermissionRule 对象
        DeptDataPermissionRule rule = new DeptDataPermissionRule(permissionApi);
        // 补全表配置
        customizers.forEach(customizer -> customizer.customize(rule));
        return rule;
    }
}
