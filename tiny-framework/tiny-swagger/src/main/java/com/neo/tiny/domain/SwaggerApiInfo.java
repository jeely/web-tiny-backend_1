package com.neo.tiny.domain;

import com.google.common.base.Predicate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import springfox.documentation.RequestHandler;

/**
 * @author yqz
 * @Description Swagger自定义配置
 * @CreateDate 2022/8/3 16:56
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SwaggerApiInfo {

    /**
     * API文档生成基础路径
     */
    private Predicate<RequestHandler> predicate;
    /**
     * API文档生成基础路径
     */
    private String apiBasePackage;
    /**
     * 是否要启用登录认证
     */
    private boolean enableSecurity;
    /**
     * 文档标题
     */
    private String title;
    /**
     * 文档描述
     */
    private String description;
    /**
     * 文档版本
     */
    private String version;
    /**
     * 文档联系人姓名
     */
    private String contactName;
    /**
     * 文档联系人网址
     */
    private String contactUrl;
    /**
     * 文档联系人邮箱
     */
    private String contactEmail;

    /**
     * 分组信息
     */
    private String group;
}
