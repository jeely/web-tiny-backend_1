package com.neo.tiny.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author yqz
 * @Description Redis操作Service
 * @CreateDate 2022/8/3 17:03
 */
public interface RedisService {


    /**
     * 保存属性
     *
     * @param key      键
     * @param value    值
     * @param time     过期时间
     * @param timeUnit 时间单位
     */
    void set(String key, Object value, long time, TimeUnit timeUnit);

    /**
     * 保存属性
     *
     * @param key   键
     * @param value 值
     * @param time  过期时间
     */
    void set(String key, Object value, long time);


    /**
     * 保存属性
     *
     * @param key   键
     * @param value 值
     */
    void set(String key, Object value);

    /**
     * 获取属性
     *
     * @param key 键
     * @return
     */
    Object get(String key);

    /**
     * 获取属性，根据clazz返回响应体
     *
     * @param key   键
     * @param clazz 响应体类型
     * @return
     */
    <T> Optional<T> get(String key, Class<? extends T> clazz);


    /**
     * 删除属性
     *
     * @param key 键
     * @return
     */
    Boolean del(String key);


    /**
     * 批量删除属性
     *
     * @param keys 键集合
     * @return
     */
    Long del(Set<String> keys);

    /**
     * 根据如惨，模糊获取key集合
     *
     * @param pattern 模糊表达式
     * @return
     */
    Set<String> keys(String pattern);


    /**
     * 设置过期时间
     *
     * @param key  键
     * @param time 时间
     * @return
     */
    Boolean expire(String key, long time);


    /**
     * 获取过期时间
     *
     * @param key 键
     * @return
     */
    Long getExpire(String key);

    /**
     * 判断是否有该属性
     *
     * @param key 键
     * @return
     */
    Boolean hasKey(String key);

    /**
     * 按delta递增
     *
     * @param key   键
     * @param delta 步长
     * @return 当前值
     */
    Long incr(String key, long delta);


    /**
     * 按delta递减
     *
     * @param key   键
     * @param delta 步长
     * @return 当前值
     */
    Long decr(String key, long delta);

    /**
     *
     */
    /**
     * 获取Hash结构中的属性
     *
     * @param key     键
     * @param hashKey hash键
     * @return 结果
     */
    Object hGet(String key, String hashKey);


    /**
     * 向Hash结构中放入一个属性
     *
     * @param key     键
     * @param hashKey hash键
     * @param value   值
     * @param time    过期时间
     * @return 放入是否成功
     */
    Boolean hSet(String key, String hashKey, Object value, long time);


    /**
     * 向Hash结构中放入一个属性
     *
     * @param key     键
     * @param hashKey hash键
     * @param value   值
     */
    void hSet(String key, String hashKey, Object value);


    /**
     * 直接获取整个Hash结构
     *
     * @param key 键
     * @return 结果集
     */
    Map<Object, Object> hGetAll(String key);

    /**
     * 直接设置整个Hash结构
     *
     * @param key  键
     * @param map  值集合
     * @param time 过期时间
     * @return 放入结果
     */
    Boolean hSetAll(String key, Map<String, Object> map, long time);


    /**
     * 直接设置整个Hash结构
     *
     * @param key 键
     * @param map 值集
     */
    void hSetAll(String key, Map<String, ?> map);


    /**
     * 删除Hash结构中的属性
     *
     * @param key     键
     * @param hashKey 属性键
     */
    void hDel(String key, Object... hashKey);


    /**
     * 判断Hash结构中是否有该属性
     *
     * @param key     键
     * @param hashKey 属性键
     * @return
     */
    Boolean hHasKey(String key, String hashKey);


    /**
     * Hash结构中属性递增
     *
     * @param key     键
     * @param hashKey 属性键
     * @param delta   递增步长
     * @return 当前值
     */
    Long hIncr(String key, String hashKey, Long delta);


    /**
     * Hash结构中属性递减
     *
     * @param key     键
     * @param hashKey 属性键
     * @param delta   步长
     * @return 当前值
     */
    Long hDecr(String key, String hashKey, Long delta);

    /**
     * 获取Set结构
     *
     * @param key 键
     * @return Set结构
     */
    Set<Object> sMembers(String key);


    /**
     * 向Set结构中添加属性
     *
     * @param key    键
     * @param values 值集合
     * @return
     */
    Long sAdd(String key, Object... values);

    /**
     * 向Set结构中添加属性
     *
     * @param key    键
     * @param time   过期时间
     * @param values 值集合
     * @return
     */
    Long sAdd(String key, long time, Object... values);


    /**
     * 是否为Set中的属性
     *
     * @param key   键
     * @param value 值
     * @return
     */
    Boolean sIsMember(String key, Object value);


    /**
     * 获取Set结构的长度
     *
     * @param key 键
     * @return
     */
    Long sSize(String key);


    /**
     * 删除Set结构中的属性
     *
     * @param key    键
     * @param values 属性集合
     * @return
     */
    Long sRemove(String key, Object... values);


    /**
     * 获取List结构中的属性
     *
     * @param key   键
     * @param start 开始节点
     * @param end   结束节点
     * @return
     */
    List<Object> lRange(String key, long start, long end);


    /**
     * 获取List结构的长度
     *
     * @param key 键
     * @return
     */
    Long lSize(String key);

    /**
     * 根据索引获取List中的属性
     *
     * @param key   键
     * @param index 索引
     * @return
     */
    Object lIndex(String key, long index);


    /**
     * 向List结构中添加属性
     *
     * @param key   键
     * @param value 值
     * @return
     */
    Long lPush(String key, Object value);


    /**
     * 向List结构中添加属性
     *
     * @param key   键
     * @param value 值
     * @param time  过期时间
     * @return
     */
    Long lPush(String key, Object value, long time);


    /**
     * 向List结构中批量添加属性
     *
     * @param key    键
     * @param values 键集合
     * @return 结果
     */
    Long lPushAll(String key, Object... values);


    /**
     * 向List结构中批量添加属性
     *
     * @param key    键
     * @param time   过期时间
     * @param values 值集合
     * @return
     */
    Long lPushAll(String key, Long time, Object... values);


    /**
     * 从存储在键中的列表中删除等于值的元素的第一个计数事件。
     *
     * @param key   键
     * @param count count> 0：删除等于从左到右移动的值的第一个元素；
     *              count< 0：删除等于从右到左移动的值的第一个元素；
     *              count= 0：删除等于value的所有元素。
     * @param value 值
     * @return
     */
    Long lRemove(String key, long count, Object value);


    /**
     * 加锁
     *
     * @param key        键
     * @param value      值
     * @param expireTime 过期时间，秒
     * @return
     */
    Boolean getLock(String key, String value, long expireTime);

    /**
     * 释放锁
     **/
    Long releaseLock(String key, String value);
}
