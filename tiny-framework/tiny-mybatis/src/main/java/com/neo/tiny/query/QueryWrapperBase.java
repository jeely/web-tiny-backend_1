package com.neo.tiny.query;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Collection;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/9/25 23:16
 */
public class QueryWrapperBase<T> extends QueryWrapper<T> {

    public QueryWrapperBase<T> likeIfPresent(String column, String val) {
        if (StringUtils.hasText(val)) {
            return (QueryWrapperBase<T>) super.like(column, val);
        }
        return this;
    }

    public QueryWrapperBase<T> inIfPresent(String column, Collection<?> values) {
        if (!CollectionUtils.isEmpty(values)) {
            return (QueryWrapperBase<T>) super.in(column, values);
        }
        return this;
    }

    public QueryWrapperBase<T> inIfPresent(String column, Object... values) {
        if (!ArrayUtils.isEmpty(values)) {
            return (QueryWrapperBase<T>) super.in(column, values);
        }
        return this;
    }

    public QueryWrapperBase<T> eqIfPresent(String column, Object val) {
        if (ObjectUtils.isNotEmpty(val)) {
            return (QueryWrapperBase<T>) super.eq(column, val);
        }
        return this;
    }

    public QueryWrapperBase<T> neIfPresent(String column, Object val) {
        if (ObjectUtils.isNotEmpty(val)) {
            return (QueryWrapperBase<T>) super.ne(column, val);
        }
        return this;
    }

    public QueryWrapperBase<T> gtIfPresent(String column, Object val) {
        if (ObjectUtils.isNotEmpty(val)) {
            return (QueryWrapperBase<T>) super.gt(column, val);
        }
        return this;
    }

    public QueryWrapperBase<T> geIfPresent(String column, Object val) {
        if (ObjectUtils.isNotEmpty(val)) {
            return (QueryWrapperBase<T>) super.ge(column, val);
        }
        return this;
    }

    public QueryWrapperBase<T> ltIfPresent(String column, Object val) {
        if (ObjectUtils.isNotEmpty(val)) {
            return (QueryWrapperBase<T>) super.lt(column, val);
        }
        return this;
    }

    public QueryWrapperBase<T> leIfPresent(String column, Object val) {
        if (ObjectUtils.isNotEmpty(val)) {
            return (QueryWrapperBase<T>) super.le(column, val);
        }
        return this;
    }

    public QueryWrapperBase<T> betweenIfPresent(String column, Object val1, Object val2) {
        if (ObjectUtils.isNotEmpty(val1) && ObjectUtils.isNotEmpty(val2)) {
            return (QueryWrapperBase<T>) super.between(column, val1, val2);
        }
        if (ObjectUtils.isNotEmpty(val1)) {
            return (QueryWrapperBase<T>) ge(column, val1);
        }
        if (ObjectUtils.isNotEmpty(val2)) {
            return (QueryWrapperBase<T>) le(column, val2);
        }
        return this;
    }


    // ========== 重写父类方法，方便链式调用 ==========

    @Override
    public QueryWrapperBase<T> eq(boolean condition, String column, Object val) {
        super.eq(condition, column, val);
        return this;
    }

    @Override
    public QueryWrapperBase<T> eq(String column, Object val) {
        super.eq(column, val);
        return this;
    }

    @Override
    public QueryWrapperBase<T> orderByDesc(String column) {
        super.orderByDesc(true, column);
        return this;
    }

    @Override
    public QueryWrapperBase<T> last(String lastSql) {
        super.last(lastSql);
        return this;
    }

    @Override
    public QueryWrapperBase<T> in(String column, Collection<?> coll) {
        super.in(column, coll);
        return this;
    }

}
