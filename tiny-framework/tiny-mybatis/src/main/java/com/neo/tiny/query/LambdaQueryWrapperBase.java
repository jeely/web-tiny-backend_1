package com.neo.tiny.query;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ArrayUtils;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;

/**
 * @param <T> 数据类型
 * @Description: 拓展 MyBatis Plus QueryWrapper 类，主要增加如下功能：
 * <p>
 * 1. 拼接条件的方法，增加 xxxIfPresent 方法，用于判断值不存在的时候，不要拼接到条件中。
 * @Author: yqz
 * @CreateDate: 2022/9/25 23:15
 */
public class LambdaQueryWrapperBase<T> extends LambdaQueryWrapper<T> {

    public LambdaQueryWrapperBase<T> likeIfPresent(SFunction<T, ?> column, String val) {
        if (StringUtils.hasText(val)) {
            return (LambdaQueryWrapperBase<T>) super.like(column, val);
        }
        return this;
    }

    public LambdaQueryWrapperBase<T> inIfPresent(SFunction<T, ?> column, Collection<?> values) {
        if (!CollectionUtils.isEmpty(values)) {
            return (LambdaQueryWrapperBase<T>) super.in(column, values);
        }
        return this;
    }

    public LambdaQueryWrapperBase<T> inIfPresent(SFunction<T, ?> column, Object... values) {
        if (!ArrayUtils.isEmpty(values)) {
            return (LambdaQueryWrapperBase<T>) super.in(column, values);
        }
        return this;
    }

    public LambdaQueryWrapperBase<T> eqIfPresent(SFunction<T, ?> column, Object val) {
        if (ObjectUtil.isNotEmpty(val)) {
            return (LambdaQueryWrapperBase<T>) super.eq(column, val);
        }
        return this;
    }

    public LambdaQueryWrapperBase<T> neIfPresent(SFunction<T, ?> column, Object val) {
        if (val != null) {
            return (LambdaQueryWrapperBase<T>) super.ne(column, val);
        }
        return this;
    }

    public LambdaQueryWrapperBase<T> gtIfPresent(SFunction<T, ?> column, Object val) {
        if (val != null) {
            return (LambdaQueryWrapperBase<T>) super.gt(column, val);
        }
        return this;
    }

    public LambdaQueryWrapperBase<T> geIfPresent(SFunction<T, ?> column, Object val) {
        if (val != null) {
            return (LambdaQueryWrapperBase<T>) super.ge(column, val);
        }
        return this;
    }

    public LambdaQueryWrapperBase<T> ltIfPresent(SFunction<T, ?> column, Object val) {
        if (val != null) {
            return (LambdaQueryWrapperBase<T>) super.lt(column, val);
        }
        return this;
    }

    public LambdaQueryWrapperBase<T> leIfPresent(SFunction<T, ?> column, Object val) {
        if (val != null) {
            return (LambdaQueryWrapperBase<T>) super.le(column, val);
        }
        return this;
    }

    public LambdaQueryWrapperBase<T> betweenIfPresent(SFunction<T, ?> column, Object val1, Object val2) {
        if (val1 != null && val2 != null) {
            return (LambdaQueryWrapperBase<T>) super.between(column, val1, val2);
        }
        if (val1 != null) {
            return (LambdaQueryWrapperBase<T>) ge(column, val1);
        }
        if (val2 != null) {
            return (LambdaQueryWrapperBase<T>) le(column, val2);
        }
        return this;
    }

    public LambdaQueryWrapperBase<T> betweenIfPresent(SFunction<T, ?> column, List<?> values) {
        Object val1 = CollUtil.get(values, 0);
        Object val2 = CollUtil.get(values, 1);
        return betweenIfPresent(column, val1, val2);
    }

    // ========== 重写父类方法，方便链式调用 ==========

    @Override
    public LambdaQueryWrapperBase<T> eq(boolean condition, SFunction<T, ?> column, Object val) {
        super.eq(condition, column, val);
        return this;
    }

    @Override
    public LambdaQueryWrapperBase<T> eq(SFunction<T, ?> column, Object val) {
        super.eq(column, val);
        return this;
    }

    @Override
    public LambdaQueryWrapperBase<T> orderByDesc(SFunction<T, ?> column) {
        super.orderByDesc(true, column);
        return this;
    }

    @Override
    public LambdaQueryWrapperBase<T> last(String lastSql) {
        super.last(lastSql);
        return this;
    }

    @Override
    public LambdaQueryWrapperBase<T> in(SFunction<T, ?> column, Collection<?> coll) {
        super.in(column, coll);
        return this;
    }

}
