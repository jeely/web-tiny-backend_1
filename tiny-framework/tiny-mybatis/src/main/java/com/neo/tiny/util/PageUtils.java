package com.neo.tiny.util;

import com.neo.tiny.common.common.PageParam;
import lombok.experimental.UtilityClass;

/**
 * @Description: {@link com.neo.tiny.common.common.PageParam} 工具类
 * @Author: yqz
 * @CreateDate: 2022/9/26 00:35
 */
@UtilityClass
public class PageUtils {
    public int getStart(PageParam pageParam) {
        return (pageParam.getCurrent() - 1) * pageParam.getSize();
    }
}
