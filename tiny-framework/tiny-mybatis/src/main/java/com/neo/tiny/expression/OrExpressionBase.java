package com.neo.tiny.expression;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;

/**
 * @author yqz
 * @Description OrExpression 的扩展类(会在原有表达式两端加上括号)
 * @CreateDate 2023/12/14 18:33
 */
public class OrExpressionBase extends OrExpression {

    public OrExpressionBase() {
    }

    public OrExpressionBase(Expression leftExpression, Expression rightExpression) {
        this.setLeftExpression(leftExpression);
        this.setRightExpression(rightExpression);
    }

    @Override
    public String toString() {
        return "(" + super.toString() + ")";
    }

}
