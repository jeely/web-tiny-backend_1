package com.neo.tiny.admin.service.sys;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.sys.SysMenu;
import com.neo.tiny.admin.vo.menu.SysMenuVO;

import java.util.List;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/7 19:25
 */
public interface SysMenuService extends IService<SysMenu> {


    /**
     * 查询菜单
     *
     * @param menus
     * @param parentId
     * @return
     */
    List<Tree<Long>> filterMenu(List<SysMenuVO> menus, Long parentId);

    /**
     * 构建树
     * @param lazy 是否是懒加载
     * @param parentId 父节点ID
     * @return
     */
    List<Tree<Long>> treeMenu(boolean lazy, Long parentId);

}
