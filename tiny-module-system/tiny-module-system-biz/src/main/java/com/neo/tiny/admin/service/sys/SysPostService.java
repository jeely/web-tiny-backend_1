package com.neo.tiny.admin.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.sys.SysPost;

import java.util.Collection;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:19
 */
public interface SysPostService extends IService<SysPost> {


    /**
     * 校验岗位们是否有效。如下情况，视为无效：
     * 1. 岗位编号不存在
     * 2. 岗位被禁用
     *
     * @param ids 岗位编号数组
     */
    void validPosts(Collection<Long> ids);
}
