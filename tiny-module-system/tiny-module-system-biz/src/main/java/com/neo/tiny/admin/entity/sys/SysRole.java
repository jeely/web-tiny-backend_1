package com.neo.tiny.admin.entity.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.common.enums.DataScopeEnum;
import com.neo.tiny.data.BaseDO;
import com.neo.tiny.handler.JsonLongSetTypeHandler;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/7 21:32
 */
@Data
@TableName(autoResultMap = true, value = "sys_role")
@EqualsAndHashCode(callSuper = true)
public class SysRole extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty("角色编号")
    private Long id;

    @ApiModelProperty("角色名称")
    private String roleName;

    @ApiModelProperty("角色标识")
    private String roleKey;

    @ApiModelProperty("角色描述")
    private String roleDesc;

    @ApiModelProperty("显示顺序")
    private String roleSort;

    /**
     * 数据范围
     * <p>
     * 枚举 {@link DataScopeEnum}
     */
    private Integer dataScope;

    /**
     * 数据范围(指定部门数组)
     * <p>
     * 适用于 {@link #dataScope} 的值为 {@link DataScopeEnum#DEPT_CUSTOM} 时
     */
    @TableField(typeHandler = JsonLongSetTypeHandler.class)
    private Set<Long> dataScopeDeptIds;
}
