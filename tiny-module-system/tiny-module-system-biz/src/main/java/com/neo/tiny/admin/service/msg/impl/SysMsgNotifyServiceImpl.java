
package com.neo.tiny.admin.service.msg.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.admin.entity.msg.SysMsgNotifyDO;
import com.neo.tiny.admin.entity.msg.SysMsgNotifyTemplateDO;
import com.neo.tiny.admin.entity.sys.SysUser;
import com.neo.tiny.admin.mapper.msg.SysMsgNotifyMapper;
import com.neo.tiny.admin.service.msg.SysMsgNotifyService;
import com.neo.tiny.admin.service.sys.SysUserService;
import com.neo.tiny.admin.vo.msg.MyNotifyPageReqVO;
import com.neo.tiny.admin.vo.msg.SysMsgNotifyResVO;
import com.neo.tiny.admin.vo.msg.UnreadNotifyPageReq;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.secrity.util.SecurityUtils;
import com.neo.tiny.util.MyBatisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Description: 站内信
 * @Author: yqz
 * @CreateDate: 2023-02-11 14:13:28
 */
@Slf4j
@Service
public class SysMsgNotifyServiceImpl extends ServiceImpl<SysMsgNotifyMapper, SysMsgNotifyDO> implements SysMsgNotifyService {

    @Autowired
    private SysUserService userService;

    @Autowired
    private SysMsgNotifyMapper msgNotifyMapper;

    @Override
    public Long createNotifyMsg(Long toUserId, Long fromUserId,
                                SysMsgNotifyTemplateDO template, String notifyContent) {


        List<Long> userIds = new ArrayList<>();
        userIds.add(toUserId);
        userIds.add(fromUserId);
        List<SysUser> userList = userService.listByIds(userIds);

        Map<Long, SysUser> userMap = userList.stream().collect(Collectors.toMap(SysUser::getId, Function.identity(), (key1, key2) -> key1));

        SysMsgNotifyDO notify = new SysMsgNotifyDO();
        notify.setNotifyContent(notifyContent);
        notify.setToUserId(toUserId);
        notify.setToUsername(userMap.get(toUserId).getUserName());
        notify.setFromUserId(fromUserId);
        notify.setFromUsername(userMap.get(fromUserId).getUserName());
        notify.setNotifyTemplateId(template.getId());
        notify.setNotifyTemplateCode(template.getTemplateCode());
        notify.setNotifyTemplateType(template.getTemplateType());
        notify.setReadStatus(0);
        this.save(notify);

        return notify.getId();
    }

    @Override
    public IPage<SysMsgNotifyDO> getUnreadNotifyList(UnreadNotifyPageReq req) {

        LambdaQueryWrapperBase<SysMsgNotifyDO> wrapper = new LambdaQueryWrapperBase<SysMsgNotifyDO>()
                .eq(SysMsgNotifyDO::getToUserId, SecurityUtils.getUserId())
                .eq(SysMsgNotifyDO::getReadStatus, 0);
        return this.page(MyBatisUtils.buildPage(req), wrapper);
    }

    @Override
    public IPage<SysMsgNotifyResVO> myNotifyList(MyNotifyPageReqVO req) {

        Page<SysMsgNotifyDO> page = msgNotifyMapper.selectPage(MyBatisUtils.buildPage(req), new LambdaQueryWrapperBase<SysMsgNotifyDO>()
                .eqIfPresent(SysMsgNotifyDO::getReadStatus, req.getReadStatus())
                .betweenIfPresent(SysMsgNotifyDO::getCreateTime, req.getCreateTime())
                .eq(SysMsgNotifyDO::getToUserId, SecurityUtils.getUserId())
                .orderByDesc(SysMsgNotifyDO::getId));
        return CommonPage.restPage(page, SysMsgNotifyResVO.class);
    }

    @Override
    public void updateNotifyRead(List<Long> ids, Long toUserId) {
        SysMsgNotifyDO notify = new SysMsgNotifyDO();
        notify.setReadStatus(1);
        notify.setReadTime(LocalDateTime.now());
        LambdaQueryWrapperBase<SysMsgNotifyDO> wrapper = new LambdaQueryWrapperBase<>();
        wrapper.in(SysMsgNotifyDO::getId, ids)
                .eq(SysMsgNotifyDO::getToUserId, toUserId)
                .eq(SysMsgNotifyDO::getReadStatus, 0);
        int count = msgNotifyMapper.update(notify, wrapper);
        log.info("更新用户：{} 的站内信为已读，更新条数：{}", toUserId, count);
    }

    @Override
    public void updateAllNotifyRead(Long toUserId) {
        SysMsgNotifyDO notify = new SysMsgNotifyDO();
        notify.setReadStatus(1);
        notify.setReadTime(LocalDateTime.now());
        LambdaQueryWrapperBase<SysMsgNotifyDO> wrapper = new LambdaQueryWrapperBase<>();

        wrapper.eq(SysMsgNotifyDO::getToUserId,toUserId)
                .eq(SysMsgNotifyDO::getReadStatus,0);
        int count = msgNotifyMapper.update(notify, wrapper);
        log.info("更新用户：{} 的站内信全部为已读，更新条数：{}", toUserId, count);
    }
}
