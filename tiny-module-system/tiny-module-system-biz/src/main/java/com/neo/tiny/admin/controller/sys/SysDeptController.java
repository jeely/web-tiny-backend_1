

package com.neo.tiny.admin.controller.sys;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.neo.tiny.admin.entity.sys.SysDept;
import com.neo.tiny.admin.entity.sys.SysUser;
import com.neo.tiny.admin.service.sys.SysDeptService;
import com.neo.tiny.admin.service.sys.SysUserService;
import com.neo.tiny.admin.vo.dept.DeptSimpleRespVO;
import com.neo.tiny.admin.vo.dept.SysDeptTreeVO;
import com.neo.tiny.admin.vo.dept.SysDeptVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.common.util.CommonDoTransfer;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 部门
 *
 * @author yqz
 * @date 2022-08-15 23:18:24
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/dept")
@Api(tags = "部门管理")
public class SysDeptController {

    private final SysDeptService sysDeptService;
    private final SysUserService userService;

    /**
     * 返回树形菜单集合
     *
     * @return 树形菜单
     */
    @ApiOperation("返回树形菜单集合")
    @GetMapping("/tree")
    public ResResult<List<SysDeptTreeVO>> listDeptTrees() {
        return ResResult.success(sysDeptService.listDeptTree(0L));
    }


    /**
     * 添加部门
     */
    @ApiOperation("添加部门")
    @PostMapping("/save")
    public ResResult<Boolean> save(@Valid @RequestBody SysDeptVO vo) {

        log.info("添加部门入参：{}", vo);

        return ResResult.success(sysDeptService.save(BeanUtil.copyProperties(vo, SysDept.class)));
    }


    /**
     * 编辑部门
     *
     * @return 编辑部门
     */
    @ApiOperation("编辑部门")
    @PutMapping("/edit")
    public ResResult<Boolean> edit(@Valid @RequestBody SysDeptVO vo) {
        log.info("编辑部门入参：{}", vo);

        SysDept sysDept = BeanUtil.copyProperties(vo, SysDept.class);
        sysDeptService.updateById(sysDept);

        return ResResult.success(Boolean.TRUE);
    }

    /**
     * 通过ID查询
     *
     * @return 通过ID查询
     */
    @ApiOperation("通过ID查询")
    @GetMapping("/getById/{id:\\d+}")
    public ResResult<SysDeptVO> getById(@PathVariable Long id) {

        log.info("通过ID查询入参：{}", id);

        return ResResult.success(BeanUtil.copyProperties(sysDeptService.getById(id), SysDeptVO.class));
    }


    @ApiOperation("根据id删除部门")
    @DeleteMapping("/remove/{id:\\d+}")
    public ResResult<?> removeById(@PathVariable("id") Long id) {

        long count = sysDeptService.count(new LambdaQueryWrapper<SysDept>().eq(SysDept::getParentId, id));
        Assert.isFalse(count > 0, "该部门下有下级，不允许删除");

        long users = userService.count(new LambdaQueryWrapperBase<SysUser>()
                .eq(SysUser::getDeptId, id));
        Assert.isFalse(users > 0, "该部门下有用户，不允许删除");

        return ResResult.success(sysDeptService.removeById(id));
    }


    @ApiOperation("部门列表")
    @GetMapping("/list-dept")
    public ResResult<List<DeptSimpleRespVO>> listDept() {

        List<SysDept> list = sysDeptService.list(new LambdaQueryWrapper<SysDept>()
                .orderByAsc(SysDept::getOrderNum));
        return ResResult.success(CommonDoTransfer.transfer(list, DeptSimpleRespVO.class));

    }
}
