package com.neo.tiny.admin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.admin.entity.sys.SysUserPost;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:22
 */
@Mapper
public interface SysUserPostMapper extends BaseMapper<SysUserPost> {
}
