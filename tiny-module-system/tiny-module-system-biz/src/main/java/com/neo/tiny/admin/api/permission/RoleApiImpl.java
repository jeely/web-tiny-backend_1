package com.neo.tiny.admin.api.permission;

import com.neo.tiny.admin.service.sys.SysRoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/10/4 18:52
 */
@Service
@AllArgsConstructor
public class RoleApiImpl implements RoleApi {

    private final SysRoleService roleService;

    @Override
    public void validRoles(Collection<Long> ids) {
        roleService.validRoles(ids);
    }
}
