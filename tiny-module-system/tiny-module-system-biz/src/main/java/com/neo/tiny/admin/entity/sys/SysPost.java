package com.neo.tiny.admin.entity.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:13
 */
@Data
@TableName("sys_post")
@EqualsAndHashCode(callSuper = true)
public class SysPost extends BaseDO {
    /**
     * 岗位ID
     */
    @TableId(value = "id",type = IdType.AUTO)
    @ApiModelProperty("岗位ID")
    private Long id;

    /**
     * 岗位编码
     */
    @ApiModelProperty("岗位编码")
    private String postCode;

    /**
     * 岗位名称
     */
    @ApiModelProperty("岗位名称")
    private String postName;

    /**
     * 岗位排序
     */
    @ApiModelProperty("岗位排序")
    private Integer postSort;

    /**
     * 状态（0正常 1停用）
     */
    @ApiModelProperty("状态（0正常 1停用）")
    private String status;


    /**
     * 备注信息
     */
    @ApiModelProperty("备注信息")
    private String remark;

}
