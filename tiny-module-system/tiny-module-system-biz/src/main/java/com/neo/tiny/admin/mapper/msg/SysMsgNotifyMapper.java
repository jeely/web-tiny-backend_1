package com.neo.tiny.admin.mapper.msg;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.admin.entity.msg.SysMsgNotifyDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 站内信
 * @Author: yqz
 * @CreateDate: 2023-02-11 14:13:28
 */
@Mapper
public interface SysMsgNotifyMapper extends BaseMapper<SysMsgNotifyDO> {

}
