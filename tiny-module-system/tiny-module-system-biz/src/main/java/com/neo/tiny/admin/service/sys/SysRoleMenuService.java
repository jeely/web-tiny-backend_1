package com.neo.tiny.admin.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.sys.SysRoleMenu;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:11
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

    /**
     * 保存角色与菜单关系
     *
     * @param roleId
     * @param menusIds
     * @return
     */
    Boolean saveRoleMenu(Long roleId, String menusIds);
}
