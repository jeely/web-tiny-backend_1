package com.neo.tiny.admin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.admin.entity.sys.SysDept;
import org.apache.ibatis.annotations.Mapper;

/**
 * 部门
 *
 * @author yqz
 * @date 2022-08-15 23:18:24
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
