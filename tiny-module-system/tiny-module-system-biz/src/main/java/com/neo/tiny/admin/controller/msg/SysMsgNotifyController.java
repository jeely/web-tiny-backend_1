package com.neo.tiny.admin.controller.msg;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.admin.entity.msg.SysMsgNotifyDO;
import com.neo.tiny.admin.service.msg.SysMsgNotifyService;
import com.neo.tiny.admin.vo.msg.*;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.secrity.util.SecurityUtils;
import com.neo.tiny.util.MyBatisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description: 站内信
 * @Author: yqz
 * @CreateDate: 2023-02-11 14:13:28
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/msg/sys-msg-notify")
@Api(tags = "站内信管理")
public class SysMsgNotifyController {

    private final SysMsgNotifyService sysMsgNotifyService;

    /**
     * 分页查询
     *
     * @param req 站内信分页查询
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<IPage<SysMsgNotifyResVO>> getSysMsgNotifyPage(@Valid SysMsgNotifyPageReqVO req) {

        LambdaQueryWrapperBase<SysMsgNotifyDO> wrapper = new LambdaQueryWrapperBase<SysMsgNotifyDO>()
                .likeIfPresent(SysMsgNotifyDO::getToUsername, req.getToUsername())
                .eqIfPresent(SysMsgNotifyDO::getNotifyTemplateCode, req.getNotifyTemplateCode())
                .eqIfPresent(SysMsgNotifyDO::getNotifyTemplateType, req.getNotifyTemplateType())
                .betweenIfPresent(SysMsgNotifyDO::getCreateTime, req.getCreateTime());
        Page<SysMsgNotifyDO> page = sysMsgNotifyService.page(MyBatisUtils.buildPage(req), wrapper);
        return ResResult.success(CommonPage.restPage(page, SysMsgNotifyResVO.class));
    }


    /**
     * 通过id查询站内信
     *
     * @param id id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{id}")
    public ResResult<SysMsgNotifyResVO> getById(@PathVariable("id") Long id) {
        return ResResult.success(BeanUtil.copyProperties(sysMsgNotifyService.getById(id), SysMsgNotifyResVO.class));
    }

    /**
     * 新增站内信
     *
     * @param sysMsgNotifyCreateReqVO 站内信
     * @return ResResult
     */
    @ApiOperation("新增站内信")
    @PostMapping("/create")
    public ResResult<Long> save(@Valid @RequestBody SysMsgNotifyCreateReqVO sysMsgNotifyCreateReqVO) {

        SysMsgNotifyDO sysMsgNotify = BeanUtil.copyProperties(sysMsgNotifyCreateReqVO, SysMsgNotifyDO.class);
        sysMsgNotifyService.save(sysMsgNotify);

        return ResResult.success(sysMsgNotify.getId());
    }

    /**
     * 修改站内信
     *
     * @param sysMsgNotifyUpdateReqVO 站内信
     * @return ResResult
     */
    @ApiOperation("修改站内信")
    @PutMapping("/update")
    public ResResult<Boolean> updateById(@Valid @RequestBody SysMsgNotifyUpdateReqVO sysMsgNotifyUpdateReqVO) {
        sysMsgNotifyService.updateById(BeanUtil.copyProperties(sysMsgNotifyUpdateReqVO, SysMsgNotifyDO.class));
        return ResResult.success(true);
    }

    /**
     * 通过id删除站内信
     *
     * @param id id
     * @return R
     */
    @ApiOperation("通过id删除站内信")
    @DeleteMapping("/{id}")
    public ResResult<Boolean> removeById(@PathVariable Long id) {
        return ResResult.success(sysMsgNotifyService.removeById(id));
    }

    @GetMapping("/get-unread-page")
    @ApiOperation("获取当前用户的最新站内信列表，默认 10 条")
    public ResResult<IPage<SysMsgNotifyResVO>> getUnreadNotifyList(UnreadNotifyPageReq req) {
        IPage<SysMsgNotifyDO> unreadNotifyList = sysMsgNotifyService.getUnreadNotifyList(req);
        return ResResult.success(CommonPage.restPage(unreadNotifyList, SysMsgNotifyResVO.class));
    }

    @ApiOperation("我的站内信")
    @GetMapping("/my-notify-page")
    public ResResult<IPage<SysMsgNotifyResVO>> getMyNotify(@Valid MyNotifyPageReqVO req) {
        return ResResult.success(sysMsgNotifyService.myNotifyList(req));
    }

    @PutMapping("/update-read")
    @ApiOperation("标记站内信为已读")
    @ApiImplicitParam(name = "ids", value = "编号列表", required = true, example = "1024")
    public ResResult<Boolean> updateNotifyMessageRead(@RequestBody List<Long> ids) {
        sysMsgNotifyService.updateNotifyRead(ids, SecurityUtils.getUserId());
        return ResResult.success(Boolean.TRUE);
    }

    @PutMapping("/update-all-read")
    @ApiOperation("标记所有站内信为已读")
    public ResResult<Boolean> updateAllNotifyMessageRead() {
        sysMsgNotifyService.updateAllNotifyRead(SecurityUtils.getUserId());
        return ResResult.success(Boolean.TRUE);
    }
}
