
package com.neo.tiny.admin.entity.msg;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @Description: 站内信
 * @Author: yqz
 * @CreateDate: 2023-02-11 14:13:28
 */
@Data
@TableName(value = "sys_msg_notify", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "站内信")
public class SysMsgNotifyDO extends BaseDO {

    /**
     * 主键自增
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("主键自增")
    private Long id;

    /**
     * 接受人id
     */
    @ApiModelProperty("接受人id")
    private Long toUserId;

    /**
     * 接受人名称
     */
    @ApiModelProperty("接受人名称")
    private String toUsername;

    /**
     * 发送者id
     */
    @ApiModelProperty("发送者id")
    private Long fromUserId;

    /**
     * 发送人名称
     */
    @ApiModelProperty("发送人名称")
    private String fromUsername;

    /**
     * 站内信模板id
     */
    @ApiModelProperty("站内信模板id")
    private Long notifyTemplateId;

    /**
     * 站内信模板编码
     */
    @ApiModelProperty("站内信模板编码")
    private String notifyTemplateCode;

    /**
     * 站内信模板类型：0-系统，1-通知公告
     */
    @ApiModelProperty("站内信模板类型：0-系统，1-通知公告")
    private Integer notifyTemplateType;

    /**
     * 发送内容
     */
    @ApiModelProperty("发送内容")
    private String notifyContent;

    /**
     * 阅读状态：0-未读，1-已读
     */
    @ApiModelProperty("阅读状态：0-未读，1-已读")
    private Integer readStatus;

    /**
     * 已读时间
     */
    @ApiModelProperty("已读时间")
    private LocalDateTime readTime;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


}
