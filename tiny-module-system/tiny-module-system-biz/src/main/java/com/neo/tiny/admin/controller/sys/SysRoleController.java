package com.neo.tiny.admin.controller.sys;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.admin.entity.sys.SysRole;
import com.neo.tiny.admin.entity.sys.SysUserRole;
import com.neo.tiny.admin.service.sys.SysRoleMenuService;
import com.neo.tiny.admin.service.sys.SysRoleService;
import com.neo.tiny.admin.service.sys.SysUserRoleService;
import com.neo.tiny.admin.vo.role.RoleDataScopeReqVO;
import com.neo.tiny.admin.vo.role.RoleMenuVO;
import com.neo.tiny.admin.vo.role.SysRoleVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.common.constant.CacheConstants;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.service.RedisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 19:38
 */
@Api(tags = "角色管理")
@Slf4j
@RestController
@RequestMapping("/admin/role")
public class SysRoleController {


    @Autowired
    private SysRoleService roleService;

    @Autowired
    private SysRoleMenuService roleMenuService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private SysUserRoleService userRoleService;



    @ApiOperation("分页查询所有角色")
    @GetMapping("/page")
    public ResResult<IPage<SysRoleVO>> page(Page<SysRole> page, SysRoleVO vo) {

        Page<SysRole> res = roleService.page(page, new LambdaQueryWrapper<SysRole>()
                .like(StrUtil.isNotBlank(vo.getRoleName()), SysRole::getRoleName, vo.getRoleName()));

        return ResResult.success(CommonPage.restPage(res, SysRoleVO.class));

    }


    @ApiOperation("增加权限")
    @PostMapping("/menu/update")
    public ResResult<Boolean> update(@Valid @RequestBody RoleMenuVO vo) {

        log.info("增加权限入参：{}", vo);

        return ResResult.success(roleMenuService.saveRoleMenu(vo.getRoleId(), vo.getMenuIds()));
    }


    @PostMapping("/dataScope/update")
    @ApiOperation("更新角色数据权限")
    public ResResult<String> updateDataScope(@RequestBody RoleDataScopeReqVO req) {

        roleService.updateDataScope(req.getRoleId(), req.getDataScope(), req.getDataScopeDeptIds());
        return ResResult.success("更新成功");
    }

    @ApiOperation("角色列表")
    @GetMapping("/list")
    public ResResult<List<SysRoleVO>> listRole() {

        List<SysRole> list = roleService.list();

        return ResResult.success(
                list.stream()
                        .map(role -> BeanUtil.copyProperties(role, SysRoleVO.class))
                        .collect(Collectors.toList())
        );
    }

    @ApiOperation("添加角色")
    @PostMapping("/save")
    public ResResult<Boolean> save(@Valid @RequestBody SysRoleVO vo) {

        return ResResult.success(roleService
                .save(BeanUtil.copyProperties(vo, SysRole.class)));
    }

    @ApiOperation("更新角色")
    @PostMapping("/update")
    public ResResult<Boolean> update(@Valid @RequestBody SysRoleVO vo) {

        return ResResult.success(roleService
                .updateById(BeanUtil.copyProperties(vo, SysRole.class)));
    }


    @ApiOperation("删除角色")
    @DeleteMapping("/del/{id:\\d+}")
    public ResResult<Boolean> del(@PathVariable("id") Long id) {

        long count = userRoleService.count(new LambdaUpdateWrapper<SysUserRole>().eq(SysUserRole::getRoleId, id));
        Assert.isFalse(count > 0, "该角色下有用户，不可删除");

        if (roleService.removeById(id)) {
            Set<String> keys = redisService.keys(CacheConstants.USER_DETAILS + StrUtil.COLON + CacheConstants.CACHE_PATTERN_PREFIX);
            redisService.del(keys);
            return ResResult.success(Boolean.TRUE);
        }
        return ResResult.failed("删除失败");
    }

}
