package com.neo.tiny.admin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.admin.entity.sys.SysPost;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:12
 */
@Mapper
public interface SysPostMapper extends BaseMapper<SysPost> {


    /**
     * 通过用户ID，查询岗位信息
     * @param userId 用户id
     * @return 岗位信息
     */
    List<SysPost> listPostsByUserId(Long userId);
}
