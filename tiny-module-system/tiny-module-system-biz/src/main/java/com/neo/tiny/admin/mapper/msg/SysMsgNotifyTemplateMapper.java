package com.neo.tiny.admin.mapper.msg;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.admin.entity.msg.SysMsgNotifyTemplateDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 站内信模板
 * @Author: yqz
 * @CreateDate: 2023-02-11 14:13:15
 */
@Mapper
public interface SysMsgNotifyTemplateMapper extends BaseMapper<SysMsgNotifyTemplateDO> {

}
