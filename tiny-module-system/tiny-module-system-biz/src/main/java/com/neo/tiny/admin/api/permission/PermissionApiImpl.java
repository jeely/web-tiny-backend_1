package com.neo.tiny.admin.api.permission;

import com.neo.tiny.admin.dto.dept.DeptDataPermissionResDTO;
import com.neo.tiny.admin.service.sys.SysPermissionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

/**
 * @Description: 权限 API 实现类
 * @Author: yqz
 * @CreateDate: 2022/10/16 10:12
 */
@Service
@AllArgsConstructor
public class PermissionApiImpl implements PermissionApi {


    private SysPermissionService permissionService;

    @Override
    public Set<Long> getUserRoleIdListByRoleIds(Collection<Long> roleIds) {
        return permissionService.getUserRoleIdListByRoleIds(roleIds);
    }

    /**
     * 获得登陆用户的部门数据权限
     *
     * @param userId 用户编号
     * @return 部门数据权限
     */
    @Override
    public DeptDataPermissionResDTO getDeptDataPermission(Long userId) {
        return permissionService.getDeptDataPermission(userId);
    }
}
