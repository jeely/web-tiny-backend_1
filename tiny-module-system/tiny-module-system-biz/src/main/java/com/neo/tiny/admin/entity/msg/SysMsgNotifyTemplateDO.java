
package com.neo.tiny.admin.entity.msg;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Description: 站内信模板
 * @Author: yqz
 * @CreateDate: 2023-02-11 14:13:15
 */
@Data
@TableName(value = "sys_msg_notify_template", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "站内信模板")
public class SysMsgNotifyTemplateDO extends BaseDO {

    /**
     * 主键自增
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("主键自增")
    private Long id;

    /**
     * 站内信模板名称
     */
    @ApiModelProperty("站内信模板名称")
    private String templateName;

    /**
     * 站内信模板编码
     */
    @ApiModelProperty("站内信模板编码")
    private String templateCode;

    /**
     * 站内信模板内容
     */
    @ApiModelProperty("站内信模板内容")
    private String templateContent;

    /**
     * 模板类型(0-系统消息，1-通知公告)
     */
    @ApiModelProperty("模板类型(0-系统消息，1-通知公告)")
    private Integer templateType;

    /**
     * 模板参数数组
     */
    @ApiModelProperty("模板参数数组")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> templateParams;

    /**
     * 状态（0-关闭，1-正常）
     */
    @ApiModelProperty("状态（0-关闭，1-正常）")
    private String status;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


}
