package com.neo.tiny.admin.service.sys.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.admin.entity.sys.SysUserPost;
import com.neo.tiny.admin.mapper.sys.SysUserPostMapper;
import com.neo.tiny.admin.service.sys.SysUserPostService;
import org.springframework.stereotype.Service;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:25
 */
@Service
public class SysUserPostServiceImpl extends ServiceImpl<SysUserPostMapper, SysUserPost> implements SysUserPostService {
}
