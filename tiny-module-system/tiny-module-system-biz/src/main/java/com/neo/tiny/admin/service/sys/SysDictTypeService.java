
package com.neo.tiny.admin.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.sys.SysDictType;

/**
 * 字典
 *
 * @author yqz
 * @date 2022-09-03 13:42:52
 */
public interface SysDictTypeService extends IService<SysDictType> {

}
