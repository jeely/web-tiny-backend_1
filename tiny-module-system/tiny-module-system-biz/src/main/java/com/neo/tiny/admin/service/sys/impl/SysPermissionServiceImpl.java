package com.neo.tiny.admin.service.sys.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import com.neo.tiny.admin.dto.dept.DeptDataPermissionResDTO;
import com.neo.tiny.admin.entity.sys.SysRole;
import com.neo.tiny.admin.entity.sys.SysUserRole;
import com.neo.tiny.admin.mapper.sys.SysUserRoleMapper;
import com.neo.tiny.admin.service.sys.SysDeptService;
import com.neo.tiny.admin.service.sys.SysPermissionService;
import com.neo.tiny.admin.service.sys.SysRoleService;
import com.neo.tiny.admin.vo.dept.SysDeptVO;
import com.neo.tiny.admin.vo.role.SysRoleVO;
import com.neo.tiny.annotation.DataPermission;
import com.neo.tiny.common.enums.DataScopeEnum;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import com.neo.tiny.secrity.util.SecurityUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 权限 Service 实现类
 * @Author: yqz
 * @CreateDate: 2022/10/16 10:21
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysPermissionServiceImpl implements SysPermissionService {


    private final SysUserRoleMapper userRoleMapper;

    private final SysRoleService roleService;

    private final SysDeptService deptService;

    @Override
    public Set<Long> getUserRoleIdListByRoleIds(Collection<Long> roleIds) {

        List<SysUserRole> userRoles = userRoleMapper.selectList(new LambdaQueryWrapperBase<SysUserRole>()
                .eq(SysUserRole::getRoleId, roleIds));
        if (CollUtil.isEmpty(userRoles)) {
            return new HashSet<>();
        }
        return userRoles.stream().map(SysUserRole::getUserId).filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    /**
     * 获得登陆用户的部门数据权限
     *
     * @param userId 用户编号
     * @return 部门数据权限
     */
    @DataPermission(enable = false) // 关闭数据权限，不然就会出现递归获取数据权限的问题
    @Override
    public DeptDataPermissionResDTO getDeptDataPermission(Long userId) {

        List<SysRoleVO> roleList = SecurityUtils.getUser().getRoleList();
        DeptDataPermissionResDTO result = new DeptDataPermissionResDTO();

        if (CollUtil.isEmpty(roleList)) {
            // 如果角色为空，则只能查看自己
            result.setSelf(true);
            return result;
        }
        Set<Long> roleIds = roleList.stream().map(SysRoleVO::getId).collect(Collectors.toSet());

        List<SysRole> roleListDO = roleService.list(new LambdaQueryWrapperBase<SysRole>()
                .in(SysRole::getId, roleIds));
        // 获取当前用户的部门信息
        SysDeptVO dept = SecurityUtils.getUser().getDept();

        // 遍历角色，计算数据权限
        for (SysRole role : roleListDO) {

            // 为空时，跳过
            if (role.getDataScope() == null) {
                continue;
            }
            // 情况一，ALL
            if (Objects.equals(role.getDataScope(), DataScopeEnum.ALL.getScope())) {
                result.setAll(true);
                continue;
            }
            // 情况二，DEPT_CUSTOM
            if (Objects.equals(role.getDataScope(), DataScopeEnum.DEPT_CUSTOM.getScope())) {
                CollUtil.addAll(result.getDeptIds(), role.getDataScopeDeptIds());
                // 自定义可见部门时，保证可以看到自己所在的部门。否则，一些场景下可能会有问题。
                // 例如说，登录时，基于 t_user 的 username 查询会可能被 dept_id 过滤掉
                CollUtil.addAll(result.getDeptIds(), dept);
                continue;
            }
            // 情况三，DEPT_ONLY
            if (Objects.equals(role.getDataScope(), DataScopeEnum.DEPT_ONLY.getScope())) {
                if (BeanUtil.isNotEmpty(dept)) {
                    CollUtil.addAll(result.getDeptIds(),dept.getId());
                }
                continue;
            }
            // 情况四，DEPT_DEPT_AND_CHILD
            if (Objects.equals(role.getDataScope(), DataScopeEnum.DEPT_AND_CHILD.getScope())) {
                List<Long> deptIds = deptService.getDeptListByParentId(dept.getId());
                CollUtil.addAll(result.getDeptIds(), deptIds);
                // 添加本身部门编号
                CollUtil.addAll(result.getDeptIds(), dept.getId());
                continue;
            }
            // 情况五，SELF
            if (Objects.equals(role.getDataScope(), DataScopeEnum.SELF.getScope())) {
                result.setSelf(true);
                continue;
            }
            // 未知情况，error log 即可
            log.error("[getDeptDataPermission][LoginUser({}) role({}) 无法处理]", userId, JSONUtil.toJsonStr(result));
        }
        return result;

    }
}
