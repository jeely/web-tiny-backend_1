package com.neo.tiny.admin.service.sys.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.admin.entity.sys.SysPost;
import com.neo.tiny.admin.mapper.sys.SysPostMapper;
import com.neo.tiny.admin.service.sys.SysPostService;
import com.neo.tiny.common.constant.ErrorCodeConstants;
import com.neo.tiny.common.exception.WebApiException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:19
 */
@Service
@AllArgsConstructor
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPost> implements SysPostService {

    private final SysPostMapper postMapper;

    @Override
    public void validPosts(Collection<Long> ids) {
        if (CollUtil.isEmpty(ids)) {
            return;
        }
        List<SysPost> postList = postMapper.selectBatchIds(ids);

        Map<Long, SysPost> postMap = postList.stream()
                .collect(Collectors.toMap(SysPost::getId, Function.identity(), (key1, key2) -> key1));
        ids.forEach(postId -> {
            SysPost post = postMap.get(postId);
            if (Objects.isNull(post)) {
                throw new WebApiException(ErrorCodeConstants.POST_NOT_FOUND);
            }
        });
    }
}
