package com.neo.tiny.admin.api.dict;

import com.neo.tiny.admin.service.sys.SysDictItemService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * @Description: 字典数据 API 接口
 * @Author: yqz
 * @CreateDate: 2022/10/4 21:33
 */
@Service
@AllArgsConstructor
public class DictDataApiImpl implements DictDataApi {


    private final SysDictItemService dictItemService;

    @Override
    public void validDictDatas(String dictType, Collection<String> values) {
        dictItemService.validDictDatas(dictType, values);
    }

}
