
package com.neo.tiny.admin.service.msg;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.msg.SysMsgNotifyTemplateDO;
import com.neo.tiny.admin.vo.msg.SysMsgNotifyTemplateCreateReqVO;
import com.neo.tiny.admin.vo.msg.SysMsgNotifyTemplateUpdateReqVO;

/**
 * @Description: 站内信模板
 * @Author: yqz
 * @CreateDate: 2023-02-11 14:13:15
 */
public interface SysMsgNotifyTemplateService extends IService<SysMsgNotifyTemplateDO> {

    /**
     * 创建模板
     *
     * @param req 入参
     */
    Long createTemplate(SysMsgNotifyTemplateCreateReqVO req);

    /**
     * 更新模板呢
     *
     * @param req
     * @return
     */
    Boolean updateTemplate(SysMsgNotifyTemplateUpdateReqVO req);

    /**
     * 根据id删除模板
     *
     * @param templateId 模板id
     * @return
     */
    Boolean delTemplate(Long templateId);
}
