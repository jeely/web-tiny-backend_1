package com.neo.tiny.admin.service.sys;

import com.neo.tiny.admin.dto.dept.DeptDataPermissionResDTO;

import java.util.Collection;
import java.util.Set;

/**
 * @Description: 权限 Service 接口
 * <p>
 * 提供用户-角色、角色-菜单、角色-部门的关联权限处理
 * @Author: yqz
 * @CreateDate: 2022/10/16 10:20
 */
public interface SysPermissionService {

    /**
     * 获得拥有多个角色的用户编号集合
     *
     * @param roleIds 角色编号集合
     * @return 用户编号集合
     */
    Set<Long> getUserRoleIdListByRoleIds(Collection<Long> roleIds);

    /**
     * 获得登陆用户的部门数据权限
     *
     * @param userId 用户编号
     * @return 部门数据权限
     */
    DeptDataPermissionResDTO getDeptDataPermission(Long userId);

}
