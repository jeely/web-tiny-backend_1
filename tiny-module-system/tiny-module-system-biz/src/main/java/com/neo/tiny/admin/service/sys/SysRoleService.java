package com.neo.tiny.admin.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.sys.SysRole;

import java.util.Collection;
import java.util.Set;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:08
 */
public interface SysRoleService extends IService<SysRole> {


    /**
     * 校验角色们是否有效。如下情况，视为无效：
     * 1. 角色编号不存在
     * 2. 角色被禁用
     *
     * @param ids 角色编号数组
     */
    void validRoles(Collection<Long> ids);


    /**
     * 更新角色数据范围
     *
     * @param roleId           角色id
     * @param dataScope        数据范围
     * @param dataScopeDeptIds 自定义部门id
     */

    void updateDataScope(Long roleId, Integer dataScope, Set<Long> dataScopeDeptIds);
}
