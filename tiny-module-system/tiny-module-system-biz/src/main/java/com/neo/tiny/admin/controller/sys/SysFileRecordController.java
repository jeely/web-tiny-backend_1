package com.neo.tiny.admin.controller.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.admin.vo.file.SysFilePageReqVO;
import com.neo.tiny.admin.vo.file.SysFileResVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.middle.file.api.FileRecordApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 文件存储记录
 * @Author: yqz
 * @CreateDate: 2022/12/6 00:07
 */
@Slf4j
@RestController
@AllArgsConstructor
@Api(tags = "文件存储记录管理")
@RequestMapping("/file-record")
public class SysFileRecordController {

    private final FileRecordApi fileRecordApi;


    @ApiOperation("分页查询文件记录")
    @GetMapping("/page")
    public ResResult<IPage<SysFileResVO>> page(SysFilePageReqVO req) {

        return ResResult.success(fileRecordApi.fileRecordPage(req));
    }

    @ApiOperation("根据主键id删除文件")
    @DeleteMapping("/file-del/{id}")
    public ResResult<Boolean> del(@PathVariable(value = "id") Long id) {
        return ResResult.success(fileRecordApi.removeById(id));
    }
}
