package com.neo.tiny.admin.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.sys.SysUserPost;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:24
 */
public interface SysUserPostService extends IService<SysUserPost> {
}
