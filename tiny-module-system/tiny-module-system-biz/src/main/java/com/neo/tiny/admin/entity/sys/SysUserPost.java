package com.neo.tiny.admin.entity.sys;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysUserPost extends Model<SysUserPost> {
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @ApiModelProperty("用户id")
    private Long userId;

    /**
     * 岗位ID
     */
    @ApiModelProperty("岗位id")
    private Long postId;

}
