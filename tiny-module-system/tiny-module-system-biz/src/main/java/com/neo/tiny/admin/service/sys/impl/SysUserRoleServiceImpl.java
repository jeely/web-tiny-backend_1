package com.neo.tiny.admin.service.sys.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.admin.mapper.sys.SysUserRoleMapper;
import com.neo.tiny.admin.service.sys.SysUserRoleService;
import com.neo.tiny.admin.entity.sys.SysUserRole;
import org.springframework.stereotype.Service;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:07
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

}
