package com.neo.tiny.admin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.admin.entity.sys.SysUser;
import com.neo.tiny.admin.vo.user.SysUserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 用户表 Mapper 接口
 * @Author: yqz
 * @CreateDate: 2022/8/7 13:59
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 分页查询用户信息（含角色）
     * @param page 分页
     * @param userDTO 查询参数
     * @return list
     */
    IPage<SysUserVO> getUserVosPage(Page page, @Param("query") SysUserVO userDTO);

}
