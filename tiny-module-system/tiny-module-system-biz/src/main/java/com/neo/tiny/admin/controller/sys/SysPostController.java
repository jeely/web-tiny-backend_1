package com.neo.tiny.admin.controller.sys;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.admin.entity.sys.SysPost;
import com.neo.tiny.admin.service.sys.SysPostService;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.admin.vo.post.PostVO;
import com.neo.tiny.common.common.ResResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/18 23:25
 */
@Api(tags = "岗位管理")
@RestController
@RequestMapping("/admin/post")
public class SysPostController {

    @Autowired
    private SysPostService postService;

    @ApiOperation("岗位列表")
    @GetMapping("/list")
    public ResResult<List<PostVO>> list() {

        List<SysPost> list = postService.list(new LambdaQueryWrapper<SysPost>()
                .orderByAsc(SysPost::getPostSort));

        return ResResult.success(
                list.stream()
                        .map(post -> BeanUtil.copyProperties(post, PostVO.class))
                        .collect(Collectors.toList())
        );
    }

    @ApiOperation("分页查询岗位列表")
    @GetMapping("/page")
    public ResResult<Page<PostVO>> pageResResul(Page<SysPost> page, PostVO vo) {

        Page<SysPost> postPage = postService.page(page, Wrappers.query(BeanUtil.copyProperties(vo, SysPost.class)));

        return ResResult.success(CommonPage.restPage(postPage, PostVO.class));
    }

    @ApiOperation("保存岗位")
    @PostMapping("/save")
    public ResResult<Boolean> save(@Valid @RequestBody PostVO vo) {

        return ResResult.success(postService.save(BeanUtil.copyProperties(vo, SysPost.class)));
    }

    @ApiOperation("修改岗位")
    @PostMapping("/update")
    public ResResult<Boolean> update(@RequestBody PostVO vo) {
        return ResResult.success(postService.updateById(BeanUtil.copyProperties(vo, SysPost.class)));
    }


    @ApiOperation("根据id删除岗位")
    @DeleteMapping("/del/{postId}")
    public ResResult<Boolean> del(@PathVariable("postId") Long postId) {
        return ResResult.success(postService.removeById(postId));
    }

}
