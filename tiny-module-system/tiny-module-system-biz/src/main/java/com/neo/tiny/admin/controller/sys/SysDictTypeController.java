

package com.neo.tiny.admin.controller.sys;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.admin.entity.sys.SysDictItem;
import com.neo.tiny.admin.entity.sys.SysDictType;
import com.neo.tiny.admin.service.sys.SysDictItemService;
import com.neo.tiny.admin.service.sys.SysDictTypeService;
import com.neo.tiny.admin.vo.sys.SysDictItemVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.common.util.CommonDoTransfer;
import com.neo.tiny.resolver.CommonPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典
 *
 * @author yqz
 * @date 2022-09-03 13:42:52
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/sys_dict")
@Api(tags = "字典管理")
public class SysDictTypeController {

    private final SysDictTypeService sysDictTypeService;

    private final SysDictItemService sysDictItemService;

    /**
     * 分页查询
     *
     * @param page        分页对象
     * @param sysDictType 字典
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<?> getSysDictTypePage(Page page, SysDictType sysDictType) {
        return ResResult.success(sysDictTypeService.page(page, Wrappers.query(sysDictType)));
    }


    /**
     * 通过id查询字典
     *
     * @param dictId id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{dictId::\\d+}")
    public ResResult<?> getById(@PathVariable("dictId") Long dictId) {
        return ResResult.success(sysDictTypeService.getById(dictId));
    }

    /**
     * 新增字典
     *
     * @param sysDictType 字典
     * @return ResResult
     */
    @ApiOperation("新增字典")
    @PostMapping
    public ResResult<?> save(@RequestBody SysDictType sysDictType) {
        return ResResult.success(sysDictTypeService.save(sysDictType));
    }

    /**
     * 修改字典
     *
     * @param sysDictType 字典
     * @return ResResult
     */
    @ApiOperation("修改字典")
    @PutMapping
    public ResResult<Boolean> updateById(@RequestBody SysDictType sysDictType) {
        sysDictTypeService.updateById(sysDictType);
        sysDictItemService.update(new LambdaUpdateWrapper<SysDictItem>().set(SysDictItem::getDictType, sysDictType.getDictType())
                .eq(SysDictItem::getDictId, sysDictType.getId()));
        return ResResult.success(Boolean.TRUE);
    }

    /**
     * 通过id删除字典
     *
     * @param dictId id
     * @return R
     */
    @ApiOperation("通过id删除字典")
    @DeleteMapping("/{dictId}")
    public ResResult<Boolean> removeById(@PathVariable Long dictId) {
        sysDictTypeService.removeById(dictId);
        sysDictItemService.remove(new LambdaQueryWrapper<SysDictItem>().eq(SysDictItem::getDictId, dictId));
        return ResResult.success(Boolean.TRUE);
    }


    /**
     * 通过字典类型查找字典
     *
     * @param type 类型
     * @return 同类型字典
     */
    @GetMapping("/type/{type}")
    public ResResult<List<SysDictItemVO>> getDictByType(@PathVariable String type) {
        List<SysDictItem> list = sysDictItemService.list(Wrappers.<SysDictItem>query().lambda()
                .eq(SysDictItem::getDictType, type));
        return ResResult.success(CommonDoTransfer.transfer(list, SysDictItemVO.class));
    }


    // 字典子项相关

    /**
     * 分页查询
     *
     * @param page 分页对象
     * @param vo   字典项
     * @return
     */
    @ApiOperation("分页查询字典")
    @GetMapping("/item/page")
    public ResResult<IPage<SysDictItemVO>> getSysDictItemPage(Page<SysDictItem> page, SysDictItemVO vo) {

        Page<SysDictItem> res = sysDictItemService.page(page, Wrappers.query(BeanUtil.copyProperties(vo, SysDictItem.class)));

        return ResResult.success(CommonPage.restPage(res, SysDictItemVO.class));
    }

    /**
     * 通过id查询字典数据
     *
     * @param itemId id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/item/{itemId}")
    public ResResult<SysDictItemVO> getItemById(@PathVariable("itemId") Long itemId) {
        return ResResult.success(BeanUtil.copyProperties(sysDictItemService.getById(itemId), SysDictItemVO.class));
    }

    /**
     * 新增字典数据
     *
     * @param vo 字典数据
     * @return ResResult
     */
    @ApiOperation("新增字典数据")
    @PostMapping("/item")
    public ResResult<Boolean> save(@RequestBody SysDictItemVO vo) {
        return ResResult.success(sysDictItemService.save(BeanUtil.copyProperties(vo, SysDictItem.class)));
    }

    /**
     * 修改字典数据
     *
     * @param sysDictItem 字典数据
     * @return ResResult
     */
    @ApiOperation("修改字典数据")
    @PutMapping("/item")
    public ResResult<Boolean> updateById(@RequestBody SysDictItem sysDictItem) {
        return ResResult.success(sysDictItemService.updateById(sysDictItem));
    }

    /**
     * 通过id删除字典数据
     *
     * @param itemId id
     * @return R
     */
    @ApiOperation("通过id删除字典数据")
    @DeleteMapping("/item/{itemId}")
    public ResResult<Boolean> removeDictItemById(@PathVariable Long itemId) {
        return ResResult.success(sysDictItemService.removeById(itemId));
    }

    @ApiOperation("获取所有字典")
    @GetMapping("/list-all")
    public ResResult<List<SysDictItemVO>> listAllDict() {
        List<SysDictItem> list = sysDictItemService.list(new LambdaQueryWrapper<SysDictItem>()
                .select(SysDictItem::getId, SysDictItem::getDictId
                        , SysDictItem::getDictLabel, SysDictItem::getDictValue
                        , SysDictItem::getColorType
                        , SysDictItem::getDictType, SysDictItem::getDictSort));

        return ResResult.success(CommonDoTransfer.transfer(list, SysDictItemVO.class));
    }
}
