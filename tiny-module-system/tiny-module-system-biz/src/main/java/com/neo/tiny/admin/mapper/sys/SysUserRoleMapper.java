package com.neo.tiny.admin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.admin.entity.sys.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:04
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
}
