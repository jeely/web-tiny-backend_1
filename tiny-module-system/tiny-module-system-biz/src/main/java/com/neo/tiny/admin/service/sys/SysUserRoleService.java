package com.neo.tiny.admin.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.sys.SysUserRole;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 15:06
 */
public interface SysUserRoleService extends IService<SysUserRole> {

}
