package com.neo.tiny.admin.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import com.neo.tiny.config.BaseSwaggerConfig;
import com.neo.tiny.domain.SwaggerApiInfo;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/3 18:00
 */
@Profile({"dev", "local", "remote"})
@Configuration
@EnableSwagger2
@EnableKnife4j
public class SwaggerConfig extends BaseSwaggerConfig {

    @Override
    public void swaggerApiInfo(SwaggerApiInfo swaggerApiInfo) {
        swaggerApiInfo.setApiBasePackage("com.neo.tiny");
        swaggerApiInfo.setTitle("基础脚手架");
        swaggerApiInfo.setDescription("基础脚手架后台接口");
        swaggerApiInfo.setContactName("yqz");
        swaggerApiInfo.setVersion("1.0");
        swaggerApiInfo.setGroup("基础脚手架");
        swaggerApiInfo.setEnableSecurity(true);
    }
}
