package com.neo.tiny.admin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.admin.entity.sys.SysDictItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典数据
 *
 * @author yqz
 * @date 2022-09-03 17:45:11
 */
@Mapper
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

}
