package com.neo.tiny.admin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.admin.entity.sys.SysDictType;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典
 *
 * @author yqz
 * @date 2022-09-03 13:42:52
 */
@Mapper
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

}
