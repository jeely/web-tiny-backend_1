package com.neo.tiny.admin.controller.sys;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.admin.params.auth.UserPasswordUpdateReq;
import com.neo.tiny.admin.params.sys.UserParams;
import com.neo.tiny.admin.service.sys.SysUserService;
import com.neo.tiny.admin.vo.user.SysUserVO;
import com.neo.tiny.admin.entity.sys.SysUser;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.common.util.CommonDoTransfer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/15 16:49
 */
@Slf4j
@Api(tags = "系统用户管理")
@RestController
@RequestMapping("/admin/user")
public class SysUserController {

    @Autowired
    private SysUserService userService;


    @ApiOperation("分页获取用户")
    @GetMapping("/page")
    public ResResult<IPage<SysUserVO>> page(Page<SysUserVO> page, SysUserVO vo) {

        log.info("分页获取用户入参:{}", vo);

        return ResResult.success(userService.getUserWithRoleDeptPage(page, vo));

    }

    /**
     * 判断用户是否存在
     *
     * @param vo 查询条件
     * @return
     */
    @ApiOperation("检查用户是否存在")
    @GetMapping("/check/exist")
    public ResResult<Boolean> isExsit(SysUserVO vo) {
        List<SysUser> sysUserList = userService.list(new LambdaQueryWrapper<>(BeanUtil.copyProperties(vo, SysUser.class)));

        if (CollUtil.isNotEmpty(sysUserList)) {
            return ResResult.success(Boolean.TRUE);
        }
        return ResResult.success(Boolean.FALSE);
    }

    @ApiOperation("新增用户")
    @PostMapping("/save")
    public ResResult<Boolean> save(@Valid @RequestBody UserParams params) {
        return ResResult.success(userService.saveUser(params));
    }

    @ApiOperation("编辑用户")
    @PutMapping("/edit")
    public ResResult<Boolean> edit(@Valid @RequestBody UserParams params) {

        return ResResult.success(userService.editUser(params));
    }


    @ApiOperation("用户列表")
    @GetMapping("/listUser")
    public ResResult<List<SysUserVO>> listUser() {

        List<SysUser> userList = userService.list(new LambdaQueryWrapper<SysUser>()
                .select(SysUser::getId, SysUser::getUserName, SysUser::getNickName));
        return ResResult.success(CommonDoTransfer.transfer(userList, SysUserVO.class));
    }

    @ApiOperation("根据用户id删除用户")
    @DeleteMapping("/{userId}")
    public ResResult<Boolean> del(@PathVariable("userId") Long userId) {

        return ResResult.success(userService.removeById(userId));
    }

    @ApiOperation("重置用户密码")
    @PostMapping("/reset-password")
    public ResResult<String> resetPassword(@Valid @RequestBody UserPasswordUpdateReq req) {

        userService.updateUserPassword(req.getUserId(), req.getNewPassword());

        return ResResult.success("重置成功");
    }
}
