package com.neo.tiny.admin.controller.demo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/3 17:55
 */
@Api(tags = "hello world")
@RequestMapping("/hello")
@RestController
public class HelloWordController {

    @ApiOperation("你好")
    @GetMapping("/world")
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok("hello world");
    }
}
