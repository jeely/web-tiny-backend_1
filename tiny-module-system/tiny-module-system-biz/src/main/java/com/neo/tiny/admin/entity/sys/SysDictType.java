
package com.neo.tiny.admin.entity.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典
 *
 * @author yqz
 * @date 2022-09-03 13:42:52
 */
@Data
@TableName("sys_dict_type")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "字典")
public class SysDictType extends BaseDO {

    /**
     * 字典主键
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("字典主键")
    private Long id;


    /**
     * 字典类型
     */
    @ApiModelProperty("字典类型")
    private String dictType;

    /**
     * 字典描述
     */
    @ApiModelProperty("字典描述")
    private String description;

    /**
     * 字典类型:0-系统内置，1-业务字典
     */
    @ApiModelProperty("字典类型:0-系统内置，1-业务字典")
    private String systemFlag;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


}
