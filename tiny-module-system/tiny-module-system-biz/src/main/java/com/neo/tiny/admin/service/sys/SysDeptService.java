
package com.neo.tiny.admin.service.sys;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.sys.SysDept;
import com.neo.tiny.admin.vo.dept.SysDeptTreeVO;

import java.util.Collection;
import java.util.List;

/**
 * 部门
 *
 * @author yqz
 * @date 2022-08-15 23:18:24
 */
public interface SysDeptService extends IService<SysDept> {

    /**
     * 查询部门树菜单
     *
     * @return 树
     */
    List<Tree<Long>> listDeptTrees();

    /**
     * 查询部门树菜单
     *
     * @param rootId 根节点
     * @return 树
     */
    List<SysDeptTreeVO> listDeptTree(Long rootId);

    /**
     * 校验部门们是否有效。如下情况，视为无效：
     * 1. 部门编号不存在
     * 2. 部门被禁用
     *
     * @param ids 角色编号数组
     */
    void validDepts(Collection<Long> ids);


    /**
     * 获得所有子部门，从缓存中
     *
     * @param parentId 部门编号
     * @return 子部门列表
     */
    List<Long> getDeptListByParentId(Long parentId);


}
