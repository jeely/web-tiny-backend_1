package com.neo.tiny.admin.api.dept;

import com.neo.tiny.admin.service.sys.SysPostService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * @Description: 岗位 API 实现类
 * @Author: yqz
 * @CreateDate: 2022/10/4 21:00
 */
@Service
@AllArgsConstructor
public class PostApiImpl implements PostApi {

    private final SysPostService postService;

    @Override
    public void validPosts(Collection<Long> ids) {
        postService.validPosts(ids);
    }
}
