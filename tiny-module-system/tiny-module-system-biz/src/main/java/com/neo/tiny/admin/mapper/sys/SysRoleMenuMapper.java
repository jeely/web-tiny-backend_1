package com.neo.tiny.admin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.admin.entity.sys.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yqz
 * @Description 角色菜单表 Mapper 接口
 * @CreateDate 2022/8/8 15:11
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {
}
