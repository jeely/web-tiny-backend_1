
package com.neo.tiny.admin.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.sys.SysDictItem;

import java.util.Collection;

/**
 * 字典数据
 *
 * @author yqz
 * @date 2022-09-03 17:45:11
 */
public interface SysDictItemService extends IService<SysDictItem> {

    /**
     * 校验字典数据们是否有效。如下情况，视为无效：
     * 1. 字典数据不存在
     * 2. 字典数据被禁用
     *
     * @param dictType 字典类型
     * @param values 字典数据值的数组
     */
    void validDictDatas(String dictType, Collection<String> values);
}
