
package com.neo.tiny.admin.entity.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 部门
 *
 * @author yqz
 * @date 2022-08-15 23:13:03
 */
@Data
@TableName("sys_dept")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "部门")
public class SysDept extends BaseDO {

    /**
     * 部门id
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("部门id")
    private Long id;

    /**
     * 父部门id
     */
    @ApiModelProperty("父部门id")
    private Long parentId;

    /**
     * 祖级列表
     */
    @ApiModelProperty("祖级列表")
    private String ancestors;

    /**
     * 部门名称
     */
    @ApiModelProperty("部门名称")
    private String deptName;

    /**
     * 显示顺序
     */
    @ApiModelProperty("显示顺序")
    private Integer orderNum;

    /**
     * 负责人
     */
    @ApiModelProperty("负责人")
    private Long leader;

    /**
     * 联系电话
     */
    @ApiModelProperty("联系电话")
    private String phone;

    /**
     * 邮箱
     */
    @ApiModelProperty("邮箱")
    private String email;

    /**
     * 部门状态（0正常 1停用）
     */
    @ApiModelProperty("部门状态（0正常 1停用）")
    private String status;


}
