package com.neo.tiny.admin.config;

import com.neo.tiny.admin.entity.sys.SysDept;
import com.neo.tiny.admin.entity.sys.SysUser;
import com.neo.tiny.rule.DeptDataPermissionRuleCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author yqz
 * @Description 数据权限 Configuration
 * @CreateDate 2023/12/15 16:15
 */
@Configuration(proxyBeanMethods = false)
public class DataPermissionConfiguration {
    @Bean
    public DeptDataPermissionRuleCustomizer sysDeptDataPermissionRuleCustomizer() {
        return rule -> {
            // dept
            rule.addDeptColumn(SysUser.class);
            rule.addDeptColumn(SysDept.class, "id");
            // user
            rule.addUserColumn(SysUser.class, "id");
        };
    }
}
