package com.neo.tiny.admin.service.msg;

import java.util.Map;

/**
 * @Description: 站内信收发业务
 * @Author: yqz
 * @CreateDate: 2023/2/11 21:36
 */
public interface NotifySendService {

    /**
     * 发送单条站内信给用户
     *
     * @param toUserId      用户编号
     * @param fromUserId    用户类型
     * @param templateCode   站内信模板编号
     * @param templateParams 站内信模板参数
     * @return 发送日志编号
     */
    Long sendSingleNotify(Long toUserId, Long fromUserId,
                          String templateCode, Map<String, Object> templateParams);

}
