
package com.neo.tiny.admin.service.msg;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.msg.SysMsgNotifyDO;
import com.neo.tiny.admin.entity.msg.SysMsgNotifyTemplateDO;
import com.neo.tiny.admin.vo.msg.MyNotifyPageReqVO;
import com.neo.tiny.admin.vo.msg.SysMsgNotifyResVO;
import com.neo.tiny.admin.vo.msg.UnreadNotifyPageReq;

import java.util.List;

/**
 * @Description: 站内信
 * @Author: yqz
 * @CreateDate: 2023-02-11 14:13:28
 */
public interface SysMsgNotifyService extends IService<SysMsgNotifyDO> {


    /**
     * 创建站内信
     *
     * @param toUserId      用户编号
     * @param fromUserId    用户类型
     * @param template      模版信息
     * @param notifyContent 模版内容
     * @return 站内信编号
     */
    Long createNotifyMsg(Long toUserId, Long fromUserId,
                         SysMsgNotifyTemplateDO template, String notifyContent);


    /**
     * 分页查询未读消息
     *
     * @param req 参数
     * @return
     */
    IPage<SysMsgNotifyDO> getUnreadNotifyList(UnreadNotifyPageReq req);

    /**
     * 我的站内信
     *
     * @param req 入参
     * @return
     */
    IPage<SysMsgNotifyResVO> myNotifyList(MyNotifyPageReqVO req);

    /**
     * 更新站内信为已读
     *
     * @param ids      站内信主键id
     * @param toUserId 接收人
     * @return
     */
    void updateNotifyRead(List<Long> ids, Long toUserId);

    /**
     * 更新所以为已读
     *
     * @param toUserId 收件人
     * @return
     */
    void updateAllNotifyRead(Long toUserId);
}
