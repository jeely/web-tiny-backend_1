package com.neo.tiny.admin.entity.sys;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/7 21:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysRoleMenu extends Model<SysRoleMenu> {

    private static final long serialVersionUID = 1L;

    /**
     * 角色ID
     */
    @ApiModelProperty("角色id")
    private Long roleId;

    /**
     * 菜单ID
     */
    @ApiModelProperty("菜单id")
    private Long menuId;

}
