package com.neo.tiny.admin.api.dept;

import cn.hutool.core.collection.CollUtil;
import com.neo.tiny.admin.dto.dept.SysDeptDTO;
import com.neo.tiny.admin.entity.sys.SysDept;
import com.neo.tiny.admin.service.sys.SysDeptService;
import com.neo.tiny.common.util.CommonDoTransfer;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/10/4 19:25
 */
@Service
@AllArgsConstructor
public class DeptApiImpl implements DeptApi {

    private final SysDeptService deptService;

    @Override
    public void validDepts(Collection<Long> ids) {
        deptService.validDepts(ids);
    }

    @Override
    public SysDeptDTO getDept(Long deptId) {
        SysDept dept = deptService.getOne(new LambdaQueryWrapperBase<SysDept>()
                .eq(SysDept::getId, deptId));
        return CommonDoTransfer.transfer(dept, SysDeptDTO.class);
    }

    @Override
    public List<SysDeptDTO> getDepts(Collection<Long> ids) {

        if (CollUtil.isEmpty(ids)) {
            return Collections.emptyList();
        }
        List<SysDept> sysDepts = deptService.listByIds(ids);
        return CommonDoTransfer.transfer(sysDepts, SysDeptDTO.class);

    }
}
