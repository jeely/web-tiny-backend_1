
package com.neo.tiny.admin.service.sys.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.admin.entity.sys.SysDept;
import com.neo.tiny.admin.mapper.sys.SysDeptMapper;
import com.neo.tiny.admin.service.sys.SysDeptService;
import com.neo.tiny.admin.vo.dept.SysDeptTreeVO;
import com.neo.tiny.common.constant.ErrorCodeConstants;
import com.neo.tiny.common.exception.WebApiException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 部门
 *
 * @author yqz
 * @date 2022-08-15 23:18:24
 */
@Service
@AllArgsConstructor
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    private final SysDeptMapper deptMapper;

    @Override
    public List<Tree<Long>> listDeptTrees() {
        return this.getDeptTree(this.list(Wrappers.emptyWrapper()), 0L);
    }

    @Override
    public List<SysDeptTreeVO> listDeptTree(Long rootId) {
        return buildDeptTree(list(), rootId);
    }

    @Override
    public void validDepts(Collection<Long> ids) {
        Assert.notEmpty(ids, "待校验部门id集合不能为空");

        List<SysDept> deptList = deptMapper.selectBatchIds(ids);
        Map<Long, SysDept> deptMap = deptList.stream()
                .collect(Collectors.toMap(SysDept::getId, Function.identity(), (key1, key2) -> key1));

        ids.forEach(deptId -> {
            SysDept dept = deptMap.get(deptId);
            if (Objects.isNull(dept)) {
                throw new WebApiException(ErrorCodeConstants.DEPT_NOT_FOUND);
            }
        });
    }

    /**
     * 获得所有子部门
     *
     * @param parentId 部门编号
     * @return 子部门列表
     */
    @Override
    public List<Long> getDeptListByParentId(Long parentId) {
        if (parentId == null) {
            return Collections.emptyList();
        }
        List<Long> childIds = new ArrayList<>();
        findChildId(listDeptTree(parentId), childIds);
        return childIds;
    }

    /**
     * @param deptTree 部门树
     * @param childIds 子节点id集合
     */
    public void findChildId(List<SysDeptTreeVO> deptTree, List<Long> childIds) {
        for (SysDeptTreeVO vo : deptTree) {
            if (ObjectUtil.isNotEmpty(vo.getChildren())) {
                childIds.add(vo.getId());
                findChildId(vo.getChildren(), childIds);
            } else {
                childIds.add(vo.getId());
            }
        }
    }


    /**
     * 构建部门树
     *
     * @param depts    部门
     * @param parentId 父级id
     * @return
     */
    private List<Tree<Long>> getDeptTree(List<SysDept> depts, Long parentId) {
        List<TreeNode<Long>> collect = depts.stream().filter(dept -> dept.getId().intValue() != dept.getParentId())
                .sorted(Comparator.comparingInt(SysDept::getOrderNum)).map(dept -> {
                    TreeNode<Long> treeNode = new TreeNode();
                    treeNode.setId(dept.getId());
                    treeNode.setParentId(dept.getParentId());
                    treeNode.setName(dept.getDeptName());
                    treeNode.setWeight(dept.getOrderNum());
                    // 扩展属性
                    Map<String, Object> extra = new HashMap<>(4);
                    extra.put("createTime", dept.getCreateTime());
                    extra.put("leader", dept.getLeader());
                    treeNode.setExtra(extra);
                    return treeNode;
                }).collect(Collectors.toList());

        return TreeUtil.build(collect, parentId);
    }

    public List<SysDeptTreeVO> buildDeptTree(List<SysDept> deptList, Long root) {

        List<SysDeptTreeVO> res = new ArrayList<>();
        for (SysDept dept : deptList) {
            // 如果是parentId=0则是根节点
            if (Objects.equals(dept.getParentId(), root)) {
                SysDeptTreeVO rootDept = BeanUtil.copyProperties(dept, SysDeptTreeVO.class);
                rootDept.setName(dept.getDeptName());
                rootDept.setWeight(dept.getOrderNum());
                SysDeptTreeVO vo = findChild(rootDept, deptList);
                res.add(vo);
            }
        }
        return res.stream()
                .sorted(Comparator.comparing(SysDeptTreeVO::getOrderNum, Comparator.nullsFirst(Integer::compareTo)))
                .collect(Collectors.toList());
    }


    public SysDeptTreeVO findChild(SysDeptTreeVO rootDept, List<SysDept> deptList) {
        for (SysDept dept : deptList) {
            if (Objects.equals(dept.getParentId(), rootDept.getId())) {
                if (CollUtil.isEmpty(rootDept.getChildren())) {
                    rootDept.setChildren(new ArrayList<>());
                }
                SysDeptTreeVO vo = BeanUtil.copyProperties(dept, SysDeptTreeVO.class);
                vo.setName(dept.getDeptName());
                vo.setWeight(dept.getOrderNum());
                SysDeptTreeVO child = findChild(vo, deptList);
                rootDept.getChildren().add(child);
            }
        }
        if (CollUtil.isNotEmpty(rootDept.getChildren())) {
            List<SysDeptTreeVO> collect = rootDept.getChildren().stream()
                    .sorted(Comparator.comparing(SysDeptTreeVO::getOrderNum, Comparator.nullsFirst(Integer::compareTo)))
                    .collect(Collectors.toList());
            rootDept.setChildren(collect);
        }
        return rootDept;
    }
}
