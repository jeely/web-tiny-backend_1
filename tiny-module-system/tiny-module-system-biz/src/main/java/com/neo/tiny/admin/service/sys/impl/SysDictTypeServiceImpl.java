
package com.neo.tiny.admin.service.sys.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.admin.entity.sys.SysDictType;
import com.neo.tiny.admin.mapper.sys.SysDictTypeMapper;
import com.neo.tiny.admin.service.sys.SysDictTypeService;
import org.springframework.stereotype.Service;

/**
 * 字典
 *
 * @author yqz
 * @date 2022-09-03 13:42:52
 */
@Service
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements SysDictTypeService {

}
