
package com.neo.tiny.admin.entity.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典数据
 *
 * @author yqz
 * @date 2022-09-03 17:45:11
 */
@Data
@TableName("sys_dict_item")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "字典数据")
public class SysDictItem extends BaseDO {

    /**
     * 字典编码
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("字典编码")
    private Long id;

    /**
     * sys_dict_type.id
     */
    @ApiModelProperty("sys_dict_type.id")
    private Integer dictId;

    /**
     * 字典标签
     */
    @ApiModelProperty("字典标签")
    private String dictLabel;

    /**
     * 字典键值
     */
    @ApiModelProperty("字典键值")
    private String dictValue;

    /**
     * 字典类型
     */
    @ApiModelProperty("字典类型")
    private String dictType;

    /**
     * 颜色类型
     */
    @ApiModelProperty("颜色类型")
    private String colorType;

    /**
     * 解释
     */
    @ApiModelProperty("解释")
    private String description;

    /**
     * 字典排序
     */
    @ApiModelProperty("字典排序")
    private Long dictSort;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


}
