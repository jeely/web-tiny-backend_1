package com.neo.tiny.admin.controller.msg;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.admin.entity.msg.SysMsgNotifyTemplateDO;
import com.neo.tiny.admin.service.msg.NotifySendService;
import com.neo.tiny.admin.service.msg.SysMsgNotifyTemplateService;
import com.neo.tiny.admin.vo.msg.*;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.secrity.util.SecurityUtils;
import com.neo.tiny.util.MyBatisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Description: 站内信模板
 * @Author: yqz
 * @CreateDate: 2023-02-11 14:13:15
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/msg/sys-msg-notify-template")
@Api(tags = "站内信模板管理")
public class SysMsgNotifyTemplateController {

    private final SysMsgNotifyTemplateService sysMsgNotifyTemplateService;


    private final NotifySendService notifySendService;

    /**
     * 分页查询
     *
     * @param req 站内信模板
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<IPage<SysMsgNotifyTemplateResVO>> getSysMsgNotifyTemplatePage(@Valid SysMsgNotifyTemplatePageReqVO req) {

        LambdaQueryWrapperBase<SysMsgNotifyTemplateDO> wrapper = new LambdaQueryWrapperBase<SysMsgNotifyTemplateDO>()
                .likeIfPresent(SysMsgNotifyTemplateDO::getTemplateName, req.getTemplateName())
                .likeIfPresent(SysMsgNotifyTemplateDO::getTemplateCode, req.getTemplateCode())
                .eqIfPresent(SysMsgNotifyTemplateDO::getStatus, req.getStatus())
                .betweenIfPresent(SysMsgNotifyTemplateDO::getCreateTime, req.getCreateTime());
        Page<SysMsgNotifyTemplateDO> page = sysMsgNotifyTemplateService.page(MyBatisUtils.buildPage(req), wrapper);
        return ResResult.success(CommonPage.restPage(page, SysMsgNotifyTemplateResVO.class));
    }


    /**
     * 通过id查询站内信模板
     *
     * @param id id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{id}")
    public ResResult<SysMsgNotifyTemplateResVO> getById(@PathVariable("id") Integer id) {
        return ResResult.success(BeanUtil.copyProperties(sysMsgNotifyTemplateService.getById(id), SysMsgNotifyTemplateResVO.class));
    }

    /**
     * 新增站内信模板
     *
     * @param req 站内信模板
     * @return ResResult
     */
    @ApiOperation("新增站内信模板")
    @PostMapping("/create")
    public ResResult<Long> save(@Valid @RequestBody SysMsgNotifyTemplateCreateReqVO req) {

        return ResResult.success(sysMsgNotifyTemplateService.createTemplate(req));
    }

    /**
     * 修改站内信模板
     *
     * @param req 站内信模板
     * @return ResResult
     */
    @ApiOperation("修改站内信模板")
    @PutMapping("/update")
    public ResResult<Boolean> updateById(@Valid @RequestBody SysMsgNotifyTemplateUpdateReqVO req) {
        return ResResult.success(sysMsgNotifyTemplateService.updateTemplate(req));
    }

    /**
     * 通过id删除站内信模板
     *
     * @param id id
     * @return R
     */
    @ApiOperation("通过id删除站内信模板")
    @DeleteMapping("/{id}")
    public ResResult<Boolean> removeById(@PathVariable Long id) {
        return ResResult.success(sysMsgNotifyTemplateService.removeById(id));
    }

    @ApiOperation("测试发送站内信")
    @PostMapping("/send-test")
    public ResResult<Long> sendNotify(@Valid @RequestBody SysMsgNotifySendReq req) {
        Long aLong = notifySendService.sendSingleNotify(req.getUserId(), SecurityUtils.getUserId(), req.getTemplateCode()
                , req.getParams());
        return ResResult.success(aLong);
    }

}
