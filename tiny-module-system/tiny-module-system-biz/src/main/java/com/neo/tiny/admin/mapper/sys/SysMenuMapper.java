package com.neo.tiny.admin.mapper.sys;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.admin.entity.sys.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import java.util.Set;

/**
 * @Description: 菜单权限表 Mapper 接口
 * @Author: yqz
 * @CreateDate: 2022/8/7 19:41
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 通过角色编号查询菜单
     *
     * @param roleId 角色ID
     * @return
     */
    Set<SysMenu> listMenusByRoleId(Long roleId);

}
