

package com.neo.tiny.admin.controller.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.admin.entity.SysConfig;
import com.neo.tiny.admin.sysConfig.interfaces.SysConfigService;
import com.neo.tiny.common.common.ResResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * 系统设置
 *
 * @author yqz
 * @date 2022-09-03 11:10:52
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/sysconfig")
@Api(tags = "系统设置管理")
public class SysConfigController {

    private final SysConfigService sysConfigService;

    /**
     * 分页查询
     *
     * @param page      分页对象
     * @param sysConfig 系统设置
     * @return ResResult
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<?> getSysConfigPage(Page page, SysConfig sysConfig) {
        return ResResult.success(sysConfigService.page(page, Wrappers.query(sysConfig)));
    }


    /**
     * 通过id查询系统设置
     *
     * @param configId id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{configId}")
    public ResResult<?> getById(@PathVariable("configId") Integer configId) {
        return ResResult.success(sysConfigService.getById(configId));
    }

    /**
     * 通过id查询系统设置
     *
     * @param key key
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/configValue/{key}")
    public ResResult<SysConfig> getObjByKey(@PathVariable("key") String key) {

        return ResResult.success(sysConfigService
                .getOne(new LambdaQueryWrapper<SysConfig>().eq(SysConfig::getConfigKey, key)));
    }

    /**
     * 新增系统设置
     *
     * @param sysConfig 系统设置
     * @return ResResult
     */
    @ApiOperation("新增系统设置")
    @PostMapping
    public ResResult<?> save(@RequestBody SysConfig sysConfig) {
        return ResResult.success(sysConfigService.save(sysConfig));
    }

    /**
     * 修改系统设置
     *
     * @param sysConfig 系统设置
     * @return ResResult
     */
    @ApiOperation("修改系统设置")
    @PutMapping
    public ResResult<?> updateById(@RequestBody SysConfig sysConfig) {
        return ResResult.success(sysConfigService.updateById(sysConfig));
    }

    /**
     * 通过id删除系统设置
     *
     * @param configId id
     * @return ResResult
     */
    @ApiOperation("通过id删除系统设置")
    @DeleteMapping("/{configId}")
    public ResResult<?> removeById(@PathVariable Integer configId) {
        return ResResult.success(sysConfigService.removeById(configId));
    }

}
