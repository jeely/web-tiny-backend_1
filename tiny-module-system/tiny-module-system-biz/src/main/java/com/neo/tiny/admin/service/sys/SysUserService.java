package com.neo.tiny.admin.service.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.params.sys.UserParams;
import com.neo.tiny.admin.entity.sys.SysUser;
import com.neo.tiny.admin.vo.user.SysUserVO;

import java.util.Set;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/7 14:00
 */
public interface SysUserService extends IService<SysUser> {


    /**
     * 分页查询
     *
     * @param page
     * @param vo
     * @return
     */
    IPage<SysUserVO> getUserWithRoleDeptPage(Page<SysUserVO> page, SysUserVO vo);


    /**
     * 新增系统用户
     *
     * @param params
     * @return
     */
    Boolean saveUser(UserParams params);


    /**
     * 编辑系统用户
     *
     * @param params
     * @return
     */
    Boolean editUser(UserParams params);

    /**
     * 校验用户们是否有效。如下情况，视为无效：
     * 1. 用户编号不存在
     * 2. 用户被禁用
     *
     * @param ids 用户编号数组
     */
    void validUsers(Set<Long> ids);

    /**
     * 后台重置用户密码
     *
     * @param userId      用户id
     * @param newPassword 新密码
     */
    void updateUserPassword(Long userId, String newPassword);

}
