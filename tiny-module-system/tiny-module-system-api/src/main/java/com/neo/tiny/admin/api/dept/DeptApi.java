package com.neo.tiny.admin.api.dept;

import cn.hutool.core.collection.CollUtil;
import com.neo.tiny.admin.dto.dept.SysDeptDTO;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Description: 部门 API 接口
 * @Author: yqz
 * @CreateDate: 2022/10/4 19:24
 */
public interface DeptApi {

    /**
     * 校验部门们是否有效。如下情况，视为无效：
     * 1. 部门编号不存在
     * 2. 部门被禁用
     *
     * @param ids 角色编号数组
     */
    void validDepts(Collection<Long> ids);

    /**
     * 获得部门信息
     *
     * @param deptId 部门编号
     * @return 部门信息
     */
    SysDeptDTO getDept(Long deptId);

    /**
     * 获得部门信息数组
     *
     * @param ids 部门编号数组
     * @return 部门信息数组
     */
    List<SysDeptDTO> getDepts(Collection<Long> ids);

    /**
     * 获得指定编号的部门 Map
     *
     * @param ids 部门编号数组
     * @return 部门 Map
     */
    default Map<Long, SysDeptDTO> getDeptMap(Set<Long> ids) {
        List<SysDeptDTO> list = getDepts(ids);

        if (CollUtil.isEmpty(list)) {
            return new HashMap<>(0);
        }

        return list.stream()
                .collect(Collectors.toMap(SysDeptDTO::getId, Function.identity(), (key1, key2) -> key1));
    }

}
