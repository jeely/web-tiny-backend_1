
package com.neo.tiny.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统设置
 *
 * @author yqz
 * @date 2022-09-03 11:10:52
 */
@Data
@TableName(value = "sys_config", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "系统设置")
public class SysConfig extends BaseDO {

    /**
     * 参数主键
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("参数主键")
    private Integer id;

    /**
     * 参数名称
     */
    @ApiModelProperty("参数名称")
    private String configName;

    /**
     * 参数键名
     */
    @ApiModelProperty("参数键名")
    private String configKey;

    /**
     * 参数键值
     */
    @ApiModelProperty("参数键值")
    private String configValue;

    /**
     * 系统内置（Y是 N否）
     */
    @ApiModelProperty("系统内置（Y是 N否）")
    private String configType;

    /**
     * 0：有效，1：无效
     */
    @ApiModelProperty("0：有效，1：无效")
    private String status;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


}
