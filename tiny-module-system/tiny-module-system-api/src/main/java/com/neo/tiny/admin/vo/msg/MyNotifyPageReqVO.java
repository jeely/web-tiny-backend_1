package com.neo.tiny.admin.vo.msg;

import com.neo.tiny.common.common.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description: 我的站内信
 * @Author: yqz
 * @CreateDate: 2023/2/19 22:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MyNotifyPageReqVO extends PageParam implements Serializable {


    @ApiModelProperty("是否已读")
    private Integer readStatus;

    @ApiModelProperty("创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private List<LocalDateTime> createTime;
}
