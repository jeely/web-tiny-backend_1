package com.neo.tiny.admin.api.user;

import cn.hutool.core.collection.CollUtil;
import com.neo.tiny.admin.dto.user.UserInfoDTO;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Description: Admin 用户 API 接口
 * @Author: yqz
 * @CreateDate: 2022/10/4 21:11
 */
public interface AdminUserApi {
    /**
     * 校验用户们是否有效。如下情况，视为无效：
     * 1. 用户编号不存在
     * 2. 用户被禁用
     *
     * @param ids 用户编号数组
     */
    void validUsers(Set<Long> ids);

    /**
     * 通过用户 ID 查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    UserInfoDTO getUser(Long userId);

    /**
     * 通过用户 ID 查询用户们
     *
     * @param ids 用户 ID 们
     * @return 用户对象信息
     */
    List<UserInfoDTO> getUsers(Collection<Long> ids);

    /**
     * 获得指定部门的用户数组
     *
     * @param deptIds 部门数组
     * @return 用户数组
     */
    List<UserInfoDTO> getUsersByDeptIds(Collection<Long> deptIds);



    /**
     * 获得指定岗位的用户数组
     *
     * @param postIds 岗位数组
     * @return 用户数组
     */
    List<UserInfoDTO> getUsersByPostIds(Collection<Long> postIds);

    /**
     * 获得用户 Map
     *
     * @param ids 用户编号数组
     * @return 用户 Map
     */
    default Map<Long, UserInfoDTO> getUserMap(Collection<Long> ids) {
        List<UserInfoDTO> users = getUsers(ids);

        if (CollUtil.isEmpty(users)) {
            return new HashMap<>(0);
        }
        return users.stream().collect(Collectors.toMap(UserInfoDTO::getId, Function.identity(), (key1, key2) -> key1));
    }

}
