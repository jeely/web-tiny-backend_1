package com.neo.tiny.admin.vo.post;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/18 23:27
 */
@Data
public class PostVO implements Serializable {
    /**
     * 岗位ID
     */
    @ApiModelProperty("岗位ID")
    private Long id;

    /**
     * 岗位编码
     */
    @ApiModelProperty("岗位编码")
    @NotBlank(message = "岗位编码不能为空")
    private String postCode;

    /**
     * 岗位名称
     */
    @ApiModelProperty("岗位名称")
    @NotBlank(message = "岗位名称不能为空")
    private String postName;

    /**
     * 岗位排序
     */
    @ApiModelProperty("岗位排序")
    private Integer postSort;

    /**
     * 状态（0正常 1停用）
     */
    @ApiModelProperty("状态（0正常 1停用）")
    private String status;


    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;


    /**
     * 备注信息
     */
    @ApiModelProperty("备注信息")
    private String remark;

}
