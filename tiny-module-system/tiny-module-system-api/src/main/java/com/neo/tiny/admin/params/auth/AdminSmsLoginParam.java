package com.neo.tiny.admin.params.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 短信验证码登录
 * @Author: yqz
 * @CreateDate: 2022/11/8 23:05
 */
@Data
public class AdminSmsLoginParam implements Serializable {

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("验证码")
    private String code;
}
