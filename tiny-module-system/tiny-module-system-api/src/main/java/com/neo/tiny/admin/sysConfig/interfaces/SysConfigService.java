package com.neo.tiny.admin.sysConfig.interfaces;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.entity.SysConfig;
import com.neo.tiny.admin.sysConfig.bean.StorageClientConfig;

/**
 * 系统设置
 *
 * @author yqz
 * @date 2022-09-03 11:10:52
 */
public interface SysConfigService extends IService<SysConfig> {


    /**
     * 根据配置key，加载存储配置
     * @param key 固定值 STORAGE_CONFIG {@link com.neo.tiny.common.constant.FileConstants}
     * @return
     */
    StorageClientConfig loadStoreConfig(String key);

}
