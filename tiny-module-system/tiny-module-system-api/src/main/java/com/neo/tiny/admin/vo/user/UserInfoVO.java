package com.neo.tiny.admin.vo.user;

import com.neo.tiny.admin.dto.user.UserInfoDTO;
import com.neo.tiny.admin.vo.dept.SysDeptVO;
import com.neo.tiny.admin.vo.post.PostVO;
import com.neo.tiny.admin.vo.role.SysRoleVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 19:04
 */
@Data
public class UserInfoVO implements Serializable {


    @ApiModelProperty("系统用户信息")
    private UserInfoDTO sysUser;

    @ApiModelProperty("权限列表")
    private List<String> permissions;


    @ApiModelProperty("角色列表")
    private List<SysRoleVO> roleList;


    @ApiModelProperty("岗位信息")
    private List<PostVO> postList;

    @ApiModelProperty("部门")
    private SysDeptVO dept;
}
