package com.neo.tiny.admin.dto.user;

import com.neo.tiny.admin.vo.post.PostVO;
import com.neo.tiny.admin.vo.role.SysRoleVO;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/15 16:53
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysUserDTO extends BaseDO {
    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键id")
    private Long id;


    /**
     * 部门id
     */
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /**
     * 部门名称
     */
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String userName;

    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String phone;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    private String avatar;


    /**
     * 帐号状态（0-停用 1-正常）
     */
    @ApiModelProperty(value = "帐号状态（0-停用 1-正常）")
    private String status;

    /**
     * 最后登录ip
     */
    @ApiModelProperty(value = "最后登录ip")
    private String loginIp;

    @ApiModelProperty(value = "用户密码")
    private String password;
    /**
     * 最后登录时间
     */
    @ApiModelProperty(value = "最后登录时间")
    private LocalDateTime loginDate;


    /**
     * 角色列表
     */
    private List<SysRoleVO> roleList;

    /**
     * 岗位列表
     */
    private List<PostVO> postList;

}
