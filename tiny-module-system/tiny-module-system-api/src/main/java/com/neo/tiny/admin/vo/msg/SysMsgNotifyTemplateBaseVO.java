package com.neo.tiny.admin.vo.msg;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * @Description: 站内信模板基础类
 * @author yqz
 * @date 2023-02-11 14:13:15
 */
@Data
@ApiModel(description = "站内信模板")
public class SysMsgNotifyTemplateBaseVO implements Serializable {

    /**
     * 主键自增
     */
    @ApiModelProperty("主键自增")
    private Long id;

    /**
     * 站内信模板名称
     */
    @ApiModelProperty("站内信模板名称")
    private String templateName;

    /**
     * 站内信模板编码
     */
    @ApiModelProperty("站内信模板编码")
    private String templateCode;

    /**
     * 站内信模板内容
     */
    @ApiModelProperty("站内信模板内容")
    private String templateContent;

    /**
     * 模板类型(0-系统消息，1-通知公告)
     */
    @ApiModelProperty("模板类型(0-系统消息，1-通知公告)")
    private Integer templateType;

    /**
     * 模板参数数组
     */
    @ApiModelProperty("模板参数数组")
    private List<String> templateParams;

    /**
     * 状态（0-关闭，1-正常）
     */
    @ApiModelProperty("状态（0-关闭，1-正常）")
    private String status;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


}
