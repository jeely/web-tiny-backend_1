package com.neo.tiny.admin.sysConfig.bean;

/**
 * @Description: 存储客户端抽象接口
 * @Author: yqz
 * @CreateDate: 2022/11/14 23:17
 */
public interface Storage {

    /**
     * 当前存储客户端是否开启
     *
     * @return 开关状态
     */
    Boolean getEnable();


    /**
     * @return 存储器名称
     */
    String getName();

    /**
     * 存储器名称
     *
     * @return
     */
    String getBucketName();
}
