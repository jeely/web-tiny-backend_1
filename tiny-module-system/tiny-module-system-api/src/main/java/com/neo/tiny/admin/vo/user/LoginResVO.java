package com.neo.tiny.admin.vo.user;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description: 登录结果
 * @Author: yqz
 * @CreateDate: 2022/11/13 19:37
 */
@Data
public class LoginResVO implements Serializable {

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private Long userId;

    /**
     * 用户账户
     */
    @ApiModelProperty("用户账户")
    private String userName;

    /**
     * 用户类型
     */
    @ApiModelProperty("用户类型")
    private Integer userType;

    /**
     * 访问令牌
     */
    @ApiModelProperty("访问令牌")
    private String accessToken;

    /**
     * 刷新令牌
     */
    @ApiModelProperty("刷新令牌")
    private String refreshToken;
    /**
     * 客户端编号
     */
    @ApiModelProperty("客户端编号")
    private String clientId;

    /**
     * 授权范围
     */
    @ApiModelProperty("授权范围")
    private List<String> scopes;

    /**
     * 过期时间
     */
    @ApiModelProperty("过期时间")
    private LocalDateTime expiresTime;
}
