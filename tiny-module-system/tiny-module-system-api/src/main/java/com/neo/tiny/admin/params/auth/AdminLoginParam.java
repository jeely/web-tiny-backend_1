package com.neo.tiny.admin.params.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/7 14:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AdminLoginParam implements Serializable {

    @NotEmpty(message = "用户名不能为空")
    @ApiModelProperty(value = "username", required = true)
    private String username;

    @NotEmpty(message = "密码不能为空")
    @ApiModelProperty(value = "password", required = true)
    private String password;

    @NotEmpty(message = "验证码标识不能为空")
    @ApiModelProperty(value = "uuid", required = true)
    private String uuid;

    @NotEmpty(message = "验证码不能为空")
    @ApiModelProperty(value = "captcha", required = true)
    private String captcha;

}
