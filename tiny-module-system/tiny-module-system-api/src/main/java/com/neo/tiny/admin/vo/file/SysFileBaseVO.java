package com.neo.tiny.admin.vo.file;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 *
 * @Description: 文件存储记录基础类
 * @author yqz
 * @date 2022-12-05 23:32:22
 */
@Data
@ApiModel(description = "文件存储记录")
public class SysFileBaseVO implements Serializable {

    /**
     * id
     */
    @ApiModelProperty("id")
    private Long id;

    /**
     * 文件转义后名称
     */
    @ApiModelProperty("文件转义后名称")
    private String fileName;

    /**
     * 桶名称
     */
    @ApiModelProperty("桶名称")
    private String bucketName;

    /**
     * 文件原始名称
     */
    @ApiModelProperty("文件原始名称")
    private String originalFileName;

    /**
     * fileType
     */
    @ApiModelProperty("fileType")
    private String fileType;

    /**
     * 文件大小
     */
    @ApiModelProperty("文件大小")
    private Long fileSize;

    /**
     * 文件相对路径
     */
    @ApiModelProperty("文件相对路径")
    private String path;


}
