package com.neo.tiny.admin.params.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author yqz
 * @Description 个人修改密码
 * @CreateDate 2023/6/6 9:19
 */
@Data
public class UserProfilePasswordUpdateReq implements Serializable {

    @ApiModelProperty("新密码")
    @NotEmpty(message = "新密码不能为空")
    @Length(min = 4, max = 16, message = "密码长度为 4-16 位")
    private String newPassword;

    @ApiModelProperty("旧密码")
    @NotEmpty(message = "旧密码不能为空")
    private String oldPassword;
}
