package com.neo.tiny.admin.vo.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/11/11 13:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthUserDetails {

    private Long userId;

    private String username;

    private String password;
}
