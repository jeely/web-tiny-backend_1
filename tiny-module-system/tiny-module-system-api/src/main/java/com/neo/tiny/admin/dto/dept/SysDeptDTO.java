package com.neo.tiny.admin.dto.dept;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description: 系统部门
 * @Author: yqz
 * @CreateDate: 2022/10/9 23:33
 */
@Data
public class SysDeptDTO implements Serializable {
    /**
     * 部门id
     */
    @ApiModelProperty("部门id")
    private Long id;

    /**
     * 父部门id
     */
    @NotNull(message = "父级部门不能为空")
    @ApiModelProperty("父部门id")
    private Long parentId;

    /**
     * 祖级列表
     */
    @ApiModelProperty("祖级列表")
    private String ancestors;

    /**
     * 部门名称
     */
    @NotBlank(message = "部门名称不能为空")
    @ApiModelProperty("部门名称")
    private String deptName;

    /**
     * 显示顺序
     */
    @ApiModelProperty("显示顺序")
    private Integer orderNum;

    /**
     * 负责人
     */
    @ApiModelProperty("负责人")
    private Long leader;

    /**
     * 联系电话
     */
    @ApiModelProperty("联系电话")
    private String phone;

    /**
     * 邮箱
     */
    @ApiModelProperty("邮箱")
    private String email;

    /**
     * 部门状态（0正常 1停用）
     */
    @ApiModelProperty("部门状态（0正常 1停用）")
    private String status;
}
