package com.neo.tiny.admin.params.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @Description: 发送短信验证码
 * @Author: yqz
 * @CreateDate: 2022/11/8 23:10
 */
@Data
public class AdminLoginSendSmsParam implements Serializable {

    @ApiModelProperty("手机号")
    @NotEmpty(message = "手机号不能为空")
    private String phone;

}
