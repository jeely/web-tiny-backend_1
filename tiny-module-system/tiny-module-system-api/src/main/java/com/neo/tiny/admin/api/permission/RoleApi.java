package com.neo.tiny.admin.api.permission;

import java.util.Collection;

/**
 * @Description: 角色 API 接口
 * @Author: yqz
 * @CreateDate: 2022/10/4 18:50
 */
public interface RoleApi {

    /**
     * 校验角色们是否有效。如下情况，视为无效：
     * 1. 角色编号不存在
     * 2. 角色被禁用
     *
     * @param ids 角色编号数组 TODO 如果升级为微服务，这里可以改造为调用biz的controller
     */
    void validRoles(Collection<Long> ids);

}
