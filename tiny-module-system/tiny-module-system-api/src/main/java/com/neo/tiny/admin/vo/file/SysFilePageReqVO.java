package com.neo.tiny.admin.vo.file;

import com.neo.tiny.common.common.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * @Description: 文件存储记录分页请求类
 * @author yqz
 * @date 2022-12-05 23:32:22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "文件存储记录")
public class SysFilePageReqVO extends PageParam implements Serializable {

    /**
     * 文件转义后名称
     */
    @ApiModelProperty("文件转义后名称")
    private String originalFileName;

}
