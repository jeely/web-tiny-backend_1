package com.neo.tiny.admin.vo.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/9/27 00:24
 */
@ApiModel("管理后台 - 角色精简信息 Response VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleSimpleRespVO {

    @ApiModelProperty(value = "角色编号", required = true, example = "1024")
    private Long id;

    @ApiModelProperty(value = "角色名称", required = true, example = "tiny")
    private String name;
}
