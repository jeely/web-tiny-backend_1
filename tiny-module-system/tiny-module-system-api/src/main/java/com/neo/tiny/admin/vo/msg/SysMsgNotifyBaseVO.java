package com.neo.tiny.admin.vo.msg;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @Description: 站内信基础类
 * @author yqz
 * @date 2023-02-11 14:13:28
 */
@Data
@ApiModel(description = "站内信")
public class SysMsgNotifyBaseVO implements Serializable {

    /**
     * 接受人id
     */
    @ApiModelProperty("接收人id")
    private Long toUserId;

    /**
     * 接收人名称
     */
    @ApiModelProperty("接收人名称")
    private String toUsername;

    /**
     * 发送者id
     */
    @ApiModelProperty("发送者id")
    private Long fromUserId;

    /**
     * 发送人名称
     */
    @ApiModelProperty("发送人名称")
    private String fromUsername;


    /**
     * 站内信模板id
     */
    @ApiModelProperty("站内信模板id")
    private Long notifyTemplateId;

    /**
     * 站内信模板编码
     */
    @ApiModelProperty("站内信模板编码")
    private String notifyTemplateCode;

    /**
     * 站内信模板类型：0-系统，1-通知公告
     */
    @ApiModelProperty("站内信模板类型：0-系统，1-通知公告")
    private Integer notifyTemplateType;

    /**
     * 发送内容
     */
    @ApiModelProperty("发送内容")
    private String notifyContent;

    /**
     * 阅读状态：0-未读，1-已读
     */
    @ApiModelProperty("阅读状态：0-未读，1-已读")
    private Integer readStatus;

    /**
     * 已读时间
     */
    @ApiModelProperty("已读时间")
    private LocalDateTime readTime;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


}
