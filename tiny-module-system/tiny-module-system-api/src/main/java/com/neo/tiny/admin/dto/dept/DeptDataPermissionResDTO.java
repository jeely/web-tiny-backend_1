package com.neo.tiny.admin.dto.dept;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

/**
 * @author yqz
 * @Description 部门的数据权限 Response DTO
 * @CreateDate 2023/12/14 16:42
 */
@Data
public class DeptDataPermissionResDTO {

    /**
     * 是否可查看全部数据
     */
    private Boolean all;
    /**
     * 是否可查看自己的数据
     */
    private Boolean self;
    /**
     * 可查看的部门编号数组
     */
    private Set<Long> deptIds;

    public DeptDataPermissionResDTO() {
        this.all = false;
        this.self = false;
        this.deptIds = new HashSet<>();
    }

}
