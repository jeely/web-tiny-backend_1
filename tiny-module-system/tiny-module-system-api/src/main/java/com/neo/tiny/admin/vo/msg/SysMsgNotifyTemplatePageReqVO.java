package com.neo.tiny.admin.vo.msg;

import com.neo.tiny.common.common.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description: 站内信模板分页请求类
 * @author yqz
 * @date 2023-02-11 14:13:15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "站内信模板")
public class SysMsgNotifyTemplatePageReqVO extends PageParam implements Serializable {


    /**
     * 站内信模板名称
     */
    @ApiModelProperty("站内信模板名称")
    private String templateName;

    /**
     * 站内信模板编码
     */
    @ApiModelProperty("站内信模板编码")
    private String templateCode;


    /**
     * 状态（0-正常，1-关闭）
     */
    @ApiModelProperty("状态（0-正常，1-关闭）")
    private String status;

    @ApiModelProperty("创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private List<LocalDateTime> createTime;

}
