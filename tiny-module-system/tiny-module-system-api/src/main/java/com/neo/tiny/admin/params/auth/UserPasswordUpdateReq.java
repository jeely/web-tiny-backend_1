package com.neo.tiny.admin.params.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yqz
 * @Description 管理员重置用户密码
 * @CreateDate 2023/6/6 11:04
 */
@Data
public class UserPasswordUpdateReq implements Serializable {
    @ApiModelProperty("用户id")
    @NotNull(message = "用户id不能为空")
    private Long userId;

    @ApiModelProperty("新密码")
    @NotNull(message = "新密码不能为空")
    @Length(min = 4, max = 16, message = "密码长度为 4-16 位")
    private String newPassword;
}
