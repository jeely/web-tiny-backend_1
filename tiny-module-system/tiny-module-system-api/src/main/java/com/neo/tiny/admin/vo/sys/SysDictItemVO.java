package com.neo.tiny.admin.vo.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/9/4 11:42
 */
@Data
public class SysDictItemVO implements Serializable {
    /**
     * 字典编码
     */
    @ApiModelProperty("字典编码")
    private Long id;

    /**
     * sys_dict_type.id
     */
    @ApiModelProperty("sys_dict_type.id")
    private Integer dictId;

    /**
     * 字典标签
     */
    @ApiModelProperty("字典标签")
    private String dictLabel;

    /**
     * 字典键值
     */
    @ApiModelProperty("字典键值")
    private String dictValue;

    /**
     * 字典类型
     */
    @ApiModelProperty("字典类型")
    private String dictType;

    /**
     * 颜色类型
     */
    @ApiModelProperty("颜色类型")
    private String colorType;

    /**
     * 解释
     */
    @ApiModelProperty("解释")
    private String description;

    /**
     * 字典排序
     */
    @ApiModelProperty("字典排序")
    private Long dictSort;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;

}
