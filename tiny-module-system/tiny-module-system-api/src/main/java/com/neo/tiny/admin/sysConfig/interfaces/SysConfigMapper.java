package com.neo.tiny.admin.sysConfig.interfaces;

import com.neo.tiny.admin.entity.SysConfig;
import com.neo.tiny.query.BaseMapperX;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统设置
 *
 * @author yqz
 * @date 2022-09-03 11:10:52
 */
@Mapper
public interface SysConfigMapper extends BaseMapperX<SysConfig> {

    /**
     * 根据配置key，查询系统配置项
     *
     * @param key key
     * @return 系统配置
     */
    default SysConfig selectConfigByKey(String key) {
        return selectOne(SysConfig::getConfigKey, key);
    }

}
