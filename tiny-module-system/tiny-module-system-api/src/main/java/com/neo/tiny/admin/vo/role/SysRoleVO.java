package com.neo.tiny.admin.vo.role;

import com.baomidou.mybatisplus.annotation.TableField;
import com.neo.tiny.common.enums.DataScopeEnum;
import com.neo.tiny.handler.JsonLongSetTypeHandler;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 19:48
 */
@Data
public class SysRoleVO implements Serializable {


    @ApiModelProperty("角色编号")
    private Long id;

    @NotBlank(message = "角色名称 不能为空")
    @ApiModelProperty("角色名称")
    private String roleName;

    @NotBlank(message = "角色标识 不能为空")
    @ApiModelProperty("角色标识")
    private String roleKey;

    @NotBlank(message = "角色描述 不能为空")
    @ApiModelProperty("角色描述")
    private String roleDesc;

    @ApiModelProperty("显示顺序")
    private String roleSort;

    /**
     * 数据范围
     * <p>
     * 枚举 {@link DataScopeEnum}
     */
    private Integer dataScope;

    /**
     * 数据范围(指定部门数组)
     * <p>
     * 适用于 {@link #dataScope} 的值为 {@link DataScopeEnum#DEPT_CUSTOM} 时
     */
    private Set<Long> dataScopeDeptIds;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

}
