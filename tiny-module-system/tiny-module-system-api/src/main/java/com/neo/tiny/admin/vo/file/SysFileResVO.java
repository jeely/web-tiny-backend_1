package com.neo.tiny.admin.vo.file;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 *
 * @Description: 文件存储记录响应类
 * @author yqz
 * @date 2022-12-05 23:32:22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "文件存储记录")
public class SysFileResVO extends SysFileBaseVO implements Serializable {

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    private LocalDateTime updateTime;


}
