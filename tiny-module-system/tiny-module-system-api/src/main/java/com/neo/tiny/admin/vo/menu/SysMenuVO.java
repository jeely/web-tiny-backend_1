package com.neo.tiny.admin.vo.menu;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/8 22:37
 */
@Data
public class SysMenuVO implements Serializable {

    /**
     * 菜单ID
     */
    @ApiModelProperty("菜单id")
    private Long id;

    /**
     * 菜单名称
     */
    @NotBlank(message = "菜单名称不能为空")
    @ApiModelProperty("菜单名称")
    private String menuName;

    /**
     * 菜单权限标识
     */
    @ApiModelProperty("菜单权限标识")
    private String perms;


    /**
     * 前端URL
     */
    @ApiModelProperty("前端路由标识路径")
    private String path;

    /**
     * 组件地址
     */
    @ApiModelProperty("组件地址")
    private String component;

    /**
     * 父菜单ID
     */
    @NotNull(message = "菜单父ID不能为空")
    @ApiModelProperty("菜单父id")
    private Long parentId;

    /**
     * 图标
     */
    @ApiModelProperty("菜单图标")
    private String icon;


    /**
     * 路由缓冲
     */
    @ApiModelProperty("路由缓冲")
    private Integer keepAlive;


    /**
     * 菜单类型 （菜单类型（M目录 C菜单 F按钮 T顶部菜单））
     */
    @ApiModelProperty("菜单类型不能为空")
    private String menuType;

    /**
     * 路由参数
     */
    @ApiModelProperty("路由参数")
    private String query;


    /**
     * 是否为外链 0是 1否
     */
    @ApiModelProperty("是否为外链 0是 1否")
    private Integer isFrame;

    /**
     * 排序值
     */
    @ApiModelProperty("排序值")
    private Integer sortOrder;
}
