package com.neo.tiny.admin.vo.dept;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author yqz
 * @Description 部门树
 * @CreateDate 2023/12/14 18:05
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysDeptTreeVO extends SysDeptVO implements Serializable {

    @ApiModelProperty("比重、排序")
    private Integer weight;

    @ApiModelProperty("部门名称")
    private String name;

    private List<SysDeptTreeVO> children;

}
