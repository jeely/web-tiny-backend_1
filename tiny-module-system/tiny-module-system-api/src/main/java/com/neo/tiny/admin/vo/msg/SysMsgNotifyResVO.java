package com.neo.tiny.admin.vo.msg;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @Description: 站内信响应类
 * @author yqz
 * @date 2023-02-11 14:13:28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "站内信")
public class SysMsgNotifyResVO extends SysMsgNotifyBaseVO implements Serializable {

    @ApiModelProperty(value = "公告ID")
    private Integer id;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

}
