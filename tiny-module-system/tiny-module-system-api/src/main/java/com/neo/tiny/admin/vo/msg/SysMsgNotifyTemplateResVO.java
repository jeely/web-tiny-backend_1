package com.neo.tiny.admin.vo.msg;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @Description: 站内信模板响应类
 * @author yqz
 * @date 2023-02-11 14:13:15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "站内信模板")
public class SysMsgNotifyTemplateResVO extends SysMsgNotifyTemplateBaseVO implements Serializable {
    /**
     * 创建时间
     */
    private LocalDateTime createTime;

}
