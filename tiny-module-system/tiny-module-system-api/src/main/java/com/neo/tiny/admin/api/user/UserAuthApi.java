package com.neo.tiny.admin.api.user;


import com.neo.tiny.admin.vo.user.AuthUserDetails;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/11/11 11:52
 */
public interface UserAuthApi {


    /**
     * 通过用户名喝密码认证
     *
     * @param username 用户名
     * @param password 密码
     * @return 系统用户
     */
    AuthUserDetails authenticate(String username, String password);
}
