package com.neo.tiny.admin.sysConfig.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 存储配置
 * @Author: yqz
 * @CreateDate: 2022/11/14 22:51
 */
@Data
public class StorageClientConfig {

    private Local local;

    private MinIO minIo;

    private AliyunOSS aliyun;

    private Storage activeStorage;

    @Data
    public static class Local implements Storage {
        // 开关
        @ApiModelProperty("开关")
        private Boolean enable;
        // 存储器名称
        @ApiModelProperty("存储器名称")
        private String name;
        // url请求的路径
        @ApiModelProperty("url请求的路径")
        private String address;
        // 图片存放的真实路径
        @ApiModelProperty("图片存放的真实路径")
        private String storagePath;
        @Override
        public String getBucketName() {
            return this.name;
        }
    }

    @Data
    public static class MinIO implements Storage {
        // 开关
        @ApiModelProperty("开关")
        private Boolean enable;
        // 存储器名称
        @ApiModelProperty("存储器名称")
        private String name;
        private String accessKey;
        private String secretKey;
        private String endpoint;
        private String bucketName;
    }
    @Data
    public static class AliyunOSS implements Storage {
        // 开关
        @ApiModelProperty("开关")
        private Boolean enable;

        // 存储器名称
        @ApiModelProperty("存储器名称")
        private String name;

        private String accessKey;

        private String secretKey;

        private String endpoint;

        private String bucketName;

        @ApiModelProperty("文件前缀")
        private String filePre;
    }
}
