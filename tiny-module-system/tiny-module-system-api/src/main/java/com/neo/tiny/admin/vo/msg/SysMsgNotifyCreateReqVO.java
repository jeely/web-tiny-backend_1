package com.neo.tiny.admin.vo.msg;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 创建站内信
 * @author yqz
 * @date 2023-02-11 14:13:28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "站内信")
public class SysMsgNotifyCreateReqVO extends SysMsgNotifyBaseVO implements Serializable {


}
