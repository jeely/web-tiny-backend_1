package com.neo.tiny.admin.vo.msg;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Map;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2023/2/11 22:31
 */
@Data
public class SysMsgNotifySendReq implements Serializable {

    @ApiModelProperty("用户id")
    @NotNull(message = "用户id不能为空")
    private Long userId;

    @ApiModelProperty("模板编码")
    @NotEmpty(message = "模板编码不能为空")
    private String templateCode;

    @ApiModelProperty("模板参数")
    private Map<String, Object> params;
}
