package com.neo.tiny.admin.vo.msg;

import com.neo.tiny.common.common.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description: 站内信分页请求类
 * @author yqz
 * @date 2023-02-11 14:13:28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "站内信")
public class SysMsgNotifyPageReqVO extends PageParam implements Serializable {

    /**
     * 接收人名称
     */
    @ApiModelProperty("接收人名称")
    private String toUsername;


    /**
     * 站内信模板编码
     */
    @ApiModelProperty("站内信模板编码")
    private String notifyTemplateCode;


    /**
     * 站内信模板类型：0-系统，1-通知公告
     */
    @ApiModelProperty("站内信模板类型：0-系统，1-通知公告")
    private Integer notifyTemplateType;

    @ApiModelProperty("创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private List<LocalDateTime> createTime;


}
