package com.neo.tiny.admin.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/8 19:15
 */
@Data
public class UserInfoDTO implements Serializable {
    /**
     * 主键ID
     */
    @ApiModelProperty(value = "用户id")
    private Long id;


    /**
     * 部门id
     */
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String userName;
    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String phone;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    private String avatar;

    /**
     * 岗位编号数组
     */
    @ApiModelProperty(value = "岗位编号数组")
    private Set<Long> postIds;

    /**
     * 帐号状态（0-停用 1-正常）
     */
    @ApiModelProperty(value = "帐号状态（0-停用 1-正常）")
    private String status;
}
