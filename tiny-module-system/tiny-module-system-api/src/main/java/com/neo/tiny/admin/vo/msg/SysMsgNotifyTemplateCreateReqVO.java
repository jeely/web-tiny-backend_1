package com.neo.tiny.admin.vo.msg;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 创建站内信模板
 * @author yqz
 * @date 2023-02-11 14:13:15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "站内信模板")
public class SysMsgNotifyTemplateCreateReqVO extends SysMsgNotifyTemplateBaseVO implements Serializable {


}
