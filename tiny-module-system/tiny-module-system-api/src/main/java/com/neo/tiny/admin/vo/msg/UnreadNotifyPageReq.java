package com.neo.tiny.admin.vo.msg;

import com.neo.tiny.common.common.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @Description: 未读消息分页查询
 * @Author: yqz
 * @CreateDate: 2023/2/19 11:14
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UnreadNotifyPageReq extends PageParam implements Serializable {


}
