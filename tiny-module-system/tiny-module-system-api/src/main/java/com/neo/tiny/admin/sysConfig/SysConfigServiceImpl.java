package com.neo.tiny.admin.sysConfig;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.admin.entity.SysConfig;
import com.neo.tiny.admin.sysConfig.bean.StorageClientConfig;
import com.neo.tiny.admin.sysConfig.interfaces.SysConfigMapper;
import com.neo.tiny.admin.sysConfig.interfaces.SysConfigService;
import com.neo.tiny.common.exception.WebApiException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 系统设置
 *
 * @author yqz
 * @date 2022-09-03 11:10:52
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements SysConfigService {

    private final SysConfigMapper configMapper;

    @Override
    public StorageClientConfig loadStoreConfig(String key) {
        if (StrUtil.isBlank(key)) {
            throw new WebApiException("系统配置key不能为空");
        }
        SysConfig sysConfig = configMapper.selectConfigByKey(key);

        if (BeanUtil.isEmpty(sysConfig)) {
            return null;
        }
        String configValue = sysConfig.getConfigValue();

        StorageClientConfig storageConfig = JSONUtil.toBean(configValue, StorageClientConfig.class);
        log.info("存储配置：{}", storageConfig);

        return storageConfig;
    }
}
