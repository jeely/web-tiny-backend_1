package com.neo.tiny.admin.vo.role;

import com.neo.tiny.common.enums.DataScopeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * @author yqz
 * @Description 更新角色数据权限
 * @CreateDate 2023/12/18 16:43
 */
@Data
public class RoleDataScopeReqVO implements Serializable {
    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 数据范围
     * <p>
     * 枚举 {@link DataScopeEnum}
     */
    @ApiModelProperty("1-全部数据权限，2-指定部门数据权限，3-部门数据权限，4-部门及以下数据权限，5-仅本人数据权限")
    private Integer dataScope;

    /**
     * 数据范围(指定部门数组)
     * <p>
     * 适用于 {@link #dataScope} 的值为 {@link DataScopeEnum#DEPT_CUSTOM} 时
     */
    @ApiModelProperty("数据范围(指定部门数组)")
    private Set<Long> dataScopeDeptIds;


}
