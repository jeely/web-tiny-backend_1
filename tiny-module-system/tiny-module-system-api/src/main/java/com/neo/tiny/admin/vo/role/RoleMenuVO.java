package com.neo.tiny.admin.vo.role;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 角色菜单
 * @Author: yqz
 * @CreateDate: 2022/8/13 15:50
 */
@Data
public class RoleMenuVO implements Serializable {

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 菜单列表
     */
    private String menuIds;

}
