# 搜索模块

## 一、介绍

该模块为脚手架提供搜索功能，主要业务在`tiny-search-es-biz`	模块，该模块使用`es`作为搜索引擎，如果使用该模块则在`tiny-server`的pom.xml中引入即可。

### 1、目录结构

```sh
├── tiny-search-db-api // 搜素公共资源，此模块为搜索模块的公共模块，有部分连接数据库的代码，暂时用做同步数据库数据至mysql，后续如若加入mongoDB也可以引入该模块
└── tiny-search-es-biz // 此模块elasticsearch作为搜索引擎，提供中文、拼音分词搜索。
```

## 二、安装ElasticSearch、中文、拼音分词插件

### 1、下载地址

本模块暂时使用es7.6.2，现在地址如下：

```
elasticsearch：
https://www.elastic.co/cn/downloads/elasticsearch
中文插件：
https://github.com/medcl/elasticsearch-analysis-ik
拼音插件：
https://github.com/medcl/elasticsearch-analysis-pinyin
```

### 2、安装步骤

#### 1) 新建用户elk

es需要用非root账户启动，

```
[root@localhost ~]# groupadd elk
[root@localhost ~]# useradd -g elk elk
[root@localhost ~]# passwd elk
更改用户 elk 的密码 。
新的 密码：
重新输入新的 密码：
passwd：所有的身份验证令牌已经成功更新。
[root@localhost ~]# chown -R elk:elk /opt/es/ -R 
[root@localhost ~]# su elk
用户/密码
elk/elk
```

#### 2)配置

创建安装目录，暂时安装到`/opt/es`目录下

```sh
mkdir /opt/es
```

解压

```sh
tar -zxvf elasticsearch-7.6.2-linux-x86_64.tar.gz
```

修改配置文件

vim elasticsearch.yml

```yml
cluster.name: es-Cluster    #ELK的集群名称，名称相同即属于是同一个集群
node.name: es-node1    #本机在集群内的节点名称
path.data: /opt/es/data    #数据存放目录
path.logs: /opt/es/data/log    #日志保存目录
bootstrap.memory_lock: true    #服务启动的时候锁定足够的内存，防止数据写入swap
network.host: 192.168.31.10    #监听的IP地址
http.port: 9200   #服务监听的端口
discovery.seed_hosts: 192.168.31.10  #单播配置一台即可
cluster.initial_master_nodes: es-node1  
```

如果报错：

```sh
{
    "statusCode": 500,
    "error": "Internal Server Error",
    "message": "[parent] Data too large, data for [<http_request>] would be [1983350952/1.8gb], which is larger than the limit of [1973865676/1.8gb], real usage: [1983350952/1.8gb], new bytes reserved: [0/0b], usages [request=0/0b, fielddata=3765/3.6kb, in_flight_requests=0/0b, accounting=361115778/344.3mb]: [circuit_breaking_exception] [parent] Data too large, data for [<http_request>] would be [1983350952/1.8gb], which is larger than the limit of [1973865676/1.8gb], real usage: [1983350952/1.8gb], new bytes reserved: [0/0b], usages [request=0/0b, fielddata=3765/3.6kb, in_flight_requests=0/0b, accounting=361115778/344.3mb], with { bytes_wanted=1983350952 & bytes_limit=1973865676 & durability=\"PERMANENT\" }"
}
```

添加以下配置：vim /config/elasticsearch.yml

```
# 缓存回收大小，无默认值
# 有了这个设置，最久未使用（LRU）的 fielddata 会被回收为新数据腾出空间
# 控制fielddata允许内存大小，达到HEAP 20% 自动清理旧cache
indices.fielddata.cache.size: 20%
indices.breaker.total.use_real_memory: false
# fielddata 断路器默认设置堆的 60% 作为 fielddata 大小的上限。
indices.breaker.fielddata.limit: 40%
# request 断路器估算需要完成其他请求部分的结构大小，例如创建一个聚合桶，默认限制是堆内存的 40%。
indices.breaker.request.limit: 40%
# total 揉合 request 和 fielddata 断路器保证两者组合起来不会使用超过堆内存的 70%(默认值)。
indices.breaker.total.limit: 95%
```

配置内存限制

```sh
[root@localhost config]# vim jvm.options 
配置如下
-Xms1g
-Xmx1g
```

使用elk启动

```sh
su elk
cd /opt/es/elasticsearch-7.6.2/bin
./elasticsearch
```

如果启动报错：

```
ERROR: [3] bootstrap checks failed
[1]: max file descriptors [4096] for elasticsearch process is too low, increase to at least [65536]
[2]: memory locking requested for elasticsearch process but memory is not locked
[3]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]
```

需要修改文件的最大打开数以及内存锁定

修改内核参数：

```
[root@localhost bin]# vim /etc/security/limits.conf
```

配置如下：

```
* soft nofile 65536
* hard nofile 131072
* soft memlock unlimited
* hard memlock unlimited
```

修改最大线程数

```
[root@localhost bin]# vim /etc/security/limits.d/20-nproc.conf
```

配置如下：

```
*     soft    nproc    unlimited
```

```
[root@localhost bin]# vim /etc/sysctl.conf
```

配置如下：

```
vm.max_map_count = 655360
```

令配置生效：需要重新登录用户，才会生效

```
[root@localhost bin]# sysctl -p
```

启动服务

```sh
[root@localhost ~]# su elk
cd /opt/zs/elasticsearch-7.6.2/bin/
[elk@localhost bin]$ nohup ./elasticsearch & #后台运行
[elk@localhost bin]$ netstat -nltup |grep java
```

#### 3)安装分词器

中文分词器，将下载的中文分词插件`elasticsearch-analysis-ik-7.6.2.zip`移动至`elasticsearch-7.6.2\plugins\ik`中并解压，如下：

```sh
[root@localhost ik]# pwd
/opt/es/elasticsearch-7.6.2/plugins/ik
[root@localhost ik]# ll
-rw-r--r--. 1 elk elk 263965 May  6  2018 commons-codec-1.9.jar
-rw-r--r--. 1 elk elk  61829 May  6  2018 commons-logging-1.2.jar
drwxr-xr-x. 2 elk elk   4096 Dec 25  2019 config
-rw-r--r--. 1 elk elk  54599 Apr  1  2020 elasticsearch-analysis-ik-7.6.2.jar
-rw-r--r--. 1 elk elk 736658 May  6  2018 httpclient-4.5.2.jar
-rw-r--r--. 1 elk elk 326724 May  6  2018 httpcore-4.4.4.jar
-rw-r--r--. 1 elk elk   1805 Apr  1  2020 plugin-descriptor.properties
-rw-r--r--. 1 elk elk    125 Apr  1  2020 plugin-security.policy
```

拼音分词器，将下载的中文分词插件`elasticsearch-analysis-pinyin-7.6.2.zip`移动至`elasticsearch-7.6.2\plugins\pinyin`中并解压，如下：

```sh
[root@localhost pinyin]# pwd
/opt/es/elasticsearch-7.6.2/plugins/pinyin
[root@localhost pinyin]# ll
-rw-r--r--. 1 elk elk   26051 Apr  1  2020 elasticsearch-analysis-pinyin-7.6.2.jar
-rw-r--r--. 1 elk elk 8091448 Apr  1  2020 nlp-lang-1.7.jar
-rw-r--r--. 1 elk elk    1822 Apr  1  2020 plugin-descriptor.properties
```

启动es，如下：

编写boot.sh启动脚本

```sh
vim boot.sh
nohup ./elasticsearch  &
```

启动后成功加载插件

```sh
[2022-10-22T23:43:38,336][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [aggs-matrix-stats]
[2022-10-22T23:43:38,337][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [analysis-common]
[2022-10-22T23:43:38,337][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [flattened]
[2022-10-22T23:43:38,337][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [frozen-indices]
[2022-10-22T23:43:38,337][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [ingest-common]
[2022-10-22T23:43:38,337][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [ingest-geoip]
[2022-10-22T23:43:38,337][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [ingest-user-agent]
[2022-10-22T23:43:38,338][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [lang-expression]
[2022-10-22T23:43:38,338][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [lang-mustache]
[2022-10-22T23:43:38,338][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [lang-painless]
[2022-10-22T23:43:38,338][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [mapper-extras]
[2022-10-22T23:43:38,338][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [parent-join]
[2022-10-22T23:43:38,338][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [percolator]
[2022-10-22T23:43:38,338][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [rank-eval]
[2022-10-22T23:43:38,339][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [reindex]
[2022-10-22T23:43:38,339][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [repository-url]
[2022-10-22T23:43:38,339][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [search-business-rules]
[2022-10-22T23:43:38,339][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [spatial]
[2022-10-22T23:43:38,339][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [transform]
[2022-10-22T23:43:38,339][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [transport-netty4]
[2022-10-22T23:43:38,339][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [vectors]
[2022-10-22T23:43:38,340][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-analytics]
[2022-10-22T23:43:38,340][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-ccr]
[2022-10-22T23:43:38,340][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-core]
[2022-10-22T23:43:38,340][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-deprecation]
[2022-10-22T23:43:38,340][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-enrich]
[2022-10-22T23:43:38,340][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-graph]
[2022-10-22T23:43:38,340][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-ilm]
[2022-10-22T23:43:38,340][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-logstash]
[2022-10-22T23:43:38,341][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-ml]
[2022-10-22T23:43:38,341][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-monitoring]
[2022-10-22T23:43:38,341][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-rollup]
[2022-10-22T23:43:38,341][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-security]
[2022-10-22T23:43:38,341][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-sql]
[2022-10-22T23:43:38,341][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-voting-only-node]
[2022-10-22T23:43:38,341][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded module [x-pack-watcher]
[2022-10-22T23:43:38,342][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded plugin [analysis-ik]
[2022-10-22T23:43:38,342][INFO ][o.e.p.PluginsService     ] [elk-node1] loaded plugin [analysis-pinyin]
```

### 3、或使用docker安装

下载elasticsearch 7.6.2的docker镜像；

```bash
docker pull elasticsearch:.7.6.2
```

修改虚拟内存区域大小，否则会因为过小而无法启动；

```bash
sysctl -w vm.max_map_count=262144
```

创建elasticsearch.yml配置文件

```sh
echo "http.host: 0.0.0.0" >> /opt/docker/elasticsearch/config/elasticsearch.yml

并加入一下配置，放开跨域，否则elasticsearch-header无法使用
http.cors.enabled: true
http.cors.allow-origin: "*"
```

使用docker命令启动；

```bash
docker run -p 9200:9200 -p 9300:9300 --name elasticsearch \
-e "discovery.type=single-node" \
-e "cluster.name=elasticsearch" \
-v /opt/docker/elasticsearch/plugins:/usr/share/elasticsearch/plugins \
-v /opt/docker/elasticsearch/data:/usr/share/elasticsearch/data \
-v /opt/docker/elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v /etc/localtime:/etc/localtime:ro \
-d elasticsearch:7.6.2
```

启动时会发现`/usr/share/elasticsearch/data`目录没有访问权限，只需要修改该目录的权限，再重新启动即可；

```bash
chmod -R 777 /opt/docker/elasticsearch/data/
```

安装中文、pinyin分词器，并重新启动。

将两个分词器分别放入/opt/docker/elasticsearch/plugins目录下的ik、pinyin目录

```sh
[root@localhost plugins]# pwd
/opt/docker/elasticsearch/plugins
[root@localhost plugins]# tree -L 2
├── ik
│   ├── commons-codec-1.9.jar
│   ├── commons-logging-1.2.jar
│   ├── config
│   ├── elasticsearch-analysis-ik-7.6.2.jar
│   ├── httpclient-4.5.2.jar
│   ├── httpcore-4.4.4.jar
│   ├── plugin-descriptor.properties
│   └── plugin-security.policy
└── pinyin
    ├── elasticsearch-analysis-pinyin-7.6.2.jar
    ├── nlp-lang-1.7.jar
    └── plugin-descriptor.properties
```

访问会返回版本信息：http://192.168.31.10:9200/



## 三、编写代码

es的java客户端有多种，目前使用了spring-data、ElasticsearchRestTemplate、RestHighLevelClient

### 1、引入依赖

```xml
<!--Elasticsearch相关依赖-->
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-elasticsearch</artifactId>
</dependency>
```

### 2、编写实体类映射json

在resources下创建es目录

将setting.json、mapping.json放入该目录

`setting.json`，具体释义可参考[elasticsearch-analysis-pinyin参数解读 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/421031084)

```json
{
  "analysis": {
    "analyzer": {
      "ik_es_smart_pinyin": {
        "type": "custom",
        "tokenizer": "ik_smart",
        "filter": [
          "my_pinyin"
        ]
      },
      "ik_es_max_word_pinyin": {
        "type": "custom",
        "tokenizer": "ik_max_word",
        "filter": [
          "my_pinyin"
        ]
      }
    },
    "filter": {
      "my_pinyin": {
        "type": "pinyin",
        "keep_first_letter": true,
        "keep_separate_first_letter": false,
        "keep_full_pinyin": true,
        "keep_original": false,
        "limit_first_letter_length": 16,
        "lowercase": true
      }
    }
  }
}
```

`mapping.json`

```json
{
  "properties": {
    "unid": {
      "type": "text"
    },
    "catalogUnid": {
      "type": "text"
    },
    "serviceName": {
      "type": "text",
      "analyzer": "ik_es_max_word_pinyin",
      "search_analyzer": "ik_es_smart_pinyin"
    },
    "keyWords": {
      "type": "text",
      "analyzer": "ik_es_max_word_pinyin",
      "search_analyzer": "ik_es_smart_pinyin"
    },
    "serviceCode": {
      "type": "text"
    }
  }
}
```

搜索实体类

```java
package com.neo.tiny.search.es.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import java.io.Serializable;

/**
 * @Description: indexName--索引库名次，mysql中数据库的概念,type -- 文档类型，mysql中表的概念,
 * shards -- 默认分片数, replicas -- 默认副本数量
 * @Author: yqz
 * @CreateDate: 2022/10/20 23:07
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Mapping(mappingPath = "/es/mapping.json")
@Setting(settingPath = "/es/settings.json")
@Document(indexName = "pre_index")
public class EsSearchPre implements Serializable {

    /**
     * 主键
     */
    @Id
    private String unid;

    /**
     * 所属目录清单UNID
     */
//    @Field(type = FieldType.Keyword)
    private String catalogUnid;

    /**
     * 事项名称
     */
//    @Field(analyzer = "pinyin", type = FieldType.Text)
    private String serviceName;

    /**
     * 事项关键词
     */
    @ApiModelProperty("事项关键词")
//    @Field(analyzer = "pinyin", type = FieldType.Text)
    private String keyWords;

    /**
     * 事项编码
     */
//    @Field(type = FieldType.Keyword)
    private String serviceCode;
}

```

### 3、配置写入索引代码

使用`elasticsearchRestTemplate`对es进行操作时，ElasticsearchRestTemplate不读@Filed注解，所以你在@Field里面写再多代码也没用，以下代码为错误代码配置

```java
@Field(analyzer = "ik_smart", type = FieldType.Text)
private String commentText;
```

ElasticsearchRestTemplate在创建索引的时候不读@Mapping，也就是需要两步才能创建完整的索引

 1、创建索引 

2、更新字段mapping

ElasticsearchRestTemplate 创建的索引名只读@Document注解，所以必须包含@Document注解，以下代码为c错误示例：

```java
@Mapping(mappingPath="mapper/mapping.json")
@Data
public class MyDocument {
    @Id
    private String id;
    private String title;
}
```

创建索引时指定配置

```java
    /**
     * 初始化数据时创建映射
     *
     * @param clz es 实体类
     * @param <T> 范型约束
     */
    private <T> void createIndexMapping(Class<T> clz) {
        //创建索引 可以指定在postconstruct内
        IndexOperations ops = elasticsearchRestTemplate.indexOps(clz);
        if (ops.exists()) {
            ops.delete();
        }
        ops.create();
        ops.refresh();
        Map<String, Object> settings = ops.getSettings();
        log.info("es 索引setting设置详情：{}", JSONUtil.toJsonStr(settings));
        Document mapping = ops.createMapping();
        log.info("es 索引setting 映射文件：{}", JSONUtil.toJsonStr((mapping)));
        ops.putMapping(mapping);
    }
```



### 4、高亮搜索

使用ElasticsearchRestTemplate进行高亮搜索，即为关键词进行`<span style=color:red>item</span>`标签包裹，具体代码如下：

```java
    public Page<EsSearchPre> highLightSearch(String keyword, Integer pageNum, Integer pageSize) {

        // 分页
        Pageable pageable = PageRequest.of(pageNum, pageSize);

        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        Optional.ofNullable(keyword).ifPresent(kw ->
                queryBuilder.withQuery(QueryBuilders.multiMatchQuery(kw, "serviceName", "keyWords")));

        // 配置高亮
        HighlightBuilder highlightBuilder = new HighlightBuilder()
                .field("serviceName")
                // 关闭多处高亮
                .requireFieldMatch(false)
                .preTags("<span style=color:red>")
                .postTags("</span>");

        // 构建搜索条件
        NativeSearchQuery query = queryBuilder.withHighlightBuilder(highlightBuilder).withPageable(pageable).build();

        log.info("es-DSL:【{}】", query.getQuery());
        SearchHits<EsSearchPre> resList = elasticsearchRestTemplate.search(query, EsSearchPre.class);


        if (resList.getTotalHits() == 0) {
            return new PageImpl<>(new ArrayList<>(), pageable, 0);
        }

        List<EsSearchPre> preList = resList.getSearchHits().stream().map(hit -> {
            Map<String, List<String>> highlightFields = hit.getHighlightFields();
            EsSearchPre searchPre = hit.getContent();

            List<String> serviceNames = highlightFields.get("serviceName");
            if (CollUtil.isNotEmpty(serviceNames)) {
                // 将添加高亮span标签的目标字段重新放回对象
                serviceNames.forEach(searchPre::setServiceName);
            }
            return searchPre;
        }).collect(Collectors.toList());
        return new PageImpl<>(preList, pageable, resList.getTotalHits());
    }

```





