package com.neo.tiny.search.es.domain.es;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import java.io.Serializable;

/**
 * @Description: indexName--索引库名次，mysql中数据库的概念,type -- 文档类型，mysql中表的概念,
 * shards -- 默认分片数, replicas -- 默认副本数量
 * @Author: yqz
 * @CreateDate: 2022/10/20 23:07
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Mapping(mappingPath = "/es/mapping.json")
@Setting(settingPath = "/es/settings.json")
@Document(indexName = "pre_index")
public class EsSearchPre implements Serializable {

    /**
     * 主键
     */
    @Id
    private String unid;

    /**
     * 所属目录清单UNID
     */
//    @Field(type = FieldType.Keyword)
    private String catalogUnid;

    /**
     * 事项名称
     */
    @Field(analyzer = "ik_max_word", type = FieldType.Text)
    private String serviceName;

    /**
     * 事项关键词
     */
    @ApiModelProperty("事项关键词")
    @Field(analyzer = "ik_max_word", type = FieldType.Text)
    private String keyWords;

    @ApiModelProperty("带高亮标签的结果字段")
    private String highLightServiceName;

    /**
     * 事项编码
     */
//    @Field(type = FieldType.Keyword)
    private String serviceCode;
}
