package com.neo.tiny.search.es.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.search.es.domain.db.EsPreServiceDO;
import com.neo.tiny.search.es.mapper.EsPreServiceMapper;
import com.neo.tiny.search.es.service.EsPreServiceService;
import org.springframework.stereotype.Service;

/**
 *
 * @Description: 事项基本信息
 * @Author: yqz
 * @CreateDate: 2022-10-20 22:57:57
 */
@Service
public class EsPreServiceServiceImpl extends ServiceImpl<EsPreServiceMapper, EsPreServiceDO> implements EsPreServiceService {

}
