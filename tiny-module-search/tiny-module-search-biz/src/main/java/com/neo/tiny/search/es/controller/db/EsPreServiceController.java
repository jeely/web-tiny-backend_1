package com.neo.tiny.search.es.controller.db;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.search.api.vo.db.EsPreServiceCreateReqVO;
import com.neo.tiny.search.api.vo.db.EsPreServicePageReqVO;
import com.neo.tiny.search.api.vo.db.EsPreServiceResVO;
import com.neo.tiny.search.api.vo.db.EsPreServiceUpdateReqVO;
import com.neo.tiny.search.es.domain.db.EsPreServiceDO;
import com.neo.tiny.search.es.service.EsPreServiceService;
import com.neo.tiny.util.MyBatisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Description: 事项基本信息
 * @Author: yqz
 * @CreateDate: 2022-10-20 22:57:57
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/sys-es-pre-service")
@Api(tags = "事项基本信息管理")
public class EsPreServiceController {

    private final EsPreServiceService sysEsPreServiceService;

    /**
     * 分页查询
     *
     * @param sysEsPreServicePageReqVO 事项基本信息
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<IPage<EsPreServiceResVO>> getSysEsPreServicePage(@Valid EsPreServicePageReqVO sysEsPreServicePageReqVO) {

        QueryWrapper<EsPreServiceDO> wrapper = new QueryWrapper<>(BeanUtil.copyProperties(sysEsPreServicePageReqVO, EsPreServiceDO.
                class));

        return ResResult.success(CommonPage
                .restPage(sysEsPreServiceService
                        .page(MyBatisUtils.buildPage(sysEsPreServicePageReqVO), wrapper), EsPreServiceResVO.class));
    }


    /**
     * 通过id查询事项基本信息
     *
     * @param unid id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{unid}")
    public ResResult<EsPreServiceResVO> getById(@PathVariable("unid") String unid) {
        return ResResult.success(BeanUtil.copyProperties(sysEsPreServiceService.getById(unid), EsPreServiceResVO.class));
    }

    /**
     * 新增事项基本信息
     *
     * @param sysEsPreServiceCreateReqVO 事项基本信息
     * @return ResResult
     */
    @ApiOperation("新增事项基本信息")
    @PostMapping("/create")
    public ResResult<String> save(@Valid @RequestBody EsPreServiceCreateReqVO sysEsPreServiceCreateReqVO) {

        EsPreServiceDO sysEsPreService = BeanUtil.copyProperties(sysEsPreServiceCreateReqVO, EsPreServiceDO.class);
        sysEsPreServiceService.save(BeanUtil.copyProperties(sysEsPreServiceCreateReqVO, EsPreServiceDO.class));

        return ResResult.success(sysEsPreService.getUnid());
    }

    /**
     * 修改事项基本信息
     *
     * @param sysEsPreServiceUpdateReqVO 事项基本信息
     * @return ResResult
     */
    @ApiOperation("修改事项基本信息")
    @PutMapping("/update")
    public ResResult<Boolean> updateById(@Valid @RequestBody EsPreServiceUpdateReqVO sysEsPreServiceUpdateReqVO) {
        sysEsPreServiceService.updateById(BeanUtil.copyProperties(sysEsPreServiceUpdateReqVO, EsPreServiceDO.class));
        return ResResult.success(true);
    }

    /**
     * 通过id删除事项基本信息
     *
     * @param unid id
     * @return R
     */
    @ApiOperation("通过id删除事项基本信息")
    @DeleteMapping("/{unid}")
    public ResResult<Boolean> removeById(@PathVariable String unid) {
        return ResResult.success(sysEsPreServiceService.removeById(unid));
    }

}
