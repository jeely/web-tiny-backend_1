
package com.neo.tiny.search.es.domain.db;

import com.neo.tiny.data.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @Description: 事项基本信息
 * @Author: yqz
 * @CreateDate: 2022-10-20 22:57:57
 */
@Data
@TableName("sys_es_pre_service")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "事项基本信息")
public class EsPreServiceDO extends BaseDO {

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_UUID)
    @ApiModelProperty("主键")
    private String unid;

    /**
     * 所属目录清单UNID
     */
    @ApiModelProperty("所属目录清单UNID")
    private String catalogUnid;

    /**
     * 事项名称
     */
    @ApiModelProperty("事项名称")
    private String serviceName;

    /**
     * 事项关键词
     */
    @ApiModelProperty("事项关键词")
    private String keyWords;

    /**
     * 事项编码
     */
    @ApiModelProperty("事项编码")
    private String serviceCode;


}
