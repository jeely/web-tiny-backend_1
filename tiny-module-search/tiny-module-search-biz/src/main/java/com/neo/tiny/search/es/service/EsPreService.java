package com.neo.tiny.search.es.service;

import com.neo.tiny.search.es.domain.es.EsSearchPre;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.Collection;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/10/20 23:16
 */
public interface EsPreService {


    /**
     * 从数据库中导入所有数据到ES
     *
     * @return 导入数量
     */
    int importAllData();

    /**
     * 获取索引索引
     *
     * @return 索引列表
     * @throws IOException 异常
     */
    Collection<String> listAllIndex() throws IOException;

    /**
     * 根据索引号删除索引
     *
     * @param index 索引号 listAllIndex()
     * @return 删除结果
     */
    Boolean delIndex(String index) throws IOException;

    /**
     * 根据关键字搜索名称
     *
     * @param keyword  关键词
     * @param pageNum  页码
     * @param pageSize 页面大小
     * @return 搜索结果
     */
    Page<EsSearchPre> search(String keyword, Integer pageNum, Integer pageSize);

    /**
     * 按条件搜索，返回高亮标签
     *
     * @param keyword  关键词
     * @param pageNum  页码
     * @param pageSize 页面大小
     * @return 搜索结果
     */
    Page<EsSearchPre> highLightSearch(String keyword, Integer pageNum, Integer pageSize);
}
