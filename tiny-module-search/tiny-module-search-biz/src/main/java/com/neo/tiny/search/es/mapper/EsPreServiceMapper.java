package com.neo.tiny.search.es.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.search.es.domain.db.EsPreServiceDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: 事项基本信息
 * @Author: yqz
 * @CreateDate: 2022-10-20 22:57:57
 */
@Mapper
public interface EsPreServiceMapper extends BaseMapper<EsPreServiceDO> {

}
