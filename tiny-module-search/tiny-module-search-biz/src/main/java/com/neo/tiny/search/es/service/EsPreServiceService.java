package com.neo.tiny.search.es.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.search.es.domain.db.EsPreServiceDO;

/**
 *
 * @Description: 事项基本信息
 * @Author: yqz
 * @CreateDate: 2022-10-20 22:57:57
 */
public interface EsPreServiceService extends IService<EsPreServiceDO> {

}
