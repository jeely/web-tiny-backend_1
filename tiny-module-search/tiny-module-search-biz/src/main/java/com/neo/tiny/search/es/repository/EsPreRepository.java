package com.neo.tiny.search.es.repository;

import com.neo.tiny.search.es.domain.es.EsSearchPre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @Description: 搜索事项ES操作类
 * @Author: yqz
 * @CreateDate: 2022/10/20 23:04
 */
public interface EsPreRepository extends ElasticsearchRepository<EsSearchPre, Long> {

    /**
     * 搜索查询
     *
     * @param serviceName 商品关键字
     * @param keyWord     关键词
     * @param serviceCode 事项编码
     * @param page        分页信息
     * @return 分页数据
     */
    Page<EsSearchPre> findByServiceNameOrKeyWordsOrServiceCode(String serviceName, String keyWord, String serviceCode, Pageable page);

}
