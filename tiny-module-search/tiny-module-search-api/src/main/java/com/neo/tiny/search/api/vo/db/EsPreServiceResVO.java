package com.neo.tiny.search.api.vo.db;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 *
 * @Description: 事项基本信息响应类
 * @author yqz
 * @date 2022-10-20 22:57:57
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "事项基本信息")
public class EsPreServiceResVO extends EsPreServiceBaseVO implements Serializable {


    @ApiModelProperty("带高亮标签的结果字段")
    private String highLightServiceName;

    @ApiModelProperty("事项关键词")
    private String keyWords;

}
