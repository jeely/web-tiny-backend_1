package com.neo.tiny.search.api.vo.db;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 *
 * @Description: 事项基本信息更新请求类
 * @author yqzBpmFormUpdateReqVO.java
 * @date 2022-10-20 22:57:57
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "事项基本信息")
public class EsPreServiceUpdateReqVO extends EsPreServiceBaseVO implements Serializable {

    /**
     * 主键
     */
    @ApiModelProperty("主键")
    private String unid;

    /**
     * 所属目录清单UNID
     */
    @ApiModelProperty("所属目录清单UNID")
    private String catalogUnid;

    /**
     * 事项名称
     */
    @ApiModelProperty("事项名称")
    private String serviceName;

    /**
     * 事项编码
     */
    @ApiModelProperty("事项编码")
    private String serviceCode;


}
