package com.neo.tiny.search.api.vo.es;

import com.neo.tiny.common.common.EsPageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/10/26 00:21
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class HighLightSearchReqVO extends EsPageParam implements Serializable {

    @ApiModelProperty("关键词")
    private String keyword;
}

