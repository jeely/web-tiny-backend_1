package com.neo.tiny.middle.file.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yqz
 * @Description 文件上传接口返回参数
 * @CreateDate 2023/5/24 17:02
 */
@Data
public class FileUploadResVO implements Serializable {

    @ApiModelProperty(value = "业务主键")
    private Long bizId;

    @ApiModelProperty("文件名称")
    private String fileName;

    @ApiModelProperty("文件类型")
    private String fileType;

    @ApiModelProperty(value = "存储路径")
    private String uri;
}
