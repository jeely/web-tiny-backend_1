package com.neo.tiny.middle.file.vo;

import com.neo.tiny.admin.sysConfig.bean.StorageClientConfig;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 存储配置保存请求体
 * @Author: yqz
 * @CreateDate: 2022/11/17 20:57
 */
@Data
public class StorageReqVO implements Serializable {

    @ApiModelProperty("本地存储配置")
    private StorageClientConfig.Local local;

    @ApiModelProperty("minio配置")
    private StorageClientConfig.MinIO minIo;

    @ApiModelProperty("aliyun配置")
    private StorageClientConfig.AliyunOSS aliyun;

}
