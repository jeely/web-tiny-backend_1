package com.neo.tiny.middle.file.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.admin.vo.file.SysFilePageReqVO;
import com.neo.tiny.admin.vo.file.SysFileResVO;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/12/10 12:55
 */
public interface FileRecordApi {


    /**
     * 获得文件记录分页
     *
     * @param req 请求
     * @return 访问令牌分页
     */
    IPage<SysFileResVO> fileRecordPage(SysFilePageReqVO req);


    /**
     * 根据id删除文件记录
     *
     * @param id 文件id
     * @return 结果
     */
    Boolean removeById(Long id);
}
