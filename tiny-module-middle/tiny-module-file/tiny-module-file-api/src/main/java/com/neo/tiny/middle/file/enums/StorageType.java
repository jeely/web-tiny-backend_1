package com.neo.tiny.middle.file.enums;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/11/14 23:26
 */
public enum StorageType {
    /**
     * minio存储
     */
    MINIO("minio", "minio存储"),

    ALIYUN_OSS("aliyun_oss", "阿里云OSS"),

    /**
     * 本地存储
     */
    LOCAL("local", "本地存储");

    private final String storageType;
    private final String storageName;

    StorageType(String storageType, String storageName) {
        this.storageType = storageType;
        this.storageName = storageName;
    }

    public String getStorageName() {
        return storageName;
    }

    public String getStorageType() {
        return storageType;
    }
}
