package com.neo.tiny.middle.file.api;

import com.neo.tiny.middle.file.vo.FileUploadResVO;

import java.io.InputStream;

/**
 * @Description: 文件存储api，别的module可以依赖该module，实现类在biz
 * @Author: yqz
 * @CreateDate: 2022/12/10 12:56
 */
public interface StorageServiceApi {
    /**
     * 存储文件
     *
     * @param inputStream   文件输入流
     * @param contentLength 文件长度
     * @param contentType   文件类型
     * @param fileName      文件名
     * @return 文件存储路径
     */
    FileUploadResVO upload(InputStream inputStream, long contentLength,
                           String contentType, String fileName);
}
