package com.neo.tiny.middle.file.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.admin.vo.file.SysFilePageReqVO;
import com.neo.tiny.admin.vo.file.SysFileResVO;
import com.neo.tiny.middle.file.log.service.SysFileRecordService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @Description: 文件存储记录实现类
 * @Author: yqz
 * @CreateDate: 2022/12/10 12:57
 */
@Service
@AllArgsConstructor
public class FileRecordApiImpl implements FileRecordApi{

    private final SysFileRecordService fileRecordService;

    @Override
    public IPage<SysFileResVO> fileRecordPage(SysFilePageReqVO req) {
        return fileRecordService.getFileRecordPage(req);
    }

    @Override
    public Boolean removeById(Long id) {
        return fileRecordService.removeById(id);
    }
}
