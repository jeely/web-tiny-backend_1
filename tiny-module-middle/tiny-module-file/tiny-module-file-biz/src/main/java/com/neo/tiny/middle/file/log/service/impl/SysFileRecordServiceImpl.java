
package com.neo.tiny.middle.file.log.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.admin.vo.file.SysFilePageReqVO;
import com.neo.tiny.admin.vo.file.SysFileResVO;
import com.neo.tiny.middle.file.log.mapper.SysFileRecordMapper;
import com.neo.tiny.middle.file.log.entity.SysFileRecordDO;
import com.neo.tiny.middle.file.log.service.SysFileRecordService;
import com.neo.tiny.resolver.CommonPage;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @Description: 文件存储记录
 * @Author: yqz
 * @CreateDate: 2022-12-05 23:32:22
 */
@Service
@AllArgsConstructor
public class SysFileRecordServiceImpl extends ServiceImpl<SysFileRecordMapper, SysFileRecordDO> implements SysFileRecordService {

    private final SysFileRecordMapper recordMapper;

    @Override
    public IPage<SysFileResVO> getFileRecordPage(SysFilePageReqVO req) {
        IPage<SysFileRecordDO> page = recordMapper.selectPage(req);
        return CommonPage.restPage(page, SysFileResVO.class);
    }
}
