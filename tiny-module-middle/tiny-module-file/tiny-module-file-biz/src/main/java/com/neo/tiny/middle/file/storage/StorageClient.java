package com.neo.tiny.middle.file.storage;

import com.neo.tiny.admin.sysConfig.bean.StorageClientConfig;
import com.neo.tiny.middle.file.enums.StorageType;

import java.io.InputStream;

/**
 * @Description: 文件存储接口
 * @Author: yqz
 * @CreateDate: 2022/11/14 22:40
 */
public interface StorageClient {

    /**
     * 设置客户端配置
     *
     * @return 当前客户端配置
     */
    StorageClientConfig getClientConfig();

    /**
     * 存储一个文件对象
     *
     * @param inputStream   文件输入流
     * @param contentLength 文件长度
     * @param contentType   文件类型
     * @param keyName       文件名
     */
    void store(InputStream inputStream,
               long contentLength,
               String contentType, String keyName);


    /**
     * 生成文件路径
     *
     * @param keyName 文件名称
     * @return 文件路径
     */
    String generateUrl(String keyName);

    /**
     * 当前支持的实现类
     *
     * @param storageType 存储类型 {@link StorageType}
     * @return 存储枚举
     */
    Boolean support(String storageType);
}
