package com.neo.tiny.middle.file.api;

import com.neo.tiny.middle.file.service.StorageService;
import com.neo.tiny.middle.file.vo.FileUploadResVO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.InputStream;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/12/10 13:02
 */
@Service
@AllArgsConstructor
public class StorageServiceApiImpl implements StorageServiceApi {

    private final StorageService storageService;

    @Override
    public FileUploadResVO upload(InputStream inputStream, long contentLength, String contentType, String fileName) {
        return storageService.store(inputStream, contentLength, contentType, fileName);
    }
}
