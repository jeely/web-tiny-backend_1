package com.neo.tiny.middle.file.log.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.admin.vo.file.SysFilePageReqVO;
import com.neo.tiny.middle.file.log.entity.SysFileRecordDO;
import com.neo.tiny.query.BaseMapperX;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 文件存储记录
 * @Author: yqz
 * @CreateDate: 2022-12-05 23:32:22
 */
@Mapper
public interface SysFileRecordMapper extends BaseMapperX<SysFileRecordDO> {

    default IPage<SysFileRecordDO> selectPage(SysFilePageReqVO req) {
        return selectPage(req, new LambdaQueryWrapperBase<SysFileRecordDO>()
                .likeIfPresent(SysFileRecordDO::getOriginalFileName, req.getOriginalFileName())
                .orderByDesc(SysFileRecordDO::getCreateTime));
    }
}
