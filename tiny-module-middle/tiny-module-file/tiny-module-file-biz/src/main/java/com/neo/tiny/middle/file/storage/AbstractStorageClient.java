package com.neo.tiny.middle.file.storage;

import com.neo.tiny.admin.sysConfig.bean.StorageClientConfig;

/**
 * @Description: 存储客户端配置
 * @Author: yqz
 * @CreateDate: 2022/12/5 23:40
 */
public abstract class AbstractStorageClient implements StorageClient {

    private StorageClientConfig config;

    /**
     * 设置客户端配置
     *
     * @param clientConfig 客户端接口
     */
    public void setClientConfig(StorageClientConfig clientConfig) {
        this.config = clientConfig;
    }

    @Override
    public StorageClientConfig getClientConfig() {
        return this.config;
    }
}
