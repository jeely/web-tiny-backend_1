
package com.neo.tiny.middle.file.log.entity;

import com.neo.tiny.data.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 文件存储记录
 * @Author: yqz
 * @CreateDate: 2022-12-05 23:32:22
 */
@Data
@TableName(value = "sys_file_record", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "文件存储记录")
public class SysFileRecordDO extends BaseDO {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("id")
    private Long id;

    /**
     * 文件转义后名称
     */
    @ApiModelProperty("文件转义后名称")
    private String fileName;

    /**
     * 桶名称
     */
    @ApiModelProperty("桶名称")
    private String bucketName;

    /**
     * 文件原始名称
     */
    @ApiModelProperty("文件原始名称")
    private String originalFileName;

    /**
     * fileType
     */
    @ApiModelProperty("fileType")
    private String fileType;

    /**
     * 文件大小
     */
    @ApiModelProperty("文件大小")
    private Long fileSize;

    /**
     * 文件相对路径
     */
    @ApiModelProperty("文件相对路径")
    private String path;


}
