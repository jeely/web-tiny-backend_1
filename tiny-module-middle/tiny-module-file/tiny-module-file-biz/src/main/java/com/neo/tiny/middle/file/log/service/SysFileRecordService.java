
package com.neo.tiny.middle.file.log.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.admin.vo.file.SysFilePageReqVO;
import com.neo.tiny.admin.vo.file.SysFileResVO;
import com.neo.tiny.middle.file.log.entity.SysFileRecordDO;

/**
 *
 * @Description: 文件存储记录
 * @Author: yqz
 * @CreateDate: 2022-12-05 23:32:22
 */
public interface SysFileRecordService extends IService<SysFileRecordDO> {


    /**
     * 获得访问令牌分页
     *
     * @param req 请求
     * @return 访问令牌分页
     */
    IPage<SysFileResVO> getFileRecordPage(SysFilePageReqVO req);
}
