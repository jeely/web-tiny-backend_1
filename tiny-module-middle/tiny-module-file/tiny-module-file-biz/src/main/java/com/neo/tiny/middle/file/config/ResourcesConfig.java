package com.neo.tiny.middle.file.config;

import cn.hutool.core.bean.BeanUtil;
import com.neo.tiny.admin.sysConfig.bean.StorageClientConfig;
import com.neo.tiny.admin.sysConfig.interfaces.SysConfigService;
import com.neo.tiny.common.constant.FileConstants;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.constraints.NotNull;

/**
 * @Description: 本地存储资源映射
 * @Author: yqz
 * @CreateDate: 2022/12/5 22:48
 */
@Configuration
@AllArgsConstructor
public class ResourcesConfig implements WebMvcConfigurer {
    private final SysConfigService configService;

    @Override
    public void addResourceHandlers(@NotNull ResourceHandlerRegistry registry) {
        StorageClientConfig storageConfig = configService.loadStoreConfig(FileConstants.STORAGE_CONFIG);
        if (BeanUtil.isNotEmpty(storageConfig)) {
            StorageClientConfig.Local local = storageConfig.getLocal();
            if (BeanUtil.isNotEmpty(local)) {
                //addResourceHandler是指你想在url请求的路径
                registry.addResourceHandler(local.getAddress() + "**")
                        //addResourceLocations是图片存放的真实路径
                        .addResourceLocations("file:" + local.getStoragePath());
            }
        }
    }
}
