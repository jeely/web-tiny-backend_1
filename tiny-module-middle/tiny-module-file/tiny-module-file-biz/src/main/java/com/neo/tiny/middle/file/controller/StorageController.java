package com.neo.tiny.middle.file.controller;

import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.middle.file.config.ResourcesConfig;
import com.neo.tiny.middle.file.service.StorageService;
import com.neo.tiny.middle.file.vo.FileUploadResVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/11/14 23:58
 */
@Api(tags = "文件存储")
@RestController
@AllArgsConstructor
@RequestMapping("/storage")
public class StorageController {
    private final StorageService storageService;

    @ApiOperation("统一文件存储")
    @PostMapping("/upload")
    public ResResult<FileUploadResVO> getStorageService(MultipartFile file) throws IOException {

        return ResResult.success(storageService.store(file.getInputStream(),
                file.getSize(),
                file.getContentType(),
                file.getOriginalFilename()));
    }

    /**
     * TODO 目前获取文件路径是通过 {@link ResourcesConfig} 配置，
     * 不过如果需要的话，可以单独创建获取文件的接口，然后该接口便于控制权限
     */
}
