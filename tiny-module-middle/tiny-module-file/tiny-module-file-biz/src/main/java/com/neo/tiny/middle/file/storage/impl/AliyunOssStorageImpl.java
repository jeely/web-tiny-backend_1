package com.neo.tiny.middle.file.storage.impl;

import cn.hutool.core.util.StrUtil;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.CreateBucketRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.neo.tiny.admin.sysConfig.bean.StorageClientConfig;
import com.neo.tiny.middle.file.enums.StorageType;
import com.neo.tiny.middle.file.storage.AbstractStorageClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author yqz
 * @Description 阿里云对象存储
 * @CreateDate 2024/3/6 14:49
 */
@Slf4j
@Component
public class AliyunOssStorageImpl extends AbstractStorageClient {

    public OSS getOssClient() {

        StorageClientConfig config = getClientConfig();
        StorageClientConfig.AliyunOSS aliyunOSS = config.getAliyun();
        String endpoint = aliyunOSS.getEndpoint();
        String accessKey = aliyunOSS.getAccessKey();
        String secretKey = aliyunOSS.getSecretKey();
        return new OSSClientBuilder()
                .build(endpoint, accessKey, secretKey);
    }

    /**
     * 存储一个文件对象
     *
     * @param inputStream   文件输入流
     * @param contentLength 文件长度
     * @param contentType   文件类型
     * @param keyName       文件名
     */
    @Override
    public void store(InputStream inputStream, long contentLength, String contentType, String keyName) {
        OSS client = getOssClient();
        String bucketName = getClientConfig().getAliyun().getBucketName();
        if (StrUtil.isNotBlank(getClientConfig().getAliyun().getFilePre())) {
            keyName = getClientConfig().getAliyun().getFilePre() + StrUtil.C_SLASH + keyName;
        }
        try {
            // 判断bucket是否存在，不存在则创建bucket。
            if (!client.doesBucketExist(bucketName)) {
                log.info("aliyun_OSS bucketName 不存在:{}", bucketName);
                CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);
                client.createBucket(createBucketRequest);
            }
            PutObjectResult result = client.putObject(bucketName, keyName, inputStream);
            log.info("阿里云oss存储结果：{}", result.getETag());
        } catch (OSSException e) {
            log.error("阿里云存储文件失败", e);
        } finally {
            if (client != null) {
                client.shutdown();
            }
            try {
                inputStream.close();
            } catch (IOException e) {
                log.error("阿里云存储关闭文件流失败", e);
            }
        }
    }

    /**
     * 生成文件路径
     *
     * @param keyName 文件名称
     * @return 文件路径
     */
    @Override
    public String generateUrl(String keyName) {
        if (StrUtil.isNotBlank(getClientConfig().getAliyun().getFilePre())) {
            keyName = StrUtil.C_SLASH + getClientConfig().getAliyun().getFilePre() + StrUtil.C_SLASH + keyName;
        } else {
            keyName = StrUtil.C_SLASH + keyName;
        }
        return keyName;
    }

    /**
     * 当前支持的实现类
     *
     * @param storageType 存储类型 {@link StorageType}
     * @return 存储枚举
     */
    @Override
    public Boolean support(String storageType) {
        return StorageType.ALIYUN_OSS.getStorageType().equals(storageType);
    }
}
