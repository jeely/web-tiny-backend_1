# 文件服务

# 一、使用可切换的文件服务

​		暂时使用本地存储和minio作为对象存储，管理端提供可实时动态配置的功能，程序会判断当前所配置的可用的文件服务器，动态的将文件流存入当前文件服务器。

## 二、minio

使用docker 安装minio

```
docker run -p 9001:9001 -p 9090:9090 --name minio \
-d --restart=always \
-e "MINIO_ACCESS_KEY=admin" \
-e "MINIO_SECRET_KEY=admin123456" \
-v /mydata/docker/minio/data:/data \
-v /mydata/docker/minio/config:/root/.minio \
minio/minio server /data --console-address ':9090' --address ":9001" 
```

--address 为api地址

 --console-address后台管理地址

启动成功后的日志

```log
[root@localhost docker]# docker logs -f minio
WARNING: MINIO_ACCESS_KEY and MINIO_SECRET_KEY are deprecated.
         Please use MINIO_ROOT_USER and MINIO_ROOT_PASSWORD
API: http://172.17.0.5:9001  http://127.0.0.1:9001 

Console: http://172.17.0.5:9090 http://127.0.0.1:9090 

Documentation: https://docs.min.io

 You are running an older version of MinIO released 11 months ago 
 Update: 
Run `mc admin update` 
```

