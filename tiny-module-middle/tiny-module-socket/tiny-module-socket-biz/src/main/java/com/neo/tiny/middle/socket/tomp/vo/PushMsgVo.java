package com.neo.tiny.middle.socket.tomp.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/20 23:01
 */
@Data
public class PushMsgVo implements Serializable {

    private String topic;

    private Map<String,Object> msg;

}
