package com.neo.tiny.middle.socket.websocket.api;

import com.neo.tiny.business.iot.enums.RelaySwitchStatus;
import com.neo.tiny.middle.socket.api.SocketSessionClient;
import com.neo.tiny.middle.socket.dto.IClientInfo;
import com.neo.tiny.middle.socket.dto.IotWebsocketClientInfo;
import com.neo.tiny.middle.socket.enums.WebSocketType;
import com.neo.tiny.middle.socket.websocket.util.IotWebSocketMsgSender;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: socket会话查询
 * @Author: yqz
 * @CreateDate: 2023/1/15 21:25
 */
@Service
public class IotSocketSessionClientImpl implements SocketSessionClient<String> {

    @Override
    public List<IClientInfo> listAllSession() {
        Map<String, IotWebsocketClientInfo> client = IotWebSocketMsgSender.listAllWebSocketClient();
        return new ArrayList<>(client.values());
    }

    @Override
    public void sendMsg(String deviceId, String content) {
        IotWebSocketMsgSender.sendMsg(deviceId, content);

        if (content.contains(RelaySwitchStatus.ON.getAction())) {
            IotWebSocketMsgSender.changeIotClientStatus(deviceId, RelaySwitchStatus.ON.getAction());
        } else {
            IotWebSocketMsgSender.changeIotClientStatus(deviceId, RelaySwitchStatus.OFF.getAction());
        }
    }

    @Override
    public Boolean support(Integer socketType) {
        return WebSocketType.IOT.getType().equals(socketType);
    }

}
