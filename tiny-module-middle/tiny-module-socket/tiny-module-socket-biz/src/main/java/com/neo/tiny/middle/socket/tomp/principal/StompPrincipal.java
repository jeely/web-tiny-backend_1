package com.neo.tiny.middle.socket.tomp.principal;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.security.Principal;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2023/1/14 11:45
 */
@Data
@AllArgsConstructor
public class StompPrincipal  implements Principal {

    /**
     * 客户端主机名称，对应 HostName
     */
    private String name;

    /**
     * 客户端主机IP地址，对应 HostAddress
     */
    private String publicName;
}
