package com.neo.tiny.middle.socket.websocket.msg;

import com.neo.tiny.middle.socket.websocket.util.IotWebSocketMsgSender;
import com.neo.tiny.middle.socket.websocket.vo.SendMsgVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/28 12:50
 */
@Slf4j
@RestController
@RequestMapping("/web-socket")
public class WebSocketController {

    @PostMapping("/send")
    public void sendMsg(@RequestBody SendMsgVO vo) {
        IotWebSocketMsgSender.sendMsg(vo.getMsgId(), vo.getMsg());
    }
}
