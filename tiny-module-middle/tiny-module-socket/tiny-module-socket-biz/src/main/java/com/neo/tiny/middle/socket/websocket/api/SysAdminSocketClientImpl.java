package com.neo.tiny.middle.socket.websocket.api;

import com.neo.tiny.middle.socket.api.SocketSessionClient;
import com.neo.tiny.middle.socket.enums.WebSocketType;
import com.neo.tiny.middle.socket.dto.IClientInfo;
import com.neo.tiny.middle.socket.dto.SysWebSocketClientInfo;
import com.neo.tiny.middle.socket.websocket.util.SysAdminWebSocketSender;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2023/2/13 23:14
 */
@Service
public class SysAdminSocketClientImpl implements SocketSessionClient<Long> {

    @Override
    public List<IClientInfo> listAllSession() {
        Map<Long, SysWebSocketClientInfo> client = SysAdminWebSocketSender.listAllWebSocketClient();
        return new ArrayList<>(client.values());
    }

    @Override
    public void sendMsg(Long deviceId, String content) {
        SysAdminWebSocketSender.sendMsg(deviceId, content);
    }

    @Override
    public Boolean support(Integer socketType) {
        return WebSocketType.SYS_ADMIN.getType().equals(socketType);
    }
}
