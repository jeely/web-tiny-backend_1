package com.neo.tiny.middle.socket.tomp.msg;

import cn.hutool.json.JSONUtil;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.middle.socket.tomp.vo.PushMsgVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/20 13:31
 */
@Slf4j
@Controller
public class WsController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    /**
     * 1、通过@MessageMapping 来暴露节点路径，有点类似 @RequestMapping。注意这里虽然写的是 hello ，
     * 但是我们客户端调用的真正地址是 /app/hello。 因为我们在上面的 config 里配置了registry.setApplicationDestinationPrefixes("/app")。
     * 2、@SendTo这个注解会把返回值的内容发送给订阅了/topic/hello 的客户端，与之类似的还有一个@SendToUser 只不过他是发送给用户端一对一通信的。
     * 这两个注解一般是应答时响应的，如果服务端主动发送消息可以通过 simpMessagingTemplate类的convertAndSend方法。
     * 注意 simpMessagingTemplate.convertAndSendToUser(token, "/msg", msg) ，联系到我们上文配置的 registry.setUserDestinationPrefix("/user/"),
     * 这里客户端订阅的是/user/{token}/msg,千万不要搞错。
     *
     * @param requestMessage
     * @return
     */
    @MessageMapping("/hello")
    @SendTo("/topic/hello")
    public ResResult<String> hello(String requestMessage) {
        log.info("接收消息：" + requestMessage);
        return ResResult.success("服务端接收到你发的：" + requestMessage);
    }


    @MessageMapping("/heartbeat")
    @SendTo("/topic/heartbeat")
    public ResResult<String> heartBeat(String requestMessage) {
        log.info("接收心跳消息：" + requestMessage);
        return ResResult.success("服务端接收到你发的心跳消息：" + requestMessage);
    }

    @ResponseBody
    @PostMapping("/sendMsgByAll")
    public ResResult<String> sendMsgByAll(@RequestBody PushMsgVo vo) {
        simpMessagingTemplate.convertAndSend(vo.getTopic(), JSONUtil.toJsonStr(vo.getMsg()));
        return ResResult.success("success");
    }
}
