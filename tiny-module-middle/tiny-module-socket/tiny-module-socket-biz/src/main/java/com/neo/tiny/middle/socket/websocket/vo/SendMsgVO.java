package com.neo.tiny.middle.socket.websocket.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/28 12:53
 */
@Data
public class SendMsgVO implements Serializable {

    private String msgId;

    private String msg;
}
