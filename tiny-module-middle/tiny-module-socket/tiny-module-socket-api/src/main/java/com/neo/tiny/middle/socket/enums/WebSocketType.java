package com.neo.tiny.middle.socket.enums;

/**
 * @Description: websocket客户端类型
 * @Author: yqz
 * @CreateDate: 2023/2/13 22:57
 */
public enum WebSocketType {
    /**
     * Iot设备
     */
    IOT(0, "Iot设备"),
    /**
     * 系统后台用户
     */
    SYS_ADMIN(1, "系统后台用户");

    private final Integer type;

    private final String name;

    WebSocketType(Integer type, String name) {
        this.type = type;
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
