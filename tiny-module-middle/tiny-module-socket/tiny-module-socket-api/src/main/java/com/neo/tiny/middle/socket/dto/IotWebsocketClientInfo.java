package com.neo.tiny.middle.socket.dto;

import lombok.Data;

import javax.websocket.Session;

/**
 * @Description: IoT客户端信息
 * @Author: yqz
 * @CreateDate: 2023/1/15 20:32
 */
@Data
public class IotWebsocketClientInfo implements IClientInfo {

    /**
     * 客户端的唯一id
     */
    private String clientId;

    /**
     * 客户端ip
     */
    private String clientIp;

    /**
     * 设备mac地址
     */
    private String deviceMac;

    /**
     * 客户端主机名
     */
    private String clientHost;


    /**
     * 开关状态：on-开，off-关
     */
    private String switchStatus;

    /**
     * 本人的session信息，用于接受数据
     */
    private Session session;

    public Session getSession() {
        return session;
    }
}
