package com.neo.tiny.middle.socket.api;

import com.neo.tiny.middle.socket.dto.IClientInfo;

import java.util.List;

/**
 * @Description: socket客户端获取
 * @Author: yqz
 * @CreateDate: 2023/1/15 21:21
 */
public interface SocketSessionClient<E> {


    /**
     * 获取所有会话
     *
     * @return socket会话
     */
    List<IClientInfo> listAllSession();

    /**
     * 发送消息
     *
     * @param targetId 目标id
     * @param content  内容
     */
    void sendMsg(E targetId, String content);


    /**
     * 判断是否支持
     *
     * @param socketType 客户端类型
     * @return 是否支持
     */
    Boolean support(Integer socketType);
}
