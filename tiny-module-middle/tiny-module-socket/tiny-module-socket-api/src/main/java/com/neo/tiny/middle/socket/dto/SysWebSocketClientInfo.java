package com.neo.tiny.middle.socket.dto;

import lombok.Data;

import javax.websocket.Session;

/**
 * @Description: 系统Websocket信息
 * @Author: yqz
 * @CreateDate: 2023/2/13 22:48
 */
@Data
public class SysWebSocketClientInfo implements IClientInfo {

    /**
     * 连接用户id
     */
    private Long userId;

    /**
     * 连接着ip
     */
    private String clientIp;

    /**
     * 会话
     */
    private Session session;
}
