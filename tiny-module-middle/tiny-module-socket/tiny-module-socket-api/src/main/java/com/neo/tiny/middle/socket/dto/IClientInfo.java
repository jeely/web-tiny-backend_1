package com.neo.tiny.middle.socket.dto;

import javax.websocket.Session;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2023/2/13 23:00
 */
public interface IClientInfo {


    /**
     * 获取客户端ip
     *
     * @return 客户端ip
     */
    String getClientIp();

    /**
     * 获取会话
     *
     * @return 会话
     */
    Session getSession();
}
