package com.neo.tiny.bpm.flowable.core.behavior;

import com.neo.tiny.bpm.service.process.BpmTaskAssignRuleService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;
import org.flowable.bpmn.model.Activity;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.impl.bpmn.behavior.AbstractBpmnActivityBehavior;
import org.flowable.engine.impl.bpmn.behavior.ParallelMultiInstanceBehavior;
import org.flowable.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.flowable.engine.impl.bpmn.parser.factory.DefaultActivityBehaviorFactory;

/**
 * @Description: 自定义的 ActivityBehaviorFactory 实现类，目的如下：
 * 1. 自定义 {@link #createUserTaskActivityBehavior(UserTask)}：实现自定义的流程任务的 assignee 负责人的分配
 * @Author: yqz
 * @CreateDate: 2022/10/16 09:54
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmActivityBehaviorFactory extends DefaultActivityBehaviorFactory {

    @Setter
    private BpmTaskAssignRuleService bpmTaskRuleService;

    @Override
    public UserTaskActivityBehavior createUserTaskActivityBehavior(UserTask userTask) {
        BpmUserTaskActivityBehavior singleBehavior = new BpmUserTaskActivityBehavior(userTask);
        singleBehavior.setBpmTaskRuleService(bpmTaskRuleService);
        return singleBehavior;
    }

    @Override
    public ParallelMultiInstanceBehavior createParallelMultiInstanceBehavior(Activity activity,
                                                                             AbstractBpmnActivityBehavior innerActivityBehavior) {
        BpmParallelMultiInstanceBehavior multiInstanceBehavior = new BpmParallelMultiInstanceBehavior(activity, innerActivityBehavior);
        multiInstanceBehavior.setBpmTaskRuleService(bpmTaskRuleService);
        return multiInstanceBehavior;

    }

    // TODO @ke：SequentialMultiInstanceBehavior 这个抽空也可以看看

}
