
package com.neo.tiny.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author yqz
 * @Description: 流程实例
 * @date 2022-10-07 21:41:44
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@TableName(value = "sys_bpm_process_instance_ext", autoResultMap = true)
@ApiModel(description = "流程实例")
public class BpmProcessInstanceExtDO extends BaseDO {

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("编号")
    private Long id;

    /**
     * 发起流程的用户编号
     */
    @ApiModelProperty("发起流程的用户编号")
    private Long startUserId;

    /**
     * 流程实例的名字
     */
    @ApiModelProperty("流程实例的名字")
    private String name;

    /**
     * 流程实例的编号
     */
    @ApiModelProperty("流程实例的编号")
    private String processInstanceId;

    /**
     * 流程定义的编号
     */
    @ApiModelProperty("流程定义的编号")
    private String processDefinitionId;

    /**
     * 流程分类
     */
    @ApiModelProperty("流程分类")
    private String category;

    /**
     * 流程实例的状态
     */
    @ApiModelProperty("流程实例的状态")
    private Integer status;

    /**
     * 流程实例的结果
     */
    @ApiModelProperty("流程实例的结果")
    private Integer result;

    /**
     * 结束时间
     */
    @ApiModelProperty("结束时间")
    private LocalDateTime endTime;

    /**
     * 提交的表单值
     */
    @ApiModelProperty("表单值")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private Map<String, Object> formVariables;

}
