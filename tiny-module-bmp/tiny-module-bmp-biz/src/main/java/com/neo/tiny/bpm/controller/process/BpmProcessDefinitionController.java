package com.neo.tiny.bpm.controller.process;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.bpm.service.process.BpmProcessDefinitionService;
import com.neo.tiny.bpm.vo.process.BpmProcessDefinitionListReqVO;
import com.neo.tiny.bpm.vo.process.BpmProcessDefinitionPageItemRespVO;
import com.neo.tiny.bpm.vo.process.BpmProcessDefinitionPageReqVO;
import com.neo.tiny.bpm.vo.process.BpmProcessDefinitionRespVO;
import com.neo.tiny.common.common.ResResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: 管理后台 - 流程定义
 * @Author: yqz
 * @CreateDate: 2022/10/5 20:43
 */
@Api(tags = "管理后台 - 流程定义")
@RestController
@AllArgsConstructor
@RequestMapping("/bpm/process-definition")
public class BpmProcessDefinitionController {

    private final BpmProcessDefinitionService processDefinitionService;

    @GetMapping("/page")
    @ApiOperation(value = "获得流程定义分页")
    public ResResult<IPage<BpmProcessDefinitionPageItemRespVO>> getProcessDefinitionPage(
            BpmProcessDefinitionPageReqVO pageReqVO) {
        return ResResult.success(processDefinitionService.getProcessDefinitionPage(pageReqVO));
    }

    @GetMapping("/get-bpmn-xml")
    @ApiOperation(value = "获得流程定义的 BPMN XML")
    @ApiImplicitParam(name = "id", value = "编号", required = true,
            example = "1024", dataTypeClass = String.class)
    public ResResult<String> getProcessDefinitionBpmnXml(@RequestParam("id") String id) {
        return ResResult.success(processDefinitionService.getProcessDefinitionBpmnXml(id));
    }

    @GetMapping ("/list")
    @ApiOperation(value = "获得流程定义列表")
    public ResResult<List<BpmProcessDefinitionRespVO>> getProcessDefinitionList(
            BpmProcessDefinitionListReqVO listReqVO) {
        return ResResult.success(processDefinitionService.getProcessDefinitionList(listReqVO));
    }
}
