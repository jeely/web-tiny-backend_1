
package com.neo.tiny.bpm.service.process;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.bpm.entity.BpmFormDO;
import com.neo.tiny.bpm.vo.form.BpmFormCreateReqVO;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author yqz
 * @Description: 工作流的表单定义
 * @date 2022-09-15 23:29:46
 */
public interface BpmFormService extends IService<BpmFormDO> {

    /**
     * 创建表单
     *
     * @param vo
     * @return
     */
    Long createForm(BpmFormCreateReqVO vo);

    /**
     * 获得动态表单列表
     *
     * @param ids 编号
     * @return 动态表单列表
     */
    List<BpmFormDO> getFormList(Collection<Long> ids);


    /**
     * 获得动态表单 Map
     *
     * @param ids 编号
     * @return 动态表单 Map
     */
    default Map<Long, BpmFormDO> getFormMap(Collection<Long> ids) {
        if (CollUtil.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        List<BpmFormDO> formList = this.getFormList(ids);
        if (CollUtil.isEmpty(formList)) {
            return new HashMap<>();
        }
        return formList.stream().collect(Collectors.toMap(BpmFormDO::getId, Function.identity(), (key1, key2) -> key1));
    }

}
