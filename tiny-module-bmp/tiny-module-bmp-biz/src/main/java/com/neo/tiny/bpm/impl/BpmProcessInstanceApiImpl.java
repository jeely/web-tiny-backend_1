package com.neo.tiny.bpm.impl;

import com.neo.tiny.bpm.api.BpmProcessInstanceApi;
import com.neo.tiny.bpm.dto.task.BpmProcessInstanceCreateReqDTO;
import com.neo.tiny.bpm.service.task.BpmProcessInstanceExtService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @Description: Flowable 流程实例 Api 实现类
 * @Author: yqz
 * @CreateDate: 2022/10/19 23:16
 */
@Service
@AllArgsConstructor
public class BpmProcessInstanceApiImpl implements BpmProcessInstanceApi {

    private final BpmProcessInstanceExtService processInstanceService;

    @Override
    public String createProcessInstance(Long userId, BpmProcessInstanceCreateReqDTO reqDTO) {
        return processInstanceService.createProcessInstance(userId, reqDTO);
    }
}
