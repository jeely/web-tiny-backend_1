package com.neo.tiny.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.bpm.entity.BpmTaskAssignRuleDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: Bpm 任务规则表
 * @author yqz
 * @date 2022-10-04 16:02:43
 */
@Mapper
public interface BpmTaskAssignRuleMapper extends BaseMapper<BpmTaskAssignRuleDO> {

}
