
package com.neo.tiny.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.bpm.enums.task.BpmTaskAssignRuleTypeEnum;
import com.neo.tiny.bpm.enums.task.BpmTaskRuleScriptEnum;
import com.neo.tiny.data.BaseDO;
import com.neo.tiny.handler.JsonLongSetTypeHandler;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Set;

/**
 * @author yqz
 * @Description: Bpm 任务规则表
 * @date 2022-10-04 16:02:43
 */
@TableName(value = "sys_bpm_task_assign_rule", autoResultMap = true)
@Data
@Builder
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BpmTaskAssignRuleDO extends BaseDO {

    /**
     * {@link #processDefinitionId} 空串，用于标识属于流程模型，而不属于流程定义
     */
    public static final String PROCESS_DEFINITION_ID_NULL = "";

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("编号")
    private Long id;

    /**
     * 流程模型的编号
     */
    @ApiModelProperty("流程模型的编号")
    private String modelId;

    /**
     * 流程定义的编号
     */
    @ApiModelProperty("流程定义的编号")
    private String processDefinitionId;

    /**
     * 流程任务定义的 key
     */
    @ApiModelProperty("流程任务定义的 key")
    private String taskDefinitionKey;

    /**
     * 规则类型
     */
    @ApiModelProperty("规则类型")
    private Integer type;

    /**
     * 规则值数组，一般关联指定表的编号
     * 根据 type 不同，对应的值是不同的：
     * <p>
     * 1. {@link BpmTaskAssignRuleTypeEnum#ROLE} 时：角色编号
     * 2. {@link BpmTaskAssignRuleTypeEnum#DEPT_MEMBER} 时：部门编号
     * 3. {@link BpmTaskAssignRuleTypeEnum#DEPT_LEADER} 时：部门编号
     * 4. {@link BpmTaskAssignRuleTypeEnum#USER} 时：用户编号
     * 5. {@link BpmTaskAssignRuleTypeEnum#USER_GROUP} 时：用户组编号
     * 6. {@link BpmTaskAssignRuleTypeEnum#SCRIPT} 时：脚本编号，目前通过 {@link BpmTaskRuleScriptEnum#getId()} 标识
     */
    @TableField(typeHandler = JsonLongSetTypeHandler.class)
    private Set<Long> options;

}
