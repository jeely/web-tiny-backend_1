package com.neo.tiny.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.bpm.entity.BpmProcessDefinitionExtDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/10/5 13:48
 */
@Mapper
public interface BpmProcessDefinitionExtMapper extends BaseMapper<BpmProcessDefinitionExtDO> {
}
