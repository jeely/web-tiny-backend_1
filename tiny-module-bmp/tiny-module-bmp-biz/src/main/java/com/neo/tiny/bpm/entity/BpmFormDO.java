
package com.neo.tiny.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

/**
 * 工作流的表单定义
 *
 * @author yqz
 * @date 2022-09-15 00:08:25
 */
@Data
@Builder
@TableName(value = "sys_bpm_form", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "工作流的表单定义")
public class BpmFormDO extends BaseDO {

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("编号")
    private Long id;

    /**
     * 表单名
     */
    @ApiModelProperty("表单名")
    private String name;

    /**
     * 开启状态
     */
    @ApiModelProperty("开启状态")
    private Integer status;

    /**
     * 表单的配置
     */
    @ApiModelProperty("表单的配置")
    private String conf;

    /**
     * 表单项的数组
     * 目前直接将 https://github.com/JakHuang/form-generator 生成的 JSON 串，直接保存
     * 定义：https://github.com/JakHuang/form-generator/issues/46
     */
    @ApiModelProperty("表单项的数组")
    @TableField(value = "fields",typeHandler = JacksonTypeHandler.class)
    private List<String> fields;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


}
