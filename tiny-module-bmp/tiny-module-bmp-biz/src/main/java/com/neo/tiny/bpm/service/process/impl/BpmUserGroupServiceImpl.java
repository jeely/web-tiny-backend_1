
package com.neo.tiny.bpm.service.process.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.bpm.entity.BpmUserGroupDO;
import com.neo.tiny.bpm.mapper.BpmUserGroupMapper;
import com.neo.tiny.bpm.service.process.BpmUserGroupService;
import com.neo.tiny.common.constant.ErrorCodeConstants;
import com.neo.tiny.common.exception.WebApiException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author yqz
 * @Description: 流程用户分组
 * @date 2022-09-25 09:32:20
 */
@Service
@AllArgsConstructor
public class BpmUserGroupServiceImpl extends ServiceImpl<BpmUserGroupMapper, BpmUserGroupDO> implements BpmUserGroupService {


    private final BpmUserGroupMapper userGroupMapper;

    @Override
    public void validUserGroups(Set<Long> ids) {

        if (CollUtil.isEmpty(ids)) {
            return;
        }
        // 获取用户组信息
        List<BpmUserGroupDO> groupList = userGroupMapper.selectBatchIds(ids);

        Map<Long, BpmUserGroupDO> userGroupMap = groupList.stream()
                .collect(Collectors.toMap(BpmUserGroupDO::getId, Function.identity(), (key1, key2) -> key1));
        ids.forEach(groupId -> {
            BpmUserGroupDO bpmUserGroupDO = userGroupMap.get(groupId);
            if (Objects.isNull(bpmUserGroupDO)) {
                throw new WebApiException(ErrorCodeConstants.USER_GROUP_NOT_EXISTS);
            }
        });
    }

    @Override
    public List<BpmUserGroupDO> getUserGroupList(Collection<Long> ids) {
        if (CollUtil.isEmpty(ids)) {
            return Collections.emptyList();
        }
        return userGroupMapper.selectBatchIds(ids);
    }
}
