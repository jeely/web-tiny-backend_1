
package com.neo.tiny.bpm.service.process;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.bpm.entity.BpmUserGroupDO;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author yqz
 * @Description: 流程用户分组
 * @date 2022-09-25 09:32:20
 */
public interface BpmUserGroupService extends IService<BpmUserGroupDO> {

    /**
     * 校验用户组们是否有效。如下情况，视为无效：
     * 1. 用户组编号不存在
     * 2. 用户组被禁用
     *
     * @param ids 用户组编号数组
     */
    void validUserGroups(Set<Long> ids);

    /**
     * 获得用户组列表
     *
     * @param ids 编号
     * @return 用户组列表
     */
    List<BpmUserGroupDO> getUserGroupList(Collection<Long> ids);

}
