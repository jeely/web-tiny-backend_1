package com.neo.tiny.bpm.flowable.core.script.impl;

import com.neo.tiny.bpm.enums.task.BpmTaskRuleScriptEnum;
import com.neo.tiny.bpm.flowable.core.script.BpmTaskAssignScript;
import com.neo.tiny.bpm.service.task.BpmProcessInstanceExtService;
import com.neo.tiny.common.util.NumberUtils;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @Description: 分配给发起人审批的 Script 实现类
 * @Author: yqz
 * @CreateDate: 2022/10/16 12:24
 */
@Component
public class BpmTaskAssignStartUserScript implements BpmTaskAssignScript {

    /**
     * 解决循环依赖
     */
    @Resource
    @Lazy
    private BpmProcessInstanceExtService bpmProcessInstanceService;


    @Override
    public Set<Long> calculateTaskCandidateUsers(DelegateExecution execution) {
        ProcessInstance processInstance = bpmProcessInstanceService
                .getProcessInstance(execution.getProcessInstanceId());
        Long startUserId = NumberUtils.parseUpperCaseLong(processInstance.getStartUserId());
        return new HashSet<>(Collections.singletonList(startUserId));
    }

    @Override
    public BpmTaskRuleScriptEnum getEnum() {
        return BpmTaskRuleScriptEnum.START_USER;
    }
}
