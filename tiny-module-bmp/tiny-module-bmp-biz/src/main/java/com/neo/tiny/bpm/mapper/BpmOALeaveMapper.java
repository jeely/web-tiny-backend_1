package com.neo.tiny.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.bpm.entity.oa.BpmOALeaveDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 请假申请 Mapper
 * @Author: yqz
 * @CreateDate: 2022/10/19 23:07
 */
@Mapper
public interface BpmOALeaveMapper extends BaseMapper<BpmOALeaveDO> {
}
