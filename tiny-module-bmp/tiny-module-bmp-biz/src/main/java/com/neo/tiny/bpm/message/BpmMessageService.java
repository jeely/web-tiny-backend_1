package com.neo.tiny.bpm.message;

import com.neo.tiny.bpm.dto.message.BpmMessageSendWhenProcessInstanceApproveReqDTO;
import com.neo.tiny.bpm.dto.message.BpmMessageSendWhenProcessInstanceRejectReqDTO;
import com.neo.tiny.bpm.dto.message.BpmMessageSendWhenTaskCreatedReqDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

/**
 * @Description: BPM 消息 Service 接口 TODO  待实现
 * @Author: yqz
 * @CreateDate: 2022/10/15 20:02
 */
@Slf4j
@Service
public class BpmMessageService {

    /**
     * 发送流程实例被通过的消息
     *
     * @param reqDTO 发送信息
     */
    public void sendMessageWhenProcessInstanceApprove(@Valid BpmMessageSendWhenProcessInstanceApproveReqDTO reqDTO) {
        log.info("发送流程实例被通过的消息:{}", reqDTO);
    }

    /**
     * 发送流程实例被不通过的消息
     *
     * @param reqDTO 发送信息
     */
    public void sendMessageWhenProcessInstanceReject(@Valid BpmMessageSendWhenProcessInstanceRejectReqDTO reqDTO) {
        log.info("发送流程实例被不通过的消息:{}", reqDTO);
    }

    /**
     * 发送任务被分配的消息
     *
     * @param reqDTO 发送信息
     */
    public void sendMessageWhenTaskAssigned(@Valid BpmMessageSendWhenTaskCreatedReqDTO reqDTO) {
        log.info("发送任务被分配的消息:{}", reqDTO);
    }
}
