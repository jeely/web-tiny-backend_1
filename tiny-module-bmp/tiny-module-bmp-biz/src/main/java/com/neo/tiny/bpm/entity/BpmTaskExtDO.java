
package com.neo.tiny.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @Author: yqz
 * @Description: Bpm 流程任务的拓展表
 * 主要解决 Activity Task 和 HistoricTaskInstance 不支持拓展字段，所以新建拓展表
 * @CreateDate: 2022-10-12 21:37:47
 */
@Data
@TableName("sys_bpm_task_ext")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "Bpm 流程任务的拓展表")
public class BpmTaskExtDO extends BaseDO {

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("编号")
    private Long id;

    /**
     * 任务的审批人
     */
    @ApiModelProperty("任务的审批人")
    private Long assigneeUserId;

    /**
     * 任务的名字
     */
    @ApiModelProperty("任务的名字")
    private String name;

    /**
     * 流程任务key
     */
    @ApiModelProperty("流程任务key")
    private String taskDefKey;

    /**
     * 任务的编号
     */
    @ApiModelProperty("任务的编号")
    private String taskId;

    /**
     * 任务的结果
     */
    @ApiModelProperty("任务的结果")
    private Integer result;

    /**
     * 审批建议
     */
    @ApiModelProperty("审批建议")
    private String reason;

    /**
     * 任务的结束时间
     */
    @ApiModelProperty("任务的结束时间")
    private LocalDateTime endTime;

    /**
     * 流程实例的编号
     */
    @ApiModelProperty("流程实例的编号")
    private String processInstanceId;

    /**
     * 流程定义的编号
     */
    @ApiModelProperty("流程定义的编号")
    private String processDefinitionId;


}
