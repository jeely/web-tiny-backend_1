package com.neo.tiny.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.bpm.entity.BpmFormDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: 工作流的表单定义
 * @author yqz
 * @date 2022-09-15 23:29:46
 */
@Mapper
public interface BpmFormMapper extends BaseMapper<BpmFormDO> {

}
