package com.neo.tiny.bpm.flowable.core.script.impl;

import com.neo.tiny.bpm.enums.task.BpmTaskRuleScriptEnum;
import org.flowable.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @Description: 分配给发起人的一级 Leader 审批的 Script 实现类
 * @Author: yqz
 * @CreateDate: 2022/10/16 12:11
 */
@Component
public class BpmTaskAssignLeaderX1Script extends BpmTaskAssignLeaderAbstractScript {


    @Override
    public Set<Long> calculateTaskCandidateUsers(DelegateExecution execution) {
        return calculateTaskCandidateUsers(execution, 1);
    }

    @Override
    public BpmTaskRuleScriptEnum getEnum() {
        return BpmTaskRuleScriptEnum.LEADER_X1;
    }
}
