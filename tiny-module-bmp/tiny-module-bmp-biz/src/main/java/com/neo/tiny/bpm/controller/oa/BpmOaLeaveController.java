package com.neo.tiny.bpm.controller.oa;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.bpm.entity.oa.BpmOALeaveDO;
import com.neo.tiny.bpm.service.oa.BpmOALeaveService;
import com.neo.tiny.bpm.vo.oa.BpmOALeaveCreateReqVO;
import com.neo.tiny.bpm.vo.oa.BpmOALeavePageReqVO;
import com.neo.tiny.bpm.vo.oa.BpmOALeaveRespVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.common.util.CommonDoTransfer;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.secrity.util.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Description: OA 请假申请 Controller，用于演示自己存储数据，接入工作流的例子
 * @Author: yqz
 * @CreateDate: 2022/10/19 22:56
 */
@Api(tags = "管理后台 - OA 请假申请")
@RestController
@AllArgsConstructor
@RequestMapping("/bpm/oa/leave")
public class BpmOaLeaveController {

    private final BpmOALeaveService leaveService;

    @PostMapping("/create")
    @ApiOperation("创建请求申请")
    public ResResult<Long> createLeave(@Valid @RequestBody BpmOALeaveCreateReqVO createReqVO) {
        return ResResult.success(leaveService.createLeave(SecurityUtils.getUserId(), createReqVO));
    }

    @ApiOperation("获得请假申请分页")
    @GetMapping("/page")
    public ResResult<IPage<BpmOALeaveRespVO>> getLeavePage(@Valid BpmOALeavePageReqVO pageVO) {

        IPage<BpmOALeaveDO> pageResult = leaveService.getLeavePage(SecurityUtils.getUserId(), pageVO);

        return ResResult.success(CommonPage.restPage(pageResult, BpmOALeaveRespVO.class));
    }

    @GetMapping("/get")
    @ApiOperation("获得请假申请")
    @ApiImplicitParam(name = "id", value = "编号", required = true, example = "1024", dataTypeClass = Long.class)
    public ResResult<BpmOALeaveRespVO> getLeave(@RequestParam("id") Long id) {
        BpmOALeaveDO leave = leaveService.getLeave(id);
        return ResResult.success(CommonDoTransfer.transfer(leave, BpmOALeaveRespVO.class));
    }
}
