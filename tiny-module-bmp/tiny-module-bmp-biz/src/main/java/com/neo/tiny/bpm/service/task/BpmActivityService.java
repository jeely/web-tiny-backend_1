package com.neo.tiny.bpm.service.task;

import com.neo.tiny.bpm.vo.activity.BpmActivityRespVO;

import java.util.List;

/**
 * @Description: BPM 活动实例 Service 接口
 * @Author: yqz
 * @CreateDate: 2022/10/14 22:46
 */
public interface BpmActivityService {


    /**
     * 获得指定流程实例的活动实例列表
     *
     * @param processInstanceId 流程实例的编号
     * @return 活动实例列表
     */
    List<BpmActivityRespVO> getActivityListByProcessInstanceId(String processInstanceId);


}
