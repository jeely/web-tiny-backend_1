package com.neo.tiny.bpm.service.oa;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.bpm.entity.oa.BpmOALeaveDO;
import com.neo.tiny.bpm.vo.oa.BpmOALeaveCreateReqVO;
import com.neo.tiny.bpm.vo.oa.BpmOALeavePageReqVO;

import javax.validation.Valid;

/**
 * @Description: 请假申请 Service 接口
 * @Author: yqz
 * @CreateDate: 2022/10/19 22:56
 */
public interface BpmOALeaveService extends IService<BpmOALeaveDO> {

    /**
     * 创建请假申请
     *
     * @param userId      用户编号
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createLeave(Long userId, @Valid BpmOALeaveCreateReqVO createReqVO);


    /**
     * 获得请假申请分页
     *
     * @param userId    用户编号
     * @param pageReqVO 分页查询
     * @return 请假申请分页
     */
    IPage<BpmOALeaveDO> getLeavePage(Long userId, BpmOALeavePageReqVO pageReqVO);

    /**
     * 获得请假申请
     *
     * @param id 编号
     * @return 请假申请
     */
    BpmOALeaveDO getLeave(Long id);


    /**
     * 更新请假申请的状态
     *
     * @param id 编号
     * @param result 结果
     */
    void updateLeaveResult(Long id, Integer result);
}
