package com.neo.tiny.bpm.config;

import com.neo.tiny.bpm.flowable.event.publisher.BpmProcessInstanceResultEventPublisher;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/10/9 00:06
 */
@Configuration
public class EventPublisherConfig {

    @Bean
    public BpmProcessInstanceResultEventPublisher processInstanceResultEventPublisher(ApplicationEventPublisher publisher) {
        return new BpmProcessInstanceResultEventPublisher(publisher);
    }
}
