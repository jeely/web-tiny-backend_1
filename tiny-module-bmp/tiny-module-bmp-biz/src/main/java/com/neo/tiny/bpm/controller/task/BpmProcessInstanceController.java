package com.neo.tiny.bpm.controller.task;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.bpm.service.task.BpmProcessInstanceExtService;
import com.neo.tiny.bpm.vo.instance.BpmProcessInstanceCreateReqVO;
import com.neo.tiny.bpm.vo.instance.BpmProcessInstancePageItemRespVO;
import com.neo.tiny.bpm.vo.instance.BpmProcessInstancePageReqVO;
import com.neo.tiny.bpm.vo.instance.BpmProcessInstanceRespVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.secrity.util.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Description: 流程实例，通过流程定义创建的一次“申请”
 * @Author: yqz
 * @CreateDate: 2022/10/5 23:11
 */
@Api(tags = "管理后台 - 流程实例")
@RestController
@AllArgsConstructor
@RequestMapping("/bpm/process-instance")
public class BpmProcessInstanceController {

    private final BpmProcessInstanceExtService instanceExtService;

    @GetMapping("/page")
    @ApiOperation(value = "获得我的实例分页列表", notes = "在【我的流程】菜单中，进行调用")
    public ResResult<Page<BpmProcessInstancePageItemRespVO>> page(@Valid BpmProcessInstancePageReqVO pageReqVO) {
        Long userId = SecurityUtils.getUserId();
        return ResResult.success(instanceExtService.getMyProcessInstancePage(userId, pageReqVO));
    }

    @PostMapping("/create")
    @ApiOperation("新建流程实例")
    public ResResult<String> createProcessInstance(@Valid @RequestBody BpmProcessInstanceCreateReqVO createReqVO) {
        return ResResult.success(instanceExtService.createProcessInstance(SecurityUtils.getUserId(), createReqVO));
    }

    @GetMapping("/get")
    @ApiOperation(value = "获得指定流程实例", notes = "在【流程详细】界面中，进行调用")
    @ApiImplicitParam(name = "id", value = "流程实例的编号", required = true, dataTypeClass = String.class)
    public ResResult<BpmProcessInstanceRespVO> getProcessInstance(@RequestParam("id") String id) {
        return ResResult.success(instanceExtService.getProcessInstanceVO(id));
    }

}
