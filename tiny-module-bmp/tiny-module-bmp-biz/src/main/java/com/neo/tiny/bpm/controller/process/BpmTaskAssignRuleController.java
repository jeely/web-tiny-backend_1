package com.neo.tiny.bpm.controller.process;

import com.neo.tiny.bpm.service.process.BpmTaskAssignRuleService;
import com.neo.tiny.bpm.vo.rule.BpmTaskAssignRuleCreateReqVO;
import com.neo.tiny.bpm.vo.rule.BpmTaskAssignRuleRespVO;
import com.neo.tiny.bpm.vo.rule.BpmTaskAssignRuleUpdateReqVO;
import com.neo.tiny.common.common.ResResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author yqz
 * @Description: Bpm 任务规则表
 * @date 2022-10-04 16:02:43
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bpm/task-assign-rule")
@Api(tags = "Bpm 任务规则表管理")
public class BpmTaskAssignRuleController {

    private final BpmTaskAssignRuleService taskAssignRuleService;


    @GetMapping("/list")
    @ApiOperation(value = "获得任务分配规则列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "modelId", value = "模型编号", example = "1024", dataTypeClass = String.class),
            @ApiImplicitParam(name = "processDefinitionId", value = "流程定义的编号", example = "2048", dataTypeClass = String.class)
    })
    public ResResult<List<BpmTaskAssignRuleRespVO>> getTaskAssignRuleList(
            @RequestParam(value = "modelId", required = false) String modelId,
            @RequestParam(value = "processDefinitionId", required = false) String processDefinitionId) {
        return ResResult.success(taskAssignRuleService.getTaskAssignRuleList(modelId, processDefinitionId));
    }

    @PostMapping("/create")
    @ApiOperation(value = "创建任务分配规则")
    public ResResult<Long> createTaskAssignRule(@Valid @RequestBody BpmTaskAssignRuleCreateReqVO reqVO) {
        return ResResult.success(taskAssignRuleService.createTaskAssignRule(reqVO));
    }
    @PutMapping("/update")
    @ApiOperation(value = "更新任务分配规则")
    public ResResult<Boolean> updateTaskAssignRule(@Valid @RequestBody BpmTaskAssignRuleUpdateReqVO reqVO) {
        taskAssignRuleService.updateTaskAssignRule(reqVO);
        return ResResult.success(true);
    }
}
