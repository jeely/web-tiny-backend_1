package com.neo.tiny.bpm.flowable.event.bean;

import org.springframework.context.ApplicationEvent;

import javax.validation.constraints.NotNull;

/**
 * @Description: 流程实例的结果发生变化的 Event
 * 定位：由于额外增加了 {@link com.neo.tiny.bpm.entity.BpmProcessInstanceExtDO#getResult()} 结果，所以增加该事件
 * @Author: yqz
 * @CreateDate: 2022/10/9 00:03
 */

public class BpmProcessInstanceResultEvent extends ApplicationEvent {

    /**
     * 流程实例的编号
     */
    @NotNull(message = "流程实例的编号不能为空")
    private String id;
    /**
     * 流程实例的 key
     */
    @NotNull(message = "流程实例的 key 不能为空")
    private String processDefinitionKey;
    /**
     * 流程实例的结果
     */
    @NotNull(message = "流程实例的结果不能为空")
    private Integer result;
    /**
     * 流程实例对应的业务标识
     * 例如说，请假
     */
    private String businessKey;

    public BpmProcessInstanceResultEvent(Object source) {
        super(source);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }
}
