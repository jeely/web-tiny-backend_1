package com.neo.tiny.bpm.controller.process;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.bpm.entity.BpmUserGroupDO;
import com.neo.tiny.bpm.service.process.BpmUserGroupService;
import com.neo.tiny.bpm.vo.group.BpmUserGroupCreateReqVO;
import com.neo.tiny.bpm.vo.group.BpmUserGroupPageReqVO;
import com.neo.tiny.bpm.vo.group.BpmUserGroupResVO;
import com.neo.tiny.bpm.vo.group.BpmUserGroupUpdateReqVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.common.util.CommonDoTransfer;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.util.MyBatisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author yqz
 * @Description: 流程用户分组
 * @date 2022-09-25 10:48:23
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bpm/bpm-user-group")
@Api(tags = "流程用户分组管理")
public class BpmUserGroupController {

    private final BpmUserGroupService bpmUserGroupService;

    /**
     * 分页查询
     *
     * @param vo 流程用户分组
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<IPage<BpmUserGroupResVO>> getBpmUserGroupPage(@Valid BpmUserGroupPageReqVO vo) {

        LambdaQueryWrapperBase<BpmUserGroupDO> wrapper = new LambdaQueryWrapperBase<>();
        wrapper.likeIfPresent(BpmUserGroupDO::getGroupName, vo.getGroupName())
                .eqIfPresent(BpmUserGroupDO::getStatus, vo.getStatus())
                .betweenIfPresent(BpmUserGroupDO::getCreateTime, vo.getBeginCreateTime(), vo.getEndCreateTime())
                .orderByDesc(BpmUserGroupDO::getCreateTime);


        return ResResult.success(CommonPage.restPage(bpmUserGroupService
                .page(MyBatisUtils.buildPage(vo), wrapper), BpmUserGroupResVO.class));
    }


    /**
     * 通过id查询流程用户分组
     *
     * @param id id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{id}")
    public ResResult<BpmUserGroupResVO> getById(@PathVariable("id") Long id) {
        return ResResult.success(BeanUtil.copyProperties(bpmUserGroupService.getById(id), BpmUserGroupResVO.class));
    }

    /**
     * 新增流程用户分组
     *
     * @param bpmUserGroupCreateReqVO 流程用户分组
     * @return ResResult
     */
    @ApiOperation("新增流程用户分组")
    @PostMapping("/create")
    public ResResult<Long> save(@Valid @RequestBody BpmUserGroupCreateReqVO bpmUserGroupCreateReqVO) {

        BpmUserGroupDO bpmUserGroup = BeanUtil.copyProperties(bpmUserGroupCreateReqVO, BpmUserGroupDO.class);
        bpmUserGroupService.save(BeanUtil.copyProperties(bpmUserGroupCreateReqVO, BpmUserGroupDO.class));

        return ResResult.success(bpmUserGroup.getId());
    }

    /**
     * 修改流程用户分组
     *
     * @param bpmUserGroupUpdateReqVO 流程用户分组
     * @return ResResult
     */
    @ApiOperation("修改流程用户分组")
    @PutMapping("/update")
    public ResResult<Boolean> updateById(@Valid @RequestBody BpmUserGroupUpdateReqVO bpmUserGroupUpdateReqVO) {
        bpmUserGroupService.updateById(BeanUtil.copyProperties(bpmUserGroupUpdateReqVO, BpmUserGroupDO.class));
        return ResResult.success(true);
    }

    /**
     * 通过id删除流程用户分组
     *
     * @param id id
     * @return R
     */
    @ApiOperation("通过id删除流程用户分组")
    @DeleteMapping("/{id}")
    public ResResult<Boolean> removeById(@PathVariable Long id) {
        return ResResult.success(bpmUserGroupService.removeById(id));
    }


    @ApiOperation("用户分组列表")
    @GetMapping("/listUserGroups")
    public ResResult<List<BpmUserGroupResVO>> listUserGroups() {
        List<BpmUserGroupDO> groups = bpmUserGroupService.list();

        return ResResult.success(CommonDoTransfer.transfer(groups, BpmUserGroupResVO.class));
    }

}
