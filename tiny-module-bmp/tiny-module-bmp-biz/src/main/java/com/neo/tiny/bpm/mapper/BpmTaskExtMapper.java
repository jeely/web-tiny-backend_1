package com.neo.tiny.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.bpm.entity.BpmTaskExtDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: Bpm 流程任务的拓展表
 * @Author: yqz
 * @CreateDate: 2022-10-12 21:37:47
 */
@Mapper
public interface BpmTaskExtMapper extends BaseMapper<BpmTaskExtDO> {

}
