package com.neo.tiny.bpm.service.process;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.bpm.vo.model.*;
import com.neo.tiny.common.common.ResResult;
import org.flowable.bpmn.model.BpmnModel;

import javax.validation.Valid;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/9/25 23:45
 */
public interface BpmModelService {

    /**
     * 分页查询流程模型
     *
     * @param vo
     * @return
     */
    ResResult<IPage<BpmModelPageItemRespVO>> modelPage(BpmModelPageReqVO vo);

    /**
     * 创建流程模型
     *
     * @param modelVO 创建信息
     * @param bpmnXml BPMN XML
     * @return 创建的流程模型的编号
     */
    String createModel(@Valid BpmModelCreateReqVO modelVO, String bpmnXml);

    /**
     * 修改流程模型
     *
     * @param updateReqVO 更新信息
     */
    void updateModel(@Valid BpmModelUpdateReqVO updateReqVO);


    /**
     * 获得流程模块
     *
     * @param modelId 编号
     * @return 流程模型
     */
    BpmModelRespVO getModel(String modelId);


    /**
     * 将流程模型，部署成一个流程定义
     *
     * @param modelId 编号
     */
    void deployModel(String modelId);

    /**
     * 修改模型的状态，实际更新的部署的流程定义的状态
     *
     * @param id    编号
     * @param state 状态
     */
    void updateModelState(String id, Integer state);


    /**
     * 删除模型
     *
     * @param id 编号
     */
    void deleteModel(String id);

    /**
     * 获得流程模型编号对应的 BPMN Model
     *
     * @param id 流程模型编号
     * @return BPMN Model
     */
    BpmnModel getBpmnByModelId(String id);
}
