package com.neo.tiny.bpm.service.task.impl;

import com.neo.tiny.bpm.service.task.BpmActivityService;
import com.neo.tiny.bpm.vo.activity.BpmActivityRespVO;
import lombok.AllArgsConstructor;
import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricActivityInstance;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Description: BPM 活动实例 Service 实现类
 * @Author: yqz
 * @CreateDate: 2022/10/14 22:47
 */
@Service
@AllArgsConstructor
public class BpmActivityServiceImpl implements BpmActivityService {


    private final HistoryService historyService;

    @Override
    public List<BpmActivityRespVO> getActivityListByProcessInstanceId(String processInstanceId) {

        List<HistoricActivityInstance> activityList = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(processInstanceId).list();

        if (Objects.isNull(activityList)) {
            return null;
        }

        ArrayList<BpmActivityRespVO> list = new ArrayList<>(activityList.size());
        for (HistoricActivityInstance task : activityList) {
            BpmActivityRespVO vo = new BpmActivityRespVO();
            vo.setKey(task.getActivityId());
            vo.setType(task.getActivityType());
            vo.setStartTime(task.getStartTime());
            vo.setEndTime(task.getEndTime());
            vo.setTaskId(task.getTaskId());
            list.add(vo);
        }
        return list;
    }
}
