package com.neo.tiny.bpm.controller.task;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.bpm.service.task.BpmTaskService;
import com.neo.tiny.bpm.vo.task.*;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.secrity.util.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description: 管理后台 - 流程任务实例
 * @Author: yqz
 * @CreateDate: 2022/10/9 23:53
 */
@Api(tags = "管理后台 - 流程任务实例")
@RestController
@AllArgsConstructor
@RequestMapping("/bpm/task")
public class BpmTaskController {

    private final BpmTaskService taskService;


    @GetMapping("/list-by-process-instance-id")
    @ApiOperation(value = "获得指定流程实例的任务列表", notes = "包括完成的、未完成的")
    @ApiImplicitParam(name = "processInstanceId", value = "流程实例的编号", required = true, dataTypeClass = String.class)
    public ResResult<List<BpmTaskRespVO>> getTaskListByProcessInstanceId(
            @RequestParam("processInstanceId") String processInstanceId) {
        return ResResult.success(taskService.getTaskListByProcessInstanceId(processInstanceId));
    }

    @ApiOperation("通过任务")
    @PutMapping("/approve")
    public ResResult<Boolean> approveTask(@Valid @RequestBody BpmTaskApproveReqVO reqVO) {
        Long userId = SecurityUtils.getUserId();
        taskService.approveTask(userId, reqVO);
        return ResResult.success(true);
    }
    @PutMapping("/reject")
    @ApiOperation("不通过任务")
    public ResResult<Boolean> rejectTask(@Valid @RequestBody BpmTaskRejectReqVO reqVO) {
        Long userId = SecurityUtils.getUserId();
        taskService.rejectTask(userId, reqVO);
        return ResResult.success(true);
    }
    @PutMapping("/update-assignee")
    @ApiOperation(value = "更新任务的负责人", notes = "用于【流程详情】的【转派】按钮")
    public ResResult<Boolean> updateTaskAssignee(@Valid @RequestBody BpmTaskUpdateAssigneeReqVO reqVO) {
        taskService.updateTaskAssignee(SecurityUtils.getUserId(), reqVO);
        return ResResult.success(true);
    }

    @GetMapping("todo-page")
    @ApiOperation("获取 Todo 待办任务分页")
    public ResResult<IPage<BpmTaskTodoPageItemRespVO>> getTodoTaskPage(@Valid BpmTaskTodoPageReqVO pageVO) {

        Long userId = SecurityUtils.getUserId();

        return ResResult.success(taskService.getTodoTaskPage(userId, pageVO));
    }

    @GetMapping("done-page")
    @ApiOperation("获取 Done 已办任务分页")
    public ResResult<IPage<BpmTaskDonePageItemRespVO>> getDoneTaskPage(@Valid BpmTaskDonePageReqVO pageVO) {
        Long userId = SecurityUtils.getUserId();
        return ResResult.success(taskService.getDoneTaskPage(userId, pageVO));
    }

}
