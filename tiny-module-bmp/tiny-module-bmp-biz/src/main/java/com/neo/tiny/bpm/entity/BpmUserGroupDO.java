
package com.neo.tiny.bpm.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.neo.tiny.data.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.handler.JsonLongSetTypeHandler;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Set;

/**
 * @author yqz
 * @Description: 流程用户分组
 * @date 2022-09-25 10:08:47
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "sys_bpm_user_group", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "流程用户分组")
public class BpmUserGroupDO extends BaseDO {

    /**
     * 自增主键
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("自增主键")
    private Long id;

    /**
     * 分组名称
     */
    @ApiModelProperty("分组名称")
    private String groupName;

    /**
     * 分组注释
     */
    @ApiModelProperty("分组注释")
    private String description;

    /**
     * 用户的id集合-list
     */
    @ApiModelProperty("用户的id集合-list")
    @TableField(value = "user_ids", typeHandler = JsonLongSetTypeHandler.class)
    private Set<Long> userIds;

    /**
     * 开启状态：0-开启，1-关闭
     */
    @ApiModelProperty("开启状态：0-开启，1-关闭")
    private Integer status;


}
