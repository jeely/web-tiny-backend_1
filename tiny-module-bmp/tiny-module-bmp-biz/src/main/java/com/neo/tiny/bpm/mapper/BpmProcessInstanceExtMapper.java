package com.neo.tiny.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.bpm.entity.BpmProcessInstanceExtDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: 流程实例
 * @author yqz
 * @date 2022-10-07 21:41:44
 */
@Mapper
public interface BpmProcessInstanceExtMapper extends BaseMapper<BpmProcessInstanceExtDO> {

}
