package com.neo.tiny.bpm.flowable.listener;

import cn.hutool.core.util.StrUtil;
import com.neo.tiny.bpm.flowable.event.bean.BpmProcessInstanceResultEvent;
import org.springframework.context.ApplicationListener;

/**
 * @Description: {@link BpmProcessInstanceResultEvent} 的监听器
 * @Author: yqz
 * @CreateDate: 2022/10/20 00:23
 */
public abstract class BpmProcessInstanceResultEventListener
        implements ApplicationListener<BpmProcessInstanceResultEvent> {


    @Override
    public void onApplicationEvent(BpmProcessInstanceResultEvent event) {
        // 判断 流程定义key是否与预设一致，不一致的话就return
        if (!StrUtil.equals(event.getProcessDefinitionKey(), getProcessDefinitionKey())) {
            return;
        }
        onEvent(event);
    }


    /**
     * @return 返回监听的流程定义 Key
     */
    protected abstract String getProcessDefinitionKey();

    /**
     * 处理事件
     *
     * @param event 事件
     */
    protected abstract void onEvent(BpmProcessInstanceResultEvent event);
}
