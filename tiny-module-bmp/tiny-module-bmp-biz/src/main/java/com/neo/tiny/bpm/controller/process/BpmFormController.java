package com.neo.tiny.bpm.controller.process;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.bpm.entity.BpmFormDO;
import com.neo.tiny.bpm.service.process.BpmFormService;
import com.neo.tiny.bpm.vo.form.BpmFormCreateReqVO;
import com.neo.tiny.bpm.vo.form.BpmFormPageReqVO;
import com.neo.tiny.bpm.vo.form.BpmFormResVO;
import com.neo.tiny.bpm.vo.form.BpmFormSimpleRespVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.common.util.CommonDoTransfer;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.util.MyBatisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author yqz
 * @Description: 工作流的表单定义
 * @date 2022-09-15 23:38:09
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bpm/bpm-form")
@Api(tags = "工作流的表单定义管理")
public class BpmFormController {

    private final BpmFormService bpmFormService;

    /**
     * 分页查询
     *
     * @param bpmFormPageReqVO 工作流的表单定义
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<Page<BpmFormResVO>> getBpmFormPage(@Valid BpmFormPageReqVO bpmFormPageReqVO) {

        LambdaQueryWrapperBase<BpmFormDO> wrapper = new LambdaQueryWrapperBase<>();
        wrapper.likeIfPresent(BpmFormDO::getName, bpmFormPageReqVO.getName());

        return ResResult.success(CommonPage.restPage(bpmFormService
                .page(MyBatisUtils.buildPage(bpmFormPageReqVO), wrapper), BpmFormResVO.class));
    }


    /**
     * 通过id查询工作流的表单定义
     *
     * @param id id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{id}")
    public ResResult<BpmFormResVO> getById(@PathVariable("id") Long id) {
        return ResResult.success(BeanUtil.copyProperties(bpmFormService.getById(id), BpmFormResVO.class));
    }

    /**
     * 新增工作流的表单定义
     *
     * @param vo 工作流的表单定义
     * @return ResResult
     */
    @ApiOperation("新增工作流的表单定义")
    @PostMapping("/create")
    public ResResult<Long> save(@RequestBody BpmFormCreateReqVO vo) {

        return ResResult.success(bpmFormService.createForm(vo));
    }

    /**
     * 修改工作流的表单定义
     *
     * @param bpmForm 工作流的表单定义
     * @return ResResult
     */
    @ApiOperation("修改工作流的表单定义")
    @PutMapping
    public ResResult<?> updateById(@RequestBody BpmFormDO bpmForm) {
        return ResResult.success(bpmFormService.updateById(bpmForm));
    }

    /**
     * 通过id删除工作流的表单定义
     *
     * @param id id
     * @return R
     */
    @ApiOperation("通过id删除工作流的表单定义")
    @DeleteMapping("/{id}")
    public ResResult<?> removeById(@PathVariable Long id) {
        return ResResult.success(bpmFormService.removeById(id));
    }


    @ApiOperation(value = "获得动态表单的精简列表", notes = "用于表单下拉框")
    @GetMapping("/list-form")
    public ResResult<List<BpmFormSimpleRespVO>> getSimpleForms() {
        List<BpmFormDO> list = bpmFormService.list();
        return ResResult.success(CommonDoTransfer.transfer(list, BpmFormSimpleRespVO.class));
    }
}
