package com.neo.tiny.bpm.flowable.event.publisher;

import com.neo.tiny.bpm.flowable.event.bean.BpmProcessInstanceResultEvent;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;

import javax.validation.Valid;

/**
 * @Description: {@link BpmProcessInstanceResultEvent} 的生产者
 * @Author: yqz
 * @CreateDate: 2022/10/9 00:05
 */
@AllArgsConstructor
public class BpmProcessInstanceResultEventPublisher {
    private final ApplicationEventPublisher publisher;

    public void sendProcessInstanceResultEvent(@Valid BpmProcessInstanceResultEvent event) {
        publisher.publishEvent(event);
    }
}
