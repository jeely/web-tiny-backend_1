package com.neo.tiny.bpm.flowable.listener;

import com.neo.tiny.bpm.flowable.event.bean.BpmProcessInstanceResultEvent;
import com.neo.tiny.bpm.service.oa.BpmOALeaveService;
import com.neo.tiny.bpm.service.oa.impl.BpmOALeaveServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description: OA 自定义表单（diy） 请假单的结果的监听器实现类
 * @Author: yqz
 * @CreateDate: 2022/10/20 00:22
 */
@Component
public class BpmOALeaveResultListener extends BpmProcessInstanceResultEventListener {

    @Autowired
    private BpmOALeaveService leaveService;

    @Override
    protected String getProcessDefinitionKey() {
        return BpmOALeaveServiceImpl.PROCESS_KEY;
    }

    @Override
    protected void onEvent(BpmProcessInstanceResultEvent event) {
        leaveService.updateLeaveResult(Long.parseLong(event.getBusinessKey()), event.getResult());
    }
}
