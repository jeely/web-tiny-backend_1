package com.neo.tiny.bpm.controller.process;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.bpm.service.process.BpmModelService;
import com.neo.tiny.bpm.vo.model.*;
import com.neo.tiny.common.common.ResResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Description: 流程模型控制器
 * @Author: yqz
 * @CreateDate: 2022/9/25 23:37
 */
@Api(tags = "管理后台 - 流程模型")
@RestController
@RequestMapping("/bpm/model")
public class BpmModelController {

    @Autowired
    private BpmModelService bpmModelService;

    @ApiOperation("分页查询流程模型")
    @GetMapping("/page")
    public ResResult<IPage<BpmModelPageItemRespVO>> page(BpmModelPageReqVO vo) {

        return bpmModelService.modelPage(vo);

    }

    @ApiOperation("创建模型")
    @PostMapping("/create")
    public ResResult<String> create(@Valid @RequestBody BpmModelCreateReqVO vo) {

        return ResResult.success(bpmModelService.createModel(vo, null));
    }

    @PutMapping("/update")
    @ApiOperation(value = "修改模型")
    public ResResult<Boolean> updateModel(@Valid @RequestBody BpmModelUpdateReqVO modelVO) {
        bpmModelService.updateModel(modelVO);
        return ResResult.success(true);
    }

    @GetMapping("/get")
    @ApiOperation("获得模型")
    @ApiImplicitParam(name = "id", value = "编号", required = true, example = "1024", dataTypeClass = String.class)
    public ResResult<BpmModelRespVO> getModel(@RequestParam("id") String id) {
        BpmModelRespVO model = bpmModelService.getModel(id);
        return ResResult.success(model);
    }


    @PostMapping("/deploy")
    @ApiOperation(value = "部署模型")
    @ApiImplicitParam(name = "id", value = "编号", required = true, example = "1024", dataTypeClass = String.class)
    public ResResult<Boolean> deployModel(@RequestParam("id") String id) {
        bpmModelService.deployModel(id);
        return ResResult.success(true);
    }

    @PutMapping("/update-state")
    @ApiOperation(value = "修改模型的状态", notes = "实际更新的部署的流程定义的状态")
    public ResResult<Boolean> updateModelState(@Valid @RequestBody BpmModelUpdateStateReqVO reqVO) {
        bpmModelService.updateModelState(reqVO.getId(), reqVO.getState());
        return ResResult.success(true);
    }

    @DeleteMapping("/delete")
    @ApiOperation("删除模型")
    @ApiImplicitParam(name = "id", value = "编号", required = true, example = "1024", dataTypeClass = String.class)
    public ResResult<Boolean> deleteModel(@RequestParam("id") String id) {
        bpmModelService.deleteModel(id);
        return ResResult.success(true);
    }
}
