package com.neo.tiny.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.bpm.entity.BpmUserGroupDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: 流程用户分组
 * @author yqz
 * @date 2022-09-25 09:32:20
 */
@Mapper
public interface BpmUserGroupMapper extends BaseMapper<BpmUserGroupDO> {

}
