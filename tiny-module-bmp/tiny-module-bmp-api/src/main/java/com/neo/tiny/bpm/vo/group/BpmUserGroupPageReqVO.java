package com.neo.tiny.bpm.vo.group;

import com.neo.tiny.common.common.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @Description: 流程用户分组分页请求类
 * @author yqz
 * @date 2022-09-25 09:32:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "流程用户分组")
public class BpmUserGroupPageReqVO extends PageParam implements Serializable {

    /**
     * 分组名称
     */
    @ApiModelProperty("分组名称")
    private String groupName;


    /**
     * 开启状态：0-开启，1-关闭
     */
    @ApiModelProperty("开启状态：0-开启，1-关闭")
    private Integer status;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始创建时间")
    private Date beginCreateTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结束创建时间")
    private Date endCreateTime;


}
