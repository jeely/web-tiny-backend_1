package com.neo.tiny.bpm.vo.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;

/**
 * @Description: 管理后台 - 流程模型的分页的每一项
 * @Author: yqz
 * @CreateDate: 2022/9/25 23:50
 */
@ApiModel("管理后台 - 流程模型的分页的每一项 Response VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BpmModelPageItemRespVO extends BpmModelBaseVO {

    @ApiModelProperty(value = "编号", required = true, example = "1024")
    private String id;

    @ApiModelProperty(value = "表单名字", example = "请假表单")
    private String formName;

    @ApiModelProperty(value = "创建时间", required = true)
    private Date createTime;

    /**
     * 最新部署的流程定义
     */
    private ProcessDefinition processDefinition;

    @ApiModel("流程定义")
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ProcessDefinition {

        public ProcessDefinition(String id, Integer version) {
            this.id = id;
            this.version = version;
        }

        @ApiModelProperty(value = "编号", required = true, example = "1024")
        private String id;

        @ApiModelProperty(value = "版本", required = true, example = "1")
        private Integer version;

        @ApiModelProperty(value = "部署时间", required = true)
        private Date deploymentTime;

        @ApiModelProperty(value = "中断状态", required = true, example = "1", notes = "参见 SuspensionState 枚举")
        private Integer suspensionState;

    }

}