package com.neo.tiny.bpm.enums.instance;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description: 流程实例的状态
 * @Author: yqz
 * @CreateDate: 2022/10/8 23:43
 */
@Getter
@AllArgsConstructor
public enum BpmProcessInstanceStatusEnum {
    /**
     * 进行中
     */
    RUNNING(1, "进行中"),
    /**
     * 已完成
     */
    FINISH(2, "已完成");

    /**
     * 状态
     */
    private final Integer status;
    /**
     * 描述
     */
    private final String desc;
}
