package com.neo.tiny.bpm.dto.form;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Description: Bpm 表单的 Field 表单项 Response DTO
 * 字段的定义，可见 <a href="https://github.com/JakHuang/form-generator/issues/46">官方</a> 文档
 * @Author: yqz
 * @CreateDate: 2022/9/21 23:25
 */
@Data
public class BpmFormFieldRespDTO {

    /**
     * 表单标题
     */
    private String label;
    /**
     * 表单字段的属性名，可自定义
     */
    @JsonProperty(value = "vModel")
    private String vModel;
}
