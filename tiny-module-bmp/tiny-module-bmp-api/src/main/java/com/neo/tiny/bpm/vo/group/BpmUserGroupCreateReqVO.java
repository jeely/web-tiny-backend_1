package com.neo.tiny.bpm.vo.group;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 *
 * @Description: 创建流程用户分组
 * @author yqz
 * @date 2022-09-25 09:32:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "流程用户分组")
public class BpmUserGroupCreateReqVO extends BpmUserGroupBaseVO implements Serializable {



}
