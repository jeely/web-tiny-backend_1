package com.neo.tiny.bpm.vo.rule;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * @author yqzBpmFormUpdateReqVO.java
 * @Description: Bpm 任务规则表更新请求类
 * @date 2022-10-04 16:02:43
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "Bpm 任务规则表")
public class BpmTaskAssignRuleUpdateReqVO extends BpmTaskAssignRuleBaseVO implements Serializable {

    @ApiModelProperty(value = "任务分配规则的编号", required = true, example = "1024")
    @NotNull(message = "任务分配规则的编号不能为空")
    private Long id;

}
