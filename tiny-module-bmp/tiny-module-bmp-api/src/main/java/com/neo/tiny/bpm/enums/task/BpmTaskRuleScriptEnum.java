package com.neo.tiny.bpm.enums.task;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description: BPM 任务规则的脚本枚举
 * @Author: yqz
 * @CreateDate: 2022/10/4 17:37
 */
@Getter
@AllArgsConstructor
public enum BpmTaskRuleScriptEnum {
    /**
     * 流程发起人
     */
    START_USER(10L, "流程发起人"),
    /**
     * 流程发起人的一级领导
     */
    LEADER_X1(20L, "流程发起人的一级领导"),
    /**
     * 流程发起人的二级领导
     */
    LEADER_X2(21L, "流程发起人的二级领导");

    /**
     * 脚本编号
     */
    private final Long id;
    /**
     * 脚本描述
     */
    private final String desc;
}
