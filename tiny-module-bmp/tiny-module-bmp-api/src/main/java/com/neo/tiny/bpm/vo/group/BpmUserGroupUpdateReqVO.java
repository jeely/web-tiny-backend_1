package com.neo.tiny.bpm.vo.group;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * @author yqzBpmFormUpdateReqVO.java
 * @Description: 流程用户分组更新请求类
 * @date 2022-09-25 09:32:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "流程用户分组")
public class BpmUserGroupUpdateReqVO extends BpmUserGroupBaseVO implements Serializable {


    @ApiModelProperty("自增主键")
    @NotNull(message = "主键不能为空")
    private Long id;

}
