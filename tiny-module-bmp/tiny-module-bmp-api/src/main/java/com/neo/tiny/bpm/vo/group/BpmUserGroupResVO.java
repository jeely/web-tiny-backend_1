package com.neo.tiny.bpm.vo.group;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 *
 * @Description: 流程用户分组响应类
 * @author yqz
 * @date 2022-09-25 09:32:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "流程用户分组")
public class BpmUserGroupResVO extends BpmUserGroupBaseVO implements Serializable {

    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;
}
