package com.neo.tiny.bpm.vo.form;

import com.neo.tiny.common.common.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 工作流的表单定义
 *
 * @author yqz
 * @date 2022-09-15 00:08:25
 */
@Data
@ApiModel(description = "工作流的表单定义")
@EqualsAndHashCode(callSuper = true)
public class BpmFormVO extends PageParam {

    /**
     * 编号
     */
    @ApiModelProperty("编号")
    private Long id;

    /**
     * 表单名
     */
    @ApiModelProperty("表单名")
    private String name;

    /**
     * 开启状态
     */
    @ApiModelProperty("开启状态")
    private Integer status;

    /**
     * 表单的配置
     */
    @ApiModelProperty("表单的配置")
    private String conf;

    /**
     * 表单项的数组
     */
    @ApiModelProperty("表单项的数组")
    private String fields;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 租户编号
     */
    @ApiModelProperty("租户编号")
    private Long tenantId;


}
