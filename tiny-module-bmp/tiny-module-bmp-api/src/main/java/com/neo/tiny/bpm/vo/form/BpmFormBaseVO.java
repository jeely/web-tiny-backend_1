package com.neo.tiny.bpm.vo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Description: 流程表单基础类
 * @Author: yqz
 * @CreateDate: 2022/9/18 22:50
 */
@Data
public class BpmFormBaseVO {
    @ApiModelProperty(value = "表单名称")
    @NotNull(message = "表单名称不能为空")
    private String name;

    @ApiModelProperty(value = "表单状态", example = "1")
    @NotNull(message = "表单状态不能为空")
    private Integer status;

    @ApiModelProperty(value = "备注", example = "我是备注")
    private String remark;
}
