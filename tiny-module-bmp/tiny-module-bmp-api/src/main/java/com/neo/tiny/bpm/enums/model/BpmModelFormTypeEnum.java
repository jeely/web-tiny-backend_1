package com.neo.tiny.bpm.enums.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/9/26 00:46
 */
@Getter
@AllArgsConstructor
public enum BpmModelFormTypeEnum {
    // 对应 BpmFormDO
    NORMAL(10, "流程表单"),
    // 业务自己定义的表单，自己进行数据的存储
    CUSTOM(20, "业务表单");

    private final Integer type;
    private final String desc;
}
