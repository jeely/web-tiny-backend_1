package com.neo.tiny.bpm.vo.group;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;


/**
 *
 * @Description: 流程用户分组基础类
 * @author yqz
 * @date 2022-09-25 09:32:20
 */
@Data
@ApiModel(description = "流程用户分组")
public class BpmUserGroupBaseVO implements Serializable {

    /**
     * 自增主键
     */
    @ApiModelProperty("自增主键")
    private Long id;

    /**
     * 分组名称
     */
    @ApiModelProperty("分组名称")
    private String groupName;

    /**
     * 分组注释
     */
    @ApiModelProperty("分组注释")
    private String description;

    /**
     * 用户的id集合-list
     */
    @ApiModelProperty("用户的id集合-list")
    private Set<Long> userIds;

    /**
     * 开启状态：0-开启，1-关闭
     */
    @ApiModelProperty("开启状态：0-开启，1-关闭")
    private String status;


}
