package com.neo.tiny.bpm.vo.rule;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


/**
 *
 * @Description: 创建Bpm 任务规则表
 * @author yqz
 * @date 2022-10-04 16:02:43
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "Bpm 任务规则表")
public class BpmTaskAssignRuleCreateReqVO extends BpmTaskAssignRuleBaseVO implements Serializable {

    @ApiModelProperty(value = "流程模型的编号", required = true, example = "1024")
    @NotEmpty(message = "流程模型的编号不能为空")
    private String modelId;

    @ApiModelProperty(value = "流程任务定义的编号", required = true, example = "2048")
    @NotEmpty(message = "流程任务定义的编号不能为空")
    private String taskDefinitionKey;
}
