package com.neo.tiny.bpm.vo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @Description: 流程表单分页响应数据
 * @Author: yqz
 * @CreateDate: 2022/9/18 22:48
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BpmFormResVO extends BpmFormBaseVO {

    @ApiModelProperty(value = "表单编号")
    private Long id;

    @ApiModelProperty(value = "表单的配置", required = true, notes = "JSON 字符串")
    @NotNull(message = "表单的配置不能为空")
    private String conf;

    @ApiModelProperty(value = "表单项的数组", required = true, notes = "JSON 字符串的数组")
    @NotNull(message = "表单项的数组不能为空")
    private List<String> fields;

    @ApiModelProperty(value = "创建时间", required = true)
    private Date createTime;
}
