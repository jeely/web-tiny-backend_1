package com.neo.tiny.bpm.dto.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Description: BPM 发送流程实例被通过 Request DTO
 * @Author: yqz
 * @CreateDate: 2022/10/15 20:03
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BpmMessageSendWhenProcessInstanceApproveReqDTO {


    /**
     * 流程实例的编号
     */
    @NotEmpty(message = "流程实例的编号不能为空")
    private String processInstanceId;

    /**
     * 流程实例的名字
     */
    @NotEmpty(message = "流程实例的名字不能为空")
    private String processInstanceName;

    @NotNull(message = "发起人的用户编号")
    private Long startUserId;

}
