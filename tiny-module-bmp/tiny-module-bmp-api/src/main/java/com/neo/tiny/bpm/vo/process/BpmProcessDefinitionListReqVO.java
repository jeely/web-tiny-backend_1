package com.neo.tiny.bpm.vo.process;

import com.neo.tiny.common.common.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 管理后台 - 流程定义列表 Request VO
 * @Author: yqz
 * @CreateDate: 2022/10/8 22:19
 */
@ApiModel("管理后台 - 流程定义列表 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
public class BpmProcessDefinitionListReqVO extends PageParam {

    @ApiModelProperty(value = "中断状态", example = "1", notes = "参见 SuspensionState 枚举")
    private Integer suspensionState;

}
