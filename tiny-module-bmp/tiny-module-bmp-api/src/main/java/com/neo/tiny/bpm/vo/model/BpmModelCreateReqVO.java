package com.neo.tiny.bpm.vo.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @Description: 管理后台 - 流程模型的创建 Request VO
 * @Author: yqz
 * @CreateDate: 2022/9/27 22:36
 */
@Data
@ApiModel("管理后台 - 流程模型的创建 Request VO")
public class BpmModelCreateReqVO {
    @ApiModelProperty(value = "流程标识", required = true, example = "process_tiny")
    @NotEmpty(message = "流程标识不能为空")
    private String key;

    @ApiModelProperty(value = "流程名称", required = true, example = "tiny")
    @NotEmpty(message = "流程名称不能为空")
    private String name;

    @ApiModelProperty(value = "流程描述", example = "我是描述")
    private String description;

}
