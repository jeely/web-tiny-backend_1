package com.neo.tiny.bpm.vo.form;

import com.neo.tiny.common.common.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 流程表单分页查询请求入参
 * @Author: yqz
 * @CreateDate: 2022/9/18 23:21
 */
@Data
@ApiModel(description = "工作流的表单定义")
@EqualsAndHashCode(callSuper = true)
public class BpmFormPageReqVO extends PageParam {
    /**
     * 表单名
     */
    @ApiModelProperty("表单名")
    private String name;

    /**
     * 开启状态
     */
    @ApiModelProperty("开启状态")
    private Integer status;
}
