package com.neo.tiny.bpm.util;

import lombok.experimental.UtilityClass;
import org.springframework.util.StringUtils;

import java.util.regex.Pattern;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/9/27 22:44
 */
@UtilityClass
public class ValidationUtils {

    private static final Pattern PATTERN_XML_NCNAME = Pattern.compile("[a-zA-Z_][\\-_.0-9_a-zA-Z$]*");


    /**
     * 校验模型key
     * 需要以字母或下划线开头，后接任意字母、数字、中划线、下划线、句点！
     *
     * @param str 待校验数据
     * @return 校验结果
     */
    public boolean isXmlName(String str) {
        return StringUtils.hasText(str)
                && PATTERN_XML_NCNAME.matcher(str).matches();
    }
}
