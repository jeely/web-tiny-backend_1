package com.neo.tiny.bpm.enums.task;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

/**
 * @Description: 流程实例的结果
 * @Author: yqz
 * @CreateDate: 2022/10/15 19:45
 */
@Getter
@AllArgsConstructor
public enum BpmProcessInstanceResultEnum {

    /**
     * 处理中
     */
    PROCESS(1, "处理中"),
    /**
     * 通过
     */
    APPROVE(2, "通过"),
    /**
     * 不通过
     */
    REJECT(3, "不通过"),
    /**
     * 已取消
     */
    CANCEL(4, "已取消"),

    // ========== 流程任务独有的状态 ==========
    /**
     * 退回/驳回
     */
    BACK(5, "退回/驳回");

    /**
     * 结果
     * <p>
     * 如果新增时，注意 {@link #isEndResult(Integer)} 是否需要变更
     */
    private final Integer result;
    /**
     * 描述
     */
    private final String desc;

    /**
     * 判断该结果是否已经处于 End 最终结果
     * <p>
     * 主要用于一些结果更新的逻辑，如果已经是最终结果，就不再进行更新
     *
     * @param result 结果
     * @return 是否
     */
    public static boolean isEndResult(Integer result) {

        List<Integer> list = Arrays.asList(APPROVE.getResult(), REJECT.getResult(), CANCEL.getResult(), BACK.getResult());

        return list.contains(result);
    }
}
