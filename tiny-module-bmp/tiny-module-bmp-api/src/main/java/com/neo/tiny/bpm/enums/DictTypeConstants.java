package com.neo.tiny.bpm.enums;

/**
 * @Description: interface
 * @Author: yqz
 * @CreateDate: 2022/10/4 21:42
 */
public interface DictTypeConstants {

    /**
     * 任务分配规则类型
     */
    String TASK_ASSIGN_RULE_TYPE = "bpm_task_assign_rule_type";
    /**
     * 任务分配自定义脚本
     */
    String TASK_ASSIGN_SCRIPT = "bpm_task_assign_script";
}
