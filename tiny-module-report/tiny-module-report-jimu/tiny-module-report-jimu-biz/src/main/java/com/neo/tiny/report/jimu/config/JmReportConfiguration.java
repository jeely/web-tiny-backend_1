package com.neo.tiny.report.jimu.config;

import com.neo.tiny.oauth.api.Oauth2TokenApi;
import com.neo.tiny.report.jimu.security.JimuReportTokenServiceImpl;
import org.jeecg.modules.jmreport.api.JmReportTokenServiceI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Description: 积木报表配置，扫描积木报表的包
 * @Author: yqz
 * @CreateDate: 2022/12/10 23:25
 */
@Configuration(proxyBeanMethods = false)
@ComponentScan(basePackages = "org.jeecg.modules.jmreport")
public class JmReportConfiguration {

    @Bean
    public JmReportTokenServiceI jmReportTokenService(Oauth2TokenApi oauth2TokenApi, UserDetailsService userDetailsService) {
        return new JimuReportTokenServiceImpl(oauth2TokenApi, userDetailsService);
    }
}
