# 报表设计

​		该模块准备使用开源的ureport和积木报表，关于二者优劣，可以参考：[积木报表JimuReport跟ureport2对比 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/357323394)

模块结构

```

├── tiny-module-ureport
│   ├── src
│   └── pom.xml
├── tiny-module-report-jimu
│   ├── tiny-module-report-jimu-biz
│   ├── tiny-module-report-jimu-api
│   └── pom.xml
├── pom.xml
└── README.md

```

# 一、积木报表

## 1.1 初始化sql脚本

可参考官方文档[项目介绍 · JimuReport 积木报表 · 看云 (jeecg.com)](http://report.jeecg.com/2078898)

脚本位置`/doc/sql/report/jimureport.mysql5.7.create.sql`

## 1.2 引入依赖

pom中引入积木报表最新依赖

```xml
  <dependency>
    <groupId>org.jeecgframework.jimureport</groupId>
    <artifactId>jimureport-spring-boot-starter</artifactId>
    <version>1.5.5</version>
  </dependency>
```

## 1.3 添加积木扫描目录

```java
package com.neo.tiny.report.jimu.config;

import com.neo.tiny.oauth.api.Oauth2TokenApi;
import com.neo.tiny.report.jimu.security.JimuReportTokenServiceImpl;
import org.jeecg.modules.jmreport.api.JmReportTokenServiceI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Description: 积木报表配置，扫描积木报表的包
 * @Author: yqz
 * @CreateDate: 2022/12/10 23:25
 */
@Configuration(proxyBeanMethods = false)
@ComponentScan(basePackages = "org.jeecg.modules.jmreport")
public class JmReportConfiguration {

    @Bean
    public JmReportTokenServiceI jmReportTokenService(Oauth2TokenApi oauth2TokenApi, UserDetailsService userDetailsService) {
        return new JimuReportTokenServiceImpl(oauth2TokenApi, userDetailsService);
    }
}

```

## 1.4 SecurityConfig拦截排除

修改`com.neo.tiny.secrity.config.AdminSecurityConfig`配置，添加`/jmreport/**`白名单

```java
package com.neo.tiny.secrity.config;

import com.neo.tiny.config.BaseSecurityConfig;
import com.neo.tiny.secrity.filter.RedisAuthenticationTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/3 18:04
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AdminSecurityConfig extends BaseSecurityConfig {

    @Autowired
    private RedisAuthenticationTokenFilter authenticationTokenFilter;

    @Override
    public String[] getWhiteUri() {
        return new String[]{
                "/hello/world",
                //积木报表
                "/jmreport/**"
        };
    }

    @Override
    public OncePerRequestFilter getRequestFilter() {
        return authenticationTokenFilter;
    }
}
```

## 1.5 用户token传递

​	前端使用`iframe`将报表页面嵌入到系统内，这里统一使用同一个封装后的iframe组件。

### 1.5.1 菜单配置：

将报表的请求路径添加到菜单`http://localhost:8088/jmreport/list`

![config-menu](png/config-menu.png)

### 1.5.2 路由参数

路由参数必须使用json结构，否则会解析错误

```json
{
    "path": "http://localhost:8088/jmreport/list", // iframe跳转地址
    "neadToken": "yes" // 是否需要token，yes--需要，no--不需要
}
```

`/frame/layouts/IframePageView.vue`组件初始化会加载获取路由参数

```js
getUrl() {
  let params = this.$route.query;
  console.log("参数", JSON.stringify(params))
  if (params && params.path) {
    let path = params.path;
    // 是否需要token：0-需要，1-不需要
    this.url = params.neadToken === 'yes' ? path = path + "?token="+ store.getters.access_token : path;
    console.log("------url------" + this.url)
    this.id = this.$route.path
  }
}
```

### 1.5.3 前端iframe解析流程

当前端配置路由参数后，点击左边导航栏时会经过以下方法：

**第一步：**`page/index/sidebar/sidebarItem.vue`的`open`方法

```js
open(item) {
      if (this.screen <= 1) this.$store.commit("SET_COLLAPSE");
      this.$router.$avueRouter.group = item.group;
      console.log("菜单配置的路由参数", item.query)
      let params = item.query ? JSON.parse(item.query) : null;
      console.log("JSON化后的菜单配置的路由参数", params)
      this.$router.push({
        path: this.$router.$avueRouter.getPath({
          name: item[this.labelKey],
          src: item[this.pathKey]
        }),
        query: params
      }).catch(() => {
      });
    }
```

**第二步：**该方法执行时会调用`/router/avue-router.js`的`getPath`方法，如果是超链接，则直接跳转

```js
// 处理路由
getPath: function(params) {
  const { src } = params
  let result = src || '/'
  // 判断是否为超链接，即以http｜https开头
  if (src.substring(0,4) === 'http' || src.substring(0,5) === 'https') {
    result = `/myiframe/urlPath?${objToform(params)}`
  }
  return result
},
```

**第三步：**`permission.js`拦截路由

此处会判断菜单是否为超链接，如果是则open新标签页，value为路由地址path

```js
// 针对外链跳转
if (value.substring(0,4) === 'http' || value.substring(0,5) === 'https') {
  NProgress.done();
  // 如果是链接，则打开新窗口
  window.open(value, "_blank");
  return;
}
```

