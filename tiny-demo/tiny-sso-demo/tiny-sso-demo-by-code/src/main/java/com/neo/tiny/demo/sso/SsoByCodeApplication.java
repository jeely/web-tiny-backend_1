package com.neo.tiny.demo.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description: 使用授权码模式单点登录
 * @Author: yqz
 * @CreateDate: 2022/11/13 20:03
 */
@SpringBootApplication
public class SsoByCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsoByCodeApplication.class, args);
    }
}
