package com.neo.tiny.demo.sso.controller;

import com.neo.tiny.demo.sso.api.OauthApi;
import com.neo.tiny.demo.sso.common.ResResult;
import com.neo.tiny.demo.sso.dto.UserInfoRespDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 用户信息
 * @Author: yqz
 * @CreateDate: 2022/11/13 22:05
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private OauthApi oauthApi;

    @GetMapping("/info")
    public ResResult<UserInfoRespDTO> getUserInfo() {

        return oauthApi.getUserInfo();
    }

}
