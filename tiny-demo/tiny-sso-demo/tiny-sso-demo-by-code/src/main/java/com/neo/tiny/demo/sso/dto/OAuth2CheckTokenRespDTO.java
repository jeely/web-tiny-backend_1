package com.neo.tiny.demo.sso.dto;

import lombok.Data;

import java.util.List;

/**
 * @Description: 校验token
 * @Author: yqz
 * @CreateDate: 2022/11/13 20:58
 */
@Data
public class OAuth2CheckTokenRespDTO {


    /**
     * 用户编号
     */
    private Long userId;


    /**
     * 用户名
     */
    private String userName;


    /**
     * 用户类型
     */
    private Integer userType;


    private String clientId;

    /**
     * 授权范围
     */
    private List<String> scopes;

    /**
     * 访问令牌
     */
    private String accessToken;

    /**
     * 过期时间
     * <p>
     * 时间戳 / 1000，即单位：秒
     */
    private Long exp;

}
