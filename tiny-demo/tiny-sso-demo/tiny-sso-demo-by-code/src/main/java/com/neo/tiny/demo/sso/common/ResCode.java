package com.neo.tiny.demo.sso.common;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/3 18:15
 */
public enum ResCode implements ResErrorCode{
    SUCCESS(200, "操作成功"),
    FAILED(500, "操作失败"),
    VALIDATE_FAILED(500, "参数检验失败"),
    UNAUTHORIZED(401, "暂未登录或token已经过期"),
    FORBIDDEN(403, "没有相关权限");
    private Integer code;
    private String msg;

    ResCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
