package com.neo.tiny.demo.sso.config.security.model;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/11/13 20:53
 */
@Data
public class AdminUserDetails implements UserDetails {

    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户类型
     */
    private Integer userType;

    /**
     * 授权范围
     */
    private List<String> scopes;

    /**
     * 访问令牌
     */
    private String accessToken;

    /**
     * 权限信息
     */
    private Collection<GrantedAuthority> authorities;

    public AdminUserDetails(Long userId, String userName, Integer userType, List<String> scopes, String accessToken) {
        this.userId = userId;
        this.userName = userName;
        this.userType = userType;
        this.scopes = scopes;
        this.accessToken = accessToken;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }


    @Override
    public boolean isEnabled() {
        return true;
    }
}
