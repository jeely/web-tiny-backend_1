package com.neo.tiny.demo.sso.common;

/**
 * @Description: 错误码与错误信息
 * @Author: yqz
 * @CreateDate: 2022/9/21 23:33
 */
public interface ResErrorCode {

    /**
     * 获取错误码
     *
     * @return 错误码
     */
    Integer getCode();

    /**
     * 获取错误信息
     *
     * @return 错误信息
     */
    String getMsg();
}
