package com.neo.tiny.demo.sso.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/11/13 20:41
 */
@Data
public class OAuth2AccessTokenRespDTO {
    /**
     * 访问令牌
     */
    private String accessToken;

    /**
     * 刷新令牌
     */
    private String refreshToken;

    /**
     * 令牌类型
     */
    private String tokenType;

    /**
     * 过期时间；单位：秒
     */
    private Long expiresIn;

    /**
     * 授权范围；如果多个授权范围，使用空格分隔
     */
    private String scope;
}
