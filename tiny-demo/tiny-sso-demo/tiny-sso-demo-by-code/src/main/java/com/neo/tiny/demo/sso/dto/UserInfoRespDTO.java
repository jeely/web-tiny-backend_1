package com.neo.tiny.demo.sso.dto;

import lombok.Data;

import java.util.List;

/**
 * @Description: 用户信息
 * @Author: yqz
 * @CreateDate: 2022/11/13 22:10
 */
@Data
public class UserInfoRespDTO {

    private Long userId;

    private String userName;

    private String nickName;

    private String email;

    private String phone;

    private Integer sex;

    private String avatar;

    /**
     * 所在部门
     */
    private Dept dept;

    /**
     * 所属岗位数组
     */
    private List<Post> posts;

    @Data
    public static class Dept {

        private Long deptId;

        private String deptName;

    }

    @Data
    public static class Post {

        private Long postId;

        private String postName;

    }
}
