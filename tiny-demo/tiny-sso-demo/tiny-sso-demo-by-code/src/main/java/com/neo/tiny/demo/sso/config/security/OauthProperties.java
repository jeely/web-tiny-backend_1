package com.neo.tiny.demo.sso.config.security;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/11/13 20:24
 */
@Data
@Component
@ConfigurationProperties("oauth.sso")
public class OauthProperties {

    /**
     * 认证授权服务地址
     */
    private String baseUrl;

    /**
     * 客户端id
     */
    private String clientId;

    /**
     * 客户端密码
     */
    private String clientSecret;

    /**
     * 回调地址
     */
    private String redirectUri;
}
