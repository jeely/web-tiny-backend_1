/*
 Navicat Premium Data Transfer

 Source Server         : home
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : 22q376j554.oicp.vip:3366
 Source Schema         : tiny-web-backend

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 29/10/2022 12:06:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for business_machine
-- ----------------------------
DROP TABLE IF EXISTS `business_machine`;
CREATE TABLE `business_machine` (
  `ticket_machine_id` bigint NOT NULL AUTO_INCREMENT COMMENT '取号机id（自增）',
  `ticket_machine_key` varchar(40) DEFAULT NULL COMMENT '取号机编码',
  `machine_area` varchar(40) DEFAULT NULL COMMENT '取号机所属业务区',
  `mac_address` varchar(80) DEFAULT NULL COMMENT '取号机物理地址',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标记：0-正常，1-删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(30) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`ticket_machine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='取号机';

-- ----------------------------
-- Table structure for business_queue
-- ----------------------------
DROP TABLE IF EXISTS `business_queue`;
CREATE TABLE `business_queue` (
  `queue_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `queue_key` varchar(80) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '队列标识',
  `queue_name` varchar(40) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '队列名称',
  `queue_area` varchar(40) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '队列所在区域（专区）',
  `queue_describe` varchar(1024) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '队列描述',
  `ticket_max_limit` int DEFAULT NULL COMMENT '日最大取号量限制',
  `del_flag` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标记：0-正常，1-删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`queue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='取叫号队列';

-- ----------------------------
-- Table structure for business_queue_service_relation
-- ----------------------------
DROP TABLE IF EXISTS `business_queue_service_relation`;
CREATE TABLE `business_queue_service_relation` (
  `relation_id` bigint NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `queue_id` bigint DEFAULT NULL COMMENT '队列id',
  `service_id` varchar(80) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '业务办理项id',
  `del_flag` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标记：0-正常，1-删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`relation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='队列&办理项关联';

-- ----------------------------
-- Table structure for business_queue_ticket_machine_relation
-- ----------------------------
DROP TABLE IF EXISTS `business_queue_ticket_machine_relation`;
CREATE TABLE `business_queue_ticket_machine_relation` (
  `relation_id` bigint NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `queue_id` bigint DEFAULT NULL COMMENT '队列id',
  `ticket_machine_id` bigint DEFAULT NULL COMMENT '取号机id',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标记：0-正常，1-删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(30) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`relation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='队列&取号机关联';

-- ----------------------------
-- Table structure for business_queue_window_relation
-- ----------------------------
DROP TABLE IF EXISTS `business_queue_window_relation`;
CREATE TABLE `business_queue_window_relation` (
  `relation_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `queue_id` bigint DEFAULT NULL COMMENT '队列id',
  `window_id` bigint DEFAULT NULL COMMENT '窗口id',
  `del_flag` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标记：0-正常，1-删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`relation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='队列&窗口关联';

-- ----------------------------
-- Table structure for business_ticket_order
-- ----------------------------
DROP TABLE IF EXISTS `business_ticket_order`;
CREATE TABLE `business_ticket_order` (
  `order_id` bigint NOT NULL COMMENT '自增主键',
  `business_id` varchar(60) DEFAULT NULL COMMENT '业务编码（交易流水号）',
  `ticket_no` varchar(40) DEFAULT NULL COMMENT '票号',
  `window_id` bigint DEFAULT NULL COMMENT '窗口id',
  `window_name` varchar(40) DEFAULT NULL COMMENT '窗口名（缴话费）',
  `window_no` varchar(20) DEFAULT NULL COMMENT '窗口号（A01）',
  `queue_id` bigint DEFAULT NULL COMMENT '队列主键',
  `queue_key` varchar(40) DEFAULT NULL COMMENT '队列标识',
  `queue_name` varchar(40) DEFAULT NULL COMMENT '队列名称',
  `ticket_status` int DEFAULT NULL COMMENT '取号状态：0-预约，1-取号，2-叫号，3-离开，4-过号',
  `ticket_print_time` datetime DEFAULT NULL COMMENT '取号时间',
  `ticket_call_time` datetime DEFAULT NULL COMMENT '叫号时间',
  `leave_time` datetime DEFAULT NULL COMMENT '离开时间',
  `pass_time` datetime DEFAULT NULL COMMENT '过号时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标记：0-正常，1-删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(30) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='业务流水';

-- ----------------------------
-- Table structure for business_window
-- ----------------------------
DROP TABLE IF EXISTS `business_window`;
CREATE TABLE `business_window` (
  `window_id` bigint NOT NULL AUTO_INCREMENT COMMENT '窗口id（自增）',
  `window_name` varchar(40) DEFAULT NULL COMMENT '窗口名（缴话费）',
  `window_no` varchar(20) DEFAULT NULL COMMENT '窗口号（A01）',
  `window_key` varchar(40) DEFAULT NULL COMMENT '窗口编码',
  `window_status` int DEFAULT '0' COMMENT '窗口状态（0-空闲，1-受理中，2-暂停服务）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标记：0-正常，1-删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(30) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(30) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`window_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='窗口';

SET FOREIGN_KEY_CHECKS = 1;
