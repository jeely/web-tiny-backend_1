/*
 Navicat Premium Data Transfer

 Source Server         : home
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : 22q376j554.oicp.vip:3366
 Source Schema         : tiny-web-backend

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 11/05/2023 10:39:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `status` int(11) NULL DEFAULT 0 COMMENT '0：有效，1：无效',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '逻辑删除：0-正常，1-删除',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '系统名称', 'SYS_NAME', '基础脚手架', '1', 0, '0', 'youngAdmin', '2022-09-04 17:04:25', 'youngAdmin', '2022-09-04 17:04:25', NULL);
INSERT INTO `sys_config` VALUES (2, '系统名称', 'SYS_NAME', '基础脚手架', '1', 0, '1', 'youngAdmin', '2022-09-04 17:04:25', 'youngAdmin', '2022-09-04 17:04:33', NULL);
INSERT INTO `sys_config` VALUES (3, '系统名称', 'SYS_NAME', '基础脚手架', '1', 0, '1', 'youngAdmin', '2022-09-04 17:04:25', 'youngAdmin', '2022-09-04 17:04:35', NULL);
INSERT INTO `sys_config` VALUES (4, 'STORAGE_CONFIG', 'STORAGE_CONFIG', '{\"local\":{\"enable\":false,\"name\":\"local\",\"address\":\"/local/storage/\",\"storagePath\":\"/Users/yqz/MyData/MyData/Code/CodeWorkSpace/tiny/web-tiny-backend/local/storage/\"},\"minIo\":{\"enable\":true,\"name\":\"minio\",\"accessKey\":\"admin\",\"secretKey\":\"admin123456\",\"endpoint\":\"http://192.168.31.10:9001\",\"bucketName\":\"web-tiny\"}}', 'N', 0, '0', 'youngAdmin', '2022-12-03 15:23:30', 'youngAdmin', '2022-12-03 15:23:30', NULL);
INSERT INTO `sys_config` VALUES (5, '1qqq', 'WWE', '1', '1', 0, '1', 'youngAdmin', '2022-12-27 00:21:23', 'youngAdmin', '2022-12-27 00:21:40', NULL);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(11) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 0, '', '郑州分公司', 0, '3', NULL, NULL, '0', '0', 'youngAdmin', '2022-08-16 13:58:46', 'yqz', '2022-10-17 11:05:07');
INSERT INTO `sys_dept` VALUES (2, 1, '', '业务一部', 0, '2', NULL, NULL, '0', '0', 'youngAdmin', '2022-08-16 13:59:11', 'yqz', '2022-10-17 11:04:43');
INSERT INTO `sys_dept` VALUES (3, 0, '', '董事会', 1, NULL, NULL, NULL, '0', '0', 'youngAdmin', '2022-08-25 22:57:38', 'youngAdmin', '2022-08-25 22:57:38');
INSERT INTO `sys_dept` VALUES (4, 3, '', '股东组', 2, '1', NULL, NULL, '0', '0', 'youngAdmin', '2022-08-25 22:57:51', 'yqz', '2022-10-17 11:53:56');

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_id` int(11) NULL DEFAULT NULL COMMENT 'sys_dict_type.dict_id',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `color_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '颜色类型',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '解释',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `dict_sort` bigint(20) NULL DEFAULT 0 COMMENT '字典排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 74 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES (1, 1, '系统类', '1', 'dict_type', 'default', '系统类字典', '0', 0, 'admin', '2022-09-04 11:41:28', 'youngAdmin', '2022-09-21 22:35:12', '系统类');
INSERT INTO `sys_dict_item` VALUES (2, 1, '业务类', '0', 'dict_type', 'default', '业务类字典', '0', 1, 'admin', '2022-09-04 11:41:28', 'youngAdmin', '2022-09-21 22:35:15', '业务类');
INSERT INTO `sys_dict_item` VALUES (3, 2, '病假', '1', 'bpm_oa_leave_type', NULL, '病假', '0', 1, 'youngAdmin', '2022-09-04 15:50:05', 'youngAdmin', '2022-09-04 15:50:05', '病假');
INSERT INTO `sys_dict_item` VALUES (4, 2, '事假', '2', 'bpm_oa_leave_type', NULL, '2', '0', 2, 'youngAdmin', '2022-09-04 15:54:17', 'youngAdmin', '2022-09-04 15:54:17', '事假');
INSERT INTO `sys_dict_item` VALUES (5, 2, '婚假', '3', 'bpm_oa_leave_type', NULL, '婚假', '0', 3, 'youngAdmin', '2022-09-04 15:54:30', 'youngAdmin', '2022-09-04 15:55:45', '婚假');
INSERT INTO `sys_dict_item` VALUES (6, 3, '状态正常', '0', 'status_type', NULL, '状态正常', '0', 1, 'youngAdmin', '2022-09-04 16:00:03', 'youngAdmin', '2022-09-04 16:00:03', '状态正常');
INSERT INTO `sys_dict_item` VALUES (7, 3, '状态冻结', '9', 'status_type', NULL, '状态冻结', '0', 2, 'youngAdmin', '2022-09-04 16:00:15', 'youngAdmin', '2022-09-04 16:00:15', NULL);
INSERT INTO `sys_dict_item` VALUES (8, 4, '检索', '1', 'param_type', NULL, '检索', '0', 1, 'youngAdmin', '2022-09-04 16:54:47', 'youngAdmin', '2022-09-04 16:54:47', '检索');
INSERT INTO `sys_dict_item` VALUES (9, 4, '原文', '2', 'param_type', NULL, '原文', '0', 2, 'youngAdmin', '2022-09-04 16:54:59', 'youngAdmin', '2022-09-04 16:54:59', '原文');
INSERT INTO `sys_dict_item` VALUES (10, 4, '报表', '3', 'param_type', NULL, '报表', '0', 3, 'youngAdmin', '2022-09-04 16:55:11', 'youngAdmin', '2022-09-04 16:55:11', '报表');
INSERT INTO `sys_dict_item` VALUES (11, 4, '安全', '4', 'param_type', NULL, '安全', '0', 4, 'youngAdmin', '2022-09-04 16:55:28', 'youngAdmin', '2022-09-04 16:55:28', '安全');
INSERT INTO `sys_dict_item` VALUES (12, 4, '文档', '4', 'param_type', NULL, '文档', '0', 4, 'youngAdmin', '2022-09-04 16:55:46', 'youngAdmin', '2022-09-04 16:55:46', '文档');
INSERT INTO `sys_dict_item` VALUES (13, 4, '消息', '6', 'param_type', NULL, '消息', '0', 6, 'youngAdmin', '2022-09-04 16:56:03', 'youngAdmin', '2022-09-04 16:56:03', '消息');
INSERT INTO `sys_dict_item` VALUES (14, 4, '其他', '9', 'param_type', NULL, '其他', '0', 7, 'youngAdmin', '2022-09-04 16:56:23', 'youngAdmin', '2022-09-04 16:56:59', '其他');
INSERT INTO `sys_dict_item` VALUES (15, 4, '默认', '0', 'param_type', NULL, '默认', '0', 8, 'youngAdmin', '2022-09-04 16:56:45', 'youngAdmin', '2022-09-04 16:56:45', '默认');
INSERT INTO `sys_dict_item` VALUES (16, 5, '默认', 'default', 'label_color_type', 'default', '默认', '0', 0, 'youngAdmin', '2022-09-21 22:31:39', 'youngAdmin', '2022-09-21 22:32:45', NULL);
INSERT INTO `sys_dict_item` VALUES (17, 5, '主要', 'primary', 'label_color_type', 'primary', '主要', '1', 1, 'youngAdmin', '2022-09-21 22:31:59', 'youngAdmin', '2022-09-21 22:34:39', NULL);
INSERT INTO `sys_dict_item` VALUES (18, 5, '成功', 'success', 'label_color_type', 'success', '成功', '0', 2, 'youngAdmin', '2022-09-21 22:33:12', 'youngAdmin', '2022-09-21 22:34:43', NULL);
INSERT INTO `sys_dict_item` VALUES (19, 5, '信息', 'info', 'label_color_type', 'info', '信息', '0', 3, 'youngAdmin', '2022-09-21 22:33:34', 'youngAdmin', '2022-09-21 22:34:48', NULL);
INSERT INTO `sys_dict_item` VALUES (20, 5, '警告', 'warning', 'label_color_type', 'warning', '警告', '0', 4, 'youngAdmin', '2022-09-21 22:33:53', 'youngAdmin', '2022-09-21 22:34:53', NULL);
INSERT INTO `sys_dict_item` VALUES (21, 5, '危险', 'danger', 'label_color_type', 'danger', '危险', '0', 5, 'youngAdmin', '2022-09-21 22:34:11', 'youngAdmin', '2022-09-21 22:34:58', NULL);
INSERT INTO `sys_dict_item` VALUES (22, 6, '开启', '0', 'common_status', 'primary', '开启', '0', 0, 'youngAdmin', '2022-09-22 00:46:25', 'youngAdmin', '2022-09-22 00:57:22', NULL);
INSERT INTO `sys_dict_item` VALUES (23, 6, '关闭', '1', 'common_status', 'warning', '关闭', '0', 1, 'youngAdmin', '2022-09-22 00:46:43', 'youngAdmin', '2022-09-22 00:57:29', NULL);
INSERT INTO `sys_dict_item` VALUES (24, 7, '业务表单', '20', 'bpm_model_form_type', 'default', '业务表单', '0', 20, 'youngAdmin', '2022-09-28 22:15:56', 'youngAdmin', '2022-09-28 22:15:56', '业务表单');
INSERT INTO `sys_dict_item` VALUES (25, 7, '流程表单', '10', 'bpm_model_form_type', 'default', '流程表单', '0', 10, 'youngAdmin', '2022-09-28 22:16:13', 'youngAdmin', '2022-09-28 22:16:19', '流程表单');
INSERT INTO `sys_dict_item` VALUES (26, 8, '默认', '1', 'bpm_model_category', 'primary', '默认', '0', 1, 'youngAdmin', '2022-09-28 22:35:46', 'youngAdmin', '2022-10-16 18:35:21', '');
INSERT INTO `sys_dict_item` VALUES (27, 8, 'OA', '2', 'bpm_model_category', 'success', 'OA', '0', 2, 'youngAdmin', '2022-09-28 22:36:00', 'youngAdmin', '2022-10-20 00:14:52', '');
INSERT INTO `sys_dict_item` VALUES (28, 9, '自定义脚本', '50', 'bpm_task_assign_rule_type', 'default', '自定义脚本', '0', 50, 'youngAdmin', '2022-10-04 18:41:47', 'youngAdmin', '2022-10-04 18:41:47', '自定义脚本');
INSERT INTO `sys_dict_item` VALUES (29, 9, '用户组', '40', 'bpm_task_assign_rule_type', 'default', '用户组', '0', 40, 'youngAdmin', '2022-10-04 18:42:08', 'youngAdmin', '2022-10-04 18:42:08', '用户组');
INSERT INTO `sys_dict_item` VALUES (30, 9, '用户', '30', 'bpm_task_assign_rule_type', 'default', '用户', '0', 30, 'youngAdmin', '2022-10-04 18:42:23', 'youngAdmin', '2022-10-04 18:42:23', '用户');
INSERT INTO `sys_dict_item` VALUES (31, 9, '岗位', '22', 'bpm_task_assign_rule_type', 'default', '岗位', '0', 22, 'youngAdmin', '2022-10-04 18:42:44', 'youngAdmin', '2022-10-04 18:42:44', '岗位');
INSERT INTO `sys_dict_item` VALUES (32, 9, '部门的负责人', '21', 'bpm_task_assign_rule_type', 'default', '部门的负责人', '0', 21, 'youngAdmin', '2022-10-04 18:43:00', 'youngAdmin', '2022-10-04 18:43:00', '部门的负责人');
INSERT INTO `sys_dict_item` VALUES (33, 9, '部门的成员', '20', 'bpm_task_assign_rule_type', 'default', '部门的成员', '0', 20, 'youngAdmin', '2022-10-04 18:43:20', 'youngAdmin', '2022-10-04 18:43:20', '部门的成员');
INSERT INTO `sys_dict_item` VALUES (34, 9, '角色', '10', 'bpm_task_assign_rule_type', 'default', '角色', '0', 10, 'youngAdmin', '2022-10-04 18:43:39', 'youngAdmin', '2022-10-04 18:43:39', '');
INSERT INTO `sys_dict_item` VALUES (35, 10, '流程发起人', '10', 'bpm_task_assign_script', 'default', '流程发起人', '0', 10, 'youngAdmin', '2022-10-04 22:43:17', 'youngAdmin', '2022-10-04 22:43:29', '');
INSERT INTO `sys_dict_item` VALUES (36, 10, '	 流程发起人的一级领导', '20', 'bpm_task_assign_script', 'default', '	 流程发起人的一级领导', '0', 20, 'youngAdmin', '2022-10-04 22:43:51', 'youngAdmin', '2022-10-04 22:44:37', '');
INSERT INTO `sys_dict_item` VALUES (37, 10, '流程发起人的二级领导', '21', 'bpm_task_assign_script', 'default', '流程发起人的二级领导', '0', 21, 'youngAdmin', '2022-10-04 22:44:50', 'youngAdmin', '2022-10-04 22:44:50', '');
INSERT INTO `sys_dict_item` VALUES (38, 11, '已完成', '2', 'bpm_process_instance_status', 'success', '已完成', '0', 2, 'youngAdmin', '2022-10-16 18:18:29', 'youngAdmin', '2022-10-16 23:09:07', '');
INSERT INTO `sys_dict_item` VALUES (39, 11, '进行中', '1', 'bpm_process_instance_status', 'primary', '进行中', '0', 1, 'youngAdmin', '2022-10-16 18:18:52', 'youngAdmin', '2022-10-16 18:34:35', '');
INSERT INTO `sys_dict_item` VALUES (40, 12, '处理中', '1', 'bpm_process_instance_result', 'primary', '处理中', '0', 1, 'youngAdmin', '2022-10-16 18:21:50', 'youngAdmin', '2022-10-16 18:35:00', '');
INSERT INTO `sys_dict_item` VALUES (41, 12, '通过', '2', 'bpm_process_instance_result', 'success', '通过', '0', 2, 'youngAdmin', '2022-10-16 18:22:05', 'youngAdmin', '2022-10-16 23:09:16', '');
INSERT INTO `sys_dict_item` VALUES (42, 12, '不通过', '3', 'bpm_process_instance_result', 'warning', '不通过', '0', 3, 'youngAdmin', '2022-10-16 18:22:20', 'youngAdmin', '2022-10-16 23:09:23', '');
INSERT INTO `sys_dict_item` VALUES (43, 12, '已取消', '4', 'bpm_process_instance_result', 'danger', '已取消', '0', 4, 'youngAdmin', '2022-10-16 18:22:37', 'youngAdmin', '2022-10-16 23:09:29', '');
INSERT INTO `sys_dict_item` VALUES (44, 13, '空闲', '0', 'window_status', 'primary', '空闲', '0', 0, 'youngAdmin', '2022-10-29 20:41:04', 'youngAdmin', '2022-10-29 20:41:04', '空闲');
INSERT INTO `sys_dict_item` VALUES (45, 13, '受理中', '1', 'window_status', 'success', '受理中', '0', 1, 'youngAdmin', '2022-10-29 20:41:20', 'youngAdmin', '2022-10-29 20:41:31', '受理中');
INSERT INTO `sys_dict_item` VALUES (46, 13, '暂停服务', '2', 'window_status', 'info', '暂停服务', '0', 2, 'youngAdmin', '2022-10-29 20:41:48', 'youngAdmin', '2022-10-29 20:41:48', '暂停服务');
INSERT INTO `sys_dict_item` VALUES (47, 14, '预约', '0', 'ticket_status', 'primary', '预约', '0', 0, 'youngAdmin', '2022-10-30 10:38:48', 'youngAdmin', '2022-10-30 10:38:48', '预约');
INSERT INTO `sys_dict_item` VALUES (48, 14, '取号', '10', 'ticket_status', 'success', '取号', '0', 10, 'youngAdmin', '2022-10-30 10:41:22', 'youngAdmin', '2022-10-30 10:41:22', '取号');
INSERT INTO `sys_dict_item` VALUES (49, 14, '叫号', '20', 'ticket_status', 'info', '叫号', '0', 20, 'youngAdmin', '2022-10-30 10:41:38', 'youngAdmin', '2022-10-30 10:41:38', '叫号');
INSERT INTO `sys_dict_item` VALUES (50, 14, '离开', '30', 'ticket_status', 'info', '离开', '0', 30, 'youngAdmin', '2022-10-30 10:42:00', 'youngAdmin', '2022-10-30 10:42:00', '离开');
INSERT INTO `sys_dict_item` VALUES (51, 14, '过号', '40', 'ticket_status', 'warning', '过号', '0', 40, 'youngAdmin', '2022-10-30 10:42:16', 'youngAdmin', '2022-10-30 10:42:16', '过号');
INSERT INTO `sys_dict_item` VALUES (52, 15, 'password', 'password', 'system_oauth2_grant_type', 'default', '密码模式', '0', 1, 'youngAdmin', '2022-11-10 17:30:43', 'youngAdmin', '2022-11-10 18:13:42', '密码模式');
INSERT INTO `sys_dict_item` VALUES (53, 15, '授权码模式', 'authorization_code', 'system_oauth2_grant_type', 'primary', '授权码模式', '0', 2, 'youngAdmin', '2022-11-10 17:31:21', 'youngAdmin', '2022-11-10 18:13:44', '授权码模式');
INSERT INTO `sys_dict_item` VALUES (54, 15, '简化模式', 'implicit', 'system_oauth2_grant_type', 'success', '简化模式', '0', 3, 'youngAdmin', '2022-11-10 17:31:37', 'youngAdmin', '2022-11-10 18:13:47', '简化模式');
INSERT INTO `sys_dict_item` VALUES (55, 15, '客户端模式', 'client_credentials', 'system_oauth2_grant_type', 'info', '客户端模式', '0', 4, 'youngAdmin', '2022-11-10 17:31:52', 'youngAdmin', '2022-11-10 18:13:50', '客户端模式');
INSERT INTO `sys_dict_item` VALUES (56, 15, '刷新模式', 'refresh_token', 'system_oauth2_grant_type', 'warning', '刷新模式', '0', 5, 'youngAdmin', '2022-11-10 17:32:08', 'youngAdmin', '2022-11-10 18:13:52', '刷新模式');
INSERT INTO `sys_dict_item` VALUES (57, 16, '移动端用户', '1', 'user_type', 'primary', '移动端用户', '0', 1, 'youngAdmin', '2022-11-12 22:10:53', 'youngAdmin', '2022-11-12 22:10:53', '移动端用户');
INSERT INTO `sys_dict_item` VALUES (58, 16, '后台用户', '2', 'user_type', 'success', '后台用户', '0', 2, 'youngAdmin', '2022-11-12 22:11:17', 'youngAdmin', '2022-11-12 22:11:17', '后台用户');
INSERT INTO `sys_dict_item` VALUES (59, 16, '客户端模式', '3', 'user_type', 'warning', '客户端模式', '0', 3, 'youngAdmin', '2022-11-12 22:11:44', 'youngAdmin', '2022-11-12 22:11:44', '客户端模式');
INSERT INTO `sys_dict_item` VALUES (60, 17, '1', '1', 'q', 'default', '1', '1', 1, 'youngAdmin', '2022-12-27 00:18:19', 'youngAdmin', '2023-01-16 23:45:30', '');
INSERT INTO `sys_dict_item` VALUES (61, 17, '2', '2', 'q', 'default', '2', '1', 2, 'youngAdmin', '2022-12-27 00:18:35', 'youngAdmin', '2023-01-16 23:45:07', '');
INSERT INTO `sys_dict_item` VALUES (62, 17, '安好是', '3', 'q', 'success', '3', '1', 3, 'youngAdmin', '2022-12-27 00:20:32', 'youngAdmin', '2023-01-16 23:45:25', '');
INSERT INTO `sys_dict_item` VALUES (63, 18, '掉线', '0', 'iot_online_status', 'warning', '掉线', '0', 0, 'youngAdmin', '2023-01-16 23:17:22', 'youngAdmin', '2023-01-16 23:17:22', '');
INSERT INTO `sys_dict_item` VALUES (64, 18, '在线', '1', 'iot_online_status', 'success', '在线', '0', 1, 'youngAdmin', '2023-01-16 23:17:32', 'youngAdmin', '2023-01-16 23:17:32', '');
INSERT INTO `sys_dict_item` VALUES (65, 17, '哈哈哈', '1', 'q', 'default', '1231', '1', 1, 'youngAdmin', '2023-01-16 23:45:45', 'youngAdmin', '2023-01-16 23:45:45', '');
INSERT INTO `sys_dict_item` VALUES (66, 19, '未接入', '0', 'device_access_status', 'warning', '未接入', '0', 0, 'youngAdmin', '2023-02-04 21:55:28', 'youngAdmin', '2023-02-04 21:55:28', '');
INSERT INTO `sys_dict_item` VALUES (67, 19, '已接入', '1', 'device_access_status', 'success', '已接入', '0', 1, 'youngAdmin', '2023-02-04 21:55:57', 'youngAdmin', '2023-02-04 21:55:57', '');
INSERT INTO `sys_dict_item` VALUES (68, 20, '系统消息', '0', 'system_notify_template_type', 'success', '系统消息', '0', 0, 'youngAdmin', '2023-02-11 16:16:46', 'youngAdmin', '2023-02-11 16:16:46', '');
INSERT INTO `sys_dict_item` VALUES (69, 20, '通知公告', '1', 'system_notify_template_type', 'info', '通知公告', '0', 1, 'youngAdmin', '2023-02-11 16:17:00', 'youngAdmin', '2023-02-11 16:17:00', '');
INSERT INTO `sys_dict_item` VALUES (70, 21, '未读', '0', 'system_notify_read_status', 'warning', '未读', '0', 0, 'youngAdmin', '2023-02-11 23:04:25', 'youngAdmin', '2023-02-11 23:05:11', '');
INSERT INTO `sys_dict_item` VALUES (71, 21, '已读', '1', 'system_notify_read_status', 'success', '已读', '0', 1, 'youngAdmin', '2023-02-11 23:06:46', 'youngAdmin', '2023-02-11 23:06:46', '');
INSERT INTO `sys_dict_item` VALUES (72, 22, '否', '0', 'boole_status', 'danger', '否', '0', 0, 'youngAdmin', '2023-02-19 22:45:02', 'youngAdmin', '2023-02-19 22:45:28', '');
INSERT INTO `sys_dict_item` VALUES (73, 22, '是·', '1', 'boole_status', 'success', '是', '0', 1, 'youngAdmin', '2023-02-19 22:45:18', 'youngAdmin', '2023-02-19 22:45:18', '');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典描述',
  `system_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类型:0-系统内置，1-业务字典',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, 'dict_type', '字典类型', '0', '0', 'admin', '2022-09-04 11:40:11', 'youngAdmin', '2022-09-04 13:31:02', '系统设置');
INSERT INTO `sys_dict_type` VALUES (2, 'bpm_oa_leave_type', 'OA 请假类型', '0', '0', 'youngAdmin', '2022-09-04 14:09:27', 'youngAdmin', '2022-09-04 14:09:27', 'OA 请假类型');
INSERT INTO `sys_dict_type` VALUES (3, 'status_type', '系统状态', '1', '0', 'youngAdmin', '2022-09-04 15:59:40', 'youngAdmin', '2022-09-04 15:59:40', '状态');
INSERT INTO `sys_dict_type` VALUES (4, 'param_type', '参数配置', '1', '0', 'youngAdmin', '2022-09-04 16:54:25', 'youngAdmin', '2022-09-04 16:54:25', '检索、原文、报表、安全、文档、消息、其他');
INSERT INTO `sys_dict_type` VALUES (5, 'label_color_type', '字典标签颜色', '1', '0', 'youngAdmin', '2022-09-21 22:30:58', 'youngAdmin', '2022-09-21 22:30:58', NULL);
INSERT INTO `sys_dict_type` VALUES (6, 'common_status', '系统状态', '1', '0', 'youngAdmin', '2022-09-22 00:45:57', 'youngAdmin', '2022-09-22 00:45:57', '系统状态');
INSERT INTO `sys_dict_type` VALUES (7, 'bpm_model_form_type', '流程的表单类型', '0', '0', 'youngAdmin', '2022-09-28 22:14:08', 'youngAdmin', '2022-09-28 22:14:08', '');
INSERT INTO `sys_dict_type` VALUES (8, 'bpm_model_category', '流程分类', '0', '0', 'youngAdmin', '2022-09-28 22:35:20', 'youngAdmin', '2022-09-28 22:35:20', '');
INSERT INTO `sys_dict_type` VALUES (9, 'bpm_task_assign_rule_type', '任务分配规则的类型', '0', '0', 'youngAdmin', '2022-10-04 18:40:31', 'youngAdmin', '2022-10-04 18:40:31', '任务分配规则的类型');
INSERT INTO `sys_dict_type` VALUES (10, 'bpm_task_assign_script', '任务分配自定义脚本', '0', '0', 'youngAdmin', '2022-10-04 22:42:39', 'youngAdmin', '2022-10-04 22:42:39', '');
INSERT INTO `sys_dict_type` VALUES (11, 'bpm_process_instance_status', '流程实例的状态', '0', '0', 'youngAdmin', '2022-10-16 18:17:05', 'youngAdmin', '2022-10-16 18:17:05', '');
INSERT INTO `sys_dict_type` VALUES (12, 'bpm_process_instance_result', '流程实例的结果', '0', '0', 'youngAdmin', '2022-10-16 18:21:30', 'youngAdmin', '2022-10-16 18:21:30', '');
INSERT INTO `sys_dict_type` VALUES (13, 'window_status', '窗口状态', '0', '0', 'youngAdmin', '2022-10-29 20:40:19', 'youngAdmin', '2022-10-29 20:40:19', '窗口状态');
INSERT INTO `sys_dict_type` VALUES (14, 'ticket_status', '票号状态', '0', '0', 'youngAdmin', '2022-10-30 10:38:25', 'youngAdmin', '2022-10-30 10:38:25', '票号状态');
INSERT INTO `sys_dict_type` VALUES (15, 'system_oauth2_grant_type', 'OAuth 2.0 授权', '0', '0', 'youngAdmin', '2022-11-10 17:30:09', 'youngAdmin', '2022-11-10 18:12:35', 'OAuth 2.0 授权类型');
INSERT INTO `sys_dict_type` VALUES (16, 'user_type', '用户类型', '0', '0', 'youngAdmin', '2022-11-12 22:10:22', 'youngAdmin', '2022-11-12 22:10:22', '用户类型');
INSERT INTO `sys_dict_type` VALUES (17, 'q', 'q', '0', '1', 'youngAdmin', '2022-12-27 00:16:16', 'youngAdmin', '2023-01-16 23:52:08', 'q');
INSERT INTO `sys_dict_type` VALUES (18, 'iot_online_status', 'IoT设备在线状态：0-不在线，1-在线', '0', '0', 'youngAdmin', '2023-01-16 23:16:19', 'youngAdmin', '2023-01-16 23:49:58', '');
INSERT INTO `sys_dict_type` VALUES (19, 'device_access_status', '设备接入状态：0-未接入，1-已接入', '0', '0', 'youngAdmin', '2023-02-04 21:54:48', 'youngAdmin', '2023-02-04 21:54:48', '');
INSERT INTO `sys_dict_type` VALUES (20, 'system_notify_template_type', '站内信模版的类型', '0', '0', 'youngAdmin', '2023-02-11 16:16:12', 'youngAdmin', '2023-02-11 16:16:12', '');
INSERT INTO `sys_dict_type` VALUES (21, 'system_notify_read_status', '站内信阅读状态', '0', '0', 'youngAdmin', '2023-02-11 23:03:40', 'youngAdmin', '2023-02-11 23:03:40', '');
INSERT INTO `sys_dict_type` VALUES (22, 'boole_status', '是--否', '0', '0', 'youngAdmin', '2023-02-19 22:43:54', 'youngAdmin', '2023-02-19 22:43:54', '');

-- ----------------------------
-- Table structure for sys_es_pre_service
-- ----------------------------
DROP TABLE IF EXISTS `sys_es_pre_service`;
CREATE TABLE `sys_es_pre_service`  (
  `unid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `catalog_unid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属目录清单UNID',
  `service_name` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '事项名称',
  `service_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '事项编码',
  `key_words` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关键词',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0',
  `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`unid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '事项基本信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_es_pre_service
-- ----------------------------
INSERT INTO `sys_es_pre_service` VALUES ('22B4D7F654D9FB346BA92EDEC0277103', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更生产地址（异地改建、扩建车间或者生产线的）', 'MB1912669XK5572000a', '药品生产许可证变更生产地址（异地改建、扩建车间或者生产线的）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('22BEA4647EFA712B1244E8A250BCE1F5', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）核减仓库', 'MB1912669XK5572100a', '药品经营许可证（批发、零售连锁企业总部）核减仓库', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('22C365F4CC50C4CEF197D2931B3BEF59', 'FB5D2BFE593C144DAEED487C13AAB79F', '食品生产许可注销（省级）', 'MB1912677XK06655004', '食品生产许可注销（省级）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('23C3384DD3B8BD35AF4DB3CC0CBFA448', '9D9DAA743A0BCBA7ADA27FE5E3A7B8AD', '地质灾害危险性评估单位乙级资质认定（延续）', '005184224XK10466009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2424159BFCFCF3963704285D322E1D8A', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更企业名称', 'MB1912669XK55720002', '药品生产许可证变更企业名称', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2608A5B127565D1883970A644A2101E1', 'D8D76AD01BC3D8789EF498BCE97A2E77', '矿藏勘查、开采以及其他各类工程建设占用林地延续许可', '005184558XKXZOIM003', '矿藏勘查、开采以及其他各类工程建设占用林地延续许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('269C68E3C95C27CFB4D12D56D551CB88', 'B043C5F466781BBE630CE9808479D221', '药品、医疗器械互联网信息服务审批变更法定代表人', 'MB1912669XK00233008', '药品、医疗器械互联网信息服务审批变更法定代表人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('26FB8C3D42542422D3FEC843DEF9C803', 'AA22DD7AC5944A79EC9329846D8915F4', '高等学校章程制定核准', '005184662XK55914001', '高等学校章程制定核准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2708E2DC95C8A496FDA1824A242B187B', 'B53FAC1C19C26A138518BAFAE647749E', '采矿权扩大矿区范围变更登记', '005184224XK15283001', '采矿权扩大矿区范围变更登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2765A1F626909D878E9E8183EB313549', '9BF3A1243DFFC7AF6F7F3F40F718E374', '企业离退休人员疑似死亡暂停养老金发放', '698732712GG06323010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2767CB37E3AB47C005D6703E21903926', '1B4A8E8E6B977C71480C43CD9892ED72', '二级注册建筑师执业资格延续申请', '005184400QT59418004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('28138CF236CD505826A60936B376F487', '97F33ECB9678A0580191D152C7AE919A', '纳编的原社会招聘部队文职人员参保登记（机关事业单位养老保险）', '698732712QZ00659012', '纳编的原社会招聘部队文职人员参保登记（机关事业单位养老保险）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('284C27ECA6A7B8D6091E921CEB4EF658', '5C9C99BF8FBB6290DD02C7B6AB7F7569', '第二、三类医疗器械生产许可证变更企业负责人', 'MB1912669XK0023500a', '第二、三类医疗器械生产许可证变更企业负责人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('291498398A9E52B1827C416ADD8D9BF0', '2A1129964340B6A77C4A2C16C11FCCEF', '稀土矿山开发项目核准', 'MB0P03925XK55679002', '稀土矿山开发项目核准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('298EAFE471E87B3A475D3BE985B193BD', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证延续', 'MB1912669XK55718002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2A7200828A217B74082944464292FFF2', '87C4208BED1361682065E50EF91BFBA4', '医疗机构制剂再注册审批', 'MB1912669XK55835002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2B2DF2CA4B1A65BB4E11A63C8F3E4F5A', '31AE25ABF5F312A8CB2954C1EE72B8D2', '机关事业单位在职人员死亡', '698732712GG07754006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2BF2CDEC5EF5CE078630BC82A7050867', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证注销', 'MB1912669XK55718003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2CDD36B696ED46E726E21D94944A0380', '770D26852BFB1FB3995834B1FAAE65B4', '中外及与港澳台合作办学机构变更审批', '005184662XK55911002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2D7DF57A1230870FC4310B140983353C', 'B043C5F466781BBE630CE9808479D221', '药品互联网信息服务审批换发', 'MB1912669XK00233003', '药品互联网信息服务审批换发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2DB2A603F9B967DEA6C4D64B32FA717E', 'FB3B33DC52DEA876F80F74FBA0C6C03C', '高致病性病原微生物菌（毒）种或样本运输审批', 'MB1957883XK00276001', '高致病性病原微生物菌（毒）种或样本运输审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2DCBFD3EE84FC09A2A2C5457522C35CE', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类体外诊断试剂产品包装规格，适用机型，检测条件、阳性判断值或者参考区间，产品储存条件和/或有效期，修改产品技术要求、但不降低产品有效性的变更', 'MB1912669XK00227016', '第二类体外诊断试剂产品包装规格，适用机型，检测条件、阳性判断值或者参考区间，产品储存条件和/或有效期，修改产品技术要求、但不降低产品有效性的变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2E1067C3B4D8D047EC786D7704314F5A', 'FD58613B8861972EEE57F2D6119019DC', '出口国家重点保护的或进出口国际公约禁止或者限制贸易的陆生野生动物或其制品新办审批', '005184558XK37289001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2E2661F225D0ADABDFA38AF5FF05BCAB', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证注销', 'MB1912669XK55720006', '药品生产许可证注销', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2E85C8A39FAC27476A1A1941CDE8529E', 'B043C5F466781BBE630CE9808479D221', '医疗器械互联网信息服务审批换发', 'MB1912669XK0023300e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2FA35775C67CD56E6BD888DA78970A04', 'AA0E47373A403A595664A74FB69D11EC', '数据资源目录查询', '000000CSJXK8383500f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2FA4BFDEBE22C7B8236E06351803519D', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更住所', 'MB1912669XK55718010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('2FF7777CC4D85A74C363D237419E5DFD', '923061E371580C114E501BB08FF4A409', '出售、收购国家二级保护野生植物审批', '005184558XK60867001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3024B62080D41D2A7BE8D42767301D71', '41FC47AC50A224E64BFC88ACAFD335E4', '乙、丙级城乡规划编制单位资质新设立', '005184224XK12543001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('30B9B449C84321F046EBDDED19D223CC', '51650E09E814DBEA959D3BEEC9F115C4', '药品经营许可证（批发）核发', '000000CSJXK59386001', '药品经营许可证（批发）核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('310794E20B543CBE7E8B75C946C496D1', '5C9C99BF8FBB6290DD02C7B6AB7F7569', '第二、三类医疗器械生产许可证变更企业名称', 'MB1912669XK00235002', '第二、三类医疗器械生产许可证变更企业名称', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3185BB0C3F7C14C597038EC20CFB9552', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更企业名称', 'MB1912669XK55718004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('323F23218C2A611C92D89019CAF5B0C7', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类体外诊断试剂产品注册', 'MB1912669XK00227002', '第二类体外诊断试剂产品注册', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('338224346F3CC09F7C7574175C0F32A2', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更生产地址（核减生产地址）', 'MB1912669XK55720017', '药品生产许可证变更生产地址（核减生产地址）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3389BA5C8AF16BF3A9CE06D4202E8346', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（零售连锁企业总部）换发', 'MB1912669XK55721005', '药品经营许可证（零售连锁企业总部）换发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('33A9C18C02B972DDA044B8CF191477FE', 'FFD49B2D50431CF59CC8744EB0C3D3CB', '失踪人员找到恢复养老金发放', '698732712GG66003004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('33F07280023E1F886610E8E1A0ADF2A5', '706CC1B4AFA88BE602FCCE93A262730B', '自学考试合格成绩证明', '005184662GG07546001', '自学考试合格成绩证明', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('344D4436D858B9FA20D5826755D1591A', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构中药制剂委托配制备案', 'MB1912669XK55646006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('349AFE2D2E92B1DB35866EBE97366D81', 'CFF6A995E991C480BA7EC47582BC2351', '国家基本水文测站上下游建设影响水文监测工程的审批', '005184566XK54551005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('34D694B231F17575329515F0C0819B68', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）原址仓库布局调整', 'MB1912669XK5572100b', '药品经营许可证（批发、零售连锁企业总部）原址仓库布局调整', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('351D7A13960A7398BEC852C2CEFFEC02', 'A1817746F17C96F14825B64419B10819', '医疗用毒性药品的收购、批发企业批准', 'MB1912669XK00238001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3544866F2F991E913F728DDD990B19D7', '8F852B61DCDD632F4220E4D3934F3144', '外国人对国家重点保护陆生野生动物进行野外考察或在野外拍摄电影、录像审批', '005184558XK97418001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3565C53E86A6414AF7AD5A8614798702', 'FFD49B2D50431CF59CC8744EB0C3D3CB', '查实举报错误恢复养老金发放', '698732712GG66003003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('35BAFFEB7AE3DA0785E7BD362E08DFD6', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证补证', 'MB1912669XK55646003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('35DB3769D70826F78AB866002E52866F', 'A5A5996D532AB90EE5C26D8CEA855E15', '国家二级保护陆生野生动物特许猎捕证核发', '005184558XK98058001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('35DBC85DFC77F1D6D8DE7116BDF2E5FE', '87C4208BED1361682065E50EF91BFBA4', '变更医疗机构制剂有效期审批', 'MB1912669XK55835003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3640ABE72EDA2CB53F6C4530882DAFE1', 'F0CB7655FBE18CBD01D3308EA11E8AA0', '食品添加剂生产许可新办（省级）', 'MB1912677XK27763001', '食品添加剂生产许可新办（省级）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('36A63807D0800F4F4FEC3A133AD2CC4C', '69E15D25B34F4C05596C2D9FDF9F85E6', '非营利组织免税资格认定', '005184603QR12988001', '非营利组织免税资格认定', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('36DF58BDA5B0263CA84AF58400A82121', '41FC47AC50A224E64BFC88ACAFD335E4', '城乡规划编制单位资质认定（升级）', '005184224XK1254300b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3774119B56EBBFED3AFAB5F14150BD12', '41FA3B6D2DF03AACB95F7C870A41DF09', '国产药品再注册审批', 'MB1912669XK00237001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('37E267BE327949DBC198F10346930227', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类医疗器械产品注册人名称变更', 'MB1912669XK00227003', '第二类医疗器械产品注册人名称变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('384DA89D16586C5F4570CCCB94F2362D', '336C716771BBE39EAB5E27CF37A5ADE5', '药品经营许可证（批发、零售连锁企业总部）补发', 'MB1912677XK39447001', '药品经营许可证（批发、零售连锁企业总部）补发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('390E04E8B8C96205D215FB75E5A68EC0', 'FED7D4BD65BC1CCF3535BFB40893B4BC', '中外及与港澳台合作办学项目变更审批', '005184662XK55912003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3ABCEC869581C000EF494306EA2A0A7E', 'F0CB7655FBE18CBD01D3308EA11E8AA0', '食品添加剂生产许可变更（省级）', 'MB1912677XK27763002', '食品添加剂生产许可变更（省级）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3B69AADBB1AB9C469C0B15C77C0A4DD7', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）注册地址、仓库地址文字性变更', 'MB1912669XK55721011', '药品经营许可证（批发、零售连锁企业总部）注册地址、仓库地址文字性变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3B7764AB8FA8637AF2373304523E5082', '00A3A498AF128075BE4FE07DAF67B123', '食品（含保健食品）生产许可延续', 'MB1912669XK00216004', '食品（含保健食品）生产许可延续', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3B8F8ABB033D9287010F934778076557', '41FC47AC50A224E64BFC88ACAFD335E4', '城乡规划编制单位乙级资质认定（遗失补办）', '005184224XK12543009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3C88083246F061CCC4D8D3D8ABC3D6E5', '470B457EC5BEA5F697B0C951BB37E7D5', '机关事业单位在职人员缴费工资基数变更', '698732712GG33228006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3CD6246067BF76C9BF24983644711BCF', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类医疗器械产品延续注册', 'MB1912669XK00227004', '第二类医疗器械产品延续注册', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3D93528901D7A9C6BD9DF68287FC2CEC', '5330E67B0D62F249896CDE1AA7F13D06', '小额贷款公司变更公司法人、董事长、执行董事、总经理（审核）', '687145571QT00369008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3DA22602F89B28B85E681E3A0FF75925', '5C3E645A610D16DE563E3B9C8CD77BB6', '二级注册结构工程师变更(外省转入)', '005184400XK0929700a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3DEA3904A4685BE8D3C90840A293F363', '9BF3A1243DFFC7AF6F7F3F40F718E374', '企业离退休人员超期未参加资格认证暂停养老金发放', '698732712GG06323017', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3E52EF26BD8E813437382D70A6E401A3', '98FCACB02F569C838973E76B3C1FEFA3', '国家一级保护野生动物特许猎捕证核发初审', '005184558XK99812001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3EB50FE0D534151ECA395152534ADA45', '21C0831D337B178996B102BF362D2F50', '机关事业单位基本养老保险转入城镇企业职工基本养老保险', '698732712GG59360004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3F5094B3E2381E62A7E965A8297EB571', 'DC4F08623B0C88C554283E65A9F58F3E', '放射性药品生产企业许可证核发', 'MB1912669XK00223001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3F72F4F5278D525B2EC1A79937A91143', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类体外诊断试剂产品对产品说明书和/或产品技术要求中文字的修改，但不涉及技术内容的变更', 'MB1912669XK0022700a', '第二类体外诊断试剂产品对产品说明书和/或产品技术要求中文字的修改，但不涉及技术内容的变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('3FDE399624C4B868744BF024DE4BBD3A', '29BECF1A9D9990D6679C75458E8CEBE8', '河南省特岗教师招聘网上报名', '005184662GG33219002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('41EC0E150D7BFD8A4F5EE25B3B862C18', '2D1055872C9544CF086F84D38DEBB357', '三类超限运输车辆跨省行驶普通干线公路许可', '005184435XK0032200f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('427D26498FFD597C4EC5BA5292FDB002', '00A3A498AF128075BE4FE07DAF67B123', '药品委托生产批准', 'MB1912669XK55722001', '药品委托生产批准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4396F43984A303BE25FE4CFB3CD91082', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更生产地址（异地新建或搬迁）', 'MB1912669XK55720003', '药品生产许可证变更生产地址（异地新建或搬迁）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('439EE9C7FBA382F490C01BB1E69C8709', 'E7B234C807C51669078A1C3BB3463566', '权限内利用国家重点保护陆生野生动物通过马戏团活体展演审批', '005184558XK68302004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4428F19EAEBA62F2B48C2F75083112F6', '5330E67B0D62F249896CDE1AA7F13D06', '小额贷款公司设立（审核）', '687145571QT00369012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('446B82B5AC82161A02F16F18106AF098', '41FC47AC50A224E64BFC88ACAFD335E4', '城乡规划编制单位乙级资质认定（变更）', '005184224XK1254300a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('454677552ED0105D981A8315AA8AAF14', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更生产地址（原址或同一生产地址内新建、改建、扩建车间或者生产线的）', 'MB1912669XK55720009', '药品生产许可证变更生产地址（原址或同一生产地址内新建、改建、扩建车间或者生产线的）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('462E132C509AC7178D65AE93D1875609', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证核发', 'MB1912669XK55718001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('47635B60BBF5C398CB4B992C55AEDD42', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证核发', 'MB1912669XK55646001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('47CCFF7D737FD1F4AA2E3B2AC64634F9', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发）换发', 'MB1912669XK55721016', '药品经营许可证（批发）换发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('47D28D2DAB5BAD49356886EC47140057', 'BC60ECBA4AE58BE7AB7CC8C611863392', '融资担保机构变更名称备案（审核）', 'MB1530417XK00368006', '融资担保机构变更名称备案（审核）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('47F5A4926CA0896B26B5301B471BE50F', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产企业变更生产设施设备（可能影响产品质量安全的）', 'MB1912669XK55718011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4908DEA0097C2159F2A00B1DC1555DBF', 'B548B58EF47E5787E92C82F0257B4FBD', '第二类精神药品零售连锁企业总部业务审批', 'MB1912669XK55781006', '第二类精神药品零售连锁企业总部业务审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('49465E4559C4AEB406378C999264815B', '68FA887F1DF49091B7E78347FE07A05A', '医疗机构静脉用药集中调配中心（室）执业审核（中医、中西医结合医院）', 'MB1957883QT00269002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4950CC8FA71D77AC599743B46919700F', '00A3A498AF128075BE4FE07DAF67B123', '药品生产质量管理规范认证证书核发', 'MB1912669XK55669001', '药品生产质量管理规范认证证书核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('495496139A43D0B390E8FD8C936C0167', '187D237D8FD90CF36D361DD7B9CC1AA8', '二级建造师执业资格初始注册', '005184400XK83761003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('49C2FC19B8F512B6E1C742FF80F7A6ED', '4DED12F98AA4B3210E96CDDD133A6258', '河南省普通高校招生章程核定', '005184662XK55914002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4B5B6D300E2C1A2A7614A00C563D1E5F', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类体外诊断试剂产品增加临床适应症的变更', 'MB1912669XK00227012', '第二类体外诊断试剂产品增加临床适应症的变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4B7A60169E9AFF6E2A2DC812376CD67A', '187D237D8FD90CF36D361DD7B9CC1AA8', '二级建造师执业资格变更注册（聘用企业变更）', '005184400XK83761004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4BA0D6D00DD6B7E850898AFEE3D8AD5A', '4C89A468ADB43DB226D2102E5D18D8CE', '临时占用草原', '005184558XK84021001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4BDA5E1C263766935F168ABACD832CDE', '1B4A8E8E6B977C71480C43CD9892ED72', '二级注册建筑师变更(跨省转出)', '005184400QT59418008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4D2D8307CB2A6E725E8BD35EA92FAB2A', '42EFB01138010A18BD2DC8EBCE0CEBE5', '乙级测绘资质新增专业范围', '005184224XK00329005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4E33CCA2EE96275D92A3BAFE60DA8954', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发）核发', 'MB1912669XK55721001', '药品经营许可证（批发）核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4E830BCAD3A22AFAA4DDCB0CA3324951', '6904ADC22C5F55B8D5494DDC9E1F14F3', '因矿藏开采使用草原的审核', '005184558XK12024007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4E9758037099DBB074A50BABFDC7017E', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更统一社会信用代码', 'MB1912669XK5571800d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4EB6B37DABBCD07FC8B30011C847D1E8', 'F0CB7655FBE18CBD01D3308EA11E8AA0', '食品添加剂生产许可延续（省级）', 'MB1912677XK27763003', '食品添加剂生产许可延续（省级）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4F3F981B638A0AFAB841D49A5D39DDDE', '6904ADC22C5F55B8D5494DDC9E1F14F3', '因矿藏开采征收草原的审核', '005184558XK12024001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4F89A0A43F8B52ACA8E825A3C2B3A6BD', '16473BBB7F39ACB9897785731084E6A0', '对外提供属于国家秘密的测绘成果审批', '005184224XK44242001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('4FB0D0EC6ABEE51C924FD609B6E7A44F', 'DB4D63E4E122530930CA2E85CD052CDE', '对高校教师资格证书的遗失补办', '005184662GG07532001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5041C24BD200BEC9A31109BB59718105', '00A3A498AF128075BE4FE07DAF67B123', '食品（含保健食品）生产许可核发', 'MB1912669XK00216001', '食品（含保健食品）生产许可核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('50A19296C6B487F13FFBEFE53C8E913B', '3BDAB30133A9D418486952DA02E918BA', '企业人员中断社会保险关系', '698732712GG33231006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('50CE2BC6EA2C16FE27EC83A742DBBACC', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发）核发', 'MB1912669XK55721002', '药品经营许可证（批发）核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('50CFB9337D8C3AC17ACD1FD47AA1EE80', '26E393F10294ADA5F113B833A8857126', '放射卫生技术服务机构资质认定', 'MB1957883XK56022001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5125EC348A2ABA1B9422569F203D9A67', '97F33ECB9678A0580191D152C7AE919A', '机关事业单位之间跨省调入人员参保登记（机关事业单位养老保险）', '698732712QZ00659008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('516DCEEECA0857C61A6AADEBDCB985E3', '3B13DB4B65D46DF8209C5BE8AB6EC15D', '煤矿安全生产标准化考核', 'MB0P03925QT00158002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('53722A22D7DF70788554F20B3E1D99FD', '1B4A8E8E6B977C71480C43CD9892ED72', '二级注册建筑师执业资格证书补办申请', '005184400QT59418003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('540913B472E0FA55066F14663C371ADA', 'A670566973A0A9572FB1E0071F5FCFCF', '建设项目用地预审与规划选址（贫困县涉永久基本农田）核发', '005184224XK00166005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5436BC28EEF2D88B7C3B8EBE9F5432F7', '187D237D8FD90CF36D361DD7B9CC1AA8', '二级建造师执业资格注册证书补办', '005184400XK83761005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('54B2501D662C0AB9AFB5FECAC4841D69', '1091C25D68618ACC9CA66C9ECAC83390', '省外城镇企业职工基本养老保险转入省内城镇企业职工基本养老保险', '698732712GG08079002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('54E9C917723A7F809ACEC2491C804CA0', '0EA47F1AAA87CF8048B90CFB85AEEF2C', '企业离退休人员因工非因病死亡丧葬补助金、抚恤金申领（工伤未领取待遇）', '698732712GG08062005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('556EC1C6BBCB737B702D90571F2EE9F2', 'B9AB43F0556A700B5CA24A12E4CCDCA2', '安全生产考核合格证书变更（跨省转出B类人员）', '005184400QR0235500b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('55B3BE787A9502CD083ADB05E115E08B', 'EE2CF154F1A25E9F0BBCD0ED07AE64BF', '高校人文社科研究成果奖申报', '05184662MGG06253001', '高校人文社科研究成果奖申报', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('55F0EA434C5F9D4F14B140A7768A38E1', '97F33ECB9678A0580191D152C7AE919A', '招录,录用,企业调入,退伍兵安置等首次在机关事业单位参加工作的人员参保登记（机关事业单位养老保险）', '698732712QZ0065900c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('57755976E6B3A97533AD451FEE684E0C', 'AF5D164E85D6A3E113631201707F17F7', '医疗器械出口销售证明', 'MB1912669QT00226001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('57CB9F5BCE91F0756B79E9BFB04B273D', '8C05D70DC099A99852FD587E1077F42C', '企业人员退休申请（已有社会化发放信息-正常退休）', '698732712GG56377007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('57DA82BD1786088CB0A4EE75DC33EBBB', '00A3A498AF128075BE4FE07DAF67B123', '食品添加剂生产许可补办', 'MB1912669XK55817005', '食品添加剂生产许可补办', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('57E742DE4857CA0C2989AB87FEDF5AA0', 'A9A5D3835850BAF683CEA791F5D3A76B', '医疗单位使用放射性药品许可证注销', 'MB1912669XK00225002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5822C8906C371B7FD4C28255E9190B61', 'B043C5F466781BBE630CE9808479D221', '药品、医疗器械互联网信息服务审批变更网站负责人', 'MB1912669XK00233006', '药品、医疗器械互联网信息服务审批变更网站负责人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('58303BC560B68616ADE1B01CBD10DF64', 'F01441879DAB55C3D7EB727D92F3336F', '地质灾害治理工程勘查单位乙级资质认定（延续）', '005184224XK28033001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('585188D252625DFC0CBFD7910A72A0D2', 'B043C5F466781BBE630CE9808479D221', '医疗器械互联网信息服务审批核发', 'MB1912669XK0023300d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('587C1BB21765273D2C05249295990920', '00A3A498AF128075BE4FE07DAF67B123', '食品（含保健食品）生产许可变更许可事项（含工艺设备布局和工艺流程、生产设备设施、食品类别和生产场所）', 'MB1912669XK00216003', '食品（含保健食品）生产许可变更许可事项（含工艺设备布局和工艺流程、生产设备设施、食品类别和生产场所）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('589581B8604C67FD6F172B7423E687D8', '87C4208BED1361682065E50EF91BFBA4', '医疗机构制剂调剂使用审批', 'MB1912669XK55835004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('58AD6285B7BD44AB49C3A02A363BC9EB', 'FB5D2BFE593C144DAEED487C13AAB79F', '食品生产许可新办（省级）', 'MB1912677XK06655001', '食品生产许可新办（省级）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5905DE9E0067786C9E428EE2A5FC5167', 'F4884B206D1F671D08B7DD76294621F0', '采集国家一级保护野生植物延续审批', '005184558XK4G3EF002', '采集国家一级保护野生植物延续审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('59598AAB4168638E5948E1CECEC8A879', '2160A63ECAF0C46518EFFCB350CEB714', '森林经营单位修筑直接为林业生产服务的工程设施占用林地审批', '005184558XK97845001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5A7249FDA26906BD915A015298D79819', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更生产地址（核减生产车间或者生产线的）', 'MB1912669XK55720012', '药品生产许可证变更生产地址（核减生产车间或者生产线的）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5AC359C27DBA507BDE82E0646E7CDE31', '13C2E1A26150CFAA6A4D60042BC095CD', '成人高招录取审批表', '005184662GG07043001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5B25479524446B5626641144A400A650', '31AE25ABF5F312A8CB2954C1EE72B8D2', '机关事业单位在职人员丧失中国国籍', '698732712GG07754007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5C315AF1C552020F0EC831217EB5B9C1', '31AE25ABF5F312A8CB2954C1EE72B8D2', '企业人员个人帐户一次性待遇申领（退休时缴费不足15年自愿退保）', '698732712GG07754009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5C53861F45F645C7EA9B8C7BC48EFF0F', '00A3A498AF128075BE4FE07DAF67B123', '食品（含保健食品）生产许可变更登记事项（含生产者名称、法定代表人（负责人）、住所名称、生产地址）', 'MB1912669XK00216002', '食品（含保健食品）生产许可变更登记事项（含生产者名称、法定代表人（负责人）、住所名称、生产地址）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5CCAE893FEDBE92BAB502534791A3096', 'F9F6C2B5D1572005269DA71358583CBF', '特种设备检验、检测机构核准（注销）', 'MB1912677XK5566700e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5CDB795EC3ABBF13C9805BE63EC03018', '42EFB01138010A18BD2DC8EBCE0CEBE5', '乙级测绘资质变更', '005184224XK00329006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5CE925D2D0D5CE4D89C2A02DF929AE33', '187D237D8FD90CF36D361DD7B9CC1AA8', '二级建造师执业资格注销注册', '005184400XK83761006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5D13DF48257373BAB64F2089A1690D7C', 'F01441879DAB55C3D7EB727D92F3336F', '地质灾害治理工程勘查单位乙级资质认定（变更）', '005184224XK2803300d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5D39B37C6DF2B172DB994E2839851312', '1D128980467B7C4C7E2FB6806A6DD51D', '自学考试毕业生登记表证明', '005184662GG33220002', '自学考试毕业生登记表证明', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5DF3C46D7450A4EB03186EDCD0A80E2C', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更生产范围（新增）', 'MB1912669XK55720019', '药品生产许可证变更生产范围（新增）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5E576D4B1A58B53E06864711918E3693', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更生产地址或者生产范围（已取得《药品生产许可证》的药品上市许可持有人委托生产制剂的或者持有人自行生产变更为委托生产）', 'MB1912669XK55720020', '药品生产许可证变更生产地址或者生产范围（已取得《药品生产许可证》的药品上市许可持有人委托生产制剂的或者持有人自行生产变更为委托生产）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('5F550A65D7FB224DC14986541FE03295', 'D8D76AD01BC3D8789EF498BCE97A2E77', '矿藏勘查、开采以及其他各类工程建设占用林地变更许可', '005184558XKXZOIM002', '矿藏勘查、开采以及其他各类工程建设占用林地变更许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('61B8B8B63EBFDC10C8E08551B44D1DE5', '237B03A336270FBA9C97AA0231A67536', '仅应用传统工艺配制的中药制剂品种备案', 'MB1912669QT0872N001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('61CC643FB230CE4799819E603F33CB4F', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）变更仓库地址（含增加仓库）', 'MB1912669XK55721017', '药品经营许可证（批发、零售连锁企业总部）变更仓库地址（含增加仓库）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('624031D3F6759ECCCDABEC601585956C', 'A9A5D3835850BAF683CEA791F5D3A76B', '医疗单位使用放射性药品（一、二类） 许可证核发', 'MB1912669XK00225001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6300AEC80EAEB2B261A8678A0C24C55D', '3BDAB30133A9D418486952DA02E918BA', '终止社会保险关系(企业) — 出国定居', '698732712GG33231005', '终止社会保险关系(企业) — 出国定居', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6348C9EAE9F99630E03E5A16C393C194', '00A3A498AF128075BE4FE07DAF67B123', '药品经营质量管理规范认证证书变更（批发）', 'MB1912669XK55836002', '药品经营质量管理规范认证证书变更（批发）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6417CBC5BABBA1BE08C4B97221CDD033', '5C9C99BF8FBB6290DD02C7B6AB7F7569', '第二、三类医疗器械生产许可证变更住所', 'MB1912669XK0023500e', '第二、三类医疗器械生产许可证变更住所', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('64266CAF32B55491FFB2AD35CD6EEFE8', 'AA1D66BF6F25A14E4C97BB3368C6A4A2', '草原防火期内在草原上进行爆破活动审批', '005184558XK38614001', '草原防火期内在草原上进行爆破活动审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('649FD81E4A5751C7E3DBCFB7398252CE', 'B86A49C8706B765773E67437735F447D', '医师资格证书遗失或损毁补办', 'MB1957883GGNKR7G001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('66434BD5C4399E779B38B209EE23F175', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更生产范围（核减）', 'MB1912669XK5572000f', '药品生产许可证变更生产范围（核减）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('66EB06B1FA60E41934EDD51BF69FA7D6', '024C43635CD1AC64B204F3B32E027D77', '变更国产药品直接接触药品的包装材料或者容器', 'MB1912669XK55819003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6779CF37235F87D14D397750C02023A7', 'F9F6C2B5D1572005269DA71358583CBF', '特种设备检验、检测机构核准（单位名称改变或地址更名，且工商营业执照未发生变化）', 'MB1912677XK55667006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6804BF7CFF1C4387CB51B5555CED0896', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更法定代表人', 'MB1912669XK5571800f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('680A35EC065C461B7E89408EC595614D', '2E6784788D59549E3E810E44F689C3E9', '医疗机构变更地址', 'MB1957883XK55857008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6814163C094C6C5D3BE4E5DB58FC2C13', '00A3A498AF128075BE4FE07DAF67B123', '食品（含保健食品）生产许可补办', 'MB1912669XK00216005', '食品（含保健食品）生产许可补办', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6824CC62510CC590BD4860A31CD5E459', 'B548B58EF47E5787E92C82F0257B4FBD', '第二类精神药品批发企业批准', 'MB1912669XK55781001', '第二类精神药品批发企业批准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('69125192C155D4F0BED5C15BFB23CAC9', 'A9C89515506EEF92AC5B922BC3773E57', '企业职工欠费补缴', '698732712GG33232004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('698205C7AA10DDAB9E5D6ACB3E272002', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）增加经营范围（特殊药品除外）', 'MB1912669XK5572100e', '药品经营许可证（批发、零售连锁企业总部）增加经营范围（特殊药品除外）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('69D590B9DEBE070C01C96E31B7977F14', 'E1A7364007B092BCF2CBA19B1234217A', '采集国家二级保护野生植物审批（终审）', '005184558XK94108001', '采集国家二级保护野生植物审批（终审）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6A72BC2EF2CB9AEAECD7CC37605E3063', '00A3A498AF128075BE4FE07DAF67B123', '药品生产企业质量负责人、生产负责人变更备案', 'MB1912669GG33186001', '药品生产企业质量负责人、生产负责人变更备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6B246F0BF7730231F9D75AE24F1C9623', '26E393F10294ADA5F113B833A8857126', '放射卫生技术服务机构资质（变更法定代表人）', 'MB1957883XK00272009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6BC77BEFD6AFB2B681E1605F851F33F3', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类体外诊断试剂产品增加临床测定用样本类型的变更', 'MB1912669XK0022700f', '第二类体外诊断试剂产品增加临床测定用样本类型的变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6BCEFE60AE27B38CAC22F49379274707', '9BF3A1243DFFC7AF6F7F3F40F718E374', '机关事业单位退休人员死亡', '698732712GG06323016', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6C985BCAFBC6074DB4CF0EAA6E85FAD1', 'AD7A092684DF73742274612371BBD060', '出口国家重点保护或进出口国际公约限制进出口的野生植物或其产品新办审批', '005184558XK6HY4N001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6CB3E231071906208C3C126CED875764', '97F33ECB9678A0580191D152C7AE919A', '省内机关事业单位已参保人员恢复缴费（机关事业单位养老保险）', '698732712QZ0065900a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6CDC04AF92C0953845ECDBCD25A2F10B', 'BC60ECBA4AE58BE7AB7CC8C611863392', '融资担保机构合并（审核）', '687145571XK00368007', '融资担保机构合并（审核）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6E5DF0C1B4002F5DCC9266F0555FDBB6', '2A8763136AEC7E2C74B811C406A5F547', '备案制的建设项目占用林地许可', '005184558XK54697004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('6F3381A4F61D70C09446F4FCEF3D54C2', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证换发', 'MB1912669XK55646004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('70226371197495607E695EE66125A926', '8031E4CBFC7C992F5C496EDD51C10F21', '中等职业教育学历认证（2008年秋季及以后毕业）', '005184662GG33213001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('702B227F959EF1DD673EE999E5B5E449', '5C9C99BF8FBB6290DD02C7B6AB7F7569', '第二、三类医疗器械生产许可证生产地址文字性变更', 'MB1912669XK00235008', '第二、三类医疗器械生产许可证生产地址文字性变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('718BA30D74857B9C5406B9C97B12DCC1', 'DC4F08623B0C88C554283E65A9F58F3E', '放射性药品生产、经营企业许可证换发', 'MB1912669XK00223003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('71995D97D3791E3E8D7121C445920437', '9BF3A1243DFFC7AF6F7F3F40F718E374', '企业离退休人员触犯刑律暂停养老金发放', '698732712GG06323012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('71BEE880484DF579FAEEE6DA86C97183', '0338B4A9430E36A2F03B90AAB5662650', '设立商业保理公司审批（审核）', 'MB1530417QT2794R001', '设立商业保理公司审批（审核）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('721FB8D0F5406C3E6B35202655DA42DC', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（危化品）生产许可证发证（涉及产业政策）', 'MB1912677XK5565100d', '重要工业产品（危化品）生产许可证发证（涉及产业政策）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('728305EEF700CC7F8E019F7892B8F18B', '4C3A958D921BE61037ABBFCED737ED32', '一级社会体育指导员审批（破格授予）', '005184726QR13003002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('734FD218F3CABA5C64283EB06EB4AC76', '53F99A0C68D02E83306716A3E0115802', '普通高招录取审批表', '005184662GG07033001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('73F7BA889FF15F6FADABEF2A622C4052', 'AD5E9289B3F01FB9C95E38F17E9C201D', '新设采矿权登记', '005184224XK42500001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7468712E4D32DF9253013266B561A7C2', '42EFB01138010A18BD2DC8EBCE0CEBE5', '乙级测绘资质补充和修改数据', '005184224XK0032900f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('74A6120193905CA49E683258210A55E9', 'FECEAD8D960C1F6E1D7C3F98C7932766', '集体建设用地使用权注销登记', 'ZRZYQQDJJQR48513015', '集体建设用地使用权注销登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('75C4D80BF91DE89E93CE2D3FB27FF320', 'D84DAB0D54832D0BE8DD025697CF66D1', '母婴保健技术服务（产前诊断）机构校验', 'MB1957883QT4010O001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7673FF6A14316A2C48468BD95596285D', '8C05D70DC099A99852FD587E1077F42C', '企业人员退休申请（无社会化发放信息-正常退休）', '698732712GG56377006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('76DB5B661BC49D7CFEE1E74F6703C00A', '31AE25ABF5F312A8CB2954C1EE72B8D2', '养老在职出国定居（企业）', '698732712GG07754003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('77D96B7D3E0611ACBCB453AFB1FD05C6', '49CE791E08DA8CBB573A2FAF54F615B0', '蛋白同化制剂、肽类激素进口准许证核发', 'MB1912669XK00224001', '蛋白同化制剂、肽类激素进口准许证核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('77DB013C4C97F44CA981C25BB8C1AB57', 'D8D76AD01BC3D8789EF498BCE97A2E77', '矿藏勘查、开采以及其他各类工程建设占用林地新办许可', '005184558XKXZOIM001', '矿藏勘查、开采以及其他各类工程建设占用林地新办许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('78973ED90867F457411EB202808D6F97', '96DB4FFEDA05656B962F41AD9B907BC9', '区域性批发企业从定点生产企业购买麻醉药品和第一类精神药品审批', 'MB1912669XK55784001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('78DB96145D35E240D0902C2A8D5099BC', 'C6052ABA4745E61814FFE09D8BDA579B', '采矿许可证补发', '005184224GG56905001', '采矿许可证补发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('78E457C37F35564E03C6580B251C274D', '5C3E645A610D16DE563E3B9C8CD77BB6', '二级注册结构工程师变更(注册证书企业信息变更)', '005184400XK09297008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('78EED00FE5130847D8A581306675B38B', 'FCACF021C8079AF0FE33493D55023217', '认定取得医师资格证书信息修改', 'MB1957883GGATTFO001', '认定取得医师资格证书信息修改', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('791D636C456A56C73D863D3E10198CD2', 'D8EB815D1F16EF155118FA46E10886A9', '建设项目压覆重要矿床（矿产资源）审批', '051842241XK00168002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('792B09F7D0173B486E1D49FB473D8359', 'D84DAB0D54832D0BE8DD025697CF66D1', '母婴保健技术服务机构校验', 'MB1957883QT4010O002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7940313B24106F72B3232B28C4807515', '6F732A88A8B201CC629C2052F0B48039', '公开年度对外招商项目名册', '41000002SGG52847001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('794733F80068EC74E3761BDBAC4ADCF8', '0083435BF03BFF435BACF95CB47D1FCC', '渔业技术咨询、宣传教育、培训服务', '41000002SGG08217001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('795047DF6FE86EEF136F8C99614AF460', 'BE9A6EA7ABFDFF0ABCBBBE9790A2A353', '运输行业从业资格信息查询', '41000001SGG12367006', '运输行业从业资格信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7957C5FFEBEBECD1A04392779BE52EAF', '0DCABC6DB8E5557658F705EBE03ACF4A', '省交通运输厅信息公开', '41000002SGG49011001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('795A022129C7882A77EC413AF6DA2B0E', '2CB826D6539186FAE26D8A0CC8B19F54', '台湾地区的投资者在内地投资设立合资、合作经营的演出经纪机构从事营业性演出经营活动审批', 'MB1848628XK55952001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('796DFC541034AE1C7A6C643A4E5A6862', '754004025F17D26D8ABDBAE237CAA73D', '河南省投资项目法律法规查询', '41000002SGG23547009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('799110A053005BFE499467C1E7F43B1C', '1036342E86AED43074C41991FDE58BDD', '河南省技师学院院校名单', '41000002SGG47039001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('79951EB30AB6CF62AD743573A5BBC1D0', '314F10B68D58B232F5CE48FE9E52CB87', '河南省中小企业公共服务平台人才资讯查询', '41000002SGG96084001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('799A52DAE430724285A89AD8759E9D7D', 'EB1F6634F696494775F46861D4CC439E', '政府定价目录查询', '41000002SGG61171001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('79A345F460CEF5F6D859FB32BC28D5B1', '5C3FCD9104BA9B98F4257B08A89E3A6A', '古生物化石保护工作中做出突出成绩奖励', '005184224JL39676001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('79A4E3F908C209BAA6528526D4E9345B', '05AD416C3B11D5AAA2AEAD2C790AC637', '优抚对象短期疗休养', '41000002SGG63565001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('79AB1D472B6EF3713ADAD11333E9A15D', 'E0620E6B18A10BBB4DF48C2AE13FC017', '水利工程建设项目竣工验收', '005184566QT5215A002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('79B041D54A5B3C52597DE4B2D2EBEB14', 'B2642CE68BB069C952511511EEFAE041', '双学位授予权单位审批', '005184662XK55919003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('79B882B6EA9C8212BE946AE4C43E1B15', '0512484F935A7E427DB5FEB5AB6BD6B1', ' 外商投资企业资质查询', '41000002SGG24961001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('79BCAE0B11C8552FFFE2027E61232864', 'B6BA689040DAF4F0B5707FE21B5324B0', '国家重点公路建设项目竣工验收', '005184435XK55990001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('79CE7946AB3FC17183C9498DB9617A26', '7CDC7391346730BE935DEB62313AC6E5', '普通高校招生计划查询', '41000002SGG04054001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('79D5674733592DE4E79637F9D6727939', '3E4BB1769A8A7373957B69D7DF63AD18', '学位证书认证查询', '41000002SGG47409001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('79FA51F6F146BEB0B2DF1A013DC969BD', 'B1C4DA2A1244FC660CF47EB4F965EA39', '单位参保证明查询打印（企业基本养老保险）', '698732712GG33234001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A04E7A050F0F67A9B71C1E021D342E3', '9C0B7EC43BBE71A2C5A95B2523D30B76', '河南省开标信息', '41000002SGG59519001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A07639BA8CDDBAD8170F85632601672', '3FAE0261754585A6BEA42F6153821A75', '对国家新闻出版署负责的出版新的期刊审批的初审', 'YZ0000000XK55675003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A235C32C3AF51B352B2711810F875BF', 'CAFFF3522503E54A49B1B0D359973B27', '资产评估机构名称变更备案', '005184603XK55636003', '资产评估机构名称变更备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A301B640851408B8A3CE27100CAA5D3', '39AADB766F358006367D280BE0EEFE0C', '公益性群众团体公益性捐赠税前扣除资格认定', '005184603QR00032001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A34F335622C0738568FD424B40DA878', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省社会工作信息查询', '41000002SGG37554010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A4BD8B77D7B6DDF70081D5DBB5C33D4', '0EA47F1AAA87CF8048B90CFB85AEEF2C', '丧葬补助金、抚恤金申领（企业基本养老保险）', '698732712GG08062001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A4C3F4D913FA7E28ADB962856224FD3', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动延续', 'MB1848628XK00096006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A4DBB6E358F42D6440E9B788776F005', 'BFB89262A855E2305396FE7187950AAD', '车用气瓶充装单位许可（增项）', 'MB1912677XK5565300f', '车用气瓶充装单位许可（增项）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A5DC22868CB0DAED95CBF9126FE81AD', 'FE3E194376806EAA75867FC750E311E4', '初次申请甲级测绘资质', '005184224QT0804A007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A6037BBE2680FB44474011CE9FC1414', '0DA863915E3F24F3E957D0BDF3384C91', '流动人员养老医疗保险查询', '41000002SGG76829001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A7831CDC8CC6EAA61DB3E48EDE91CFF', '9C3AF2EA482E584452AD48D58171CD1D', '河南省科技成果技术需求查询', '41000002SGG44042002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A90661172F4ADB0ED4C52FE2EE801F5', '650D59D220E5B6061BF36BBD9C6FCA0F', '产地检疫', '005184531QT68322001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A99FAF7E701E07E927BCE321C1C5E74', 'B043C5F466781BBE630CE9808479D221', '药品、医疗器械互联网信息服务审批变更单位地址及邮编', 'MB1912669XK0023300a', '药品、医疗器械互联网信息服务审批变更单位地址及邮编', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7A9EC83081B0DDD68FEB63D28C753BF7', '56AD1875DE83AAF09229BF733FB0B8DA', '固定采血点（屋）、采血车备案', 'MB1957883QT00276001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7AB51EEC9827ACEE883DDFC57878AFBB', '1DCADF8941110631E23704ADD32560E6', '音像、电子出版物复制单位变更地址审批（经营场所为单位自有产权）', 'YZ0000000XK55723006', '音像、电子出版物复制单位变更地址审批（经营场所为单位自有产权）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7AB8B37F62EF2DBCFF67DB162C65B9E6', 'E52A4C5A08E154D3C75890E85684BC2E', '外国公民、组织和国际组织参观未开放的文物点和考古发掘现场审批', '005184689XK00374002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7ABCCCB8AB0340902E479C44BB302BF2', '56FEEBADEA7EC0A72B6499E2EB2DBA53', '虚开增值税专用发票或者虚开用于骗取出口退税抵扣税款的其他发票查询', '41000002SGG67186006', '虚开增值税专用发票或者虚开用于骗取出口退税抵扣税款的其他发票查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7AC9986225CB68A6051B8FBB373E7934', '00A3A498AF128075BE4FE07DAF67B123', '特殊医学用途配方食品广告审批', 'MB1912669XK55716001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7AE2414669ACA3BA37E29F8A965BEA43', '6B8B188416E63F6484A3D9BBB34C5D93', '煤业事故调查', '41000002SGG53313001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7AEA28A0FA7C811BC0E17062C2D956C8', '72D6AD9BF46372DD78CC8DC0A983DA60', '特殊医学用途配方食品广告审查', 'MB1912677XK39621001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7AF82A58E0C4912419351F54D49BB29A', 'DAB301DDC04E0D58DCD9AA1BC0217944', '省级建设项目环境影响评价文件审批（辐射类报告书）重新报批', '005185040XK4997100e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7AFB49B5FD9E680FD8A9E4C101FB0D8A', '754004025F17D26D8ABDBAE237CAA73D', '河南省发展改革委员会政策解读查询', '41000002SGG23547016', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B15831369C635E08D435FE3B05AC49C', 'B03E53495641EAC7607996F1E59239CB', '律师事务所住所变更备案', '005184291QT00250003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B1D7AB803340041918F872244C46EB3', 'E0AFB9F2A6088F3C5B92BC3B78F2BF24', '司法鉴定人变更登记（变更技术职称）审批', '005184291XK06476018', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B2CE4504669B02A5716F723598B6306', '4C4DD7F949B1C7416C8A9E90EB113E83', '社会团体、民办非企业单位、基金会、律师事务所、会计师事务所等单位社会保险登记', '698732712GG06212013', '社会团体、民办非企业单位、基金会、律师事务所、会计师事务所等单位社会保险登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B307B854884097EE73FE163879F5978', '587E68202E7FAB6B1421F82342D2B64A', '消毒产品卫生安全评价报告备案', 'MB1957883XK18949001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B314D4D9AB22DB5C392E87A9E0664FE', '5DB18E8D8C049A94B7E7B7145CA10B39', '河南省产权交易产（股）权挂牌信息查询', '41000002SGG21755005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B535E28FCBB19AA60E195D4C8EC4A4A', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省村民自治信息查询', '41000002SGG37554007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B53EABEF2F622744B921B2E6BC0D1AF', '7F945BF5B233B55A06904A8DA2774AF7', '河南省安全生产许可证查询', '41000002SGG08258001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B5560C585BECD9CD1949BF1A7C2908C', '93E1D0C3D7054BEAFFD9808C53C4570F', '省老龄办信息公开', '41000002SGG72702001', '省老龄办信息公开', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B570FA13B9DB212063CCF5EC7DD474B', '9A6881845BBDF84832342D4CC4BB33B8', '商业特许经营企业备案', '005184590QT00221001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B671A833C5D067C424DC4E33483AA54', 'DBC3D7CFE77564A60F87FBFC49798AF1', '河南省事管局人才公寓查询', '41000002SGG74847007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B6F593A92C40DD01D6C4CCB4CB6B4B8', 'FD12CDF03E7591BDACE4479C2C7B541D', '河南省国有资产监督委员会在线访谈', '41000002SGG94374002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7B7D4A7CE7ED2B6B3250F5C10083239A', 'B80D5F37B8BA1DB6410B2EB02D40FC2A', '工程勘察企业资质证书地址变更', '005184400XK20097009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7BA5ACAEE7F9960741107E13B495EEEF', '96699B162078EC5F0AC8AB770454DB8D', '事业单位公开招聘工作人员拟聘用人员公示', '41000002SGG46208001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7BB072908C2429DE46692B1EE0DB4828', 'E3B95F4BAC523EAD48FFA645D4B012FB', '煤矿安全生产检测检验资质认可变更授权签字人', 'MB0P03925XK50108004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7BBBC46F1533FBA7D978076D64BA9710', '0CAA5CCD5B2ED113457D61D7FED04571', '一级注册消防工程师注销注册审批', 'MB1D56024XK65409004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7BD38124785E8A2A6CABCF65483487F3', '9A0B72B675692F9ADC75878D879CABEB', '医疗器械网络交易服务第三方平台备案', 'MB1912669QT28532001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7BE77514FEE7B2518D88AB4A2454D6C5', '613594C3C542EB8214D611492DFFE7AB', '人民调解申请资料查询', '41000002SGG05920002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7BEF9C1AFB6D78A63718037F125351C9', '4A69BA6BAFCF104471659E7668FD4C34', '公路工程综合类丙级试验检测机构等级的资格认定', '005184435QR13082004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7BF7DA2C68CDA8FA3F453170F3BB3C58', '787D5630B5EFB1210C29774306904B03', '年度河南省装备制造业十大标志性高端装备名单公示', '41000002SGG91743001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7BFADF1E483BFFFC0F921599068FB932', 'AA003C2BDDD98931D0643005E1EF36FB', '拍卖企业经营文物拍卖的许可（变更法定代表人)', '005184689XK06113003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7C14F973BFDE3F9F9B2DB615BCAECB71', '42E2D78CEB7515287F794F340BCF4363', '税收统计', '41000002SGG46793001', '税收统计', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7C1E2788457D6DC25A01AD0CA51CF69F', '3865CF4C89A1DD2420720829F3FD7820', '兽药生产许可证核发（原址改扩建）', '005184531XK84280006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7C27A302D72E30A83339160CF033ABD9', '1FB9C66E10DC7B91CF17F92BE17571B6', '河南省档案馆档案捐赠查询', '41000002SGG90658007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7CA0C98112EBF1D9312024B378E925E6', '57FA0E0B2ACA402AE501919DC56A2D03', '特种设备检验、检测人员资格认定（检测换证）', 'MB1912677XK00084002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7CA1A5AB923D9BB4EE997E573396BC73', '46EA59D5711A8C6351AE575174FC1A69', '因修建机场建设工程占用、挖掘公路使高速公路改线审批', '005184435XK00320031', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7CEE299564EDEBCA73A5258DADCE54F2', '68F95414A840D142CC70BCFC475CDD74', '机动车状态查询', '41000002SGG60092001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7D413F52345DC679053714ED0DF3BA40', 'A3FF2E23263B29DAB594A0C53D9542BF', '跨越高速公路架设电缆设施许可', '005184435XK55757015', '跨越高速公路架设电缆设施许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7D461AEB586BC63DF5E608C447E642E5', 'E0AFB9F2A6088F3C5B92BC3B78F2BF24', '司法鉴定人注销登记审批', '005184291XK06476014', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7D5914F8F04CE1DF8E31FB4406FBE0EF', 'E772282DE7107405860D744675E3B8B5', '新兴产业专利数据查询', '41000002SGG14532001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7D755A4D1488466A7CDDA1380DBC5928', 'B9203383CCAAFF911E31EA3FA2D20290', '学习贯彻新修订森林法', '41000002SGG58373006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7D76A549977B649CD245F44B427ECE35', '0EBA0F42D82FA5AB32547430DDC162A0', '血站变更法定代表人', 'MB1957883XK00268009', '血站变更法定代表人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7D7CAAA651C4FE2C49C6065B16C3ABD2', '33420A82C739726657B52F72D5B82C05', '除利用新材料、新工艺和新化学物质生产的涉及饮用水卫生安全产品的审批（变更申请单位地址）', 'MB1957883XK00264006', '除利用新材料、新工艺和新化学物质生产的涉及饮用水卫生安全产品的审批（变更申请单位地址）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7D83CAC42D6EF741D44FB9E01FCAF536', '63D75AB786A89FFCC15BF0EF430C97E0', '地质灾害治理工程设计单位乙级资质认定（延续）', '005184224XK16091002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7D87978BB00B8670B20657DE3FD5198E', '9417B489A6ADAE152C71C581FC471996', '河南省公路管理局工作动态查询', '41000002SGG21730011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7D9518F2CFD669C79041CD7195613BD3', '016D6FEF2A88EA957174ED0128EED5F3', '报废机动车回收拆解企业资质首次申请（复审）', '005184590QT00209001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7D9B1409DBC633F7A1E791B6D85B72E9', '00A3A498AF128075BE4FE07DAF67B123', '药品生产企业的关键生产设施等条件与现状变更备案', 'MB1912669QT00228001', '药品生产企业的关键生产设施等条件与现状变更备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7DA68ACFCF32395C0B5AC3AC84CA6536', '0D6582456CC93A329857F4B5BF4C05DB', '河南省档案局政策法规', '41000002SGG26449001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7DAB19AF6EC1D3A8D94B881042D97F5B', '643D32E0B4ED227553BFD18336FCA331', '特岗教师笔试成绩结果查询', '41000002SGG28259002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7DAC281641D2D6D2499B6C10783A7DFE', 'D2F881D9E1D1088AA071D469EE4C3659', '河南省医用耗材采购监管服务', '41000002SGG55666001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7DCFA2AE3045CADFA3F08A5A5EC8736E', '6BB8B174E30C39C6E871097C7A55E308', ' 定制客运试点车辆信息查询', '41000002SGG46553001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7DDB415B721E1AFDAA111517EE6B27C9', 'AB356DD23BF009B75D84975CC3CBF8A6', '河南省建筑市场监管公共服务平台-诚信记录信息查询', '41000002SGG22791001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7DF0197B6C3B7B3079D1BA0066A8C251', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）减少经营范围', 'MB1912669XK55721010', '药品经营许可证（批发、零售连锁企业总部）减少经营范围', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7DF34FB1F9A3211429B85E6B68A2E7A9', 'AB90F27BB70C83C2F92039FC625954EC', '无偿献血返还', '41000002SGG19754001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7DFAC102D2673C4E93964E6D87A31BEC', '613340BD60125024C93CA8750215C093', '机关事业单位基本信息变更', '698732712GG33225006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7DFFFB2F94ECB0A1FBCCE16D594F1A6D', '7342660123B43F44480FA3707071C532', '河南省农业农村厅工作动态查询', '41000002SGG61100002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7E2092203D0EE146BFA6D07C686BBE60', 'E017D310C86BEF78FDDEBE75A336AA92', '军校学员学籍管理查询', '41000002SGG77294004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7E2A748E027488A71EBED251EC4E6857', '1C36DF262228D2E59D2379BC53B19E03', '对婚前医学检查结果有异议的医学技术鉴定', 'MB1957883QR13034001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7E3316D2C53E5E74E169E0C2A90ED794', '9A9A48E1D29AB0E374272F673F7936C3', '体育类民办非企业单位注册资金变更的审查', '005184726GG33171004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7E44E03640631AF0B5BD02AAA7DF891D', 'E8B58A1EDB2B1C188AAB601B8313B24C', '（404）安全生产考核合格（A类）证书查询', '41000002SGG67567001', '（404）安全生产考核合格（A类）证书查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7E4A833D043E793DEFB1DF733590D125', '138782B5DA914F2AFF118C19A50DEFA1', '新建迁移撤销临时气象观测备案', '415805134QT29965001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7E5F9A56DB7B5DE2E64B1B965C159669', 'AB6FE43989F5CC172EDA62215DF4638C', '出版单位配合本版出版物出版电子出版物审核', 'YZ0000000GG33189002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7E60A40402DAAF411CF9B2AD80608F03', '20BB5637246D6D07AC9D1F5174430A45', '小学学校名单', '41000002SGG14957001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7E6822880D1B0E0599A3EC468A3C168F', '76D6F1EBF680F8394259007D9D8DCB47', '兼职律师变更执业机构（省内变更）', '005184291XK11640019', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7E7C328129CB3203D8ABA3957FC533FB', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类医疗器械产品注册人住所变更', 'MB1912669XK0022700e', '第二类医疗器械产品注册人住所变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7E9F9B4B250C9B1199BEF23F97CF0A87', '60B10F8EA983D9FD0EFAE102BE71D0D2', '旅行社注销申请', 'MB1848628XK00198003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7EAA81897A90B3E60DAD4ABD43BEBBE4', 'DFAE48CAC05BEE2AF514BA45272477A8', '机关事业单位缴费信息查询', '41000002SGG75960006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7EB8E435F1355F95387A3A8B72DBEBB1', 'DFDCBA519BC6815539095F325BE39F40', '河南省道路交通事故社会救助基金救助网点查询', '41000002SGG23971002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7EBD143359BEC289D774817D2C640702', '2A1129964340B6A77C4A2C16C11FCCEF', '稀土深加工项目核准', 'MB0P03925XK55679003', '稀土深加工项目核准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7EC71BBFB879EB82E343A63B0B6A9F09', '66166E4E889FF9A7822DB5BC40DFA025', '水利工程质量检测单位乙级资质延续（岩土工程）', '005184566XK97268008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7ECDCC4FBD667B8F8887634A88EB7E8A', 'FB123F22D108F8A2780F34C04BD7D554', '基金会登记政策咨询', '41000002SGG70285001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7ED7EF2698625153086AB1861E538C09', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省区划工作相关信息查询', '41000002SGG37554008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7EF390827D113D58BA040BDB59C0BBA4', '0D1D7F459A6C219B2B8CA1CBF777D530', '组织企业参加进口博览会等展会', '41000002SGG16883002', '组织企业参加进口博览会等展会', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F191732441CE6819A7ACEF8A5BB6B0F', '5C3E645A610D16DE563E3B9C8CD77BB6', '二级注册结构工程师初始申请', '005184400XK09297002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F1E9D5D50ED6444B84AFC48C6F31D0A', '2CB826D6539186FAE26D8A0CC8B19F54', '台湾地区的投资者在内地投资设立合资、合作经营的演出经纪机构从事营业性演出经营活动延续', 'MB1848628XK55952002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F200010441411C836AB5E9B53121AED', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省社区建设信息查询', '41000002SGG37554006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F201F9B27E0A69EB44C630C60FDF4DA', 'AB53211E7DDAF4421CF93160994E67DF', '生产性服务业集聚示范区名单发布', '41000002SGG82849001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F28E1FE209D5B8B255AA4A4B27FFAF9', '78317AD72C1FD34CECEDC7FCC4C53349', '人才之窗单位推荐', '41000002SGG28329002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F2E4B29E9F400DC39F0F0D7A9B9E064', '8A9A82758BF33235BE654E8AA5087891', '生态气象查询', '41000002SGG97829004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F3711629CC482858E567D32E348D267', 'D4976D037CACB07FC657E39F5E29A7E8', '任职资格证书查询', '41000002SGG86524001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F3E79D9247EDFAD0F3C153A9E2905A4', '23F072CB88E4F86437986EF2383535C4', '一般纳税人资格查询', '41000002SGG03933001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F4490F42472A612E2099F55A1B7CC12', 'BFB89262A855E2305396FE7187950AAD', '车用气瓶充装单位许可（单位名称改变或地址更名，且工商营业执照发生变化）', 'MB1912677XK55653007', '车用气瓶充装单位许可（单位名称改变或地址更名，且工商营业执照发生变化）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F5DAA8BF4A3B0815E26071CAA730219', '42EFB01138010A18BD2DC8EBCE0CEBE5', '初次申请乙级测绘资质', '005184224XK0032901b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F5EDAB39092AC84ECA619C18D42CF49', '233415851095954C6D58BD8C0A84D744', '慈善组织担任受托人慈善信托重新备案', '005184312QT00201007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F690A1DE51DEB720AC5E4EFA755B170', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '律师事务所组织形式变更（国资所改制为特殊的普通合伙所）', '005184291XK55809013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F74EA31AEE919E58786AEB46720C84F', '7E2F45D9697999CC147882C7B05E48EA', '音像复制单位接收委托复制境外音像制品许可', 'YZ0000000XK55741002', '音像复制单位接收委托复制境外音像制品许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7F79F4053BFB0F4E5A56D73519FFDFAB', 'CB6CE6EF4381602C1A69014D6B001DA1', '音像制作单位设立、变更名称、兼并、合并、分立审批（工作场所为租赁性质）', 'YZ0000000XK25874004', '音像制作单位设立、变更名称、兼并、合并、分立审批（工作场所为租赁性质）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7FAFB2F656D9E8EAE54079F3EC2429E9', '2E6784788D59549E3E810E44F689C3E9', '医疗机构执业登记（中医、中西医结合医院）（变更执业地址）', 'MB1957883XK55857012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7FB35FB9B7C3903C65A5EF0ADF603D0C', '19E91F7F39CCFB42953CCC1130D51B4E', '河南省畜牧兽医执法监管服务', '41000002SGG52698001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7FBCD6A60579C69F17AD7C3707FBF2B7', 'A3FF2E23263B29DAB594A0C53D9542BF', '穿越高速公路埋设电缆设施许可', '005184435XK5575701a', '穿越高速公路埋设电缆设施许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7FD7568D4854E1C2DB535B2572FD2FF0', 'DD08833CEFFB8D7B5545EFE4B6C35D4B', '个人船员证书信息', '41000002SGG00456001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7FF3657A3036F72B1E5402C0E197F68C', '759F3550750C47067B64FEAFC30DAE9A', '香港、澳门地区人员申请注册会计师注册', '005184603XK55634003', '香港、澳门地区人员申请注册会计师注册', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('7FFA9724F282F630F48ACCC1763B63C5', 'B4FB53DDFC9E4BC0A95C43022E609B97', '农药生产许可证变更', '005184531XK95721002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('800C76C13767CC51821EFC94092394DA', '968F054ECAE8B120D3270083B11E6800', '社会团体、民办非企业单位、基金会、律师事务所、会计师事务所等单位社会保险注销登记', '698732712GG0620600c', '社会团体、民办非企业单位、基金会、律师事务所、会计师事务所等单位社会保险注销登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8012CF702DBE1F7CA7DF84BFB19C012A', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更法定代表人', 'MB1912669XK5572001f', '药品生产许可证变更法定代表人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('80189751228755E9D5710232DC9A8AC8', '7E53A326058F4BE1A74A4FDD8202AA44', '非国有省级文物保护单位不可移动文物转让、抵押或改变用途备案', '005184689QT22399004', '非国有省级文物保护单位不可移动文物转让、抵押或改变用途备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('80324BC569F4E6D637372CE286195376', '5DA2F3FF054BDEAAD9FE642E4B51D197', '求职创业补贴标准查询', '41000002SGG54406001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('803311A90AD03259306FA5D03940E438', 'EFE580E8D561D8EB8AFE24DACCD6D336', '饲料生产企业（续展）', '005184531XK40671007', '饲料生产企业（续展）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('803313CB48805837502D541581615E2F', 'ED1AB790D3AC960E00A9D7834C16EC74', '水运工程监理乙级企业资质认定首次申请', '005184435XK5567700a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('806D11CAA890A40402DE3179921B2CA4', '6A00FD4A1E99D9AE81440837CBAAB7C8', '举办台湾地区的文艺表演团体、个人参加的营业性演出变更（节目）', 'MB1848628XK12282001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('806F2772D629E679052F97E7C8C59603', '146DE1561018FBE802CBE7681D4B4DFE', '文化和自然遗产日资讯查询', '41000002SGG06421008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('807DD09C0FB3B27079260818D79C119B', 'B14FF8D52007DDCDD759F477346794BB', '档案中介服务机构备案复核（数字化服务）', '725831987QT00457003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('809B191ED7025EA91FB58F9F9BD606B1', 'E292B6CBC858077D7204B259257F1392', '开放档案查询', '41000002SGG94816001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('80DFA6B7ED467F9E76E2A004E7B7361A', 'C6FB9CE85C24504833631548F2A94902', '河南省企业研究开发财政补助管理', '005184638GG06155003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('80E053806C2DBEA29B86811BCB16393D', '8C35B97822C64BBFF8FB2F30E55ABA7B', '医疗机构开展人类辅助生殖技术许可变更机构名称', 'MB1957883XK00270003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('80E4F3B58C73AD0D872C34750688BA9A', '00A3A498AF128075BE4FE07DAF67B123', '药品委托生产变更', 'MB1912669XK55722002', '药品委托生产变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8101313DC7CB340CDEF5CD9BB470DA01', 'D4BE3C18CBF1ADAB2D68F5AF68316627', '中外合资经营、中外合作经营、外商独资经营企业互联网上网服务营业场所经营单位从事互联网上网服务经营活动变更（网络地址）', 'MB1848628XK78400006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('810B5F4F6EA0F30BFC95309296B6DBC2', '7342660123B43F44480FA3707071C532', '河南省农业农村厅人事信息查询', '41000002SGG61100010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('814410E2211565B7CCCF2D5D246907F5', 'D89DD96B6BE36CD2F8126341C967D00D', '省级行政区域内经营广播电视节目传送（无线）业务审批', '005184654XK35389001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('81731BDA06D763FB9E3B26B615D32042', '5C3E645A610D16DE563E3B9C8CD77BB6', '二级注册结构工程师资格注销申请', '005184400XK09297003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('81A84C6523EDCA29B4E842B94B9976E9', '48208B275D3117B60611BC000666643B', '河南省教育厅关于公布河南省第26期省级普通话水平测试员名单的通知', '41000002SGG48669001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('81C1F77AC86AA8BA81376CF6A1BC4BC0', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证变更注册地址', 'MB1912669XK5564600a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('81D1D022496399F7D441B94D8DB646D8', '2D0BBB663B32174B050689FB75CBA29B', '母婴保健技术服务（产前诊断）机构执业许可', 'MB1957883XK00279001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('81DFE37E8DAD55DAE1E2C5E0585C1CA1', '2A8763136AEC7E2C74B811C406A5F547', '审批初步设计的建设项目占用林地许可', '005184558XK54697005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('81EE9906131C3AE4D32C4C344ADDC85C', '29A96599608D140F9FC36594FDF8674C', '电子期刊查询', '41000002SGG35680001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('81FC55A926598E9B2F59CCC57C88ACAF', 'EDD2875C05B8639AB6293D4E24AA082C', '征期日历', '41000002SGG35983001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('821544F4566B4A0F3EF0B17B7F40CE17', 'ED1AB790D3AC960E00A9D7834C16EC74', '水运机电工程专项监理企业资质认定定期检验申请', '005184435XK55677013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('821EF60CF90B846EF2765C34CCAB6336', 'E3B95F4BAC523EAD48FFA645D4B012FB', '煤矿安全生产检测检验资质认可变更专职技术负责人', 'MB0P03925XK50108006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8255C1395241838662EC729BE62746D2', '6433BBAB242BC891B53D0B4BEB4FDBF0', '外国（地区）企业在中国境内从事生产经营活动开业登记', 'MB1912677XK55984002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8279BA1432FD6BC2CE39F75BBF15B214', '754004025F17D26D8ABDBAE237CAA73D', '河南省投资项目推介项目查询', '41000002SGG23547005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('829B4D271E15E9E992233F854FEC259C', '12567BB928ED40C5F25CCBB7A25340E6', '应用传统工艺配制中药制剂备案查询', '41000002SGG09120001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('82A5C5DED399186CA9CF06F1401C567A', 'A9DB8E35D2FC2F34CE5887BB84EC3389', '广播电视专用频段频率使用许可证（甲类）延期（初审）', '005184654XK51599003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('82F41B5EF669C715354C16B9F5955BE5', 'BDA5C09BCB9FD6C0220BDED43D8A7135', '个人账户明细', '41000002SGG62166005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('82F6685A60FCED4170E4240D211D12A7', 'B14FF8D52007DDCDD759F477346794BB', '档案中介服务机构备案复核（寄存服务）', '725831987QT00457002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8305C59267D0F471F6BB06A1BF09D874', '523B1C3D081BB3511DE5C38CAC3C10B2', '河南省图书馆豫图讲坛讲座预告查询', '41000002SGG75161001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('831F87C666F78BE02ECDBFE648FF9596', '563556F5BF7A3E5448A9207E818163B3', '未就业随军配偶基本养老保险关系转入机关事业单位养老保险', '698732712GG07724004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8328361EC1A0092DFDB9B6533FB24C70', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动变更（法定代表人、主要负责人）', 'MB1848628XK00096013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('833CB886D480F1FA4E361189A9852AAA', 'DF65985BD0DA6DA622EC54052F0BBBC4', '乡镇设立广播电视站和机关、部队、团体、企业事业单位设立有线广播电视站审批(注销）', '005184654XK55693002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('836953EB2FC8776CABEA694EEFE27294', '61E319C3185C459C9E95C89FB3479D16', '河南省重大创新引领专项管理', '005184638GG06155004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83A1D1C6C28D7F306FFEF168953BAE7F', '18506144639794501E1BD063CD35D03F', '人民防空工程防护设备检测机构开展检测工作前备案', '005184427QT3295L002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83B60FD41E67310FB587808C8DC39B4A', 'A533F11DB1C7B66D214878D03179DEED', '仿印邮票图案审批', '717817421XK55684002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83C000F7D775B8BF409C47DB78D483D5', '2D1A53542057BC7458BC14BF38F309D1', '机动车驾驶员培训信息查询', '41000002SGG70278001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83D08B1F393CE13FAF32452BE633F15A', '1552236E4C104D01E821FBEE294290A4', '公益文物展览免费参观服务', '41000002SGG30484001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83DFCBFD31C5B6867DAB1ABFEE8CB439', '3FFE5D12F3AFFFFF4B4B6269AE9434A9', '文物保护工程监理资质年检', '005184689JF00012001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83E14C955D4E363BAB2869E704D72E05', '30D70BFA323AD2533987F4E53A4BAAA4', '船员适任证书核发（职务晋升）', '005184435XK00313002', '船员适任证书核发（职务晋升）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83E196D1AAB9D711D3AED9EE28CB7B61', '3865CF4C89A1DD2420720829F3FD7820', '兽药生产许可证核发（异地改扩建）', '005184531XK84280003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83E7E6C72C285A1B30BB227526ADBDEB', '472BD22570EE1C860F9A249E259A0561', '接收卫星传送的境内电视节目审批（省直单位）（注销）', '005184654XK55662003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83E9F9D03058F5125DC70ECA694FD009', '8A9A82758BF33235BE654E8AA5087891', '河南省天气实况', '41000002SGG97829007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83EC57BE014D159CB059B7945544C7CA', '9C9E36BD9F11E18344C572602BEF5CBF', '报名参加穆斯林朝觐', '41000002SGG27018001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83EF576F1D7DE2D99C0D0428B3598F0E', '02A71EFC59424FADAC74A915C1BBB69F', '注册安全工程师信息查询', '41000002SGG98073001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('83F440917712C126BC19B009111B5BC9', '3DB7BC4546E0241C68DBF692C45EDC59', '工程系列中级专业技术职务任职资格人员名册', '41000002SGG44788001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8410616A81B9DD4CB7343B3616E2614E', '6BD654211E42460AAF688E90F9E9AF1E', '社会团体住所变更登记（凭产权证办理）', '005184312XK00204001', '社会团体住所变更登记（凭产权证办理）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('841C0B2C694F7827771F902AC4FD040E', 'F30E492057CE0E6365E6FEDD74EF2432', ' 地名标志设置', '41000002SGG05984001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('841C2508AC853894BCB0F9D7CCEDFD96', 'B043C5F466781BBE630CE9808479D221', '药品、医疗器械互联网信息服务审批变更机构名称', 'MB1912669XK00233002', '药品、医疗器械互联网信息服务审批变更机构名称', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('841CA3D4DA4B5252C70FAFDDA752CCB9', '8E65827EE8C7E6A7C1A0967FA8783D7B', '河南省科研设施与仪器共享专家查询', '41000002SGG56282001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('841EA0D1379B08BA0344F6D083FAC1C1', '44A2E0350A3C45D5F9A2061448B5C365', ' 招标信息查询', '41000002SGG24693001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('841F81171A75F4611FB9687A292E7B3E', 'E27919616185837BE8B39737E4FA5F1B', '施工图审查机构法人代表变更', '005184400QR7380900d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('84293616C7A08E5303BC35551D05F06E', '99148D4E92FC06070FBFEA35B3A333E7', '第二类、三类医疗器械委托生产备案', 'MB1912669QT24384001', '第二类、三类医疗器械委托生产备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('843BBF80B7F6691330C85019F703BD99', 'DFAE48CAC05BEE2AF514BA45272477A8', '机关养老账户信息查询', '41000002SGG75960007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8448F7C9955659D06DE64994A3E26FE2', '77C5B6574A8A7EB9BBD98212CBD584F6', '野生动植物保护名录', '41000002SGG53933005', '野生动植物保护名录', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('844EF6D1D8A04F336B1FE18BC06F34DD', 'BCD76C7D68E68A3068AF50CA3F7E5E26', '企业改革信息查询', '41000002SGG97727001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('84588F7B6CAE5A0B177979650D68B23D', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类医疗器械产品适用范围的变更', 'MB1912669XK00227007', '第二类医疗器械产品适用范围的变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('846F30329D92B5041D8ACA0EE7349040', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动变更（注册资本）', 'MB1848628XK00096017', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('848890529CE3492474818EA6AC4CFB2C', '515B380585DC11108AAFC2A666F870CB', '教育系列中级专业技术职务任职资格人员名册', '41000002SGG53141001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('84896D30CE98D07AAF44EB3784618A08', '375DB634E25898EC29D4004087F29A2E', '煤矿安全评价机构资质注销', 'MB0P03925XK42032005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8494151DA5AD2A2FF0E092D9DF830C42', '01D9E6117B935FF760EF4F20406F078C', '河南省人力资源和社会保障厅信息公开', '41000002SGG25826001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8496EFD95DAE36FD8F257CC3BA8005BE', 'C5018DF030AD133CCFC5C2FCA0468044', '食品经营许可证查询', '41000001SGG89294016', '食品经营许可证查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('84CD953EF767E65875246083415ABCFE', 'ECF017AF536045A7EA13437C93BEC2F0', '河南省情查询', '41000002SGG27167001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('84E19BC54B30652CB6339763FF184A05', '98CEA708A9F0443D208730886E0F11E6', '单独修建的人民防空工程竣工验收备案', '005184427QT9869L002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('84E37B916F23529ADED1103B08C83E16', '1F8399806A6849E1BD402C7672EC8D11', '区域性有线广播电视传输覆盖网总体规划、建设方案审核', '005184654XK55705001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('85116D73ADE5E50048190E16D6945A93', 'FE78F90F35BA31131922A28650BA4DE1', '产业专利数据查询', '41000001SGG00521001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('85223288154954450775D68ABE8082F5', '803790B446EDDADE8B0B39AE73AF178F', '2019年在豫开展高等学历继续教育校外教学点名单公示', '41000002SGG94823001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8545CDE282D3020853C8B1E01B608736', '728E730E964AC7E78366B472AA542324', '省残联理论研究', '41000002SGG90529001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8551A30DE9EDC81461C8DC71B0BFE02D', 'B397A935B73BFEC5A63ACFC5CE126B8D', '普通高校招生考试成绩证明（对口招生、专升本）', '005184662GG06628001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('856CDAC40F93CEA0252E351E274E6CE7', '58B8696831A7C6EC5E41430A925CC2F8', '从事经营性互联网文化活动补证', 'MB1848628XK02935009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8580F29F01E3FB3D8591C848D8AF15C6', '910AB99230A87FB0179F725DFDD2270B', '民用爆炸物品安全生产许可', '005184259XK55674001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('858D7BDF9BB4D01E5361D8081CEDF37B', '95C6036B5568E15782ACAB9EF180C2F1', '人民调解相关法规查询', '41000002SGG34488001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8590A81759AB69D30DB795F4DE5846D7', 'EE4409318D2B9304B4D83940EB6B9E30', '医疗机构从事戒毒治疗业务许可', 'MB1957883XK00265001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('85A9F948062CB25745918E191B83E405', 'D59084D67689943919BDA24DA66D73CD', '办税指南', '41000002SGG64256001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('85AE00E868B295821EA88B7EA571BCD6', '3FAD9893FE58BA5BF16B53F3B225A02B', ' 交通运输建设市场项目查询', '41000002SGG47518001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('85DAC34C6C57B7500AC9DADF2EF28E44', 'CD1788134D4EC991249E9D691D14FEBD', '河南省煤炭行业职业卫生专家库专家名单', '41000002SGG18782001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('85DB1845D916EFFEECCF725441B33C5E', '7342660123B43F44480FA3707071C532', '河南省农业农村厅意见征集查询', '41000002SGG61100014', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('85DFCA550D85E486189B32315D02A1A9', 'CB6CE6EF4381602C1A69014D6B001DA1', '音像制作单位变更法定代表人（负责人）审批', 'YZ0000000XK25874006', '音像制作单位变更法定代表人（负责人）审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('85ED69C7A9092B949FAD8B304D6AB459', '0256FD800DDD43744235E4580A5B4420', '全国残疾人按比例就业情况联网认证', '514559194ZS00124001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('861151CA0DFD1D145E0A858024CFD413', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '律师事务所组织形式变更（个人所改制为普通合伙所）', '005184291XK55809014', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8617FEC9BF8223D8510912C0342A0FDB', 'FD12CDF03E7591BDACE4479C2C7B541D', '河南省国有资产监督委员会信息查询', '41000002SGG94374001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8632AD257EDB7BE5D6885198ED48790F', '40770A05E7994EB6703F261FDF3181AA', '对国家新闻出版署负责的订户订购境外出版物审批的初审', 'YZ0000000XK55763001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('86430F1C6105928CDF37401514851D46', '46EA59D5711A8C6351AE575174FC1A69', '因修建供电建设工程占用、挖掘公路使高速公路改线审批', '005184435XK00320026', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8643CB8D5ABEA7131959200FE994584D', 'B515CF307F6951C04A15C8B0E0F11E06', '船员特殊培训合格证再有效', '005184435QR47208002', '船员特殊培训合格证再有效', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8652F397503174FF7C06B1DE85551050', '028CFED51685EDDDF80B2F7B05629FD4', '医疗器械广告审查', 'MB1912677XK27875001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8657C06BE34247A86C605959C7D777AA', '24B6AE3ED330D2804D7A7055CA96356E', ' 旅游包车资质信息查询', '41000002SGG19660001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('86581F5A5198A3CD109D559FA7AF9D88', '746C232A0A26A8F4FD97F1070EAF0146', '生猪运载工具备案查询', '41000002SGG13038001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8692D14C001F31EC814559C2B822C4E5', '20C677BD0AA82CBBA99826B94FBD8653', '传统村落查询', '41000002SGG30838006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('86AB3484116D30D41F2BF7D489957A7A', '61DFF7DA12B27F417B6B5741BB3D785C', '河南省机关事业单位工勤技能岗位人员电子证书打印', '41000002SGG93836001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('86B6C0316B113D234AC52489A277E577', '4817D97AD22A2C80987C339C69897837', '航行通告查询', '41000002SGG15423002', '航行通告查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('86C5EDD35856032B3758FCA72023A9EF', 'F9F6C2B5D1572005269DA71358583CBF', '特种设备检验、检测机构核准（地址搬迁，且工商营业执照发生变化）', 'MB1912677XK55667007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('86DEC0D22DE665564FFCC08FA912C53E', 'ACBED3B4D3E2B736A3B4C5B20E9658CD', '设区的市、县级地方新闻单位的信息网络传播视听节目许可证核发（延期）', '005184654QT0028700d', '设区的市、县级地方新闻单位的信息网络传播视听节目许可证核发（延期）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('871B9A829307D7E01726E325B3F42C11', '88ADD209F176624071DC2365578EA88D', '建筑业企业资质信息查询', '41000002SGG44890001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8732F70C1A7297D27A0705AC6E8CA060', '877B1A9B9BF7843BFD7E1956827709D7', '社会保障卡补（换）卡工本费收费标准', '41000002SGG36064001', '社会保障卡补（换）卡工本费收费标准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('87366F86E3D07B610A94D4F95A13FA00', 'DA8C60BD84AC3812AEAFB96E4490AF58', '香港、澳门律师事务所与内地律师事务所联营核准', '005184291XK00256001', '香港、澳门律师事务所与内地律师事务所联营核准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8738299F0ECBC51EDA71C8030CD64470', '5330E67B0D62F249896CDE1AA7F13D06', '小额贷款公司变更公司组织形式（审核）', '687145571QT00369004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('874672BC4E6753361E34F2A2F4CA538E', '78317AD72C1FD34CECEDC7FCC4C53349', '人才之窗岗位推荐', '41000002SGG28329001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8746AFFCE4390E5CF69EA47CA30F332A', '78EBDF4091A50015D16AB20FFFFD178E', '放射源诊疗技术和医用辐射机构许可（变更项目）', 'MB1957883XK00274008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('874FB9D448A73CF479DB658161BD84D6', '4817D97AD22A2C80987C339C69897837', '变更通航水域水上水下活动许可进度查询', '41000002SGG15423005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('87557CB3B8A8C07489DF16BFBED3EAE7', '0CA77513AD08FC7FFD1D6E32C8BCB707', '省内机关事业单位养老保险转出到省外机关事业单位基本养老保险', '698732712GG36356003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('877D68607647D9FD246272D1475A0A2D', 'ACE3DEB6F873805D4081C52A336E2A53', '药品生产注销证书信息查询', '41000002SGG27249006', '药品生产注销证书信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('87892029EC04F4069D87B9E2158AF96D', 'A69FAD52FE02C3C387DCE6CC08608602', '结婚登记查询', '41000002SGG59970001', '结婚登记查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('879A1A9B3F7D716D0BA5C905E509849A', 'A3FF2E23263B29DAB594A0C53D9542BF', '利用普通公路桥梁铺设电缆设施许可', '005184435XK55757002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('87A12E544858493F1F69043E07F9DA21', '9F3EE808C088287B8BA3203101CCD79E', '麻醉药品、第一类精神药品和第二类精神药品原料药定点生产企业以及需要使用麻醉药品、第一类精神药品为原料生产普通药品的药品生产企业计划审查上报', 'MB1912669XK55787004', '麻醉药品、第一类精神药品和第二类精神药品原料药定点生产企业以及需要使用麻醉药品、第一类精神药品为原料生产普通药品的药品生产企业计划审查上报', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('87B1BF16C1D5395304B163E14C6E6F88', '4753DF671DDB8BC3FF0496E43079C6B5', '意见箱', '41000002SGG90924001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('87D72B627C035795E4A81B7857BFC285', '589E443F48082C811CCC66BA710099B3', '中外及与港澳台合作办学项目名单', '41000002SGG02077001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('87EB74D26CB325B512C0ED10AAFE0475', '76D6F1EBF680F8394259007D9D8DCB47', '兼职律师变更执业机构（转入河南）', '005184291XK1164001b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('880DEEBFBB34957484373B8224955D7E', '0DD90E46D187BD7A287F7B2FF6982D17', '关于对拟推荐参加全国职业院校技能大赛教学能力比赛（中职组）名单进行公示的通知', '41000002SGG94078001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8813A1A278FD6AC9BD7DE41414B3D7B2', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动注销', 'MB1848628XK00096008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('881B5C248B216AA95FCE5D605C0F8078', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动审批（租赁场地）', 'MB1848628XK00096001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88232B86B93F369E40B025E202B0BD5B', 'FDA879034139553CFE158E2A744C84F6', '河南省中小企业公共服务平台创业信息查询', '41000002SGG83510001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('882FC9CBB6D764A62547443945CFFC2D', 'C5018DF030AD133CCFC5C2FCA0468044', '河南省市场监督管理局财政信息查询', '41000001SGG89294008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88476EAA742738D4A56682972622A01E', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动变更（外国法定代表人、外国主要负责人）', 'MB1848628XK0009601d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('886281C06D8E653104F8CB9F94AE8989', '7A0B88FEB0CDEE51B53E31F76F39E0BB', '文物修复资质审批', '005184689XK00385003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8871ABD4D94BBAC5D6A7F75841579425', '80E1D20B0A47D0B6EE6CE77C70F790D8', '对自考考生免考课程的确认', '005184662QR13064002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88720181B940C7645F8B41F6A70ECA46', 'B03E53495641EAC7607996F1E59239CB', '律师事务所分所住所变更备案', '005184291QT00250004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8887AAF031F6A577D126F65668903C1E', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证换发', 'MB1912669XK55720004', '药品生产许可证换发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88888CD7A76A966C0141F111639CFB0E', '4B8A1ACDB6E9A39514B1D851706F16F1', '河南省文物局政务公开', '41000002SGG01457001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8893871DD104E00084D8EB0F6A3E7D1A', '21336D7B16D3D82510E63B3ED76A983A', '药品出口销售证明的出具', 'MB1912669QT00231001', '药品出口销售证明的出具', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88C3CAD8BA4BD76FE589AE052E09B5D1', '472BD22570EE1C860F9A249E259A0561', '接收卫星传送的境外电视节目审批', '005184654XK55662007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88D051FB4D50643EE12A92B7A36AE13A', '2E71B28E67BA332A6C3B7BBE9181E9A8', '户籍地所在派出所查询', '41000002SGG90727001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88D11DCAD2165D6DB87D5E55BC76ABD9', 'E215E3FC13FC2ECE64DF89FFDA5E4A5F', '纳税人状态查询', '41000002SGG06208001', '纳税人状态查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88E5BC08FF1CA7A7840F0DEB2987C463', '8995304E974CE9C0F6942F0A7F6D707C', '教育系列初级专业技术职务任职资格人员名册', '41000002SGG72801001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88ECD8460A8C9F2CCD56BB382D79C154', 'A36EE644B0E9E910C7988E07F3A5DBE7', '设立中外合资、合作印刷企业审批（工作场所为单位自有产权）', 'YZ0000000XK55759001', '设立中外合资、合作印刷企业审批（工作场所为单位自有产权）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88EE76C003C8A90BFA10D96F4153B2FC', '71D70239CC9853D8B26FE243C0C6F219', '举办国际性广播电视节目交流、交易活动审批（初审）', '005184654XK13630001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88F1D9449C069EA48DC0C77445285E06', 'C525B5E72537B0A23B51D0D0C4C4876F', '水土保持政策法规', '41000002SGG68756001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88F6A7DD735E29E65BDB186DEEB3E20B', '2E6784788D59549E3E810E44F689C3E9', '医疗机构执业登记（中医、中西医结合医院）（变更法定代表人或主要负责人）', 'MB1957883XK5585700f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('88FE448C9129F0F799D4FEFB223B78C1', 'A1776CF64C8CDE6E4CB81A5FCC4567CA', '建设工程质量检测机构资质证书补办', '005184400XK55725003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('891248392076F7706B8D307AC8FE146D', 'D37A031E96F0B1264E27D9A4B285DDDA', '第二类监控化学品使用许可', 'MB0P03925XK55637001', '第二类监控化学品使用许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8922F1BC96D6E42E7B03483EF188B37C', 'AF8C630E088E9EBC57C0A559224C73FA', '民办非企业单位修改章程核准', '005184312XK55643001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('89350E6FF6CD3C1A54C16D4768BEA495', '9BA387903C7EB43493352FDC30E51C24', '药品委托检验', '41000002SGG07921001', '药品委托检验', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('894E92D9DCD2283521DA4029845DF0ED', 'F0AC63373773B5C1D07D5D2F75DA63A0', '国家公派出国留学名单', '41000002SGG10739002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8964DCB5BC58AACAB311530538AAA360', '91D9E5E11B5C473F0BCACE330C353258', '河南省卫生健康委员会内设机构查询', '41000002SGG63521004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('897A750656550756699A59A7E5229549', '07F7288AF1458573F124D507E47BF36D', ' 最低生活保障业务申请预约', '41000002SGG19186001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('897BB3EABEDA19F692F11742210FDEC6', '90E83B3FA61877948BE35DFCEE6FCC08', '水利工程质量检测单位资质等级标准查询', '41000002SGG07695001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('89AD6D967E1ABBFFD0649C0B63D14141', '1F87C50BD96E51CCD9DA0F18C62D2650', '统计人员培训咨询', '41000002SGG46006001', '统计人员培训咨询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('89B12A3CFAA6DD4C23EBC4BF66F88149', 'B17DE80325F5D10A3E5C38A29AC34A1D', '河南省交通运输厅标准规范查询', '41000002SGG78807001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('89D43CEBE86033EB933F91648176CFF2', 'E3B95F4BAC523EAD48FFA645D4B012FB', '煤矿安全生产检测检验资质认可机构改制、分立、合并', 'MB0P03925XK50108007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('89DCC2CC501506695475395EC8BAF4A2', 'B9203383CCAAFF911E31EA3FA2D20290', '森林旅游信息查询', '41000002SGG58373003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('89E86AD2CB1A6EB2C7B49882BEA3A498', 'F01441879DAB55C3D7EB727D92F3336F', '地质灾害治理工程勘查单位乙级资质认定（补证）', '005184224XK2803300c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('89EBEEA1ADB1D4885CB8B8704C7B9610', '70C329FF199E573A358075E959214034', '不动产登记证明查询', '41000002SGG49651001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('89FFEC5CF9A635F50284A0E6774F2590', 'E0AFB9F2A6088F3C5B92BC3B78F2BF24', '司法鉴定人延续登记审批', '005184291XK06476012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8A01F46ADFE961A9CCFC1C7759EE93F0', 'CAFFF3522503E54A49B1B0D359973B27', '资产评估分支机构名称变更备案', '005184603XK55636004', '资产评估分支机构名称变更备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8A1B8B6D787B6B861E399FD652A32A48', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '律师事务所组织形式变更（普通合伙所改制为特殊的普通合伙所）', '005184291XK55809016', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8A37A4012B19B897897D739CC3F55DEC', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（水泥、建筑用钢筋）生产许可证发证（不涉及产业政策,兼并重组）', 'MB1912677XK55651029', '重要工业产品（水泥、建筑用钢筋）生产许可证发证（不涉及产业政策,兼并重组）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8A4C7BCC3E6CD73F2944E8C6318F481A', 'B80D5F37B8BA1DB6410B2EB02D40FC2A', '工程勘察企业资质证书注册资本金变更', '005184400XK2009700c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8A57F1BAC7A5C3CED64787E9DC510CE8', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动换证', 'MB1848628XK00096025', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8A62F4B1F1F44A126B78D477CEFF3311', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '撤回派驻律师（河南总所撤回派驻省内分所律师）', '005184291XK5580901f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8A757134B8DBA06B5996E307FCC4D40D', '613340BD60125024C93CA8750215C093', '单位关键信息变更（企业）', '698732712GG33225005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8A8A980D8D084F3E40AAF50FE1951BB2', 'DFB86E8A59ABE91B58630324BA39C9D4', '河道管理范围内有关活动许可（挖筑鱼塘许可）', '005184566QT00239004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8AA0DF4073E9ED0283833D8E6EDC9D5D', 'ED1AB790D3AC960E00A9D7834C16EC74', '水运工程监理乙级企业资质认定延续申请', '005184435XK5567700b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8ABBA6C9C59CB8F0BDEACF149BB9B64D', 'C1D82BB486BF887D5C3B1C65ED902420', '行政审批处罚公示查询', '41000002SGG98738007', '行政审批处罚公示查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8AD4D1FB07279C0D9D59A656F08B1C66', '2E6784788D59549E3E810E44F689C3E9', '医疗机构变更诊疗科目', 'MB1957883XK5585700c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8AE543E4F96F99BC4BD24DCEF46A7B16', '1786DCDA846BB5BAD475C9FC2F9E5426', '河南省残联劳动就业服务查询', '41000002SGG57083006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8AE7A5B948FC9A09D53EBF95159FA284', '0186FAE67E1102BAD6A5879CF3665319', '携带少量麻醉药品和精神药品出入境证明出具', 'MB1912669XK00226001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8AF738CC73AD58F914BCF2A4D595C7C0', '5C3E645A610D16DE563E3B9C8CD77BB6', '二级注册结构工程师注册证书补办申请', '005184400XK09297004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8B0728B280656801B7132A6319CBDA08', '432C0AAD9FC26FB9867A3ACBD9644A43', '公共资源交易综合管理协调', '41000002SGG34181001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8B29D6E1839356FA7E6309EF5A01FA00', 'E245E4E82DF88F0851C6FC41D1531426', '农作物种子生产经营许可证核发(B证设立)', '005184531XK96500005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8B4C0E0CB44F462E73C8A6060A0A76B9', '0E4D73C63FB40CFF30A51EB67441174F', '航空港主要经济指标', '41000002SGG24839001', '航空港主要经济指标', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8B6CFB2C737BFDCB56EE8DB635AE4321', '5553B26E87C519D05A4533F5D8EA9B93', '二级注册计量师变更注册（变更执业单位）', 'MB1912677QT00044002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8B8B04D4EA84A6EF72803FF508A7A433', 'A670566973A0A9572FB1E0071F5FCFCF', '建设项目用地预审与规划选址（不涉永久基本农田但跨省辖市、省直管县市）核发', '005184224XK00166004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8B8F299E86BB6AFC309DCA87C6A16A63', 'E0D774B1944090269E99BAD3A0237BF6', '药品生产企业委托检验备案', 'MB1912669QT00232001', '药品生产企业委托检验备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8B9037A4C0CD62FED57AF1AA1383C579', '131AFEE53EA53913F153AD4613AD7F68', '省级行政区域内经营广播电视节目传送（有线）业务审批（变更）', '005184654XK40260002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8BA5E808FF7E9C3AAD8D2BC0E8F635F8', '4A83FC132D650EBBBD749F6FD472E8FA', '河南省图书馆网上续借须知查询', '41000002SGG17760001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8BBB5AF6C24A44D8A590FA052B8AB61B', '1786DCDA846BB5BAD475C9FC2F9E5426', '河南省残联文化体育服务查询', '41000002SGG57083007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8BCE3ED608130EE131CAF26C2BB05C57', '0EBA0F42D82FA5AB32547430DDC162A0', '血站变更地址', 'MB1957883XK00268004', '血站变更地址', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8BDA54F4B0F8515A16B1F4CDBA6A1DE5', 'CCB3BFF63772B6DBE0970BDD943E5F93', '省税务局税务视频查询', '41000002SGG44593007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8BF021D1A0ACA78856CB4860700DDC4D', 'BB503C435C1F859B358B080BCDA64072', '中国人体器官捐献咨询', '41000002SGG53349001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8BF5FE5463A6869916EFF055FC7572DE', '5384982128486C8A61BEE9326133A26D', '河南省志愿组织查询', '41000002SGG09619002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8C0C9CAB39C4846D6FFE6FCDF64AE209', '4D5CDE76D1D2222952AB7B3DD652FE6A', '河南省流动人员档案查询', '41000002SGG40383001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8C13AC94FA618A3EC78F9D03994BC1DE', '0EF2E76F71AA11CDC8CD5405876DC3EE', '河南省名师工作室查询', '41000002SGG17684001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8C17B4D040E99F6E989C6ACB419237D4', 'A3FF2E23263B29DAB594A0C53D9542BF', '跨越普通公路修建桥梁设施许可', '005184435XK55757004', '跨越普通公路修建桥梁设施许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8C2C504D7CAE6BA1CA24267E7B4C60F8', '41FC47AC50A224E64BFC88ACAFD335E4', '城乡规划编制单位乙级资质认定（注销）', '005184224XK1254300c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8C5A4E67893CA9737054B6FBBC6EAB29', '2D0BBB663B32174B050689FB75CBA29B', '母婴保健技术服务（产前诊断）机构变更机构名称', 'MB1957883XK00279006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8C5D82320C007471DFBDA1BE1C38D25D', 'FC5C37984298D5A123631BEA3E498401', '省级医疗事故技术鉴定', '41000002SGG39168001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8C8A451AF5B124F8774452EF0BD89AE0', '598545B61577966532C21C1E1BA4D277', '河南省图书馆馆藏分布地图查询', '41000002SGG37645002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8CC28723936713209469A17F64AB7117', '06A447EA2F4D0E4B8E34E0AF0455F3AD', '卫生许可查询', '41000002SGG03500001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8CC37710D40E4F8626F78F32BF38F9C6', '00A3A498AF128075BE4FE07DAF67B123', '药品生产质量管理规范认证证书注销', 'MB1912669XK55669003', '药品生产质量管理规范认证证书注销', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8CCB323329AFA64CD9B28EBC84EE84EC', 'EEE4E4CC95690EEA148DABF6F8F22CA0', '12369环境污染举报热线', '41000002SGG13930001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8CCB8D80669EE9B653B9C4BBC4761630', 'FB2F1EF1A256BE5298EF56E3628B009B', '民办职业培训学校分立、合并审批', '698732712XK00123002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8CE7E0D45370318FBB1C8D9D1DABE2D4', '77F5844D7BFF1BAD6A4D65B153DC96E7', '固体废物申报登记确认', '005185040QR13054001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8CE951E392DCF6C5A493A654CE86B219', '76A27C3FAB51112E394E34FB5104976F', '快递企业注册地址变更', '717817421XK55671003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8CEB8BBD409713A4DF971DF17BBFA2C5', 'D73A84CE98092089FDCB3FFA854534B0', '禁捕怀卵亲体的捕捞许可', '005184531XK68909003', '禁捕怀卵亲体的捕捞许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8CEDE99434F5E47A3CEDC7A1111777B3', 'CCB3BFF63772B6DBE0970BDD943E5F93', '省税务局媒体视点查询', '41000002SGG44593008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8CF47B880BDED60447E2057321ADE7F9', '3876818245353DEE7557DF0AAF399754', '享受税收优惠政策的动漫企业认定初审', '005184670QR00018001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8D0826B5A1C8505280027D582ABA29FC', 'E7DF2F0F0E6D2A20ED4A32E65C64E98B', '河南省测绘优质工程', '415800560QT00321001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8D280971E3C0A4178B6EE49E58D75329', 'C5ED770FBE11FF9998E2A244DE3CBE94', '交通运输执法监察和违法超限治理相关政策咨询', '41000002SGG72081001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8D2AE49FAA5DE8465D57816FCD34755F', '069869E17765B9B7E90E84422BEE6635', '船舶进行散装液体污染危害性货物水上过驳作业审批', '005184435XK56006001', '船舶进行散装液体污染危害性货物水上过驳作业审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8D30CA882E6A582AC313266EA2E0F3BF', '1BB5DFAAB8B01AF6BA2ECEAB57EBFEC1', '出版物批发单位终止经营活动审批', 'YZ0000000XK55752009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8D56CE18E778D5E262C1B41E3AC65082', '85B25C00BDF6B9FF2D70E643170AC28D', '消防信息查询', '41000002SGG87315001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8D5D9CFAF79F0D34000CD6DCF394E54F', '84B9B5A492650A1639F5589ACA398314', '河南省水利史查询', '41000002SGG02958001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8D5E070BDD049FBF270FE9720D7A9C12', '45FA45E780B387B61B2D8F51EFA326DA', '权限内国家重点保护陆生野生动物人工繁育许可证核发（增项）', '005184558XK99852003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8D71878C9BD5490E9469A9D231950C6C', 'A94E093793118A2E0F24DC5A769994A8', '法人验收质量结论核定核备(单位工程）', '005184566QR00060003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8D829C06B79AB3E56E27FA0AC42D6F19', 'D86A1F3246F843EDE7A51CCEAC37618C', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动变更（地址-自有场地）', 'MB1848628XK55961010', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动变更（地址-自有场地）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8D97F07BE3C8AE6205B75F4C7E7360C1', 'F039F2F83D5A3CC4606776B58A5D18A2', '河南省中小企业公共服务平台其他政策查询', '41000002SGG03029004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8DB015676135184CCAF8AD276CDEA3E1', '8EF83C680FBA25ACA9BDC535B2AAEEA3', '拟认定的河南省中小企业特色产业集群名单', '41000002SGG22769001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8DC3FD680CDA3F714A1CE8F0723C5C35', 'CAFFF3522503E54A49B1B0D359973B27', '资产评估机构合伙人或者股东变更备案', '005184603XK55636007', '资产评估机构合伙人或者股东变更备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8DDDEE70115D0F60C70499DD14B6C148', '2C4F18F5A54B0264EAAC6E6013E00F49', '民办非企业单位宗旨和业务范围变更登记', '005184312XK00205003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8DE7B99AC2B73095599423541BB58E3E', '74706F0C853EAB8E896EA8D8231DB062', '中国地震台网地震活动', '41000002SGG63690001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8DE98E69BDF74154D3D86EDEC309095C', '8B8EDDB96C539CB45CB8354C60BDE5C7', ' 拟定特困人员供养标准', '41000002SGG02797001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8DF04413F1E944D42906BEC6A3E372DA', '2D0BBB663B32174B050689FB75CBA29B', '母婴保健技术服务（产前诊断）机构变更法定代表人', 'MB1957883XK00279004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8DF17ADF2761C77EEF9CA4602F81F1F9', '34165C876D71C21CEE81E38C95DB45CE', '在高速公路建筑控制区内埋设管道设施许可', '005184435XK55764001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8DF1EE7401E8FCE7092999C2E8954457', 'C7C883239C676080AA66A57C61EEAC8A', '河南省中小企业公共服务示范平台拟认定名单', '41000002SGG25694001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8DFB47C27FC2DA62F904A1A1D3D98E49', 'F9F6C2B5D1572005269DA71358583CBF', '特种设备检验、检测机构核准（地址搬迁，且工商营业执照未发生变化）', 'MB1912677XK55667004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8E0381531C1B90899092DD2D83780653', 'ED1AB790D3AC960E00A9D7834C16EC74', '水运工程监理乙级企业资质认定变更申请', '005184435XK5567700d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8E0CD49CADD5D288F2CD74ADDD37A415', 'DE9F87A2D677F5AF5A93A0749D546D88', '法人或者其他组织需要利用属于国家秘密的基础测绘成果审批', '415800560XK00330001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8E3BCB2362BEA1FA69CDFFD7352D61DF', 'EFC54A32DF7246CD23B059BEB930122A', '举办香港特别行政区、澳门特别行政区的文艺表演团体、个人参加的营业性演出审批(不含未成年，在演出场所举办的营业性演出)', 'MB1848628XK55962007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8E493CCEB7920F2348990AA820838DAB', '1EFFF9875D74B9524A813BC8178AC7F3', '延续申请涉外调查机构资格认定', '005184355XK00076006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8E4D53D86F4BA5EB00C45556432E439C', '472F7FBAFBCF9D5D6AB0212817EEEE56', '省体育局省局动态查询', '41000002SGG78500012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8E6A6CADA2D8000C5A585E6D40A88E0E', '472F7FBAFBCF9D5D6AB0212817EEEE56', '省体育局体育产业查询', '41000002SGG78500004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8E71811CCC8E6600556CDD4CAD76A8F3', '8A9A82758BF33235BE654E8AA5087891', '图解气象查询', '41000002SGG97829006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8E7E9BCF9871DBA768453486A2912D70', '8AEEEDDE4DE0CDD1069228F5B20C345C', '河南省客运客运丢失、损毁补办查询', '41000002SGG57878005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8E90D0C79B73BDDD4AFA33552B39210C', 'BE52D4B55AA450ECE0F0565C222F99EC', '人民防空工程施工质量检查（顶板钢筋及管道、预埋件隐蔽前）', '005184427QT6050N003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8EA501BBE52377EDFE80B583054E46B2', 'C923DE4DDC9797FE75E84CEC09AB7D3A', '在自贸试验区内外商独资经营的游艺娱乐场所经营单位注销', 'MB1848628GG1310700b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8EB3EA3D1F1857AAEA7A887EB2FF1E3B', '41FC47AC50A224E64BFC88ACAFD335E4', '城乡规划编制单位乙级资质认定（初次申请）', '005184224XK12543007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8EB71711283F5AB920118478FE069F0F', '00636AE0CB26698E3329A4CB74FAA0D9', '外省进豫备案信息查询（调转网站地址无人维护）', '41000002SGG63126001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8EE132B5E92C5DD209B0C100D5B14F8A', '1F3E8080C43AE39DB9B9307DF4CC159C', '特种设备生产单位许可（设计、安装、改造、修理单位地址搬迁，且工商营业执照未发生变化）', 'MB1912677XK00080015', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8F0FFC71F2506376A22DCED158A0059B', '00A3A498AF128075BE4FE07DAF67B123', '药品经营质量管理规范认证证书核发（批发）', 'MB1912669XK55836001', '药品经营质量管理规范认证证书核发（批发）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8F29D044E18220CC01FB9FFE56DCC7BF', 'F039F2F83D5A3CC4606776B58A5D18A2', '河南省中小企业公共服务平台民营政策查询', '41000002SGG03029005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8F2A8B70308E7EB18B2D0729BA77F4C7', '944E250FB6D836BE8C15DD9833DFA47A', '河南省财政厅政府采购', '41000002SGG75858001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8F4BAB51684ECD42D6BE8B033B70C330', '713E174D89169A54A91D10F9B5ADD1FB', '海员证核发（到期换发）', '005184435XK56002003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8F8AFBAEFA9047FF3F10BCA9B67B382D', '3DB34CA479A41AE7B41A5E27D9913A21', '煤矿企业主要负责人安全生产知识和管理能力考核', 'MB0P03925QT00151001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8F9416006B77EC80E862596699C9A786', '523B1C3D081BB3511DE5C38CAC3C10B2', '河南省图书馆豫图讲坛嘉宾查询', '41000002SGG75161002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8FA357512E1519836EB0A0869846BEC4', '1A9A13D94B43C8F26C2C5CED6EFAD7F7', '设立宗教活动场所及登记', '41000002SGG12638001', '设立宗教活动场所及登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8FAEAC436D8D9D565467D007CC42BC0F', 'D0E40284EC9C9663DD50C4001D0B139E', '举办外国的文艺表演团体、个人参加的营业性演出增加演出地备案(在演出场所举办的营业性演出)', 'MB1848628XK1806900b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('8FF8261BF19347B866B5B14F67D44A60', 'C6E3FA90446B65339FDB4AF9FEF6527C', '出境游名单审核', '005184830QT00196001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90196532A241B1FED8F99CD566A082FF', '7247A33F7A1033FF0E56410550405C5F', '河南省文化和旅游厅政务公开', '41000002SGG76164001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('901A121627018444A94268BD058C2E5F', 'BFB89262A855E2305396FE7187950AAD', '车用气瓶充装单位许可（单位名称改变或地址更名，且工商营业执照未发生变化）', 'MB1912677XK55653004', '车用气瓶充装单位许可（单位名称改变或地址更名，且工商营业执照未发生变化）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('901B88ACCFB934059AC379D5E0EB2E74', '0EBA0F42D82FA5AB32547430DDC162A0', '血站变更采供血范围', 'MB1957883XK00268008', '血站变更采供血范围', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('901EBAA1809EF79DD948005C3EFF9B98', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更负责人', 'MB1912669XK55718006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9022036DC9E533FB71E0D744943226F8', 'CAFFF3522503E54A49B1B0D359973B27', '资产评估分支机构负责人变更备案', '005184603XK55636009', '资产评估分支机构负责人变更备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90227F1CB653B338E8F4E552C1CEB265', '175C971222104069558624D3087E5397', '广播电视节目制作经营单位设立审批（变更机构名称）', '005184654XK00299006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('904331E0DC0444D881D1FD974FC39919', '5346867A280E796357998B9940AB4D32', '河南省扶贫开发办公室信息公开', '41000002SGG47097001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('905D1FFF35166A6EF83464C680F9CD1C', 'ADCFC24A2EFD16EE91E1CDE652FFBBBB', '生产经营单位主要负责人和安全管理人员安全生产能力考核合格证', '41000002SGG52563001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9080C09712271364648F3F3D6A40CDEA', '563AF1C1A9238505C004CEEEE0F23F12', '食盐定点生产企业证书注销', 'MB1912677XKEDPF8004', '食盐定点生产企业证书注销', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90886B523F9F8B44CE460CBFDC31D52A', '17BDE32805D595CE6D6727FB7A1D6811', '电视剧制作许可证（甲种）核发（初审）', '005184654XK00667001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('908975710E81E37F0C558CC54BE0F3C9', '5C32AB34F13735A9B97793F5C5D3C390', '河南省统计公报查询', '41000002SGG24941001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9089D42E9462B6CE6353427999747FC5', '48C6A5131CFB54F090B4988861C530D4', '司法行政案例查询', '41000002SGG29701001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('908C298424F86DA3F9FB6168EC976814', '54E2C68F7845CDE9E82835698EDD90BE', '实验动物生产许可（到期换证）', '005184638XK00104005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('908F7ED167A47E6E86BF1DE8FF66BA88', '3FE40A4B5CB4775C4CA4C3F2C99B1EEC', '天气预报', '41000002SGG45459001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('909DD98AD8E5F688EDA690D7F67D14D3', '53D4F9641723E237938D1518951B7822', '省检验检测高技术公共服务', '41000002SGG46741001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90A85308EEA2752F263907A3F13813AC', '00A3A498AF128075BE4FE07DAF67B123', '药品委托生产延续', 'MB1912669XK55722003', '药品委托生产延续', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90A9A826B60F47D8BA7CAC9F1BCDC033', '8A9A82758BF33235BE654E8AA5087891', '河南省气候查询', '41000002SGG97829008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90BF384E60682761EA44D479237D7453', '9E6BAD254F3237C8E0543EE01BF3004F', '六五世界环境日宣传活动', '41000002SGG10157001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90DC3F4480204055DD5C759E907C4109', 'F3A5EFFE79212CE56FFD5325A49BB511', '河南省中小企业公共服务平台投资合作资讯查询', '41000002SGG68056002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90E4F202D0D3EE596C64974848CFE396', '5330E67B0D62F249896CDE1AA7F13D06', '小额贷款公司修改公司章程（审核）', '687145571QT00369010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90ED940479B1A9FC627A7A2C4C5600F7', '61A76621CA09D68F8981AE37C18050D7', '医疗机构放射性职业病危害建设项目预评价报告审核', 'MB1957883XK00263001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90F3A76BEEEABF103D631B8310ACC289', '8BB961AD31ED8FE644EF6B9B5AE2A62B', '河南省教育厅关于2019年度国家和河南省精品在线开放课程评审结果的公示', '41000002SGG48829001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90F88F8ACFB42BA38BD003A56B9D5237', '60B10F8EA983D9FD0EFAE102BE71D0D2', '旅行社信息变更（地址-租赁场地）', 'MB1848628XK0019800a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('90FA31D8969A14EAA73CB6ED4F25A72A', '9417B489A6ADAE152C71C581FC471996', '河南省公路管理局政府信息公开查询', '41000002SGG21730009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('912130969411466927493EB542E4D205', '2D1055872C9544CF086F84D38DEBB357', '三类超限运输车辆在省范围内跨设区的市行驶普通干线公路许可', '005184435XK0032200d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9126EA6E5A9CE59B72061AF5749A708F', 'B7B1C00A507B5DF5D70A5834D6F12195', '海外孔子学院名单', '41000002SGG87565001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('912B827B051332CE2943B5F4123309DD', '788711B922B638559783205F0016331A', '在自贸试验区内的中外合资、中外合作经营、外资经营的演出经纪机构申请从事营业性演出经营活动换证', 'MB1848628XK55922006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('912FA312761F292999F74CF70C449879', '370496933CCE52C470B0C82736FC227A', '河南省民族宗教工作动态查询', '41000002SGG59185008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9155923E2047F0C82EDBE1A8E1B39D03', '78EBDF4091A50015D16AB20FFFFD178E', '放射源诊疗技术和医用辐射机构许可（变更机构名称）', 'MB1957883XK00274002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('91694261BD3EA6FDD4A021D917D8E68E', 'B80D5F37B8BA1DB6410B2EB02D40FC2A', '工程勘察企业资质吸收合并', '005184400XK83829006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('917FFE6F45CC491DC603E469D274AE95', '628D42B0CD977081D8C468E7623C28F7', '就业见习补贴标准查询', '41000002SGG12262008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('918BCAE6820C042468F141D0036F52FE', '09521EC965AC45C298B704C2BB15F424', '申请变更频道名称、呼号、标识审批（初审）', '005184654XK42638004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('91A1ABA7D2BE21D9782F6D5EAC8CFAE3', 'BFB89262A855E2305396FE7187950AAD', '车用气瓶充装单位许可（地址搬迁，且工商营业执照发生变化）', 'MB1912677XK55653003', '车用气瓶充装单位许可（地址搬迁，且工商营业执照发生变化）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('91A6280D9936FB625BFD6AD43A139840', 'CC714B8097E205E94007D8EED3A15690', '河南省教育厅关于公布河南省第三批现代学徒制试点单位的通知', '41000002SGG65468001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('91AA11CE55A626838D043C5E437434F0', '01BCC45456D10181BA4CF6C3AC7DD94B', '统一公共支付', '41000002SGG16929001', '统一公共支付', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('91AE46A3BF7DE8D209BAF83AC51A32E2', '60B10F8EA983D9FD0EFAE102BE71D0D2', '旅行社信息变更（名称）', 'MB1848628XK00198006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('91DC6EEDD9CF88E233504B5CA03BD799', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动换证', 'MB1848628XK00096026', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('91E2B9F191FCA620CA84198DA3E5BCD2', '375DB634E25898EC29D4004087F29A2E', '煤矿安全评价机构变更机构名称', 'MB0P03925XK42032004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('92015A997AF72E238E920EDD7B993DDA', '0A9D34C293742743E870DD63877AE556', '受理省外院校毕业生报到（毕业生就业报到证签发到中央驻豫及河南省省直机关、企事业单位的）', '005184662GG33214001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('920169708B373B3EBC5214116E6DE03A', 'DB553F8E937BA18F2237BA0AD1F86D16', '产业信息查询', '41000002SGG65249001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('922C9EF46979633E9CE1D926C901E4C8', 'F01441879DAB55C3D7EB727D92F3336F', '地质灾害治理工程勘查单位乙级资质认定（注销）', '005184224XK2803300a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('923CA42C3DDAAAEA83143B4F05F0A82C', 'BE80C6B10253ECB2E3CA1150ACE4FD6B', '企业单位演出经纪机构从事营业性演出经营活动延续', 'MB1848628XK5597700d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('924B9D1F509D8E0CB5DEC13DD436EC81', '520FE4BC9F6456669DC8C96C3969EDB3', '河南营商环境工作状态查询', '41000002SGG60852001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('926109AF268FA43F7F50AED8ABE6C038', '8417BF93AB5076B8CA7524F0022823A9', '出入境记录查询', '41000002SGG84288001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9268E0147E1368389DACF12DBE2C3E3B', 'CEF6CD18FF985612CE1AE47D61ECA9CD', '招收外国留学生资格高等学校名单', '41000002SGG11750001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('927A580EB557E0A72389DE1C285AE949', 'C79652ADAB31888F6D02D0C6868789D0', '河南省扶贫开发办公室河南省扶贫政策', '41000002SGG84765001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('927B573D558255253121EFF3DABC8F67', 'A38A4DFECB112B15FC6923E2FA779B48', '广播电台、电视台以卫星等传输方式进口、转播境外广播电视节目审批（初审）', '005184654XK71888001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('92847B6D937812F29A4304B517475E86', '66166E4E889FF9A7822DB5BC40DFA025', '水利工程质量检测单位乙级资质延续（混凝土工程）', '005184566XK97268010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('928FB415675E259E53E242940AE63EF9', 'FAE7C882AE3F49825CFD0E065270B022', '通航水域禁航区、交通管制区、锚地和安全作业区划定', '005184435QT00318002', '通航水域禁航区、交通管制区、锚地和安全作业区划定', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9293E60C56D93CEACE5610B26B58EBD9', 'A7721BBF9C6B14E3D8E1A15383216AB6', '注销时间查询', '41000002SGG62239001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('92A1DDE16C29FEC4375353F132404281', 'B80D5F37B8BA1DB6410B2EB02D40FC2A', '申请工程勘察资质增项', '005184400XK20097002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('92A54BB654E42DE404F87DC9F7FFA47F', '2EC86F59A47632C09758C99DF2D963AF', '地震局政策法规', '41000002SGG48710001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('92DB449C969E916C42CAAE30FC2FDC48', 'E4EE6E1ECF5C561B5847FE8493C9C701', '民办普通高校名单', '41000002SGG87875001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('932A74E2E27CE2E6A04C83758ED44081', 'B102E006CDEE8D2A1C0C721937B8E389', '河南省统计年鉴', '41000002SGG59252001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9336F4FC43E63FCB607E4031CCCCCE88', 'BC60ECBA4AE58BE7AB7CC8C611863392', '融资担保机构设立（审核）', '687145571XK00368006', '融资担保机构设立（审核）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('933F7F449BE90D6EEE119FA4849BBC02', 'FB2F1EF1A256BE5298EF56E3628B009B', '民办职业培训学校变更审批（办学地址变更）', '698732712XK00123005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('93483F924B48B6382C835456F994E3A2', 'CFB89D1CC54215D59441D4B125D83196', '河南省农村危房改造相关政策查询', '41000002SGG10937001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('935B7F93C911BF9A72031CBE55531FAE', '2BDCCA0F048C862A2F98DFB3AB0589CA', '邮政基本资费表', '41000002SGG39739001', '邮政基本资费表', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('93678680C60E5D45D86045399E5E09EA', 'CEA06E6BA4AF9642120B970200250C4F', '分类处理人社领域信访投诉请求九项途径', '41000002SGG85720001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('936C710C325E8C0FFDDA540B8BDC06EF', '56FEEBADEA7EC0A72B6499E2EB2DBA53', '重大税收违法失信案件年度查询', '41000002SGG67186011', '重大税收违法失信案件年度查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('936CC2D224D6588E7995B61D3A86C08B', '78F0F23B34FA0FE80E28B9D0E1B4DAAC', '采集国家一级保护野生植物（农业类）审批(进行科学考察、进行野生植物人工培育驯化,承担省部级以上科研项目)', '005184531XK24555004', '采集国家一级保护野生植物（农业类）审批(进行科学考察、进行野生植物人工培育驯化,承担省部级以上科研项目)', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('93789A59A31E5385CD6322AA85218EF0', '8D70BAA00223955E4A996550799B5765', '河南省图书馆学术研究查询', '41000002SGG33977002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9381EA13E5D42FFC62E63188B3D6235D', '0805FCE80EE2EF0A095B3B1BBB3F61B2', '开展节能宣传周活动', '41000002SGG06420001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('93A2A588431A72984E614B6B0E76E5AF', 'F18C00D7CD54B9132371C4FFC00D3DE1', '培育新的畜禽品种、配套系进行中间试验的批准', '005184531XK46176001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('93BD4C537CA7DE1E9B21956C51AE59DA', '9417B489A6ADAE152C71C581FC471996', '河南省公路客运汽车站新闻公告查询', '41000002SGG21730012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('93E538A1C52DE1BE805451391C7939B4', '002447207831DA53248577890AA4AD3B', '文物保护工程勘察设计乙级证书注销', '005184689XK94270017', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('93FA7602482772EF4A6D675B271124CC', 'D49C411DD9718C900A47C62DE1C239B8', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出场所经营单位从事演出场所经营活动换证', 'MB1848628XK55963007', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出场所经营单位从事演出场所经营活动换证', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('94151CCA8632BB4197D784F88A286818', 'E3B95F4BAC523EAD48FFA645D4B012FB', '煤矿安全生产检测检验机构资质认可延续申请', 'MB0P03925XK50108003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9419B344A7F0F2284EE4FF8AE9D3F525', '46EA59D5711A8C6351AE575174FC1A69', '因修建水利建设工程占用、挖掘公路使高速公路改线审批', '005184435XK0032002d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('94423F338F60B7CE4972BC0D12A8F8CE', '4F70A8321E1F44ED99983ADA119D59EA', '河南省航务局政策法规查询', '41000002SGG35866003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9460590D238807B365E8DA4943D13D73', '595C4D2A23EF64E93FF53F42F5CE3055', '在公共场所开展环境保护宣传教育活动', '41000002SGG03007002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('946FA981AAA81DE65AC2A8560CA6615A', '107ED0DF64A891CFA53C8993DFA7F7CC', 'A级旅游景区咨询', '41000002SGG31915001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('948EBC7CE85122BF72E7933811A4AE83', '355058280058A75ABABE2A8C92183700', ' 港航法规查询', '41000002SGG78146001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9493AB773C5CD0678AE6BFBE36466538', '7C70089AE675347745C60F8CF8FA402F', '河南省交通运输信用公示', '41000002SGG79266004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('949A9B23DD1B5FB04FCEB086D8867061', '4A74C0B3E471AAA1A64C830BB10A2916', '企业养老待遇发放信息查询', '41000002SGG17744001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('94A3C6431B580561372CFAB1DC2F1B8B', 'DAB301DDC04E0D58DCD9AA1BC0217944', '省级建设项目环境影响评价文件审批（非辐射类且编制报告书的项目）首次申请', '005185040XK49971007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('94ACD17D712F37838E05B8052BA271C3', 'F338DDA3617143AB04BE216850AEB6C0', '养老保险个人信息查询', '41000002SGG98988001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('94B51485B5A1A658906263C197FF0898', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动变更（企业类型）', 'MB1848628XK00096022', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('94BE940451EE65DD2BF2960C0EB97431', 'F5EF19BC786AC1D926559D5DB0166B64', '香港医师来内地短期执业许可', 'MB1957883XK55867001', '香港医师来内地短期执业许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('94D4C3535FA9CE49B47C77D468B2BC68', '5330E67B0D62F249896CDE1AA7F13D06', '小额贷款公司增加注册资本（审核）', '687145571QT00369005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('94DB3D0A5B0306B7A70B2FAD34585866', '8B68B942D3261AE9B4F9B7A8A7CF8061', '仲裁委员会变更备案', '005184291XK00262003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('94DC1EC359B0978E994F82ACA1EE05FF', 'EFA175BBF686C624B24D011A5BB98B06', '异议登记信息查询', '41000002SGG97331005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('94E0377507EA73361190C932919106C6', 'D235FDA5164044BB0DD76F865E0AB516', '河南省住院医师规范化培训报名学员查询', '41000002SGG40702001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('94EA415CC1A002E803DBF16D713CCD98', '080D3EE957C7223A38277D6480772328', '疫情防控社会捐赠款物查询', '41000002SGG52805001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('95062110A5810588D4DBE2609443BC1E', 'E9FB56D19BBFA89348FB04165357C4FA', '航道通航条件影响评价审核', '005184435XK00312002', '航道通航条件影响评价审核', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('950A779413C684314B6CE38C242A9E64', '6E1BF3D87733B54093A13208B537DEFB', '水利工程招标投标信息查询', '41000002SGG49714001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('952167818DC2E2BC12F9E16AD445C2E4', '00A3A498AF128075BE4FE07DAF67B123', '食品添加剂生产许可注销', 'MB1912669XK55817006', '食品添加剂生产许可注销', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9524E47FF4A718DF9A2061538E341261', 'B87CA072459921939B82FF7ECDF833BE', '食盐定点批发企业证书补办', 'MB1912677XKS7985005', '食盐定点批发企业证书补办', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('954B1E141E38FAB1AFCAD469DA615E79', 'F4884B206D1F671D08B7DD76294621F0', '采集国家一级保护野生植物新办审批', '005184558XK4G3EF001', '采集国家一级保护野生植物新办审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('954F6AE847AFD20B18117F5904FBA270', 'CCB3BFF63772B6DBE0970BDD943E5F93', '省税务局税务新闻查询', '41000002SGG44593006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9562EE1645495E3A84C494BB718AD6A7', 'D4BE3C18CBF1ADAB2D68F5AF68316627', '中外合资经营、中外合作经营、外商独资经营企业互联网上网服务营业场所经营单位从事互联网上网服务经营活动审批(最终审核)', 'MB1848628XK78400003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('95710805E6CAC6953C4E0AA499A1062D', 'D86A1F3246F843EDE7A51CCEAC37618C', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动补证', 'MB1848628XK5596100b', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动补证', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9578B8EABABDC0D8093D84BDC375F6C8', 'D823335B562A38F81032B4BB684801CC', '在森林和野生动物类型国家级自然保护区修筑设施延续审批', '005184558XKE424L002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9593414EBEE51AF847790C4011F71E6E', '32F2BBAB2BADC5755A529902086879CE', '铁路公路内河通车通航里程', '41000002SGG43211001', '铁路公路内河通车通航里程', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('959D45832B0060EC058E591681EE88DA', '5D597E722611B3FB06E5468B5746E0DD', '音像出版单位年度出版计划审核', 'YZ0000000GG33184004', '音像出版单位年度出版计划审核', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('95A258C1008C2AF8866FC38CBAD66FE6', 'C6390A8135998641CBC991CEDAABC31F', '工程设计企业资质吸收合并', '005184400XK83829008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('95A8B5359EDD7F902B5F2450C358BB09', '2DB3EB3E06661E0466C4372EA49603D0', '仪器预约查询', '41000002SGG36351001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('95AFC9304B739384CFF85AE0EC956ABA', 'D312AF932FBD03CEFCC5042FE04D2E23', '河南省交通运输厅项目批复查询', '41000002SGG80310002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('95CA8212D7039E5F47B014EF2870D4BA', '9828FBA89BF6247ABA6AC6F3704C6285', '房地产估价机构延续备案', '005184400GG33154009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('95EE37AC1097BB134E650EB91593DCB8', '89617B8D7EBC6D68C366C8641BB1504B', '公共档案查询', '41000002SGG94297001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('95F89271D19AD77F0826CFFB807B47D1', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省婚姻登记处查询', '41000002SGG37554003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('95F8FD7E97FF780FAD1BA3DEF47D84E3', '9EC1DD96551E1F1344DAAEAC22CD3AE0', '在站博士后招收经费申报', '698732712JL1JUYP001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9636A46827FB42908481B2AB04128E6C', '9828FBA89BF6247ABA6AC6F3704C6285', '房地产估价机构设立备案', '005184400GG33154007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('963F7F246459160EFD8466204BC36C68', 'B4A72E5BEF14C9B4F1E01E392CF3E175', '服兵役情况查询', '41000002SGG49516002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('96455C7EF35BD968C3962355047AE810', '5330E67B0D62F249896CDE1AA7F13D06', '小额贷款公司变更股权结构（涉及第一大股东或其他一致行动人股东合计持股变更比例高于第一大股东现有股份）（审核）', '687145571QT00369009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('96561DD29C5248A4F9B2F37F1F307D9F', '146DE1561018FBE802CBE7681D4B4DFE', '文博资讯查询', '41000002SGG06421002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('965FC2EA33F53252457956ACA2A89D49', '6BD654211E42460AAF688E90F9E9AF1E', '社会团体法定代表人变更登记', '005184312XK99203001', '社会团体法定代表人变更登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('966654E0A404E312D10C9BD9564E188F', 'CD5958AF012288AC37C5B389B31FF431', '水上水下活动许可（大型群众性活动、体育比赛）', '005184435QT00314017', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('966AC3B99A56BE75F1F7FC432FBEE472', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）变更企业名称', 'MB1912669XK55721009', '药品经营许可证（批发、零售连锁企业总部）变更企业名称', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('967E36DF8F00749C3BA86CABC4D47546', 'F1FB62F165CD90744E705C53791DE2C7', '全国邮政编码查询', '41000002SGG36819001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('96844AB3269587148FC64E33553DF458', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类医疗器械产品生产地址变更', 'MB1912669XK00227015', '第二类医疗器械产品生产地址变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('96A9A7537B5F974B93CE374B1C7A9EC9', '925C9172780E9B588D9E1F2F51E7A66A', '全国煤炭劳模评选河南省拟劳动模范推荐对象', '41000002SGG12714001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('96C23C450E074497ED9C69D7AC4812A8', '81BE8C2F203F2F01DC2831B0DE6D32CD', '博士后出站办理', '698732712GG33277003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('96DFCC90B3F97F9C08AACF0C7DED8118', '5C0F3090FC83C6F6B51190CBD8B0D9DC', '（404）河南省住房和城乡建设厅人员查询', '41000002SGG16542001', '（404）河南省住房和城乡建设厅人员查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('96F4801A3587B80CFF6FB9720294C696', '849949BB26761485D37DA2AC03E4230A', '危险化学品水路运输集装箱装箱现场检查人员资格认可', '005184435XK55792001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('96F917BC5334C2C5C25D663AD6930839', '852A38EA40E938D4B238DB8A7AC008DA', '医保定点医疗机构地图查询', '41000002SGG78585001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('96FB0D362ACCA0632F24BB485D008C90', 'E380416C825B88B92D090782154B36BD', '定期公布县（市、区）统计调查项目目录', '41000002SGG72445001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('97064FDCA9A9E63425B95B716A5C3567', 'E11A52F69537C09572BA8646CC59D9CF', '省人防办美丽山乡查询', '41000002SGG11684005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('97215585FE406B9BD044535A8AE8A8FE', '91F0148728E0014F4C85CE2BA2DFF90A', '音视频档案', '41000002SGG28837001', '音视频档案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9729132F0951C42D05D507DACF7E1455', 'CE5A25367F2F9B289FCDA757EC55FD6A', '律师查找', '41000002SGG72443001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('972D66E4C6BAA3C3AA05B83224DF7669', 'F8BE49B21E441B707FF59729552F2F12', '河南省发展和改革委员会委发公文', '41000002SGG42653001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9735EB9A696FFBA1CEFD9B23599357E3', '30BE103EBEB8E993E5F4C4CAEB4C372D', '外商投资旅行社注销申请（经营范围中取消旅行社业务）', 'MB1848628XK0019900c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('975161802B7AA1A8700011323AF9A9DA', '2D2977D979F21D76CA3C6A14B4CA3136', '环保公众开放活动 ', '41000002SGG25151001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('977D00369EC80EFAC93EED86790CD081', 'E1F46D1FB8EFAA03807C24ED268BC8BB', '软件出口登记备案', '005184590QT00223001', '软件出口登记备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9781843E6011021F241CEEE8A4F85E7B', '037D88431EFE6E2086D27597DDB58169', '招生档案查询', '41000002SGG35930001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9783BEBAF3E8ACC3520F1B39196668C6', 'A9B66875D2B777E4FA55F9CC88578F88', '河南省科学技术厅政策文件查询', '41000002SGG14935001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9784BFA1E9BACAFDC1019952DCAEED90', '0F31E0604B4E09B951A3EA6C22082316', '国家小型微型企业创业创新示范基地推荐', 'MB0P03925QT00154001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9784E084D9FA5C00CA32EBA8BC5F84AE', '2F5BC9F4EEBE2A48300F3A0E84BFE937', '消毒产品生产企业卫生许可（变更生产类别）', 'MB1957883XK00271007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('97877DEEEBE57BB7A5FEC57D038BAFA9', '35171C3D3F94537DC9AD9EEFB3729D19', '公证服务在线咨询', '41000002SGG50545001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9789A5BDD7F0E7BF4D61882A30FCF881', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）变更法定代表人', 'MB1912669XK55721004', '药品经营许可证（批发、零售连锁企业总部）变更法定代表人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('979C71E5596434F83A79AA74F5D8DFA6', '59F1507234FC5199261624BB60F7B5CC', '小学学校名单查询', '41000002SGG03141002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('97A9FF5160C33F25062EDFF7A32CC30A', '2BCCB134CE6B8239D0CBCB7516D83CA7', '档案中介服务机构初始备案（寄存服务）', '725831987GG33180002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('97AD5282F4D4237BB0C3BAA03606D46D', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证变更法定代表人', 'MB1912669XK55646012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('97D21C699BDCE5141932F2470CFD501A', '1FA3AC3B0CA9BF4A794A97256638C965', '河南省机构编制委员会办公室中国机构检索', '41000002SGG91434001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('97D842CA214895D78A68C44EFDB14814', '57FA0E0B2ACA402AE501919DC56A2D03', '特种设备检验、检测人员资格认定（检测换证）', 'MB1912677XK00084005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('97DA28FD6C0D67C7658979DDEF7ACE87', '60B10F8EA983D9FD0EFAE102BE71D0D2', '旅行社设立许可（租赁场地）', '005184830XK00198001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('97ECCFF26898AE3337127C8D2F690A0E', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省民政厅规范性文件公开查询', '41000002SGG37554021', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('97F0E289BB6256BE46178D8C7413CAD5', '1F3E8080C43AE39DB9B9307DF4CC159C', '特种设备生产单位许可（制造地址注销，且工商营业执照发生变化）', 'MB1912677XK00080018', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('97F15FDB2992EBF385E93B79E6C604E8', 'B80D5F37B8BA1DB6410B2EB02D40FC2A', '首次申请工程勘察资质', '005184400XK20097001', '首次申请工程勘察资质', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('983D95611468E6F91F910740D294FA5A', 'E145B3139E1F3DD6BA6D96177D981FDE', '社会保障卡非卡面信息变更', '698732712GG88123002', '社会保障卡非卡面信息变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9867EEC8B6DA82D0C0EB60CF24FB46BE', 'ECD4D9D98D1C2589FD3959BE740C3AE4', ' 孤儿基本生活最低养育标准制定和调整', '41000002SGG89985001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('98736991B581579A95B61DF1EE98A7C6', 'ACBED3B4D3E2B736A3B4C5B20E9658CD', '设区的市、县级地方新闻单位的信息网络传播视听节目许可证核发', '005184654QT0028700e', '设区的市、县级地方新闻单位的信息网络传播视听节目许可证核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('988C43543CD404F3A9CEFB30EE8CD79A', '63294946DA2972E30B67ED3044A808DB', '森林防火期内进入森林防火区进行实弹演习活动审批', '005184558XK00186001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9896A3642D749BF7C4F4B50E8FDE5CB3', '782F4D72D5DC71C8EDB7416446E63A95', '河南省教育厅 关于下达2020年河南省高等学校哲学社会科学应用研究重大项目立项计划的通知', '41000002SGG60407001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('98A0EFAF2A1C109BDD07BF28E9395CDE', '56FEEBADEA7EC0A72B6499E2EB2DBA53', '涉黑涉恶税收违法线索在线举报查询', '41000002SGG67186003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('98AC8415CE9C8BD55D91EFC329649EF7', 'AA197BFC77AB73B785C0BE0CCA4E2404', '取水许可变更（经营信息变更）', '005184566XK00241006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('98BBC8D4F64C2BD15AC4449D29ED0E99', '8CFB5F60818103845760C1B7E488713F', '勘查许可证补发', '005184224GG28709001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('98CD09F11345B89AD71864FCD2831EAE', 'C5018DF030AD133CCFC5C2FCA0468044', '河南省市场监督管理局公告通知', '41000001SGG89294007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('98CD5E7904D6953AA39A4880D2CDB541', '45FA45E780B387B61B2D8F51EFA326DA', '权限内国家重点保护陆生野生动物人工繁育许可证核发（变更法人或其他）', '005184558XK99852004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('98DA5545546C0D42D86FF712BB61586D', 'B80D5F37B8BA1DB6410B2EB02D40FC2A', '延续工程勘察资质', '005184400XK20097005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('98F84E2F35AD17A0B65897106C05F426', '45178A0278017908E60243E32117470A', '省体育局宣传教育专栏查询', '41000002SGG92882005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9903F9261CD0B987FD6338A917A7EC37', '0DD90E46D187BD7A287F7B2FF6982D17', '关于对拟推荐参加全国职业院校技能大赛教学能力比赛（高职组）名单进行公示的通知', '41000002SGG94078002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('99087EABA3EAFD280FAD8FFF5EA71A56', '58B8696831A7C6EC5E41430A925CC2F8', '从事经营性互联网文化活动变更（经营范围）', 'MB1848628XK02935002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('991D18D9AF7086DC5BF393D7C32EA267', '7832E03BF9B5EB5ABF7BD20FA37A1DCF', '档案中介服务机构备案注销', '725831987QT7343N001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('993EEDC55591B307D2F14179E5E89F54', '2FC4F9538B60085169CC404D7F4B591B', '河南省市场监督管理局质检知识产权政务服务', '41000002SGG11969004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('994FC789C4803DB4E0E810804D411669', '788711B922B638559783205F0016331A', '在自贸试验区内的中外合资、中外合作经营、外资经营的演出经纪机构申请从事营业性演出经营活动审批', 'MB1848628XK55922001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9960B3A58198BC8DD27A1CD3B3553291', '40790BB825D311B9F0BFF2D70C848519', '母婴保健技术服务执业许可证遗失或损毁补办', 'MB1957883GG80587001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9967AE0C996E3E9C1816C8B91DF0D7AD', '35ED4FFDA352F9225E16A3A24DC95730', '第二类精神药品制剂生产计划和第二类精神药品原料药需用计划备案', 'MB1912669GG33185001', '第二类精神药品制剂生产计划和第二类精神药品原料药需用计划备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9980F26D4319FF60920B42138AA6305E', '011AE2565F1F6D20757E8C1AF8D8471E', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出经纪机构从事营业性演出经营活动变更（名称）', 'MB1848628XK1228500a', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出经纪机构从事营业性演出经营活动变更（名称）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('998F48972578CE454ED275E3D42F1A39', '1EFFF9875D74B9524A813BC8178AC7F3', '依申请变更涉外调查机构资格认定', '005184355XK00076003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('99994805C27583CBDF920406928E8015', '66166E4E889FF9A7822DB5BC40DFA025', '水利工程质量检测单位乙级资质延续（量测）', '005184566XK9726800a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('99A83C60AF71B69C4D6F4E1F90B670B7', '52BF798818215E1CC21FB0D198934DE5', '煤矿企业安全生产许可证变更（变更企业名称、经济类型、隶属关系）', 'MB0P03925XK25968003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('99B1926E95B9CB027EAD2700EB8B2902', '421B2DD82AEECE777C41619551830FDF', '临床基因扩增检验实验室登记', 'MB1957883ZS00004001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('99C05924B0CE23E5438129E2655F099C', 'EA4BEC0C048E84D4F70996A55BF69EF0', '新型冠状病毒肺炎河南确诊病例轨迹查询', '41000002SGG55816002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('99C4CB26786A75ED8D39C82BA87C7976', '7F868DF4FB918A94185489960D1F0274', '河南省卫生健康委员会药管平台', '41000002SGG13053001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('99D066C8D3E314B868746CF508E6A1AE', 'BE80C6B10253ECB2E3CA1150ACE4FD6B', '演出经纪机构从事营业性演出经营活动变更(除变更演出经纪人员)', 'MB1848628XK55977006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('99E03B1EE3E82AE26930D92E3AC1CC7C', '93D4C4DB8BDE89F4D40DE7B9ECCE70CE', '单采血浆站执业延续', 'MB1957883XK00266002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('99E1168A42DF2DCB4350018A3C1BAFDF', '42EFB01138010A18BD2DC8EBCE0CEBE5', '乙级测绘资质注销申请', '005184224XK00329015', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('99F44BEDA8EC8041C2B51EB5E646FC0A', '4AC50ACCE46FE88D4BF8491704E3434F', '饮水安全评价准则', '41000002SGG83243001', '饮水安全评价准则', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9A09EC68DCBD65D78E6DADFB4BCA1D17', 'BFB89262A855E2305396FE7187950AAD', '车用气瓶充装单位许可（地址搬迁，且工商营业执照未发生变化）', 'MB1912677XK5565300d', '车用气瓶充装单位许可（地址搬迁，且工商营业执照未发生变化）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9A123C9EDF3CF3822ED8F7FBDA65A826', '5D597E722611B3FB06E5468B5746E0DD', '图书出版单位年度出版计划审核', 'YZ0000000GG33184001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9A320C014FFDFA658769C196400F847A', '4F10630809F3E5317BB56D5D7CBBBC5D', '牵头组织对外资研发中心采购设备免、退税资格进行审核认定', '005184590QR83936001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9A36E8158449BF66209616B1C0CF741A', '91D9E5E11B5C473F0BCACE330C353258', '河南省卫生健康委员会公开工作报告信息查询', '41000002SGG63521006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9A3916C2BE9233F747E8B201A138F37E', 'F3A5EFFE79212CE56FFD5325A49BB511', '河南省中小企业公共服务平台项目合作资讯查询', '41000002SGG68056003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9A4DEB0DEB41147109F15392C655FDE3', '336C716771BBE39EAB5E27CF37A5ADE5', '药品经营许可证（批发、零售连锁企业总部）变更质量负责人', 'MB1912677XK39447006', '药品经营许可证（批发、零售连锁企业总部）变更质量负责人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9A71423D8F86019A5ECA724097BCFA07', '7474E42EDBD949F33DC528C90255FCDD', '社会艺术水平考级机构资格证', '005184670XK00102001', '社会艺术水平考级机构资格证', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9A7D950B5BD503BC44242CB72E14F63E', '571C4464AB22C2DC5804669B711284F1', '总投资在5000万元以下的内资项目进口设备免税确认', '005185008QR12998001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9A7F21D0635E994A24B729AF4C31590C', '0CA77513AD08FC7FFD1D6E32C8BCB707', '省外机关事业单位基本养老保险转入省内机关事业单位养老保险', '698732712GG36356002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9A9C6D289036F6A64ED4BE2476DC550A', '19C331B9938E526E3DE41720DFD92522', '公益基金项目查询', '41000001SGG87861002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9AA262CF5A6329C10226D0888C3D56A2', 'C5018DF030AD133CCFC5C2FCA0468044', '放心消费活动信息查询', '41000001SGG89294022', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9AA4EF6DB35E34D242E665FC451A030C', '5E900F05C0942195A7195E909BF3642F', '草种进出口审批', '005184558XK13543001', '草种进出口审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9ABA591979F27E490615B11C80284716', '681810843E3A1A4828F42B92ECA1CB66', '河南省住院医师规范化培训报名', '41000002SGG36539001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9ACA77F123F5455D380B00A45546CB36', '9EE04833796F04A03C545C4B8F5AD6F8', '测绘任务备案', '415800560QT00323001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9AF4156EA2B44FB85FA8843FC4A340B9', '6B8B188416E63F6484A3D9BBB34C5D93', '省煤监安全要闻查询', '41000002SGG53313006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B0B12289C0DEE44A4CE24B8CE49512E', 'C3224562F7953E76964F7D0D45642FF5', '空气质量预报', '41000002SGG34704001', '空气质量预报', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B0BB6D7DBB5CF75F93A265546589042', 'EFA175BBF686C624B24D011A5BB98B06', '查封登记信息', '41000002SGG97331004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B178A02B83042A80D9F412F7923D809', '9828FBA89BF6247ABA6AC6F3704C6285', '房地产估价机构变更备案', '005184400GG3315400a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B17B1CFECAD5977893F101D8B7E444D', '41C46B498D17D01FB6896C55895191F2', '猎捕国家二级保护水生野生动物审批', '005184531XK35160001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B3AAF624797141B6572CD9B3DE63BBD', 'EFC54A32DF7246CD23B059BEB930122A', '举办香港特别行政区、澳门特别行政区的文艺表演团体、个人参加的营业性演出变更（演员、含未成年）', 'MB1848628XK5596200a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B3EDD1E844D12964D2DAC22DA989285', 'E7B234C807C51669078A1C3BB3463566', '权限内利用国家重点保护陆生野生动物其它活体出售审批', '005184558XK68302003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B3FACBA8977DF63845B3733A21F973B', '5C38996401ADC49FD0F6A4591EF13A80', '医师资格准入（香港特别行政区医师获得内地医师资格认定）', 'MB1957883XK55861004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B401E87E40B154245F8DD62874117BB', 'E0AFB9F2A6088F3C5B92BC3B78F2BF24', '司法鉴定人变更登记（减少业务类别）审批', '005184291XK06476013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B5051A4671350D8BC38DAB16068830D', '002447207831DA53248577890AA4AD3B', '文物保护工程勘察设计乙级证书经济性质变更', '005184689XK94270023', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B584B4B689BB2DD415C083ACA1E5A0E', '1786DCDA846BB5BAD475C9FC2F9E5426', '河南省残联残疾人证服务查询', '41000002SGG57083001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B58ED58C3407D0AF44C40136BDA0D95', 'EFE580E8D561D8EB8AFE24DACCD6D336', '饲料添加剂生产企业（续展）', '005184531XK40671009', '饲料添加剂生产企业（续展）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B6F9C7115B6479D63590E19C776BD10', 'CDFE730AF159A98A742B8B4DEB9BC329', '道路救助基金的政策法规', '41000002SGG76029001', '道路救助基金的政策法规', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B770E5D49BBEA2C2607733478BE4D81', '210868935EBAD2BCD4821021BEBF9656', '检验检测机构资质认定地址名称变更', 'MB1912677XK55650007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B781845E36A46722E50FA5D051B0FD7', 'B6D9035650D1A59BC6E31C4F6D2253B4', '从事出版物印刷经营活动企业变更经营场所审批（工作场所为租赁性质）', 'YZ0000000XK55728006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B918CF1578643FCA63947D01480F180', 'C5018DF030AD133CCFC5C2FCA0468044', '河南省食品小作坊申请事项登记信息公示', '41000001SGG89294014', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B99A6D0BF1960824117061B22896858', 'DCFE6CCEFAE881203985CCDD84660114', '举办区域性广播电视节目交流、交易活动审批', '005184654XK55707001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9B9C1FBC310A77089D4DFB1B12D4147E', '9828FBA89BF6247ABA6AC6F3704C6285', '房地产估价机构等级备案', '005184400GG33154008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9BAF5C5B1391AA701CA9F3775670225D', 'A9A5D3835850BAF683CEA791F5D3A76B', '医疗单位使用放射性药品许可证许可类别变更', 'MB1912669XK00225004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9BB708D83EE1C675040853B3501D3EDD', '2C4F18F5A54B0264EAAC6E6013E00F49', '民办非企业单位业务主管单位变更登记', '005184312XK05267004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9BCD6B9A2705F9E329A693CF4487006E', '427FF13211607224B1305D6FEEDECA06', '河南省网上农展馆专题展会查询', '41000002SGG05790001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9BD36A98FCFC7D5F931751483612CFDC', '4732C84FD8B151131BAAD1A170D62E95', '死亡医学证明办理', '41000002SGG40965001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9BDFA01BF428C3549DE876C40D2F4BBB', 'B03D342696AA095874AAF674B63413C4', '省通信行业政策法规咨询', '41000002SGG58558001', '省通信行业政策法规咨询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9BE1707B280C5C97A30DCA783A453856', 'BC1F5F2F79DF8FF4A998171A28EEA238', '无线电频率使用许可（延续）', 'MB0P03925XK58764002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9BE977208D9FB1B9DBEC311BA1841B9E', '6373E3A19D4EBDBFE4C740C04C09C5DB', '麻醉药品和精神药品生产企业审批', 'MB1912669XK55750003', '麻醉药品和精神药品生产企业审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9BED25FCB7A173A584CFAA253F3E6294', 'CB6CE6EF4381602C1A69014D6B001DA1', '音像制作单位设立、变更名称、兼并、合并、分立审批（工作场所为单位自有产权）', 'YZ0000000XK25874003', '音像制作单位设立、变更名称、兼并、合并、分立审批（工作场所为单位自有产权）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9BFC1D5721058AE53FB4DD480A96D3C1', '1DFF73FBDC01A2CC91A473019462168C', '普通高中学生毕业证书查询', '41000002SGG12889001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9BFCC2B2EFAB077D66413A6C8586811D', 'C239AECCD2715DC1FEBCE8633556D338', '到期换发《危险废物经营许可证》', '005185040XK00173008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9C12605D6A2C56E4BE5E0D64FF513EE6', 'E0B563387D49C44A4CAB633E9A290042', '增值电信业务经营许可证延期经营', '726991560XK07809005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9C36F37286860D03367B2DB991AA8CF6', 'ACBED3B4D3E2B736A3B4C5B20E9658CD', '信息网络传播视听节目许可证变更开办单位名称（初审）', '005184654QT0028700b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9C4120DCCFA3310504480683A9F5BF72', 'CEFB63BF521FB46DE00CD01EEB88870A', '计量器具型式批准申请', 'MB1912677XK00081003', '计量器具型式批准申请', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9C4378128107158D985A16FB002D99C9', '8AEEEDDE4DE0CDD1069228F5B20C345C', '河南省客运停靠站点查询', '41000002SGG57878001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9C52967DCB3B7418DB05C0BB846AC185', 'AEEB6041E5E91A07A64E9BB3F7E39D38', '国家税务总局河南省税务局通知公告', '41000002SGG01472001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9C5B8DB7D0FF95B90F0C3B50D36467F6', 'E9DD82F9CE79C3412E0B12916E7121FE', '河南省残联法律维权业务查询', '41000002SGG21854003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9C7D4645EE3BDF8312C2D114D672E2FA', 'FFD97505ADFAD0C049489BCFD0AB92AB', '常住户户口类别查询', '41000002SGG41401001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9C8BCCE9818ACC58BA6CE270E6AF53DD', '05956703C7C12294B6C005328D2DF29A', '河南省煤炭行业大工匠创新工作室拟认定名单', '41000002SGG67280001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9C91017D3478B23E4C999C14293BBE4D', 'D9F89BDDD8C5046DFC8BCA91E397F580', '地质灾害治理工程监理单位乙级资质认定（新设）', '005184224XK5392400b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9C9506F4B7E29751C6EF0B50A837A45E', '201F1F734B91A18A70951E4B48FFBC1E', '全国性批发企业向取得麻醉药品和第一类精神药品使用资格的医疗机构销售麻醉药品和第一类精神药品审批', 'MB1912669XK55782001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9C9839EF19A83FA8BB9B161D38A0A04E', '245DB85954A481B9E679463356E6F4D5', '公证服务机构查找', '41000002SGG79914001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9CA4C147E9809EBCC25ED8F6C09475B5', 'E8BB58B6699EAAE999EB79B5E60C8E57', '河南省交通材料沥青主要产品分析报告查询', '41000002SGG01680002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9CC4CB18D64D02F929EC5B73F45C0988', '2D1055872C9544CF086F84D38DEBB357', '一类超限运输车辆在省范围内跨设区的市行驶高速公路许可', '005184435XK00322011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9CC53DB9A3FA071BA5FDA96C9A1DA786', 'C5018DF030AD133CCFC5C2FCA0468044', '河南省名称库查询', '41000001SGG89294018', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9CC624CC855351A1DE1078EA8291235C', '98C42FBE09181CF04311A76D244606BF', '年度河南工程技术学校公开招聘工作人员计划一览表', '41000002SGG09041001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9CD6DF7BEDC2073596ACFD0E085B2034', '45178A0278017908E60243E32117470A', '省体育局“不忘初心、牢记使命”主题教育查询', '41000002SGG92882007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9CD752CC4AA794AC380871D332A36601', 'C5018DF030AD133CCFC5C2FCA0468044', '市场主体登记管理档案查询', 'MB1912677GG89294001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9CF3F7427AE71B735A79C1DFE8D228C4', 'A36EE644B0E9E910C7988E07F3A5DBE7', '设立外商独资包装装潢印刷企业审批（工作场所为单位自有产权）', 'YZ0000000XK55759004', '设立外商独资包装装潢印刷企业审批（工作场所为单位自有产权）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9D133D96F1CCBCF3BBBE5CF32163A834', '175C971222104069558624D3087E5397', '广播电视节目制作经营单位设立审批（变更法定代表人）', '005184654XK0029900a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9D21F6009F9D4874A32ED38049119682', 'D034003C07A34F0C4B94306E4C1E5813', '地震局政务公开', '41000002SGG71294001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9D2769E82D723A57FB2FB04CEEDC4B47', 'D86A1F3246F843EDE7A51CCEAC37618C', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动延续', 'MB1848628XK55961013', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动延续', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9D46A0F0D5F36707DAFC408F5ACC2635', 'F59276FDAA163B062F2A9F5474AACBF1', '广播电视视频点播业务许可证（乙种）审批（延续）', '005184654XK55698006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9D88A1ECF91250ED17C2A4C1360DCD87', '51EF707AC968F72DAC6A3451C7821730', '婚姻状况查询', '41000002SGG67769001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9DA964A798651F3A4AB8EA20DEA4B509', '089A1EA0E57B1B7A3E5393BC692FD658', '文书模板下载', '41000002SGG40647001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9DBF0FE656029E50227A3B191FEDE273', '48CE142F00FBA0E19590703B4CF1252C', '兽药经营许可证核发（变更）', '005184531XK29094002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9DC743E5C37C2B27B8E6DCA952986A85', '54E2C68F7845CDE9E82835698EDD90BE', '实验动物生产许可（注销）', '005184638XK00104004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9DD5F7EF5615BBD6B7DBE87D4870C8F5', '63294946DA2972E30B67ED3044A808DB', '森林防火期内进入森林防火区进行爆破活动审批', '005184558XK00186002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9DEBD672BA10B1F5AB60EC4A253A3954', '2D519B6E86950C2986F4ED447A316449', '河南省交通运输厅在线访谈预告查询', '41000002SGG41253001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9DEEA274808512739A60E24E11835D06', '68EA39E42D92910CD36BD8F1E86BF594', '游戏游艺设备内容审核', '005184670QT00048001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9DF12D99B333762C06261CA785FBF5D9', '412590880E3F8374E98BFFB7A86800A7', '省残联政务公开', '41000002SGG70651001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E0E75D3FB3C047372A19C0283AF790C', 'C5018DF030AD133CCFC5C2FCA0468044', '河南省市场监督管理局行政许可查询', '41000001SGG89294011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E0FAF65332D353D362BAA26F3DEA758', 'DFFFF113EA827AA981D01815E3B4898B', '船员适任职务证书核发', '41000002SGG12875001', '船员适任职务证书核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E155A14E4492C339B025C389C85F3AE', '6D6758C1AA0FD418A15A6358046233AE', '生猪等价格预警信息发布', '41000002SGG95815001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E2B9F8E3A6EEF35E4AF319A6BE9E0E1', '94AC6D5E96D52737CA6C436B148E80C5', '互联网网站公安机关联网备案查询', '41000002SGG68660001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E3105043C59335797B1FE162D3C94E5', '8D2652D1A67643E5129EC2A600F41DCC', '居民身份证号码查询', '41000002SGG00596002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E32AFFFED8F779387983CA594DD91F0', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品生产许可证证书延续（符合延续条件，涉及产业政策，需实地核查）', 'MB1912677XK5565100b', '重要工业产品生产许可证证书延续（符合延续条件，涉及产业政策，需实地核查）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E343941BE7BEAA7121C9B96302B4914', '8FD5DF3CA5DAD4F05F819A711E5BA765', '社会保障卡解挂', '698732712GG06930002', '社会保障卡解挂', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E496437D82269CF1BFA04A3A19FFCDA', 'C4951F6BDA98FD3B508BF1EFCD1B2F30', '出卖国家所有档案的复制件的审批', '725831987XK53912004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E53EA225ABA08B6A4D7F9966DC3DA6A', 'EF64454331616DE66C881EFF59E4A4B4', '普通高中学校名单', '41000002SGG66353002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E55BBD3E949F01BEFEA4AC29673965E', '5330E67B0D62F249896CDE1AA7F13D06', '小额贷款公司跨县（市、区）行政区域变更公司住所（审核）', '687145571QT00369006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E738FBE19E2A313FFECCFF010325F1C', 'FB5D2BFE593C144DAEED487C13AAB79F', '食品生产许可延续（省级）', 'MB1912677XK06655003', '食品生产许可延续（省级）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E96E5A9342DD81A0B6C48CCA8A39EDF', '4C89A468ADB43DB226D2102E5D18D8CE', '在草原上修建直接为草原保护的工程设施审批', '005184558XK84021003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9E9C5845227B7CC9467DEC8ADA77B39B', 'A08CD94CA4D9A9EDC55F7D2B7884DF10', '河南省安全生产监督管理局许可信息査询', '41000002SGG57325001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9EA719846A2C32B88E2DB1BE7E69F795', '46EA59D5711A8C6351AE575174FC1A69', '因修建通信建设工程占用、挖掘公路使高速公路改线审批', '005184435XK0032002e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9EF3A5A66D35D4F63D0BDDAC1D0F73AE', '8032A1E258BFE02FE09F5B7B0527033A', '依法申请公开', '41000002SGG49446001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9EFFDDCD962AF20407FAF77D7636ED43', 'C14384DC9E924CEC88299EB396696DC0', '河南省具有普通高等学历教育招生资格的高等学校名单', '41000002SGG03275001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9F3B5E81340219F5E479654E59A07B56', 'D3B381B38C78593FAA565B2BA3A27F61', '河南省益农信息社业务态图', '41000002SGG50719002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9F3EFCDD1121DA3BAAAB75C2AE0A6FEA', 'BEA22334528884915512BB8B81D5BBC0', '医疗机构名称裁定', 'MB1957883CJ00151001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9F427AEDB96F42432889BB7F206201C1', 'DBC3D7CFE77564A60F87FBFC49798AF1', '河南省事管局市县动态查询', '41000002SGG74847002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9F43D4C1419C8E6B7A7FBF378A62AF6C', '43471A5A469C43703EE2EEECFA562FA3', '今日水情', '41000002SGG64273001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9F5E53C2A0BDD86A27FA9B69E81B6847', '0E28B8DEAFA754AD9D90E039BE074D41', '河南省自创区创新引领型产业集群专项管理', '005184638GG9635S002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9F625794F42C71CE5F969A87BE5B484F', '6E35D017C60ADC3ECF85E33B805C03BB', '人力资源服务许可审批', '698732712XK00125001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9F6E758B2648652DC858756A139BCFA2', 'C1D82BB486BF887D5C3B1C65ED902420', '河南省林业局信息公开', '41000002SGG98738001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9F72A68C3C4BF325BAF46AB9405707B6', 'BC8280A5C488A817564C78B9250C2AF7', '重大税收违法失信案件查询', '41000002SGG85874001', '重大税收违法失信案件查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9F7C8565F0ED8DB1A47546283810131B', 'BE2CE42A8677E3021A403BB593C73667', ' 法人和其他组织信用信息查询', '41000002SGG41202001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9FA01D17BF87B771CF0377B4D3AE1FCF', 'A9A5D3835850BAF683CEA791F5D3A76B', '医疗单位使用放射性药品许可证非许可类别变更', 'MB1912669XK00225006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9FB87992A85B6B757F232431AD5FEB47', '26E393F10294ADA5F113B833A8857126', '放射卫生技术服务机构资质（变更地址）', 'MB1957883XK00272007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9FDD8DDA21328B37ECDA0342BDA69CE6', '3783715975A16011F91FC84DC7308A4B', '指导、组织、开展山东省全民健身运动会等全民健身活动', '41000002SGG23484001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9FE413FC803F497821C4BACE6542C559', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动变更（地址-租赁场地）', 'MB1848628XK00096018', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9FE663D24BB88B2BBB13A275891ADC84', '4C2CA143BDBFA85210AF37E965D52C26', '公司设立登记', 'MB1912677XK0013900a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('9FF196BBCE50D623DC2D4D01CE4D49E1', '05CD0808DEF4F6ACA254272FC4D378CE', '省农业农村厅信息公开', '41000002SGG89149001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A002854F0010271EDA8EC362F509BDB7', 'C029CE65A0173EDAAC2CB0D83A1F65CB', '河南省建筑市场监管公共服务平台-从业人员信息查询', '41000001SGG71052001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A01D23CE51779FE02336A721DDA34B2D', 'E186C53DB642BC17D53A1923F9723901', '第二类监控化学品经营许可', 'MB0P03925XK55638001', '第二类监控化学品经营许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A028D2D44E1898A8435EA3C90DFEE23F', '91D9E5E11B5C473F0BCACE330C353258', '河南省卫生健康委员会财政预算、决算报告信息查询', '41000002SGG63521001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A03A1E46900AD9F3D9D0BA2EBA054F18', 'FDA879034139553CFE158E2A744C84F6', '河南省中小企业公共服务平台创业故事查询', '41000002SGG83510002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A0449481AA34274C1819CD81DCDF594F', 'A3FF2E23263B29DAB594A0C53D9542BF', '利用普通公路隧道铺设电缆设施许可', '005184435XK55757005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A0503FD77252AE726A9FC8636F26C72C', 'DB62AB169EE5335FDAB9079EC0C6F27B', '河南省图书馆交通指南查询', '41000002SGG09355001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A055AC71BE227BF5633267EC0C5C9AB5', '6A3688B471E53726F2C2FDAF3E7868B1', '公证员执业机构变更核准（省级终审）（跨省）', '005184291QT00256007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A06DDB2F6430FC804F37866B98F35345', 'E27919616185837BE8B39737E4FA5F1B', '施工图审查机构升级', '005184400QR73809004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A072F67926C4ACF226775528AC9459D4', '10DA917A368A6FF10781F842C0D17D0B', '个人基本信息变更（企业）（民族、性别、户口所在地、户口所在地所属行政区代码、户口簿编号、户口性质变更）', '698732712GG3322700e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A084E61B2FFE95A0BCB7614C44875FCF', 'C7308CB7DAC38CFAD111A95DBF5211A0', '测绘局规划计划', '41000001SGG59749001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A09675EB220C07EF7F572E245E537C4D', '016D6FEF2A88EA957174ED0128EED5F3', '报废机动车回收拆解企业资质首次申请（含迁建）', '005184590QT00209007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A0B09C85EA7E2579F96F90EDC91BEA31', 'FC4EC99FA09E7634B95F567D565B8689', '河南省工程勘察设计资质申报服务', '41000002SGG04105001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A0BBC68FC98998D18284A59E627BFEEC', '64A10ADBFAEE343C9DBFE630AC794542', '全国生猪运输车辆信息备案查询', '41000002SGG01724002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A0D6DF28E14E2BD2B6313F29D8BAFB6C', '3A0A40F463D4AAF3AE497C925856E48F', '经常外汇管理', '41000002SGG73574001', '经常外汇管理', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A0E3854618C89C21E35BAB21A634E052', '3BDAB30133A9D418486952DA02E918BA', '终止社会保险关系(企业) — 重复参保', '698732712GG33231008', '终止社会保险关系(企业) — 重复参保', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A0ED687751FFCE98DCA9ED29148DF8CD', '221A89EE740F42EC59555E0B7E6C560D', '（404）二级建造师临时执业证书查询', '41000002SGG78860001', '（404）二级建造师临时执业证书查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A120EFCEE717527A33FCCB486CA756C6', '1D6F4ED04B44FD8C22C6E509F7B53314', '河南省中小企业公共服务平台纺织服装行业资讯查询', '41000002SGG69935004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A136C0750E9512FCFA1F7B1F17BC299C', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证变更制剂室负责人', 'MB1912669XK55646007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A136FDC0B36127C88963B98D115E3F41', 'FE3E194376806EAA75867FC750E311E4', '甲级测绘资质新增专业范围', '005184224QT0804A006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A137AC4B42DDA8AED164F2AB34DD9E00', 'ED1AB790D3AC960E00A9D7834C16EC74', '水运机电工程专项监理企业资质认定补办申请', '005184435XK55677010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A1597D387C767D3C7EED95723D47D970', '6AF4666BB6DBA9748BA281B6F176633D', '生产建设项目水土保持方案审批', '005184566XK00251002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A15D431FEBA1D4A6E0F9C745EB20D2B2', 'FC5ECD587758F5B1653383902F676647', '河南省创新龙头企业培育', '005184638GG06346002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A17E7838E98FBC857DF03CB215FE5497', '412B797CC18843BB6AF78A8CEFEE4003', '河南省医疗保障局信息公开', '41000002SGG82260001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A1A503A332F292D2DA996E27388ABBC9', '66166E4E889FF9A7822DB5BC40DFA025', '水利工程质量检测单位乙级资质延续（金属结构）', '005184566XK97268009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A1C33B985CD951C8C153A35C9D39317B', 'C1D82BB486BF887D5C3B1C65ED902420', '河南省林业局法律法规查询', '41000002SGG98738002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A1D6DAB75AB48D78CA4D5E47D9B8610E', 'CEFB63BF521FB46DE00CD01EEB88870A', '计量器具型式批准申请（跨省通办）', 'MB1912677XK00081004', '计量器具型式批准申请（跨省通办）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A1E205A4FF3530E5271717CEDA851065', '7D8066CEED985AF8568149F00AFD6FD4', '探矿权转采矿权储量评审备案（省级发证）', '005184224QT00168007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A20CE8ED8175234432B3C844C67403BF', '64233151D76C0AD7ABA6213E0E8A82E9', '基金会住所变更登记（凭租赁协议办理）', '005184312XK00206002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A2369F8590383806B1689FDE652C10E5', 'F7A5887DB87062DE74D6FDB9C6A0A6A8', '户成员关系查询', '41000002SGG77568001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A23C2BF7360AF133330DC3636213BFC7', '4817D97AD22A2C80987C339C69897837', '航行警告查询', '41000002SGG15423003', '航行警告查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A27C2CD7B6C6343A620A2F7D5DC61E6B', '12DB14BFE1144414F69EFAE72D12DF3D', ' 危险货运企业资质查询', '41000002SGG41431001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A27DE526A77CD9B7E49C00BCC1BCDF89', '4D6D57AB59E8AA6BCC62729D912FFC0A', '河南省高速路信息查询', '41000002SGG98592001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A2894735C2B0D51B7042672F76B1BD4B', 'EAE70C4B60E47356570496E5C253E161', '在自贸试验区内的中外合资、中外合作经营、外商独资经营的演出场所经营单位申请从事营业性演出经营活动换证', 'MB1848628XK55968007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A28EF6B5F2E9EC162162D01B8F67CCC5', 'F9F6C2B5D1572005269DA71358583CBF', '特种设备检验、检测机构核准（许可级别改变）', 'MB1912677XK55667009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A291E2C9CEDE9DF97B5B07D537417A6B', '3CCFA29BF20B77E25B2C73BC6E18ECDA', '电视剧制作许可证（乙种）核发审批（变更）', '005184654QT23678004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A29B702FC2BE0312E891FFA4C237CB6A', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省救助管理机构24小时值班电话表查询', '41000002SGG37554012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A2A793ACCE6E2D999CF1411EC551C1A8', '9C5660B437D2FE454589B19C627805B3', '电信网码号资源变更审批', '726991560XK95552002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A2B17F14B9ECCE26E6E6CC2021DC201C', '61E319C3185C459C9E95C89FB3479D16', '河南省创新示范专项立项', '005184638GG7145S001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A2BB5236144C5AC19FA498B7FE88C062', '2E3B535C96A32C6D6B98C9C364E99205', '博士后科研流动站申报', '698732712GG33276004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A2C57449979319D6E7E08ECACB1F7E0C', '2C7E2FB43C4BD12EA403007217A8F8EC', '河南省中小企业公共服务平台养老保险资讯查询', '41000002SGG55038004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A2DC5D9E2EF423F3766DFC83A349ABD2', '18E283C3B876FE185CF72FB62631F9E1', '测绘局财政公开', '41000002SGG96456001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A304390CD82B5A81606F5F3E2BE1F5CA', '146DE1561018FBE802CBE7681D4B4DFE', '“寻塔记”系列报道查询', '41000002SGG06421010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A32C2603287A23C9B615C737A108C43B', '10D033BF03C9082E73A6612DEEBDA889', '第一批河南省首台套重大技术装备认定产品名单', '41000002SGG31532001', '第一批河南省首台套重大技术装备认定产品名单', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A32E70D8A232806E07B34CAF236F5CA5', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证变更医疗机构类别', 'MB1912669XK55646010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A33851FEA3CC726CE19FDDFB2329C742', '1091C25D68618ACC9CA66C9ECAC83390', '城镇企业职工基本养老保险关系转出', '698732712GG08079004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A347EF3A3C7994E50FE06EDD273E0B22', 'C72FBC2C23F10325F9CD8736B71C7B3D', '食用菌菌种进出口审批', '005184531XK99385002', '食用菌菌种进出口审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A36F88F3D593081E1E508D99FBAC4AE0', '145DDF1D41CD02D35186B75105DDD046', '除电力、通信以外的防雷装置检测单位资质的跨省迁入', '415800448XK16357007', '除电力、通信以外的防雷装置检测单位资质的跨省迁入', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A3D6AF96430BB9771D45D255229B5367', '27DEF9D1E09B677EE899D5BB11DD0E37', '建筑施工特种作业人员资格证书补办', '005184400XK55735005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A3DA11ADD67C1E0A2B7B46D68281402C', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省民政厅养老服务信息查询', '41000002SGG37554024', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A3DF759FDB9680E268B0E5ABFF84E7AC', '45178A0278017908E60243E32117470A', '河南体育手机报', '41000002SGG92882011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A3F30567CB3AB2AC37B256C02EF737C8', '2C7E2FB43C4BD12EA403007217A8F8EC', '河南省中小企业公共服务平台劳动保障法制查询', '41000002SGG55038001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A3F8FC725E28094A85AE12B58D3ED5E8', '501295D5907D7C52DE215A798E4AF017', '馆藏二、三级文物及不可移动文物的单体文物的复制审批', '005184689XK67165003', '馆藏二、三级文物及不可移动文物的单体文物的复制审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A3FC833FDCB557EF0AA3781FB304D623', 'CAFFF3522503E54A49B1B0D359973B27', '资产评估机构执行事务合伙人或者法定代表人变更备案', '005184603XK55636006', '资产评估机构执行事务合伙人或者法定代表人变更备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A40267F3D2D0456053E2CCAB92117119', '204AB5BD56B912BE73413DC4FA6127E3', '河南省公共交通查询', '41000002SGG52807001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A40C4C9B671698CA8C8AA7F5CB57EAF1', 'D4BE3C18CBF1ADAB2D68F5AF68316627', '中外合资经营、中外合作经营、外商独资经营企业互联网上网服务营业场所经营单位从事互联网上网服务经营活动补证', 'MB1848628XK7840000b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A43BB38D5B7FFAA7ACB1BD46BAAD8F3A', '8FF8DE28437025F56B07F851B60B59D4', '河南省图书馆馆藏文献资源及服务查询', '41000002SGG29671003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A469BA7F73B2675039EB67635527267C', 'C5018DF030AD133CCFC5C2FCA0468044', '河南省市场监督管理局信息公开年报查询', '41000001SGG89294012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A47E156F1CB5BC136C9F80DD2C881193', '2655CF5EA72A1B0BBD22FF6BF8C77460', '河南省应急管理厅信息系统服务查询', '41000002SGG98652001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A48985A74EE8DE52785C5C7643277F66', '76A27C3FAB51112E394E34FB5104976F', '快递企业法人变更', '717817421XK55671007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A4A7AF14E66922124184D730D3A43900', 'C6390A8135998641CBC991CEDAABC31F', '工程设计企业资质证书增补', '005184400XK8382900a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A4A889E2EE64099ACECE7631476F73FE', '4A69BA6BAFCF104471659E7668FD4C34', '公路工程综合类乙级试验检测机构等级的资格认定', '005184435QR13082001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A4CF13F7912D9B5A2EAC175D69005DEE', '472BD22570EE1C860F9A249E259A0561', '接收卫星传送的境外电视节目审批（变更）', '005184654XK55662008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A4D290ECBA62F7FBD3CE95015E83D7FF', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更住所（经营场所）', 'MB1912669XK55720011', '药品生产许可证变更住所（经营场所）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A5033165DB46FFE1A6B78FE281E5E4AE', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更质量负责人', 'MB1912669XK55720007', '药品生产许可证变更质量负责人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A5120AB3AECBAF6295AC39AC5A278D66', 'AA197BFC77AB73B785C0BE0CCA4E2404', '取水许可新办', '005184566XK00241002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A5198719EFA42DB8DD182994731BDDB6', '4D660CBDD76951F2658839ED738AF134', '从事种子进出口业务公司的农作物种子生产经营许可证的初审', '005184531XK36145001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A549FCCDF59854032950299C0A4AE2CB', 'FCCA0378D77595F6FBDEE29CC5F588E9', '河南省住房和城乡建设厅办事进度查询', '41000002SGG95722001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A55482C80311F4FA27AD5CAFB01B6CA1', '5F8D26ECDE3D72E872CCB821421E5ECC', '自学考试补办毕业证明书', '005184662GG07705001', '自学考试补办毕业证明书', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A55CB4A22FC49CE2AA7A22A1BF1F701E', 'BC60ECBA4AE58BE7AB7CC8C611863392', '融资担保机构变更董事、监事、高级管理人员备案（审核）', 'MB1530417XK00368008', '融资担保机构变更董事、监事、高级管理人员备案（审核）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A574B707B6F909684857AEA0E056E145', 'B03D342696AA095874AAF674B63413C4', '河南省通信管理局法律法规查询', '41000002SGG58558002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A590231AF91F4931EC33DDDA663142F8', '3B9FE7C3335DF76ABD6EE4D0C28BD411', '医师执业注册（离职备案）', 'MB1957883XK00278005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A5AB7B992781413F95B4F8D0D4AB3523', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品生产许可证证书延续（符合延续条件，不涉及产业政策，需实地核查）', 'MB1912677XK55651005', '重要工业产品生产许可证证书延续（符合延续条件，不涉及产业政策，需实地核查）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A5AC69022FC72B19E011E8EACCDA156A', 'D906949049EB3C2140D5DDA8B118FCEE', '河南省文化和旅游厅公共服务查询', '41000002SGG35765001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A5C465399828D7E9018FA6A19C15A6A6', 'DAF79A490A65454E538E21CF184ADCCE', '河南省中国传统村落名录查询', '41000002SGG41140001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A5C56A45B1A9288A21D61027FA94C450', 'E90A00DD330DE53554150506050CC97F', '河南省教育厅关于公布河南省第二批现代学徒制试点单位的通知', '41000002SGG36940001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A5CBA15E8600A523779DFAE7C85E6B7C', 'BE9A6EA7ABFDFF0ABCBBBE9790A2A353', '运输行业监理企业查询', '41000001SGG12367008', '运输行业监理企业查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A5CD104F53EFABC25E6477DB2F8F8BD0', 'C43C47B865849EF86F28C17C000EFD43', '权限内肥料登记（变更）', '005184531XK46213003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A5D05AF3428FF0462B6DAFE646F5B844', 'A026C144C050FB4C25D768C360C55B6B', '河南省图书馆数字图书馆外购资源查询', '41000002SGG80230002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A5E0546A07CFB61396C52BFDA130903E', '370496933CCE52C470B0C82736FC227A', '河南省宗教法规查询', '41000002SGG59185006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A6011517173689C3BD7DC35A5D5A9E3F', '826E4BAEACF36CF115B69243ADE3D6FD', '保安员考试成绩查询', '41000002SGG10679001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A63BF84B198B2DE785B53991934EEAF4', '22CAFC591062394F86554938AED00711', '执业药师首次注册', 'MB1912669XK00239001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A642F6FD0B4AD7BAEE19D68125D1BAC7', '7F68A15C524C1B9C36D047E48D694ACB', '二级建造师注册证书查询', '41000002SGG64735001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A644769DBB1224BFA415EB5E722820A4', '7C70089AE675347745C60F8CF8FA402F', '河南省交通运输信用动态查询', '41000002SGG79266003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A64D095897B5C5BFAE9F3F9658F267DD', 'ADE22C79F202F1B667C3D55EF9C34F13', ' 危险化学品港口装卸管理人员从业资格考核信息查询', '41000002SGG78161001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A66B14A45D4506510BCB3B2D9D1750DD', '02A271F8F3B4EC2F2F752B97E6A203F6', '产业集聚区地图查询', '41000002SGG80947001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A6747112FFFD1C6D875910FFF66CDC50', 'ED0E8A014A29C7CA63B4D81E82128FEC', '公路路政政策咨询及路况信息发布服务', '41000002SGG88166001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A6BFD4E6374107787009FD14D6442CF7', '8B57F564DEA175D90F5BA5A4AABDAA2E', '河南省工程勘察技术人员注册查询', '41000002SGG64534001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A6C51153C11F67B4C28B419837C03EE4', '6D1A849B77120866144F4F89D78F599F', '司法鉴定机构变更登记（减少业务范围）审批', '005184291XK5590500c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A6E9B158E36F5E02F2CD33F4D2666A35', 'EA1C657E025CBE799E9EB65617F70024', '河南省图书馆少儿中心读者指南查询', '41000002SGG95064003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A71546BEDF00C8DE47EB43AA5EC8F82F', '002447207831DA53248577890AA4AD3B', '文物保护工程监理资质乙级证书地址变更', '005184689XK9427000b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A724D6890FE1E325386186C5E777D7CB', '61F3061E897AEECA06F1A2D4C0796465', '型煤产品质量河南省专项监督抽查企业名单', '41000002SGG05719001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A72955A224AA3C6E78B07BD977C4E4BC', '855CF13AC35AE0A21E97E2F978F99BB6', '河南人事考试成绩查询', '41000002SGG71037001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A73B2DD7F5A381A51B750A4ABE06F0E7', 'C1685B8801507D7F6D310BCC8206D5AC', '重点污染源自动监控超标情况查询', '41000002SGG94670001', '重点污染源自动监控超标情况查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A74352BF0E275F4C7497A74905835D7C', '53021F33D8E22208E3DC2DC7ABB815C6', '废旧放射源备案（送至原生产厂家）', '005185040QT00173003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A744FD5F416A74977F700CCFF31F7058', '04AB80E847CEFD3F812174109D8EDCC6', '境内申请外国人来华工作许可', '005184638XK94086006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A747EDFA5D9A58086E696CA830ED2B6C', 'ACBED3B4D3E2B736A3B4C5B20E9658CD', '信息网络传播视听节目许可证变更地址（初审）', '005184654QT00287009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A775D60E657A656D00CCF833F9B14838', '0DCABC6DB8E5557658F705EBE03ACF4A', '河南省运输事业发展中心文件下载', '41000002SGG49011011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A7837CEC2B40CE584232AAAD233642C6', '3865CF4C89A1DD2420720829F3FD7820', '兽药生产许可证核发（复验换发）', '005184531XK84280008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A7923B39182ACAC60EF65685A40244A9', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '撤回派驻律师（外省总所撤回派驻河南分所律师）', '005184291XK55809021', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A7935B6DE54209FEE78CE055363BF4DC', '06F6EBD375B3FE82B7FC5B360E40D04F', '科技型中小企业评价', '005184638QR13046002', '科技型中小企业评价', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A7A48CDCDDC030D5AE744A5CEB5E945A', '7B9248D2A437ED8B76E204F2C088B421', '水产苗种进口审（核）批', '005184531XK98207001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A7A4E2E86E9EF2068D6957CCE7900E18', '6AA52CBD1EBC6CA6D4A70F01D828AF53', '贮存危险废物超过一年的批准', '005185040XK55881001', '贮存危险废物超过一年的批准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A7AF39E34970D0AC60063A9B673255DA', '76A27C3FAB51112E394E34FB5104976F', '快递企业股权关系变更', '717817421XK55671006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A7CA21FBB22C4781ABA806E0A9FB7581', 'C44D74D128721A914F5EFF2C91600AC2', '快递企业名录', '41000002SGG84244001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A7D81F90DDFF70626071710C1CE34CFA', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更企业负责人', 'MB1912669XK55720015', '药品生产许可证变更企业负责人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A7E87F04BBFDBF8B14762679628AE2DA', 'BFCF7C62F45488B987494DB20FC72B59', '互联网域名注册服务机构信息变更', '726991560XK96300002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A7F17F3D2574C7B857F7D511FC654E18', '0B6378CF858DEC0C87C5879498BCD42E', '安全生产许可证经济类型变更', '005184400XK55625008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A81457A0B2DD2003A57E1F026E63971A', 'D312AF932FBD03CEFCC5042FE04D2E23', '河南省交通运输厅行业精神文明建设查询', '41000002SGG80310004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A81BE39F36E2B8F0F67ADEDB2FCB451D', '450A91883ACB3FD62CD18F788AEF7061', '医师执业注册查询', '41000002SGG89390002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A821E9023CF165BEF8EF98003266075C', '1BEE7CB71BC077D9713F9FACFEE53997', '台湾地区的投资者在内地投资设立合资、合作经营的演出场所经营单位从事演出场所经营活动审批（自有场地）', 'MB1848628XK55943004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A831793DFE720226EDCFB5A87ABDACBC', 'FF4D5677E964B1ABA6D12470BA118E0A', '司法鉴定申请资料查询', '41000002SGG46822001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A8493E9DA6F2752373BE775FB19E674E', '05A566873B42D449BE1A40660186AFC9', '居住证', '41000002SGG36035001', '暂住证', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A84B13F9D838334665CF3A3EE2D2AF2D', 'E245E4E82DF88F0851C6FC41D1531426', '农作物种子生产经营许可证核发(主证变更)', '005184531XK96500003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A85A29CBCD606B49208D599C68DC7BEA', '628D42B0CD977081D8C468E7623C28F7', '创业担保贷款申请政策查询', '41000002SGG12262013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A85F5DA4DB494863EB317B1D2F7E1308', 'DBC3D7CFE77564A60F87FBFC49798AF1', '河南省事管局平安建设查询', '41000002SGG74847009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A86BD0A8A6219C9D162A7BDB3ECA0597', '4445715177D3B71861F31AF1E1B9AB0E', '森林高火险期内，进入森林高火险区的活动审批', '005184558XK94992001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A8728C471068C77E75B449B67508A468', 'CAD1035A4D5700D6674399F47FEA4EEB', '建立相对独立的平面坐标系统审批（属于由自然资源部审批外其他情形的）', '415800560XK00326001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A88728DE7585E24329817D9E1DDA7028', 'E9DF9366C7171B099E4EAE00202D6F74', '组织艾滋病检测实验室验收', 'MB1957883QT00281002', '组织艾滋病检测实验室验收', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A899C138AE86D9FDC83A18B3A6D0B934', '6082673D8C30D0EDFCB9F342C5D34250', '猎捕国家一级保护水生野生动物初审', '005184531XK88998001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A89FF230BE3B45C9C750FD42D99BFCD2', 'BFA0676B9452A35F6192ED6D2BBB53AA', '博士后科研工作站查询', '41000002SGG10522001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A8AE4AB1BAD0DF215ABCEEEF72E54E7B', '1B3F30B58A02896ED613F78881155B86', '河南省民族宗教事务委员会办事指南查询', '41000002SGG54228001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A8DE3AE52A5B7B8487C53B256BD58A30', '1F3E8080C43AE39DB9B9307DF4CC159C', '特种设备生产单位许可（制造地址注销，且工商营业执照未发生变化）', 'MB1912677XK0008001a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A8F30E93D9D6E047C83080E2113E90AA', '5C38996401ADC49FD0F6A4591EF13A80', '医师资格准入（澳门特别行政区医师获得内地医师资格认定）', 'MB1957883XK55861002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A900FDD3999242BC33C618D7D1BB0D5E', '175C971222104069558624D3087E5397', '广播电视节目制作经营单位设立审批（变更住所）(自有产权场地)', '005184654XK00299005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A90695FB5AAD33D7773D893B37E822A3', 'F16A055203856FE610578745CB21D8B9', '法治动态查询', '41000002SGG06108001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A90D3936C84D207C36EFFA1614C2CBD5', '5BE7F8F6B842B6C526992661ADE9D423', '活动断层查询', '41000002SGG10978001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A92E6E744B003067EED1825C9AFE6DBF', 'C239AECCD2715DC1FEBCE8633556D338', '首次申领《危险废物经营许可证》', '005185040XK00173007', '首次申领《危险废物经营许可证》', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A936F4E2AD3A40C94EE867EC87EA260E', '6AF4666BB6DBA9748BA281B6F176633D', '生产建设项目水土保持方案变更审批', '005184566XK00251004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A93AC2F9A08F1745161BADFDDCC0FA34', '002447207831DA53248577890AA4AD3B', '文物保护工程勘察设计乙级资质审批', '005184689XK94270021', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A944E7B2F72670718E03BF8814CDD427', 'E11A52F69537C09572BA8646CC59D9CF', '省人防办十九届四中全会全精神查询', '41000002SGG11684008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A9A69FFB4AE6B4F0F295A5A713DF3563', 'EACD9911457D64DD07710715528C6A42', '司法鉴定办事指南', '41000002SGG49585001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A9DA8292D26BE7E242E6B68443172275', 'BDA5C09BCB9FD6C0220BDED43D8A7135', '省直公积金查询', '41000002SGG62166001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('A9F330405AC36522C1832141447F7F54', '4C2CA143BDBFA85210AF37E965D52C26', '分公司、非法人分支机构、营业单位变更（备案）登记', 'MB1912677XK00139010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AA026617E03394B077FFADD7B46E9785', 'BFB89262A855E2305396FE7187950AAD', '车用气瓶充装单位许可（地址注销，且工商营业执照发生变化）', 'MB1912677XK55653005', '车用气瓶充装单位许可（地址注销，且工商营业执照发生变化）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AA2DE63DBCE255CE595E929CDA655315', '826D3C45E1A34A2608834C6CED7B1971', '农业转基因生物加工审批(申请)', '005184531XK97619001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AA4D7CF8DE3E98E6E557E8187CB8B9BC', 'B5FD74967D384D31C841BA9EFF52F301', '中国银行外汇牌价查询服务', '41000002SGG69294001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AA5F5C0A552207A599E8CAFD6BE8A80E', '2D1055872C9544CF086F84D38DEBB357', '公路超限运输许可（省内Ⅲ类）', '005184435XK00322016', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AA6661992F2E0CA0E4B8A46C1E2E5A52', '753400F02D7B40F8D3D5C0AEE09110E5', '在自贸试验区内外商独资经营的歌舞娱乐场所经营单位延续', 'MB1848628XK18047005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AA8047618CFE4B9F5F0833CBED432B9F', '210868935EBAD2BCD4821021BEBF9656', '检验检测机构资质认定法人单位变更', 'MB1912677XK5565000c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AA9AC12D496041E666BE488AF88C46E0', '3865CF4C89A1DD2420720829F3FD7820', '兽药生产许可证核发（迁址重建）', '005184531XK84280005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AA9BF06BD0A12078A3E8F4573185F327', '011AE2565F1F6D20757E8C1AF8D8471E', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出经纪机构从事营业性演出经营活动变更（地址-自有场地）', 'MB1848628XK12285007', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出经纪机构从事营业性演出经营活动变更（地址-自有场地）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AAA0076233D5D45526FE07DC3126F221', 'C19CC35C2E1D1CE5BF47DEBC21162BAF', '饲料添加剂生产企业委托生产备案', '005184531QT7596A002', '饲料添加剂生产企业委托生产备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AAA75ED7D04CC8179B7768F5366ECC27', 'DC4F08623B0C88C554283E65A9F58F3E', '放射性药品生产、经营企业许可证注销', 'MB1912669XK00223002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AAAC31AD5383D8184F2C0658F03936EE', '8015654E0CA1AF91F59A9CD610956141', '建设项目压覆重要矿产资源查询', '41000002SGG14411001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AABD1FE887EDC7A1127D42875B332A31', 'F324391F5B6F920146EF90D500879094', '机动车业务查询', '41000002SGG91950001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AAC643139F673B1882EDDCE5DF96293B', 'D823335B562A38F81032B4BB684801CC', '在森林和野生动物类型国家级自然保护区修筑设施新办审批', '005184558XKE424L001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AACB0AB2B01FC53DB3FE3603CD8FDA84', '1BB5DFAAB8B01AF6BA2ECEAB57EBFEC1', '出版物批发单位变更注册资本审批', 'YZ0000000XK55752008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AAD6BCCF27EEA1B5D885908285FEA25F', '20C677BD0AA82CBBA99826B94FBD8653', '博物馆纪念馆查询', '41000002SGG30838004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AADAB7B9BA9B39CFF60F5C91A213700C', 'D9778C02BCB09AEE2E01C423770C1CAD', '住院病历复印', '41000002SGG16653001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AAE47117435BD317DECD131E91A24787', '55E58455D4C66A6CEBA2F0A65698FE47', '中国中原人才网简历信息查询', '41000002SGG37135001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AAEF36036DD7542D790E2733C7522D54', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证增加配制范围', 'MB1912669XK55646009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AAFDB8AB7023630DB5B2598778A879E3', 'FE3E194376806EAA75867FC750E311E4', '甲级测绘资质变更', '005184224QT0804A008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AAFEFF54EC5053D476EB028EB3C43490', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动变更（企业类型）', 'MB1848628XK00096015', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB02CA50ABF416D56B54229F6329B91B', '5C9C99BF8FBB6290DD02C7B6AB7F7569', '第二、三类医疗器械生产许可证延续', 'MB1912669XK00235004', '第二、三类医疗器械生产许可证延续', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB21D2FFB8486C070984C943E948F2FD', 'C06C81E53712D151B9400E7B6DA14BB2', '运输不属于国家所有但对国家和社会具有保存价值的档案及其复制件出境的审批', '725831987XK3634800b', '运输不属于国家所有但对国家和社会具有保存价值的档案及其复制件出境的审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB2F00602C1FDA47B3E6B078D868A3CF', 'D798FC4A1C7A2494FFC7BE727173C240', '便民缴费', '41000002SGG14090001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB50B94B1F89629B0D6F69360B7FCB7E', '00A3A498AF128075BE4FE07DAF67B123', '食品添加剂生产许可核发', 'MB1912669XK55817001', '食品添加剂生产许可核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB545CB2FF5C2EC0E07662C0DE043D65', 'A3FF2E23263B29DAB594A0C53D9542BF', '利用普通公路涵洞铺设电缆设施许可', '005184435XK55757014', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB5AE7644B8168B5177919E145C5ABA7', '29C9D3159798CBB02E5497DE7ADECCA9', '企业黑名单查询', '41000002SGG73857001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB65D33E452353ECF58261F2D77827E5', '4960CC93B38364505FEC48E6EC917CB8', '安全生产管理人员证书查询', '41000002SGG31990003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB6C83BBFC18202F8349CF1765F1E4DB', '002447207831DA53248577890AA4AD3B', '文物保护工程施工资质二级证书法定代表人变更', '005184689XK94270013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB6F06D857C05C6DE242E23C984CE8BD', '002447207831DA53248577890AA4AD3B', '文物保护工程施工资质二级证书地址变更', '005184689XK94270016', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB939AEF2DBAB9F7CE857B83BC0E51E8', 'EDAE6FB2A1532004A0E66FF955BA3FAA', '户籍档案查询', '41000002SGG87190001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB94B1241F24D63FA4FC76C68BB9A9B3', 'B87CA072459921939B82FF7ECDF833BE', '食盐定点批发企业地址变更', 'MB1912677XKS7985003', '食盐定点批发企业地址变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AB94C10D1DD53BBAD5EC33482DAD061E', 'C448FAD4A61B2DF6F082085AABCC476A', '河南省常驻人口统计', '41000001SGG49547001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ABB1F6736552B337523C036279F2042B', '2657150C418DBE951956C3D5F0FA185E', '省外工程造价企业进豫登记信息查询', '41000002SGG61733002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ABBC7A9D863D1FEEDE7637B1B3EE41A0', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类体外诊断试剂产品延续注册', 'MB1912669XK00227005', '第二类体外诊断试剂产品延续注册', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ABEB834BC990F9ED046EB8B55B04CAF0', '002447207831DA53248577890AA4AD3B', '文物保护工程施工资质二级证书名称变更', '005184689XK9427000f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC01A898D95A166689A329DCF1014F70', 'B809E00AC48A7555C2212365951D789D', '准予办理煤矿企业主要负责人安全生产知识和管理能力考核合格证人员名单', '41000002SGG91215001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC0BF81FA5E346BC309F9AE4222A50AF', '2D0BBB663B32174B050689FB75CBA29B', '母婴保健技术服务（产前诊断）机构变更地址', 'MB1957883XK00279002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC32E7352402BD5EF7D5EEACE1F51DF3', '3FAE0261754585A6BEA42F6153821A75', '对国家新闻出版署负责的出版新的报纸审批的初审', 'YZ0000000XK55675001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC33DDF8C51BC995F2B6F4B99C3630DB', 'C1E97B068A422AA05AAB79B24D4229FF', '国际注册内部审计师考试简介', '41000002SGG82508001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC3683B330D6F6E1A5DB5004A820D85F', '1D87E039DDFA85ECCB06D5FDB92BEB50', '医疗机构校验', 'MB1957883QT6952O001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC39AF897AB45F73A0FCDCDACA2921D8', 'EFE580E8D561D8EB8AFE24DACCD6D336', '饲料生产企业（变更）', '005184531XK40671003', '饲料生产企业（变更）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC3D3347352216D112C74ED10C1B1C97', '0690F42BD599BB873D5C4F12B9C0005E', '特岗教师拟录用名单查询', '41000002SGG68498001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC45C850006FF35CFA5120CB8E9F19B6', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '律师事务所组织形式变更（个人所改制为特殊的普通合伙所）', '005184291XK55809015', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC545C69A6E445D8622A1CF186881090', '1DCCA60FE917B18FCD6A3F54A9FD812D', '河南省交通运输厅河南省客运公示信息查询', '41000002SGG01695001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC63BC7CCA46D0A403476F9F468137CC', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省社会救助信息查询', '41000002SGG37554004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC6E6A61408CEA740EFBDDC8F7BE1FBE', 'F2ACD30E51E73AD90BE12E12EAE0D676', '河南省土地整改从业单位信用查询', '41000002SGG84671001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC86D595DAB5B9EE2A835C93A6BF390C', '2C4F18F5A54B0264EAAC6E6013E00F49', '民办非企业单位开办资金变更登记', '005184312XK05267001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AC99C7D83BBBAF0E4A0196E58B49CEA8', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更生产负责人', 'MB1912669XK55720008', '药品生产许可证变更生产负责人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ACB2D81A01AA482E827B49766B39ECC1', '366B2F04CE794834E2DB485F4554F01F', '河南省教育厅关于公布2018年度人文社会科学研究成果奖的通知', '41000002SGG31099001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ACBEDEF0ADEF84F6B818BD915A9081E9', '66166E4E889FF9A7822DB5BC40DFA025', '水利工程质量检测单位乙级资质延续（机械电气）', '005184566XK9726800d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ACF324849AC23B1805FBBBDFCD2C2B84', 'FB2F1EF1A256BE5298EF56E3628B009B', '省直民办职业培训学校设立审批', '698732712XK00123001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ACF9542E5917A3A7B241CB39BC76AE64', 'DBE8BBF0865E001B654A5CCE1F4BA5F4', '普通高中学校名单', '41000002SGG03655001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AD35C72AA7EB32F352C93F103B3CE997', '8E65827EE8C7E6A7C1A0967FA8783D7B', '河南省科研设施与仪器共享仪器资源查询', '41000002SGG56282002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AD37D55666C9AEA5D3CE80D616C01603', '25D624AB3CF27DACE5D279C893E987A7', '司法鉴定相关法规查询', '41000002SGG81156001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AD3FDD0734E20BF350FF9FC4B054C30A', 'D49C411DD9718C900A47C62DE1C239B8', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出场所经营单位从事演出场所经营活动注销', 'MB1848628XK55963004', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出场所经营单位从事演出场所经营活动注销', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AD4D7A3478960C7A86F217092C9FCEB9', '76D6F1EBF680F8394259007D9D8DCB47', '专职律师变更执业机构（转到外省）', '005184291XK11640011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AD7B978F5FAF1394C5161875D1F6348F', '54E2C68F7845CDE9E82835698EDD90BE', '实验动物使用许可（变更）', '005184638XK00104007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AD9567D56F90533DE12868CAC2988A1C', '6A00FD4A1E99D9AE81440837CBAAB7C8', '举办台湾地区的文艺表演团体、个人参加的营业性演出审批(含未成年，在歌舞娱乐场所、酒吧、饭店等非演出场所举办的营业性演出)', '005184670XK12282001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ADA164A6086DCC6606C194C7733BB40E', '3F73D23386FB13A18CCBC2C3FAA94B72', '省级辐射安全许可证变更', '005185040XK00172011', '省级辐射安全许可证变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ADAA9CA8ED99CE7F3F868E5EBAA00BEB', '7BAA147FD16DCC6A14DF9C64899C7F09', ' 拟定地方定价目录', '41000002SGG86199001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ADB1BB3EAADD816351E675CE30B08366', 'D86A1F3246F843EDE7A51CCEAC37618C', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动换证', 'MB1848628XK5596100c', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动换证', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ADBFD2DBB5477CF9905091E46B87BA10', '76A27C3FAB51112E394E34FB5104976F', '快递企业注册资本变更', '717817421XK5567100b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ADC728D2FCEBD98C7C62A549DED3E407', '500FCFA560AA8BA64BC4F7E11F11EEC3', '河南省2019年中小学教师资格认定公告', '41000002SGG33171001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ADFDFFB96077ED5C62EC964D0AED03F1', 'E2259407A2F50F8AAD558285F6F7BB48', '河南省中小企业公共服务平台法律顾问合同范本查询', '41000002SGG80640001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AE0DB32116315E33F806CD46F9249927', '4549C460AFA0F9ECC1512F6B5BF64DD6', '（404）安全生产考核合格（B类）证书查询', '41000002SGG76678001', '（404）安全生产考核合格（B类）证书查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AE17FEB1E94339DD1DABCD05B4DB8ABD', '64233151D76C0AD7ABA6213E0E8A82E9', '基金会名称变更登记', '005184312XK97678002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AE2DF391B0B2B67A77D23FED7025B31F', '87C4208BED1361682065E50EF91BFBA4', '变更医疗机构制剂配制单位审批', 'MB1912669XK5583500a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AE35B58194E9776313A0DCA313337AA9', '145DDF1D41CD02D35186B75105DDD046', '除电力、通信以外的防雷装置检测单位资质的注销', '415800448XK16357006', '除电力、通信以外的防雷装置检测单位资质的注销', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AE3719B0A41C6245A5FC4A2DC739424F', 'F2D89A961BFDC743C8232DF93B6C8F21', '国家重点保护的天然种质资源的采集、采伐批准', '005184531XK17301001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AE4F125E8A01C9D8BA7EDE5D62A436BE', 'A9D71D04C2AF6996D687B7A2E9FC91CB', '船舶设计图纸审核', '005184435QT00316002', '船舶设计图纸审核', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AE5E807B5650BBD96E6768A34C2CA80E', '828A9EC1BB36A5B6D65FCEC3121D741A', ' 临时价格干预范围事项提价申报', '41000002SGG13899001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AE6EC391CFBDAFA55B1FF1E976BD10D8', '93D4C4DB8BDE89F4D40DE7B9ECCE70CE', '单采血浆站变更法定代表人', 'MB1957883XK00266004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AE74A45382CBCADDEFEC1987383A1431', 'B9203383CCAAFF911E31EA3FA2D20290', '河南林业扶贫攻坚信息查询', '41000002SGG58373005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AE854BE5D6FA67226B2409C0C5854FDC', '131AFEE53EA53913F153AD4613AD7F68', '省级行政区域内经营广播电视节目传送（有线）业务审批（延续）', '005184654XK40260003', '省级行政区域内经营广播电视节目传送（有线）业务审批（延续）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AE8CB13955D9ADE702C4AA70F0BD36ED', '9BF3A1243DFFC7AF6F7F3F40F718E374', '企业离退休人员申报失踪、下落不明超过6个月暂停养老金发放', '698732712GG0632300f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AEAE9454444BAD413B2B7EA8B9FEB350', '04C1BC41C13AFFAEAA8A43F51C5E1132', '河南省监狱管理局狱务公开', '41000002SGG30239001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AEBEC6462FD27A1D4E16589CCFCEBAED', '6C6412A5C647FE3BA2FCDB229FEAA9DE', '森林病虫防治知识技术咨询', '41000002SGG74753001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AEC78436747EF3E3EC14F8D2AE5C9C72', 'E14605D00650B5CD060F03CFC82BB8AA', '报纸变更刊期审批', 'YZ0000000XK55761003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AEC9B5A704E579CD11577FE6BE89A22B', '0A6B2C5241E97581B4F2236319DC47D3', '审批制、核准制的建设项目临时占用林地许可', '005184558XK94519001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AEF22837B4401CE72DFFCF93EE157E67', 'A2538E4EC0A9CDC5A939A6A4B582BFD2', '个人参保信息', '41000002SGG88133001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF0C76043826A971FAFEADB3C82C50A8', 'FE3E194376806EAA75867FC750E311E4', '甲级测绘资质补充和修改数据', '005184224QT0804A004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF315719706BF197D1C0D955B8303725', '6F9B1E6B94323B9820D0E64FFE93A3F2', '人体器官移植医师执业资格认定', 'MB1957883XK76215001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF31970D85073316D735D9DD7F36A1E5', 'CB6CE6EF4381602C1A69014D6B001DA1', '音像制作单位变更地址审批（工作场所为租赁性质）', 'YZ0000000XK25874001', '音像制作单位变更地址审批（工作场所为租赁性质）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF3F6935CDB765A65EC63256347CFB97', '053A64C1C16C3E21411CF7DDD6B3C4CB', '河南省统计失信企业及人员信息查询', '41000001SGG38624001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF44F367EBB53806CB9D66F22A875287', '45178A0278017908E60243E32117470A', '健康嗨起来', '41000002SGG92882010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF53C622AC0CB7A8AAF7F712089445DC', 'A1776CF64C8CDE6E4CB81A5FCC4567CA', '建设工程质量检测机构资质注销', '005184400XK55725004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF67E2C9DEAD814DFB25C20DA1D65F5C', 'DD08833CEFFB8D7B5545EFE4B6C35D4B', '船员证书查询', '41000002SGG00456002', '船员证书查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF6E0EE7C853EA811F9E75BB09FA2056', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更质量安全负责人', 'MB1912669XK55718012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF72720BC5352C2F22F52EB70AF49DB2', 'E9B68A8EB73FE41BEEF8FF12C470F88A', '河南省各地中小学生防欺凌治理工作机构及联系电话查询', '41000002SGG47841001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF770CF6DB61138B0CF052144E19157B', '9EB60BA69C65C3F4578EFF5A8EB867B0', '河南省建筑市场监管公共服务平台-进豫企业信息查询', '41000002SGG34247001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF7F671DFDF70B1F9E824172446DA8A1', '2E6784788D59549E3E810E44F689C3E9', '医疗机构执业登记', 'MB1957883XK55857001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AF928EFE4CB0F7175CBAB9F4EBCFCFCF', 'DAB301DDC04E0D58DCD9AA1BC0217944', '省级建设项目环境影响评价文件审批（辐射类报告书）重新审核', '005185040XK4997100d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AFB3C967310B707F404F32ECB7F6400A', '210868935EBAD2BCD4821021BEBF9656', '检验检测机构资质认定人员变更', 'MB1912677XK5565000a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AFBDB2DBDADC4D4D222F9EBBBBC28688', '1D6F4ED04B44FD8C22C6E509F7B53314', '河南省中小企业公共服务平台建材行业资讯查询', '41000002SGG69935008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AFCAB7255EBB27D21130F6640EC6C271', '73D12CDF58BD300C0C3DE2A390751C81', '河南自然博物馆社会教育活动', '41000002SGG45782001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AFD47556C19C511C0E2A849BF99E067B', '754004025F17D26D8ABDBAE237CAA73D', '河南省发展改革委员会新闻查询', '41000002SGG23547011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AFE3AEC1F6FC8BEEE9292CD91F4087DB', 'E0AFB9F2A6088F3C5B92BC3B78F2BF24', '司法鉴定人变更登记（变更执业资格）审批', '005184291XK06476016', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AFE8F239AECC4D0D1BF1246E8B3FC9A2', '2CB826D6539186FAE26D8A0CC8B19F54', '台湾地区的投资者在内地投资设立合资、合作经营的演出经纪机构从事营业性演出经营活动变更（名称）', 'MB1848628XK55952009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('AFF2160A9B36BC5F3B79FA70E09B1071', '2044A300C2EAB88E6F28F85472E41F98', '港澳台医师短期行医执业证书遗失或损毁补办', 'MB1957883GG12395001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B0081D5B4499B499D0E881657F8005B0', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动审批（租赁场地）', '005184670XK12190003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B01A4732CD3B09440D961FE9E3178F06', '2A86853D25CA7F5B36D5CFB4ACF242CE', '实施专科教育的高等学校的设立审批', '005184662XK55913001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B01DF0BE342E0AA6FF01012A4A0BFC46', 'A9A5D3835850BAF683CEA791F5D3A76B', '医疗机构制备正电子类放射性药品备案', 'MB1912669XK00225008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B0246964C2639AE04617080E80331B8B', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）补发', 'MB1912669XK55721006', '药品经营许可证（批发、零售连锁企业总部）补发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B02652AFC82EC06C6D7369F528914962', 'F4AB05DFA39CF3C4AA051E7C2BEE7026', '驾驶证业务查询', '41000002SGG54256001', '驾驶证业务查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B03EDA9913A76CF4ADE68B8B368A99DD', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动变更（外国法定代表人、外国主要负责人）', 'MB1848628XK00096010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B04E9D61629232FB69D1C3CBF95A847B', 'D312AF932FBD03CEFCC5042FE04D2E23', '河南省交通运输厅新闻查询', '41000002SGG80310003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B04F549A70B0AF2AA9CEB455E25F1A3F', '20D22AEE726D7E9565D4EB72C7CF2608', '医疗美容主诊医师备案', 'MB1957883QT1567A001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B05C05E0B5238F2CBD552879C6ADFD52', '36657A0238700B20CDE306DE320D4135', '创业担保贷款申请政策查询', '41000002SGG77874001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B086B278324B184B4BF775182D876C76', '28DA720BD61EABDFDFD9652D4E1E4CAC', '对保护航标单位和个人的奖励', '005184435JL00474001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B0B60815A81EAE5617A78FF7FCE52906', '56FEEBADEA7EC0A72B6499E2EB2DBA53', '省税务局偷税查询', '41000002SGG67186008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B0BD5DA9DFD5ECD5FD769396D470DE04', '002447207831DA53248577890AA4AD3B', '文物保护工程施工资质申请增加二级资质业务范围审批', '005184689XK94270024', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B0CB61FA100AA21CA080BCDD3B180ED7', 'B9A2DA515D9C5FCB80D2B34067423AF4', '卫生监督协管', '41000002SGG79677001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B0D5FACE7B8C2FC951E4AAC16BD83719', '766C890B62C0FB8BB67A80E8E19C7449', '编制产业发展规划', '41000002SGG20771001', '编制产业发展规划', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B0D6428CE27857ABC041879DAB5B86E3', '9BF3A1243DFFC7AF6F7F3F40F718E374', '企业离退休人员重复领取养老保险待遇暂停养老金发放', '698732712GG0632300a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B0E19D3B3B7C71059F66ED177F553B24', '3AAF21856C478E41EAED1E6D30068443', '食用菌菌种生产经营许可证核发（母种）', '005184531XK67400001', '食用菌菌种生产经营许可证核发（母种）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B0FE075F2F77FCBEB1F49357C93DCEA5', '69ACE9773F9DBA89B44AEE57D7DBE870', '河南省教育厅政府信息公开指南', '41000002SGG89680001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B103304FD3D4C320CAEF953758228039', '403699F5F2F186B4DB67EB786FD5F658', '省委军民融合办政策法规查询', '41000002SGG84189001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B11EC1F37DA4B2AB9B6E3CA5850E3087', 'D0E40284EC9C9663DD50C4001D0B139E', '举办外国的文艺表演团体、个人参加的营业性演出变更（场地-在演出场所举办的营业性演出）', 'MB1848628XK1806900a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1224E41246D3E92E23208C9F540F20A', 'BE80C6B10253ECB2E3CA1150ACE4FD6B', '民办非企业单位演出经纪机构从事营业性演出经营活动审批', 'MB1848628XK55977008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B12661148A7B5033C8101056B1CB38C2', 'BFB89262A855E2305396FE7187950AAD', '车用气瓶充装单位许可（地址注销，且工商营业执照未发生变化）', 'MB1912677XK55653006', '车用气瓶充装单位许可（地址注销，且工商营业执照未发生变化）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1277510DD41CDD366D22EAAF48D833B', 'F24B6B879B593353B99DCECBBE1F1289', '中国地震动参数区划图', '41000002SGG60901002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B146EFEE24FA4DB997B1811B9306ED83', 'B3EF21FB567A5AD5F7536A6C4D7315FF', '办事流程查询', '41000002SGG66788009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B15932858290579AF6DB4A1B7CD62DEA', '0DCABC6DB8E5557658F705EBE03ACF4A', '河南省运输事业发展中心通知通告查询', '41000002SGG49011009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B159CAD17AE1A9341B405815010AE455', '754004025F17D26D8ABDBAE237CAA73D', '河南省发展改革委员会文件下载查询', '41000002SGG23547024', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B17253BCD4E0587811133DC553C75084', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类体外诊断试剂产品抗原、抗体等主要材料供应商的变更', 'MB1912669XK00227006', '第二类体外诊断试剂产品抗原、抗体等主要材料供应商的变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B17AEC3A68814839B67CA78866ECC631', '56FEEBADEA7EC0A72B6499E2EB2DBA53', '其他案件性质查询', '41000002SGG67186013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B18269494C016AA9338593C9115BE26D', '6BD654211E42460AAF688E90F9E9AF1E', '社会团体业务范围变更登记', '005184312XK99203005', '社会团体业务范围变更登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1972B451EC8D21D1100B5DE8B8AD98F', '4B8FEE40FBF5804CB7236B9B1CA83527', '河南省教育厅小豫老师在线', '41000002SGG37232001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1B496D92769336DF322DD879F0A2C56', '20B2F43E6FAAA7D08F0AE5DF28177144', '违法处理业务查询', '41000002SGG54836001', '违法处理业务查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1B6B2C30E47002962005550093FB160', '30D70BFA323AD2533987F4E53A4BAAA4', '船员适任证书核发（到期换发）', '005184435XK00313006', '船员适任证书核发（到期换发）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1C2CC41187C8BC520E721ABE9AD1C7B', '1F064EF64B7B458BBA0CCB9BF5C4B7F6', '图书馆多媒体文献外借', '41000002SGG49209001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1C7ACB4B63A87F0ED1B39CB3240BBC5', 'E32E00F60A087A42763B51BFD25E2293', '就业登记查询', '41000002SGG51862001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1D06F6294160FC94DF70D45C26AEF2E', '8EF170AF6ACBF96867BFDB8D11F3E062', '河南省加油站查询', '41000002SGG93874001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1DD60EFEE7049BC371FAE442D90C112', 'ED1AB790D3AC960E00A9D7834C16EC74', '水运机电工程专项监理企业资质认定注销申请', '005184435XK55677012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1EDE912149AAE5D07581FD452535025', '5FEB651236BF3E3F0BCDD3DE87BC115D', '港、澳、台投资者在内地设置独资医院（中医、中西医结合医院）设置审批', 'MB1957883XK15943003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1EEB052F81919DD1361E33AE976FB60', '7008409B5358C61840A454427411A563', '检测机构查询', '41000002SGG79557001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B1F2D8F46A688B51768068059DAFD1DA', 'FE3E194376806EAA75867FC750E311E4', '甲级测绘资质注销申请初审', '005184224QT0804A002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B20C3587C582C7E48260039D88ECF7BA', '3881E82D7332450E9579988ABAA4FA67', '河南省中小企业公共服务平台管理技巧查询', '41000002SGG69551006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B21187627BF5505D08AEBE83F87407E1', 'CD5958AF012288AC37C5B389B31FF431', '水上水下活动许可（通航水域岸线安全使用）', '005184435QT00314018', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B23D05F87F96719197BEFE0B9BE7688F', '87C4208BED1361682065E50EF91BFBA4', '变更医疗机构制剂质量标准审批', 'MB1912669XK55835006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B24A661287FFCC20B588DB5383EB5864', '3881E82D7332450E9579988ABAA4FA67', '河南省中小企业公共服务平台开店技巧查询', '41000002SGG69551004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B2539D0CBDC51F92412D5A66E9AE8D96', '47A4ED83CEE0AA57465F30E426954DF5', '业务进度查询', '41000002SGG16906004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B254F031CAFF26DF9D441F4DA6A3E648', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品生产许可证证书延续（符合延续条件，不涉及产业政策，免实地核查）', 'MB1912677XK55651022', '重要工业产品生产许可证证书延续（符合延续条件，不涉及产业政策，免实地核查）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B26E407273191EF8DD6EDBA3A2718237', '3865CF4C89A1DD2420720829F3FD7820', '兽药生产许可证核发（注销）', '005184531XK84280004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B286D1374F4EC4032D6B113B2895A73A', 'C108A994A60F2FEB86F3480A7D8E780B', '政府信息依申请公开', '41000002SGG58867001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B2894A7FBB73AF7CF3848DE9BA83D4B0', '35E9697DA081BF19A42EC12E4CC99F79', '检验检测', '41000002SGG55748001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B29D204875D95C4ACFA5FDA0A020400A', '93D4C4DB8BDE89F4D40DE7B9ECCE70CE', '单采血浆站变更主要负责人', 'MB1957883XK00266007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B2B9626890E0EA4C0E027E44782FF6C4', 'B3EF21FB567A5AD5F7536A6C4D7315FF', '党群工作信息查询', '41000002SGG66788004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B2BC1452E6C0F50E447D48282A7148A4', '4C2CA143BDBFA85210AF37E965D52C26', '非公司企业法人变更（备案）登记', 'MB1912677XK00139012', '非公司企业法人变更（备案）登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B2D65566ECCD388F2622EA3C20E67B0C', '314F10B68D58B232F5CE48FE9E52CB87', '河南省中小企业公共服务平台面试指南查询', '41000002SGG96084002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B2D9C8459E6C1D686080BE77643E8178', 'A4D25033A2890861BAC94D4B735B440C', '河南省地震目录查询', '41000002SGG36125002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B2F8168636D9A3ECCF0A742A38353A99', '1E1773146D9A5A85D3DC6C79B780F98A', '医师资格信息更正及补办', '41000002SGG67879001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B310D6C064C02FECCA9C0D152B4BB2DF', '1D6F4ED04B44FD8C22C6E509F7B53314', '河南省中小企业公共服务平台汽车工业行业资讯查询', '41000002SGG69935010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B3215C5E59BE2384C38C76C2D4ACD96A', '3CCFA29BF20B77E25B2C73BC6E18ECDA', '电视剧制作许可证（乙种）核发审批（延期）', '005184654QT23678003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B365604A10EAD72D6C09958E6E6E9411', 'CCB3BFF63772B6DBE0970BDD943E5F93', '省税务局新媒体查询', '41000002SGG44593003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B37F54B64FA0A421923BE727FE19F06C', 'A1776CF64C8CDE6E4CB81A5FCC4567CA', '建设工程质量检测机构资质增项核准', '005184400XK55725007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B3A4C596D8B1934C2B1CA3AECCB28D5D', '7DEE923FC05B35180BD59DB7B849AAC0', '煤矿企业安全生产许可证注销', 'MB0P03925XKJ2QKK001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B3B10044987B06C8DC0D98E50B2C5ABA', '5C3E645A610D16DE563E3B9C8CD77BB6', '二级注册结构工程师延续申请', '005184400XK09297005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B3C2260D5ECBF909427F06C11631BB16', '1BEE7CB71BC077D9713F9FACFEE53997', '台湾地区的投资者在内地投资设立合资、合作经营的演出场所经营单位从事演出场所经营活动变更（地址-自有场地）', 'MB1848628XK5594300a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B3C50486F2B72492D13831B591A3C073', 'B3EF21FB567A5AD5F7536A6C4D7315FF', '精神文明信息查询', '41000002SGG66788005', '精神文明信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B3D8067CD762B6607682421D1F4D863E', 'FFDC49BC967101AC519D3994437AC137', '河南省煤矿安全培训教学精品课件名单', '41000002SGG12598001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B3E367914EE3A14D68AC5AAFDD89AF1E', 'B0F26046AFD46B3B67859E299D303EFF', '河南省粮食和物资储备局政府信息公开', '41000002SGG14687001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B3EFAF5EFD09D3AA4B0091E29F9EB9F1', '87C4208BED1361682065E50EF91BFBA4', '变更直接接触制剂的包装材料或者容器审批', 'MB1912669XK55835007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B3F0CF7DFCC804C2B5E6A98BCDA5B00E', '89A9E08C99193CF98991B0CE18DDD3C0', '河南省教育厅 关于河南省高等职业院校创新发展行动计划（2015-2018年）教育部认定项目推荐的公示', '41000002SGG94643001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B3F61A86D41746B3AE4B9953F20351FA', '18944E47F6023F82ED9A362E1F886F5F', '拟确认三级安全生产标准化煤矿名单', '41000002SGG60702001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B3FF7C582AD86980FEC4AD2B5EAB8AAD', '7D6755DE38573A96E45041C056542857', '河南省特色小镇验收命名地图查询', '41000002SGG10258001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B409496063F8E3CA2F7A49EB3D75ECEC', 'A1776CF64C8CDE6E4CB81A5FCC4567CA', '建设工程质量检测机构资质延续核准', '005184400XK55725006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B415F76AA6397E1E08F6A629E3BD5533', '199CA744F5CD7AB4A8D4E1AA83AEF338', '税务检查证查验', '41000002SGG66701001', '税务检查证查验', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B4376693CD856FEEF6D661871DC461A6', '85EDF718BC6CDF7CBC5DC8736C16A7CA', '河南省图书馆学会表彰查询', '41000002SGG76036005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B4467D5A8F3272ABF9F71DBE6356E16F', '6272B2E7DA5EDE117AB6083211C88E18', '河南省图书馆古籍保护工作简报查询', '41000002SGG01895005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B4607C7B3513A1F3061E6345DDF1AA61', 'EF24AE2F967F13A0E918C4B4C6E011D0', '河南省图书馆历年年报查询', '41000002SGG49678001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B47FA0FCFB1BCEE5B524C1ECD12EAD15', '754004025F17D26D8ABDBAE237CAA73D', '河南省投资项目重要公共查询', '41000002SGG23547004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B49FE08C5C640E88782964010E1E438D', 'F05C7FD7E33FF225FF2BA152901336BE', '河南省工程造价企业资质信息查询', '41000002SGG08935001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B4A0EAA2CC4FA3E96B7BBB6F3029C069', 'BFB19A2998E1F8D37256D3FA057B21C5', '河南省广电局信用信息公示随机抽查', '41000002SGG75539001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B4AA02012E6E8EC3F13BF1B8DC2C0EC0', '29CFE7DB3D26F4578C912DD0D4B9B408', '境外会计师事务所来内地临时办理审计业务审批（港澳）', '005184603XK55626004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B4B534EBB610AE1C9BF6B580C8E24FEE', '7C6F52899DD689F99BBB81757E7D4CEB', '价格鉴证援助', '41000002SGG39986001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B4D083314549B825C659BF0B1099D21D', 'F9F6C2B5D1572005269DA71358583CBF', '特种设备检验、检测机构核准（延期）', 'MB1912677XK55667003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B4DB577C3AFFA19A25347A25F3A9AAB1', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更质量受权人', 'MB1912669XK55720018', '药品生产许可证变更质量受权人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B4E76E37E7DEC04D70306879617F6812', '45B3F0D5D1E32B0589311A282D162609', '满分审验教育查询', '41000002SGG74990001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B4E7EE481685BF2C3B6F94DAED10E8A4', '04D3CB82804B3D721BC4577AF475E8E5', '农产品质量安全检测机构考核(延续)', '005184531QR0002306f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B5004D43EBBC023F9C21EB03D7DF8A7F', 'E9E86D7B348A1A15B87D7D686B278C0C', '政府定价目录公布', '41000002SGG26786001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B5153192B4FA4EF5334A793BC5228E99', 'E9D1EA6973CD23CEF8FFF9D8F99848A0', '往来港澳通行证有效期查询', '41000002SGG56464001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B517A772FEF0497E9B785E7122EBA12E', 'D89DD96B6BE36CD2F8126341C967D00D', '省级行政区域内经营广播电视节目传送（无线）业务审批(延续）', '005184654XK35389002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B52F080E7A05401A0D18C6086727076B', '592B65455FF7DAB3A0B2332D48F6429C', '河南省自然资源厅审批结果查询', '41000002SGG95704001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B536E313321EBFB7B6655F4414C51093', '681810843E3A1A4828F42B92ECA1CB66', '河南省住院医师规范化培训报名学员查询', '41000002SGG36539002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B55070B25E1AE80E92A1B6B5E064FC86', '7C70089AE675347745C60F8CF8FA402F', '河南省交通运输信用信息异常处理', '41000002SGG79266005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B56A53FEB875707BAAA21408351F0162', '146DE1561018FBE802CBE7681D4B4DFE', '文博河南查询', '41000002SGG06421007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B574FAD3021B9CBA78FE0B5FA44977A6', 'B47BA8EC3638DBA1F994D6D533D17B92', ' 面向民间资本推介项目查询', '41000002SGG84415001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B57DDBA4412C20B58D07112A48F705C9', 'B9203383CCAAFF911E31EA3FA2D20290', '自然保护信息查询', '41000002SGG58373002', '自然保护信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B57E7DBF35A160B585A4ADF852002837', 'B9AB43F0556A700B5CA24A12E4CCDCA2', '安全生产考核合格认定延续', '005184400QR0235500e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B5815DF377754B420F21DBD47ACB91B5', '1DCADF8941110631E23704ADD32560E6', '音像、电子出版物复制单位变更地址审批（经营场所为租赁性质）', 'YZ0000000XK55723002', '音像、电子出版物复制单位变更地址审批（经营场所为租赁性质）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B5A33773921034A4AEB8DC410669BAEF', 'E28B8190978B6DB69297D902422A0A5B', '地理位置查询', '41000002SGG43940001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B5C62E8224BE384D2116685F93DDB39E', '76A27C3FAB51112E394E34FB5104976F', '快递企业企业类型变更', '717817421XK5567100a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B5C6D0A0F8B9696F3A62E11D3AA2EAF0', 'B9AB43F0556A700B5CA24A12E4CCDCA2', '安全生产考核合格证书变更（跨省转入）', '005184400QR02355015', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B5CD6D1A5EB2F572A71C7508F5D45663', 'D0E40284EC9C9663DD50C4001D0B139E', '举办外国的文艺表演团体、个人参加的营业性演出变更（节目）', 'MB1848628XK18069003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B5D3A567C113CDB742D7E1C0BB3337AA', '472F7FBAFBCF9D5D6AB0212817EEEE56', '省体育局通知公告查询', '41000002SGG78500011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B5DDFCCB09EA4C361B7CED5DD1AB2FD2', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '律师事务所负责人变更', '005184291XK5580900f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B5E99188C2175C656C1C3BF8A1C27326', '7E105B152280F3C7E60EE29BC14CFF8D', '维权动态查询', '41000002SGG28964001', '维权动态查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B5FB863055C66FD5412140372E5586E2', '2655CF5EA72A1B0BBD22FF6BF8C77460', '河南省安全生产专项整治三年活动', '41000002SGG98652003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B602296AAB237943587896958CBE6D93', '9918EF1770C80B92E4A01E8C77480CD5', '电台广播', '41000002SGG37956002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B68C336FB22057B566B4C31CBB3D0D45', 'B3EF21FB567A5AD5F7536A6C4D7315FF', '人事教育信息查询', '41000002SGG66788001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B6B2FAAC457B0671A98DBB7AA727B40F', '3BC85EAC8AE4DE1C60D93AA31448EC1D', '河南省各省辖市、省直管县（市）职业技能鉴定指导中心业务联系电话', '41000002SGG74291001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B6B6840D19FC951C859246BBCE45FE07', '21C0831D337B178996B102BF362D2F50', '企业职工基本养老保险关系转入机关事业单位养老保险', '698732712GG59360008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B6B7FFE199A5F9BB58604AEE993A37F6', 'A3FF2E23263B29DAB594A0C53D9542BF', '利用高速公路桥梁铺设电缆设施许可', '005184435XK5575701e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B6C66F61FF5DEF4F357C704CD346FEAA', '0A417B9E6420CCA3B856D6FA94D20A33', '在高速公路增设平面交叉道口审批', '005184435XK55647000', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B6D2BF1C922353638ED19728464898C0', 'D228D201B16FB0DC9519F41E115C741F', '非密封放射性物质异地使用备案（跨省）', '005185040QT1828O001', '非密封放射性物质异地使用备案（跨省）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B6D3EBD7CFE55C5061A45AC529899043', '48CE142F00FBA0E19590703B4CF1252C', '兽药经营许可证核发（迁址重建）', '005184531XK29094004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B6DF02A8E856388BD2B5BA542C329C1F', '76EC3B93047EBC37EF09FF541C48CAA0', '生产建设项目水土保持设施自主验收报备', '005184566QT23927002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B6FD9E257F590981673D8B254D361805', '9D9DAA743A0BCBA7ADA27FE5E3A7B8AD', '地质灾害危险性评估单位乙级资质认定（变更）', '005184224XK10466008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B72B85DFF1F4619370932CD24873A59D', 'EE78E0DDC743E7AC4100E84EB753EAE5', '河南省中小企业公共服务平台环境监察资讯查询', '41000002SGG05182003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B731FECD33331B35A55F67EAA2A560E6', 'FFD49B2D50431CF59CC8744EB0C3D3CB', '企业离退休人员完成养老保险待遇领取资格认证恢复养老金发放', '698732712GG66003002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B76953FBB5B075B07749E71792118FC9', 'E017D310C86BEF78FDDEBE75A336AA92', '省委军民融合办政府信息公开', '41000002SGG77294001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B76A6D5595D652052071ACF058A5D14D', '21C0831D337B178996B102BF362D2F50', '机关事业单位养老保险转出到企业职工基本养老保险', '698732712GG59360007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B77FC04DEAA3C8C3045767812B475E05', 'EFA175BBF686C624B24D011A5BB98B06', '不动产权登记信息查询', '41000002SGG97331001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B785A2516B90FDF1837D18AD5E5B0D9B', 'EFE580E8D561D8EB8AFE24DACCD6D336', '饲料添加剂生产企业（变更）', '005184531XK40671008', '饲料添加剂生产企业（变更）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B789473116118452A3484ED91A36E02E', '2840130F47BB61EC8DD35DE5F11C75B0', '河南省自学考试成绩查询', '41000002SGG64892001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B789F10519DD84D9D95C3B7E69F63FDC', '2211255323B520868D308F5CBFAA67FC', '公布县（市、区）统计调查制度', '41000002SGG38042001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B78D488800C9D0925AC2C8803020536E', '2222BFF4ADF4A2D7512A6004C404255C', '发票真伪查询', '41000002SGG20896001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B79AB1BE02DC21DC97E2A753C553D0B4', '56AEABD46393711AF6CCBFF157815539', '河南省工业和信息化厅政策解读查询', '41000002SGG51227005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B79EB980D1C8B13F4560ACAA12356156', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更生产地址（文字性变更）', 'MB1912669XK5571800c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B7A04FA9A5BCDBF656BF3C4306AF8524', 'B341E791DA8152CB479E136C7ACB787C', '港、澳律师担任内地律师事务所法律顾问备案', '005184291QT21165002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B7BDFC9F5E1C8037FAABC90CC533AE11', 'ACE3DEB6F873805D4081C52A336E2A53', '药品生产GMP证书信息查询', '41000002SGG27249003', '药品生产GMP证书信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B7CD0772A475BB0183D2522890322685', '27D5C0183D3514BBED00CD45C5CD8607', '大型体育场馆开放', '41000002SGG29835001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B7EE375DBA2768C2AF112A9372A4ACED', '233415851095954C6D58BD8C0A84D744', '慈善组织担任受托人慈善信托设立备案', '005184312QT00201009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B7EF4BAA061DAC17D93495580537DD38', '0BBD4DA6FCEEB6E55CAE103D24B1CAFB', '政策法规库', '41000002SGG97064001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B7F189A4AF6A7ECD18E5C7A225D1E22C', '3BDC97E8CCCB9FCAAA965D8ED85E2072', '延期向社会开放档案审批', '725831987XK55758001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B817B774285FD2189BCB79F123DBA591', '275B46570C4AF0FC0F76EE3B2897CF58', '普通护照有效期查询', '41000002SGG35996001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B83A225FB8FB9DC68497B9B595DF1484', '131AFEE53EA53913F153AD4613AD7F68', '省级行政区域内经营广播电视节目传送（有线）业务审批', '005184654XK40260001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B83A7707B2967DF21E8F662C3B3A99A6', '1BB5DFAAB8B01AF6BA2ECEAB57EBFEC1', '出版物批发单位变更经营地址审批（工作场所为单位自有产权）', 'YZ0000000XK55752005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B85F20E58FCBEB49A55241106B4A5506', '600C265FA0DBB411C4BBD8EC1DE6B9B9', '高校人文社科项目申报', '05184662MGG06251001', '高校人文社科项目申报', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B894DD6B07BAC5AED64A035C39CC7ED3', '0A2D83BEBC650C36EB80EE0FF91DDB76', '国产非特殊用途化妆品备案', 'MB1912669QT5506N001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B89CA64140CF9A0816900FF477F09F18', 'E6DA82B14798B2295D2BB491230662B5', '水利综述查询', '41000002SGG85234001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B89F180A69FE724ACFC04B495E36D9D8', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证减少配制范围', 'MB1912669XK5564600d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B8AFEB12DF9F85EAF8E110644EB6F574', '4F2725AF9835848FE8DFA2C608FF4F01', '河南省教育厅优秀教育管理人才评审', '005184662GG06142002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B8CD44B71D363F1300142E79B94CFAE6', 'B80D5F37B8BA1DB6410B2EB02D40FC2A', '申请工程勘察劳务资质', '005184400XK20097004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B8D30688A25A6EE9FE2032F7F6522FDD', 'E11A52F69537C09572BA8646CC59D9CF', '省人防办扶贫一线查询', '41000002SGG11684004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B8D7B1EB64A41C8D0BAA655385520F42', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（危化品、水泥、建筑用钢筋）生产许可证许可范围变更（重要生产工艺和技术、关键生产设备和检验设备变化、生产地址迁移、增加生产场点、新建生产线、增加产品、产品升级等情况，且不涉及产业政策）', 'MB1912677XK5565100a', '重要工业产品（危化品、水泥、建筑用钢筋）生产许可证许可范围变更（重要生产工艺和技术、关键生产设备和检验设备变化、生产地址迁移、增加生产场点、新建生产线、增加产品、产品升级等情况，且不涉及产业政策）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B8FA90D4AC53FF02D0A85A8AECE64484', 'ACE3DEB6F873805D4081C52A336E2A53', '药品GSP证书信息查询', '41000002SGG27249001', '药品GSP证书信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B8FC749943D6DAE050BDFCCC3CBCBA5A', '56C5981C54D0126A46E5DB1CAC4E793C', '二级造价工程师执业资格注销注册', '005184400XK14177007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B919FA2B8B80617F1710D5AD23A3736B', 'EC348957C16D3015BA78304D22DC972C', '动物制品的进出口及其它', '005184558XK00183004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B92DFAD4137B7EC54A6AB3B6B9A63035', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省民政厅建议提案公开', '41000002SGG37554022', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B94A33CC42794B0627CC556615C0E1AB', 'D0869EE6AF1F3CB746FC24A7C44DD526', '高考录取查询', '41000002SGG95558001', '高考录取查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B94B518A21E0E190FBD6F8B223E3A7E1', '8B7615B158D68A5A524AB76774B3F630', '河南省审计厅信息公开', '41000002SGG86835001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B95CEA8FDA51B652E746661B03DC480A', '4F70A8321E1F44ED99983ADA119D59EA', '河南省航务局动态查询', '41000002SGG35866001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B97362600ED493857987055E6ACA7F56', 'DAE2A4A34D72DBF90BE43DD1EF572A7D', '河南省2019年军校和国防生招生军检化验结果查询', '41000002SGG49212001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B97E23D4C84D4F1A64FC4BB1282E3142', '9417B489A6ADAE152C71C581FC471996', '河南省公路管理局网上咨询', '41000002SGG21730008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B98126BFAF776EAD46712B6B6D86AC5A', 'A1776CF64C8CDE6E4CB81A5FCC4567CA', '建设工程质量检测机构资质技术负责人变更', '005184400XK5572500f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B9813FA21E97205C18811B2603BA6AAC', 'BE52D4B55AA450ECE0F0565C222F99EC', '人民防空工程施工质量检查（主体结构验收）', '005184427QT6050N005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B99726C83F4ACF202ED29D765BF5E2A5', 'C923DE4DDC9797FE75E84CEC09AB7D3A', '在自贸试验区内外商独资经营的游艺娱乐场所经营单位补证', 'MB1848628GG1310700a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B9980C47E209D086CEC8A1016ABB98F6', 'FC5F0F3DE7D94D7C3474936458DB7BC7', '探矿权保留登记', '005184224XK35465001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B9A16F8427338FDAEDA30F22A118E633', 'C1D82BB486BF887D5C3B1C65ED902420', '河南省林业局局发文件查询', '41000002SGG98738003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B9ABFA152169211506E0921E8F7A57FC', '63D75AB786A89FFCC15BF0EF430C97E0', '地质灾害治理工程设计单位乙级资质认定（变更）', '005184224XK1609100b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B9B149C5A02E33AF72D85AAC7FE38891', '3F73D23386FB13A18CCBC2C3FAA94B72', '省级辐射安全许可证法人变更', '005185040XK00172012', '省级辐射安全许可证法人变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B9C2B951603451A51725151E22A9E1BE', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证补发', 'MB1912669XK55718005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B9C8D37CA52F9588C27163020F238CE9', 'D9E9DBD37EEBE5A30704A77254ED20A0', '拍卖企业分公司从事拍卖业务许可的变更（复审）', '005184590XK18378004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B9C9A7869451BEEFB1FFC445292C18EF', '9FA143449C620C070B6E1FD908F5CEB2', '临时证户籍地址查询', '41000002SGG06012001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B9E15B626CD728BEA6AD49FE39023D7F', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动变更（经营范围）', 'MB1848628XK00096014', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('B9E3AF69EA84520132A1210A96938514', 'B9AB43F0556A700B5CA24A12E4CCDCA2', '首次申请安全生产考核合格认定（B类）', '005184400QR02355013', '首次申请安全生产考核合格认定（B类）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA13ECF7728C3C961624557108AEA9DC', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（电线电缆、人民币鉴别仪、广播电视传输设备、预应力混凝土铁路桥简支梁、危险化学品包装物、容器）生产许可证许可范围变更（重要生产工艺和技术、关键生产设备和检验设备变化、生产地址迁移、增加生产场点、新建生产线、增加产品、产品升级等情况，且不涉及产业政策）', 'MB1912677XK55651015', '重要工业产品（电线电缆、人民币鉴别仪、广播电视传输设备、预应力混凝土铁路桥简支梁、危险化学品包装物、容器）生产许可证许可范围变更（重要生产工艺和技术、关键生产设备和检验设备变化、生产地址迁移、增加生产', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA14F1EDCD1BD42935FE4C98A87A7906', 'D4BE3C18CBF1ADAB2D68F5AF68316627', '中外合资经营、中外合作经营、外商独资经营企业互联网上网服务营业场所经营单位从事互联网上网服务经营活动审批(租赁场所筹建)', 'MB1848628XK78400002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA20FFC80E66738D5A5BA52EC72E94F0', '2D2977D979F21D76CA3C6A14B4CA3136', '环保公众开放活动', '41000002SGG25151002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA38B922B8AFC8E9E9FC801CF958E97F', '213CAA3E44C4296906B16FB9A53DA768', '创业培训补贴标准查询', '41000002SGG60978001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA3E773B4A0A26F299F6721E7E2EF0B5', '1FCFF1B1D61424D6AAFE7383FC008AD0', '建设项目占用防洪规划保留区用地审核', '005184566QT00241002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA4A5D602A4F1888AA6B7A5F3946C8F2', '601C87161F995BBDEE76402DCF9F747D', '国家外汇管理局河南省分局特色服务', '41000002SGG12751001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA5F285342E2641181FF24F2BD10251F', 'C5018DF030AD133CCFC5C2FCA0468044', '食品生产许可证证书查询', '41000001SGG89294017', '食品生产许可证证书查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA71E519AD7A4973830F098359538211', '6C8975BBB882294116500D6ABBD61C52', '受理、审查法律援助申请', '005184291GGT6A1R001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA7F286166165B2DDA7481CA7E73735D', '6A0FA0F8E5B0562EBC55A0E171E55BC3', '畜禽遗传资源进出境和对外合作研究利用初审', '005184531XK94258001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA80D0EDB5B81C8611F7892F54E15431', 'CA97376B355D3020992F7B608C92376B', '社保卡挂失', '41000002SGG57811001', '社保卡挂失', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA924C3515737269CEB82C69234A8E90', '860AD9BD6D2E884AD9961726AC04D383', '三类医疗器械经营许可证信息查询', '41000002SGG76137003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA93CCD63BEB20F8A0AF27B3AF21813E', '123522338223A5C38FA6DC530B4B7338', '河南省教育厅关于对2019年河南省优秀教师和河南省优秀教育工作者评选结果公示的通知', '41000002SGG82612001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BA96C68BD57C8050645EDC905870A460', '2E545D72926FB86FD1DF5EC7DCC69597', '计划生育技术服务机构执业许可', 'MB1957883XK55801001', '计划生育技术服务机构执业许可', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BAA20152A7CA94732C710F821953D437', '54E2C68F7845CDE9E82835698EDD90BE', '实验动物使用许可（到期换证）', '005184638XK00104003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BAA2475819A78BEEDBA7EBC4D43A9F87', 'D7959D4DDC946DCA2185906EA82FD97A', '渔业安全生产宣传', '41000002SGG94615001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BAB0EE235703BE4DDEA4A548263E7C24', 'EFC54A32DF7246CD23B059BEB930122A', '举办香港特别行政区、澳门特别行政区的文艺表演团体、个人参加的营业性演出变更（演员、不含未成年）', 'MB1848628XK5596200c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BAD06B189F84678EA50DABD076993C8F', 'FFD49B2D50431CF59CC8744EB0C3D3CB', '待遇重复领取清算完成恢复养老金发放', '698732712GG66003001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BADF79BA12CE29021AA2BCDC37E424E7', 'F8E3C2A829C52E63A9AE2C27836A74D9', '公布省级统计调查制度', '41000002SGG02979001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB0F3E3AAD1E5E3FFB588056FF1769B9', '1BEE7CB71BC077D9713F9FACFEE53997', '台湾地区的投资者在内地投资设立合资、合作经营的演出场所经营单位从事演出场所经营活动变更（地址-租赁场地）', 'MB1848628XK5594300b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB192BC0665B06219E914F3F97E7665C', '6AF4666BB6DBA9748BA281B6F176633D', '水土保持区域评估报告审批', '005184566XK00251005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB1FE6E34C54B2C6AAA8EAB4A2C21B6F', '563AF1C1A9238505C004CEEEE0F23F12', '食盐定点生产企业生产地址变更', 'MB1912677XKEDPF8003', '食盐定点生产企业生产地址变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB2242BCB9976264231AFA8A62EF676B', '016D6FEF2A88EA957174ED0128EED5F3', '报废机动车回收拆解企业名称变更（复审）', '005184590QT00209003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB324B57A5069C704CC6F876B8ECB0A1', '19DC4F0A71426794BBB00603C3890C62', '属展览性从国外引进林木种子、苗木检疫审批', '005184558XK97519001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB355717937636F94D44D4AA6415EFBA', 'F8BE43FDE2346A4A09565F6154CC7985', '人民群众满意度评价调查', '41000002SGG43150001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB3B21FE1CDD8F2A6B719FE8A63BC2D7', 'A1776CF64C8CDE6E4CB81A5FCC4567CA', '建设工程质量检测机构资质详细地址变更', '005184400XK5572500c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB4186EB895AAF9F7C2637388B669E04', '6351E55B713043F92DEA0FA37103A735', '特种设备作业人员资格认定（取证）', 'MB1912677XK62933002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB49C5B6D2583BC2A369595DFA3EEDA1', '56AEABD46393711AF6CCBFF157815539', '河南省工业和信息化厅公示公告查询', '41000002SGG51227003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB4A17A5BA8E27D16F00CC375F99A940', 'ACE3DEB6F873805D4081C52A336E2A53', '药品批发经营证书信息查询', '41000002SGG27249007', '药品批发经营证书信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB68A0F287603EEFAB9A1C1823C89C64', '4F2F86D530A1E8CD6C233EC2ACC0ECE3', '河南省博士后创新实践基地设立批准', '698732712QT00081003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB6BF943E9F9C351BD6D71435920AAC6', '1FBFA1F52C736D35A5416B9C752968FD', '一级运动员授予', '005184726QRX8S3I001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB7B726DA04F3039DE60B0FEB87EDF99', 'F495F7090C94BB720620CC984D02D330', '参保信息变更登记', '41000002SGG90605001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB7D13F79A24BD69C1973DCD8C422BCF', 'E7B234C807C51669078A1C3BB3463566', '权限内利用国家重点保护陆生野生动物及其制品入药审批', '005184558XK68302001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB81BE15A345C93B3A71D0F5E9491B04', '3DA96C6C22D6FB7AC667833D2EDD99C1', '民办普通高校名单', '41000002SGG59928001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BB83B66397BE12E91C7D1D77659EB408', '76D6F1EBF680F8394259007D9D8DCB47', '专职律师变更执业机构（转入河南）', '005184291XK11640012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BBCF953CC2AB5C0F1B9C950B1B95BE97', '92C4D4FBC0C12EEF1827E8993E8D2CF4', '河南省星创天地绩效评价', '005184638GG06163003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BBD94566C9F1BD81368D148B5A4D6406', '175C971222104069558624D3087E5397', '广播电视节目制作经营单位设立审批（变更住所）(非自有产权场地)', '005184654XK00299007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BBF55DC5EA68751B14D434B5E5A0C466', '024C43635CD1AC64B204F3B32E027D77', '改变国内生产药品的有效期审批', 'MB1912669XK55819002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC0256C28D038046403EABD563DC65E3', '268EFF492C88003F026B347B1D6351F1', '卫星导航定位基准站建设备案', '415800560QT00324001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC05BB7298D78591BA403D424703639E', 'E83773975A45EB82B6DA27CE2D67A8FD', '法考服务下载资料查询', '41000002SGG76127001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC0705B51FA931337904E3135B70856D', '9F486FB4F662566702D9D0642DC56240', '河南省交通运输厅客运场站查询', '41000002SGG48254001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC0785E8B1BA3CC39C19E825B6383B17', 'B28702113C7EAD6D85EEA8EFEA97B2D9', '省卫生计生委信息公开', '41000002SGG31744001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC0F1CF5FDD9344BE57E956A4BC6045A', '453897CA1257AC052431166E2F56B428', '中共河南省委高校工委 河南省教育厅关于征集庆祝新中国成立 70 周年活动方案的通知', '41000002SGG67480001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC22655B051A3E34E2138938654F2ABB', '54E2C68F7845CDE9E82835698EDD90BE', '实验动物使用许可（注销）', '005184638XK00104008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC2E0550351A3594D9D398891FFC29CC', 'F4A8547ADC9E7425FB4C3DBE5C38E6A2', '广播电台、电视台设立审批(初审）', '005184654XK55702001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC2F23661711849E6ADE525F5FB71A35', '11ACD5A353E30FBCD3CAC964A98F1E63', ' 定制客运试点企业信息查询', '41000002SGG12120001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC3C0AF7E35A2B7BD27480594894E61E', '2BB0CADC373980AD6EE9E28C0D8A1FD3', '河南审计职称考试网', '41000002SGG33068001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC3CF65C2E990D72D94CFB96C8EDE1D4', '68451DF63317B98B82FF00463D92F9CA', '探矿权人名称变更登记', '005184224XK22442004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC5352AC43B83B7319A27C29706CEA93', 'ACBED3B4D3E2B736A3B4C5B20E9658CD', '信息网络传播视听节目许可证变更法定代表人（初审）', '005184654QT00287008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC53AA3E587AA76DF6CA8DE7836D4350', 'E69D637D47516967809D154AE5F7712B', '外国非企业经济组织在华设立常驻代表机构审批（复审）', '005184590XK55918001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC67C5B08264E778A859F7D1F3BBC086', '00A3A498AF128075BE4FE07DAF67B123', '食品（含保健食品）生产许可注销', 'MB1912669XK00216006', '食品（含保健食品）生产许可注销', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC705AE9EDA15C838C270A5568DEEBD4', '23AD95EFCA26FC69744F91D9891AB14F', '非国有文物收藏单位和其他单位借用国有文物收藏单位馆藏文物（不含一级文物）审批', '005184689XK03160001', '非国有文物收藏单位和其他单位借用国有文物收藏单位馆藏文物（不含一级文物）审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BC80285A720BE0DB531B2BCB916A0243', 'B043C5F466781BBE630CE9808479D221', '药品互联网信息服务审批核发', 'MB1912669XK00233001', '药品互联网信息服务审批核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BCB8D21A92121DCC1970BF06B1CD858F', 'F3FB7A60F23FFE846454F1ADD675CAFD', '危险化学品水路运输申报人员资格认可', '005184435XK55790001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BCE4F04029B7E6344F1B44021C2EF507', '472BD22570EE1C860F9A249E259A0561', '接收卫星传送的境内电视节目审批（省直单位）', '005184654XK55662001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BCFA0F42AAC982EE28C688FEE37DAEF9', '024C43635CD1AC64B204F3B32E027D77', '变更国内药品生产企业名称审批', 'MB1912669XK55819001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD050EF78A651DDDD0A2EED8DEBBC7D7', '2759F7790BA9EE53C07FED57DD9AD978', '河南省职称证书查询', '41000002SGG24780001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD07A5D7B138AC8BAACA621AD8A54E15', 'E7C36B352AD51C4B5FB34457C1AA39FE', '港航法规查询', '41000002SGG00544001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD109A2C2CA03062F216AB94A4F7C968', 'AB5C68ADFBE2E3CDCF6361F812E793A8', '放射源诊疗技术和医用辐射机构许可（校验）', 'MB1957883QT6341O001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD138A029959BE16EC428A181B3B7978', '450A91883ACB3FD62CD18F788AEF7061', '全国医疗机构查询', '41000002SGG89390004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD1A55ED010FD448DB8313EC97DFB2B3', '29BEDE59DD4502B60C19E812811E535C', '律师事务所境外分支机构备案', '005184291QTE3446001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD1D7EF32F99C66A4B6F9CA8F96CF038', '76D6F1EBF680F8394259007D9D8DCB47', '专职律师变更执业机构（省内变更）', '005184291XK1164001e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD34B95B5C6EC919259EEA1AC0734AA0', '45178A0278017908E60243E32117470A', '体育视频、图片查询', '41000002SGG92882013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD35F97819EECAB1618427739FA8858A', 'FE09B1510D8C790C15C8129246089F44', '注销原因查询', '41000002SGG67084001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD39D1B3BEA7F67FEBBD77AB1AB6E296', 'EF64454331616DE66C881EFF59E4A4B4', '高考录取查询', '41000002SGG66353005', '高考录取查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD478CF7EAEE8572CFB019DB4D660309', 'D4BE3C18CBF1ADAB2D68F5AF68316627', '中外合资经营、中外合作经营、外商独资经营企业互联网上网服务营业场所经营单位从事互联网上网服务经营活动变更（机器台数）', 'MB1848628XK78400007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD4C541039EFA45E304B54889F21942C', '628D42B0CD977081D8C468E7623C28F7', '求职创业补贴标准查询', '41000002SGG12262009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD5D3140A14DE50A64AAA38C0E21FDE3', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证变更统一社会信用代码', 'MB1912669XK5572000e', '药品生产许可证变更统一社会信用代码', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD6EED4BA5E45819D55F655DA9ED0901', 'E245E4E82DF88F0851C6FC41D1531426', '农作物种子生产经营许可证核发(A证设立)', '005184531XK96500001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BD8A0B01614B863E7B974F59D1105123', '42EFB01138010A18BD2DC8EBCE0CEBE5', '乙级测绘资质延续申请', '005184224XK0032900a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BDAE0C130BA787C2760C7CD9F4CE013B', '97220A9271559926249286F3E4B23CD2', '种畜禽生产经营许可（注销）', '005184531XK06435006', '种畜禽生产经营许可（注销）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BDB96192AB4BEF503AF672F5663BA887', 'EC12E989B3565BEAF16149E170B49BE7', '首批河南省制造业创新中心培育单位名单', '41000002SGG38631001', '首批河南省制造业创新中心培育单位名单', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BDBC4FB0DB712BB238597D6F2B94481C', 'D86A1F3246F843EDE7A51CCEAC37618C', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动变更（地址-租赁场地）', 'MB1848628XK55961011', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动变更（地址-租赁场地）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BDC48587A9BD7DE30A70FB5954731A94', 'BFB89262A855E2305396FE7187950AAD', '车用气瓶充装单位许可（延期）', 'MB1912677XK55653002', '车用气瓶充装单位许可（延期）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BDDB841C0407DF2114655DF0D290F1B6', 'D0E40284EC9C9663DD50C4001D0B139E', '举办外国的文艺表演团体、个人参加的营业性演出变更（场地-在歌舞娱乐场所、酒吧、饭店等非演出场所举办的营业性演出）', 'MB1848628XK1806900c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BDFE490EE167896B58A9C5942753F84D', '05033A26E9D75C64B22282FCE4B39280', '一类放射性物品运输备案', '005185040QT00174002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BE088D314DC15CCE631262472CC9EE6C', 'A501DF343313F4152EC41E48E650A8C2', '地质灾害乙丙级资质查询', '41000002SGG85206001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BE0933A8122F9BE3FB6ECF7079324AB3', 'AD8657E89D8AF24BFEF228FC01D2A866', '国家税务总局河南省税务局人事信息', '41000002SGG99547001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BE3751E489C267AA464D660DF0E8E4C1', 'D8B7955D44BFD1E348168B4F27F9821E', '中华人民共和国出入境通行证业务进度查询', '41000002SGG54984001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BE382845939400FEF3B459DA24391780', '5F8424CE9EC2E0A3BDAFA352A37E53C2', '中药品种保护初审', 'MB1912669QT00237001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BE47C830953EA586251823799251446E', '628D42B0CD977081D8C468E7623C28F7', '就业技能培训相关人员报名相关政策查询', '41000002SGG12262012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BE4F7E9B9C368EDAD8A6D140A68032B5', '6460D06FA8702F8874FF8C545E860B8F', '水运工程建设项目初步设计及概算审批', '005184435XK55731003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BE55193AA1C09E175A50784B3AA30CE8', 'E37EB7A8109464C5106178777B2A2896', '限制使用农药经营许可证注销', '005184531XK93138004', '限制使用农药经营许可证注销', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BE6DE7B3C1C63B0EDC2A5F68474A7D34', '1D6F4ED04B44FD8C22C6E509F7B53314', '河南省中小企业公共服务平台化工行业资讯查询', '41000002SGG69935001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BE9F1313CD9165DAEE8C5B0B84AE21E2', 'B99EF053D57CB3FC3AB066E16514279F', '河南省交通运输厅统计数据查询', '41000002SGG82288001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BEBCC4C9B005AD4FFEE1292A99BDCAA2', '1DBA1EA2BCEF1CBF8CF90D4188A2A052', '河南省图书馆历任馆长信息查询', '41000002SGG99783001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BEBCC76BFAC2E457920A0EF784278607', 'EB16870A069D2358EF255FCD9AF0D445', '出具文物影响评估报告', '41000002SGG79166001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BEBF8D99DF5DF41F8AFB405C985BB592', 'BD2AA32002A93C686ACAC7D0181F9040', '不动产登记信息查询', '41000002SGG02305007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BEC1E6AB58A2F496FBBF765D13C918BE', 'C8AC7809F7EA1C63885CCF7F9877227A', '打假举报', '41000002SGG02911001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BECE90A6739C331F77C52B2CC7C7B66E', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证配制地址文字性变更(原许可条件未改变)', 'MB1912669XK55646011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BF28E5A6AE98DC891468F7BE3F3666E8', '69ACE9773F9DBA89B44AEE57D7DBE870', '河南省教育厅政府信息公开', '41000002SGG89680002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BF50F0C5C0EBA6A7E1C894BA1E7A7E71', '1FB9C66E10DC7B91CF17F92BE17571B6', '河南省档案馆意见建议', '41000002SGG90658005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BF54D1DCCE5B45F867780C9E96DB0662', 'E27919616185837BE8B39737E4FA5F1B', '施工图审查机构技术负责人变更', '005184400QR7380900c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BF65917BA658F0C3E1929FEAF90DB84A', '8A9A82758BF33235BE654E8AA5087891', '河南省防雷中心查询', '41000002SGG97829011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BF666D4891AF10DB966B3854241C0680', '04AB80E847CEFD3F812174109D8EDCC6', '外国人来华工作许可注销', '005184638XK56009005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BF80010351C600CEE80E66E8FF92731A', '8B73C298E9DC3C9721B85E07602CACC8', '普通高等学校教师资格认定', '005184662XK55917001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BF9BD9E806780E9A59F310BA47FFB8B8', '0CA81AFB77AB1DCCAEF0E748578F09B5', '往来台湾通行证号码查询', '41000002SGG03719001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BFA06ED9E97D810431F77DEC9A447B30', '7342660123B43F44480FA3707071C532', '河南省农业农村厅视频新闻查询', '41000002SGG61100004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BFA30AFFBB2E5C63FBB6FF5D9F74A5A5', '9D9DAA743A0BCBA7ADA27FE5E3A7B8AD', '地质灾害危险性评估单位乙级资质认定（补证）', '005184224XK1046600a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BFA8C1FA8BE75633E67E1909ABA22CE8', '2E6784788D59549E3E810E44F689C3E9', '医疗机构变更名称', 'MB1957883XK55857002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BFB53B45FA307AE7FDB78040B3CB0FEF', '2A1129964340B6A77C4A2C16C11FCCEF', '采选黄金矿项目核准', 'MB0P03925XK55679005', '采选黄金矿项目核准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BFBF1420FDD957AA1B128A1940F38FDF', '978BDAF50032300E1E4DBC1BE8327A51', '身份证民族查询', '41000002SGG80449001', '身份证民族查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BFC46CAE10197B6318E30333528555AE', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证配制地址变更', 'MB1912669XK55646013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BFC4BAEA57CE3BD9B04379058C2AD660', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更生产许可项目（核减）', 'MB1912669XK5571800a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BFE7B321BE6EAC234E87326F7865FBD4', '1B74EA67CCE9281340DE72E177DCFE84', '营运船舶检验', '005184435XK55995001', '营运船舶检验', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BFE87BA9BF17616F27FB33121EEC0A7F', '76D6F1EBF680F8394259007D9D8DCB47', '重新执业申请专职律师（曾任法律援助律师）', '005184291XK1164000d', '重新执业申请专职律师（曾任法律援助律师）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('BFF06C32FBF9A49CFCA0AF492A86192B', 'ACE3DEB6F873805D4081C52A336E2A53', '药品生产证书信息查询', '41000002SGG27249005', '药品生产证书信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C006E93B1528E60B96E25648D8671706', 'A1B24B0454823D54861A4CDA07A748C5', '河南省残联精神残疾服务查询', '41000002SGG68528005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C0084932D0DC87718CC147663DCDC461', 'BD82378ECB30699BF9E8D5CF6F1736FA', '普通护照号码查询', '41000002SGG95288001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C00E907EEC2E4C3BA4986D2370098B2B', '9935C2015F86AAB3077855FEFE094964', '驾驶人记分查询', '41000002SGG55737001', '驾驶人记分查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C0117831E774EC92FDD845F921D02A43', '2BCDD0FC019B2EA7CC36CD1EB440F554', '执业兽医资格证书核发', '005184531XK05654001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C01E35F7AAE0D882D6D987D27E6BD0A7', '628D42B0CD977081D8C468E7623C28F7', '就业困难人员申请政策查询', '41000002SGG12262011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C0216E4BDA6718174BDC8BDD8F5FF4D6', 'F5EF19BC786AC1D926559D5DB0166B64', '澳门医师来内地短期执业许可', 'MB1957883XK55867002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C02CAB10422717B72483FBFE1E63A902', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动注销', 'MB1848628XK0009600e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C02CFBB2A66EB98781C9957B78E34E69', 'EE30EF5FB7426C4A4617ADA7767E5B5F', '河南省财政厅政策解读', '41000002SGG84033001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C02E80E02EE4D9BECC5E710FF9B6A20C', 'BE9A6EA7ABFDFF0ABCBBBE9790A2A353', '河南省运输行业公司资质查询', '41000001SGG12367002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C052D2A360B83D1101E23D315530B8AA', '20C677BD0AA82CBBA99826B94FBD8653', '历史文化名城查询', '41000002SGG30838005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C05C4B5C147ADEAFA8A58B114499F445', 'A9C89515506EEF92AC5B922BC3773E57', '困难企业职工欠费补缴', '698732712GG33232005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C06356604AD5ABD4D26DF8D8C28FB768', '1ABF5A6FB11317D0A909A52480ABFA7C', '河南省图书馆共享工程动态查询', '41000002SGG58243002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C085935B3E39A9A94B721B43238797A8', 'E37EB7A8109464C5106178777B2A2896', '限制使用农药经营许可证申请', '005184531XK93138001', '限制使用农药经营许可证申请', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C0AFB730E32A4460F75AF6DD8656EDAF', '00A3A498AF128075BE4FE07DAF67B123', '食品添加剂生产许可延续', 'MB1912669XK55817004', '食品添加剂生产许可延续', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C0B501F986B6629590275048418A4B2E', '465253A52B1F1938A1554295EA495A57', '定期公布设区市统计调查项目目录', '41000002SGG23205001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C0BC3FA6CADABF184DD70C5CF2B3A63C', '210868935EBAD2BCD4821021BEBF9656', '检验检测机构资质认定授权签字人变更', 'MB1912677XK55650004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C0C0464674C720EFA85798399E6DFD7C', '1FC267BB24F0A62E7B7789E78D6A6FC0', '计量校准', '41000002SGG86206001', '计量校准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C0E1DDFBD11415631AC44EA7658AB960', 'E4EE6E1ECF5C561B5847FE8493C9C701', '招收外国留学生资格高等学校名单', '41000002SGG87875002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C0E89D3F233A5BAFEFE3E9911627BAE0', '8B93D119C1C021F68D4B5DBE2F397B79', '健康档案', '41000002SGG00536001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C0F19A4AE798E2B4785B2D656A9AC645', '121D559201836004A8EDF7FE9B8055A3', '省级非物质文化遗产代表性传承人的评审认定', 'MB1848628QR00019002', '省级非物质文化遗产代表性传承人的评审认定', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C0FB3790ED51DB31973B6AEAB5172C96', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省殡葬管理信息查询', '41000002SGG37554009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C101EB576615E8928237E7ACBD0F9E87', '8C35B97822C64BBFF8FB2F30E55ABA7B', '医疗机构开展人类辅助生殖技术许可变更地址', 'MB1957883XK00270002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C119A0D8C1C9756965901D3CEBE99C21', '3F73D23386FB13A18CCBC2C3FAA94B72', '省级辐射安全许可证补发', '005185040XK00172019', '省级辐射安全许可证补发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C119E9684029B2C6A7480861BB20925C', '6329329D4BC6A1F1EE95471A375BAA0D', '矿权网上交易公示', '41000001SGG63384001', '矿权网上交易公示', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C11E6FC6F402DFAB17D5AB96F9CD2155', '66166E4E889FF9A7822DB5BC40DFA025', '水利工程质量检测单位乙级资质变更（企业名称）', '005184566XK9726800f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C13341391EAB6D16F41496969ED65935', '966A525DEAA3FA96F33EEB50D06FBC59', '特种作业操作证及安全生产知识和管理能力考核合格信息查询', '41000002SGG30821001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C1349A76DAF30751050845E42A784429', '93D4C4DB8BDE89F4D40DE7B9ECCE70CE', '单采血浆站变更地址', 'MB1957883XK00266008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C1851E4C851324743E3FA8E065B22A4F', '8031E4CBFC7C992F5C496EDD51C10F21', '中职学籍查询', '41000002SGG33213002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C1A5AA86F754754CA267E3ED4AA86A95', '0CAA5CCD5B2ED113457D61D7FED04571', '一级注册消防工程师变更注册审批', 'MB1D56024XK65409002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C1ADA69BF2FF16E2063B35858F7D2421', '1F0DBFC950D8F2766D90C35B5BC122A9', '教学科研单位购买第一类中的药品类易制毒化学品审批', 'MB1912669XK55736002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C1C78A1AEA1C13B8CCB1B973FFC047AB', 'E744416281FCE38299280C9923ABA5E5', '个人权益记录查询打印（机关事业单位养老保险）', '698732712GG33235002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C1FF810F8A06451188B1CD82DF679E94', 'BE52D4B55AA450ECE0F0565C222F99EC', '人民防空工程施工质量检查（单独修建的人民防空工程竣工验收）', '005184427QT6050N007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C208300BBE1A7B6C0A1E7B8A1E98458E', 'C6390A8135998641CBC991CEDAABC31F', '申请工程设计资质升级', '005184400XK83829002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C21257E371F38967848B6A103AE5C273', '8879273E37EE7AC19957139A0F51BB45', ' 长途客运网上订票查询', '41000002SGG02832001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C2152942058C82EF6BAD59BFEA72CE3A', 'A1B24B0454823D54861A4CDA07A748C5', '河南省残联肢体残疾服务查询', '41000002SGG68528003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C233439B26FAA38D327D5CF238FB2DBB', '9051F4475734C7ABB8181D3BE0F73B96', '医疗机构开展人类辅助生殖技术校验', 'MB1957883QT9974O001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C2498A57C1F01C935ADFC125DEFAA221', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '律师事务所设立', '005184291XK5580900c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C25E275C8F0F2E02955862595224119D', '210868935EBAD2BCD4821021BEBF9656', '检验检测机构资质认定取消检验检测能力', 'MB1912677XK5565000e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C27237564B3CF4596BF356DC3129104D', '7648BB9EA44C72617BF3DA4ECE737570', '博士后科研流动站查询', '41000002SGG17642001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C280E35AAE4ABF899A27EFCEBE891FE6', 'CAE4AD67E2965BDB56A9AB3E2528C39E', '二级注册结构工程师注册证书查询', '41000002SGG74794001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C284C7BB91B7BF151BFE7C6FB14B0C3F', '144007212DA7BA6598CD26246DEDF3CC', '法律援助办事指南', '41000002SGG39589001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C29CE4E1448D5C314B7E0C02D160B8F0', '3985FA81C2A327E2574AD14CBA9BF750', '职权范围内的医疗机构评审（西医）（省级）', 'MB1957883XK03693003', '职权范围内的医疗机构评审（西医）（省级）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C2B50299F048325B3A040D8652DC3D43', 'D9F89BDDD8C5046DFC8BCA91E397F580', '地质灾害治理工程监理单位乙级资质认定（延续）', '005184224XK53924004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C2B5255A2BFD3DC2C24233424C8229EF', '97F33ECB9678A0580191D152C7AE919A', '现役干部、士兵转改的部队文职人员参保登记（机关事业单位养老保险）', '698732712QZ00659013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C2C14952151678190139E42FCD20C93E', 'DBC3D7CFE77564A60F87FBFC49798AF1', '河南省事管局政务公开查询', '41000002SGG74847011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C2E79F626708E22386086A0E63DA8C22', 'A5FBD0C97A12E5071CC1A0F22F3951B6', '管理局测试子目录测试事项111', '000000CSJXK54601002', '管理局测试子目录测试事项111', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C30B1875047CFD8189D300AD9DF389CA', '770D26852BFB1FB3995834B1FAAE65B4', '中外及与港澳台合作办学机构设立审批', '005184662XK55911001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C30DB550A74D69F3D258047B14CD50C4', '30BE103EBEB8E993E5F4C4CAEB4C372D', '外商投资旅行社注销申请', 'MB1848628XK00199003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C33AF15A5411E8D9C2483997FB042B1C', '4D683B2439A70CE8F401F506ECD4BE76', '房地产估价师注册信息查询', '41000002SGG12459001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C36DCE24FD6160B6138FFF4A0F59D5E9', '58B8696831A7C6EC5E41430A925CC2F8', '从事经营性互联网文化活动变更（股权结构）', 'MB1848628XK02935006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C37991E8B07C875442D90C66C59ED504', 'FED7D4BD65BC1CCF3535BFB40893B4BC', '中外及与港澳台合作办学项目设立审批', '005184662XK55912001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C38041C9B3EC08F621CB15A08EE4A07F', '826D3C45E1A34A2608834C6CED7B1971', '农业转基因生物加工审批(变更)', '005184531XK97619002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C3E8C3625707FC3DC9B9D09ECCFD13C1', '8DB0F4F4FFE9241FE4642ADF34C1BE27', '企业实行不定时工作制和综合计算工时工作制审批', '698732712XK00120001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C3EFD97CFDF089EAA93B1E368D9CA2F0', '4A69BA6BAFCF104471659E7668FD4C34', '水运工程材料类乙级试验检测机构等级的资格认定', '005184435QR13082005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C3F49F73FC7356F8259AF5B8D3D7D163', '45178A0278017908E60243E32117470A', '省体育局应对疫情相关政策查询', '41000002SGG92882006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C3FEBF22FB90B0BCAAAFAC167C1E8760', '60B10F8EA983D9FD0EFAE102BE71D0D2', '旅行社信息变更（出资人）', 'MB1848628XK00198001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C3FF15A4ED42C8372B7616F304A00D92', 'B043C5F466781BBE630CE9808479D221', '药品、医疗器械互联网信息服务审批变更网站名称', 'MB1912669XK00233007', '药品、医疗器械互联网信息服务审批变更网站名称', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C40C2A4ECA2EE6D2C76E3C0D8E694E41', '0EB5CF7DB9F412D7C2C1D19E9C9C420C', '河南省农业物联网大数据信息实时查询', '41000002SGG97623001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C40E182638D49C4028EE4FD95B46EACC', '1DA951DCC3DCF0A2CA9FAC2855F8A2C7', '中等专业学校名单', '41000002SGG62956001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C4442EE12144EC30B3E64130A421B5D9', '6B1F3AF94E258B06A24E55F1BF50E584', '河南省拟向工信部推荐服务型制造示范企业名单', '41000002SGG53414001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C4491B41C26485A3D87E4BC8F3537DD4', '1B4A8E8E6B977C71480C43CD9892ED72', '二级注册建筑师变更(外省转入)', '005184400QT59418006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C4684969083703FEB32E63D3AD1EFB84', 'DF15FEF333CF658096EA22932C754F83', '通信报装', '41000003SGG22899001', '通信报装', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C47D1743505E7314A2EE584FE84C17C4', NULL, '在公共场所开展环境保护宣传教育活动2', '41000002SGG03007000', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C47F0698FF709FBEAD32C87D246D4716', '0EBA0F42D82FA5AB32547430DDC162A0', '血站设置', 'MB1957883XK00268001', '血站设置', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C49E323914CD6DD121DC166C1EFCE4D7', '8CDC00C7AE07F0F4B1CFD668A8E0BF3F', '法治宣传教育', '41000002SGG56461001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C4ABE577552EA0F7BE204950D5D1CC56', '1B4A8E8E6B977C71480C43CD9892ED72', '二级注册建筑师变更(注册证书企业信息变更)', '005184400QT59418009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C4B47529FD006901E796CEDAC72E2C8A', '5C9C99BF8FBB6290DD02C7B6AB7F7569', '第二、三类医疗器械生产许可证核减生产范围', 'MB1912669XK0023500b', '第二、三类医疗器械生产许可证核减生产范围', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C4EAD4DB474F3F5C36B41AD198167452', 'A5C20E95B60D796CF62C9C5673DBA50C', '中小学教辅材料零售价格审核', 'YZ0000000GG33188001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C4F1C614DE5267A34A0DA5E722624160', '4EF5B24BBF74465734F2CA278F2D899B', '在地方媒体发布兽药广告的审批', '005184531XK12853001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C5033A369B7C7D5BBA904016042D7BF8', 'D74DF9492DC188FB89A0807492C553DE', ' 客车类型装备等级查询', '41000002SGG63597001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C52701D2B5C7770213AB448C3EA47972', '187D237D8FD90CF36D361DD7B9CC1AA8', '二级建造师执业资格变更注册（企业信息变更）', '005184400XK83761007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C53390BFD26C046DB139744A5BC98B40', '2D0BBB663B32174B050689FB75CBA29B', '母婴保健技术服务（产前诊断）机构变更项目', 'MB1957883XK00279005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C54206942DF9C21E8300EA495898B904', 'B1837989517CD13BF176BEE7824A5DD1', '特种作业操作证信息查询', '41000002SGG56683001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C560149461A3AE16E95848C9A9EC8545', 'C06C81E53712D151B9400E7B6DA14BB2', '邮寄不属于国家所有但对国家和社会具有保存价值的档案及其复制件出境的审批', '725831987XK36348005', '邮寄不属于国家所有但对国家和社会具有保存价值的档案及其复制件出境的审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C5652F9A6ED52FF445CEBF610E866CEB', '2EC4EC711AB603EB0B535135E4E658E9', '电力信息发布咨询', '41000002SGG89673001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C567C3AE1D79D220CC66BA9E437E8434', 'CF1CA85A9BA171A4536DB4EA32B4C590', '煤矿特种作业人员操作资格注销', 'MB0P03925XK55659006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C5963C43569E563F88A4BDB07B67DD3C', '135C2343A2E0A6DBDD6A4D728D3ECE8D', '测试05', '41000002HGG12503005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C5A9A4A809EFAB8B6B77D02000AF69A8', '131AFEE53EA53913F153AD4613AD7F68', '省级行政区域内经营广播电视节目传送（有线）业务审批（注销）', '005184654XK40260004', '省级行政区域内经营广播电视节目传送（有线）业务审批（注销）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C5AE27D196919BAFCE06DAEF68F2DC36', '60AFC728F2F986D2E956AAE8179662CE', '组织农产品产销咨询服务', '41000002SGG20980001', '组织农产品产销咨询服务', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C5AFCA216A13258B490297E559FAB5A9', '5C3E645A610D16DE563E3B9C8CD77BB6', '二级注册结构工程师变更(注册证书个人信息变更)', '005184400XK0929700b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C5C48CBD6A8FC35E32A92C757D2A612F', '002447207831DA53248577890AA4AD3B', '文物保护工程监理资质乙级证书名称变更', '005184689XK9427000a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C5C909E72B6E3923FE8CB8EE7DB4A24F', '9D9DAA743A0BCBA7ADA27FE5E3A7B8AD', '地质灾害危险性评估单位乙级资质认定（注销）', '005184224XK10466006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C5D8D2BB680D699261CDC4EA23E2D46D', '9A9A48E1D29AB0E374272F673F7936C3', '体育类民办非企业单位申请注销的审查', '005184726GG33171001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C5FD1B1F90003E0A0F424DFDDD761852', '002447207831DA53248577890AA4AD3B', '文物保护工程施工资质二级证书注销', '005184689XK94270015', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6097213190C1AC00EE79903F63203C4', 'B20953726B6D5723DD2546403D17C0DA', '农产品质量安全追溯视频中心', '41000002SGG86757004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C60AE62DEF2110F39660C6837DFDE24A', '472F7FBAFBCF9D5D6AB0212817EEEE56', '省体育局公共服务', '41000002SGG78500001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C618229DDF39D7C81325B8A6A1CFA5DD', '3419F5AC8A04C5F961816B6E272CB872', '水生野生动物保护宣传', '41000002SGG75742001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C61EBB540C36F4EE6A16A1BDF2B67E81', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（危化品、水泥、建筑用钢筋）生产许可证许可范围变更（重要生产工艺和技术、关键生产设备和检验设备变化、生产地址迁移、增加生产场点、新建生产线、增加产品、产品升级等，且涉及产业政策）', 'MB1912677XK5565101b', '重要工业产品（危化品、水泥、建筑用钢筋）生产许可证许可范围变更（重要生产工艺和技术、关键生产设备和检验设备变化、生产地址迁移、增加生产场点、新建生产线、增加产品、产品升级等，且涉及产业政策）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6206BB56677D5457899B57D61D4E251', '2533EFC19AC17012A3087D6CD706931C', '运输行业客运场站查询', '41000002SGG22591001', '运输行业客运场站查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C62526F6B110F73BE15D7E3D3B0215CE', '27DEF9D1E09B677EE899D5BB11DD0E37', '建筑施工特种作业人员资格延期认定', '005184400XK55735002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6324F6973F2A5B1A9738A53CBBF21B1', 'E2259407A2F50F8AAD558285F6F7BB48', '河南省中小企业公共服务平台法律服务案例分析查询', '41000002SGG80640002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C641E23333615630AA8AC32693E9EEA3', 'BC9CAC8FA696B37F39429CBA5C151663', '高血压患者健康管理', '41000002SGG40417001', '高血压患者健康管理', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C649E624F7F8A0F21390D962836996C2', '563AF1C1A9238505C004CEEEE0F23F12', '食盐定点生产企业证书补办', 'MB1912677XKEDPF8005', '食盐定点生产企业证书补办', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C65257AC5C2E4484AB4C65C9E9E37A58', '10DA917A368A6FF10781F842C0D17D0B', '企业离退休人员基本信息修改', '698732712GG33227006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C65653069C0FED9E61F21D7016B85A73', '6D1A849B77120866144F4F89D78F599F', '司法鉴定机构延续登记审批', '005184291XK55905005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C664331E8911D7518AFD5587001BBE05', '754004025F17D26D8ABDBAE237CAA73D', '河南省发展改革委员会精神文明建设查询', '41000002SGG23547029', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6646C910DC5C3C4C7DD6CA3EE76A694', '42EFB01138010A18BD2DC8EBCE0CEBE5', '乙级测绘资质证书补领', '005184224XK00329011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C66472D96D1D22FB5734244BFED867B0', '73DFE08455096E78CC382AFD9064C475', '河南省公共资源交易中心曝光台', '41000002SGG02423002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C66724F71193A48696529E8559DC2CC8', '754004025F17D26D8ABDBAE237CAA73D', '河南省发展改革委员会财政信息公开查询', '41000002SGG23547019', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6755732781088D211B45F2F947463BF', 'CB6CE6EF4381602C1A69014D6B001DA1', '音像制作单位终止制作经营活动审批', 'YZ0000000XK25874005', '音像制作单位终止制作经营活动审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C696D9C519FB2A6F301BCEC2FDB18708', 'EA1ECEFA6640F001EDC0C9903EC7AFFB', '特岗教师面试成绩查询', '41000002SGG27965001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C69CDFBF6E566147262E94C855AD9486', 'D0ACAA29DA10E0CCA692EF0F5DEBCCB1', '河南省专精特新优质中小企业库拟入库企业名单', '41000002SGG40859001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6A8EACF14D94ECCC4B969F3BDB45DAF', '370496933CCE52C470B0C82736FC227A', '宗教政策法规宣传信息查询', '41000002SGG59185004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6B14F074EF21B428686DFC80FCA4A05', 'DD6FCAFB9287445EE8C3BE870F06F74F', '在高速公路立交区设置广告设施许可', '005184435QT00308004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6B65D8BAE5C0366B3A27D1F3C694AD0', 'EA31267150BD07D7A9FC3B09AAE42033', '会计师事务所分支机构设立审批', '005184603XK04789003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6B6742B43322245F9E7AA334D3A760B', '76D6F1EBF680F8394259007D9D8DCB47', '重新执业申请兼职律师（曾任公职律师、公司律师）', '005184291XK11640017', '重新执业申请兼职律师（曾任公职律师、公司律师）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6D342B97AC382011A7B6F34D74BF4DA', '9F3EE808C088287B8BA3203101CCD79E', '非药品生产企业需要使用咖啡因作为原料批准', 'MB1912669XK55787002', '非药品生产企业需要使用咖啡因作为原料批准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6DAB89AA6D2B343B6858A3891BD3AAB', '2CB826D6539186FAE26D8A0CC8B19F54', '台湾地区的投资者在内地投资设立合资、合作经营的演出经纪机构从事营业性演出经营活动变更（地址-自有场地）', 'MB1848628XK55952008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6E4204BFB7E8AD0DA3296098AF6B2DD', 'EA4AE1D86B87A01C495E664BEC8CDDD9', '司法鉴定在线咨询', '41000002SGG91443001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6E60CDF6F5AF125EB2706979448238C', 'DAB301DDC04E0D58DCD9AA1BC0217944', '省级建设项目环境影响评价文件审批（非辐射类且编制报告书的项目）重新报批', '005185040XK49971008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6E8B548ABC1D2F9143090350C0DE410', '6645413D1AC4FEB48770867390ABF1BF', '省网络安全情况通报', '41000002SGG60890001', '省网络安全情况通报', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6F3A4635E90AE8B497D0ED857575906', 'AB5EE0A092C5C6D55624BE89219496F8', '外商投资和国外贷款项目进口设备免税确认', '41000002SGG48961001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C6F9EF3ED8FB5AE85D68A24D21D36C1B', 'BC60ECBA4AE58BE7AB7CC8C611863392', '融资担保机构减少注册资本（审核）', '687145571XK00368005', '融资担保机构减少注册资本（审核）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C7093362D1E4110D6B2169CBD5530298', 'C6390A8135998641CBC991CEDAABC31F', '申请工程设计资质增项', '005184400XK83829003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C71E8319DC94B732B1EBF0F0BC3BD7EE', 'CD29F591E8D1A5AB0327FC371498033D', '河南省交通运输厅机构职能查询', '41000002SGG29212001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C7222A9B51179B244DB79567F0D7FFE1', 'BEDA49426421270C0FE015ACD28FC277', '改变第二类监控化学品使用目的许可', 'MB0P03925XK55640001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C731A751C1845BB984871875C19C0061', 'A587E3E60F316BCC3E010F6A737510DC', '避难场所查询', '41000002SGG80179001', '避难场所查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C732F2508142E52EF72CBCE88898F57F', 'B9AB43F0556A700B5CA24A12E4CCDCA2', '安全生产考核合格证书变更（企业信息变更）', '005184400QR02355017', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C74EF02990293DC85BD852F65527C5A9', '1B4A8E8E6B977C71480C43CD9892ED72', '二级注册建筑师执业资格初始申请', '005184400QT59418002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C75188F3AC582954DE41C2F35A2B8079', '9F042634B4DC18ED5F4429A39286B29D', '基本建设工程文物考古调查、勘探审批', '005184689XK16297001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C75EC3C5D5F88C062162967FEBB9351C', '2AA026FBFFF3A436E49AE1816A7634FE', '省级体育彩票公益金资助建设全民健身工程', '41000002SGG05461001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C77117738D84FF92C98CB7BC87D3EC7D', '8379F57FC8D784909B51E9E56B4A20E9', '高速公路违法查询', '41000002SGG22182001', '高速公路违法查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C7AFADBD94ABA8980937CFCD05CB35B1', '7B84BE49A363E3F53468F21A30660FA4', ' 客运班车资质查询', '41000002SGG16134001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C7B39C30ED900B8EECEF26C6175E7CE7', '2C93EDF0DC13FB83CC5A1D3E74AAC7E5', '河南省交通运输厅信件统计查询', '41000002SGG00029001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C7EA7C59D397C0742B9B57F7BF0B587C', 'DFADED736D9EBD1A4FB4F38E6356A46E', '行政审批事项的办理进度查询', '41000002SGG40279001', '行政审批事项的办理进度查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C7ED99E4EDE279B596CD98FC5B00F37A', '0B590421C7C672EFADEC582F0622EB0A', '省级文物保护单位原址保护措施审批', '005184689XK86709001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C7F018E1B9F114153E3078B48B669C59', 'C5018DF030AD133CCFC5C2FCA0468044', '非医用口罩产品质量监督抽查结果查询', '41000001SGG89294023', '非医用口罩产品质量监督抽查结果查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C80CB8114D579D16E9BFC2E85C7831CF', '91531BED9962C5757AC239177CA9835A', '社会保障卡简介', '41000002SGG70243002', '社会保障卡简介', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C8107F3584DBE530D05EB93F78A538D8', '5330E67B0D62F249896CDE1AA7F13D06', '小额贷款公司减少注册资本（审核）', 'MB1530417QT0036900a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C820BC8EEEA7297CFFEC4A8585D5685E', '0EA47F1AAA87CF8048B90CFB85AEEF2C', '企业离退休人员因病非因工死亡丧葬补助金、抚恤金申领', '698732712GG08062004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C824C9DB16720AA5E94788C319F195DA', '2A8E55CC8CBE1700192B5D9AD97AAA5D', ' 班车客运企业资质查询', '41000002SGG72926001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C82D5A69889843A093953D76A10A96DB', '135C2343A2E0A6DBDD6A4D728D3ECE8D', '测试001', '41000002SGG12503001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C84A641E318C4DF62888C0430DB9EA5C', '04AB80E847CEFD3F812174109D8EDCC6', '申请外国人来华工作许可90日以下（含90日）', '005184638XK94086007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C85DACD5B772D5F1061755E5BD4319DF', 'EA0DB6AA2865B27C1CAE7F1D301110C0', '河南省财政厅服务企业', '41000002SGG03451001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C899DFC5E8DB31F3494560D2C578F8E8', '3985FA81C2A327E2574AD14CBA9BF750', '职权范围内的医疗机构评审（妇幼保健机构）（省级）', 'MB1957883XK03693002', '职权范围内的医疗机构评审（妇幼保健机构）（省级）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C8B488CAC649ED6E5AC5BEF8585B80F8', '3E3EE8FB606795741E4BFDEA15C2DC3E', '非经营性互联网信息服务备案审批', '726991560XK07741001', '非经营性互联网信息服务备案审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C8C1DCA7E477474DE84DF90CDDA2CA5C', '1FB9C66E10DC7B91CF17F92BE17571B6', '河南省档案馆档案业务查询', '41000002SGG90658003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C8C496137EB08ADAD71D36CBD4A8002C', '001D1CDF2F4EDAD6C86BCC527509310A', '机动车违法查询', '41000002SGG90281001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C8D601E9BE2443D08DC229B0181FAF18', 'F1A2A4952039EF616ABA65FBD621B07F', '河南省教育厅河南省消防总队关于做好 2019 年学校消防安全宣传教育工作的通知', '41000002SGG70925001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C8D60BAE77740FEC03F58FF40D866846', '10F0FA4CB3E8DF07FCE1B426DA3BB6FF', '河南邮政用品用具监制产品目录', '41000002SGG95745001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C8E0237544A2E88819F379F3E96C7CCF', 'A1FC122A99103F5256366B7834A934DA', '单位社保补贴标准查询', '41000002SGG25598001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C8E3C34AFA7E7E292870BCE88D22F6D3', 'EBB9CD62AEE269A9A6F903141B328E44', '观赏树种技术推广及咨询', '41000002SGG34015001', '观赏树种技术推广及咨询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C8FE3A6308954E814C473EB5B84B37C6', '42FD49E4AF85D58F93E81BAA109A66F3', '首次进口非特殊用途化妆品备案', 'MB1912669QT1462N001', '首次进口非特殊用途化妆品备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C90CB91DBF6D21049C33C8608FB86C96', '4F5FA80458271B22CA5D70F07987AE86', '我要维权服务', '41000002SGG82771001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C928624810468551D800AEEC9C975AAC', '59E0A344DD50097F7CDAAE26143741A4', '单位内部设立印刷厂登记', 'YZ0000000XK55738001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C92B4DEB7ADD9EA99605B540D7E4FED4', '76A27C3FAB51112E394E34FB5104976F', '快递企业企业名称变更', '717817421XK55671002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C942871CE3A665B49814B0A6ADE02620', '135C2343A2E0A6DBDD6A4D728D3ECE8D', '文化志愿者备案-测试01', '41000002SGG12503003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C94EC28C29CD316809C1817176082E98', '3A18EA991703203F0C584759863C4A79', '设立典当行分支机构审批（审核）', 'MB1530417XK55958004', '设立典当行分支机构审批（审核）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C94F6EECCF8EEF7BFDC3B0123ACFA1A1', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动变更（地址-自有场地）', 'MB1848628XK00096020', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C962CD249393669F1E4192563588825D', '4DEFB731C346EEB13D65F1CD5005426B', '林草种子（林木良种籽粒、穗条等繁殖材料，主要草种杂交种子及其亲本种子、常规原种种子）生产经营许可证核发', '005184558XK97966002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C96510498434CB2C932B793A6FAD0D3C', '3280902C6B9E22CC9401057B65E494C6', '河南省康养旅游示范基地评选结果查询', '41000002SGG53576005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C9663F00D75688BA0B8AC8A214AF5585', '31B95EE44C011E27EE5B25FE8B8FA135', ' 临时救助业务预约申请', '41000002SGG87288001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C969D00B12BF58A93B93F0ADE8949517', '8F901488EE391116689F7CCBD7231B07', '2020年桑树种植与管理技术要点', '41000002SGG71571003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C96D671FEEE4DEE0F4FBF9E0063F6B24', '76D6F1EBF680F8394259007D9D8DCB47', '变更执业类别（兼职律师变更为专职律师）', '005184291XK11640013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C96E3332D47DBCBA4C7A2FDABFE66BE7', '7342660123B43F44480FA3707071C532', '河南省农业农村厅办事指南信息查询', '41000002SGG61100001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C9C429230691BB5EF46E6DD86E7116F4', '50C668231F231868157EDEBEB16B5F4E', '饲料和饲料添加剂产品自由销售证明', '005184531QR52027001', '饲料和饲料添加剂产品自由销售证明', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C9CA9693B281A3A2A6320151E4C78177', 'E5DC002BC508F4566B1673107D5272E3', '河南省工程造价企业信用等级信息查询', '41000002SGG71118001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C9D6E82CF79E1384CF1ACC46B186303D', 'E27919616185837BE8B39737E4FA5F1B', '施工图审查机构延续', '005184400QR73809005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C9DC1B8068901AD1E94421D9CF94F02E', '31AE25ABF5F312A8CB2954C1EE72B8D2', '养老在职死亡（企业）', '698732712GG07754005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C9DC9A3CC5118DE7C669C67CD712552F', '903A479A85EEBDD20DA1FFEC0DA4E012', '司法考试新闻查询', '41000002SGG04717001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C9E89F21EF085CC30CD0A1E76058C633', 'AA003C2BDDD98931D0643005E1EF36FB', '拍卖企业经营文物拍卖的许可（变更专家）', '005184689XK06113005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C9EBFFAD8B8D11B474474315CC9B0B15', '9090841C1462602FF949AE319680AAA1', '河南省广电局相关其他公开', '41000002SGG45959001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C9ED49A3E9D6158BEEAEC830183BE349', 'B80D5F37B8BA1DB6410B2EB02D40FC2A', '工程勘察企业资质证书法定代表人变更', '005184400XK2009700b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('C9FAD461E4B454C7369AA0692D68E11F', '8B764505C8975E941F064F60E3DF92D7', '社会保障卡办（补）卡进度查询', '41000002SGG94665001', '社会保障卡办（补）卡进度查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CA18C8F991766FAE4B3E58B934E3AB3C', 'ED1AB790D3AC960E00A9D7834C16EC74', '水运工程监理乙级企业资质认定定期检验申请', '005184435XK55677003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CA18D4D249F31A86CA7B28C156C8CA03', '56A2FB47D506EAC1C4A20B6BB0F45684', '空气质量实时（逐小时）发布', '41000002SGG48030001', '空气质量实时（逐小时）发布', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CA29B00B3B86C35DF1295C86E6B3903A', '7D9DB2915663D7D9D5CF3379837F5BAB', ' 道路运输从业人员从业资格证查询', '41000002SGG04085001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CA2A2E67D4A0A8D724DFC2B4280DE413', '6904ADC22C5F55B8D5494DDC9E1F14F3', '因工程建设征收草原的审核', '005184558XK12024005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CA55313CF98327A2EAF16ED45046D165', '2D895427EBB2B6217BF974CEC88B5530', '中外合作职业技能培训机构变更审批', '698732712XK55967002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CA6AF83042ABC9C6E2D9F93DBB1ABF1C', 'D45D29840B86464BD7D03E074117F82A', '气象机构地图查询', '41000002SGG69706001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CA747D67262EB5305F9553B289808244', 'BE80C6B10253ECB2E3CA1150ACE4FD6B', '演出经纪机构从事营业性演出经营活动换证', 'MB1848628XK55977007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CA80C53303F5113793892F606801EA67', 'E18349F40C12B0C035A02A7912E029DB', '个人基本信息查询', '41000002SGG00204001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CA8F5183A60E8D28439FEEC1220F0751', '85EDF718BC6CDF7CBC5DC8736C16A7CA', '河南省图书馆学会会员名单查询', '41000002SGG76036002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CAA0A6BE2FED5703176F14DC3991D632', 'EA0CBDEB4750B848223CA46146E48331', '小功率的无线广播电视发射设备订购证明核发', '005184654XK55687001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CAA4FD8040D302E5009A7CDBF2FCB9B6', 'E245E4E82DF88F0851C6FC41D1531426', '农作物种子生产经营许可证核发(副证变更)', '005184531XK96500006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CABA801C729AC9449E98D89996F732FE', '3B9FE7C3335DF76ABD6EE4D0C28BD411', '医师执业注册（助理升执业）', 'MB1957883XK00278007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CAC393C640C05E7B46DAD5966B49FDA7', '145DDF1D41CD02D35186B75105DDD046', '除电力、通信以外的防雷装置检测单位资质的单位信息变更', '415800448XK16357002', '除电力、通信以外的防雷装置检测单位资质的单位信息变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CACC8531533B47D4352EADFC1D033072', 'BADAF45800533DFB5F3D9696D9AB154B', '外国人对国家重点保护水生野生动物进行科学考察、标本采集、拍摄电影、录像等活动审批', '005184531XK92992001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CADE2AEAB7DA60027D6A8D59C01C93BB', '1404382B99ECCF7AB339C382B14A052A', '互通交通查询', '41000002SGG05562003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CAE30682A9D8518C4475EAE4FA49EF72', 'DA197EFECD40C4584E803BA2B5F5D674', '预约挂号', '41000002SGG05092001', '预约挂号', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CAFFF34411C2F3E9B6F5294A435CDA60', '133BF6BD6D5CEADAAD8363AA53AE0105', '河南省卫生健康委员会政务公开', '41000002SGG84433001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CB1136F70BA6230A93DBE24B3154EB8B', '35D0B3768D99BC525A0C3A16BDAE5CD0', '作品自愿登记', 'YZ0000000QR13013001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CB17385B6B03C95DC3E9386926BBFC05', '6E1C9DE97879DB76230E6297C4DBE0C5', '编研成果查询', '41000002SGG48136001', '编研成果查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CB223A3B7FBB33B97E2FA0F532C62E40', '44F5FEE0C58EB13AE14EF1547182602F', '河南省动物卫生监督案件公开查询', '41000002SGG11246001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CB54377BDDF73A197D31F0D66342B872', '0FB8ADFE12F29B390B9A989C42D6A47B', '省级计划生育手术并发症鉴定', '41000002SGG26683001', '省级计划生育手术并发症鉴定', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CB5D12727AD23B1AB824A11AC2C2B90D', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省民政厅行政许可、行政处罚查询', '41000002SGG37554015', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CB5EBD9996D445A48BFA5936DDCA542C', '2C7E2FB43C4BD12EA403007217A8F8EC', '河南省中小企业公共服务平台劳动关系资讯查询', '41000002SGG55038003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CB73B261EA09C4D5A57D37FB5760E6E7', '725E3D05A9BBCBD411312461B0A70752', '车辆违法查询', '41000002SGG80510001', '车辆违法查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CB8C7F4DD06C4920148D52E199E1DF21', 'A3FF2E23263B29DAB594A0C53D9542BF', '利用高速公路隧道铺设电缆设施许可', '005184435XK5575700a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CBA6798BCCD7AB3F46A65ED2DDB4B26A', '66166E4E889FF9A7822DB5BC40DFA025', '水利工程质量检测单位乙级资质变更（技术负责人）', '005184566XK97268005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CBB2DD4BD1BDABAE35CC2EF1C39DD7D0', '0DCABC6DB8E5557658F705EBE03ACF4A', '河南省运输事业发展中心公告公示查询', '41000002SGG49011007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CBC9FB6B08DB449648FFB8C41F456E75', 'E2B60D3E0D48D34CB3A28FB7F6665687', '河南省中小企业公共服务平台质量管理资讯查询', '41000002SGG07981003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CBCE61A9440C7ADCAAC0B774B2CAE8D1', '249A5BFA7D888F50054FFB1B8D2F4A72', '河南省人事考试成绩查询', '41000002SGG86897001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CBD3303FD9B726F594F1A74547F92E45', '4753DF671DDB8BC3FF0496E43079C6B5', '新冠肺炎求助通道', '41000002SGG90924007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CC18644B48834D47BAFAF819A4118263', 'BE9A6EA7ABFDFF0ABCBBBE9790A2A353', '河南省运输行业从业资格查询', '41000001SGG12367001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CC30370A0EB41809170D2B5376C18F41', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（电线电缆、人民币鉴别仪、广播电视传输设备、预应力混凝土铁路桥简支梁、危险化学品包装物、容器）生产许可证许可范围变更（重要生产工艺和技术、关键生产设备和检验设备变化、生产地址迁移、增加生产场点、新建生产线、增加产品、产品升级等，且涉及产业政策）', 'MB1912677XK55651003', '重要工业产品（电线电缆、人民币鉴别仪、广播电视传输设备、预应力混凝土铁路桥简支梁、危险化学品包装物、容器）生产许可证许可范围变更（重要生产工艺和技术、关键生产设备和检验设备变化、生产地址迁移、增加生产', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CC3B3DDF1AD7D2CF60234A546CC057C7', 'A1B24B0454823D54861A4CDA07A748C5', '河南省残联听力语言残疾服务查询', '41000002SGG68528002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CC5859E8DD12A7B05ED2075DD07ED71C', '563AF1C1A9238505C004CEEEE0F23F12', '食盐定点生产企业延续', 'MB1912677XKEDPF8001', '食盐定点生产企业延续', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CCA36F6E49683D5AA2FF278E2EEC2B83', '3280902C6B9E22CC9401057B65E494C6', '文化视频影像查询', '41000002SGG53576003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CCA52CC9EA9C7F4B7D9C6D42188C8278', '50FCA51A8153ABFDFA66412BB4247033', '全省交易查询', '41000002SGG80546001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CCCABE496266DE30B68727759D87B007', '1BC41360883A9A2206FAE17DDE2BF891', '医疗机构放射性职业病危害建设项目竣工验收', 'MB1957883XK55880001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CCCFFFF67A7C775110EEDB9C0F501327', 'AA003C2BDDD98931D0643005E1EF36FB', '拍卖企业经营文物拍卖的许可（变更住所）', '005184689XK06113006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CCD88FA70C02FB6CFF4BD77C950FDDF6', 'CE20CDBF025AAD97CB822647B236624C', '港澳台律师事务所驻内地或大陆代表机构变更许可（初审）', '005184291XK06608009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CCF23851B5AA521CDCD80D9A2B488F3A', '175C971222104069558624D3087E5397', '广播电视节目制作经营单位设立审批(自有产权场地)', '005184654XK00299002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CCF555D0BA09275F52CD7A6CF8D8D8B4', 'E9AFF4C3F20BC639497327C56223F116', '车辆信息查询', '41000002SGG45927001', '车辆信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD0422CBF0B2FBC306CE1BAA531CE21F', 'E185DDCD0C95A2E61D2F8E2F10061AB8', '条码印刷品质量检验', '41000002SGG69807001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD08E6B36D880FB6015EDE6D7E39DED5', 'E9DD82F9CE79C3412E0B12916E7121FE', '河南省残联教育就业业务查询', '41000002SGG21854002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD1FBA0BBDC0DFDC48ACB07131F0E988', '9674FE495A6BE5CF7F21E67E84908AB6', '执业药师注销注册', 'MB1912669XK00239004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD26FD6D5D73EDF0B6349F5F9C815828', '8A9A82758BF33235BE654E8AA5087891', '台风路径查询', '41000002SGG97829002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD29A3D1B14ACBDE3ECDE656A44CF721', '8F901488EE391116689F7CCBD7231B07', '2020年小麦条锈病、赤霉病防控技术', '41000002SGG71571006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD2BD1B3B8AA780D68AACAE67583B073', '5EFED44FEF3031DC3589380CCBE3E6BE', '外资企业、中外合资经营企业、中外合作经营企业经营中华人民共和国沿海、江河、湖泊及其他通航水域水路运输审批', '005184435XK00309002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD30A5DB1900E0B61F1C0FCB766E56D3', 'CF1CA85A9BA171A4536DB4EA32B4C590', '煤矿特种作业人员操作资格认定', 'MB0P03925XK55659004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD3101531E3BA3AE14E8B95146E1E08E', 'FB2F6C2E9DAF5FAC1877FE3695747089', '经营第一类中的药品类易制毒化学品审批', 'MB1912669XK00234001', '经营第一类中的药品类易制毒化学品审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD36CC043A73A803B01BB58BF7BCD7C5', '6D37E3E860F1154962BB5E179EC16750', '含药饲料加工企业认定', '005184531QR51402001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD38A20E23D0ECEA3A3C5B514192C00D', 'BD2AA32002A93C686ACAC7D0181F9040', '不动产证书查检', '41000002SGG02305001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD41BA6DCDD29FB67FA45E77FF589266', '62EA4F98BAE4E33D2D97DF5F000622F4', '河南省机关事务管理局依申请公开查询', '41000002SGG70760001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD4B9CE7915F74C1411B29FC94FCE728', 'DA56B11730F61B3B2E05255DAAE95280', '省骨干煤炭企业非煤产业转型升级拟补助项目名单', '41000002SGG54849001', '省骨干煤炭企业非煤产业转型升级拟补助项目名单', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD538AE6FC78860FF833952C99B950B5', '6C327CF6C1D5EEAF82EB255BB51DE509', '出口国家重点保护农业野生植物审批（单位）', '005184531XK99520001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD53BB432BBFBF7CFE84050095B31AD5', '6104BFB735AB88FACA5DAC6E575771A9', '汽车整车投资项目备案', '41000002HGG62252001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD5EA4CCAC2D988716362926C27CD207', 'D28CA83CE984D0BA38FE11FCD8D8DF89', '河南省成招计划查询', '41000002SGG63235001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CD7F5BAAEC546E684CC9C4A09B85B2E8', 'A1776CF64C8CDE6E4CB81A5FCC4567CA', '建设工程质量检测机构资质邮编变更', '005184400XK5572500a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CDA0C81CB3B326B3A70645D3296233C9', 'A1776CF64C8CDE6E4CB81A5FCC4567CA', '建设工程质量检测机构资质电话变更', '005184400XK55725008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CDB027D132E23877F58A588D01A3EAA6', 'E54EDCE8E79EF775314B99C842286C85', '人工繁育国家重点保护水生野生动物（白鱀豚等）的初审', '005184531XK37436003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CDB7AA7A2EA1357056475266AA57C8D2', 'D3B381B38C78593FAA565B2BA3A27F61', '河南省益农信息社企业信息展示', '41000002SGG50719004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CDB917F5A540A13ACDD98BF9B64B1F87', 'A3FF2E23263B29DAB594A0C53D9542BF', '利用高速公路涵洞铺设电缆设施许可', '005184435XK55757011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CDB9DD240D1A03C388A5A6AD8BA368BA', 'EE78E0DDC743E7AC4100E84EB753EAE5', '河南省中小企业公共服务平台污染控制资讯查询', '41000002SGG05182001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CDBCFA19A191F4593F35B4DC6748C75F', '30BE103EBEB8E993E5F4C4CAEB4C372D', '外商投资旅行社补证申请', 'MB1848628XK00199002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CDC1D143D62D3283BB48CA3006DEDEEB', 'CBA6A209CB1FAECCAC8325B13C0A3CD2', '业务咨询', '41000002SGG17131001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CDC4AE6FD7F4179C84D60CDF3CEAA160', '56AEABD46393711AF6CCBFF157815539', '河南省工业和信息化厅信息公开指南查询', '41000002SGG51227001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CDF40843CFF6E0A4C0DE3D1861E6BA1D', '603DAB563E5123144C73F0684DA57A8E', '省级财政专项资金目录', '41000002SGG13072001', '省级财政专项资金目录', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CDFAF622CDC445401FA98D5B611421BC', 'F19E661C364139F976652EE978CB0B60', '河南省应急管理厅资料下载', '41000002SGG86523001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CE27701B5E11ABBB6B24F329D8303D65', 'BD2AA32002A93C686ACAC7D0181F9040', '不动产进度查询', '41000002SGG02305003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CE2B9531982637A38E925C303C746825', '04D3CB82804B3D721BC4577AF475E8E5', '农产品质量安全检测机构考核(注销)', '005184531QR0002306e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CE2EF3A57674651BBE10106F9CAB7A28', '7A0B88FEB0CDEE51B53E31F76F39E0BB', '文物复制、拓印资质审批', '005184689XK00385004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CE68B6734B25024A45F9C91B8381E69B', '627F4F2BE2B2A986E890D66A73F39EE7', '全民义务植树宣传及咨询', '41000002SGG68276001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CE89C96E0A2013B3DADF582E63E99088', 'D6453E780F5168C06A82BD05336B2BAB', '特岗教师服务期满证书查询', '41000002SGG13168001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CE9498971C6923538FA97FFCEB6C3C55', '3A52BFE88CFADDF1C640B9FBC9329DF4', '地方期刊出版单位组建期刊集团审核', 'YZ0000000GG33183002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CE9618B14A131DF08C4FED7245C0ABE6', '3AAF21856C478E41EAED1E6D30068443', '食用菌菌种生产经营许可证核发（原种）', '005184531XK67400002', '食用菌菌种生产经营许可证核发（原种）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CEA925EE6A1733B689FE8E347BFD9992', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '派驻分所律师(外省总所派驻河南分所律师)', '005184291XK5580901e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CEBEF3B804BBBE4AABC612FF9DE2DDDA', 'FFFCC0E7816F2A953CE0BA833B99F8CF', '河南省地理信息公共服务平台服务资源', '41000002SGG13896002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CECA6A346132B07D01EA4D4555AC8456', 'DFB86E8A59ABE91B58630324BA39C9D4', '河道管理范围内有关活动许可（钻探、开采地下资源许可）', '005184566QT00239007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CEEF845A89E3711BDCC5DACA8AF51CFD', 'D49C411DD9718C900A47C62DE1C239B8', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出场所经营单位从事演出场所经营活动审批（自有场地）', 'MB1848628XK55963006', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出场所经营单位从事演出场所经营活动审批（自有场地）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF09B1067A66928440D7208E6D1CE1A0', '8887779BECF53D89C75231AC1DA11C92', '水利建设工程文明工地创建办法查询', '41000002SGG31567001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF1059C510DF55428E3111E7F358CC36', 'D86A1F3246F843EDE7A51CCEAC37618C', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动变更（经营范围）', 'MB1848628XK55961012', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动变更（经营范围）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF4A72839B7D5D71795911E2F74AAD09', '2E6784788D59549E3E810E44F689C3E9', '医疗机构执业登记（中医、中西医结合医院）（变更诊疗科目）', 'MB1957883XK55857010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF54FB2CF52C5C8959F747C9B1E21BC6', '7DB93EE18F55F27ADDD41C3D9AEC96EC', '河南省交通基本建设质量检测站检测管理查询', '41000002SGG52940004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF62FC0B4844C5FA3E4C4ABD24C2523D', '002447207831DA53248577890AA4AD3B', '文物保护工程施工资质二级审批', '005184689XK94270010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF64A89FE54CE0CD4F56169649AFC437', '8D70BAA00223955E4A996550799B5765', '河南省图书馆文化信息查询', '41000002SGG33977005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF6FB70EA507F94B1DC8B1057BE39E55', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动变更（游戏游艺设备）', 'MB1848628XK00096005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF73E4E1B1CC78273D00FE5A8BD73D99', 'C98086A2A0E54E9CBA07941662D931D7', '烈士党史及相关主题展览', '41000002SGG02635001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF75CE3B8311D91D4D55285E3E360E51', 'ED1AB790D3AC960E00A9D7834C16EC74', '水运机电工程专项监理企业资质认定首次申请', '005184435XK55677002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF77F7CB2E46FBB3AAA8AE339C436D89', '2E6784788D59549E3E810E44F689C3E9', '医疗机构变更床位', 'MB1957883XK5585700a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF8E3694B0B5B81ECCA69E48FFF68CB8', '70B5971FB01A675155697DABB5FE13BA', '初级中学学校名单', '41000002SGG87268001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CF9C41170127A6BBDE128BF227E3EA0C', '2643E2EAB10A7488AE03D967DA775272', '出口食品生产企业国外卫生注册推荐', '41000002SGG84021001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CFA2E34C74428514E003D43E1839CADD', '21C0831D337B178996B102BF362D2F50', '城镇企业职工基本养老保险转出到机关事业单位基本养老保险', '698732712GG59360003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CFB442E4952DE54311E4C45FCDBF6545', 'F7965C1643D65294C60EA47F1F71F5C3', '地质灾害治理工程施工单位乙级资质认定（变更）', '005184224XK2899000b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CFBF28DB14218B7C9C0BA5F868C99204', '468FFE9ACC585DEDE2F61C65C9B7F8B1', '统一社会信用代码公示', '41000002SGG93884001', '统一社会信用代码公示', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CFBF5194F32493C4BA532D9AF32D0FD7', '0525B7844BAD6F75E2BAA51F90AAA933', '农产品配额申请材料转报国家', '41000002SGG90288001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CFC43955C7FD4E59EB81DE63E64BE1CE', '85EDF718BC6CDF7CBC5DC8736C16A7CA', '河南省图书馆学会会刊查询', '41000002SGG76036006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CFCBF3809FC7A87CDFD9659C7EDF72B0', '9ABF155614C15628D73303D1AB191BE1', '出具公路工程参建单位工作综合评价等级证书', '005184435QR13099001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CFE8804496452627EC46E93A456AE1FE', '375DB634E25898EC29D4004087F29A2E', '煤矿安全评价机构资质认可首次申请', 'MB0P03925XK42032001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('CFF1832106B2C765A349E1AD315C3AB5', '192EF4376A53C94D3520091EB219C70C', '河南省民政厅特困供养信息查询', '41000002SGG86949002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D009C6DD34F96C8989DE7C52241C706A', '4264015DB8392CF9DF22D9EC77FC3ED7', '河南省产权交易成交公告查询', '41000002SGG70025001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D015FAE243802A197C08DE7475473AD7', 'FCC7491963420D5E9A017267EE96A996', '就业技能培训相关人员报名相关政策查询', '41000002SGG44247001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D01E2932830D47D9A67326FF39DE3ABC', '4C89A468ADB43DB226D2102E5D18D8CE', '在草原上修建畜牧业生产服务的工程设施审批', '005184558XK84021004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D0389A222451107CFC3A86EBB521CECF', '6460D06FA8702F8874FF8C545E860B8F', '水运工程建设项目施工图设计审批', '005184435XK55731004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D0629E8FD390176C9B794430360F4404', '175C971222104069558624D3087E5397', '广播电视节目制作经营单位设立审批（延续）', '005184654XK00299008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D07AF774C1543523E8965E50040F3051', 'B80D5F37B8BA1DB6410B2EB02D40FC2A', '工程勘察企业资质证书增补', '005184400XK20097007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D07AFCF5A812A76955B0921B166F579A', 'E0B563387D49C44A4CAB633E9A290042', '增值电信业务经营许可证业务覆盖范围变更', '726991560XK07809006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D08E91531E69D5953AAE8F0466873E54', 'ABBF37BC8D77A8B3EB30BFF1AC3CF3B5', '技工院校毕业证书查询', '41000002SGG22991001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D0B0F6FB977E858196CAE712010EA551', 'ACE3DEB6F873805D4081C52A336E2A53', '药品批发经营注销证书信息查询', '41000002SGG27249008', '药品批发经营注销证书信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D0BBCF640A4F4A6AD05679D45C739F22', '56AEABD46393711AF6CCBFF157815539', '河南省工业和信息化厅政府公开双公示查询', '41000002SGG51227009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D0D8ABB177A47D4FBDABC56D29EC7028', 'E68B9447123C223372C3123EB45CD2C9', '国家重点监控企业污染源监督性监测信息', '41000002SGG11784001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D0DCDBDBE119ED944DE6C8E6E31426C7', 'E1A2544EDEB4B8716DAFFB71E573B264', '孕产妇保健（产后访视）', '41000002SGG22402001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D0E8DA2C65C9D674D9E9D118433C9A1C', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）注册地址变更', 'MB1912669XK55721003', '药品经营许可证（批发、零售连锁企业总部）注册地址变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D0EA32BB6F6265EF1856C9FCEBDF73BB', '33F7EA1C38D04665050817920B72BB16', '民国档案查询', '41000002SGG77257001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D0F1C33A112F5B91EB3ACF7611C8B009', '30F588A229E990A6231262AD457B2775', '农产品成本收益数据查询', '41000002SGG61432001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D0FAA6586D5EE8188C5F46189D851CAD', '00A3A498AF128075BE4FE07DAF67B123', '食品添加剂生产许可变更许可事项（含工艺设备布局和工艺流程、生产设备设施、食品类别和生产场所）', 'MB1912669XK55817003', '食品添加剂生产许可变更许可事项（含工艺设备布局和工艺流程、生产设备设施、食品类别和生产场所）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D0FBE2FA1C98D6B413DED2DB446F19D3', '6A01AFEA5F6B241BD81E032E3807682D', '食品委托检验', '41000002SGG81900001', '食品委托检验', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D11345A2D771697F6F1155F6AB64E62A', 'B80D5F37B8BA1DB6410B2EB02D40FC2A', '工程勘察企业资质证书技术负责人变更', '005184400XK2009700a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D11371EB78AB5DCAFC3983A5DDE54A95', '96B2A33FB2032976AD12BD80BF5CB2A6', '文物保护工程施工资质年检', '005184689JF00013001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D119D4C565DC0BB6D2A579932A225CA5', '0120F4CCF117172F79150AF56D141213', '12312商务举报投诉电话服务', '41000002SGG22055003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D125859598E47F9F046CADD8E26A3253', '472BD22570EE1C860F9A249E259A0561', '接收卫星传送的境外电视节目审批（延续）', '005184654XK55662006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D127AE0523DECE4A36A1D5C8FB6319D3', 'DFAE48CAC05BEE2AF514BA45272477A8', '机关养老待遇发放信息查询', '41000002SGG75960008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D13B4D0C08E23266802BE6462D915CF2', 'A13C436C0A33997FADECA977A0EA7BFF', '引进用于信息网络传播的境外电影、电视剧审批（初审）', '005184654XK08978001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D13BDB69F3E1849739B268F1FA9520B8', '2BB8CA95AC32515D4D624284D6A06260', '河南省教育厅关于公布河南省农村学校应用性教育科研课题2019年结项评审结果的通知', '41000002SGG74476001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D174863146E45E68FC8235E784B062A7', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证补证', 'MB1912669XK55720005', '药品生产许可证补证', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D17CBA22EF280BA04C88B01D8FE59FFD', '375DB634E25898EC29D4004087F29A2E', '煤矿安全评价机构变更专职技术负责人', 'MB0P03925XK42032006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D1D672069833CB6DA21642C6B9B41E09', 'FCF006C3BC3DB09EEA0686BBAD035DF6', '河南省建设煤矿生产能力情况汇总表', '41000002SGG27028001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D1DD08D63965E1133343EA41B6890B3B', 'D9B692005893432F1E059660A8919285', '商品条码业务办理', '41000002SGG88185001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D1E06682B576BDEF634D4CF8BA71D9AB', '56AEABD46393711AF6CCBFF157815539', '河南省工业和信息化厅政府采购资讯查询', '41000002SGG51227006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D1E64573AAAAC5EBB1CFD219F411C92F', '1BB5DFAAB8B01AF6BA2ECEAB57EBFEC1', '出版物批发单位变更经营地址审批（工作场所为租赁性质）', 'YZ0000000XK55752006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D1EA3A5EA156CCE0ED44EB83F8600CBF', '210868935EBAD2BCD4821021BEBF9656', '检验检测机构资质认定标准（方法）变更', 'MB1912677XK55650005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D1F06557CF656DDA81FCF98ACBCF1E83', '4CF21DDDCA57FF0992C1BDD5B6D87B1C', '地质资料查询服务', '41000002SGG51135001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D219F4F6E6CC7F0BE52956ED2E81BF15', '87C4208BED1361682065E50EF91BFBA4', '医疗机构制剂注册审批', 'MB1912669XK55835001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D23334A7B21B36F5AC392051091525F4', '1DCADF8941110631E23704ADD32560E6', '音像、电子出版物复制单位变更法定代表人（负责人）审批', 'YZ0000000XK55723003', '音像、电子出版物复制单位变更法定代表人（负责人）审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D23EBB51789E67CAAF0ACD25D3A9BA0F', 'CF72ED6FDEBEC03D1715C6E1E4ABE961', '从国外引进农业种子、苗木检疫审（核）批', '005184531XK99357001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D242BE7FBD8DDA0BF9DF4EE1B42BD334', '00A3A498AF128075BE4FE07DAF67B123', '药品生产质量管理规范认证证书补办', 'MB1912669XK55669002', '药品生产质量管理规范认证证书补办', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D276BC7E42E7390C1726FFAB507CDE35', '0F6F02DBFE8FABA982F118DDFF66025A', '区域性批发企业需就近向其他省、自治区、直辖市行政区域内的取得麻醉药品和第一类精神药品使用资格的医疗机构销售麻醉药品和第一类精神药品的审批', 'MB1912669XK55775001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D2C320B46B79987B8627F51971AD153A', 'DFFFF113EA827AA981D01815E3B4898B', '船员适任职务证书核发进度查询', '41000002SGG12875002', '船员适任职务证书核发进度查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D2CBDC76881E48D522948ECFF9EDB418', '7342660123B43F44480FA3707071C532', '河南省农业农村厅规范性文件查询', '41000002SGG61100007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D2CD4E5AC338F89D68E3A6D1773E23A2', 'E3B95F4BAC523EAD48FFA645D4B012FB', '煤矿安全生产检测检验资质认可变更实验室条件', 'MB0P03925XK50108001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D31E5F3416F6E602C11BEEA14AA38C0E', '6BD654211E42460AAF688E90F9E9AF1E', '社会团体成立登记', '005184312XK95995001', '社会团体成立登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D32231D21C57BCA6D651BE7D5019F24A', 'B87CA072459921939B82FF7ECDF833BE', '食盐定点批发企业（法定代表人、注册地址、企业名称）变更', 'MB1912677XKS7985002', '食盐定点批发企业（法定代表人、注册地址、企业名称）变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D335C94A4CBDDDF8401C462208743A78', '7E2F45D9697999CC147882C7B05E48EA', '电子出版物复制单位接收委托复制境外电子出版物许可', 'YZ0000000XK55741004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D33DE1161F8821DEDB20F8AFFBA85E61', 'D144342C3D8C081BD61845E0F9F84D56', '重点对外合作项目名录编制', '41000002SGG81494001', '重点对外合作项目名录编制', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D3403252D5FE5088612E05A690AFB209', '9D9DAA743A0BCBA7ADA27FE5E3A7B8AD', '地质灾害危险性评估单位乙级资质认定（新设）', '051842241XK10466003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D3413A3ABF6729259913FFD868BE12E8', '9710A4A8B3A108819E1B1EF21BA71F48', '测绘成果目录汇交', '005184224GG05242001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D35BE14EAFF9B55BDBC767A595989D64', 'F01E7C86E22E4332B8E330EE480A4679', '发票查询接口', '41000002SGG08512001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D36EC9189473C431FF66CF7E0772F696', '0B6378CF858DEC0C87C5879498BCD42E', '安全生产许可证首次申请核发', '005184400XK55625007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D386DFAED836F27484498A5F87BC3CA9', '050FAC3E15A70337CB3F6D6F682708B5', '省重大项目名单查询', '41000002SGG91073001', '省重大项目名单查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D38D676926441A47500066583BB7AF2F', 'CAFFF3522503E54A49B1B0D359973B27', '资产评估机构注销备案', '005184603XK55636005', '资产评估机构注销备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D3942F2534AEA751EFCB7EBC385834C8', 'C06C81E53712D151B9400E7B6DA14BB2', '携带不属于国家所有但应当保密的档案及其复制件出境的审批', '725831987XK3634800a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D3AE10743972C945B8F0F61824519801', '1EFFF9875D74B9524A813BC8178AC7F3', '补办申请涉外调查机构资格认定', '005184355XK00076004', '补办申请涉外调查机构资格认定', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D3B17F21CCF29CB4F31354282FADC8CA', '97F33ECB9678A0580191D152C7AE919A', '军队转业干部参保登记（机关事业单位养老保险）', '698732712QZ00659010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D3B732B26A216165B4C7B4E8E37C15BD', '14D7066FEB4C2FACEB1124F8164325D1', '房地产估价师考试成绩查询', '41000002SGG99881001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D3BD35E34DEFFAABFBF10FE7E7A8C5E6', '9B116595640BD930397CD4082BC761DC', '2019年中国消防救援学院招生体检结果查询', '41000002SGG67584001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D3C37D95D9D0D241B9314F6B1EB348C1', 'EC798CA89B635606DDBDD19D49230FB0', '65岁以上老年人健康管理', '41000002SGG53220001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D3DF11AB4DA7B0A5FE48B70A73BC79D5', 'A3289A2D7CFF23A1CBFA88ADA939FC95', '法律职业资格申请材料核查', '005184291QT00265002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D3E64E490159F8D19134592710FA26E9', '4753DF671DDB8BC3FF0496E43079C6B5', '新冠肺炎小区速查', '41000002SGG90924008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D4067F8DC9064C93FF199D488573D780', 'FE3E194376806EAA75867FC750E311E4', '甲级测绘资质延续申请', '005184224QT0804A005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D422590EE151E2569145B6DD4F0BEAAB', '1F3E8080C43AE39DB9B9307DF4CC159C', '特种设备生产单位许可（制造单位延期）', 'MB1912677XK0008000d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D427B5F321FA9A6AD24520EE5517ABFB', 'F039F2F83D5A3CC4606776B58A5D18A2', '河南省中小企业公共服务平台科技政策查询', '41000002SGG03029001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D43493E01E3082F2D8A5B99BBA145F82', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省民政厅业务动态查询', '41000002SGG37554025', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D43B02965124BD85026F0E9D28A94EC4', 'F6EB794CB0188ECAE0A924A32CE6BF77', '省测绘局信息公开查询', '41000002SGG96920001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D450C50F67A62340F484F93969610761', 'DAB301DDC04E0D58DCD9AA1BC0217944', '省级建设项目环境影响评价文件审批（非辐射类且编制报告表的项目）首次申请', '005185040XK4997100a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D45B58918A55A7570CB35F69B04BA50F', '3865CF4C89A1DD2420720829F3FD7820', '兽药生产许可证核发（设立）', '005184531XK84280002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D46CF5176F9FB17CDD51CFE22BC20D8B', 'DBC3D7CFE77564A60F87FBFC49798AF1', '河南省事管局局发文件查询', '41000002SGG74847012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D4784306F5AAC825CF4B1C5A986B57ED', 'ED9FF784349EB919351487871453E9A5', '注册测绘师资格的注册审查', '005184224XK42481001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D4810C206A1F729AC76BA1AC25D12AB6', '95E7411F1532E2BC162DE26925CB7739', '就业见习补贴标准查询', '41000002SGG91934001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D4AC200646D15413510250FFAE0FE193', '145DDF1D41CD02D35186B75105DDD046', '除电力、通信以外的防雷装置检测单位资质的新办', '415800448XK16357009', '除电力、通信以外的防雷装置检测单位资质的新办', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D4B8759F3E094E655F790D434E0028DD', '70937A897860B2C413EAE1F9E4DB2A5B', '利用省级文物保护单位举办大型活动审批', '005184689XK99377002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D4D6117844DD85D215F84A32E0C9ACF0', '27DEF9D1E09B677EE899D5BB11DD0E37', '建筑施工特种作业人员资格证书变更', '005184400XK55735003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D4E98CF364B67CA5F1D46C3C76F50D37', '64233151D76C0AD7ABA6213E0E8A82E9', '基金会住所变更登记（凭产权证办理）', '005184312XK00206001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D507DAAC794C4860D43DDAECACA7FB73', 'BFC581EC29CC8E59FF515C2AD4BBFA0C', '车辆报废查询', '41000002SGG17849001', '车辆报废查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D50A93460DB94765FAAC4481532C287E', '145DDF1D41CD02D35186B75105DDD046', '除电力、通信以外的防雷装置检测单位资质的升级', '415800448XK1635700c', '除电力、通信以外的防雷装置检测单位资质的升级', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D51CE44CE46A2CEC7D44A6AE37FAD445', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证变更药检室负责人', 'MB1912669XK5564600c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D525E51F73E6C33E429730148DD39F1C', '6C327CF6C1D5EEAF82EB255BB51DE509', '出口国家重点保护农业野生植物审批（个人）', '005184531XK99520003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D5427BA5CFC52988A7B8BC58703D9DCF', '6BBB1BD1C92F629F0DBE71C491977A87', '对自考合格课程跨省转移的确认', '005184662QR12317001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D554736CB94EB5548315B44A76807289', '002447207831DA53248577890AA4AD3B', '文物保护工程监理资质乙级证书注销', '005184689XK94270008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D56D8117AF9459DBD17239F9E8CDA657', 'AD554DCF464EEF008C5E88537138F247', '型式评价', '41000002SGG12941001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D589527863763CB288749918138F63D6', 'B02885956D7745A00490E1545380E5BB', '国际收支统计', '41000002SGG39927002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D5A5DC068E685B642CC233063CC4A955', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '派驻分所律师(河南总所派驻省内分所律师)', '005184291XK5580901c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D5B9F79303754614293837B558896B36', '86EEB466DB6EE2FABB79410A2317257E', '河南省中小企业公共服务平台企业创新资讯查询', '41000002SGG99265002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D6395B926891FA138344184538447510', 'AF0D12D6067E8462BE020B582FA1E6AD', '外国律师事务所驻华代表机构设立许可（初审）', '005184291XK55892004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D650FE6D8BC51F2DB8F6927D79BB06A9', 'A20529FFD23128206D701DD677FA1A56', '社会扶贫', '41000002SGG29452001', '社会扶贫', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D6649F8CF07577C116F04E57ACBD5778', '1B74EA67CCE9281340DE72E177DCFE84', '船舶建造检验', '005184435XK55995003', '船舶建造检验', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D66926ABAA6729070BD2EBD000DAE65D', '6314EECE04F8720232C4F2701079EE58', ' 转报国家农林水利项目', '41000002SGG89599001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D6933D76D74EAE5EF01FA09B8CF2B727', 'EF64454331616DE66C881EFF59E4A4B4', '初级中学学校名单', '41000002SGG66353001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D697E1F1E2230C9E6C44CEE7C25DDC47', '59D420823ED4B9F8AB0BD6E696599067', '农产品质量安全宣传', '41000002SGG69785001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D6BA1EBCC1884B7C22FF0D03BC700435', '08114689FD9AA683EEA127384E61EE37', '森林河南电视专题查询服务', '41000002SGG12926001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D6BC89CFE84F10C8B15E6D3210E3B46E', '3280902C6B9E22CC9401057B65E494C6', '河南省文化旅游大会查询', '41000002SGG53576004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D6C377EF9986E04CB83AAE7DD8B13041', '70DBCD4DF50EADCAE19FA1D65A1505C4', '高考成绩查询', '41000002SGG82568001', '高考成绩查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D6C855307195489653A5316D0A033984', 'E11A52F69537C09572BA8646CC59D9CF', '省人防办脱贫攻坚查询', '41000002SGG11684002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D6DA9E6FDA4CFB182BD37024A70E110B', '2F959943F4572D8A8952EC05EB541091', '国家基本水文测站设立审批', '005184566XK55873002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D6DE2CDE77B62852720658904C5EE06B', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（水泥、建筑用钢筋）生产许可证发证（涉及产业政策）', 'MB1912677XK55651025', '重要工业产品（水泥、建筑用钢筋）生产许可证发证（涉及产业政策）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D7107292795FB6CC8CE5476BF658E3B4', '754004025F17D26D8ABDBAE237CAA73D', '河南省PPP重点推进项目信息查询', '41000002SGG23547008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D713F058C29FBCF1AC65E926A4D2A70F', '7C70089AE675347745C60F8CF8FA402F', '河南省交通运输信用承诺书查询', '41000002SGG79266002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D78D3238295585E64356B58ABAE88D2F', 'C1203F9713C93EDCB263C3D9F53513EC', '伊斯兰教信息查询', '41000002SGG32160003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D78F798E8EF71A682A2E2973677B80C0', '45178A0278017908E60243E32117470A', '省体育局新媒体', '41000002SGG92882012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D79A1B750F34375A222E63E31F3A3999', '7C70089AE675347745C60F8CF8FA402F', '河南省交通运输评价公示', '41000002SGG79266009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D7A3EEE70D1ED04A4C26731E3A30CB66', '2D1055872C9544CF086F84D38DEBB357', '二类超限运输车辆在省范围内跨设区的市行驶高速公路许可', '005184435XK00322009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D7AB2D6C46D51CB0EEDACB868655401A', 'D9F89BDDD8C5046DFC8BCA91E397F580', '地质灾害治理工程监理单位乙级资质认定（补证）', '005184224XK5392400a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D7ADFE6E369E4AA0CC058A97694F7F07', '58B8696831A7C6EC5E41430A925CC2F8', '从事经营性互联网文化活动注销', 'MB1848628XK02935008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D7E8BACCD74F1071FFE4DFB91C0CB36A', '6A2DC40C7A5E221A238484F11EC4C4A8', '河南省图书馆历年赠书统计查询', '41000002SGG12905001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D7E9ECF6F210148CE56068822E8A717F', '6AF4666BB6DBA9748BA281B6F176633D', '生产建设项目水土保持承诺制审批', '005184566XK00251003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D7FD05118B1902FFA11D236C912C4216', '975A2457A12B87E07BC2624E58E8309F', '河南省交通运输厅专题活动查询', '41000002SGG00577001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D817E7DEFD45C57CE9FB1A8EF436B556', '3B9FE7C3335DF76ABD6EE4D0C28BD411', '医师执业注册（超期注册）', 'MB1957883XK00278008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D8311E155B79409BDD61BBD7F0D41357', '5C9C99BF8FBB6290DD02C7B6AB7F7569', '第二、三类医疗器械生产许可证增加生产范围（含增加生产产品且涉及生产条件变化，可能影响产品安全、有效的）', 'MB1912669XK00235003', '第二、三类医疗器械生产许可证增加生产范围（含增加生产产品且涉及生产条件变化，可能影响产品安全、有效的）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D83442057F747F2A679CDDC406BB4327', 'AFDA551D80B6003E06CA8CEA549B02C1', '农药登记证持有人变更初审', '005184531XK91727001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D835C11A10075E8722099D419770316B', '5DB18E8D8C049A94B7E7B7145CA10B39', '河南省产权交易产权融资服务', '41000002SGG21755003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D8406DAA916528935E3CBAEFE7C9A2E3', '271982065AD0DCC2F22A1BE65BC3DF19', '跨省经营广播电视节目传送（无线）业务审批（初审）', '005184654XK45662001', '跨省经营广播电视节目传送（无线）业务审批（初审）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D8439B65444DB9840504EB26412202B0', '1404382B99ECCF7AB339C382B14A052A', '河南省交通运输厅知识库查询', '41000002SGG05562001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D8674C92B21155BA02C78286F1773AD3', '788711B922B638559783205F0016331A', '在自贸试验区内的中外合资、中外合作经营、外资经营的演出经纪机构申请从事营业性演出经营活动注销', 'MB1848628XK55922004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D87AE56BE6170A236C5CC92CD48BD6F3', '7812B83B995C665F4B6956AEDBA9EB23', '知识产权课程与证书查询', '41000002SGG24540001', '知识产权课程与证书查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D8A973B8B9F5CB9F206A7A343ED985A8', '2E6784788D59549E3E810E44F689C3E9', '医疗机构执业登记（中医、中西医结合医院）（变更床位）', 'MB1957883XK5585700e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D8D35B6C62FF4FC098284691837E54EF', '4681FFC15B5D558924712EEFFDA9C6B1', '河南省卫生健康委媒体关注信息查询', '41000002SGG22557002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D8E420E1BF98D6D228251F13D6374923', '3B9FE7C3335DF76ABD6EE4D0C28BD411', '医师执业注册（变更执业范围）', 'MB1957883XK0027800a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D90FF2958C381373AA74A23C1B7E3BCD', '6845589B1254BBA0372106A9E14E1BBD', '河南省交通运输厅造价信息查询', '41000002SGG21171001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D913AFCE66EFDEC55D6CAE6088A72D1E', '3444C568FAAF3EFE17D0B29447415D3D', '普通高等院校名单', '41000002SGG28488001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D9212A571281F1B59E018DD8AFE44EB7', '2ECACA9F8DA81D8ABF5522211A31FF6F', '全国地理信息资源成果目录查询', '41000002SGG59048001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D9221A824429F93E74239FE163C7DBDC', '4F70A8321E1F44ED99983ADA119D59EA', '河南省航务局机构设置查询', '41000002SGG35866002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D927F1EA80CCCB58562428881BF82FA9', '7B55C303A5D11373E25A6E58D1280536', '已公开现行文件查询', '41000002SGG01916001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D92C0B3E72EC362D0A301A2B13DACD63', '37971555A5D52B0BEA5FE0312329F8E1', '河南省信息通信业经济运行情况查询', '41000002SGG95796005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D9385830E5C7DF799EFDC430221A185E', '1F3E8080C43AE39DB9B9307DF4CC159C', '特种设备生产单位许可（补发）', 'MB1912677XK0008000f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D93E4A04BE434CA642BCB033850A16C7', 'C633B4BC93B1436CB8C6E573654298BD', '基金会慈善组织认定', '005184312QR12987001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D93F6C54D8B22ACA0D2018F63ABD4D28', 'ECCB3379D27E5CFA18F8B16BF359D1E2', '文物商店销售文物备案', '005184689QT20271001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D94637CB237291C33147D0172E32A1A8', '016D6FEF2A88EA957174ED0128EED5F3', '报废机动车回收拆解企业资质的注销', '005184590QT00209006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D95A9C3785CE6A1965F516C4496F61C2', '6D1A849B77120866144F4F89D78F599F', '司法鉴定机构设立登记审批', '005184291XK55905004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D963A6F58F4A53EF5C6758279827AB91', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省民政厅政策法规查询', '41000002SGG37554019', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D969D069FCBC4E2218FD84935E4DA6F5', 'BFB89262A855E2305396FE7187950AAD', '车用气瓶充装单位许可（补发）', 'MB1912677XK5565300e', '车用气瓶充装单位许可（补发）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D9787E7AA0D8B6FC49A9EF1B18F3D41D', '09521EC965AC45C298B704C2BB15F424', '付费频道开办审批（初审）', '005184654XK42638001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D98AD098EB0F98756168ECB1B4DFCBB2', '375DB634E25898EC29D4004087F29A2E', '煤矿安全评价机构资质认可延续申请', 'MB0P03925XK42032002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D9926384C2419A41D9173E5D5612971A', '5C3E645A610D16DE563E3B9C8CD77BB6', '二级注册结构工程师变更(预注册转正常)', '005184400XK09297009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D99B2D23EB9317FD27ED3803CE9C619B', 'DC4F08623B0C88C554283E65A9F58F3E', '放射性药品经营企业许可证核发', 'MB1912669XK00223004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D99E1AE0CCE250EF54781396D5D82B9D', '48CE142F00FBA0E19590703B4CF1252C', '兽药经营许可证核发（注销）', '005184531XK29094005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D9BCBA547290A6F83F72341FFFFDFF2F', '30BE103EBEB8E993E5F4C4CAEB4C372D', '外商投资旅行社业务许可（租赁场地）', 'MB1848628XK00199010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D9E9C8147019E2BCDAF5A14A00AFDEF9', '86EEB466DB6EE2FABB79410A2317257E', '河南省中小企业公共服务平台WTO与企业资讯查询', '41000002SGG99265004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('D9FADA3FC98F5959F11C7F370E9D123A', '1E0B317EB023886A0766B7A7E8B392D9', '河南省企业事业单位环境信息查询', '41000002SGG92834001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DA13EA1F3470463534C8BF270BFDA674', 'A5014FCF9E24C8DF5B381CB63B720860', '固定资产投资法律法规政策宣传、咨询服务', '41000002SGG88984001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DA264AAD387E5DD9EC6145373C9AF1B1', '754004025F17D26D8ABDBAE237CAA73D', '河南省发展改革委员会建议提案查询', '41000002SGG23547018', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DA5E520FBFEA195925694FE3DB487289', '29CFE7DB3D26F4578C912DD0D4B9B408', '境外会计师事务所来内地临时办理审计业务审批（境外）', '005184603XK55626001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DA7214EDE4FE44A9C141B03974EECCD5', '18625A57EBAEB3522DE09450A72E6925', '河南省省直高层次人才保健卡办理', 'MB1957883GG9T35O001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DA8E410301FBA478A41793BAB00266F7', '860AD9BD6D2E884AD9961726AC04D383', '二类医疗器械经营备案凭证信息查询', '41000002SGG76137002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DAD5785443A94451D9EE2A201EB783BA', 'C1D82BB486BF887D5C3B1C65ED902420', '河南省林业局部门采购查询', '41000002SGG98738005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DAD6D9E237B9E16AB7DEF747CA547216', '628D42B0CD977081D8C468E7623C28F7', '灵活就业社保补贴申请政策查询', '41000002SGG12262010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DAEE667709C9A54007A1974E4F809E56', 'C06C81E53712D151B9400E7B6DA14BB2', '携带三级档案及其复制件出境的审批', '725831987XK36348001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DAFA4DACB58D2E21CE18D3B7893663FC', '2D895427EBB2B6217BF974CEC88B5530', '中外合作职业技能培训机构终止审批', '698732712XK55967003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DAFA90FB8710A81A59BF7E77C73B492E', '0DCABC6DB8E5557658F705EBE03ACF4A', '河南省运输事业发展中心图片信息查询', '41000002SGG49011010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DAFB966D494EC425F90D6E31709CC7E6', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省彩票公益金信息公开', '41000002SGG37554020', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DB0244939C4380309901B2C11E36F23A', 'CAFFF3522503E54A49B1B0D359973B27', '资产评估机构的备案', '005184603XK55636002', '资产评估机构的备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DB07B6951605C42CD8F618CA533E78E8', 'B6D9035650D1A59BC6E31C4F6D2253B4', '从事出版物印刷经营活动企业终止印刷经营活动审批', 'YZ0000000XK55728008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DB152E4DBF3C94C60E9CB9405D875734', '6AA38DD7C0F446E03C764DA36EB63CA9', '河南省林木种苗供应信息查询', '41000002SGG72751002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DB3A878E771C46CB34DAF409B75D75A0', 'F0CB7655FBE18CBD01D3308EA11E8AA0', '食品添加剂生产许可注销（省级）', 'MB1912677XK27763004', '食品添加剂生产许可注销（省级）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DB41A851857741FA49439B3667D61AFC', '9A0B72B675692F9ADC75878D879CABEB', '医疗器械网络交易服务第三方平台备案变更', 'MB1912669QT28532002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DB47D19ACDAE311B3DDB0F72324DE31B', 'B9AB43F0556A700B5CA24A12E4CCDCA2', '安全生产考核合格证书变更（人员信息变更）', '005184400QR0235500d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DB5EBA9CF487A0D71BE5A3D087BD61B6', '146DE1561018FBE802CBE7681D4B4DFE', '文物文创查询', '41000002SGG06421009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DB7762D990E1F6589B332A41ABAD1F8A', 'BE0CBA2DBFF16E354597F30A554B1DB2', '运行管理基本知识查询', '41000002SGG51937001', '运行管理基本知识查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DB7E9BA618E9B091E1ACA39E3CA353EE', '4C2CA143BDBFA85210AF37E965D52C26', '合伙企业变更（备案）登记', 'MB1912677XK00139014', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DB822736E644F102F516B7587C932E67', '08A2C028D816BF15DB8E54B7F79C9E8D', '司法费用计算', '41000002SGG41089001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DBAE8995B513B55362997C750BFF3DC5', 'CBE23B083C4040E6C6000602F6D679C9', '社会公众科技报告查询', '41000002SGG76766001', '社会公众科技报告查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DBDD1A01E622347313309175A9D30C82', '9F3EE808C088287B8BA3203101CCD79E', '使用麻醉药品和精神药品标准品、对照品批准', 'MB1912669XK55787001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DBDEDB81A1A3B9C79FF147ED62576ED5', '5579254EC0327FDCF8456697BC2BB554', '计量检定', '41000002SGG82153001', '计量检定', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DBDF872E8DD824306096A40F7A0C00D5', 'EE78E0DDC743E7AC4100E84EB753EAE5', '河南省中小企业公共服务平台环境评价资讯查询', '41000002SGG05182002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DBFD93E53FBE6A505366F659BF56D287', '2CB826D6539186FAE26D8A0CC8B19F54', '台湾地区的投资者在内地投资设立合资、合作经营的演出经纪机构从事营业性演出经营活动注销', 'MB1848628XK55952005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DC0CC19530EB89F5BD2053BB024F93F5', '3B9FE7C3335DF76ABD6EE4D0C28BD411', '医师执业注册（注销）', 'MB1957883XK00278006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DC129E5A59579D8D5F99B08DD420701F', '0DCABC6DB8E5557658F705EBE03ACF4A', '河南省运输事业发展中心安全监管信息查询', '41000002SGG49011003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DC1E1C693F7D7CDE2D1FBDD5972ACC04', 'A1B24B0454823D54861A4CDA07A748C5', '河南省残联多重残疾服务查询', '41000002SGG68528006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DC252902217BE4B0A37BDE43AD57212F', '8B8BC8928B90B9B23EA7F26826635236', '出售、购买、利用国家重点保护水生野生动物及其制品审批', '005184531XK23411001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DC4CDF0BBF5BB61A1147D8C89B74C7A9', '1DA514D6D2F17DDDB16DEA08B440755E', '法律援助在线咨询', '41000002SGG24105001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DC6260BA60E0AEB449E1D487B0D5CA5B', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）变更企业负责人', 'MB1912669XK5572100f', '药品经营许可证（批发、零售连锁企业总部）变更企业负责人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DC6EA24648101C50968E79F7B66A12C1', 'BF332B196781AA60C81E5FD2FF3742E9', '技工学校延续审批', '698732712XK00121005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DC7B3E3ABC541B27F8D0A51C493896B1', '09521EC965AC45C298B704C2BB15F424', '申请变更播出区域审批（初审）', '005184654XK42638002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DC7C9AE98DFD1007AF848DCC78B6B355', 'D761D556E0EE462A6D197995210EA728', '河南省图书馆开馆时间查询', '41000002SGG35610001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DC8A34DDEB07AC9A6D4B40D27A357882', '78F0F23B34FA0FE80E28B9D0E1B4DAAC', '采集国家一级保护野生植物（农业类）审批(调控野生植物种群数量、结构)', '005184531XK24555002', '采集国家一级保护野生植物（农业类）审批(调控野生植物种群数量、结构)', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DC8E67971877E0E25CA2D1BFF5F7EBF2', '0EA47F1AAA87CF8048B90CFB85AEEF2C', '企业离退休人员法院判处死刑丧葬补助金、抚恤金申领', '698732712GG08062006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DCAA139BB928F564E6F1F257980F49DA', 'DBC3D7CFE77564A60F87FBFC49798AF1', '河南省事管局四大活动查询', '41000002SGG74847008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DCD5741DAC5B124B47E576DF36B63F89', 'B043C5F466781BBE630CE9808479D221', '药品、医疗器械互联网信息服务审批变更网站域名', 'MB1912669XK00233005', '药品、医疗器械互联网信息服务审批变更网站域名', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DCD97812A798131B4D79F79B463E7D8C', '5C9C99BF8FBB6290DD02C7B6AB7F7569', '第二、三类医疗器械生产许可证变更生产地址', 'MB1912669XK00235007', '第二、三类医疗器械生产许可证变更生产地址', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DCDF18A8822E5EBC8A0B5C8A88DC505F', '8E34592BF32353CDF67AB2E1CE57120A', '河南省招商引资项目查询', '41000002SGG85266001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD00A0127FEE8A61C1E5AE702EA2930A', '5CE287277FC5E607AFC0626BB2F11F55', '物联网产品与应用系统质量检验', '41000002SGG40125001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD0A9031D2C43CADC2D1D3EBF63433FC', '729A0D530F6A7417D4F75BB0405729EE', '采矿权延续登记', '005184224XK28706001', '采矿权延续登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD16B74F45F7296FF54F33975CB4023F', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（水泥、建筑用钢筋）生产许可证发证（涉及产业政策,兼并重组）', 'MB1912677XK55651026', '重要工业产品（水泥、建筑用钢筋）生产许可证发证（涉及产业政策,兼并重组）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD227426154983113EC569D124A464B0', 'ED1AB790D3AC960E00A9D7834C16EC74', '水运工程监理乙级企业资质认定补办申请', '005184435XK55677004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD40D239BF03754BD189422359489BC7', 'A69FAD52FE02C3C387DCE6CC08608602', '离婚登记查询', '41000002SGG59970004', '离婚登记查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD4D62F54D43B3B4E0018796A62E8211', 'E7B234C807C51669078A1C3BB3463566', '权限内利用国家重点保护陆生野生动物制品审批', '005184558XK68302005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD54D32205F95912D4C3DFA576110991', '21B1C9B88A7681BF861D1706B37B3792', '河南省人民政府 关于做好当前和今后一个时期促进就业工作的 实　施　意　见', '41000002SGG48901001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD64691B41B001983ECD4EFACCD4FBF6', 'E29AC39B44871526F395C23F6A8D62A8', '药品批发企业经营蛋白同化制剂、肽类激素审批', 'MB1912669XK55793001', '药品批发企业经营蛋白同化制剂、肽类激素审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD73CE15E623A5E6271D9F890828E9E7', '1B4A8E8E6B977C71480C43CD9892ED72', '二级注册建筑师变更(注册证书个人信息变更)', '005184400QT5941800b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD7B50F55F9E8709B6FE288F9BDB5B4F', '995691582A312BE9B2D511B08A47CA80', '药品生产许可证核发（药品上市许可持有人自行生产）', 'MB1912669XK55720001', '药品生产许可证核发（药品上市许可持有人自行生产）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD8098AD6E8E94F181A80F5C12C841A3', '56C50CEF2ABFC5E0E08DC9F37C6A4DD1', '航行通（警）告办理', '005184435GG81847001', '航行通（警）告办理', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD861CF8ABF2B74983D5ADC3A8495730', 'D9D214E6B28DA262E4B67D9FA7507F88', '省级科技类民办非企业单位进口科教用品免税资格审核', '005184638QR13045002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DD8A087AFEE2E830EAFFC8757A011DE9', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省民政厅信息公开报告查询', '41000002SGG37554017', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DDAECCFB2106F0CF3EF127C31BB22ED1', '76C2185281EBF877C41153978CDFB59F', '收费公路的鉴定和验收', '005184435QR13101001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DDB87F8013E7F39F69B6BF9361B0E269', 'FCB0DCD380DF5FBBB77F2C3AEBAE9BC3', '河南邮政用品用具企业生产名录', '41000002SGG23187001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DDC2288B48215D45E63C20C3B70C8513', '387921304369BB62D5A5E603CD9465A5', '甘草采集证核发', '005184558XK99138001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DDCA7B996FEE2F72C2A17AA6D4612E98', '2E6784788D59549E3E810E44F689C3E9', '医疗机构执业登记（中医、中西医结合医院）', 'MB1957883XK55857004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DDCD7C82EC86EB4AE07D6EA45093277F', '2C4F18F5A54B0264EAAC6E6013E00F49', '民办非企业单位名称变更登记', '005184312XK05267002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DE038202B56972A9E594C8EAF746CAF4', '2A1129964340B6A77C4A2C16C11FCCEF', '新建二苯基甲烷二异氰酸酯（MDI）项目核准', 'MB0P03925XK55679004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DE27E7B95065AEF3945011F822E94724', 'C6390A8135998641CBC991CEDAABC31F', '工程设计企业资质证书（技术负责人变更）', '005184400XK8382900e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DE3810095E483DCB6B4DE83A59229CAE', 'B1F6EE27775671F45D91B34C5863AD01', ' 交通运输人员信用信息查询', '41000002SGG61576001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DE3B6BD460C92FF0FC9DBFE5EF251893', '26E393F10294ADA5F113B833A8857126', '放射卫生技术服务机构资质（变更项目）', 'MB1957883XK00272008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DE4C27DCFDF26FB8D7D3470DA4DE8DEE', '5C9C99BF8FBB6290DD02C7B6AB7F7569', '第二、三类医疗器械生产许可证补发', 'MB1912669XK00235005', '第二、三类医疗器械生产许可证补发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DE5A71D0BD6B8155C047739200C2C762', '26E393F10294ADA5F113B833A8857126', '放射卫生技术服务机构资质注销', 'MB1957883XK00272005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DE5B4B1F55F1F96346CBE466D59661B7', 'DCCAA37E38FCF7C50243637370A41622', '电子出版物制作单位设立、变更名称、兼并、合并、分立审批（工作场所为租赁性质）', 'YZ0000000XK55755003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DE704D809A736207FDB6379E57699A72', '1BA627A4431558C5EBFDDB70B20FDA37', '公路水运工程施工企业主要负责人和安全生产管理人员安全生产考核合格证书考核发证（新申请考核）', '005184435QR13079001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DE7465369B52316DE69A4F66B02B23A6', '45FA45E780B387B61B2D8F51EFA326DA', '权限内国家重点保护陆生野生动物人工繁育许可证核发（新办）', '005184558XK99852002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DE96412956252DB10BC307FA7102971F', '23EA2D0A65BECF6A1A880398250C8178', '申请延长任期签证专办员初审', 'MB1848628GG95137002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DEA09C65868C7E3C5E041B9B436F4BAA', 'B5A172F775ECED4F3AFB96795A03A335', '拟通过复核省级企业工业设计中心名单', '41000002SGG66892001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DEBEA2F207026E0D2B2B63241BE12464', 'CD5958AF012288AC37C5B389B31FF431', '水上水下活动许可（铺设、检修、拆除水上水下电缆或者管道）', '005184435QT00314019', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DEC87ED454D0F816915B87E8C9B791E4', 'DBC3D7CFE77564A60F87FBFC49798AF1', '河南省事管局工作动态查询', '41000002SGG74847001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DEC89006AB1E9A0ECCDB1E270276A917', '9C5B869366AF240637DE3D0A93E82E68', '公职律师行政确认', '005184291QR13057004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DED8BB20C7888C8252C69D4679496E54', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）注销', 'MB1912669XK55721007', '药品经营许可证（批发、零售连锁企业总部）注销', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DEEB59684091220EE8B632A7BC438ECF', '5F3BAB6747DE856B399ABCD2AD392819', '烈士党史等相关主题教育', '41000002SGG24432001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DEF0F61177AE06F4207A8034958CE9D3', '3F73D23386FB13A18CCBC2C3FAA94B72', '省级辐射安全许可证新申请', '005185040XK00172010', '省级辐射安全许可证新申请', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DEF661D065077DDEF4E27611B1AB481A', 'A1B24B0454823D54861A4CDA07A748C5', '河南省残联智力残疾服务查询', '41000002SGG68528004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DF00B0F6738E581A9DA6628D524EBD8E', '68451DF63317B98B82FF00463D92F9CA', '探矿权勘查主矿种变更登记', '005184224XK22442006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DF2395F329CA23C568439E2F9C9A6DA9', 'AA003C2BDDD98931D0643005E1EF36FB', '拍卖企业经营文物拍卖的许可（新办）', '005184689XK06113004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DF24D5A009D4B53F908BA6E9B54A8A19', '079D84190AF494E2B74BA57D631DFE3F', '跨省经营广播电视节目传送（有线）业务延续（初审）', '005184654XK99089004', '跨省经营广播电视节目传送（有线）业务延续（初审）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DF3B4F717422BC7361E97368C911D3AB', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动变更（经营范围）', 'MB1848628XK00096024', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DF4505DED87BBA6A96433E5F2D8BD437', '08134EAC058FEDAE159F6A1ECD530087', '优新绿化树种的引进和技术推广及咨询', '41000002SGG18180001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DF4C5A03557CBAB63AF0DE0E44538393', '651646904C40996C2858355CD6073848', '基层法律服务所查询', '41000002SGG73097001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DF55B38D1CE41255E4ECED665D726DDC', '57EB203321CE983EC6F6C60FA44DC88D', '非主要农作物品种登记初审', '005184531XK18591001', '非主要农作物品种登记初审', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DF5B2B2AE9BB1532CF07307FBF2FBBD8', '8AEEEDDE4DE0CDD1069228F5B20C345C', '河南省公路客运预售车次信息查询', '41000002SGG57878007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DF69AA765F133C2FBB0B4B7C0536BAC0', '2C7E2FB43C4BD12EA403007217A8F8EC', '河南省中小企业公共服务平台就业与再就业资讯查询', '41000002SGG55038002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DF935AE24F692D58F287B3783887F513', '971EE3A5F489E45E945639DA154D8289', '病残儿医学鉴定（省级）', 'MB1957883QR00065001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DF9F5D83B4C7F2768477C883CA067DBB', 'E11A52F69537C09572BA8646CC59D9CF', '省人防办不忘初心牢记使命查询', '41000002SGG11684001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DFA414EF406DE74AF711C3993B3B0C5D', 'EA31267150BD07D7A9FC3B09AAE42033', '会计师事务所设立审批', '005184603XK04789002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DFD2712D33CBB9FC4949E134523FBCED', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂许可证变更制剂质量管理组织负责人', 'MB1912669XK5564600e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DFD3BEE96067EDF4414A1DD1D554F423', '860AD9BD6D2E884AD9961726AC04D383', '医疗器械网络销售备案凭证信息查询', '41000002SGG76137004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DFD6AEEF021FC0CAC26588FFA969DB48', 'A2C9511A28D6DCA35FEAA8BD0E749432', '河南省电子税务相关信息公众查询', '41000002SGG63019001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DFE1978ECCDF41FEF7F78F04AB7D3C40', '1395B1245980E154D6EEA522724F6A09', '医疗机构静脉用药集中调配中心（室）执业审核', 'MB1957883QT1583A001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DFEB8F51F0ED1A2D6E7E7D2E37195A73', '2CB826D6539186FAE26D8A0CC8B19F54', '台湾地区的投资者在内地投资设立合资、合作经营的演出经纪机构从事营业性演出经营活动变更（地址-租赁场地）', 'MB1848628XK55952007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DFED8C2FB4D37AA183B7AE6A23D9EBD6', '6373E3A19D4EBDBFE4C740C04C09C5DB', '麻醉药品、第一类精神药品和第二类精神药品原料药定点生产审批', 'MB1912669XK55750004', '麻醉药品、第一类精神药品和第二类精神药品原料药定点生产审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('DFEEF32E2DFA56838A68BB205180A349', 'E48DA3D6DAAA71815AA8B8E4FCDFC205', '内河通航水域载运或拖带超重、超长、超高、超宽、半潜物体许可', '005184435XK55795001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E00F28F5FE626C2D5CB9912939C57642', 'BE9A6EA7ABFDFF0ABCBBBE9790A2A353', '运输行业公司资质查询', '41000001SGG12367007', '运输行业公司资质查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E021DF0288C98D47ABAF890D6F096050', '2F5BC9F4EEBE2A48300F3A0E84BFE937', '消毒产品生产企业卫生许可（变更单位名称）', 'MB1957883XK00271005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E026185EE40A388FD37FF2041C9CA95E', 'CB2F922F7FB3404FB13EEF372ECBEB5E', '企业产品采用国际标准服务', '41000002SGG75088001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E033D49B12F420946F75BB88A94BD3FE', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '省民政厅专题专栏查询', '41000002SGG37554002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E040810EC5A11CA1DFFA27E4503075C0', '8FF8DE28437025F56B07F851B60B59D4', '河南省图书馆开馆时间查询', '41000002SGG29671004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E05F34A6471DAF5D19AE1207BFD0E9D9', 'BBD353023B764B7BFEE4729E87396BA0', '更新采伐高速公路护路林审批', '005184435XK00310006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E065F0471E6076F8130D229D78C0D68E', '3280902C6B9E22CC9401057B65E494C6', '出行提示查询', '41000002SGG53576001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E0701B2074A4268C31123A7FC86CC319', '682E330F661CBC41660119952162B7BE', '律师事务所合伙人变更备案', '005184291QT48455005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E081D3BAA414DF4DF182A4FF7370D237', 'D5E48D6EA7D3C5264F4E81B1BEFE3FA1', '12316农业农村公益信息服务', '41000002SGG46932001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E09B2B5620AD1463C2157B3102D4E848', 'ACBED3B4D3E2B736A3B4C5B20E9658CD', '信息网络传播视听节目许可证延续（初审）', '005184654QT00287005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E0E287B985B174C04194A65EB19E5EB6', '91D9E5E11B5C473F0BCACE330C353258', '河南省卫生健康委员会机构职能信息查询', '41000002SGG63521003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E126F85EE95EC8AED1E4F96DEA307BCD', 'BBE35027AFD29F280F8CB14F218D691E', '河南省假日旅游预报查询', '41000002SGG81051001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E13F6945C31A3FEE1F4B8C2EFDDFC798', '4180115BD8B1D0A3287B92446AAE0826', '对在档案工作中做出显著成绩的单位和个人的表彰或者奖励', '725831987JL00382001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E14138933F13CBAA3ECC1D7AF20A5583', '26E393F10294ADA5F113B833A8857126', '放射卫生技术服务机构资质延续', 'MB1957883XK00272004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E16D083DAB6D829B13084FBD55DD4B89', 'AA6C4ADEBCD9979B2D042976C046600B', '中级客车类型划分及等级评定', '005184435QR00066002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E16E88C576F4D0FA8365D516DE9837E7', 'B9AB43F0556A700B5CA24A12E4CCDCA2', '首次申请安全生产考核合格认定（C类）', '005184400QR02355014', '首次申请安全生产考核合格认定（C类）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E1BAF30088EF95BDBBA426DA23D35000', '1BB5DFAAB8B01AF6BA2ECEAB57EBFEC1', '出版物批发单位变更名称审批', 'YZ0000000XK55752004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E1CE478B7550BFF490A2F5BA2DFF1B92', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类体外诊断试剂产品注册人名称变更', 'MB1912669XK0022700b', '第二类体外诊断试剂产品注册人名称变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E1D03B8A722C4CC22472F91506735BAF', '370496933CCE52C470B0C82736FC227A', '河南省民族宗教公告公示查询', '41000002SGG59185010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E1D4DFB0FAEF5EE514E9D1980A554FF5', 'B043C5F466781BBE630CE9808479D221', '药品、医疗器械互联网信息服务审批变更IP地址', 'MB1912669XK0023300c', '药品、医疗器械互联网信息服务审批变更IP地址', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E1E59BFF90E8571827A15F567B952DDE', '183B6B0CD23E78824C149282B6FA46FD', '就业技能培训补贴标准查询', '41000002SGG69954001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E1FF197D2F9739CB29ED92B74DA08632', 'EE9EDCFF664FF1D5A6ED62D02A7BE7DB', '印刷宗教用品审批', 'YZ0000000XK55740001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E22BD859ED5A4D6FC3C86E9FB60E7C91', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类体外诊断试剂产品注册人住所变更', 'MB1912669XK00227011', '第二类体外诊断试剂产品注册人住所变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E23CE855743CF023FA0CC9F1968BEEFE', '5A3C5E44B783191B441A8AC0430D6C18', '继续教育基地查询', '41000002SGG86482001', '继续教育基地查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E24402E797C66EDB1159BFF1DE874AD2', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类体外诊断试剂产品生产地址变更', 'MB1912669XK00227014', '第二类体外诊断试剂产品生产地址变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E24AA76107F948A65F3DE3412934E15A', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更生产地址（增加生产地址）', 'MB1912669XK55718008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E255A3980EA398A4E65BA0BBDBC8FF23', 'B53FAC1C19C26A138518BAFAE647749E', '开采主矿种、开采方式变更登记', '005184224XK15283003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E2608CA2BDEA82FB245174C1B266FAC2', 'A9DB8E35D2FC2F34CE5887BB84EC3389', '广播电视专用频段频率使用许可证（甲类）核发（初审）', '005184654XK51599001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E261E4A95253D1C9E5E2FA658145C142', 'F8AAAD4C15CAD425B71C7A68E1F24491', '母婴保健服务人员（产前诊断）资格认定', 'MB1957883XK00269001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E2648ABD146C0A54F584CE9F50A71F35', '5DB18E8D8C049A94B7E7B7145CA10B39', '河南省产权交易政策法规查询', '41000002SGG21755002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E2C658357AF07840DC83F1FD69AD5ED5', 'B2CDD843EF1E6819494143B10A92B171', '现行档案高级检索', '41000002SGG02355001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E2CBD172059E035563AF42F96D68BB9E', '26CB7E36D1CD4AF8209407DE56835FBB', '河南省大中专毕业生就业报到证改签', '005184662GG33216002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E2CCAA162C6B57B021AE22992380248D', 'CDBF7DB63E7E3F5C5A525C8EFA7B6B08', '放射源转让审批', '005185040XK00171003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E305C720B9255EDABC8E49B6A7AF8CFE', '6904ADC22C5F55B8D5494DDC9E1F14F3', '因工程建设征用草原的审核', '005184558XK12024003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E32420B02579D62E2FAFD8C7E3082BDA', '46B8366905CB760F05EBEB24E459A7F5', '中成药生产企业异地设立前处理或提取车间批准', 'MB1912669QT00233001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E351436446EDE9C1B2FBF9B77BAA06DD', '5C9C99BF8FBB6290DD02C7B6AB7F7569', '第二、三类医疗器械生产车间或者生产线进行改造（可能影响医疗器械安全、有效的）', 'MB1912669XK0023500d', '第二、三类医疗器械生产车间或者生产线进行改造（可能影响医疗器械安全、有效的）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E35501661A62B1AF5863B38634090B5B', '228AF167E94E9ACBECEF9F8F87402AB1', '政务公开', '41000002SGG54276001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E35C0B98DA0E5E5E50A96487A56B0913', 'BF332B196781AA60C81E5FD2FF3742E9', '技工学校设立审批', '698732712XK00121002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E38E4DB0841C0F7B71B842FE70D12E50', 'BE9A6EA7ABFDFF0ABCBBBE9790A2A353', '运输行业车辆行政许可查询', '41000001SGG12367004', '运输行业车辆行政许可查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E3AA46736021FF610D800DF389E2AE51', 'D9E9DBD37EEBE5A30704A77254ED20A0', '企业从事拍卖业务许可（复审）', '005184590XK00212001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E3B51F8B50AFC9222E603F7A17A7DA49', 'EC3852A64CD8F9B5BE8B3D9660F743D0', '通讯营业网点地图查询', '41000002SGG25959001', '通讯营业网点地图查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E3B768FD54F5F22AB694F17485A101AC', '0A6B2C5241E97581B4F2236319DC47D3', '宗教、殡葬设施等建设项目临时占用林地许可', '005184558XK94519006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E3C462D7BF6919B9F4E14699EF3FA0EE', 'E744416281FCE38299280C9923ABA5E5', '个人权益记录查询打印（企业基本养老保险）', '698732712GG33235001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E3C939D2E8CCE3DA13EE2B880F153873', '045E8389C960847AB4338F59214DB3F6', '农业机械维修质量纠纷调解', '41000002SGG73688001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E3E42D5CC4D7D689C0155A4AC1F2A101', 'D86A1F3246F843EDE7A51CCEAC37618C', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动变更（名称）', 'MB1848628XK5596100f', '香港特别行政区、澳门特别行政区服务提供者在内地设立内地方控股的合资文艺表演团体从事营业性演出活动变更（名称）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E3FAAD0C0D6B0DC33790DEA41D53D5B8', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动审批（自有场地）', 'MB1848628XK0009600f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E41A5A191B28504ABF7A9697D20D6675', '6A3688B471E53726F2C2FDAF3E7868B1', '公证员执业机构变更核准（省级终审）（省内）', '005184291QT00256004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E41B4375303BB0495616FAADA548D9E9', '7C70089AE675347745C60F8CF8FA402F', '河南省交通运输信用政策文件查询', '41000002SGG79266007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E437C3D2C85339326421EC4E672E557E', 'F68AFC3452CC72C51CA028D7224312EE', '纳税人信用级别查询', '41000001SGG48659001', '纳税人信用级别查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E437E77E3429E3AA1B534F33095C9E73', '53FD619B040669CA35652326ACBC42C9', '河南省卫生健康委员会生育登记服务', '41000002SGG46944001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E4431C289DE29A19F6EA913CA07D66A7', '2A8763136AEC7E2C74B811C406A5F547', '勘查、开采矿藏项目占用林地许可', '005184558XK54697006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E44A05D2A8C08F9C88A274BEC79F9F7E', '753400F02D7B40F8D3D5C0AEE09110E5', '在自贸试验区内外商独资经营的歌舞娱乐场所经营单位变更（改建、扩建营业场所或变更场地）', 'MB1848628XK18047002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E44C9B1C6E4BACA2EF2E7E006F139AFF', 'F9F6C2B5D1572005269DA71358583CBF', '特种设备检验、检测机构核准（补发）', 'MB1912677XK5566700b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E4547993313E3CDDE64B2CC0DB930E0A', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动审批（自有场地）', 'MB1848628XK0009601a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E455FEB066D7EDDD8642DC8CBAD022AB', 'EEA21A7ACD2B74B10199B139DFCA3D19', '河南省农村饮水工作监督电话和电子邮箱', '41000002SGG05764001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E45AA3A33A9C029E4FFE83966EB589BF', 'D43188D1D43791AA55481132E8AC47CF', '对营业性演出举报人的奖励', 'MB1848628JL00470001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E46A58F0DEFDD18F7F85CE0653F30AFC', '1EFFF9875D74B9524A813BC8178AC7F3', '法人单位申请涉外调查机构资格认定', '005184355XK00076001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E4708FCBCB5505E6BEC4138D4DEDF80F', '170BE52FEEF57E8B51D51D936CC8EBA5', '农作物种子质量检验机构资格认定（申请）', '005184531XK06040001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E47232A842803F5D2B6669E0894B4C63', '30F69023755E5F36D08C4BA271BA1EFF', '河南省图书馆精神文明资讯查询', '41000002SGG88041001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E4774D9CA62E30FE79612D162409569C', '153E73BA96B656E04D823C013517C8A3', '本地沿革', '41000002SGG57758001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E47D8B31D91DE3CC91871499EA6C8A67', 'F7965C1643D65294C60EA47F1F71F5C3', '地质灾害治理工程施工单位乙级资质认定（补证）', '005184224XK2899000a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E4BC9C1167FDA0F4C1565A1E6C0D900F', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品生产许可证名称变更（企业名称、住所名称或生产地址名称变化，而生产条件未发生变化，且符合变更条件）', 'MB1912677XK55651004', '重要工业产品生产许可证名称变更（企业名称、住所名称或生产地址名称变化，而生产条件未发生变化，且符合变更条件）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E4BF66EEC2502D8EB3FA813A32077A69', 'FE3E194376806EAA75867FC750E311E4', '甲级测绘资质证书补领', '005184224QT0804A003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E4C65660AA08DA1DE9CD712DBC0255F1', 'BDA5C09BCB9FD6C0220BDED43D8A7135', '个人账户信息', '41000002SGG62166003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E4CA095D6E3DF6085E15355DC7ABE5F7', 'BDA5C09BCB9FD6C0220BDED43D8A7135', '贷款还款计划查询', '41000002SGG62166004', '贷款还款计划查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E4E863B9BF64DCD7576C415F89E7A3B5', '76A27C3FAB51112E394E34FB5104976F', '快递企业企业经营地域变更', '717817421XK55671008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E4E9D31AB1EC7DE7F74AB09F7F2B3E06', 'C7C0E8C4B463972EBC70CBB70FDA0DC2', '绿色发展相关政策咨询', '41000002SGG88298001', '绿色发展相关政策咨询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E52B0CC3A967666FDE6ABF8724368287', '9EB60BA69C65C3F4578EFF5A8EB867B0', '河南省建筑市场监管公共服务平台-专业作业企业查询', '41000002SGG34247002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E53DB7EA6C25A0176DA81E9EC95A0BF9', 'B6D9035650D1A59BC6E31C4F6D2253B4', '从事出版物印刷经营活动企业的设立、兼并、合并、分立审批（工作场所为租赁性质）', 'YZ0000000XK55728003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E5535ACB858F25421F2879CC80339212', '8031E4CBFC7C992F5C496EDD51C10F21', '中等职业教育学历认证（2008年秋季以前毕业）', '005184662GG33213003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E56188D33125D0B8CE723BB6A53A607C', '3FAE0261754585A6BEA42F6153821A75', '对国家新闻出版署负责的出版新的连续性电子出版物或连续性电子出版物变更名称审批的初审', 'YZ0000000XK55675004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E572EF90C38A5A694AF01B65D3A01844', 'D47187B8F98D809E49BCD9116F983B41', '号牌寄递运单查询', '41000002SGG38771001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E574C8045DF37CC1ABD10EE09434BB91', '42D46DEDA6EDA20F019148074B7504FE', '河南省煤矿安全培训专家库名单', '41000002SGG30478001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E5793A4EBCB4E7FF4495F05AC9A74165', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品生产许可证取证方式变更（由独立取证变更为共同取证，生产场点不变）', 'MB1912677XK55651016', '重要工业产品生产许可证取证方式变更（由独立取证变更为共同取证，生产场点不变）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E57DBC1BAC4BD6D5CB52CE24C01A255E', '2FC4F9538B60085169CC404D7F4B591B', '河南省市场监督管理局质检政务公开', '41000002SGG11969002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E592625B0A4FED8D7C5F8DE080B7BDCB', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构制剂室的关键配制设施等条件变更备案', 'MB1912669XK5564600b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E594F280328AF45B75046552601A6240', '472F7FBAFBCF9D5D6AB0212817EEEE56', '省体育局三公经费查询', '41000002SGG78500009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E59A504300B3B326CEE71D4A7B408C6D', '58EA89475A708A9E84CFAED6488EE35E', '河南省农产品质量安全追溯管理信息查询', '41000002SGG87147001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E59F71A4D11D8F38FA59700DB5A0E727', 'D4DE5AD4FFA94AEE8906A028FBBDD2A2', '危险废物转移跨省审批', '005185040XK55883003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E5A29D118F62DE0758562CC27B223210', '3AC5E5B6CD8F8A845CB69D41F96BAF72', '采矿权抵押备案', '005184224GG75336002', '采矿权抵押备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E5CACB3E09964DCE95BFEA50A5AFA4B2', '5DB18E8D8C049A94B7E7B7145CA10B39', '河南省产权交易投资融资项目查询', '41000002SGG21755004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E5CC6D70FD280540C4BFECB41C5D3340', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（电线电缆、人民币鉴别仪、广播电视传输设备、预应力混凝土铁路桥简支梁、危险化学品包装物、容器）生产许可证取证方式变更（由独立取证变更为共同取证，增加生产场点）', 'MB1912677XK5565101c', '重要工业产品（电线电缆、人民币鉴别仪、广播电视传输设备、预应力混凝土铁路桥简支梁、危险化学品包装物、容器）生产许可证取证方式变更（由独立取证变更为共同取证，增加生产场点）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E5D750992EA519D5747FF0AA5C5AAAFE', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品生产许可证取证方式变更（由共同取证变更为独立取证）', 'MB1912677XK5565100e', '重要工业产品生产许可证取证方式变更（由共同取证变更为独立取证）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E5E16B3FEF5A21B3E867FFEB4C745147', '3985FA81C2A327E2574AD14CBA9BF750', '职权范围内的医疗机构评审（中医、中西医结合医院）（省级）', 'MB1957883XK03693001', '职权范围内的医疗机构评审（中医、中西医结合医院）（省级）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E5E3329802FCF6A32C2F72C6EF07B7B9', 'E4EE6E1ECF5C561B5847FE8493C9C701', '中外及港澳台合作办学机构名单', '41000002SGG87875003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E5E81335B3BD224409E4C75CE976DE6A', '6457F8D95D15A063FF04D64D871E9A84', '无植物检疫性有害生物的种苗繁育基地、母树林基地审核', '005184531QT6710A001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E5EB9F91398390CB74822C7752FCBC7A', '64233151D76C0AD7ABA6213E0E8A82E9', '基金会注销登记', '005184312XK12195001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E5F8129B59A10B9D2861E033D7F48150', 'D599305F4C0837B1E8ADFFEFC15B83C9', '开展高速公路交通安全宣传', '41000002SGG48475001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E601BB986838C32E807C0440815C2747', '2C7F4F2BE7655B2861A03510A2D75F98', '煤矿企业安全生产许可证首次申请、延期', 'MB0P03925XKH8WQ0001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E6074887E37BFDB373A7FA49A8A64E84', '66166E4E889FF9A7822DB5BC40DFA025', '水利工程质量检测单位乙级资质变更（法定代表人）', '005184566XK97268004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E623EB0F7EEABA521ECEC66C579F1F1F', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品生产许可证补领', 'MB1912677XK55651006', '重要工业产品生产许可证补领', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E6257CAC3EA0588D07A42B1139702E3B', 'B043C5F466781BBE630CE9808479D221', '药品、医疗器械互联网信息服务审批变更网站服务器地址', 'MB1912669XK0023300b', '药品、医疗器械互联网信息服务审批变更网站服务器地址', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E633CFFD174D8FCBB58E9BC20082A2C1', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省社会福利信息查询', '41000002SGG37554005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E638DFE7497C6E94DBB84780B989870A', 'B548B58EF47E5787E92C82F0257B4FBD', '麻醉药品和第一类精神药品区域性批发企业经营审批', 'MB1912669XK55781003', '麻醉药品和第一类精神药品区域性批发企业经营审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E6464CF7D3EB95231FEA40D9746EF576', '54226AD218FCD6A2D5389E9D76790518', '药品不良反应报告、医疗器械不良事件报告统计分析情况反馈', '41000002SGG94051001', '药品不良反应报告、医疗器械不良事件报告统计分析情况反馈', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E65744F20C6E2809C280CCA3258DDF1A', 'F883C64F4A79E603FCAF1E36C3FBCD2F', '河南省民政厅媒体关注查询', '41000002SGG37554026', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E65C9F2E88716937B9A04F51FE79DAD6', 'EFE580E8D561D8EB8AFE24DACCD6D336', '饲料添加剂生产企业（增项）', '005184531XK4067100b', '饲料添加剂生产企业（增项）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E6794F1BEF9263D4CA95C908F6676124', 'A3FF2E23263B29DAB594A0C53D9542BF', '在高速公路用地范围内埋设管道设施许可', '005184435XK5575700f', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E6803FE4C7CC60C878D3F6DE3BD40A2E', '1B4A8E8E6B977C71480C43CD9892ED72', '二级注册建筑师变更(预注册转正常)', '005184400QT59418007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E687770A14F3D35265EB1AED89904C30', 'D35D896695866A0AE67FB02356A15524', '与集团内部具有控股关系的药品生产企业共用前处理和提取车间批准', 'MB1912669QT00236001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E68CB948111F2B8275E0ACF32293A663', 'C4951F6BDA98FD3B508BF1EFCD1B2F30', '交换国家所有档案的复制件的审批', '725831987XK53912001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E6A4C6A7FF1538816BE71CFDE3419239', '4A69BA6BAFCF104471659E7668FD4C34', '水运工程材料类丙级试验检测机构等级的资格认定', '005184435QR13082002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E706B6CAD2AB276BA5AC36D218353117', '3FAE0261754585A6BEA42F6153821A75', '对国家新闻出版署负责的报纸变更名称审批的初审', 'YZ0000000XK55675006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E70C6FB78B8F39BD7D730EB557317E2F', 'B3EC6D54CFB0319DBF9289BFBEA74A2E', '河南省非免疫规划疫苗采购交易服务', '41000002SGG91362001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E739132CDD7B31DFB2F15B2D5E6FA69C', 'ACE3DEB6F873805D4081C52A336E2A53', '药品经营零售GSP许可证信息查询', '41000002SGG27249009', '药品经营零售GSP许可证信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E73A6840B983108E5A95FFD031B2AB7B', '33420A82C739726657B52F72D5B82C05', '除利用新材料、新工艺和新化学物质生产的涉及饮用水卫生安全产品的审批', 'MB1957883XK00264001', '除利用新材料、新工艺和新化学物质生产的涉及饮用水卫生安全产品的审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E74AFC5A1EAB09AE45621B3CF9D588EB', '5DB18E8D8C049A94B7E7B7145CA10B39', '河南省产权交易通知公告查询', '41000002SGG21755001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E74FB3ECCB9FC72293298BC385CA23BA', '210868935EBAD2BCD4821021BEBF9656', '检验检测机构资质注销申请', 'MB1912677XK55650003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E753586180C3C83A7A2E2D7D82D08A01', 'D902193984EDF6FADFB066D9B325C959', '水旱灾害雨水墒情查询', '41000002SGG30629001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E76EC1AB7EC255D8C080E5D40F543E6C', '434C6C84ACDE85383DEEAC04A1E3B1F4', '文物拍卖标的审核', '005184689XK00382003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E78BA42C2A5ABC721621399ED161FF5D', 'C923DE4DDC9797FE75E84CEC09AB7D3A', '在自贸试验区内外商独资经营的游艺娱乐场所经营单位设立审批', 'MB1848628GG13107001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E78D4A2E40B67B69C7EFD96709E2A19D', 'A1B24B0454823D54861A4CDA07A748C5', '河南省残联视力残疾服务查询', '41000002SGG68528001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E79447590AF0F0C1A4F605BE69AA5FC4', '0A7916EAE3D20E4E0221CE06E16036AC', '国产电视剧片审查（变更剧目名称）', '005184654XK14235002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E795CF9D5FF65CB49D452A06F5CF2370', '13014568F88BFD9288147B6FDDDB4D8D', ' 高速公路出行指数查询', '41000002SGG91251001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E798594DD184FC0B3D8C412857128388', 'CAFFF3522503E54A49B1B0D359973B27', '资产评估分支机构的备案', '005184603XK55636001', '资产评估分支机构的备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E7D5CB9B44C36427E34F324755CEA54B', '3058FD60B78927F1799CE46F710089B9', ' 转报国家工业项目', '41000002SGG64194001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E7D88C54D16609BC2BE2470379DCD114', 'C448FAD4A61B2DF6F082085AABCC476A', '天地图·河南 自然保护区专题', '41000001SGG49547005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E7FB60F1BA26EFEAB841C11C335D5C03', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更生产许可项目（新增）', 'MB1912669XK55718007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E802B68AEE1E337884118D52956B04E9', 'CEFB63BF521FB46DE00CD01EEB88870A', '计量器具型式批准变更', 'MB1912677XK00081002', '计量器具型式批准变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E8127D0C5BB10FBF12CBFD800ABBD532', '82C3F18BB6E762B995A2DA9C9AA92040', '普法宣传查询', '41000002SGG13131001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E829D219493D6748BAFE8A0D250A2975', 'CAFFF3522503E54A49B1B0D359973B27', '资产评估分支机构注销备案', '005184603XK5563600a', '资产评估分支机构注销备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E8306905FE12EDEE75CD62AC1BF04746', '370496933CCE52C470B0C82736FC227A', '河南省宗教工作查询', '41000002SGG59185012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E84172FF4A1C10B590B1B99A301D5B05', '9417B489A6ADAE152C71C581FC471996', '河南省道路运输留言咨询', '41000002SGG21730003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E84A8AC8DF9DF190B76460121E194E88', 'D9396508DC9D4451C427C44E8B20BD37', '国家鼓励发展的重大环保技术装备拟推荐名单', '41000002SGG44821001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E84F841A5851346921561002220BE3E3', 'B53FAC1C19C26A138518BAFAE647749E', '采矿权缩小矿区范围变更登记', '005184224XK15283002', '采矿权缩小矿区范围变更登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E8736E607C14F0E037DFFBB77F1FBED6', '011AE2565F1F6D20757E8C1AF8D8471E', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出经纪机构从事营业性演出经营活动变更（地址-租赁场地）', 'MB1848628XK12285009', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出经纪机构从事营业性演出经营活动变更（地址-租赁场地）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E8753C6990999646BCF8B42E408FA054', '2E9BFB9749BE7E75239CD0276E6D70E5', '机关事业单位缴费信息查询', '41000002SGG03228001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E8A69C524186F8A2200B213A1E39E59C', 'E3AC4CF6783531B0E8C2EE91D4DBFE27', '省级建设项目环境影响后评价报告的备案（辐射类）', '005185040GG33196002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E8AA9A99DC92DB8E41AAF54EE258D9AF', '5F4665C5B5BA6E9F99934371BC0C4AD7', '水运建设项目竣工验收', '005184435XK55992001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E8C4B588B9D21D1150726B7931AF3896', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类医疗器械注册证注销', 'MB1912669XK00227008', '第二类医疗器械注册证注销', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E8D5179BE518C54AED49EA853CF9AB18', 'A037859ECC2E6BE3D60B7B748ED1EB8B', '水资源公报查询', '41000002SGG02819001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E8DF33D24474EC59111DA3D6C893A7BB', '3FC3295FAA63E3E64F35266235AC3715', '组建图书出版集团审核', 'YZ0000000GG33182003', '组建图书出版集团审核', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E8FE98CCE5363E9890D1CBBE40E94E5D', 'C5018DF030AD133CCFC5C2FCA0468044', '河南省市场监督管理局政策解读', '41000001SGG89294006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E928ACD1FAC1DA12E83249FA1A912005', 'C239AECCD2715DC1FEBCE8633556D338', '现有经营单位重新申领《危险废物经营许可证》', '005185040XK00173009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E93DB4E599BE907A46A5C0A0B63D9A3B', 'EFE580E8D561D8EB8AFE24DACCD6D336', '饲料生产企业（增项）', '005184531XK40671006', '饲料生产企业（增项）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E94F890A9223309928EDF8312F099F78', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更生产场地（原址新建、改建、扩建车间）', 'MB1912669XK55718013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E955B972537A04E4E53BEADBC0A69615', 'EFE580E8D561D8EB8AFE24DACCD6D336', '饲料添加剂生产企业（设立）', '005184531XK40671002', '饲料添加剂生产企业（设立）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E970848D402B5D2F694190271DEDBE08', '35AD1BF7F51C16441D7ABF7384F0AA2A', '占用国家种质资源库、种质资源保护区或者种质资源保护地审批', '005184558XK00193002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E9756176E482E1FEFE26A99F275284E0', '175C971222104069558624D3087E5397', '广播电视节目制作经营单位设立审批（注销）', '005184654XK00299003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E9987AFD4FAD812A964D6FBD478C4641', '613340BD60125024C93CA8750215C093', '机关事业单位关键信息变更', '698732712GG33225004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E9AAA3B46BDEADABA4F183FC64AA1E46', 'EFC54A32DF7246CD23B059BEB930122A', '举办香港特别行政区、澳门特别行政区的文艺表演团体、个人参加的营业性演出增加演出地备案(在演出场所举办的营业性演出)', 'MB1848628XK5596200e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E9D130A8822C1524C8592B8296944EB3', '08F7332950774CDC7E0F0BBACD430496', '跨省城镇企业职工基本养老保险关系转入（多重养老保险关系个人账户退费）', '698732712GG08089002', '跨省城镇企业职工基本养老保险关系转入（多重养老保险关系个人账户退费）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E9EF1437C42AFA7E08907014D78A20E2', '62A5E229C7EBAEA99B4A5ED974698604', '城市空气质量月排名', '41000002SGG18060001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E9F79D5FE5E0622975ECDFDBDFC5D5EA', '0AA0ED62E7FA83EE4941C9468D5E21E6', '中医医疗广告发布审查', 'MB1957883QR13042001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('E9FE640B46A75162AC6B1AA70DE8C491', 'BC223B051712FD0597B6CE4C5A83A6AB', '驾驶证实习记录查询', '41000002SGG31938001', '驾驶证实习记录查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EA0AE74A5FF860E640448F0E67B88952', 'F9227BCC8D788CA213602F26CE1521F9', '广播电视播音员、主持人资格认定（初审）', '005184654XK56673001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EA25EC8E042E84CDC962A6A794D215FC', '8031E4CBFC7C992F5C496EDD51C10F21', '中等职业教育学历认证（2008年秋季（不含）以前毕业且新生入学审批表丢失的）', '005184662GG33213002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EA3314BE66854C4BE2921FE998193325', 'CB3B145F351625D116D0D8AF5736082D', '出具社会保险信息证明', '41000002SGG73638001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EA334FF6EA8A7F5D2DF7764DC55E7A25', '44C9A98EBA0FFF0EF178445920BD9A07', '河南文化旅游手机报', '41000001SGG88174002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EA3EE9B0D814CD7D79632CB4AB9C6F3B', '6D52E0ACC02F809244466C48A116C325', '公布设区市统计调查制度', '41000002SGG18644001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EA40F2E689EF84A5DABE1FE2F3C23CE3', '8218CD97BA3F9A6CE3275AA92B6705C3', '申报项目情况', '41000002SGG42116001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EA43D0DFEE1534982727F639A9A3D9E0', 'B7BA631AB119A2533D1F2FA5D7752C95', '在国家级风景名胜区内修建缆车、索道等重大建设工程项目选址方案核准', '005184558XK35973001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EA6A1DC25FBBED280826E9FA141334AE', '7FAFAC69A42582C624E46D55BD5D4A25', '服务场所查询', '41000002SGG48762001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EA991138A608E73510DBD8B0A1424A99', '2C7E2FB43C4BD12EA403007217A8F8EC', '河南省中小企业公共服务平台失业保险资讯查询', '41000002SGG55038006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EA9FF8F1D41A6E0CF0E642FC1FB7AF70', '3BBEC83B09FFA7BDC1CDDBED43E4CA42', '大型焰火燃放作业单位资质证明核发', '005184187QT00143002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EAB370F47D25471DB7CE9A78B629026D', '7945210B7D18312B79575F79371B399B', '新项目申请涉外社会调查项目审批', '005184355XK00076002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EAEE1727C47DF0F4B02ACE4D26FD72E6', '145DDF1D41CD02D35186B75105DDD046', '除电力、通信以外的防雷装置检测单位甲级资质的延续', '415800448XK16357001', '除电力、通信以外的防雷装置检测单位甲级资质的延续', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB0543C3F2E52DBBDD19440065C6507A', 'B9AB43F0556A700B5CA24A12E4CCDCA2', '安全生产考核合格证书注销', '005184400QR0235500a', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB0D92335A803994B2C7E23E0EDC351C', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动变更（投资人员）', 'MB1848628XK00096012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB1BD8A2CA3F0A202EE0BA1AE7687B12', 'EE78E0DDC743E7AC4100E84EB753EAE5', '河南省中小企业公共服务平台资源保护资讯查询', '41000002SGG05182004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB283F615D7B69BAA28723D1CF64DD92', '86900C3824E186FA3F0722DC848BD71C', '法考服务政策法规查询', '41000002SGG77553001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB2D81FB9262B54E1D9E1DCAB3C5852E', '04AB80E847CEFD3F812174109D8EDCC6', '外国人来华工作许可证补办', '005184638XK56009002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB33A3B37B15D224F942FFD01C3A8CD7', '8A9A82758BF33235BE654E8AA5087891', '临近预报查询', '41000002SGG97829003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB434062152404C60DE2A3A2C93331DE', '2F959943F4572D8A8952EC05EB541091', '国家基本水文测站调整审批', '005184566XK55873003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB501D647E923736630BEBDD715F83B2', '30BE103EBEB8E993E5F4C4CAEB4C372D', '外商投资旅行社信息变更（出资人）', 'MB1848628XK00199008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB6E67E325ED4FB45B77C8FDE1A6CE96', '1C36DF262228D2E59D2379BC53B19E03', '对产前诊断结果有异议的医学技术鉴定', 'MB1957883QR13034004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB9149A515CEC7B8B9D176F8E9235236', 'C9B066C0D86F8E168BD572AEA3B1963D', '涉税中介机构信息查询', '41000002SGG79505002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB98483F1F2E45BE5E113094C58EF3C3', '08B1A5EF99E82E0224D0C5416510657B', '河南省客运停靠站点查询', '41000002SGG86767001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EB9E5D2A90E711A97709A3BBEB6AC657', '11098A9738D837F167EC9C0B0E4A1BFC', '肥料登记备案', '005184531QTDUO3E001', '肥料登记备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EBB07F2F7C8C55BAD8A63FA25E1189A8', '1D6F4ED04B44FD8C22C6E509F7B53314', '河南省中小企业公共服务平台装备制造行业资讯查询', '41000002SGG69935009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EBD64766DD6979376D562A093D27ECDD', '78EBDF4091A50015D16AB20FFFFD178E', '放射源诊疗技术和医用辐射机构许可（变更设备）', 'MB1957883XK00274006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EBEDB3B7ABC1A83277F5307DB6B96BCD', 'C001A9228EF0F59CF627DA4088A0F1DC', '化妆品生产许可证变更生产地址（核减生产地址或生产场地、生产车间）', 'MB1912669XK55718009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EBF28022956478FF3AA4257B73A3757D', '25B361E5DC4CEAD5F39636A087DF9440', '省级文化产业示范基地命名', 'MB1848628QR13086001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC13F3F4F7A8169CD5F68485FC480CFE', 'C5018DF030AD133CCFC5C2FCA0468044', '河南省地方标准查询', '41000001SGG89294015', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC1D8F5541D71ACC7475AC39C7CFA818', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品生产许可证终止', 'MB1912677XK55651008', '重要工业产品生产许可证终止', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC3F5C044EC3BE4F1D34564E5F4F2D18', '754004025F17D26D8ABDBAE237CAA73D', '河南省发展改革委员会疫情防控政策查询', '41000002SGG23547028', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC4101F0C525F1E61EE08783BD88B5F0', '8AD6128CC9EF1B8E09EEA5CF83123EA3', '外国人在中国境内对国家重点保护农业野生植物进行野外考察审批', '005184531XK97644001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC4ECBB9247A9AE6DB7C934FA455CB2A', '2A8763136AEC7E2C74B811C406A5F547', '宗教、殡葬设施等建设项目占用林地许可', '005184558XK54697007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC4FECC618B6AE4527D29BADBD2DD533', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（水泥、建筑用钢筋）生产许可证发证（企业及子公司、分公司共同取证，不涉及产业政策）', 'MB1912677XK55651024', '重要工业产品（水泥、建筑用钢筋）生产许可证发证（企业及子公司、分公司共同取证，不涉及产业政策）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC51892A139269DCFCF7F127206437AC', '1F0DBFC950D8F2766D90C35B5BC122A9', '外贸出口企业购买第一类中的药品类易制毒化学品审批', 'MB1912669XK55736003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC65D1062D7BA4EC8292971F9FEBCAEA', 'FDD9D5D04048F5F1F0D76DE22FF49F5F', '对传染病病人尸体或者疑似传染病病人的尸体进行解剖查验的批准', 'MB1957883QR13032001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC6F84642F1C7601F592608466827F86', '7342660123B43F44480FA3707071C532', '河南省农业农村厅农业要闻查询', '41000002SGG61100003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC7242236827262BFC4287D902E03AFD', '0BC0DDDEF7E55FB5C7C2F5684C138D3F', '国产电视动画片审查', '005184654XK13798001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC75490F44F9C5922253000730C7D996', 'DD6E2A17B8DD5EBA93BC3552C1DF313F', '工程勘察单位资质证书信息查询', '41000002SGG24557001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EC895CAF22E14400F745EACD9A2411DE', 'EE8E8A764FE9A92C454AE534F1B6402B', '河南省文物局互动交流资料下载', '41000002SGG41946001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ECD5D4BB1DD6BFADC3AA94715E0459EC', 'D4BE3C18CBF1ADAB2D68F5AF68316627', '中外合资经营、中外合作经营、外商独资经营企业互联网上网服务营业场所经营单位从事互联网上网服务经营活动变更（改建、扩建）', 'MB1848628XK78400008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ECD8DFA058AF3D238819BF67373155B4', '0595DA07B3E92CF1C9AE01E9AD76B021', '郑洛新国家自主创新示范区期刊查询', '41000002SGG95398003', '郑洛新国家自主创新示范区期刊查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ECE0588240DA7376E28A2F64BCF1688A', 'DFCEFC3AE827A00910CC585F0E96F6C6', '河南省综合评标专家库建设、管理等相关事务性工作', '41000002SGG94105001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ECE686D7980AE8A370C65670D46025E9', 'F59276FDAA163B062F2A9F5474AACBF1', '广播电视视频点播业务许可证（乙种）审批（注销）', '005184654XK55698003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ED019AFF94721AD90FC1447BA10B38BF', '23EA2D0A65BECF6A1A880398250C8178', '申请取消签证专办员初审', 'MB1848628GG95137005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ED137C60CA0D7EAB91EC4DEDDB0110DC', '56FEEBADEA7EC0A72B6499E2EB2DBA53', '虚开普通发票查询', '41000002SGG67186005', '虚开普通发票查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ED1D77B9AE55B9315735F40E00AECBAF', '387921304369BB62D5A5E603CD9465A5', '麻黄草采集证核发', '005184558XK99138002', '麻黄草采集证核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ED2E9B1619FD6BA26B65DD562C427B17', '002447207831DA53248577890AA4AD3B', '文物保护工程监理资质乙级审批', '005184689XK94270006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ED3533D430C8CE3C2D53407B3BBE1350', '170BE52FEEF57E8B51D51D936CC8EBA5', '农作物种子质量检验机构资格认定（延续）', '005184531XK06040004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ED37C2F834D333F171C29455C476B267', '0A6B2C5241E97581B4F2236319DC47D3', '审批初步设计的建设项目临时占用林地许可', '005184558XK94519002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ED563EE3B73B668C1539510E55926E39', 'B945C9F568318C9714918780B3C3061F', '事业单位招考方案公布', '41000002SGG49020001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ED615847A6BE87064972AB5A9D2C8CB6', '145DDF1D41CD02D35186B75105DDD046', '除电力、通信以外的防雷装置检测单位乙级资质的延续', '415800448XK16357008', '除电力、通信以外的防雷装置检测单位乙级资质的延续', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ED624CA27AE5BEC034EFAA60524E4401', '72F488B72C201EFDB5DF85EAA5B0E28F', '循环经济相关政策咨询', '41000002SGG25650001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ED7A7A5BF0FA7D314AD9ED6636608D62', '6EB83E97536AAEBEF462B873E79DF71B', '流动人员户口查询', '41000002SGG64812001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('ED7B23B763F61624D6DE42B1ACFA39AB', 'BC60ECBA4AE58BE7AB7CC8C611863392', '融资担保机构变更持有5%以上股权的股东备案（审核）', 'MB1530417XK00368007', '融资担保机构变更持有5%以上股权的股东备案（审核）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EDB557ADD2A10B6C2B659EC2755B5132', '6D9FF4EED74232EDA0488A8405363044', '市级文物保护单位保护范围内其他建设工程或者爆破、钻探、挖掘等作业审批', '005184689XK05887001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EDB76BC4C5046E63C1C8324886159067', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（批发、零售连锁企业总部）变更质量负责人', 'MB1912669XK55721012', '药品经营许可证（批发、零售连锁企业总部）变更质量负责人', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EDD09FD0298A33D1313447EA8C3140AD', '40541585CAC6A9674460D2431016D9B7', '药品经营许可证（零售连锁企业总部）核发', 'MB1912669XK55721014', '药品经营许可证（零售连锁企业总部）核发', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EDD480E98C66C5D56D9D35950D028236', 'C2650912C108DA36C5F5B8317F026BB8', '机动车违法信息查询服务', '41000002SGG92618001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EDEDDB79E459930D4CA04C57454F2E39', '0E98DE7C8360AFE2AFCF8F4F0EC70DEE', '办税地图', '41000002SGG49049001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE08C6CB1363BECCCB0F994365E1E6E7', 'DAB301DDC04E0D58DCD9AA1BC0217944', '省级建设项目环境影响评价文件审批（辐射类报告表）重新审核', '005185040XK49971012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE17FCB8F6B046AC8D3E4C48D01886FA', '3CE4C1446E3152AAAA792FF7772116FA', '港口危险货物建设项目安全评价', '41000002SGG87612001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE2FB9A3A75D404BE7D7D197968BEB0E', '378057DC406C575A3FA2054D9FF35A7C', '食品标准备案查询', '41000002SGG74337001', '食品标准备案查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE4C98E588EFFD5587FBBEC56DC4A0D7', 'EFE580E8D561D8EB8AFE24DACCD6D336', '饲料添加剂生产企业（注销）', '005184531XK4067100c', '饲料添加剂生产企业（注销）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE5E674658428E4D94062A5F9A3E06A9', 'DCCAA37E38FCF7C50243637370A41622', '电子出版物制作单位终止制作经营活动审批', 'YZ0000000XK55755007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE65F33800EEAD31D206F563C9A85B05', '472BD22570EE1C860F9A249E259A0561', '接收卫星传送的境外电视节目审批（注销）', '005184654XK55662005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE700F2FD8E296B400B9BC991258BF1B', '6C327CF6C1D5EEAF82EB255BB51DE509', '进出口中国参加的国际公约所限制进出口的野生植物审批（单位）', '005184531XK99520005', '进出口中国参加的国际公约所限制进出口的野生植物审批（单位）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE79438F59E48C8DE7A667382A6C9A99', '0743C3CCC5F9D364F21E84E727A9A687', '风景名胜区规划档案查询与咨询', '41000002SGG70514001', '风景名胜区规划档案查询与咨询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE884C9542740E8459D1F2EB023593ED', '6076CD368EE1474C1710CA48C0A4A50D', '工程设计变更审批', '005184435XK55807002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE8A1CB4128F73101013B58D10926BEA', '6AD8FAAB15C256761FDC214E730913F6', '国际道路运输企业终止经营', '005184435XK00317004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE8A2D89FB4C942E20260AC0739510FF', '4817D97AD22A2C80987C339C69897837', '申请通航水域水上水下活动许可进度查询', '41000002SGG15423004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE9489B5A0FCE0AEE1434A32FF7331F0', '29C143FA2EE8259F56E9DC803A3D7C1D', '河南省教育厅关于对第四届河南省乡村中小学幼儿园教师优质课评选结果的公示', '41000002SGG35583001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EE9AE7066E93B4959A8C8A2515306D58', '0D1D7F459A6C219B2B8CA1CBF777D530', '组织企业参加广交会', '41000002SGG16883001', '组织企业参加广交会', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EEA6B67C7FCEFCBF9F91C680378541C3', '011AE2565F1F6D20757E8C1AF8D8471E', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出经纪机构从事营业性演出经营活动换证', 'MB1848628XK1228500b', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出经纪机构从事营业性演出经营活动换证', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EEA8C1F4306B9D9E0730A61C2B8791AD', '738E14145B6FD25BC8AB91ED29D0440B', '水生生物资源养护与增殖放流的宣传和实施', '41000002SGG53472001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EEAF3EAAF7F9203520BEE89BD1C65E53', '450A91883ACB3FD62CD18F788AEF7061', '医院执业登记查询', '41000002SGG89390001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EEC38575A722AECA91794D37B20F8BE2', '6904ADC22C5F55B8D5494DDC9E1F14F3', '因工程建设使用草原的审核', '005184558XK12024004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EED657077D36956A83DE5FC3BF96E1D3', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动变更（外国投资人员）', 'MB1848628XK0009601e', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EEE50AEC997EAEB3C176F0A4EC1B50EB', '6D1A849B77120866144F4F89D78F599F', '司法鉴定机构变更登记（增加业务范围）审批', '005184291XK55905008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EEE51BFC59C231D8A05C42895BF1A294', '276DCABB3D77219573F9F6254F9F8D59', '进入原环保部门管理的国家级自然保护区核心区审批', '005184558XK95628001', '进入原环保部门管理的国家级自然保护区核心区审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EEED34D3AFC906B2CF75295AFB93E6DB', 'DD6FCAFB9287445EE8C3BE870F06F74F', '在高速公路服务区设置广告设施许可', '005184435QT00308006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EEFFB6471FAC9A130CAB5BB2E9D8D7BA', '753400F02D7B40F8D3D5C0AEE09110E5', '在自贸试验区内外商独资经营的歌舞娱乐场所经营单位换证', 'MB1848628XK28660002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EF4D5C70C042F7EAC5A69E6F71034E06', 'D83D25CAE0D17AF7677CCEB98677D31D', '河南省地方标准政策法规查询', '41000002SGG41087002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EF5BF1ACEE8BF33C928B9819F8B343CD', '64035ED700AF0F93C71791BF62A95C60', '失业登记查询', '41000002SGG83513001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EF87E77F61A9E540E0AB404D0E7F0393', 'C380A41ABEB31EBE46C3AF90AC46965A', '医疗器械委托检验', '41000002SGG00186001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EFC9F87FB62110A80CC0FF4EA6B4BC8F', 'BE80C6B10253ECB2E3CA1150ACE4FD6B', '民办非企业单位演出经纪机构从事营业性演出经营活动延续', 'MB1848628XK5597700b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('EFECEE792899D6D5B7BD074EA8830986', '7B67D5E777C2CFE11560973D1B3CB7BC', '河南省住房和城乡建设厅信息公开', '41000002SGG68813001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F00CAB9379D9FDFF1584ADFECF310256', '0F729E7AA8FA8C91F0FD607ADCF0E566', '河南省煤炭经营企业备案名单', '41000002SGG39877001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F035ACF46B4E3C1207DD9B5CF6352FE1', '3A419B4750C0C47534EABD704D0B9DB8', '离婚登记预约', '41000002SGG66172001', '离婚登记预约', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F03FE1F4FF8680A1C65215A1BC439A23', '752AC7B08D796482EF2A5C2F80991A89', '年度第二批河南省首台套重大技术装备产品名单', '41000002SGG39212001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F040E0B23B42F8CD4865616CB47063AE', '1DCADF8941110631E23704ADD32560E6', '音像、电子出版物复制单位设立审批（经营场所为单位自有产权）', 'YZ0000000XK55723001', '音像、电子出版物复制单位设立审批（经营场所为单位自有产权）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F05065BCFFB2C014672AA2A5AC8C84D5', '5C9C99BF8FBB6290DD02C7B6AB7F7569', '第二、三类医疗器械生产许可证注销', 'MB1912669XK00235006', '第二、三类医疗器械生产许可证注销', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0506DA01441FE5BCBBFE753DA01199C', '0B325B018C831B8D7B5C1E9411ADC098', '森林植物检疫政策法规咨询', '41000002SGG98458001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F05D7BD9E76D6020DA063F5217724B5E', 'E79A29FA59965EDF5CD70391F664BDCE', '影视节目制作机构与外方合作制作电视动画片审批（初审）', '005184654XK97687003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0631D4E571ED349DCFE8D7369DA2ECC', '7E1FF2974E559D49F269395E1177901B', '河南省依法淘汰无证生产水泥企业', '41000002SGG19953001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F06AE84BFE940FA378C21AC537ECA1A3', '81BE8C2F203F2F01DC2831B0DE6D32CD', '博士后退站办理', '698732712GG33277005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0796F818D88F02CCE449955D1EE1444', '40D0976F0C5A843CA2FA8A63D55E3BE3', ' 定制客运试点定制班线信息查询', '41000002SGG36268001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F07A2C11C78BFB4DA0E12BB58CF2C379', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类医疗器械注册证遗失补办', 'MB1912669XK0022700c', '第二类医疗器械注册证遗失补办', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F085C8CB2D66C84C634FDEA1FCBA9BAC', 'DD974B4496819F37A69BFD311FA38747', '户籍地所在办事处查询', '41000002SGG34256001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F087EF6634B44BC275E2FF1F88F1593C', '0EA47F1AAA87CF8048B90CFB85AEEF2C', '企业离退休人员法院宣告死亡丧葬补助金、抚恤金申领', '698732712GG08062003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F08F1C122B5D872FA7606DD0DA8B5C0F', '68451DF63317B98B82FF00463D92F9CA', '探矿权转让变更登记', '005184224XK22442005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F09189AA74F9525B7A219344B6E44275', 'E29AC39B44871526F395C23F6A8D62A8', '药品批发企业核减蛋白同化制剂、肽类激素经营范围', 'MB1912669XK55793002', '药品批发企业核减蛋白同化制剂、肽类激素经营范围', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0A532E77181DA038CBE42F141AD4B74', 'BEB370778E66CBD4E490B6D010A16DCA', '地质灾害防治工作中做出突出贡献奖励', '005184224JL22346001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0D634DD58CE652A60098E649B5F4541', '3BDAB30133A9D418486952DA02E918BA', '终止社会保险关系(企业) — 死亡', '698732712GG33231007', '终止社会保险关系(企业) — 死亡', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0DA5B2B914CF7D6FD8B0A38B40E5C56', '10E0B8E0DD46880B53CFF12FB534BD4D', '临时证有效期查询', '41000002SGG27644001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0DAF9F5D757400022B1D158F5155E1D', 'C43C47B865849EF86F28C17C000EFD43', '权限内肥料登记（首次）', '005184531XK46213004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0DF8DE1DA7D49B06242CD7538815D27', '8B68B942D3261AE9B4F9B7A8A7CF8061', '仲裁委员会注销登记', '005184291XK00262004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0E33E5EEBB33A01B480E44E155CC442', '754004025F17D26D8ABDBAE237CAA73D', '河南省发展改革委员会价格监测查询', '41000002SGG23547014', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0E440167AE958CCA09C59AC824B4DD1', 'AA197BFC77AB73B785C0BE0CCA4E2404', '取水许可变更（水权变更）', '005184566XK00241007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0EC2A480D6BEF3926C65089D69FB02E', '46E6F4D20A4F75DD096FF8D60AE0FC7D', '建设工程质量检测机构资质信息查询', '41000002SGG03554001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0F0826668AFDDDCDF2364074BF1025A', '753400F02D7B40F8D3D5C0AEE09110E5', '在自贸试验区内外商独资经营的歌舞娱乐场所经营单位注销', 'MB1848628XK18047001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F0FDBEA7CFAD04BD2B746346A1955761', 'B1C4DA2A1244FC660CF47EB4F965EA39', '单位参保证明查询打印（机关事业单位养老保险）', '698732712GG33234002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F1196A5FF4011C1F687DE8FED98AC04E', 'B1A447745691479268DCBCD884859188', '农业植物及其产品调运检疫及植物检疫证书签发', '005184531XK21265001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F12A566C94517CC3390490C8DB8E4418', '5EDEC19FF1D34377974CBE5EB6CB6B28', '广播电视专用频段频率使用许可证（乙类）核发（延续）', '005184654XK00286002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F1440ED30CDA36E4152DFFE94B2C180A', '860AD9BD6D2E884AD9961726AC04D383', '医疗器械出口备案查询', '41000002SGG76137007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F157F261614AA2D0D3E2307CA270FE1C', '4960CC93B38364505FEC48E6EC917CB8', '特种作业人员证书查询', '41000002SGG31990002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F17B55D951BB2DDF9829F730E9156F36', '15E1D2512ECCBAA1D1DCED5976AE44AF', '普通中专录取审批表', '005184662GG07044001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F186317E49742061AB94CB916C18D115', '6BD654211E42460AAF688E90F9E9AF1E', '社会团体名称变更登记', '005184312XK99203002', '社会团体名称变更登记', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F1872002D49772CDEE9204F7CC524B73', '5820E7651D3ACFE129EA9D5DB6655A56', '公布本省文物资源、博物馆相关信息', '41000002SGG03459001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F1C0E5AC056CE271604F6FC8026A945E', '85040ACCCC79EF599EF4A470CA8E2127', '接受进口特种设备告知', '41000002SGG66880001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F1DCFB2706FB67F6E18FF5D6082B01F2', '2D895427EBB2B6217BF974CEC88B5530', '中外合作职业技能培训机构分立、合并审批', '698732712XK55967004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F1E39DB73E004B0DC22BB3C43AA7BFAF', '4C1E1C8E96B45A3204FE479F8022E88F', '职业卫生技术服务机构资质变更', 'MB1957883XK90983005', '职业卫生技术服务机构资质变更', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F1FB5FB5ABEA2070773168C5B25CBBED', '27DEF9D1E09B677EE899D5BB11DD0E37', '建筑施工特种作业人员资格证书注销', '005184400XK55735004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F210DD3D8A1E725F990E40AE18C8B0F1', 'C0D6567DD7643E5D0A9740C14F86BA01', '海员个人信息查询', '41000002SGG40209001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F21105356FE78CB1076F31B425EAE4F2', '7D4E0F32824AAC8086A17240A65158B4', '关于对2019年河南省高等职业教育技能大赛教学能力比赛获奖名单进行公示的通知', '41000002SGG08969001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F218C4485514BACF89509C72241AD670', '608D111335FBAF9AA30358F650CF38EF', '食用菌菌种质量检验机构资格认定', '005184531XK38213002', '食用菌菌种质量检验机构资格认定', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F21F9DB0B3E31475A424295FD6C65DCF', '75A6A440591438C1FE79AADCC20B7B68', '金融产品查询', '41000002SGG55538001', '金融产品查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F228F7A4467A2AAE8E2351EED0BF9920', '1D9805DD796243E506E0341FF7EAA2CC', '河南省博士后创新实践基地查询', '41000002SGG03461001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F233237C0EA097D204C5EAA2DECAFF5F', '3CD4030D00E0C96B9D9119D24533A3B6', '法律法规查询', '41000002SGG64738001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F2446673CDF43AE2C54C038427F932A8', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（危化品、水泥和建筑用钢筋）生产许可证取证方式变更（由独立取证变更为共同取证，增加生产场点）', 'MB1912677XK55651010', '重要工业产品（危化品、水泥和建筑用钢筋）生产许可证取证方式变更（由独立取证变更为共同取证，增加生产场点）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F27E2845F13DA96F8BFEDC0FD682B682', 'E9DD82F9CE79C3412E0B12916E7121FE', '河南省残联康复指导业务查询', '41000002SGG21854001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F28BF9D74B2E5FA785E0754E7DEF4FA8', '11024BAF9018AC998491AE8DA849F20B', '招收和培养国际生的学校备案', '005184662QR58692001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F28FFA2895B2036D79141E96609BF9E1', 'F7965C1643D65294C60EA47F1F71F5C3', '地质灾害治理工程施工单位乙级资质认定（注销）', '005184224XK28990009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F2A83A7D9B3D7799BB737CA2D8AAEC05', '38B50E0CA641A3511DAFA2629D8A4B81', '河南省大中专毕业生就业报到证遗失补办', '005184662GG33217001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F2B2887798DE05424B5A2101226EE796', '8DCB216657B3F0739833F8FCF2009ACA', '融资担保公司业务培训', '41000002SGG68158001', '融资担保公司业务培训', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F2BE4B19E6EADCB8D77B96E748F37E92', 'EA0DB6AA2865B27C1CAE7F1D301110C0', '省财政厅行政许可和行政处罚信息查询', '41000002SGG03451002', '省财政厅行政许可和行政处罚信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F2C04DFB8E35B31CEA634869D024A940', 'BE9A6EA7ABFDFF0ABCBBBE9790A2A353', '河南省运输行业驾驶培训查询', '41000001SGG12367003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F2CB3CB0909C1456ABE3B1F08F8F8511', '4753DF671DDB8BC3FF0496E43079C6B5', '疫情上报', '41000002SGG90924005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F2CCD28B56BD189F4E6E1C828966F8BE', 'F170B72E1A9A8E3B021C621F00CE6A41', '计划生育技术服务人员合格证', 'MB1957883XK55802001', '计划生育技术服务人员合格证', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F2D0A775A423456144293ADAA2D2B39B', '6AB185BB2DD85BFE459FFFEE64BD5D7B', '采矿权到期查询', '41000002SGG23328001', '采矿权到期查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F2DD4CDA12E2F92A60DE167F899D74DC', '3F73D23386FB13A18CCBC2C3FAA94B72', '省级辐射安全许可证部分终止', '005185040XK00172018', '省级辐射安全许可证部分终止', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F2E8888158F688BA18005C248568C3E3', 'F93F1621B74DAE2AB0161C68F2D140D3', '政府信息公开文件制度', '41000002SGG14637001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F31020256FC52DE9BBD5455518127097', 'FA7BFB850777477B7310C5DBFF172966', '河南省重点医疗物资企业名单查询', '41000002SGG87985001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F3281B25CC74344EA95739BD3CD82840', '56C16FD5DD665356F465222229A488C0', '河南省建筑市场监管公共服务平台-建设项目信息查询', '41000002SGG20243001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F329BA3BE29F17766C22ED43DD999A3F', 'AFD82617E48490D6D5940CC7B7DC0E60', '航道养护工程交（竣）工验收', '005184435QT00309004', '航道养护工程交（竣）工验收', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F33F223F5201B5737BA043A5351A97F7', '713E174D89169A54A91D10F9B5ADD1FB', '海员证核发（补发）', '005184435XK56002004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F35D1D5A602BBAA3B0275DF164AE9CD3', '1ABF5A6FB11317D0A909A52480ABFA7C', '河南省图书馆共享工程工作园地信息查询', '41000002SGG58243003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F36B02C5F9F7D0F624ADF770F83541A0', 'DD6FCAFB9287445EE8C3BE870F06F74F', '在高速公路收费站区设置广告设施许可', '005184435QT00308002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F388E33A78EB344D6797BB75E62CAE9F', 'EE7D5D62F5C8DD3882CE56680193CF9F', '法务地图查询', '41000002SGG33433001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F389AADCB974BC83151001041A55E33F', '97220A9271559926249286F3E4B23CD2', '家畜卵子、冷冻精液、胚胎等遗传材料生产经营许可（复验换发）', '005184531XK06435005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F394A6ECB31213BF76B094DEAD7CAFE6', 'BE80C6B10253ECB2E3CA1150ACE4FD6B', '演出经纪机构从事营业性演出经营活动注销', 'MB1848628XK55977005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F39E36B82196CACFD10F60CD01254F67', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事歌舞娱乐场所经营活动变更（外国投资人员）', 'MB1848628XK00096011', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F3B7026E267FF074C4C4FEDDD5101811', 'EFE580E8D561D8EB8AFE24DACCD6D336', '饲料生产企业（注销）', '005184531XK40671004', '饲料生产企业（注销）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F3CC3CEDA5B8C122F7A16CBC604EA41F', '187D237D8FD90CF36D361DD7B9CC1AA8', '二级建造师执业资格延续注册', '005184400XK83761008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F3FA0CFDF3CBA6C8EB16DC2E0F260A9B', '6351E55B713043F92DEA0FA37103A735', '特种设备作业人员资格认定（复审）', 'MB1912677XK62933003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F405B3BFD6EA651FAC5BA5B5179B5C86', 'D0E40284EC9C9663DD50C4001D0B139E', '举办外国的文艺表演团体、个人参加的营业性演出增加演出地备案(在歌舞娱乐场所、酒吧、饭店等非演出场所举办的营业性演出)', 'MB1848628XK18069002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F406719FFB42371B50AF622DCE62F55C', 'B9AB43F0556A700B5CA24A12E4CCDCA2', '首次申请安全生产考核合格认定（A类非法人）', '005184400QR02355012', '首次申请安全生产考核合格认定（A类非法人）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F408BAF9E77879896D138053DEF7D215', 'A71971B8CE1BDC8EAE43905A1DCE9A06', '行政规范性文件查询', '41000002SGG40936001', '行政规范性文件查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F4255A8BBAD376ADD876B7636FB56995', '5AA99453C4F22FC345E7847CDB24FA98', '诉讼风险评估服务', '41000002SGG67845001', '诉讼风险评估服务', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F42B29E0670D03F6DDA3B0661FC2F47B', 'B5324763D99B4BE1C4D5E46C62CA0DB4', '燃气从业人员专业培训考核合格证书查询', '41000002SGG96018001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F42D1B9B414603958037AA81D8A208BE', '9918EF1770C80B92E4A01E8C77480CD5', '信息公开目录', '41000002SGG37956001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F42EF16B1E6B0CF566E8298CADD5650E', '0EBA0F42D82FA5AB32547430DDC162A0', '血站变更业务项目', 'MB1957883XK00268007', '血站变更业务项目', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F439C03895655CCA61BDEA77D4BAE666', '1A51619AD96EBF272F5ED3E7C40A5D9F', '河南省院士工作站建设', '005184638QT00063003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F43F218FBBAAFC9D97F2A23651E4D4F1', 'B3EF21FB567A5AD5F7536A6C4D7315FF', '粮食和物资储备行政许可查询', '41000002SGG66788006', '粮食和物资储备行政许可查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F44C48806E62A70A865CE43184DB1666', 'EC68AD4701A95E09457DB8301B1D44B9', '身份证签发日期查询', '41000002SGG77555001', '身份证签发日期查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F45F40B78019F2A94015BF5859F8C406', 'B433B8CE58BAB1A68B499049F180E650', '统计专业技术资格考试和高级统计师职务任职资格评审咨询', '41000002SGG86212001', '统计专业技术资格考试和高级统计师职务任职资格评审咨询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F4600CE7084628934FB0FA36B06F22DE', '468C547213761EF8E706C0910BAD01FA', '公众出行服务查询', '41000002SGG17138001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F463E06C556C8AB2CAB99ED6390A4790', '63D75AB786A89FFCC15BF0EF430C97E0', '地质灾害治理工程设计单位乙级资质认定（补证）', '005184224XK1609100d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F46C940942E7EF6CE82C2715D9607C22', '15EC0AD7F0C7A7B5E61DAE6F084358F8', '第二类医疗器械产品注册', 'MB1912669XK00227001', '第二类医疗器械产品注册', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F488AB6FAC034DB24237DAFDC476FD9F', 'E05AF16DA73907C6C3D88E04D32BE3DF', '知识产权优势区域认定（审批）', 'MB1912677QT3784A002', '知识产权优势区域认定（审批）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F4947BF9C56621F9231903427936645F', 'ED1AB790D3AC960E00A9D7834C16EC74', '水运工程监理乙级企业资质认定注销申请', '005184435XK5567700c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F4B4E943C3298A43B7002E3C0925F108', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（电线电缆、人民币鉴别仪、广播电视传输设备、预应力混凝土铁路桥简支梁、危险化学品包装物、容器）生产许可证发证（企业及子公司、分公司共同取证，涉及产业政策）', 'MB1912677XK55651019', '重要工业产品（电线电缆、人民币鉴别仪、广播电视传输设备、预应力混凝土铁路桥简支梁、危险化学品包装物、容器）生产许可证发证（企业及子公司、分公司共同取证，涉及产业政策）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F4C430D18871B23D89E955161B5DCBE2', '48BCC05580562CBC731CD9AD7EC4DD5E', '河南煤炭卫生学校公开招聘总成绩公示', '41000002SGG91095001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F4C7ACDB7B3514DAE4E1A95F216EA432', 'E37EB7A8109464C5106178777B2A2896', '限制使用农药经营许可证延续', '005184531XK93138003', '限制使用农药经营许可证延续', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F4CEC2311CDF5EA095A25B92A0A1DBCC', '8F901488EE391116689F7CCBD7231B07', '物联网电商在未来农业中的作用', '41000002SGG71571004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F4D8BBD4F08EDC5DC182EA68605825CF', '00A3A498AF128075BE4FE07DAF67B123', '食品添加剂生产许可变更登记事项（含生产者名称、法定代表人（负责人）、住所名称、生产地址）', 'MB1912669XK55817002', '食品添加剂生产许可变更登记事项（含生产者名称、法定代表人（负责人）、住所名称、生产地址）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F4DC1D322E7BA199BD8AE3A7FF094A90', '3EB977C0E5431C9AF3ABEEA72FE13470', '河南省大中专学校毕业生就业报到证初次办理', '005184662GG33215002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F4F94663F4B86822664CB265337E05B5', '559D7213A61573F0F75A49DA7010CB54', '外国企业常驻代表机构变更登记', 'MB1912677XK55850006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F524ACFA1A75E23E2398C4837645D87C', 'C06C81E53712D151B9400E7B6DA14BB2', '运输不属于国家所有但应当保密的档案及其复制件出境的审批', '725831987XK3634800c', '运输不属于国家所有但应当保密的档案及其复制件出境的审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F536FD32E380F37F7A5F7FFF503FC1D0', '1DCADF8941110631E23704ADD32560E6', '音像、电子出版物复制单位终止复制经营活动审批', 'YZ0000000XK55723004', '音像、电子出版物复制单位终止复制经营活动审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F543D886F2403A7366DF08918FAC4BE5', 'F2976E1F16F2C0A2337476F589DCE9C3', '省级病残儿医学鉴定', '41000002SGG57486001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F556AC10AC7BE75219B01FE4933500C5', 'A3FF2E23263B29DAB594A0C53D9542BF', '在高速公路用地范围内架设电缆设施许可', '005184435XK55757009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F558B2A3E1745A4BF8BC6D6379A2FA97', '78EBDF4091A50015D16AB20FFFFD178E', '放射源诊疗技术和医用辐射机构许可', 'MB1957883XK00274001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F56C2185EBA192EB3F4796E21EF2D2B3', 'FDA879034139553CFE158E2A744C84F6', '河南省中小企业公共服务平台创业论坛查询', '41000002SGG83510003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F5731B3EE11F729520A6996FA54C44BF', 'C5018DF030AD133CCFC5C2FCA0468044', '河南省市场监督管理局文件查询', '41000001SGG89294005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F5A8AEAA1A7B3DA41BC631F5CC8C9849', 'BF10C6B3C243DF785E1686A0B5880A6F', '依法依规推动落后产能退出工作情况企业名单', '41000002SGG92254001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F5BA10B3AE9DCFFDAAF6EE76A0EFCCE2', '3F73D23386FB13A18CCBC2C3FAA94B72', '省级辐射安全许可证重新申请', '005185040XK00172015', '省级辐射安全许可证重新申请', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F5CDE31687CC05895B21792D5B86A0AE', 'C7CCF7DB6A136C39CD9790784DC4BEAF', ' 转报国家高技术产业项目', '41000002SGG40278001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F5FA079699D9E1F5190448D28491E751', 'A9A5D3835850BAF683CEA791F5D3A76B', '医疗机构制备的正电子类放射性药品向其他医疗机构调剂审批(初审)', 'MB1912669XK00225007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F60A9A267D72670B33AC822A318F2579', '4FD4F83DE3D146CB03ECF70DDCB66110', '河南省交通运输厅政策文件查询', '41000002SGG68210004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F60C438F170DC73F0E0B85605BAEF872', '58B8696831A7C6EC5E41430A925CC2F8', '从事经营性互联网文化活动变更（公司网站域名或名称）', 'MB1848628XK02935004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F61C5ECF4EA830E1647B695388069F52', NULL, '采矿权抵押备案11', '005184224GG75336001', '采矿权抵押备案11', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F627E01CC1C2EB5E25D29B7C05ED37BA', '91D9E5E11B5C473F0BCACE330C353258', '河南省卫生健康委员会规划计划信息查询', '41000002SGG63521002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F6306D076AE651AC6E3B550CFBFFF349', '2A194D54F40BA03345576811CD57CB20', '超星期刊查询', '41000002SGG04317001', '超星期刊查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F655E5B9E8E28FA821FCC0CB1F10DF73', '2840130F47BB61EC8DD35DE5F11C75B0', '河南省自学考试教材查询', '41000002SGG64892002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F6565D2E934BAD2E21E7A1F4C8CB62B5', '754004025F17D26D8ABDBAE237CAA73D', '河南省发展改革委政府信息公开指南查询', '41000002SGG23547012', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F68C7AB9475CC981CE2F3E66C114F205', '90E5978F766FE1F2050035863C2C4E7E', '新选育或引进蚕品种中间试验同意', '005184531XK96491001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F6950354F0470FD20EC9467EC3341544', 'ECCB3379D27E5CFA18F8B16BF359D1E2', '文物商店购买文物备案', '005184689QT20271002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F6BBB061847071DE21FDA3066E339B71', '44C9A98EBA0FFF0EF178445920BD9A07', '河南旅游资讯服务', '41000001SGG88174003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F6CD20AB5E2274C58A4CB6704B2108FE', 'DAA97ABE308269285E4AB018DFC50D66', '标准评价服务', '41000002SGG70446001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F6DA551079EDBDDC6B3AA9902DB57AD7', 'D0E40284EC9C9663DD50C4001D0B139E', '举办外国的文艺表演团体、个人参加的营业性演出审批(不含未成年，在演出场所举办的营业性演出)', 'MB1848628XK18069006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F6EEF4AA68D690F203C8B208F3D8C12D', 'FDC5CED1D6C3A7DB1391D01F6711F4AE', '执业药师变更注册', 'MB1912669XK00239002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F6F2BF4FC7D3053D65C1E0D0AA2DCFBE', '0A6B2C5241E97581B4F2236319DC47D3', '勘查、开采矿藏项目临时占用林地许可', '005184558XK94519005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F6FFD71B04631F401C79E0AE5A4ED5FE', 'E4EE6E1ECF5C561B5847FE8493C9C701', '中外及港澳台合作办学项目名单', '41000002SGG87875005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F70B7B518C4131FB77725C61ED9209EB', 'ACBED3B4D3E2B736A3B4C5B20E9658CD', '信息网络传播视听节目许可证注销（初审）', '005184654QT00287006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F724EB3345797A7171CDB589BACFB7C8', '33F14732029C2E64CB9FBF9916B5A34D', '肥料登记（首次）初审', '005184531XK49543001', '肥料登记（首次）初审', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F72E8DD90F46A9BC3C08EF71E7560DC5', '6988D39DB3AF1C202552A521C376E0E9', '医疗广告注销', 'MB1957883XK76216003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F72FC81461A51C2E8E0CED5C265E361D', 'DD6FCAFB9287445EE8C3BE870F06F74F', '在高速公路过路天桥区设置广告设施许可', '005184435QT00308003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F7318E3A3BD118156B8AB72FB69C4B37', '628D42B0CD977081D8C468E7623C28F7', '就业技能培训补贴标准查询', '41000002SGG12262004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F73405B32BE1F7775075775D33C93D04', '9A9A48E1D29AB0E374272F673F7936C3', '体育类民办非企业单位业务主管单位变更的审查', '005184726GG33171006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F736631A0BC1BA2424B4773CE35AA6AE', '4A69BA6BAFCF104471659E7668FD4C34', '水运工程结构类乙级试验检测机构等级的资格认定', '005184435QR13082003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F74E50EC19E6AA7222187742CBA140BC', '8FD5DF3CA5DAD4F05F819A711E5BA765', '社会保障卡挂失', '698732712GG06930001', '社会保障卡挂失', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F75363DFD0A1E8F38E3A0F9CC43CBA33', '52BF798818215E1CC21FB0D198934DE5', '煤矿企业安全生产许可证变更（变更主要负责人）', 'MB0P03925XK25968001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F78600F01E49C6D686DEBD3B9FE18822', 'B043C5F466781BBE630CE9808479D221', '药品、医疗器械互联网信息服务审批变更网站栏目设置', 'MB1912669XK00233009', '药品、医疗器械互联网信息服务审批变更网站栏目设置', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F78A1D824F2337C17EFC0FC19B4905CB', 'EAE70C4B60E47356570496E5C253E161', '在自贸试验区内的中外合资、中外合作经营、外商独资经营的演出场所经营单位申请从事营业性演出经营活动注销', 'MB1848628XK55968005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F7A80DE5D14E446C2BBE81F661CFAE94', '6272B2E7DA5EDE117AB6083211C88E18', '河南省图书馆古籍保护政策法规查询', '41000002SGG01895001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F7B1C69FFB1A2B0D39A0500702193A1D', 'AA81B7CDC79487E94DECE7625116013E', '重要工业产品（水泥、建筑用钢筋）生产许可证发证（企业及子公司、分公司共同取证，涉及产业政策）', 'MB1912677XK55651028', '重要工业产品（水泥、建筑用钢筋）生产许可证发证（企业及子公司、分公司共同取证，涉及产业政策）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F7C369EE021FBBF472299B662E8A173D', '1F3E8080C43AE39DB9B9307DF4CC159C', '特种设备生产单位许可（许可级别改变）', 'MB1912677XK00080007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F7EC294AAAFAE52067AB41D418EAB4A7', 'F6EB794CB0188ECAE0A924A32CE6BF77', '省测绘局相关下载查询', '41000002SGG96920003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F7EEF5CE1067A478404F557B10763959', '2C7E2FB43C4BD12EA403007217A8F8EC', '河南省中小企业公共服务平台工伤保险资讯查询', '41000002SGG55038008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F7FD4D563841EDF64919B0FE62577F20', '3FAE0261754585A6BEA42F6153821A75', '对国家新闻出版署负责的期刊变更名称审批的初审', 'YZ0000000XK55675005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F804802AB1161B29B82EA6683DE4CE30', '4FD4F83DE3D146CB03ECF70DDCB66110', '河南省交通运输厅政策法规查询', '41000002SGG68210003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F806EEDFF138A18E97D2CAAA60BB5975', '28BA81D7FF2EC0435797F6E4CC6D6ACF', '统一社会信用代码数据校核与信息服务', '41000002SGG81203001', '统一社会信用代码数据校核与信息服务', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F821B542339A212FAE447040890B3E74', '011AE2565F1F6D20757E8C1AF8D8471E', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出经纪机构从事营业性演出经营活动延续', 'MB1848628XK12285003', '香港特别行政区、澳门特别行政区的投资者在内地投资设立合资、合作、独资经营的演出经纪机构从事营业性演出经营活动延续', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F8438C800C859B66DC3EB7142F8791AE', 'B20953726B6D5723DD2546403D17C0DA', '农产品质量安全追溯政策法规查询', '41000002SGG86757002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F853B87B10E0BBAD76245D319B1A55EF', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '律师事务所分所负责人变更', '005184291XK5580901b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F856DBB08C22268106D5EEC0AF1694D9', '369609ABAA25B274EBC52C7EC4BE0CF4', '成人高考成绩证明', '005184662GG06626001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F8583E308B7F6C7FAEDBB88BBA30CEA8', '3B9FE7C3335DF76ABD6EE4D0C28BD411', '医师执业注册（首次注册）', 'MB1957883XK00278003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F89D65AF24FC030FB73122624B1F2449', 'F688A17375CF069B3BFBE0417C40DC00', '对中医（专长）医师的资格认定', 'MB1957883XK55803001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F8B41C803A5440332B431D6FF49018EE', '375150439552BD43299FA405D5B9817A', '旅行指南', '41000002SGG70654001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F8B7481659F01D06374222206A12E66C', '628D42B0CD977081D8C468E7623C28F7', '失业登记查询', '41000002SGG12262003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F9158C8CA3A7B049A6EC8DC56D18ACF7', '30D70BFA323AD2533987F4E53A4BAAA4', '船员适任证书核发（补发）', '005184435XK00313004', '船员适任证书核发（补发）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F91B4CA1D452ACE1AB7CD9884E960267', '7A8E2C4CFE74F1BF3B8D18A2A1DED1DD', '安全评价网上信息公开系统', '41000002SGG64106001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F9341CD74FD2F269874CCC48B8FCA139', 'E11A52F69537C09572BA8646CC59D9CF', '省审计局审计风采查询', '41000002SGG11684006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F9358083E77515024567AFE9076AA022', 'A69FAD52FE02C3C387DCE6CC08608602', '结婚登记预约', '41000002SGG59970002', '结婚登记预约', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F938998514CFD6507E0872FF10593768', '7342660123B43F44480FA3707071C532', '河南省农业农村厅来信选登查询', '41000002SGG61100016', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F938A7BBC39677DC7F1853902C554E69', 'E14605D00650B5CD060F03CFC82BB8AA', '报纸变更开版审批', 'YZ0000000XK55761004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F964017DFADE41AD0F88CC670B1FA07B', 'E90BDEDC551A9FEC08606120AE03B847', '省内《植物检疫证书》核发', '005184558XK98912004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F9A070E2F132FE8A95E3BD3665FA165E', '8A14CD533C97619717BD5954B84DA90A', '购用毒性药品类标准品、对照品许可证明', 'MB1912669QT00230001', '购用毒性药品类标准品、对照品许可证明', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('F9FC41E4E0CE6D1AE129477CB047EC73', '6988D39DB3AF1C202552A521C376E0E9', '医疗广告审查', 'MB1957883XK76216002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA0286A623A9EC3E309B33A8AD21D54E', '170BE52FEEF57E8B51D51D936CC8EBA5', '农作物种子质量检验机构资格认定（变更）', '005184531XK06040002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA20FBC4CA34F45846524B2AEA214E1C', '2A8763136AEC7E2C74B811C406A5F547', '批次用地项目占用林地许可', '005184558XK54697003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA3B319D29332EA62FB33D71D990D06D', '4E12C4CEFF59461FC6F646F1BC6FB4E6', '发展改革普法宣传', '41000002SGG48119001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA4DBFF24E5438FC85895A8277F5307B', '7945210B7D18312B79575F79371B399B', '依申请变更涉外社会调查项目审批', '005184355XK80718002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA53FCAFC96BFCAD3498AB2A405E9686', 'C448FAD4A61B2DF6F082085AABCC476A', '河南省地理信息公共服务平台在线地图', '41000001SGG49547002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA554815D382CD1BC1F4B37977F3DD0E', '7B45900F17B97D3A266ABD2C5E886EF9', '人事考试信息公布', '41000002SGG59645001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA5C379CFBBA287D706D447AD4A3CED1', '2D1055872C9544CF086F84D38DEBB357', '三类超限运输车辆在省范围内跨设区的市行驶高速公路许可', '005184435XK00322013', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA6B5372BCB5E6DD3CB7E8AA3D983F92', 'D6449AB5E4A4141B3DC9D002AFBD54CB', '农业机械质量服务', '41000002SGG03267001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA6B6EC6359246A0CC9DC84F7AC9FDD3', '76D6F1EBF680F8394259007D9D8DCB47', '重新执业申请专职律师（曾任公职律师、公司律师）', '005184291XK1164000e', '重新执业申请专职律师（曾任公职律师、公司律师）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA70BC8024AA8F348F67151F5729BEEB', '860AD9BD6D2E884AD9961726AC04D383', '一类医疗器械生产备案凭证信息查询', '41000002SGG76137001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA720916BDEC60178CCEF33AF7C1AACE', '002447207831DA53248577890AA4AD3B', '文物保护工程监理资质申请增加乙级资质业务范围审批', '005184689XK94270007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA7EFE8E520C64752DF30EBCFE2E9A23', '0A417B9E6420CCA3B856D6FA94D20A33', '在高速公路改造平面交叉道口审批', '005184435XK55647002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA92F78D77988E15898888FAEEFC0492', '5C49C7FB63D54D7DE24A5FB5939C3EED', '在大坝管理和保护范围内修建渔塘许可', '005184566XK98056003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA956902B379DEF9D502D1EAB14D83F0', 'A5F11123B3A5A9FC95C5C356386A908F', '组织林业种苗公益性社会化法治宣传活动', '41000002SGG73722001', '组织林业种苗公益性社会化法治宣传活动', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FA9BF49F5530C385D1B99B80A41FD44C', 'B9AB43F0556A700B5CA24A12E4CCDCA2', '首次申请安全生产考核合格认定（A类法人）', '005184400QR02355011', '首次申请安全生产考核合格认定（A类法人）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FAABB0AB077B673574D38EAE7B3160A9', '370496933CCE52C470B0C82736FC227A', '涉疆服务管理工作查询', '41000002SGG59185002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FAB2CBE6A9BE16C8D936ECC8F1D3E1DF', 'BA5CA67BF533CB327B70439CF150119F', '河南省等级民宿查询', '41000002SGG00437001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FABDDB3D8EFED910EDD58CB123EBA530', '375DB634E25898EC29D4004087F29A2E', '煤矿安全评价机构变更注册地址', 'MB0P03925XK42032007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FAC44B43551734C911F921AD56B32BA1', 'A9EF852B7C55B07821BDF32DCE9EF185', '河南省监狱管理局法律法规查询', '41000002SGG68403001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FAD7B2E86778537FB4BF55E11191C97E', '1AB1DD272C7821B524CBF7728FDE8A8E', '特种设备安全宣传教育', '41000002SGG18980001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FAE6854F04E108918A492CC7175C4585', '754004025F17D26D8ABDBAE237CAA73D', '河南省投资项目老备案系统项目查询', '41000002SGG23547010', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FB0B7EFCA9B35AC31F65452299C2F0B5', 'B6954033C6100E45AA295A4111020E2F', '曾用名查询', '41000002SGG49833001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FB2711B5CF76AE5112ABDBC484F561ED', '60B10F8EA983D9FD0EFAE102BE71D0D2', '旅行社信息变更（地址-自有场地）', 'MB1848628XK00198009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FB2CF4D5C43E48715DA766AE9C4B4CF0', 'ACE3DEB6F873805D4081C52A336E2A53', '药品经营零售许可证信息查询', '41000002SGG27249010', '药品经营零售许可证信息查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FB320774405443EB9AFF121BF70D07C5', 'B063E7183121725C70A30D62FBC90611', '河南省职业资格证书查询', '41000002SGG30254001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FB43382EE6285168F468F82A44A80ABF', 'DB410B23954E3BE3279A997387942EE8', '风景名胜区规划查询', '41000002SGG18708001', '风景名胜区规划查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FB5291106AABCB674D404826BACDA58E', '561DEB2F756A1BBA3A0954663CC17F68', ' 低收入家庭业务进度查询', '41000002SGG36921001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FB5BAECF386D3E4F8358751F9ACBCE30', '16EEF651436E536283FED34A20B6B3C7', '社会团体修改章程核准', '005184312XK55641001', '社会团体修改章程核准', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FB6D0FB6BCCDA8F75365B3DDB794E771', 'DA794FD2AE0A6B5C5D09124794D73E8B', '知识产权统计数据查询', '41000002SGG04872001', '知识产权统计数据查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FB7B823CE7DBA6301969FD8A3EEFFE06', '472F7FBAFBCF9D5D6AB0212817EEEE56', '省体育局体育文化查询', '41000002SGG78500006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FBAC406DE1C9405A87D68B8C75623E24', 'EEFBA915EA1ABD68437D0ACB6A68C9EA', '网络与信息安全情况通报', '41000002SGG70000001', '网络与信息安全情况通报', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FBBEE9B5D27602F5C930EE85AA66C243', '5384982128486C8A61BEE9326133A26D', '河南省志愿组织查询', '41000002SGG09619003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FBC8F92679FE62BFB57EF11F24629FD7', '314F10B68D58B232F5CE48FE9E52CB87', '河南省中小企业公共服务平台职场故事查询', '41000002SGG96084005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FBDC9214C2F7C531803A24C52A6CB83A', '8A9A82758BF33235BE654E8AA5087891', '河南省主要旅游景点未来小时天气预报', '41000002SGG97829009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FBE4FAAD793A81FC67F1F40CE6A54F75', 'D529A2CB253922B3467B5DD363C5CF95', '煤矿建设项目安全设施设计审查', 'MB0P03925XKI3UOD001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FBE834615EF8A3B6504211DFCAA30E03', 'D0E40284EC9C9663DD50C4001D0B139E', '举办外国的文艺表演团体、个人参加的营业性演出审批(含未成年，在歌舞娱乐场所、酒吧、饭店等非演出场所举办的营业性演出)', '005184670XK18069001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FBF84B1F11D74EA6D4DC8267527B3873', '2E7BFD6B4EB7A1C5CC2B743563872AE3', '河南省技工院校2019年招生公告', '41000002SGG10031001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FBFF09F4EBB9F4FF24EA96576A7122EC', 'E8BB58B6699EAAE999EB79B5E60C8E57', '河南省交通材料水泥主要产品分析报告查询', '41000002SGG01680003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FC3D8BBEA923441B4A3C13FF102E1162', '0F7DA1E20281F78362352C60C638967E', '支持中小企业政策查询', '41000002SGG16198001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FC5F4D5519740B8954B533850214A160', 'EF8557F1CDA43FBC669440ACAD1EC815', '通知文件查询', '41000002SGG24258001', '通知文件查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FC5F5AE357B3327CC419F19B92CE414E', 'E0B563387D49C44A4CAB633E9A290042', '增值电信业务经营许可证审批', '726991560XK07809001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FC6037EA828249CDED8266B9A58435E1', 'FBA2ABAEE2D4C7BD51A31717A6ED97E9', '法考服务热点新闻查询', '41000002SGG57423001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FC6A66D54727F5E7D0D2E1BE67BF2BE6', 'C239AECCD2715DC1FEBCE8633556D338', '《危险废物经营许可证》企业名称信息变更', '005185040XK0017300b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FC84C66E10F0DB34D3321D9BE1DAF1DA', '079D84190AF494E2B74BA57D631DFE3F', '跨省经营广播电视节目传送（有线）业务注销（初审）', '005184654XK99089003', '跨省经营广播电视节目传送（有线）业务注销（初审）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FC908BCC606DBC2AFC81894D43E6D4F2', 'EA931ED609EF04DA2B0C7894778BA7F1', '河南省交通运输厅高速公路货车违法超限运输信息查询', '41000002SGG39291001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FCA3D4AA87928D4B7C759E4BA9D8A859', '5217946BD602E04E2E13AA974F27DF66', '跨区域涉税事项报告查询', '41000002SGG77974001', '跨区域涉税事项报告查询', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FCA5E25E27AB4F180BACE3F43B88A3D1', '0BC0DDDEF7E55FB5C7C2F5684C138D3F', '国产电视动画片审查（变更集数）', '005184654XK13798002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FCA9E6E8BD5F2A1A61833600AD888B66', '77C5B6574A8A7EB9BBD98212CBD584F6', '河南省重点保护植物名录查询服务', '41000002SGG53933002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FCB66ACDBE193250663033E6B10D6EF1', '079D84190AF494E2B74BA57D631DFE3F', '跨省经营广播电视节目传送（有线）业务审批（初审）', '005184654XK99089001', '跨省经营广播电视节目传送（有线）业务审批（初审）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FCC0BD0FB041498E1F6B46D6FFD73F47', '8BF2055C985B33696AE716DE0645BADA', 'CIA考试报名表', '41000002SGG82921001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD068C2C4DC1BDA8D83C34120CDA9E2E', '069A29CAE19AD408E94523B1E37AD842', '三方协议查询', '41000002SGG74156001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD0A52F91A5105837B9EF103FBD8582A', '3DB34CA479A41AE7B41A5E27D9913A21', '煤矿企业安全生产管理人员安全生产知识和管理能力考核', 'MB0P03925QT00151002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD3E64586AAFE3CB857F4FDEED760E4B', '61609A8EE18389DF87CE410FAE672189', '中外合资经营、中外合作经营企业申请从事游艺娱乐场所经营活动变更（名称）', 'MB1848628XK00096023', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD43CD45285295397F8B09088E704075', '4070A676F861975F2CAC15456E5955C9', '工程造价咨询企业乙级资质证书(含暂定乙级)查询', '41000002SGG51500001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD4D68429A5CDE28C34A3A21464840DA', '292BCD63B14A0230FE66831F9E1149B5', '开采矿产资源划定矿区范围批准', '005184224XK52998001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD6C5EC7B41AC1C1F71EBBAEAEF94BA4', '427FF13211607224B1305D6FEEDECA06', '河南省网上农展馆展馆介绍查询', '41000002SGG05790003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD8B9B9211793CDC5667DB92AEA09D93', '64233151D76C0AD7ABA6213E0E8A82E9', '基金会业务范围变更登记', '005184312XK97678003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD8F93BDCA83C2A1AF86D4224C0EAD8B', 'C233C2762B7E7DDEED81CD35C816BE99', '历史记忆', '41000002SGG27006001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD93C08E1147AAE8B22025A1A4F1C7DE', 'A1776CF64C8CDE6E4CB81A5FCC4567CA', '建设工程质量检测机构资质成立时间变更', '005184400XK5572500b', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD9639C1095E2CF8FB1FF046924714E4', '7B9248D2A437ED8B76E204F2C088B421', '水产苗种出口审（核）批', '005184531XK98207003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD9AF8DDAD0E275983C6765DE2D1F593', 'EFED162386C917C8BBF55241D7B91E21', '河南煤炭行业高级技师资格考评通过人员公示名单', '41000002SGG71839001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FD9D3859A88DBFADC59FB5869A1699AA', '8E8DFEC4F50373FBBDB44191B667595D', '放射诊疗许可证遗失或损毁补办', 'MB1957883GG79848001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FDA4DE0EA2BBA766800D6EA4DD90DB18', '63D75AB786A89FFCC15BF0EF430C97E0', '地质灾害治理工程设计单位乙级资质认定（注销）', '005184224XK1609100c', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FDACED45C865D3A4F982A0C73F9B8010', '55E51F3C555FAB274DE31FBD41732A9C', '机关养老待遇发放信息查询 ', '41000002SGG90881001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FDC2308B7822B5DE7BD66FC2AF58D8CB', '4F65B53E769030A1257956C33069277D', '产业安全预警1', '41000002SGG99563001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FDD273BD2528CBC0B5A797E2E738E782', 'B4FB53DDFC9E4BC0A95C43022E609B97', '农药生产许可证延续', '005184531XK95721003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FDD8C4C830E3CBB08D0ACA47AE388BEC', '8AEEEDDE4DE0CDD1069228F5B20C345C', '河南省客运营运客车更新、调线超座位查询', '41000002SGG57878003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FDEDFA5278DBFFAC09ED1AD57E00CA73', '9A85B2D7C84248681D6D7E8CF9845A4D', '河南省内部审计协会关于举办国际注册内部审计师资格后续教育培训班的通知', '41000002SGG04771001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE013C720301E29B2684435C23DE1557', '21336D7B16D3D82510E63B3ED76A983A', '出口欧盟原料药证明文件的出具', 'MB1912669QT00231002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE09593EA530C1463F97138FE532B0C0', 'A3FF2E23263B29DAB594A0C53D9542BF', '在高速公路用地范围内埋设电缆设施许可', '005184435XK55757018', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE1233F52522DA67D061FDEE8889D726', 'DFB86E8A59ABE91B58630324BA39C9D4', '河道管理范围内有关活动许可（在河道滩地存放物料许可）', '005184566QT00239006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE3848716A625BE1A3307501ADF4E391', '754004025F17D26D8ABDBAE237CAA73D', '河南省发展改革委员会产业集聚区查询', '41000002SGG23547026', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE486F7AF06F6F98F1CEBB8317A44C68', '776430A9C2560FE69BBA6E56223241D9', '法律援助申请资料查询', '41000002SGG07059001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE512BBCA9C39CDAFBF23E939B4F5224', '753400F02D7B40F8D3D5C0AEE09110E5', '在自贸试验区内外商独资经营的歌舞娱乐场所经营单位变更（投资人员）', 'MB1848628XK28660001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE62EEFA8A93127C3C05895068331068', 'C06C81E53712D151B9400E7B6DA14BB2', '邮寄三级档案及其复制件出境的审批', '725831987XK36348004', '邮寄三级档案及其复制件出境的审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE635BBBA390A4D8317F0AE6D4097FEB', 'D89DD96B6BE36CD2F8126341C967D00D', '省级行政区域内经营广播电视节目传送（无线）业务审批(注销）', '005184654XK35389003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE79642A2B1BE60701CB9D1D3CEC4F16', '754004025F17D26D8ABDBAE237CAA73D', '河南省发展改革委员会委发公文查询', '41000002SGG23547015', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE7A7580AA3266313D26BE84EC30D0BF', 'C06C81E53712D151B9400E7B6DA14BB2', '邮寄不属于国家所有但应当保密的档案及其复制件出境的审批', '725831987XK3634800d', '邮寄不属于国家所有但应当保密的档案及其复制件出境的审批', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE8D3894045DFA1DFB930C947BA4D4B1', '187D237D8FD90CF36D361DD7B9CC1AA8', '二级建造师执业资格变更注册（个人信息变更）', '005184400XK83761009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FE999A8C906089BCBAEE03CA7058A342', '0C5C41949E0F94DCDC0A0F309D9521F5', '河南省新型冠状病毒引发肺炎定点医疗机构查询', '41000002SGG07337001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FEBDB70467350ADC5A6F1FD0AFA805CB', '2B6F32D3FAF68F05D4F05F020DF12F6A', '出版外国图书合同登记', 'YZ0000000QR13014002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FEC495CBAB64EA29282A8BC37C83F4AA', 'E2259407A2F50F8AAD558285F6F7BB48', '河南省中小企业公共服务平台法律服务法律援助查询', '41000002SGG80640004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FEC4BBBC117E0817BF302B0380713827', 'DFECFC976D9EFDFBDFC88B7E53DA2CCA', '省级文物保护单位建设控制地带内建设工程设计方案审批', '005184689XK99316001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FEC599DBC2747B5530021401B6E70E2C', '623870B908329687543B08D55B6C9171', '台站仪器查询', '41000002SGG94618001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FEC5E4C57EA886276AE1483944861E56', 'A9A5D3835850BAF683CEA791F5D3A76B', '医疗单位使用放射性药品（三、四类） 许可证核发', 'MB1912669XK00225009', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FEC782F3DB28A23E6B647025CAFD8CF3', 'D83D25CAE0D17AF7677CCEB98677D31D', '河南省地方标准通知公告查询', '41000002SGG41087003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FED295DC9983982EEF2841182C3DC6D8', 'C448FAD4A61B2DF6F082085AABCC476A', '河南省专题信息查询系统', '41000001SGG49547007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FED848D550364F8836E531B769361A6F', 'B5F0DB6EF189B7F628DF0B890B17C7E7', '河南省水利网办事指南', '41000002SGG24177001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FEDA5ED33B9DEBBCF066A52B448DA3CB', '4FA4BF99B813DCA70203836338BF9D71', '河南粮食生产核心区期刊查询', '41000002SGG93319001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FEDF0032CB9B13899A3664E830618754', 'ED933FE49D39A88FC5418CD8202E2B9E', '非经营性互联网文化单位申请从事非经营性互联网文化活动备案', 'MB1848628GG33236001', '非经营性互联网文化单位申请从事非经营性互联网文化活动备案', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF02BFDDDF21F209B27E2C33E5244447', '271982065AD0DCC2F22A1BE65BC3DF19', '跨省经营广播电视节目传送（无线）业务审批注销（初审）', '005184654XK45662004', '跨省经营广播电视节目传送（无线）业务审批注销（初审）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF0C93121F52171461A451CC31AB2C62', 'C923DE4DDC9797FE75E84CEC09AB7D3A', '在自贸试验区内外商独资经营的游艺娱乐场所经营单位变更（游戏游艺设备）', 'MB1848628GG13107008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF3115BA7B017C18E78CEEAABB1038EF', 'FFD49B2D50431CF59CC8744EB0C3D3CB', '企业离退休人员服刑期满释放后恢复养老金发放', '698732712GG66003005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF329C91D65B26EB9CFDEABCA049D960', 'F308C48291E172CF556879209CE98C51', '法考服务办事指南查询', '41000002SGG35944001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF3AB3A611CFA18AD5574B3B7DAEB26A', '0938422E8E37D33CAAB4C8F178B391F3', '医疗机构设置人类精子库审批', 'MB1957883XK55859002', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF3B8152C4B7DD91C2873E893C7811BD', '47A4ED83CEE0AA57465F30E426954DF5', '期房网签合同', '41000002SGG16906001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF3CAB8CC6A8ED99F0E33EE2C139DDEA', 'BC69F2919A26302B40851BF436D04433', '河南省历史文化名镇名村名录查询', '41000002SGG55082001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF5E6D7318BDBC6AA4F3DED4E4589B5A', '6C327CF6C1D5EEAF82EB255BB51DE509', '进出口中国参加的国际公约所限制进出口的野生植物审批（个人）', '005184531XK99520002', '进出口中国参加的国际公约所限制进出口的野生植物审批（个人）', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF5EEF02009112021A8A7D71EC422A47', '66166E4E889FF9A7822DB5BC40DFA025', '水利工程质量检测单位乙级资质变更（详细地址）', '005184566XK97268007', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF7F524A19599CAB5058461A83B2A2DE', '90F2E33321DBDEA6151B3FF26F2F6D11', '医疗机构中药制剂委托配制备案续展', 'MB1912669XK55646008', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF8E3B6DA502D5720534FA650047C479', 'B6D9035650D1A59BC6E31C4F6D2253B4', '从事出版物印刷经营活动企业变更经营场所审批（工作场所为单位自有产权）', 'YZ0000000XK55728005', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF910CDE2CA5C052410601D584EA7A0D', '643D32E0B4ED227553BFD18336FCA331', '特岗教师面试成绩结果查询', '41000002SGG28259004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FF9A0D1277258117C9C7E08B739E9C22', '145DDF1D41CD02D35186B75105DDD046', '除电力、通信以外的防雷装置检测单位甲级资质的分立', '415805134XK16357001', '除电力、通信以外的防雷装置检测单位甲级资质的分立', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FFA04C89FD0F5F363F1DA5ED6E52D8E7', '0A7916EAE3D20E4E0221CE06E16036AC', '国产电视剧片审查（变更集数）', '005184654XK14235003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FFAB9464B1444140B5EB0EBDC8AEDE7E', '5D597E722611B3FB06E5468B5746E0DD', '电子出版物出版单位年度出版计划审核', 'YZ0000000GG33184003', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FFACDBA78D28DEAD74249367E6523111', 'ED6CA5BCB484A0E3FCDE72B28810D3CB', '律师事务所注销', '005184291XK5580900d', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FFB450C099C230B8D10EC46D865AD134', 'F5C08DCE26D55866E33CA65EE7891B9A', '河南省图书馆豫图百年', '41000002SGG41208001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FFB84F72D434B51A68508C370C159819', '28164FFDB963ACCA9BA0A094FD53A59B', '对纳税人延期缴纳税款的核准', '005184611XK19949001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FFC394DF493CA8A60EB70726ABA7DA01', 'D4BE3C18CBF1ADAB2D68F5AF68316627', '中外合资经营、中外合作经营、外商独资经营企业互联网上网服务营业场所经营单位从事互联网上网服务经营活动变更（名称、法定代表人、主要负责人）', 'MB1848628XK78400004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FFD555A61105852CA4441B42A09F907B', '8A9A82758BF33235BE654E8AA5087891', '河南省公路天气查询', '41000002SGG97829001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FFDD9C56C1B571950671C213223BE044', 'FB2F1EF1A256BE5298EF56E3628B009B', '民办职业培训学校变更审批（名称变更）', '698732712XK00123006', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FFDF82C9B11943DD2969D7E3FAE163F6', '76A27C3FAB51112E394E34FB5104976F', '快递企业经营范围变更', '717817421XK55671004', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FFDF9020C8D6642F49F35B9509DA4FD3', '09AF7AD611A6D5C11985490940BC9B25', '户号查询', '41000002SGG77490001', NULL, '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_es_pre_service` VALUES ('FFE01A3C8A895F1E409F4201891C6CE2', 'C47B71DD3B9E7BD431F97D6AA02F5051', '河南省教育厅关于对第二批河南省名班主任工作室主持人拟认定名单进行公示的通知', '41000002SGG56208001', NULL, '0', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_file_record
-- ----------------------------
DROP TABLE IF EXISTS `sys_file_record`;
CREATE TABLE `sys_file_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件转义后名称',
  `bucket_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '桶名称',
  `original_file_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件原始名称',
  `file_type` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file_size` bigint(20) NULL DEFAULT NULL COMMENT '文件大小',
  `path` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件相对路径',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '0-正常，1-删除',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件管理表' ROW_FORMAT = Dynamic;

-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `keep_alive` int(11) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮 T顶部菜单）',
  `query` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(11) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `sort_order` int(11) NULL DEFAULT 0 COMMENT '显示顺序',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 147 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', NULL, '/admin', '/admin', -1, 'icon-quanxianguanli', 0, 'M', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'youngAdmin', '2022-08-16 13:50:47');
INSERT INTO `sys_menu` VALUES (2, '用户管理', NULL, '/admin/user/index', '/admin/user/index', 1, 'icon-yonghuguanli', 0, 'C', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (3, '用户新增', 'sys_user_add', '', NULL, 2, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (4, '用户修改', 'sys_user_edit', '', NULL, 2, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (5, '用户删除', 'sys_user_del', '', NULL, 2, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (6, '导入导出', 'sys_user_import_export', '', NULL, 2, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (7, '菜单管理', NULL, '/admin/menu/index', '/admin/menu/index', 1, 'icon-caidanguanli', 0, 'C', NULL, 1, 40, '0', 'admin', '2022-08-08 16:20:42', 'youngAdmin', '2022-10-23 21:39:18');
INSERT INTO `sys_menu` VALUES (11, '菜单新增', 'sys_menu_add', '', NULL, 7, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (12, '菜单修改', 'sys_menu_edit', '', NULL, 7, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (13, '菜单删除', 'sys_menu_del', '', NULL, 7, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (14, '角色管理', NULL, '/admin/role/index', '/admin/role/index', 1, 'icon-jiaoseguanli', 0, 'C', NULL, 1, 10, '0', 'admin', '2022-08-08 16:20:42', 'youngAdmin', '2022-10-23 21:38:02');
INSERT INTO `sys_menu` VALUES (15, '角色新增', 'sys_role_add', '', NULL, 14, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (16, '角色修改', 'sys_role_edit', '', NULL, 14, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (17, '角色删除', 'sys_role_del', '', NULL, 14, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (18, '分配权限', 'sys_role_perm', '', NULL, 14, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (19, '导入导出', 'sys_role_import_export', '', NULL, 14, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (20, '部门管理', NULL, '/admin/dept/index', '/admin/dept/index', 1, 'icon-web-icon-', 0, 'C', NULL, 1, 30, '0', 'admin', '2022-08-08 16:20:42', 'youngAdmin', '2022-10-23 21:38:44');
INSERT INTO `sys_menu` VALUES (21, '部门新增', 'sys_dept_add', '', NULL, 20, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (22, '部门修改', 'sys_dept_edit', '', NULL, 20, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (23, '部门删除', 'sys_dept_del', '', NULL, 20, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (24, '岗位管理', NULL, '/admin/post/index', '/admin/post/index', 1, 'icon-gangweiguanli', 0, 'C', NULL, 1, 20, '0', 'admin', '2022-08-08 16:20:42', 'youngAdmin', '2023-02-19 20:20:24');
INSERT INTO `sys_menu` VALUES (25, '岗位查看', 'sys_post_get', '', NULL, 24, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (26, '岗位新增', 'sys_post_add', '', NULL, 24, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (27, '岗位修改', 'sys_post_edit', '', NULL, 24, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (28, '岗位删除', 'sys_post_del', '', NULL, 24, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (29, '导入导出', 'sys_post_import_export', '', NULL, 24, '#', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-08-08 16:20:42', 'admin', '2022-08-08 16:20:42');
INSERT INTO `sys_menu` VALUES (30, '开发平台', NULL, '/gen', NULL, -1, 'icon-rizhiguanli', 0, 'M', NULL, 1, 20, '0', 'youngAdmin', '2022-08-14 22:20:13', 'youngAdmin', '2023-02-11 15:51:30');
INSERT INTO `sys_menu` VALUES (31, '代码生成', NULL, '/gen/index', '/gen/index', 30, 'icon-rizhi2', 0, 'C', NULL, 1, 0, '0', 'youngAdmin', '2022-08-14 22:20:46', 'youngAdmin', '2023-02-19 20:21:23');
INSERT INTO `sys_menu` VALUES (32, '表单生成', NULL, '/gen/form', '/gen/index', 30, 'icon-bug', 0, 'C', NULL, 1, 1, '0', 'youngAdmin', '2022-08-14 22:21:12', 'youngAdmin', '2023-02-19 20:21:31');
INSERT INTO `sys_menu` VALUES (33, '表单设计', NULL, '/gen/design', '/gen/design', 30, 'icon-huaban88', 0, 'C', NULL, 1, 2, '0', 'youngAdmin', '2022-08-14 22:27:27', 'youngAdmin', '2023-02-19 20:21:44');
INSERT INTO `sys_menu` VALUES (34, '数据源', NULL, '/gen/datasource', '/gen/datasource', 30, 'icon-canshu', 0, 'C', NULL, 1, 3, '0', 'youngAdmin', '2022-08-14 22:27:48', 'youngAdmin', '2023-02-19 20:21:52');
INSERT INTO `sys_menu` VALUES (35, '系统配置管理', '', '/admin/sysconfig/index', '/admin/sysconfig/index', 55, 'icon-canshu', 1, 'C', '', 1, 8, '0', 'admin', '2022-09-03 03:56:28', 'youngAdmin', '2022-09-03 13:24:37');
INSERT INTO `sys_menu` VALUES (36, '系统配置查看', 'admin_sysconfig_get', NULL, NULL, 35, '1', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-09-03 03:56:28', 'admin', '2022-09-03 03:56:28');
INSERT INTO `sys_menu` VALUES (37, '系统配置新增', 'admin_sysconfig_add', NULL, NULL, 35, '1', 0, 'F', NULL, 1, 1, '0', 'admin', '2022-09-03 03:56:28', 'admin', '2022-09-03 03:56:28');
INSERT INTO `sys_menu` VALUES (38, '系统配置修改', 'admin_sysconfig_edit', NULL, NULL, 35, '1', 0, 'F', NULL, 1, 2, '0', 'admin', '2022-09-03 03:56:28', 'admin', '2022-09-03 03:56:28');
INSERT INTO `sys_menu` VALUES (39, '系统配置删除', 'admin_sysconfig_del', NULL, NULL, 35, '1', 0, 'F', NULL, 1, 3, '0', 'admin', '2022-09-03 03:56:28', 'admin', '2022-09-03 03:56:28');
INSERT INTO `sys_menu` VALUES (40, '字典管理', '', '/admin/sysdict/index', '/admin/sysdict/index', 55, 'icon-zhongyingwen', 1, 'C', '', 1, 8, '0', 'admin', '2022-09-03 05:43:38', 'youngAdmin', '2023-02-19 20:21:12');
INSERT INTO `sys_menu` VALUES (41, '字典查看', 'admin_sysdicttype_get', NULL, NULL, 40, '1', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-09-03 05:43:38', 'admin', '2022-09-03 05:43:38');
INSERT INTO `sys_menu` VALUES (42, '字典新增', 'admin_sysdicttype_add', NULL, NULL, 40, '1', 0, 'F', NULL, 1, 1, '0', 'admin', '2022-09-03 05:43:38', 'admin', '2022-09-03 05:43:38');
INSERT INTO `sys_menu` VALUES (43, '字典修改', 'admin_sysdicttype_edit', NULL, NULL, 40, '1', 0, 'F', NULL, 1, 2, '0', 'admin', '2022-09-03 05:43:38', 'admin', '2022-09-03 05:43:38');
INSERT INTO `sys_menu` VALUES (44, '字典删除', 'admin_sysdicttype_del', NULL, NULL, 40, '1', 0, 'F', NULL, 1, 3, '0', 'admin', '2022-09-03 05:43:38', 'admin', '2022-09-03 05:43:38');
INSERT INTO `sys_menu` VALUES (45, '流程表单', '', '/bpm/form/index', '/bpm/form/index', 81, 'icon-biaodanguanli', 0, 'C', '', 1, 1, '0', 'admin', '2022-09-15 16:34:39', 'neo', '2023-02-26 23:08:21');
INSERT INTO `sys_menu` VALUES (46, '工作流的表单定义查看', 'bpm_bpmform_get', NULL, NULL, 45, '1', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-09-15 16:34:40', 'admin', '2022-09-15 16:34:40');
INSERT INTO `sys_menu` VALUES (47, '工作流的表单定义新增', 'bpm_bpmform_add', NULL, NULL, 45, '1', 0, 'F', NULL, 1, 1, '0', 'admin', '2022-09-15 16:34:40', 'admin', '2022-09-15 16:34:40');
INSERT INTO `sys_menu` VALUES (48, '工作流的表单定义修改', 'bpm_bpmform_edit', NULL, NULL, 45, '1', 0, 'F', NULL, 1, 2, '0', 'admin', '2022-09-15 16:34:40', 'admin', '2022-09-15 16:34:40');
INSERT INTO `sys_menu` VALUES (49, '工作流的表单定义删除', 'bpm_bpmform_del', NULL, NULL, 45, '1', 0, 'F', NULL, 1, 3, '0', 'admin', '2022-09-15 16:34:41', 'admin', '2022-09-15 16:34:41');
INSERT INTO `sys_menu` VALUES (50, '用户分组', '', '/bpm/group/index', '/bpm/group/index', 81, 'icon-yonghuguanli', 0, 'C', '', 1, 2, '0', 'admin', '2022-09-25 02:14:44', 'youngAdmin', '2022-10-05 22:44:51');
INSERT INTO `sys_menu` VALUES (51, '流程用户分组查看', 'bpm_user_group_get', NULL, NULL, 50, '1', 0, 'F', NULL, 1, 0, '0', 'admin', '2022-09-25 02:14:44', 'youngAdmin', '2022-09-25 11:06:23');
INSERT INTO `sys_menu` VALUES (52, '流程用户分组新增', 'bpm_user_group_add', NULL, NULL, 50, '1', 0, 'F', NULL, 1, 1, '0', 'admin', '2022-09-25 02:14:44', 'youngAdmin', '2022-09-25 11:06:32');
INSERT INTO `sys_menu` VALUES (53, '流程用户分组修改', 'bpm_user_group_edit', NULL, NULL, 50, '1', 0, 'F', NULL, 1, 2, '0', 'admin', '2022-09-25 02:14:44', 'youngAdmin', '2022-09-25 11:06:43');
INSERT INTO `sys_menu` VALUES (54, '流程用户分组删除', 'bpm_user_group_del', NULL, NULL, 50, '1', 0, 'F', NULL, 1, 3, '0', 'admin', '2022-09-25 02:14:44', 'youngAdmin', '2022-09-25 11:06:50');
INSERT INTO `sys_menu` VALUES (55, '后台配置', NULL, '/sys', NULL, -1, 'icon-xitongguanli', 0, 'M', NULL, 1, 10, '0', 'youngAdmin', '2022-09-03 13:23:49', 'youngAdmin', '2023-02-11 15:51:24');
INSERT INTO `sys_menu` VALUES (56, '基础监控', NULL, '/base', NULL, -1, 'icon-jiankong', 0, 'M', NULL, 1, 30, '0', 'youngAdmin', '2022-09-04 21:03:43', 'youngAdmin', '2023-02-19 20:16:15');
INSERT INTO `sys_menu` VALUES (67, 'swagger文档', NULL, '/frame/swagger/index', '/frame/swagger/index', 56, 'icon-canshu', 0, 'C', NULL, 1, 1, '0', 'youngAdmin', '2022-09-04 21:04:26', 'youngAdmin', '2022-09-04 23:20:28');
INSERT INTO `sys_menu` VALUES (68, 'druid', NULL, '/frame/druid/index', '/frame/druid/index', 56, 'icon-yanzhengma', 0, 'C', NULL, 1, 2, '0', 'youngAdmin', '2022-09-05 21:44:41', 'youngAdmin', '2023-02-19 20:22:06');
INSERT INTO `sys_menu` VALUES (69, '流程中心', NULL, '/bpm', '/bpm', -1, 'icon-shujuzhanshi2', 0, 'C', NULL, 1, 40, '0', 'youngAdmin', '2022-09-16 00:35:32', 'youngAdmin', '2023-02-19 20:16:46');
INSERT INTO `sys_menu` VALUES (70, '流程模型', NULL, '/bpm/model/index', '/bpm/model/index', 81, 'icon-web-icon-', 0, 'C', NULL, 1, 3, '0', 'youngAdmin', '2022-09-26 23:43:40', 'youngAdmin', '2022-10-05 22:45:21');
INSERT INTO `sys_menu` VALUES (71, '模型查询', 'bpm_model_query', '', '/bpm/model/index', 70, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2022-09-26 23:49:05', 'youngAdmin', '2022-09-26 23:49:05');
INSERT INTO `sys_menu` VALUES (72, '模型创建', 'bpm_model_create', '', '/bpm/model/index', 70, '#', 0, 'F', NULL, 1, 2, '0', 'youngAdmin', '2022-09-26 23:49:34', 'youngAdmin', '2022-09-26 23:49:34');
INSERT INTO `sys_menu` VALUES (73, '模型导入', 'bpm_model_import', '', '/bpm/model/index', 70, '#', 0, 'F', NULL, 1, 3, '0', 'youngAdmin', '2022-09-26 23:50:03', 'youngAdmin', '2022-09-26 23:50:03');
INSERT INTO `sys_menu` VALUES (74, '模型更新', 'bpm_model_update', '', '/bpm/model/index', 70, '#', 0, 'F', NULL, 1, 4, '0', 'youngAdmin', '2022-09-26 23:50:30', 'youngAdmin', '2022-09-26 23:50:30');
INSERT INTO `sys_menu` VALUES (75, '模型删除', 'bpm_model_delete', '', '/bpm/model/index', 70, '#', 0, 'F', NULL, 1, 5, '0', 'youngAdmin', '2022-09-26 23:51:01', 'youngAdmin', '2022-09-26 23:51:01');
INSERT INTO `sys_menu` VALUES (76, '模型部署', 'bpm_model_deploy', '', '/bpm/model/index', 70, '#', 0, 'F', NULL, 1, 6, '0', 'youngAdmin', '2022-09-26 23:51:35', 'youngAdmin', '2022-09-26 23:51:35');
INSERT INTO `sys_menu` VALUES (77, '流程定义查询', 'bpm_process_definition_query', '', '/bpm/model/index', 70, '#', 0, 'F', NULL, 1, 7, '0', 'youngAdmin', '2022-09-26 23:52:02', 'youngAdmin', '2022-09-26 23:52:17');
INSERT INTO `sys_menu` VALUES (78, '流程任务分配规则查询', 'bpm_task_assign_rule_query', '', '/bpm/model/index', 70, '#', 0, 'F', NULL, 1, 8, '0', 'youngAdmin', '2022-09-26 23:52:50', 'youngAdmin', '2022-09-26 23:52:50');
INSERT INTO `sys_menu` VALUES (79, '流程任务分配规则创建', 'bpm_task_assign_rule_create', '', '/bpm/model/index', 70, '#', 0, 'F', NULL, 1, 9, '0', 'youngAdmin', '2022-09-26 23:53:19', 'youngAdmin', '2022-09-26 23:53:19');
INSERT INTO `sys_menu` VALUES (80, '修改流程规则', 'task_assign_rule_update', '', NULL, 70, '#', 0, 'F', NULL, 1, 11, '0', 'youngAdmin', '2022-10-04 22:08:54', 'youngAdmin', '2022-10-04 22:09:09');
INSERT INTO `sys_menu` VALUES (81, '流程管理', NULL, '/bpm/manager', '/bpm/manager', 69, 'icon-xitongguanli', 0, 'C', NULL, 1, 1, '0', 'youngAdmin', '2022-10-05 22:44:32', 'youngAdmin', '2023-02-19 20:22:17');
INSERT INTO `sys_menu` VALUES (82, '任务中心', NULL, '/bpm/task', '/bpm/task', 69, 'icon-quanxianguanli', 0, 'C', NULL, 1, 2, '0', 'youngAdmin', '2022-10-05 22:46:27', 'youngAdmin', '2022-10-05 22:46:27');
INSERT INTO `sys_menu` VALUES (83, '我的流程', NULL, '/bpm/processInstance/index', '/bpm/processInstance/index', 82, 'icon-guanwang', 0, 'C', NULL, 1, 1, '0', 'youngAdmin', '2022-10-07 23:23:08', 'youngAdmin', '2022-10-07 23:23:08');
INSERT INTO `sys_menu` VALUES (84, '取消', 'process_instance_cancel', '', NULL, 83, '#', 0, 'F', NULL, 1, 2, '0', 'youngAdmin', '2022-10-07 23:26:42', 'youngAdmin', '2022-10-07 23:27:37');
INSERT INTO `sys_menu` VALUES (85, '发起流程', 'process_instance_start', '', NULL, 83, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2022-10-07 23:27:32', 'youngAdmin', '2022-10-07 23:28:06');
INSERT INTO `sys_menu` VALUES (86, '详情', 'process_instance_query', '', NULL, 83, '#', 0, 'F', NULL, 1, 3, '0', 'youngAdmin', '2022-10-07 23:28:39', 'youngAdmin', '2022-10-07 23:28:39');
INSERT INTO `sys_menu` VALUES (87, '待办', NULL, '/bpm/task/todo', '/bpm/task/todo', 82, 'icon-fensiguanli', 0, 'C', NULL, 1, 2, '0', 'youngAdmin', '2022-10-16 17:38:41', 'youngAdmin', '2022-10-16 17:38:41');
INSERT INTO `sys_menu` VALUES (88, '审批', 'bpm_task_update', '', '/bpm/task/todo', 87, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2022-10-16 17:39:09', 'youngAdmin', '2022-10-16 17:39:09');
INSERT INTO `sys_menu` VALUES (89, '已办', NULL, '/bpm/task/done', '/bpm/task/done', 82, 'icon-record', 0, 'C', NULL, 1, 3, '0', 'youngAdmin', '2022-10-16 20:15:03', 'youngAdmin', '2022-10-16 20:15:03');
INSERT INTO `sys_menu` VALUES (90, '已办', 'bpm_task_query', '', '/bpm/task/done', 89, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2022-10-16 20:15:14', 'youngAdmin', '2022-10-16 20:15:14');
INSERT INTO `sys_menu` VALUES (91, '开发流程页面', NULL, '/diy', '/diy', 69, 'icon-wxbgongju1', 0, 'M', NULL, 1, 3, '0', 'youngAdmin', '2022-10-19 22:46:19', 'youngAdmin', '2023-02-19 20:22:26');
INSERT INTO `sys_menu` VALUES (92, '业务表单', NULL, '/bpm/diy-oa/leave/index', '/bpm/diy-oa/leave/index', 91, 'icon-web-icon-', 0, 'C', NULL, 1, 1, '0', 'youngAdmin', '2022-10-19 22:47:03', 'youngAdmin', '2022-10-19 22:49:07');
INSERT INTO `sys_menu` VALUES (93, '创建', 'oa_leave_create', '', NULL, 92, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2022-10-19 22:51:12', 'youngAdmin', '2022-10-19 22:51:12');
INSERT INTO `sys_menu` VALUES (94, '取消', 'oa_leave_cancel', '', NULL, 92, '#', 0, 'F', NULL, 1, 2, '0', 'youngAdmin', '2022-10-19 22:54:15', 'youngAdmin', '2022-10-19 22:54:15');
INSERT INTO `sys_menu` VALUES (95, '详情', 'oa_leave_detail', '', NULL, 92, '#', 0, 'F', NULL, 1, 3, '0', 'youngAdmin', '2022-10-19 22:54:30', 'youngAdmin', '2022-10-19 22:54:30');
INSERT INTO `sys_menu` VALUES (96, '业务系统', NULL, '/business', '/business', -1, 'icon-rizhi1', 0, 'M', NULL, 1, 50, '0', 'youngAdmin', '2022-10-25 22:50:58', 'youngAdmin', '2023-02-19 20:16:57');
INSERT INTO `sys_menu` VALUES (97, '热词管理', NULL, '/business/es/index', '/business/es/index', 96, 'icon-zhongyingwen', 1, 'C', NULL, 1, 3, '0', 'youngAdmin', '2022-10-25 22:52:07', 'youngAdmin', '2023-02-19 20:22:59');
INSERT INTO `sys_menu` VALUES (98, '查看热词', 'es_detail', '', NULL, 97, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2022-10-25 23:42:48', 'youngAdmin', '2022-10-29 22:04:18');
INSERT INTO `sys_menu` VALUES (99, '修改热词', 'es_edit', '', NULL, 97, '#', 0, 'F', NULL, 1, 2, '0', 'youngAdmin', '2022-10-25 23:43:13', 'youngAdmin', '2022-10-29 22:04:22');
INSERT INTO `sys_menu` VALUES (100, '删除热词', 'es_del', '', NULL, 97, '#', 0, 'F', NULL, 1, 3, '0', 'youngAdmin', '2022-10-25 23:43:46', 'youngAdmin', '2022-10-29 22:04:26');
INSERT INTO `sys_menu` VALUES (101, '排队取号业务', NULL, '/order', NULL, 96, 'icon-biaoge1', 0, 'M', NULL, 1, 1, '0', 'youngAdmin', '2022-10-28 00:04:08', 'youngAdmin', '2023-02-19 20:22:35');
INSERT INTO `sys_menu` VALUES (102, '队列配置', NULL, '/business/queue/list', '/business/businessQueue/queue/index', 101, 'icon-canshu', 0, 'C', NULL, 1, 1, '0', 'youngAdmin', '2022-10-28 00:19:59', 'youngAdmin', '2022-10-29 19:48:56');
INSERT INTO `sys_menu` VALUES (103, '新增队列', 'business_queue_add', '', NULL, 102, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2022-10-29 11:35:34', 'youngAdmin', '2022-10-29 22:03:55');
INSERT INTO `sys_menu` VALUES (104, '修改队列', 'business_queue_edit', '', NULL, 102, '#', 0, 'F', NULL, 1, 2, '0', 'youngAdmin', '2022-10-29 13:03:16', 'youngAdmin', '2022-10-29 22:04:00');
INSERT INTO `sys_menu` VALUES (105, '绑定窗口', 'business_queue_bind_window', '', NULL, 102, '#', 0, 'F', NULL, 1, 3, '0', 'youngAdmin', '2022-10-29 13:03:30', 'youngAdmin', '2022-10-29 13:03:30');
INSERT INTO `sys_menu` VALUES (106, '删除队列', 'business_queue_del', '', NULL, 102, '#', 0, 'F', NULL, 1, 4, '0', 'youngAdmin', '2022-10-29 13:04:26', 'youngAdmin', '2022-10-29 22:04:06');
INSERT INTO `sys_menu` VALUES (107, '窗口配置', NULL, '/business/window/list', '/business/businessQueue/window/index', 101, 'icon-navicon-zdgl', 0, 'C', NULL, 1, 2, '0', 'youngAdmin', '2022-10-29 20:15:33', 'youngAdmin', '2022-10-29 20:44:13');
INSERT INTO `sys_menu` VALUES (108, '新增窗口', 'business_window_add', '', NULL, 107, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2022-10-29 20:28:57', 'youngAdmin', '2022-10-29 22:03:37');
INSERT INTO `sys_menu` VALUES (109, '修改窗口', 'business_window_edit', '', NULL, 107, '#', 0, 'F', NULL, 1, 2, '0', 'youngAdmin', '2022-10-29 20:31:46', 'youngAdmin', '2022-10-29 22:03:41');
INSERT INTO `sys_menu` VALUES (110, '删除窗口', 'business_window_del', '', NULL, 107, '#', 0, 'F', NULL, 1, 3, '0', 'youngAdmin', '2022-10-29 20:32:01', 'youngAdmin', '2022-10-29 22:03:46');
INSERT INTO `sys_menu` VALUES (111, '取号机', NULL, '/business/machine/list', '/business/businessQueue/machine/index', 101, 'icon-erji-zuhushouye', 0, 'C', NULL, 1, 3, '0', 'youngAdmin', '2022-10-29 22:01:43', 'youngAdmin', '2022-10-29 22:01:43');
INSERT INTO `sys_menu` VALUES (112, '添加取号机', 'business_machine_add', '', NULL, 111, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2022-10-29 22:02:36', 'youngAdmin', '2022-10-29 22:03:22');
INSERT INTO `sys_menu` VALUES (113, '编辑取号机', 'business_machine_edit', '', NULL, 111, '#', 0, 'F', NULL, 1, 2, '0', 'youngAdmin', '2022-10-29 22:02:55', 'youngAdmin', '2022-10-29 22:03:27');
INSERT INTO `sys_menu` VALUES (114, '删除取号机', 'business_machine_del', '', NULL, 111, '#', 0, 'F', NULL, 1, 3, '0', 'youngAdmin', '2022-10-29 22:03:14', 'youngAdmin', '2022-10-29 22:03:14');
INSERT INTO `sys_menu` VALUES (115, '取号机绑定队列业务', 'business_machine_bind_queue', '', NULL, 111, '#', 0, 'F', NULL, 1, 4, '0', 'youngAdmin', '2022-10-29 22:40:09', 'youngAdmin', '2022-10-29 22:40:09');
INSERT INTO `sys_menu` VALUES (116, '取号订单', NULL, '/business/ticket/list', '/business/businessQueue/ticketOrder/index', 101, 'icon-dingdan', 0, 'C', NULL, 1, 4, '0', 'youngAdmin', '2022-10-30 10:44:01', 'youngAdmin', '2022-10-30 10:44:01');
INSERT INTO `sys_menu` VALUES (117, '删除票号', 'business_ticket_order_del', '', NULL, 116, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2022-10-30 10:44:52', 'youngAdmin', '2022-10-30 10:44:52');
INSERT INTO `sys_menu` VALUES (118, '查看取号详情', 'business_ticket_order_view', '', NULL, 116, '#', 0, 'F', NULL, 1, 2, '0', 'youngAdmin', '2022-10-30 10:45:18', 'youngAdmin', '2022-10-30 10:45:18');
INSERT INTO `sys_menu` VALUES (119, 'Oauth2.0', NULL, '/oauth', NULL, -1, 'icon-quanxianshezhi', 0, 'M', NULL, 1, 5, '0', 'youngAdmin', '2022-11-10 15:36:55', 'youngAdmin', '2023-03-01 18:20:54');
INSERT INTO `sys_menu` VALUES (120, '客户端', NULL, '/oauth/manage/client', '/oauth2/client/index', 119, 'icon-yanzhengma1', 0, 'C', NULL, 1, 1, '0', 'youngAdmin', '2022-11-10 15:38:11', 'youngAdmin', '2023-03-02 20:33:09');
INSERT INTO `sys_menu` VALUES (121, '编辑', 'open_oauth2_client_edit', '', NULL, 120, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2022-11-12 22:04:05', 'youngAdmin', '2022-11-12 22:04:05');
INSERT INTO `sys_menu` VALUES (122, '删除', 'open_oauth2_client_del', '', NULL, 120, '#', 0, 'F', NULL, 1, 2, '0', 'youngAdmin', '2022-11-12 22:05:46', 'youngAdmin', '2022-11-12 22:05:46');
INSERT INTO `sys_menu` VALUES (123, 'token', NULL, '/oauth/manage/token', '/oauth2/token/index', 119, 'icon-quanxianshezhi', 0, 'C', NULL, 1, 2, '0', 'youngAdmin', '2022-11-12 22:06:48', 'youngAdmin', '2023-03-02 20:33:16');
INSERT INTO `sys_menu` VALUES (124, '存储配置', NULL, '/sys/preferences/config', '/sys/preferences/config/config', 125, 'icon-rizhiguanli', 0, 'C', NULL, 1, 3, '0', 'youngAdmin', '2022-11-27 22:09:16', 'youngAdmin', '2022-12-06 23:30:32');
INSERT INTO `sys_menu` VALUES (125, '对象存储', NULL, '/oss', NULL, -1, 'icon-iconset0265', 0, 'M', NULL, 1, 1, '0', 'youngAdmin', '2022-12-06 23:30:24', 'youngAdmin', '2023-05-11 10:25:37');
INSERT INTO `sys_menu` VALUES (126, '文件列表', NULL, '/file/index', '/admin/file/index', 125, 'icon-rizhiguanli', 0, 'C', NULL, 1, 1, '0', 'youngAdmin', '2022-12-06 23:31:36', 'youngAdmin', '2022-12-06 23:32:23');
INSERT INTO `sys_menu` VALUES (127, '报表中心', NULL, '/report', NULL, -1, 'icon-biaodanguanli', 0, 'M', NULL, 1, 60, '0', 'youngAdmin', '2022-12-10 15:06:57', 'youngAdmin', '2023-02-19 20:17:50');
INSERT INTO `sys_menu` VALUES (128, '来源收入统计', NULL, '/report/test1', '/frame/layouts/IframePageView', 131, 'icon-msnui-supervise', 0, 'C', '{\"path\":\"http://22q376j554.oicp.vip:8090/api/jmreport/view/f5f275b5e28b45256ef24587ec792f0c\",\"neadToken\":\"yes\"}', 1, 1, '0', 'youngAdmin', '2022-12-10 15:07:50', 'youngAdmin', '2023-03-02 14:10:10');
INSERT INTO `sys_menu` VALUES (129, 'iframe2', NULL, '/iframe/test2', '/frame/layouts/IframePageView', 131, 'icon-miyue', 0, 'C', '{\"path\":\"http://22q376j554.oicp.vip:8090/api/jmreport/view/762663796728832000\"}', 1, 2, '0', 'youngAdmin', '2022-12-10 22:28:30', 'youngAdmin', '2023-03-02 14:10:17');
INSERT INTO `sys_menu` VALUES (130, '积木表单设计', NULL, '/report/jimu', '/frame/layouts/IframePageView', 127, 'icon-canshu', 0, 'C', '{\"path\":\"http://22q376j554.oicp.vip:8090/api/jmreport/list\",\"neadToken\":\"yes\"}', 1, 0, '0', 'youngAdmin', '2022-12-11 10:32:24', 'youngAdmin', '2023-03-02 14:08:44');
INSERT INTO `sys_menu` VALUES (131, '报表列表', NULL, '/report/list', NULL, 127, 'icon-shujuzhanshi2', 0, 'M', NULL, 1, 1, '0', 'youngAdmin', '2022-12-11 13:23:05', 'youngAdmin', '2023-02-19 20:23:22');
INSERT INTO `sys_menu` VALUES (132, 'as', NULL, 'a', 'a', -1, 'icon-rizhiguanli', 0, 'C', NULL, 1, 999, '1', 'youngAdmin', '2022-12-27 00:05:43', 'youngAdmin', '2022-12-27 00:07:39');
INSERT INTO `sys_menu` VALUES (133, 'asqq', NULL, 'a', 'a', -1, 'icon-rizhiguanli', 0, 'C', NULL, 1, 999, '1', 'youngAdmin', '2022-12-27 00:05:51', 'youngAdmin', '2022-12-27 00:11:28');
INSERT INTO `sys_menu` VALUES (134, 'asqqwww111', NULL, 'a', 'a', -1, 'icon-rizhiguanli', 0, 'C', NULL, 1, 999, '1', 'youngAdmin', '2022-12-27 00:06:05', 'youngAdmin', '2022-12-27 00:11:31');
INSERT INTO `sys_menu` VALUES (135, 'IoT设备', NULL, '/iot/index', '/business/iot/iot-card', 96, 'icon-wxbgongju', 0, 'C', NULL, 1, 2, '0', 'youngAdmin', '2023-01-15 02:26:35', 'youngAdmin', '2023-02-19 20:22:51');
INSERT INTO `sys_menu` VALUES (136, '消息中心', NULL, '/msg', NULL, -1, 'icon-send', 0, 'M', NULL, 1, 70, '0', 'youngAdmin', '2023-02-11 15:52:24', 'youngAdmin', '2023-02-19 20:18:03');
INSERT INTO `sys_menu` VALUES (137, '站内信管理', NULL, '/msg/notify', '', 136, 'icon-biaodansheji', 0, 'M', NULL, 1, 1, '0', 'youngAdmin', '2023-02-11 15:53:35', 'youngAdmin', '2023-02-19 20:23:47');
INSERT INTO `sys_menu` VALUES (138, '站内信模板', NULL, '/msg/notify/notify-template', '/admin/msg/notify/template/index', 137, 'icon-canshu', 0, 'C', NULL, 1, 1, '0', 'youngAdmin', '2023-02-11 15:55:16', 'youngAdmin', '2023-02-11 20:34:51');
INSERT INTO `sys_menu` VALUES (139, '新增站内信模板', 'sys_notify_template_create', '', NULL, 138, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2023-02-11 16:21:25', 'youngAdmin', '2023-02-11 16:21:25');
INSERT INTO `sys_menu` VALUES (140, '站内信模板测试', 'sys_notify_template_test', '', NULL, 138, '#', 0, 'F', NULL, 1, 2, '0', 'youngAdmin', '2023-02-11 16:23:29', 'youngAdmin', '2023-02-11 16:24:32');
INSERT INTO `sys_menu` VALUES (141, '站内信模板修改', 'sys_notify_template_edit', '', NULL, 138, '#', 0, 'F', NULL, 1, 3, '0', 'youngAdmin', '2023-02-11 16:23:48', 'youngAdmin', '2023-02-11 16:24:01');
INSERT INTO `sys_menu` VALUES (142, '站内信模板删除', 'sys_notify_template_del', '', NULL, 138, '#', 0, 'F', NULL, 1, 4, '0', 'youngAdmin', '2023-02-11 16:24:22', 'youngAdmin', '2023-02-11 16:24:22');
INSERT INTO `sys_menu` VALUES (143, '站内信记录', NULL, '/msg/notify/record', '/admin/msg/notify/message/index', 137, 'icon-tubiaozhizuomoban-27', 0, 'C', NULL, 1, 2, '0', 'youngAdmin', '2023-02-11 21:08:18', 'youngAdmin', '2023-02-19 22:33:20');
INSERT INTO `sys_menu` VALUES (144, '邮箱管理', NULL, '/msg/mail', NULL, 136, 'icon-rizhiguanli', 0, 'M', NULL, 1, 2, '0', 'youngAdmin', '2023-02-11 23:22:26', 'youngAdmin', '2023-02-11 23:22:41');
INSERT INTO `sys_menu` VALUES (145, '消息详情', 'sys_notify_msg_detail', '', NULL, 143, '#', 0, 'F', NULL, 1, 1, '0', 'youngAdmin', '2023-02-14 01:19:18', 'youngAdmin', '2023-02-14 01:19:18');
INSERT INTO `sys_menu` VALUES (146, '我的站内信', NULL, '/msg/notify/mine', '/admin/msg/notify/mine/index', 137, 'icon-yonghuguanli1', 0, 'C', NULL, 1, 3, '0', 'youngAdmin', '2023-02-19 22:34:26', 'youngAdmin', '2023-02-19 22:34:26');

-- ----------------------------
-- Table structure for sys_msg_notify
-- ----------------------------
DROP TABLE IF EXISTS `sys_msg_notify`;
CREATE TABLE `sys_msg_notify`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `to_user_id` bigint(20) NULL DEFAULT NULL COMMENT '接受人id',
  `to_username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接收人名称',
  `from_user_id` bigint(20) NULL DEFAULT NULL COMMENT '发送者id',
  `from_username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发送人名称',
  `notify_template_id` bigint(20) NULL DEFAULT NULL COMMENT '站内信模板id',
  `notify_template_code` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '站内信模板编码',
  `notify_template_type` int(11) NULL DEFAULT NULL COMMENT '站内信模板类型：0-系统，1-通知公告',
  `notify_content` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发送内容',
  `read_status` int(11) NULL DEFAULT 1 COMMENT '阅读状态：0-未读，1-已读',
  `read_time` datetime NULL DEFAULT NULL COMMENT '已读时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '站内信消息记录' ROW_FORMAT = Dynamic;

-- ----------------------------

-- ----------------------------
-- Table structure for sys_msg_notify_template
-- ----------------------------
DROP TABLE IF EXISTS `sys_msg_notify_template`;
CREATE TABLE `sys_msg_notify_template`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `template_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '站内信模板名称',
  `template_code` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '站内信模板编码',
  `template_content` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '站内信模板内容',
  `template_type` int(11) NULL DEFAULT NULL COMMENT '模板类型(0-系统消息，1-通知公告)',
  `template_params` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模板参数数组',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态（0-正常，1-关闭）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统站内信' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_msg_notify_template
-- ----------------------------
INSERT INTO `sys_msg_notify_template` VALUES (1, '测试', 'test_code', '我是谁：{name}', 0, '[\"name\"]', '0', '1', '', '2023-02-11 16:07:59', 'youngAdmin', '2023-02-11 20:49:21', NULL);
INSERT INTO `sys_msg_notify_template` VALUES (2, '测试1', 'test_code1', '我是谁：{name}，在干什么:{thing}', 0, '[\"name\",\"thing\"]', '0', '0', 'youngAdmin', '2023-02-11 17:07:15', 'youngAdmin', '2023-02-11 20:44:46', NULL);

-- ----------------------------
-- Table structure for sys_oauth2_access_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth2_access_token`;
CREATE TABLE `sys_oauth2_access_token`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户编号',
  `user_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户账户',
  `user_type` tinyint(4) NOT NULL COMMENT '用户类型',
  `access_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访问令牌',
  `refresh_token` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '刷新令牌',
  `client_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户端编号',
  `scopes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权范围',
  `expires_time` datetime NOT NULL COMMENT '过期时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'OAuth2 访问令牌' ROW_FORMAT = DYNAMIC;

-- ----------------------------

-- ----------------------------
-- Table structure for sys_oauth2_approve
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth2_approve`;
CREATE TABLE `sys_oauth2_approve`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `user_type` tinyint(4) NOT NULL COMMENT '用户类型',
  `client_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户端编号',
  `scope` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '授权范围',
  `approved` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否接受',
  `expires_time` datetime NOT NULL COMMENT '过期时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'OAuth2 批准表' ROW_FORMAT = DYNAMIC;

-- ----------------------------

-- ----------------------------
-- Table structure for sys_oauth2_client
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth2_client`;
CREATE TABLE `sys_oauth2_client`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `client_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户端编号',
  `client_secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户端密钥',
  `client_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '应用名',
  `client_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '应用图标',
  `client_description` varchar(4096) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '应用描述',
  `client_status` tinyint(4) NOT NULL COMMENT '状态',
  `access_token_validity_seconds` int(11) NOT NULL COMMENT '访问令牌的有效期',
  `refresh_token_validity_seconds` int(11) NOT NULL COMMENT '刷新令牌的有效期',
  `redirect_uris` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '可重定向的 URI 地址',
  `authorized_grant_types` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '授权类型',
  `scopes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权范围',
  `auto_approve_scopes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '自动通过的授权范围',
  `authorities` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限',
  `resource_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源',
  `additional_information` varchar(4096) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '附加信息',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'OAuth2 客户端表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oauth2_client
-- ----------------------------
INSERT INTO `sys_oauth2_client` VALUES (1, 'admin', 'admin_secret', '单点登录测试app', 'http://192.168.31.10:9001/web-tiny/2023/02/10/0qzta3x3wajzzkq9d9o8.jpeg', NULL, 0, 1800000, 43200000, '[\"https://www.baidu.com\"]', '[\"authorization_code\",\"client_credentials\",\"password\",\"implicit\",\"refresh_token\"]', '[\"user.read\",\"user.write\"]', '[\"user.read\",\"user.write\"]', '[]', '[]', NULL, 'youngAdmin', '2022-11-10 18:30:09', 'youngAdmin', '2023-02-10 23:51:48', '0');
INSERT INTO `sys_oauth2_client` VALUES (2, 'sso', 'sso_secret', '单点登录测试app', 'http://192.168.31.10:9001/web-tiny/2023/02/10/cdbyl86igvn5bbblxrsb.jpg', NULL, 0, 1800000, 43200000, '[\"http://127.0.0.1:18088\"]', '[\"authorization_code\",\"client_credentials\",\"password\",\"implicit\",\"refresh_token\"]', '[\"user.read\",\"user.write\"]', '[\"user.read\",\"user.write\"]', '[]', '[]', NULL, 'youngAdmin', '2022-11-10 18:30:09', 'youngAdmin', '2023-02-10 23:52:11', '0');

-- ----------------------------
-- Table structure for sys_oauth2_code
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth2_code`;
CREATE TABLE `sys_oauth2_code`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `user_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `user_type` tinyint(4) NOT NULL COMMENT '用户类型',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '授权码',
  `client_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户端编号',
  `scopes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '授权范围',
  `expires_time` datetime NOT NULL COMMENT '过期时间',
  `redirect_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '可重定向的 URI 地址',
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '状态',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'OAuth2 授权码表' ROW_FORMAT = DYNAMIC;

-- ----------------------------

-- ----------------------------
-- Table structure for sys_oauth2_refresh_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth2_refresh_token`;
CREATE TABLE `sys_oauth2_refresh_token`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户编号',
  `user_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `refresh_token` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '刷新令牌',
  `user_type` tinyint(4) NOT NULL COMMENT '用户类型',
  `client_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '客户端编号',
  `scopes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权范围',
  `expires_time` datetime NOT NULL COMMENT '过期时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '刷新令牌' ROW_FORMAT = DYNAMIC;

-- ----------------------------

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(11) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'CEO', '董事长', 1, '0', '0', 'admin', '2022-08-08 16:13:18', 'admin', '2022-08-08 16:13:18', NULL);
INSERT INTO `sys_post` VALUES (2, 'manager', '总经理', 0, '0', '0', 'youngAdmin', '2022-08-27 23:38:50', 'youngAdmin', '2022-08-27 23:38:50', '总经理领导');
INSERT INTO `sys_post` VALUES (3, 'dept_leader', '部门主管', 0, '0', '0', 'youngAdmin', '2022-08-27 23:41:16', 'youngAdmin', '2022-08-27 23:46:34', '部门领导==');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `role_sort` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', '超管', 1, '0', '', '2022-08-08 16:07:17', 'youngAdmin', '2022-10-06 09:50:25');
INSERT INTO `sys_role` VALUES (2, '普通用户', 'GENERAL_USER', '普通用户', NULL, '0', 'youngAdmin', '2022-08-28 00:17:25', 'youngAdmin', '2022-12-27 00:04:26');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1);
INSERT INTO `sys_role_menu` VALUES (1, 2);
INSERT INTO `sys_role_menu` VALUES (1, 3);
INSERT INTO `sys_role_menu` VALUES (1, 4);
INSERT INTO `sys_role_menu` VALUES (1, 5);
INSERT INTO `sys_role_menu` VALUES (1, 6);
INSERT INTO `sys_role_menu` VALUES (1, 7);
INSERT INTO `sys_role_menu` VALUES (1, 11);
INSERT INTO `sys_role_menu` VALUES (1, 12);
INSERT INTO `sys_role_menu` VALUES (1, 13);
INSERT INTO `sys_role_menu` VALUES (1, 14);
INSERT INTO `sys_role_menu` VALUES (1, 15);
INSERT INTO `sys_role_menu` VALUES (1, 16);
INSERT INTO `sys_role_menu` VALUES (1, 17);
INSERT INTO `sys_role_menu` VALUES (1, 18);
INSERT INTO `sys_role_menu` VALUES (1, 19);
INSERT INTO `sys_role_menu` VALUES (1, 20);
INSERT INTO `sys_role_menu` VALUES (1, 21);
INSERT INTO `sys_role_menu` VALUES (1, 22);
INSERT INTO `sys_role_menu` VALUES (1, 23);
INSERT INTO `sys_role_menu` VALUES (1, 24);
INSERT INTO `sys_role_menu` VALUES (1, 25);
INSERT INTO `sys_role_menu` VALUES (1, 26);
INSERT INTO `sys_role_menu` VALUES (1, 27);
INSERT INTO `sys_role_menu` VALUES (1, 28);
INSERT INTO `sys_role_menu` VALUES (1, 29);
INSERT INTO `sys_role_menu` VALUES (1, 30);
INSERT INTO `sys_role_menu` VALUES (1, 31);
INSERT INTO `sys_role_menu` VALUES (1, 32);
INSERT INTO `sys_role_menu` VALUES (1, 33);
INSERT INTO `sys_role_menu` VALUES (1, 34);
INSERT INTO `sys_role_menu` VALUES (1, 35);
INSERT INTO `sys_role_menu` VALUES (1, 36);
INSERT INTO `sys_role_menu` VALUES (1, 37);
INSERT INTO `sys_role_menu` VALUES (1, 38);
INSERT INTO `sys_role_menu` VALUES (1, 39);
INSERT INTO `sys_role_menu` VALUES (1, 40);
INSERT INTO `sys_role_menu` VALUES (1, 41);
INSERT INTO `sys_role_menu` VALUES (1, 42);
INSERT INTO `sys_role_menu` VALUES (1, 43);
INSERT INTO `sys_role_menu` VALUES (1, 44);
INSERT INTO `sys_role_menu` VALUES (1, 45);
INSERT INTO `sys_role_menu` VALUES (1, 46);
INSERT INTO `sys_role_menu` VALUES (1, 47);
INSERT INTO `sys_role_menu` VALUES (1, 48);
INSERT INTO `sys_role_menu` VALUES (1, 49);
INSERT INTO `sys_role_menu` VALUES (1, 50);
INSERT INTO `sys_role_menu` VALUES (1, 51);
INSERT INTO `sys_role_menu` VALUES (1, 52);
INSERT INTO `sys_role_menu` VALUES (1, 53);
INSERT INTO `sys_role_menu` VALUES (1, 54);
INSERT INTO `sys_role_menu` VALUES (1, 55);
INSERT INTO `sys_role_menu` VALUES (1, 56);
INSERT INTO `sys_role_menu` VALUES (1, 67);
INSERT INTO `sys_role_menu` VALUES (1, 68);
INSERT INTO `sys_role_menu` VALUES (1, 69);
INSERT INTO `sys_role_menu` VALUES (1, 70);
INSERT INTO `sys_role_menu` VALUES (1, 71);
INSERT INTO `sys_role_menu` VALUES (1, 72);
INSERT INTO `sys_role_menu` VALUES (1, 73);
INSERT INTO `sys_role_menu` VALUES (1, 74);
INSERT INTO `sys_role_menu` VALUES (1, 75);
INSERT INTO `sys_role_menu` VALUES (1, 76);
INSERT INTO `sys_role_menu` VALUES (1, 77);
INSERT INTO `sys_role_menu` VALUES (1, 78);
INSERT INTO `sys_role_menu` VALUES (1, 79);
INSERT INTO `sys_role_menu` VALUES (1, 80);
INSERT INTO `sys_role_menu` VALUES (1, 81);
INSERT INTO `sys_role_menu` VALUES (1, 82);
INSERT INTO `sys_role_menu` VALUES (1, 83);
INSERT INTO `sys_role_menu` VALUES (1, 84);
INSERT INTO `sys_role_menu` VALUES (1, 85);
INSERT INTO `sys_role_menu` VALUES (1, 86);
INSERT INTO `sys_role_menu` VALUES (1, 87);
INSERT INTO `sys_role_menu` VALUES (1, 88);
INSERT INTO `sys_role_menu` VALUES (1, 89);
INSERT INTO `sys_role_menu` VALUES (1, 90);
INSERT INTO `sys_role_menu` VALUES (1, 91);
INSERT INTO `sys_role_menu` VALUES (1, 92);
INSERT INTO `sys_role_menu` VALUES (1, 93);
INSERT INTO `sys_role_menu` VALUES (1, 94);
INSERT INTO `sys_role_menu` VALUES (1, 95);
INSERT INTO `sys_role_menu` VALUES (1, 96);
INSERT INTO `sys_role_menu` VALUES (1, 97);
INSERT INTO `sys_role_menu` VALUES (1, 98);
INSERT INTO `sys_role_menu` VALUES (1, 99);
INSERT INTO `sys_role_menu` VALUES (1, 100);
INSERT INTO `sys_role_menu` VALUES (1, 101);
INSERT INTO `sys_role_menu` VALUES (1, 102);
INSERT INTO `sys_role_menu` VALUES (1, 103);
INSERT INTO `sys_role_menu` VALUES (1, 104);
INSERT INTO `sys_role_menu` VALUES (1, 105);
INSERT INTO `sys_role_menu` VALUES (1, 106);
INSERT INTO `sys_role_menu` VALUES (1, 107);
INSERT INTO `sys_role_menu` VALUES (1, 108);
INSERT INTO `sys_role_menu` VALUES (1, 109);
INSERT INTO `sys_role_menu` VALUES (1, 110);
INSERT INTO `sys_role_menu` VALUES (1, 111);
INSERT INTO `sys_role_menu` VALUES (1, 112);
INSERT INTO `sys_role_menu` VALUES (1, 113);
INSERT INTO `sys_role_menu` VALUES (1, 114);
INSERT INTO `sys_role_menu` VALUES (1, 115);
INSERT INTO `sys_role_menu` VALUES (1, 116);
INSERT INTO `sys_role_menu` VALUES (1, 117);
INSERT INTO `sys_role_menu` VALUES (1, 118);
INSERT INTO `sys_role_menu` VALUES (1, 119);
INSERT INTO `sys_role_menu` VALUES (1, 120);
INSERT INTO `sys_role_menu` VALUES (1, 121);
INSERT INTO `sys_role_menu` VALUES (1, 122);
INSERT INTO `sys_role_menu` VALUES (1, 123);
INSERT INTO `sys_role_menu` VALUES (1, 124);
INSERT INTO `sys_role_menu` VALUES (1, 125);
INSERT INTO `sys_role_menu` VALUES (1, 126);
INSERT INTO `sys_role_menu` VALUES (1, 127);
INSERT INTO `sys_role_menu` VALUES (1, 128);
INSERT INTO `sys_role_menu` VALUES (1, 129);
INSERT INTO `sys_role_menu` VALUES (1, 130);
INSERT INTO `sys_role_menu` VALUES (1, 131);
INSERT INTO `sys_role_menu` VALUES (1, 135);
INSERT INTO `sys_role_menu` VALUES (1, 136);
INSERT INTO `sys_role_menu` VALUES (1, 137);
INSERT INTO `sys_role_menu` VALUES (1, 138);
INSERT INTO `sys_role_menu` VALUES (1, 139);
INSERT INTO `sys_role_menu` VALUES (1, 140);
INSERT INTO `sys_role_menu` VALUES (1, 141);
INSERT INTO `sys_role_menu` VALUES (1, 142);
INSERT INTO `sys_role_menu` VALUES (1, 143);
INSERT INTO `sys_role_menu` VALUES (1, 144);
INSERT INTO `sys_role_menu` VALUES (1, 145);
INSERT INTO `sys_role_menu` VALUES (1, 146);
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 5);
INSERT INTO `sys_role_menu` VALUES (2, 6);
INSERT INTO `sys_role_menu` VALUES (2, 7);
INSERT INTO `sys_role_menu` VALUES (2, 11);
INSERT INTO `sys_role_menu` VALUES (2, 12);
INSERT INTO `sys_role_menu` VALUES (2, 13);
INSERT INTO `sys_role_menu` VALUES (2, 14);
INSERT INTO `sys_role_menu` VALUES (2, 15);
INSERT INTO `sys_role_menu` VALUES (2, 16);
INSERT INTO `sys_role_menu` VALUES (2, 17);
INSERT INTO `sys_role_menu` VALUES (2, 18);
INSERT INTO `sys_role_menu` VALUES (2, 19);
INSERT INTO `sys_role_menu` VALUES (2, 20);
INSERT INTO `sys_role_menu` VALUES (2, 21);
INSERT INTO `sys_role_menu` VALUES (2, 22);
INSERT INTO `sys_role_menu` VALUES (2, 23);
INSERT INTO `sys_role_menu` VALUES (2, 24);
INSERT INTO `sys_role_menu` VALUES (2, 25);
INSERT INTO `sys_role_menu` VALUES (2, 26);
INSERT INTO `sys_role_menu` VALUES (2, 27);
INSERT INTO `sys_role_menu` VALUES (2, 28);
INSERT INTO `sys_role_menu` VALUES (2, 29);
INSERT INTO `sys_role_menu` VALUES (2, 30);
INSERT INTO `sys_role_menu` VALUES (2, 31);
INSERT INTO `sys_role_menu` VALUES (2, 32);
INSERT INTO `sys_role_menu` VALUES (2, 33);
INSERT INTO `sys_role_menu` VALUES (2, 34);
INSERT INTO `sys_role_menu` VALUES (2, 35);
INSERT INTO `sys_role_menu` VALUES (2, 36);
INSERT INTO `sys_role_menu` VALUES (2, 37);
INSERT INTO `sys_role_menu` VALUES (2, 38);
INSERT INTO `sys_role_menu` VALUES (2, 39);
INSERT INTO `sys_role_menu` VALUES (2, 40);
INSERT INTO `sys_role_menu` VALUES (2, 41);
INSERT INTO `sys_role_menu` VALUES (2, 42);
INSERT INTO `sys_role_menu` VALUES (2, 43);
INSERT INTO `sys_role_menu` VALUES (2, 44);
INSERT INTO `sys_role_menu` VALUES (2, 55);
INSERT INTO `sys_role_menu` VALUES (2, 56);
INSERT INTO `sys_role_menu` VALUES (2, 67);
INSERT INTO `sys_role_menu` VALUES (2, 68);
INSERT INTO `sys_role_menu` VALUES (2, 119);
INSERT INTO `sys_role_menu` VALUES (2, 120);
INSERT INTO `sys_role_menu` VALUES (2, 121);
INSERT INTO `sys_role_menu` VALUES (2, 122);
INSERT INTO `sys_role_menu` VALUES (2, 123);
INSERT INTO `sys_role_menu` VALUES (2, 124);
INSERT INTO `sys_role_menu` VALUES (2, 125);
INSERT INTO `sys_role_menu` VALUES (2, 126);
INSERT INTO `sys_role_menu` VALUES (2, 127);
INSERT INTO `sys_role_menu` VALUES (2, 128);
INSERT INTO `sys_role_menu` VALUES (2, 129);
INSERT INTO `sys_role_menu` VALUES (2, 130);
INSERT INTO `sys_role_menu` VALUES (2, 131);
INSERT INTO `sys_role_menu` VALUES (2, 136);
INSERT INTO `sys_role_menu` VALUES (2, 137);
INSERT INTO `sys_role_menu` VALUES (2, 138);
INSERT INTO `sys_role_menu` VALUES (2, 139);
INSERT INTO `sys_role_menu` VALUES (2, 140);
INSERT INTO `sys_role_menu` VALUES (2, 141);
INSERT INTO `sys_role_menu` VALUES (2, 142);
INSERT INTO `sys_role_menu` VALUES (2, 143);
INSERT INTO `sys_role_menu` VALUES (2, 144);
INSERT INTO `sys_role_menu` VALUES (2, 145);
INSERT INTO `sys_role_menu` VALUES (2, 146);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `del_time` datetime NULL DEFAULT NULL COMMENT '删除时间-唯一索引，由于用户名为唯一索引，逻辑删除后再建相同数据时会失败',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_username`(`user_name`, `del_time`) USING BTREE COMMENT '用户名唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 1, 'young', 'young', '', '15538079781', '0', '', '$2a$10$IEwCVaD7K1lGFmKrkdidbuhxPFUV6MpRbCtFCEMcrDBCZfRwMBtPu', '0', '0', '', NULL, 'yqz', '2022-08-07 14:45:17', 'yqz', '2022-08-07 14:45:17', NULL, '2022-11-09 21:50:21');
INSERT INTO `sys_user` VALUES (2, 2, 'yqz', 'yqz_部门主管', '', '15738398298', '0', '', '$2a$10$y70e.to8NYLluna8a4ixVu7bxxGjklRDuRuF7WTaNCHO7GPRUrAC2', '0', '0', '', NULL, 'youngAdmin', '2022-08-22 22:32:03', 'youngAdmin', '2023-05-11 10:37:26', NULL, '2022-11-09 21:50:24');
INSERT INTO `sys_user` VALUES (3, 1, 'neo', 'neo_总经理', '', '13800000000', '0', '', '$2a$10$9cOeavdKyONgz8za6xP6M.6cQAikH8BBmPAjyv3JapuL1vxKAWr/q', '0', '0', '', NULL, 'youngAdmin', '2022-08-22 22:36:35', 'youngAdmin', '2023-05-11 10:37:01', NULL, '2022-11-09 21:50:28');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 3);
INSERT INTO `sys_user_post` VALUES (3, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 1);
INSERT INTO `sys_user_role` VALUES (3, 1);

SET FOREIGN_KEY_CHECKS = 1;
