package com.neo.tiny.codegen.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.codegen.entity.GenDatasourceConf;
import com.neo.tiny.codegen.service.GenDatasourceConfService;
import com.neo.tiny.common.common.ResResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author yqz
 * @Description
 * @CreateDate 2022/8/4 12:24
 */
@Api(tags = "数据源管理模块")
@RestController
@RequiredArgsConstructor
@RequestMapping("/gen/dsconf")
public class GenDsConfController {

    private final GenDatasourceConfService datasourceConfService;

    /**
     * 分页查询
     *
     * @param page           分页对象
     * @param datasourceConf 数据源表
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<IPage<GenDatasourceConf>> getSysDatasourceConfPage(Page page, GenDatasourceConf datasourceConf) {
        return ResResult.success(datasourceConfService.page(page, Wrappers.query(datasourceConf)));
    }

    /**
     * 查询全部数据源
     *
     * @return
     */
    @ApiOperation("查询全部数据源")
    @GetMapping("/list")
    public ResResult<List<GenDatasourceConf>> list() {
        return ResResult.success((datasourceConfService.list()));
    }

    /**
     * 通过id查询数据源表
     *
     * @param id id
     * @return R
     */
    @ApiOperation("通过id查询数据源表")
    @GetMapping("/{id}")
    public ResResult<GenDatasourceConf> getById(@PathVariable("id") Integer id) {
        return ResResult.success((datasourceConfService.getById(id)));
    }

    /**
     * 新增数据源表
     *
     * @param datasourceConf 数据源表
     * @return R
     */
    @ApiOperation("新增数据源表")
    @PostMapping
    public ResResult<Boolean> save(@RequestBody GenDatasourceConf datasourceConf) {
        return ResResult.success((datasourceConfService.saveDsByEnc(datasourceConf)));
    }

    /**
     * 修改数据源表
     *
     * @param conf 数据源表
     * @return R
     */
    @ApiOperation("修改数据源表")
    @PutMapping
    public ResResult<Boolean> updateById(@RequestBody GenDatasourceConf conf) {
        return ResResult.success((datasourceConfService.updateDsByEnc(conf)));
    }

    /**
     * 通过id删除数据源表
     *
     * @param id id
     * @return R
     */
    @ApiOperation("通过id删除数据源表")
    @DeleteMapping("/{id}")
    public ResResult<Boolean> removeById(@PathVariable Long id) {
        return ResResult.success((datasourceConfService.removeByDsId(id)));
    }

}
