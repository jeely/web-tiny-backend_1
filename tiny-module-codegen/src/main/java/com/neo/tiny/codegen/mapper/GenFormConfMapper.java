package com.neo.tiny.codegen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.codegen.entity.GenFormConf;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yqz
 * @Description 生成记录
 * @CreateDate 2022/8/4 12:36
 */
@Mapper
public interface GenFormConfMapper extends BaseMapper<GenFormConf> {

}
