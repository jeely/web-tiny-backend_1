package com.neo.tiny.codegen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yqz
 * @Description 生成记录
 * @CreateDate 2022/8/4 12:31
 */
@Data
@TableName("gen_form_conf")
@ApiModel(description = "生成记录")
@EqualsAndHashCode(callSuper = true)
public class GenFormConf extends BaseDO {

    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("ID")
    private Long id;

    /**
     * 表名称
     */
    @ApiModelProperty("表名称")
    private String tableName;

    /**
     * 表单信息
     */
    @ApiModelProperty("表单信息")
    private String formInfo;

    /**
     * 删除标记
     */
    @ApiModelProperty("删除标记")
    private String delFlag;

}
