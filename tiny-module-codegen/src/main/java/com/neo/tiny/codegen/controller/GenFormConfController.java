package com.neo.tiny.codegen.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neo.tiny.codegen.entity.GenFormConf;
import com.neo.tiny.codegen.service.GenFormConfService;
import com.neo.tiny.common.common.ResResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/6 19:09
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/gen/form")
@Api(tags = "表单管理")
public class GenFormConfController {

    private final GenFormConfService genRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param formConf 生成记录
     * @return
     */
    @ApiOperation( "分页查询")
    @GetMapping("/page")
    public ResResult<IPage<GenFormConf>> getGenFormConfPage(Page page, GenFormConf formConf) {
        return ResResult.success(genRecordService.page(page, Wrappers.query(formConf)));
    }

    /**
     * 通过id查询生成记录
     * @param id id
     * @return R
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{id}")
    public ResResult<GenFormConf> getById(@PathVariable("id") Integer id) {
        return ResResult.success(genRecordService.getById(id));
    }

    /**
     * 通过id查询生成记录
     * @param dsName 数据源ID
     * @param tableName tableName
     * @return R
     */
    @ApiOperation("通过tableName查询表单信息")
    @GetMapping("/info")
    public ResResult<String> form(String dsName, String tableName) {
        return ResResult.success(genRecordService.getForm(dsName, tableName));
    }

    /**
     * 新增生成记录
     * @param formConf 生成记录
     * @return R
     */
    @ApiOperation("新增生成记录")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('gen_form_add')")
    public ResResult<Boolean> save(@RequestBody GenFormConf formConf) {
        return ResResult.success(genRecordService.save(formConf));
    }

    /**
     * 通过id删除生成记录
     * @param id id
     * @return R
     */
    @ApiOperation("通过id删除生成记录")
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('gen_form_del')")
    public ResResult<Boolean> removeById(@PathVariable Long id) {
        return ResResult.success(genRecordService.removeById(id));
    }

}
