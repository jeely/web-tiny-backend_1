package com.neo.tiny.codegen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.codegen.entity.GenFormConf;

/**
 * @author yqz
 * @Description 表单管理
 * @CreateDate 2022/8/4 12:38
 */
public interface GenFormConfService extends IService<GenFormConf> {

    /**
     * 获取表单信息
     * @param dsName 数据源ID
     * @param tableName 表名称
     * @return
     */
    String getForm(String dsName, String tableName);

}
