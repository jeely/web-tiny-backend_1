package com.neo.tiny.business.iot.vo;

import lombok.Data;

/**
 * @Description: 设备信息更新请求
 * @Author: yqz
 * @CreateDate: 2023/2/4 20:58
 */
@Data
public class IotDeviceInfoReq {

    /**
     * 客户端的唯一id
     */
    private String deviceId;

    /**
     * 客户端ip
     */
    private String deviceIp;

    /**
     * 设备mac地址
     */
    private String deviceMac;

    /**
     * 设备led状态：on || off
     */
    private String ledStatus;
}
