package com.neo.tiny.business.iot.enums;

import java.util.Arrays;

/**
 * @Description:
 * @Author: yqz 继电器开关
 * @CreateDate: 2023/1/16 22:42
 */
public enum RelaySwitchStatus {
    /**
     * 开
     */
    ON("on", "1"),
    /**
     * 关
     */
    OFF("off", "0");

    RelaySwitchStatus(String action, String status) {
        this.action = action;
        this.status = status;
    }

    private final String action;

    private final String status;

    public String getStatus() {
        return status;
    }

    public String getAction() {
        return action;
    }


    /**
     * 根据状态值获取动作
     *
     * @param status 状态：0—on。1-off
     * @return 动作
     */
    public static String matchAction(String status) {
        return Arrays.stream(values()).filter(item -> item.getStatus().equals(status))
                .findFirst()
                .map(RelaySwitchStatus::getAction).orElse(null);
    }

}
