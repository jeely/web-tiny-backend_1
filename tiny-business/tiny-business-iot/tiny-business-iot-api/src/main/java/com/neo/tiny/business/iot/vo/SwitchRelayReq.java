package com.neo.tiny.business.iot.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Description: 继电器开关入参
 * @Author: yqz
 * @CreateDate: 2023/1/16 22:32
 */
@Data
public class SwitchRelayReq implements Serializable {

    @ApiModelProperty("设备ID")
    @NotBlank(message = "设备id不能为空")
    private String deviceId;

    @ApiModelProperty("动作：on-开，off-关")
    @NotBlank(message = "动作不能为空")
    private String action;
}
