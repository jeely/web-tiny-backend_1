package com.neo.tiny.business.iot.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * @author yqzBpmFormUpdateReqVO.java
 * @Description: IoT设备管理更新请求类
 * @date 2023-01-15 02:01:47
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "IoT设备管理")
public class BusinessIotDeviceUpdateReqVO extends BusinessIotDeviceBaseVO implements Serializable {

}
