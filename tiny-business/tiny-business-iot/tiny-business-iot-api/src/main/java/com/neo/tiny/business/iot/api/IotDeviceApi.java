package com.neo.tiny.business.iot.api;

import com.neo.tiny.business.iot.vo.IotDeviceInfoReq;

/**
 * @Description: IoT设备接口
 * @Author: yqz
 * @CreateDate: 2023/1/15 21:08
 */
public interface IotDeviceApi {

    /**
     * 添加设备
     *
     * @param deviceId 设备id
     */
    void addDevice(String deviceId);


    /**
     * 更新设备信息
     *
     * @param req 设备信息参数
     */
    void updateDeviceInfo(IotDeviceInfoReq req);
}
