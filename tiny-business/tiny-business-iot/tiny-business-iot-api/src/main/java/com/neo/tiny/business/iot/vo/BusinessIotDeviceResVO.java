package com.neo.tiny.business.iot.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @author yqz
 * @Description: IoT设备管理响应类
 * @date 2023-01-15 02:01:47
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "IoT设备管理")
public class BusinessIotDeviceResVO extends BusinessIotDeviceBaseVO implements Serializable {

    @ApiModelProperty("在线状态：0-不在线，1-在线")
    private Integer onlineStatus;

    @ApiModelProperty("开关状态：on-开，off-关")
    private String switchStatus;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("最后更新时间")
    private LocalDateTime updateTime;

    /**
     * 设备上线时间
     */
    @ApiModelProperty("设备上线时间")
    private LocalDateTime onlineTime;


}
