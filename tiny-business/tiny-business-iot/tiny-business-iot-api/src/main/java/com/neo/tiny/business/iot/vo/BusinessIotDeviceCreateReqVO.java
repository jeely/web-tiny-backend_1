package com.neo.tiny.business.iot.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * @author yqz
 * @Description: 创建IoT设备管理
 * @date 2023-01-15 02:01:47
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "IoT设备管理")
public class BusinessIotDeviceCreateReqVO extends BusinessIotDeviceBaseVO implements Serializable {


}
