package com.neo.tiny.business.iot.vo;

import com.neo.tiny.common.common.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author yqz
 * @Description: IoT设备管理分页请求类
 * @date 2023-01-15 02:01:47
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "IoT设备管理")
public class BusinessIotDevicePageReqVO extends PageParam implements Serializable {
    /**
     * 设备名称
     */
    @ApiModelProperty("设备名称")
    private String deviceName;

}
