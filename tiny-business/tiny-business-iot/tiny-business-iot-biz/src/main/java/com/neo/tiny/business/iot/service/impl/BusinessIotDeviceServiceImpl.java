
package com.neo.tiny.business.iot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.business.iot.entity.BusinessIotDeviceDO;
import com.neo.tiny.business.iot.mapper.BusinessIotDeviceMapper;
import com.neo.tiny.business.iot.service.BusinessIotDeviceService;
import org.springframework.stereotype.Service;

/**
 * @Description: IoT设备管理
 * @Author: yqz
 * @CreateDate: 2023-01-15 01:40:46
 */
@Service
public class BusinessIotDeviceServiceImpl extends ServiceImpl<BusinessIotDeviceMapper, BusinessIotDeviceDO> implements BusinessIotDeviceService {

}
