
package com.neo.tiny.business.iot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.neo.tiny.data.BaseDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @Description: IoT设备管理
 * @Author: yqz
 * @CreateDate: 2023-01-15 01:40:46
 */
@Data
@TableName("business_iot_device")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "IoT设备管理")
public class BusinessIotDeviceDO extends BaseDO {

    /**
     * 自增主键
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("自增主键")
    private Long id;

    /**
     * 设备id
     */
    @ApiModelProperty("设备id")
    private String deviceId;

    /**
     * 设备mac地址
     */
    @ApiModelProperty("设备mac地址")
    private String deviceMac;

    /**
     * 设备名称
     */
    @ApiModelProperty("设备名称")
    private String deviceName;

    /**
     * 客户端ip
     */
    @ApiModelProperty("客户端ip")
    private String deviceIp;

    /**
     * 客户端主机名
     */
    @ApiModelProperty("客户端主机名")
    private String deviceHost;

    /**
     * 设备接入状态，设备第一次连接服务器时为未接入，需要手动确认后方可接入：0-未接入，1-已接入
     */
    @ApiModelProperty("设备接入状态，设备第一次连接服务器时为未接入，需要手动确认后方可接入：0-未接入，1-已接入")
    private Integer accessStatus;

    /**
     * 设备接入时间
     */
    @ApiModelProperty("设备接入时间")
    private LocalDateTime accessDate;

    /**
     * 设备上线时间
     */
    @ApiModelProperty("设备上线时间")
    private LocalDateTime onlineTime;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer sort;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


}
