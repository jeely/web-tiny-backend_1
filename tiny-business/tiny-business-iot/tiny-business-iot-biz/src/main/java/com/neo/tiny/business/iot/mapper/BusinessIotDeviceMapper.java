package com.neo.tiny.business.iot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.business.iot.entity.BusinessIotDeviceDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: IoT设备管理
 * @Author: yqz
 * @CreateDate: 2023-01-15 01:40:46
 */
@Mapper
public interface BusinessIotDeviceMapper extends BaseMapper<BusinessIotDeviceDO> {

}
