
package com.neo.tiny.business.iot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.business.iot.entity.BusinessIotDeviceDO;

/**
 * @Description: IoT设备管理
 * @Author: yqz
 * @CreateDate: 2023-01-15 01:40:46
 */
public interface BusinessIotDeviceService extends IService<BusinessIotDeviceDO> {

}
