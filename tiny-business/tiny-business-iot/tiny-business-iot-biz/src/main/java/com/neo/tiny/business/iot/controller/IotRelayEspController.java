package com.neo.tiny.business.iot.controller;

import com.neo.tiny.business.iot.vo.SwitchRelayReq;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.middle.socket.api.SocketSessionClient;
import com.neo.tiny.middle.socket.enums.WebSocketType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description: ESP8266-控制器
 * @Author: yqz
 * @CreateDate: 2023/1/16 22:30
 */
@RestController
@AllArgsConstructor
@RequestMapping("/iot/esp")
@Api(tags = "IoT-esp设备管理管理")
public class IotRelayEspController {

    private final List<SocketSessionClient<String>> socketSessionClients;

    @ApiOperation("继电器开关")
    @PostMapping("/switch")
    public ResResult<Boolean> switchRelay(@Valid @RequestBody SwitchRelayReq req) {
        SocketSessionClient<String> socketSessionClient = socketSessionClients
                .stream()
                .filter(client -> client.support(WebSocketType.IOT.getType()))
                .findFirst().get();
        socketSessionClient.sendMsg(req.getDeviceId(), req.getAction());

        return ResResult.success(Boolean.TRUE);
    }
}
