package com.neo.tiny.business.queue.vo.queue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 *
 * @Description: 队列更新请求类
 * @author yqzBpmFormUpdateReqVO.java
 * @date 2022-10-27 22:48:54
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "队列")
public class QueueUpdateReqVO extends QueueBaseVO implements Serializable {


    /**
     * 主键自增
     */
    @ApiModelProperty("主键自增")
    private Long queueId;

    /**
     * 队列标识
     */
    @ApiModelProperty("队列标识")
    private String queueKey;

    /**
     * 队列名称
     */
    @ApiModelProperty("队列名称")
    private String queueName;

    /**
     * 队列所在区域（专区）
     */
    @ApiModelProperty("队列所在区域（专区）")
    private String queueArea;

    /**
     * 队列描述
     */
    @ApiModelProperty("队列描述")
    private String queueDescribe;

    /**
     * 日最大取号量限制
     */
    @ApiModelProperty("日最大取号量限制")
    private Integer ticketMaxLimit;


}
