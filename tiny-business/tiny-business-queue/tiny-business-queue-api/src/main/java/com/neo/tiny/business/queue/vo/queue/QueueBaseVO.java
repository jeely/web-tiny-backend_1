package com.neo.tiny.business.queue.vo.queue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 *
 * @Description: 队列基础类
 * @author yqz
 * @date 2022-10-27 22:48:54
 */
@Data
@ApiModel(description = "队列")
public class QueueBaseVO implements Serializable {



    /**
     * 队列标识
     */
    @ApiModelProperty("队列标识")
    private String queueKey;

    /**
     * 队列名称
     */
    @ApiModelProperty("队列名称")
    private String queueName;

    /**
     * 队列所在区域（专区）
     */
    @ApiModelProperty("队列所在区域（专区）")
    private String queueArea;

    /**
     * 队列描述
     */
    @ApiModelProperty("队列描述")
    private String queueDescribe;

    /**
     * 队列状态：1-激活，2-挂起
     */
    @ApiModelProperty("队列状态：1-激活，2-挂起")
    private Integer queueState;

    /**
     * 日最大取号量限制
     */
    @ApiModelProperty("日最大取号量限制")
    private Integer ticketMaxLimit;

    /**
     * 排序：正序
     */
    @ApiModelProperty("排序：正序")
    private Integer sortOrder;
}
