package com.neo.tiny.business.queue.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 *
 * @Description: 取叫号业务流水更新请求类
 * @author yqzBpmFormUpdateReqVO.java
 * @date 2022-10-27 22:10:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "取叫号业务流水")
public class TicketOrderUpdateReqVO extends TicketOrderBaseVO implements Serializable {

    /**
     * 自增主键
     */
    @ApiModelProperty("自增主键")
    private Long orderId;

    /**
     * 业务编码（交易流水号）
     */
    @ApiModelProperty("业务编码（交易流水号）")
    private String businessId;

    /**
     * 票号
     */
    @ApiModelProperty("票号")
    private String ticketNo;

    /**
     * 窗口id
     */
    @ApiModelProperty("窗口id")
    private Long windowId;

    /**
     * 窗口名（缴话费）
     */
    @ApiModelProperty("窗口名（缴话费）")
    private String windowName;

    /**
     * 窗口号（A01）
     */
    @ApiModelProperty("窗口号（A01）")
    private String windowNo;

    /**
     * 队列主键
     */
    @ApiModelProperty("队列主键")
    private Long queueId;

    /**
     * 队列标识
     */
    @ApiModelProperty("队列标识")
    private String queueKey;

    /**
     * 队列名称
     */
    @ApiModelProperty("队列名称")
    private String queueName;

    /**
     * 取号状态：0-预约，1-取号，2-叫号，3-离开，4-过号
     */
    @ApiModelProperty("取号状态：0-预约，1-取号，2-叫号，3-离开，4-过号")
    private Integer ticketStatus;

    /**
     * 取号时间
     */
    @ApiModelProperty("取号时间")
    private LocalDateTime ticketPrintTime;

    /**
     * 叫号时间
     */
    @ApiModelProperty("叫号时间")
    private LocalDateTime ticketCallTime;

    /**
     * 离开时间
     */
    @ApiModelProperty("离开时间")
    private LocalDateTime leaveTime;

    /**
     * 过号时间
     */
    @ApiModelProperty("过号时间")
    private LocalDateTime passTime;


}
