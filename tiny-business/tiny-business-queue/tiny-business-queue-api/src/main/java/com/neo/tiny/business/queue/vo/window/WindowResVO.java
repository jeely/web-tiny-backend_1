package com.neo.tiny.business.queue.vo.window;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @author yqz
 * @Description: 窗口响应类
 * @date 2022-10-27 22:22:54
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "窗口")
public class WindowResVO extends WindowBaseVO implements Serializable {

    /**
     * 窗口id（自增）
     */
    @ApiModelProperty("窗口id（自增）")
    private Long windowId;


    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("最后更新时间")
    private LocalDateTime updateTime;

}
