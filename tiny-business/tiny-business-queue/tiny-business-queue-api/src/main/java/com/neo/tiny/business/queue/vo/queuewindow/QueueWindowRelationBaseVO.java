package com.neo.tiny.business.queue.vo.queuewindow;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 *
 * @Description: 队列与窗口基础类
 * @author yqz
 * @date 2022-10-27 22:47:23
 */
@Data
@ApiModel(description = "队列与窗口")
public class QueueWindowRelationBaseVO implements Serializable {



    /**
     * 队列id
     */
    @ApiModelProperty("队列id")
    private Long queueId;

    /**
     * 窗口id
     */
    @ApiModelProperty("窗口id")
    private Long windowId;


}
