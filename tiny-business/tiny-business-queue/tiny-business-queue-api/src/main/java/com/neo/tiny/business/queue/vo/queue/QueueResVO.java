package com.neo.tiny.business.queue.vo.queue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @author yqz
 * @Description: 队列响应类
 * @date 2022-10-27 22:48:54
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "队列")
public class QueueResVO extends QueueBaseVO implements Serializable {

    /**
     * 主键自增
     */
    @ApiModelProperty("主键自增")
    private Long queueId;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("最后更新时间")
    private LocalDateTime updateTime;


}
