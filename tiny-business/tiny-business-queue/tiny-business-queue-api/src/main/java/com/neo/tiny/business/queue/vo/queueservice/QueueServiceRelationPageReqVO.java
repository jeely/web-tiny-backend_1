package com.neo.tiny.business.queue.vo.queueservice;

import com.neo.tiny.common.common.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * @Description: 队列与办理项分页请求类
 * @author yqz
 * @date 2022-10-27 22:41:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "队列与办理项")
public class QueueServiceRelationPageReqVO extends PageParam implements Serializable {



    /**
     * 队列id
     */
    @ApiModelProperty("队列id")
    private Long queueId;

    /**
     * 业务办理项id
     */
    @ApiModelProperty("业务办理项id")
    private String serviceId;


}
