package com.neo.tiny.business.queue.vo.ticketmachine;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;


/**
 * @author yqz
 * @Description: 创建队列与取号机
 * @date 2022-10-27 22:39:51
 */
@Data
@ApiModel(description = "队列与取号机")
public class QueueTicketMachineRelationCreateReqVO implements Serializable {

    /**
     * 取号机id
     */
    @NotNull(message = "取号机id不能为空")
    @ApiModelProperty("取号机id")
    private Long machineId;

    /**
     * 队列id集合
     */
    @NotNull(message = "队列id集合不能为空")
    @ApiModelProperty("队列id集合")
    private List<Long> queueIds;
}
