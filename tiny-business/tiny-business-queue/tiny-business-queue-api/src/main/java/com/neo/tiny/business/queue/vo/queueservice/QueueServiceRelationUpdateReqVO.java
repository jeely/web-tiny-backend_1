package com.neo.tiny.business.queue.vo.queueservice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 *
 * @Description: 队列与办理项更新请求类
 * @author yqzBpmFormUpdateReqVO.java
 * @date 2022-10-27 22:41:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "队列与办理项")
public class QueueServiceRelationUpdateReqVO extends QueueServiceRelationBaseVO implements Serializable {

    /**
     * 自增主键
     */
    @ApiModelProperty("自增主键")
    private Long relationId;

    /**
     * 队列id
     */
    @ApiModelProperty("队列id")
    private Long queueId;

    /**
     * 业务办理项id
     */
    @ApiModelProperty("业务办理项id")
    private String serviceId;


}
