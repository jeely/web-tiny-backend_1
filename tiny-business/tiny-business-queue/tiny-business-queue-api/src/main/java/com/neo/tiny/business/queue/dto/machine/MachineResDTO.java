package com.neo.tiny.business.queue.dto.machine;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description: 取号机
 * @Author: yqz
 * @CreateDate: 2022/10/29 23:35
 */
@Data
public class MachineResDTO implements Serializable {


    /**
     * 取号机id（自增）
     */
    @ApiModelProperty("取号机id（自增）")
    private Long ticketMachineId;

    /**
     * 取号机名称
     */
    @ApiModelProperty("取号机名称")
    private String ticketMachineName;

    /**
     * 取号机编码
     */
    @ApiModelProperty("取号机编码")
    private String ticketMachineKey;

    /**
     * 取号机所属业务区
     */
    @ApiModelProperty("取号机所属业务区")
    private String machineArea;

    /**
     * 取号机简介
     */
    @ApiModelProperty("取号机简介")
    private String machineDescribe;

    /**
     * 取号机物理地址
     */
    @ApiModelProperty("取号机物理地址")
    private String macAddress;


    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("最后更新时间")
    private LocalDateTime updateTime;

}
