package com.neo.tiny.business.queue.vo.window;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 *
 * @Description: 创建窗口
 * @author yqz
 * @date 2022-10-27 22:22:54
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "窗口")
public class WindowCreateReqVO extends WindowBaseVO implements Serializable {



    /**
     * 窗口名（缴话费）
     */
    @ApiModelProperty("窗口名（缴话费）")
    private String windowName;

    /**
     * 窗口号（A01）
     */
    @ApiModelProperty("窗口号（A01）")
    private String windowNo;

    /**
     * 窗口编码
     */
    @ApiModelProperty("窗口编码")
    private String windowKey;

    /**
     * 窗口状态（0-空闲，1-受理中，2-暂停服务）
     */
    @ApiModelProperty("窗口状态（0-空闲，1-受理中，2-暂停服务）")
    private Integer windowStatus;


}
