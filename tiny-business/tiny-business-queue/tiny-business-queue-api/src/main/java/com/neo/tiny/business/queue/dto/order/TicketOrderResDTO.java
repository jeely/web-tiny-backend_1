package com.neo.tiny.business.queue.dto.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description: 取号结果集
 * @Author: yqz
 * @CreateDate: 2022/10/30 00:02
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TicketOrderResDTO implements Serializable {


    /**
     * 业务编码（交易流水号）
     */
    @ApiModelProperty("业务编码（交易流水号）")
    private String businessId;

    /**
     * 票号
     */
    @ApiModelProperty("票号")
    private String ticketNo;

    // TODO 根据业务需要返回更多的属性

}
