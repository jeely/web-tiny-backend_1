package com.neo.tiny.business.queue.dto.queue;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description: 队列信息Dto
 * @Author: yqz
 * @CreateDate: 2022/10/29 23:45
 */
@Data
public class QueueInfoDTO implements Serializable {

    /**
     * 主键自增
     */
    @ApiModelProperty("主键自增")
    private Long queueId;

    /**
     * 队列标识
     */
    @ApiModelProperty("队列标识")
    private String queueKey;

    /**
     * 队列名称
     */
    @ApiModelProperty("队列名称")
    private String queueName;

    /**
     * 队列所在区域（专区）
     */
    @ApiModelProperty("队列所在区域（专区）")
    private String queueArea;

    /**
     * 队列描述
     */
    @ApiModelProperty("队列描述")
    private String queueDescribe;

    /**
     * 队列状态：1-激活，2-挂起
     */
    @ApiModelProperty("队列状态：1-激活，2-挂起")
    private Integer queueState;

    /**
     * 日最大取号量限制
     */
    @ApiModelProperty("日最大取号量限制")
    private Integer ticketMaxLimit;

    /**
     * 排序：正序
     */
    @ApiModelProperty("排序：正序")
    private Integer sortOrder;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("最后更新时间")
    private LocalDateTime updateTime;
}
