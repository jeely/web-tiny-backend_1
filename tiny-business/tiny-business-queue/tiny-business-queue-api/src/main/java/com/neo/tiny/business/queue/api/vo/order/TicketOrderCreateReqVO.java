package com.neo.tiny.business.queue.api.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


/**
 * @author yqz
 * @Description: 创建取叫号业务流水
 * @date 2022-10-27 22:10:16
 */
@Data
@ApiModel(description = "取叫号业务流水")
public class TicketOrderCreateReqVO implements Serializable {

    @ApiModelProperty("取号机id")
    @NotEmpty(message = "取号机id不能为空")
    private String ticketMachineId;


    @ApiModelProperty("队列id")
    @NotEmpty(message = "队列id不能为空")
    private String queueId;

    /**
     * 客户姓名
     */
    @ApiModelProperty("客户姓名")
    private String customerName;

    /**
     * 客户证件号码
     */
    @ApiModelProperty("客户证件号码")
    private String customerIdNo;

    /**
     * 客户联系方式
     */
    @ApiModelProperty("客户联系方式")
    private String customerPhone;

}
