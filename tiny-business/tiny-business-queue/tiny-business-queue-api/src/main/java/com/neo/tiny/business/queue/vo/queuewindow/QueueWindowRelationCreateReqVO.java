package com.neo.tiny.business.queue.vo.queuewindow;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;


/**
 * @author yqz
 * @Description: 创建队列与窗口
 * @date 2022-10-27 22:47:23
 */
@Data
@ApiModel(description = "队列与窗口")
public class QueueWindowRelationCreateReqVO implements Serializable {

    /**
     * 队列id
     */
    @NotNull(message = "队列id不能为空")
    @ApiModelProperty("队列id")
    private Long queueId;

    /**
     * 窗口id集合
     */
    @NotNull(message = "窗口id集合不能为空")
    @ApiModelProperty("窗口id集合")
    private List<Long> windowIds;


}
