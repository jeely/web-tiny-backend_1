package com.neo.tiny.business.queue.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 *
 * @Description: 取叫号业务流水响应类
 * @author yqz
 * @date 2022-10-27 22:10:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "取叫号业务流水")
public class TicketOrderResVO extends TicketOrderBaseVO implements Serializable {

    /**
     * 自增主键
     */
    @ApiModelProperty("自增主键")
    private Long orderId;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("最后更新时间")
    private LocalDateTime updateTime;


}
