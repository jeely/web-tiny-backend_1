package com.neo.tiny.business.queue.api;

import com.neo.tiny.business.queue.api.vo.call.ListTicketForCallRes;
import com.neo.tiny.business.queue.dto.order.TicketOrderReqDTO;
import com.neo.tiny.business.queue.dto.order.TicketOrderResDTO;

import java.util.Collection;
import java.util.List;

/**
 * @Description: 取号机产生取号订单api
 * @Author: yqz
 * @CreateDate: 2022/10/29 23:59
 */
public interface TicketOrderApi {

    /**
     * 创建取号
     *
     * @param req 取号入参
     * @return 取号结果
     */
    TicketOrderResDTO createTicketOrder(TicketOrderReqDTO req);

    /**
     * 根据取号机标识，生成票号
     * 使用取号机标识为前缀，redis自增函数生成数字，二者组合为票号
     *
     * @param ticketMachineKey 取号机标识
     * @return 票号
     */
    String generateTicketNo(String ticketMachineKey);


    /**
     * 根据窗口id，获取待叫号列表
     *
     * @param windowId 窗口id
     * @param status   {@link com.neo.tiny.business.queue.enums.TicketStatusEnum}
     * @return 获取待叫号列表
     */
    List<ListTicketForCallRes> listTicketForCallByWindowId(Long windowId, Collection<Integer> status);


    /**
     * 根据取号主键id，进行叫号
     *
     * @param orderId      取号票号主键id
     * @param windowId     窗口id
     * @param ticketStatus 状态 {@link com.neo.tiny.business.queue.enums.TicketStatusEnum}
     * @return 叫号结果
     */
    ListTicketForCallRes call(Long orderId, Long windowId, Integer ticketStatus);
}
