package com.neo.tiny.business.queue.vo.queuewindow;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 *
 * @Description: 队列与窗口响应类
 * @author yqz
 * @date 2022-10-27 22:47:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "队列与窗口")
public class QueueWindowRelationResVO extends QueueWindowRelationBaseVO implements Serializable {


    /**
     * 主键自增
     */
    @ApiModelProperty("主键自增")
    private Long relationId;
}
