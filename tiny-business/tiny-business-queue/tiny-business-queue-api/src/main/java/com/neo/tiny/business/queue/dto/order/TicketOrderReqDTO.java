package com.neo.tiny.business.queue.dto.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 取号请求
 * @Author: yqz
 * @CreateDate: 2022/10/30 00:00
 */
@Data
public class TicketOrderReqDTO implements Serializable {

    /**
     * 业务编码（交易流水号）
     */
    @ApiModelProperty("业务编码（交易流水号）")
    private String businessId;

    /**
     * 客户姓名
     */
    @ApiModelProperty("客户姓名")
    private String customerName;

    /**
     * 客户证件号码
     */
    @ApiModelProperty("客户证件号码")
    private String customerIdNo;

    /**
     * 客户联系方式
     */
    @ApiModelProperty("客户联系方式")
    private String customerPhone;

    /**
     * 队列主键
     */
    @ApiModelProperty("队列主键")
    private Long queueId;

    /**
     * 队列标识
     */
    @ApiModelProperty("队列标识")
    private String queueKey;


    @ApiModelProperty("取号机id")
    private Long ticketMachineId;


    /**
     * 取号机名称
     */
    @ApiModelProperty("取号机名称")
    private String ticketMachineName;

    /**
     * 取号机编码
     */
    @ApiModelProperty("取号机编码")
    private String ticketMachineKey;

}
