package com.neo.tiny.business.queue.enums;

import java.util.Arrays;

/**
 * @Description: 取号状态
 * @Author: yqz
 * @CreateDate: 2022/10/30 09:30
 */
public enum TicketStatusEnum {

    /**
     * 预约
     */
    APPOINTMENT(0, "预约"),
    /**
     * 取号
     */
    TICKET_NO(10, "取号"),
    /**
     * 叫号
     */
    CALL_TICKET(20, "叫号"),
    /**
     * 离开
     */
    LEAVE(30, "离开"),
    /**
     * 过号
     */
    PASS_TICKET(40, "过号");


    TicketStatusEnum(Integer status, String describe) {
        this.status = status;
        this.describe = describe;
    }

    private final Integer status;

    private final String describe;


    public Integer getStatus() {
        return status;
    }

    public String getDescribe() {
        return describe;
    }

    /**
     * 根据状态匹配枚举对象
     *
     * @param status 票号状态
     * @return
     */
    public static TicketStatusEnum match(Integer status) {
        return Arrays.stream(values())
                .filter(item -> item.getStatus().equals(status))
                .findFirst()
                .orElse(null);
    }

}
