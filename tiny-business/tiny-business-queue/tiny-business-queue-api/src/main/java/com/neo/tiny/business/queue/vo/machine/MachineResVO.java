package com.neo.tiny.business.queue.vo.machine;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 *
 * @Description: 取号机响应类
 * @author yqz
 * @date 2022-10-27 22:18:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "取号机")
public class MachineResVO extends MachineBaseVO implements Serializable {

    /**
     * 取号机id（自增）
     */
    @ApiModelProperty("取号机id（自增）")
    private Long ticketMachineId;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("最后更新时间")
    private LocalDateTime updateTime;


}
