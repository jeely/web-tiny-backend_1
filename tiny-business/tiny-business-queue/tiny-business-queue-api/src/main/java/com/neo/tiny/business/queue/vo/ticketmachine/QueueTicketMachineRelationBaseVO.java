package com.neo.tiny.business.queue.vo.ticketmachine;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 *
 * @Description: 队列与取号机基础类
 * @author yqz
 * @date 2022-10-27 22:39:51
 */
@Data
@ApiModel(description = "队列与取号机")
public class QueueTicketMachineRelationBaseVO implements Serializable {



    /**
     * 队列id
     */
    @ApiModelProperty("队列id")
    private Long queueId;

    /**
     * 取号机id
     */
    @ApiModelProperty("取号机id")
    private Long ticketMachineId;


}
