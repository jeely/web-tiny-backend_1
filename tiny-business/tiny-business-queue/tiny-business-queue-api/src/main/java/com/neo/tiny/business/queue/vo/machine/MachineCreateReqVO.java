package com.neo.tiny.business.queue.vo.machine;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 *
 * @Description: 创建取号机
 * @author yqz
 * @date 2022-10-27 22:18:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "取号机")
public class MachineCreateReqVO extends MachineBaseVO implements Serializable {


    /**
     * 取号机编码
     */
    @ApiModelProperty("取号机编码")
    private String ticketMachineKey;

    /**
     * 取号机所属业务区
     */
    @ApiModelProperty("取号机所属业务区")
    private String machineArea;

    /**
     * 取号机物理地址
     */
    @ApiModelProperty("取号机物理地址")
    private String macAddress;


}
