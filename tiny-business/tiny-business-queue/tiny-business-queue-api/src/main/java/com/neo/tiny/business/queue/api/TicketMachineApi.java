package com.neo.tiny.business.queue.api;

import com.neo.tiny.business.queue.dto.machine.MachineResDTO;
import com.neo.tiny.business.queue.dto.queue.QueueInfoDTO;

import java.util.List;

/**
 * @Description: 取号机对外提供接口
 * @Author: yqz
 * @CreateDate: 2022/10/29 23:30
 */
public interface TicketMachineApi {

    /**
     * 查询所有取号机
     *
     * @return 取号机列表
     */
    List<MachineResDTO> listAllMachine();

    /**
     * 根据取号机id获取队列列表
     *
     * @param machineId 取号机id
     * @return 队列列表
     */
    List<QueueInfoDTO> listQueueByMachineId(Long machineId);
}
