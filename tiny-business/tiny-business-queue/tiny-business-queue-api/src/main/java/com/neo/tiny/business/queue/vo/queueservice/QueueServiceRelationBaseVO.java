package com.neo.tiny.business.queue.vo.queueservice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 *
 * @Description: 队列与办理项基础类
 * @author yqz
 * @date 2022-10-27 22:41:34
 */
@Data
@ApiModel(description = "队列与办理项")
public class QueueServiceRelationBaseVO implements Serializable {



    /**
     * 队列id
     */
    @ApiModelProperty("队列id")
    private Long queueId;

    /**
     * 业务办理项id
     */
    @ApiModelProperty("业务办理项id")
    private String serviceId;


}
