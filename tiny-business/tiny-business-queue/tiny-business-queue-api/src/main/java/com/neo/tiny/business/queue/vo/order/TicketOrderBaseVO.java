package com.neo.tiny.business.queue.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @author yqz
 * @Description: 取叫号业务流水基础类
 * @date 2022-10-27 22:10:16
 */
@Data
@ApiModel(description = "取叫号业务流水")
public class TicketOrderBaseVO implements Serializable {



    /**
     * 业务编码（交易流水号）
     */
    @ApiModelProperty("业务编码（交易流水号）")
    private String businessId;

    /**
     * 票号
     */
    @ApiModelProperty("票号")
    private String ticketNo;

    /**
     * 客户姓名
     */
    @ApiModelProperty("客户姓名")
    private String customerName;

    /**
     * 客户证件号码
     */
    @ApiModelProperty("客户证件号码")
    private String customerIdNo;

    /**
     * 客户联系方式
     */
    @ApiModelProperty("客户联系方式")
    private String customerPhone;

    /**
     * 窗口id
     */
    @ApiModelProperty("窗口id")
    private Long windowId;

    /**
     * 窗口名（缴话费）
     */
    @ApiModelProperty("窗口名（缴话费）")
    private String windowName;

    /**
     * 窗口号（A01）
     */
    @ApiModelProperty("窗口号（A01）")
    private String windowNo;

    /**
     * 队列主键
     */
    @ApiModelProperty("队列主键")
    private Long queueId;

    /**
     * 队列标识
     */
    @ApiModelProperty("队列标识")
    private String queueKey;

    /**
     * 队列名称
     */
    @ApiModelProperty("队列名称")
    private String queueName;


    @ApiModelProperty("取号机id")
    private Long ticketMachineId;

    /**
     * 取号机名称
     */
    @ApiModelProperty("取号机名称")
    private String ticketMachineName;

    /**
     * 取号机编码
     */
    @ApiModelProperty("取号机编码")
    private String ticketMachineKey;


    /**
     * 取号状态：0-预约，10-取号，20-叫号，30-离开，40-过号
     * {@link com.neo.tiny.business.queue.enums.TicketStatusEnum}
     */
    @ApiModelProperty("取号状态：0-预约，10-取号，20-叫号，30-离开，40-过号")
    private Integer ticketStatus;

    /**
     * 取号时间
     */
    @ApiModelProperty("取号时间")
    private LocalDateTime ticketPrintTime;

    /**
     * 叫号时间
     */
    @ApiModelProperty("叫号时间")
    private LocalDateTime ticketCallTime;

    /**
     * 离开时间
     */
    @ApiModelProperty("离开时间")
    private LocalDateTime leaveTime;

    /**
     * 过号时间
     */
    @ApiModelProperty("过号时间")
    private LocalDateTime passTime;


}
