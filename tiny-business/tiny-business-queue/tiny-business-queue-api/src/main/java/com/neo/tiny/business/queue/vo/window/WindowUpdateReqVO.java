package com.neo.tiny.business.queue.vo.window;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * @author yqzBpmFormUpdateReqVO.java
 * @Description: 窗口更新请求类
 * @date 2022-10-27 22:22:54
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "窗口")
public class WindowUpdateReqVO extends WindowBaseVO implements Serializable {


    /**
     * 窗口id（自增）
     */
    @ApiModelProperty("窗口id（自增）")
    private Long windowId;

}
