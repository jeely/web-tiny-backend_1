package com.neo.tiny.business.queue.client.service;

import com.neo.tiny.business.queue.api.vo.call.ListTicketForCallRes;

import java.util.List;

/**
 * @Description: 叫号业务
 * @Author: yqz
 * @CreateDate: 2022/10/30 11:37
 */
public interface CallTicketOrderService {

    /**
     * 根据窗口id，获取待叫号列表
     *
     * @param windowId 窗口id
     * @return 获取待叫号列表
     */
    List<ListTicketForCallRes> listTicketForCallByWindowId(Long windowId);

    /**
     * 根据取号主键id，进行叫号
     *
     * @param orderId  取号票号主键id
     * @param windowId 窗口id
     * @param ticketStatus 状态 {@link com.neo.tiny.business.queue.enums.TicketStatusEnum}
     * @return 叫号结果
     */
    Boolean call(Long orderId, Long windowId, Integer ticketStatus);
}
