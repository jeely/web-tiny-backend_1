package com.neo.tiny.business.queue.client.service.impl;

import com.neo.tiny.business.queue.api.TicketMachineApi;
import com.neo.tiny.business.queue.api.TicketOrderApi;
import com.neo.tiny.business.queue.client.service.ClientMachineService;
import com.neo.tiny.business.queue.dto.machine.MachineResDTO;
import com.neo.tiny.business.queue.dto.order.TicketOrderReqDTO;
import com.neo.tiny.business.queue.dto.order.TicketOrderResDTO;
import com.neo.tiny.business.queue.dto.queue.QueueInfoDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 客户端取号机业务
 * @Author: yqz
 * @CreateDate: 2022/10/29 23:37
 */
@Service
@AllArgsConstructor
public class ClientMachineServiceImpl implements ClientMachineService {

    private final TicketMachineApi ticketMachineApi;

    private final TicketOrderApi ticketOrderApi;


    @Override
    public List<MachineResDTO> listAllMachine() {
        return ticketMachineApi.listAllMachine();
    }

    @Override
    public List<QueueInfoDTO> listQueueByMachineId(Long machineId) {
        return ticketMachineApi.listQueueByMachineId(machineId);
    }

    @Override
    public TicketOrderResDTO createTicketOrder(TicketOrderReqDTO req) {
        // TODO 校验黑名单等业务
        TicketOrderResDTO ticketOrder = ticketOrderApi.createTicketOrder(req);
        return ticketOrder;
    }
}
