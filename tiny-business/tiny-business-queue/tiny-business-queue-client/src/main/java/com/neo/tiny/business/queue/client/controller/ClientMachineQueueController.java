package com.neo.tiny.business.queue.client.controller;

import cn.hutool.core.bean.BeanUtil;
import com.neo.tiny.business.queue.api.vo.order.TicketOrderCreateReqVO;
import com.neo.tiny.business.queue.client.service.ClientMachineService;
import com.neo.tiny.business.queue.dto.machine.MachineResDTO;
import com.neo.tiny.business.queue.dto.order.TicketOrderReqDTO;
import com.neo.tiny.business.queue.dto.order.TicketOrderResDTO;
import com.neo.tiny.business.queue.dto.queue.QueueInfoDTO;
import com.neo.tiny.common.common.ResResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description: 取号机设备控制器
 * @Author: yqz
 * @CreateDate: 2022/10/29 23:23
 */
@Api(tags = "取号机设备控制器")
@RestController
@AllArgsConstructor
@RequestMapping("/queue/machine/client")
public class ClientMachineQueueController {

    private final ClientMachineService clientMachineService;

    @ApiOperation("获取所有取号机")
    @GetMapping("/list-machine")
    public ResResult<List<MachineResDTO>> listMachine() {
        return ResResult.success(clientMachineService.listAllMachine());
    }


    @ApiOperation("根据取号机id查询该取号机下的队列列表")
    @GetMapping("/list-queue-machine/{machineId}")
    public ResResult<List<QueueInfoDTO>> listQueueByMachineId(@PathVariable("machineId") Long machineId) {

        return ResResult.success(clientMachineService.listQueueByMachineId(machineId));
    }


    @ApiOperation("取号机取号")
    @PostMapping("/create-ticket-no")
    public ResResult<TicketOrderResDTO> createTicketNo(@Valid @RequestBody TicketOrderCreateReqVO req) {

        TicketOrderReqDTO dto = BeanUtil.copyProperties(req, TicketOrderReqDTO.class);

        TicketOrderResDTO ticketOrder = clientMachineService.createTicketOrder(dto);

        return ResResult.success(ticketOrder);

    }
}
