package com.neo.tiny.business.queue.client.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Description: 叫号入参
 * @Author: yqz
 * @CreateDate: 2022/10/30 11:40
 */
@Data
public class CallTicketReq implements Serializable {

    @ApiModelProperty("票号主键id")
    @NotNull(message = "票号主键id不能为空")
    private Long orderId;

    // TODO 后续接入统一赋码后可以使用该id叫号
/*    @ApiModelProperty("业务订单id")
    @NotNull(message = "业务订单id不能为空")
    private String businessId;*/

    @ApiModelProperty("窗口主键id")
    @NotNull(message = "窗口主键id不能为空")
    private Long windowId;

    /**
     * 取号状态：0-预约，10-取号，20-叫号，30-离开，40-过号
     * {@link com.neo.tiny.business.queue.enums.TicketStatusEnum}
     */
    @ApiModelProperty("票号状态：0-预约，10-取号，20-叫号，30-离开，40-过号")
    @NotNull(message = "票号状态不能为空")
    private Integer ticketStatus;
}
