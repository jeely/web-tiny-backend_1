package com.neo.tiny.business.queue.client.service;

import com.neo.tiny.business.queue.dto.machine.MachineResDTO;
import com.neo.tiny.business.queue.dto.order.TicketOrderReqDTO;
import com.neo.tiny.business.queue.dto.order.TicketOrderResDTO;
import com.neo.tiny.business.queue.dto.queue.QueueInfoDTO;

import java.util.List;

/**
 * @Description: 客户端取号机service
 * @Author: yqz
 * @CreateDate: 2022/10/29 23:36
 */
public interface ClientMachineService {

    /**
     * 所有取号机
     *
     * @return 所有取号机列表
     */
    List<MachineResDTO> listAllMachine();


    /**
     * 根据取号机id，获取该机器对应的队列列表
     *
     * @param machineId 取号机id
     * @return 队列列表
     */
    List<QueueInfoDTO> listQueueByMachineId(Long machineId);

    /**
     * 创建取号
     *
     * @param req 取号入参
     * @return 取号结果
     */
    TicketOrderResDTO createTicketOrder(TicketOrderReqDTO req);
}
