package com.neo.tiny.business.queue.client.controller;

import com.neo.tiny.business.queue.api.vo.call.ListTicketForCallRes;
import com.neo.tiny.business.queue.client.service.CallTicketOrderService;
import com.neo.tiny.business.queue.client.vo.CallTicketReq;
import com.neo.tiny.common.common.ResResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/10/30 11:17
 */
@Api(tags = "叫号设备控制器")
@RestController
@AllArgsConstructor
@RequestMapping("/call/machine")
public class CallTicketOrderMachineController {


    private final CallTicketOrderService callTicketOrderService;

    /**
     * 获取本窗口的待叫号列表
     *
     * @return
     */
    @ApiOperation("获取本窗口的待叫号列表")
    @GetMapping("/list-for-call/{windowId}")
    public ResResult<List<ListTicketForCallRes>> listForCall(@PathVariable("windowId") Long windowId) {

        List<ListTicketForCallRes> forCallResList = callTicketOrderService.listTicketForCallByWindowId(windowId);

        return ResResult.success(forCallResList);
    }


    /**
     * 呼叫，由于一个队列对应多个窗口，此处会存在多个窗口争抢同一个号的情况，所有需要增加锁的机制
     */
    @ApiOperation("呼叫")
    @PostMapping("/call")
    public ResResult<Boolean> call(@Valid @RequestBody CallTicketReq req) {

        return ResResult.success(callTicketOrderService.call(req.getOrderId(), req.getWindowId(), req.getTicketStatus()));
    }

}
