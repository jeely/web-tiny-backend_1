
package com.neo.tiny.business.queue.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.business.queue.entity.QueueServiceRelationDO;

/**
 *
 * @Description: 队列与办理项
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:41:34
 */
public interface QueueServiceRelationService extends IService<QueueServiceRelationDO> {

}
