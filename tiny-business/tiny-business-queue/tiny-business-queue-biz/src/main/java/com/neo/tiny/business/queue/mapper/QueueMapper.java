package com.neo.tiny.business.queue.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.business.queue.entity.QueueDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: 队列
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:48:54
 */
@Mapper
public interface QueueMapper extends BaseMapper<QueueDO> {

}
