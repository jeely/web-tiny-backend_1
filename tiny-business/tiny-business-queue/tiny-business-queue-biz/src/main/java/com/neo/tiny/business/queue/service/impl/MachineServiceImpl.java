
package com.neo.tiny.business.queue.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.business.queue.entity.MachineDO;
import com.neo.tiny.business.queue.mapper.MachineMapper;
import com.neo.tiny.business.queue.service.MachineService;
import org.springframework.stereotype.Service;

/**
 *
 * @Description: 取号机
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:18:31
 */
@Service
public class MachineServiceImpl extends ServiceImpl<MachineMapper, MachineDO> implements MachineService {

}
