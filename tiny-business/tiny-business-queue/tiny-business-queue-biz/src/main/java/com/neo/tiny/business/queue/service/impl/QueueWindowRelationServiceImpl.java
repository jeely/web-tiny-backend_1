
package com.neo.tiny.business.queue.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.business.queue.entity.QueueWindowRelationDO;
import com.neo.tiny.business.queue.mapper.QueueWindowRelationMapper;
import com.neo.tiny.business.queue.service.QueueWindowRelationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Description: 队列与窗口
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:47:23
 */
@Service
@AllArgsConstructor
public class QueueWindowRelationServiceImpl extends ServiceImpl<QueueWindowRelationMapper, QueueWindowRelationDO> implements QueueWindowRelationService {


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean bindQueueWithWindow(Long queueId, List<Long> windowIds) {
        // 先删除关系
        this.remove(new LambdaQueryWrapper<QueueWindowRelationDO>().eq(QueueWindowRelationDO::getQueueId, queueId));

        if (Objects.isNull(queueId) || CollUtil.isEmpty(windowIds)) {
            return Boolean.TRUE;
        }

        List<QueueWindowRelationDO> queueWindowList = windowIds.stream().map(windowId -> {
            QueueWindowRelationDO relationDO = new QueueWindowRelationDO();
            relationDO.setQueueId(queueId);
            relationDO.setWindowId(windowId);

            return relationDO;
        }).collect(Collectors.toList());
        // 批量保存
        return this.saveBatch(queueWindowList);
    }
}
