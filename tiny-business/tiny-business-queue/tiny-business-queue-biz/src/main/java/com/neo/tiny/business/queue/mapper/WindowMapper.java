package com.neo.tiny.business.queue.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.business.queue.entity.WindowDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: 窗口
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:22:54
 */
@Mapper
public interface WindowMapper extends BaseMapper<WindowDO> {

}
