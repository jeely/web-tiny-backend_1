package com.neo.tiny.business.queue.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.business.queue.entity.WindowDO;
import com.neo.tiny.business.queue.service.WindowService;
import com.neo.tiny.business.queue.vo.window.WindowCreateReqVO;
import com.neo.tiny.business.queue.vo.window.WindowPageReqVO;
import com.neo.tiny.business.queue.vo.window.WindowResVO;
import com.neo.tiny.business.queue.vo.window.WindowUpdateReqVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.common.util.CommonDoTransfer;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.util.MyBatisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * @Description: 窗口
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:22:54
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/business-window")
@Api(tags = "窗口管理")
public class WindowController {

    private final WindowService windowService;

    /**
     * 分页查询
     *
     * @param req 窗口
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<IPage<WindowResVO>> getWindowPage(@Valid WindowPageReqVO req) {

        LambdaQueryWrapper<WindowDO> wrapper = new LambdaQueryWrapper<WindowDO>()
                .like(StrUtil.isNotBlank(req.getWindowName()), WindowDO::getWindowName, req.getWindowName())
                .or()
                .like(StrUtil.isNotBlank(req.getWindowNo()), WindowDO::getWindowNo, req.getWindowNo())
                .orderByAsc(WindowDO::getSortOrder);

        return ResResult.success(CommonPage
                .restPage(windowService.page(MyBatisUtils.buildPage(req), wrapper), WindowResVO.class));
    }


    /**
     * 通过id查询窗口
     *
     * @param windowId id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{windowId}")
    public ResResult<WindowResVO> getById(@PathVariable("windowId") Long windowId) {
        return ResResult.success(BeanUtil.copyProperties(windowService.getById(windowId), WindowResVO.class));
    }

    /**
     * 新增窗口
     *
     * @param windowCreateReqVO 窗口
     * @return ResResult
     */
    @ApiOperation("新增窗口")
    @PostMapping("/create")
    public ResResult<Long> save(@Valid @RequestBody WindowCreateReqVO windowCreateReqVO) {
        checkWindowNoAndKey(null, windowCreateReqVO.getWindowNo(), windowCreateReqVO.getWindowKey());
        WindowDO window = BeanUtil.copyProperties(windowCreateReqVO, WindowDO.class);


        windowService.save(BeanUtil.copyProperties(windowCreateReqVO, WindowDO.class));

        return ResResult.success(window.getWindowId());
    }

    /**
     * 校验窗口号、窗口标识
     *
     * @param windowId  窗口id
     * @param windowNo  窗口号
     * @param windowKey 窗口标识
     */
    public void checkWindowNoAndKey(Long windowId, String windowNo, String windowKey) {
        long count = windowService.count(new LambdaQueryWrapper<WindowDO>()
                .eq(StrUtil.isNotBlank(windowNo), WindowDO::getWindowNo, windowNo)
                .ne(!Objects.isNull(windowId), WindowDO::getWindowId, windowId));
        Assert.isTrue(count <= 0, "窗口号重复");
        long countKey = windowService.count(new LambdaQueryWrapper<WindowDO>()
                .eq(StrUtil.isNotBlank(windowNo), WindowDO::getWindowKey, windowKey)
                .ne(!Objects.isNull(windowId), WindowDO::getWindowId, windowId));
        Assert.isTrue(countKey <= 0, "窗口标识重复");

    }

    /**
     * 修改窗口
     *
     * @param windowUpdateReqVO 窗口
     * @return ResResult
     */
    @ApiOperation("修改窗口")
    @PutMapping("/update")
    public ResResult<Boolean> updateById(@Valid @RequestBody WindowUpdateReqVO windowUpdateReqVO) {
        checkWindowNoAndKey(windowUpdateReqVO.getWindowId(), windowUpdateReqVO.getWindowNo(), windowUpdateReqVO.getWindowKey());

        windowService.updateById(BeanUtil.copyProperties(windowUpdateReqVO, WindowDO.class));
        return ResResult.success(true);
    }

    /**
     * 通过id删除窗口
     *
     * @param windowId id
     * @return R
     */
    @ApiOperation("通过id删除窗口")
    @DeleteMapping("/{windowId}")
    public ResResult<Boolean> removeById(@PathVariable Long windowId) {
        return ResResult.success(windowService.removeById(windowId));
    }

    @ApiOperation("查询所有窗口")
    @GetMapping("/list")
    public ResResult<List<WindowResVO>> listAllWindow() {

        List<WindowDO> list = windowService.list();

        return ResResult.success(CommonDoTransfer.transfer(list, WindowResVO.class));
    }

}
