package com.neo.tiny.business.queue.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.business.queue.entity.QueueWindowRelationDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: 队列与窗口
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:47:23
 */
@Mapper
public interface QueueWindowRelationMapper extends BaseMapper<QueueWindowRelationDO> {

}
