package com.neo.tiny.business.queue.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.business.queue.entity.TicketOrderDO;
import com.neo.tiny.business.queue.service.TicketOrderService;
import com.neo.tiny.business.queue.vo.order.TicketOrderPageReqVO;
import com.neo.tiny.business.queue.vo.order.TicketOrderResVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.query.LambdaQueryWrapperBase;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.util.MyBatisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Description: 取叫号业务流水
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:10:16
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/business-ticket-order")
@Api(tags = "取叫号业务流水管理")
public class TicketOrderController {

    private final TicketOrderService ticketOrderService;

    /**
     * 分页查询
     *
     * @param req 取叫号业务流水
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<IPage<TicketOrderResVO>> getTicketOrderPage(@Valid TicketOrderPageReqVO req) {

        LambdaQueryWrapperBase<TicketOrderDO> wrapper = new LambdaQueryWrapperBase<TicketOrderDO>()
                .likeIfPresent(TicketOrderDO::getCustomerName, req.getCustomerName())
                .eqIfPresent(TicketOrderDO::getTicketNo, req.getTicketNo())
                .orderByDesc(TicketOrderDO::getCreateTime);

        return ResResult.success(CommonPage.restPage(ticketOrderService
                .page(MyBatisUtils.buildPage(req), wrapper), TicketOrderResVO.class));
    }


    /**
     * 通过id查询取叫号业务流水
     *
     * @param orderId id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{orderId}")
    public ResResult<TicketOrderResVO> getById(@PathVariable("orderId") Long orderId) {
        return ResResult.success(BeanUtil.copyProperties(ticketOrderService.getById(orderId), TicketOrderResVO.class));
    }


    /**
     * 通过id删除取叫号业务流水
     *
     * @param orderId id
     * @return R
     */
    @ApiOperation("通过id删除取叫号业务流水")
    @DeleteMapping("/{orderId}")
    public ResResult<Boolean> removeById(@PathVariable Long orderId) {
        return ResResult.success(ticketOrderService.removeById(orderId));
    }

}
