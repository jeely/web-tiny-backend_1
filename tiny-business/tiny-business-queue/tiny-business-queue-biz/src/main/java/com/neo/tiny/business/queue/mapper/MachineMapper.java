package com.neo.tiny.business.queue.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.business.queue.entity.MachineDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: 取号机
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:18:31
 */
@Mapper
public interface MachineMapper extends BaseMapper<MachineDO> {

}
