package com.neo.tiny.business.queue.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.business.queue.entity.QueueServiceRelationDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: 队列与办理项
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:41:34
 */
@Mapper
public interface QueueServiceRelationMapper extends BaseMapper<QueueServiceRelationDO> {

}
