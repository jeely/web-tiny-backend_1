
package com.neo.tiny.business.queue.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.business.queue.entity.MachineDO;

/**
 *
 * @Description: 取号机
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:18:31
 */
public interface MachineService extends IService<MachineDO> {

}
