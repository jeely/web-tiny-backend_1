package com.neo.tiny.business.queue.api.machine;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.neo.tiny.business.queue.api.TicketMachineApi;
import com.neo.tiny.business.queue.dto.machine.MachineResDTO;
import com.neo.tiny.business.queue.dto.queue.QueueInfoDTO;
import com.neo.tiny.business.queue.entity.MachineDO;
import com.neo.tiny.business.queue.entity.QueueDO;
import com.neo.tiny.business.queue.service.MachineService;
import com.neo.tiny.business.queue.service.QueueService;
import com.neo.tiny.business.queue.service.QueueTicketMachineRelationService;
import com.neo.tiny.common.util.CommonDoTransfer;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @Description: 取号机api实现类
 * @Author: yqz
 * @CreateDate: 2022/10/29 23:32
 */
@Service
@AllArgsConstructor
public class TicketMachineApiImpl implements TicketMachineApi {

    private final MachineService machineService;

    private final QueueTicketMachineRelationService queueTicketMachineRelationService;

    private final QueueService queueService;

    @Override
    public List<MachineResDTO> listAllMachine() {
        List<MachineDO> list = machineService.list();

        return CommonDoTransfer.transfer(list, MachineResDTO.class);
    }

    @Override
    public List<QueueInfoDTO> listQueueByMachineId(Long machineId) {
        Set<Long> queueIds = queueTicketMachineRelationService.listQueueIdsByMachineId(machineId);

        if (CollUtil.isEmpty(queueIds)) {
            return new ArrayList<>();
        }
        List<QueueDO> list = queueService.list(new LambdaQueryWrapper<QueueDO>().in(QueueDO::getQueueId, queueIds));

        return CommonDoTransfer.transfer(list, QueueInfoDTO.class);
    }
}
