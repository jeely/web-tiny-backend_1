
package com.neo.tiny.business.queue.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.business.queue.entity.TicketOrderDO;

/**
 *
 * @Description: 取叫号业务流水
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:10:16
 */
public interface TicketOrderService extends IService<TicketOrderDO> {

}
