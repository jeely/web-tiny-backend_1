
package com.neo.tiny.business.queue.entity;

import com.neo.tiny.data.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @Description: 队列与办理项
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:41:34
 */
@Data
@TableName("business_queue_service_relation")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "队列与办理项")
public class QueueServiceRelationDO extends BaseDO {

    /**
     * 自增主键
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("自增主键")
    private Long relationId;

    /**
     * 队列id
     */
    @ApiModelProperty("队列id")
    private Long queueId;

    /**
     * 业务办理项id
     */
    @ApiModelProperty("业务办理项id")
    private String serviceId;


}
