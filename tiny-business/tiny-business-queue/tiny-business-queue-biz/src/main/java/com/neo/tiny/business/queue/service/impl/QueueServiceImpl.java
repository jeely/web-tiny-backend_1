
package com.neo.tiny.business.queue.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.business.queue.entity.QueueDO;
import com.neo.tiny.business.queue.mapper.QueueMapper;
import com.neo.tiny.business.queue.service.QueueService;
import org.springframework.stereotype.Service;

/**
 *
 * @Description: 队列
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:48:54
 */
@Service
public class QueueServiceImpl extends ServiceImpl<QueueMapper, QueueDO> implements QueueService {

}
