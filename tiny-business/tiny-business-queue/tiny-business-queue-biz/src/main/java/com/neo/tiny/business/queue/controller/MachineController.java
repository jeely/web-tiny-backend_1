package com.neo.tiny.business.queue.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.neo.tiny.business.queue.entity.MachineDO;
import com.neo.tiny.business.queue.service.MachineService;
import com.neo.tiny.business.queue.service.QueueTicketMachineRelationService;
import com.neo.tiny.business.queue.vo.machine.MachineCreateReqVO;
import com.neo.tiny.business.queue.vo.machine.MachinePageReqVO;
import com.neo.tiny.business.queue.vo.machine.MachineResVO;
import com.neo.tiny.business.queue.vo.machine.MachineUpdateReqVO;
import com.neo.tiny.business.queue.vo.ticketmachine.QueueTicketMachineRelationCreateReqVO;
import com.neo.tiny.common.common.ResResult;
import com.neo.tiny.resolver.CommonPage;
import com.neo.tiny.util.MyBatisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;
import java.util.Set;

/**
 * @Description: 取号机
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:18:31
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/business-machine")
@Api(tags = "取号机管理")
public class MachineController {

    private final MachineService machineService;

    private final QueueTicketMachineRelationService queueTicketMachineRelationService;

    /**
     * 分页查询
     *
     * @param req 取号机
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public ResResult<IPage<MachineResVO>> getMachinePage(@Valid MachinePageReqVO req) {

        LambdaQueryWrapper<MachineDO> wrapper = new LambdaQueryWrapper<MachineDO>()
                .like(StrUtil.isNotBlank(req.getTicketMachineName()), MachineDO::getTicketMachineName, req.getTicketMachineName())
                .or()
                .like(StrUtil.isNotBlank(req.getTicketMachineKey()), MachineDO::getTicketMachineKey, req.getTicketMachineKey())
                .orderByAsc(MachineDO::getSortOrder);

        return ResResult.success(CommonPage.restPage(machineService
                .page(MyBatisUtils.buildPage(req), wrapper), MachineResVO.class));
    }


    /**
     * 通过id查询取号机
     *
     * @param ticketMachineId id
     * @return ResResult
     */
    @ApiOperation("通过id查询")
    @GetMapping("/{ticketMachineId}")
    public ResResult<MachineResVO> getById(@PathVariable("ticketMachineId") Long ticketMachineId) {
        return ResResult.success(BeanUtil.copyProperties(machineService.getById(ticketMachineId), MachineResVO.class));
    }

    /**
     * 新增取号机
     *
     * @param machineCreateReqVO 取号机
     * @return ResResult
     */
    @ApiOperation("新增取号机")
    @PostMapping("/create")
    public ResResult<Long> save(@Valid @RequestBody MachineCreateReqVO machineCreateReqVO) {
        checkMachineKey(null, machineCreateReqVO.getTicketMachineKey());
        MachineDO machine = BeanUtil.copyProperties(machineCreateReqVO, MachineDO.class);
        machineService.save(BeanUtil.copyProperties(machineCreateReqVO, MachineDO.class));

        return ResResult.success(machine.getTicketMachineId());
    }

    /**
     * 校验窗口号、窗口标识
     *
     * @param ticketMachineId 取号机id
     * @param machineKey      取号机标识
     */
    public void checkMachineKey(Long ticketMachineId, String machineKey) {
        long count = machineService.count(new LambdaQueryWrapper<MachineDO>()
                .eq(StrUtil.isNotBlank(machineKey), MachineDO::getTicketMachineKey, machineKey)
                .ne(!Objects.isNull(ticketMachineId), MachineDO::getTicketMachineId, ticketMachineId));
        Assert.isTrue(count <= 0, "取号机标识重复");

    }

    /**
     * 修改取号机
     *
     * @param machineUpdateReqVO 取号机
     * @return ResResult
     */
    @ApiOperation("修改取号机")
    @PutMapping("/update")
    public ResResult<Boolean> updateById(@Valid @RequestBody MachineUpdateReqVO machineUpdateReqVO) {
        checkMachineKey(machineUpdateReqVO.getTicketMachineId(), machineUpdateReqVO.getTicketMachineKey());

        machineService.updateById(BeanUtil.copyProperties(machineUpdateReqVO, MachineDO.class));
        return ResResult.success(true);
    }

    /**
     * 通过id删除取号机
     *
     * @param ticketMachineId id
     * @return R
     */
    @ApiOperation("通过id删除取号机")
    @DeleteMapping("/{ticketMachineId}")
    public ResResult<Boolean> removeById(@PathVariable Long ticketMachineId) {
        return ResResult.success(machineService.removeById(ticketMachineId));
    }

    @ApiOperation("查询队列已绑定的队列")
    @GetMapping("/list-queue-machine/{machineId}")
    public ResResult<Set<Long>> listQueueWithWin(@PathVariable("machineId") Long machineId) {

        return ResResult.success(queueTicketMachineRelationService.listQueueIdsByMachineId(machineId));
    }

    @ApiOperation("绑定队列与取号机")
    @PostMapping("/bind-queue-machine")
    public ResResult<Boolean> bindQueueWithWindow(@Valid @RequestBody QueueTicketMachineRelationCreateReqVO req) {

        log.info("绑定队列与窗口入参：{}", req);

        return ResResult.success(queueTicketMachineRelationService.bindQueueWithMachine(req.getMachineId(), req.getQueueIds()));
    }
}
