
package com.neo.tiny.business.queue.entity;

import com.neo.tiny.data.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @Description: 取号机
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:18:31
 */
@Data
@TableName("business_machine")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "取号机")
public class MachineDO extends BaseDO {

    /**
     * 取号机id（自增）
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("取号机id（自增）")
    private Long ticketMachineId;

    /**
     * 取号机名称
     */
    @ApiModelProperty("取号机名称")
    private String ticketMachineName;

    /**
     * 取号机编码
     */
    @ApiModelProperty("取号机编码")
    private String ticketMachineKey;

    /**
     * 取号机所属业务区
     */
    @ApiModelProperty("取号机所属业务区")
    private String machineArea;

    /**
     * 取号机物理地址
     */
    @ApiModelProperty("取号机物理地址")
    private String macAddress;

    /**
     * 取号机简介
     */
    @ApiModelProperty("取号机简介")
    private String machineDescribe;

    /**
     * 排序：正序
     */
    @ApiModelProperty("排序：正序")
    private Integer sortOrder;


}
