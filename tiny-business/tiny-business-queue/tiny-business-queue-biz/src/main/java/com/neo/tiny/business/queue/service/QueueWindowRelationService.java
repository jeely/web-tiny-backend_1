
package com.neo.tiny.business.queue.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.business.queue.entity.QueueWindowRelationDO;

import java.util.List;

/**
 * @Description: 队列与窗口
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:47:23
 */
public interface QueueWindowRelationService extends IService<QueueWindowRelationDO> {

    /**
     * 绑定队列与窗口的关系
     *
     * @param queueId   队列id
     * @param windowIds 窗口id集合
     * @return
     */
    Boolean bindQueueWithWindow(Long queueId, List<Long> windowIds);
}
