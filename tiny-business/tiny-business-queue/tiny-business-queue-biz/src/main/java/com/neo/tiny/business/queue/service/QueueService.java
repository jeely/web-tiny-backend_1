
package com.neo.tiny.business.queue.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.business.queue.entity.QueueDO;

/**
 *
 * @Description: 队列
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:48:54
 */
public interface QueueService extends IService<QueueDO> {

}
