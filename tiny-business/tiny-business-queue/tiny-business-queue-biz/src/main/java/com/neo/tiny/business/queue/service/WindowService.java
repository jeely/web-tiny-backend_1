
package com.neo.tiny.business.queue.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.business.queue.entity.WindowDO;

/**
 *
 * @Description: 窗口
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:22:54
 */
public interface WindowService extends IService<WindowDO> {

}
