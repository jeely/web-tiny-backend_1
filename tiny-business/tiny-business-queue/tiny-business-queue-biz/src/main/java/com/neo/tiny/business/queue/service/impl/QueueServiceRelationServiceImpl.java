
package com.neo.tiny.business.queue.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.business.queue.entity.QueueServiceRelationDO;
import com.neo.tiny.business.queue.mapper.QueueServiceRelationMapper;
import com.neo.tiny.business.queue.service.QueueServiceRelationService;
import org.springframework.stereotype.Service;

/**
 *
 * @Description: 队列与办理项
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:41:34
 */
@Service
public class QueueServiceRelationServiceImpl extends ServiceImpl<QueueServiceRelationMapper, QueueServiceRelationDO> implements QueueServiceRelationService {

}
