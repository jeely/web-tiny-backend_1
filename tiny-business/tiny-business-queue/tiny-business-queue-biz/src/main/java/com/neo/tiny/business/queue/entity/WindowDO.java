
package com.neo.tiny.business.queue.entity;

import com.neo.tiny.data.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @Description: 窗口
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:22:54
 */
@Data
@TableName("business_window")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "窗口")
public class WindowDO extends BaseDO {

    /**
     * 窗口id（自增）
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("窗口id（自增）")
    private Long windowId;

    /**
     * 窗口名（缴话费）
     */
    @ApiModelProperty("窗口名（缴话费）")
    private String windowName;

    /**
     * 窗口号（A01）
     */
    @ApiModelProperty("窗口号（A01）")
    private String windowNo;

    /**
     * 窗口编码
     */
    @ApiModelProperty("窗口编码")
    private String windowKey;

    /**
     * 窗口简介
     */
    @ApiModelProperty("窗口简介")
    private String windowDescribe;

    /**
     * 排序：正序
     */
    @ApiModelProperty("排序：正序")
    private Integer sortOrder;


    /**
     * 窗口状态（0-空闲，1-受理中，2-暂停服务）
     */
    @ApiModelProperty("窗口状态（0-空闲，1-受理中，2-暂停服务）")
    private Integer windowStatus;


}
