package com.neo.tiny.business.queue.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.business.queue.entity.TicketOrderDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: 取叫号业务流水
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:10:16
 */
@Mapper
public interface TicketOrderMapper extends BaseMapper<TicketOrderDO> {

}
