
package com.neo.tiny.business.queue.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.business.queue.entity.TicketOrderDO;
import com.neo.tiny.business.queue.mapper.TicketOrderMapper;
import com.neo.tiny.business.queue.service.TicketOrderService;
import org.springframework.stereotype.Service;

/**
 *
 * @Description: 取叫号业务流水
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:10:16
 */
@Service
public class TicketOrderServiceImpl extends ServiceImpl<TicketOrderMapper, TicketOrderDO> implements TicketOrderService {

}
