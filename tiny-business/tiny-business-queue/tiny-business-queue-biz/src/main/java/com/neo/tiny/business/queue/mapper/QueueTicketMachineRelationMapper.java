package com.neo.tiny.business.queue.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neo.tiny.business.queue.entity.QueueTicketMachineRelationDO;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @Description: 队列与取号机
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:39:51
 */
@Mapper
public interface QueueTicketMachineRelationMapper extends BaseMapper<QueueTicketMachineRelationDO> {

}
