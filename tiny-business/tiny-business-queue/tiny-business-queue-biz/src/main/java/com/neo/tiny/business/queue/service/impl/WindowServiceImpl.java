
package com.neo.tiny.business.queue.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.neo.tiny.business.queue.entity.WindowDO;
import com.neo.tiny.business.queue.mapper.WindowMapper;
import com.neo.tiny.business.queue.service.WindowService;
import org.springframework.stereotype.Service;

/**
 *
 * @Description: 窗口
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:22:54
 */
@Service
public class WindowServiceImpl extends ServiceImpl<WindowMapper, WindowDO> implements WindowService {

}
