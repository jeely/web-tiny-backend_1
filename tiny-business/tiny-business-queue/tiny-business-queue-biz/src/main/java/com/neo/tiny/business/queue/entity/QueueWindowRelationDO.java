
package com.neo.tiny.business.queue.entity;

import com.neo.tiny.data.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @Description: 队列与窗口
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:47:23
 */
@Data
@TableName("business_queue_window_relation")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "队列与窗口")
public class QueueWindowRelationDO extends BaseDO {

    /**
     * 主键自增
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("主键自增")
    private Long relationId;

    /**
     * 队列id
     */
    @ApiModelProperty("队列id")
    private Long queueId;

    /**
     * 窗口id
     */
    @ApiModelProperty("窗口id")
    private Long windowId;


}
