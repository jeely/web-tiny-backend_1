
package com.neo.tiny.business.queue.entity;

import com.neo.tiny.data.BaseDO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @Description: 队列
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:48:54
 */
@Data
@TableName("business_queue")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "队列")
public class QueueDO extends BaseDO {

    /**
     * 主键自增
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("主键自增")
    private Long queueId;

    /**
     * 队列标识
     */
    @ApiModelProperty("队列标识")
    private String queueKey;

    /**
     * 队列名称
     */
    @ApiModelProperty("队列名称")
    private String queueName;

    /**
     * 队列所在区域（专区）
     */
    @ApiModelProperty("队列所在区域（专区）")
    private String queueArea;

    /**
     * 队列状态：0-激活，1-挂起
     */
    @ApiModelProperty("队列状态：0-激活，1-挂起")
    private Integer queueState;

    /**
     * 队列描述
     */
    @ApiModelProperty("队列描述")
    private String queueDescribe;

    /**
     * 日最大取号量限制
     */
    @ApiModelProperty("日最大取号量限制")
    private Integer ticketMaxLimit;

    /**
     * 排序：正序
     */
    @ApiModelProperty("排序：正序")
    private Integer sortOrder;


}
