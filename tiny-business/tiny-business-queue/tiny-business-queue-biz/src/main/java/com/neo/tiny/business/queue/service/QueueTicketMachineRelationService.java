
package com.neo.tiny.business.queue.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neo.tiny.business.queue.entity.QueueTicketMachineRelationDO;

import java.util.List;
import java.util.Set;

/**
 * @Description: 队列与取号机
 * @Author: yqz
 * @CreateDate: 2022-10-27 22:39:51
 */
public interface QueueTicketMachineRelationService extends IService<QueueTicketMachineRelationDO> {

    /**
     * 绑定取号机对应的队列
     *
     * @param machineId 取号机id
     * @param queueIds  队列id集合
     * @return
     */
    Boolean bindQueueWithMachine(Long machineId, List<Long> queueIds);

    /**
     * 根据取号机id获取队列id集合
     *
     * @param machineId 取号机id
     * @return
     */
    Set<Long> listQueueIdsByMachineId(Long machineId);
}
