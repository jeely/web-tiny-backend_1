package com.neo.tiny;

import cn.hutool.json.JSONUtil;
import com.neo.tiny.common.constant.CacheConstants;
import com.neo.tiny.admin.entity.sys.SysUser;
import com.neo.tiny.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

/**
 * @Description: redis测试
 * @Author: yqz
 * @CreateDate: 2022/8/13 16:32
 */
@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class RedisCRUDTest {

    @Autowired
    private RedisService redisService;

    @Test
    public void hSet() {

        String key = "hSetAll";

        String hKey = "pc-1";

        Map<String, Object> map = new HashMap<>();

        map.put("name", "张三");
        map.put("sex", "男");
        map.put("age", "18");

        Boolean hSetAll = redisService.hSetAll(key, map, 60 * 5);
        log.info("保存结果：{}", hSetAll);
        redisService.hSet(key, hKey, "mac-1", 60 * 10);

        Object sex = redisService.hGet(key, "sex");
        log.info("sex:{}", sex);

        Boolean hKey1 = redisService.hHasKey(key, hKey);
        log.info("是否含有hKey-1：{}", hKey1);

        //删除Hash结构中的属性
        redisService.hDel(key, hKey);

        Boolean hKey2 = redisService.hHasKey(key, hKey);
        log.info("是否含有hKey2：{}", hKey2);


        Map<Object, Object> all = redisService.hGetAll(key);
        log.info("all:{}", all);
        redisService.hSetAll(key + "1", map);
        redisService.del(key);
    }

    @Test
    public void incr() {
        String incrKey = "incr";
        Long incr = redisService.incr(incrKey, 1);
        log.info("递增：{}", incr);

        Long decr = redisService.decr(incrKey, 2);
        log.info("当前值：{}", decr);


        String hIncr = "hIncr";
        Map<String, Object> map = new HashMap<>();
        map.put("incr", 5);
        map.put("decr", 20);
        redisService.hSetAll(hIncr, map);
        Long hIncrValue = redisService.hIncr(hIncr, "incr", 2L);
        log.info("hIncr递增：{}", hIncrValue);
        Long hDecrValue = redisService.hDecr(hIncr, "decr", 5L);
        log.info("hIncr递减：{}", hDecrValue);

    }

    @Test
    public void forSet() {

        String forSetKey = "forSetKey";
        Set<String> hashSet = new HashSet<>();

        hashSet.add("张三");
        hashSet.add("李四");
        hashSet.add("王五");

        redisService.sAdd(forSetKey, 60 * 60, hashSet.toArray());

        Set<Object> members = redisService.sMembers(forSetKey);
        log.info("forSetKey ==>:{}", JSONUtil.toJsonStr(members));

        Boolean first = redisService.sIsMember(forSetKey, "张三");
        Boolean second = redisService.sIsMember(forSetKey, "王五");
        log.info("是否为集合值：{}==={}", first, second);
        Long length = redisService.sSize(forSetKey);

        log.info("集合长度：{}", length);

        Long after = redisService.sRemove(forSetKey, "张三");

        Set<Object> afterValue = redisService.sMembers(forSetKey);
        log.info("afterValue ==>:{}==={}", JSONUtil.toJsonStr(afterValue), after);

    }

    @Test
    public void list() {
        String listKey = "listKey";

        List<Map<String, Object>> list = new ArrayList<>();

        Map<String, Object> first = new HashMap<>();
        first.put("first", "这是第一个对象");

        Map<String, Object> second = new HashMap<>();
        second.put("second", "这是第二个对象");

        list.add(first);
        list.add(second);

        Long all = redisService.lPushAll(listKey, 60 * 60L, list.toArray());
        log.info("保存list：{}", all);
        Map<String, Object> third = new HashMap<>();
        third.put("third", "这是第三个对象");
        redisService.lPush(listKey, third);

        Object o = redisService.lIndex(listKey, 0);
        log.info("索引为0 的：{}", o);

    }

    @Test
    public void key() {


        redisService.set(CacheConstants.USER_DETAILS,new SysUser());

        Object o = redisService.get(CacheConstants.USER_DETAILS);
        log.info("用户信息：:{}",o);

    }
}
