package com.neo.tiny;

import com.neo.tiny.admin.entity.sys.SysUser;
import com.neo.tiny.admin.service.sys.SysUserService;
import com.neo.tiny.codegen.entity.GenFormConf;
import com.neo.tiny.codegen.service.GenFormConfService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @Description:
 * @Author: yqz
 * @CreateDate: 2022/8/6 15:08
 */
@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class ServerApplicationTests {

    @Autowired
    private GenFormConfService genFormConfService;

    @Autowired
    private SysUserService userService;

    @Test
    public void contextLoads() {

        List<GenFormConf> list = genFormConfService.list();
        log.info("结果：{}", list);
    }

    @Test
    public void loadUsers() {

        List<SysUser> list = userService.list();
        log.info("所有用户：{}", list);
    }
}
