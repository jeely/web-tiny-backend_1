package com.neo.tiny;

import com.neo.tiny.middle.socket.websocket.WebSocketSysServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author yqz
 * @Description 基础服务启动类
 * @CreateDate 2022/8/3 16:08
 */
@SpringBootApplication
public class ServerApplication {
    public static void main(String[] args) {

        // SpringApplication.run(ServerApplication.class, args);

        ConfigurableApplicationContext applicationContext = SpringApplication.run(ServerApplication.class, args);
        WebSocketSysServer.setApplicationContext(applicationContext);
    }
}
